.class Lcom/google/uploader/client/ResumableTransfer;
.super Ljava/lang/Object;
.source "ResumableTransfer.java"

# interfaces
.implements Lcom/google/uploader/client/Transfer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;,
        Lcom/google/uploader/client/ResumableTransfer$Pair;,
        Lcom/google/uploader/client/ResumableTransfer$ControlState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private backoffSecs:J

.field private chunkGranularity:I

.field private controlState:Lcom/google/uploader/client/ResumableTransfer$ControlState;

.field private currentWaitSecs:D

.field private final httpClient:Lcom/google/uploader/client/HttpClient;

.field private httpRequest:Lcom/google/uploader/client/Transfer;

.field private final idleTimeoutSecs:J

.field private progressThreshold:I

.field private random:Ljava/util/Random;

.field private startRequestHeaders:Lcom/google/api/client/http/HttpHeaders;

.field private startRequestMetadata:Ljava/lang/String;

.field private startRequestMethod:Ljava/lang/String;

.field private startRequestUrl:Ljava/lang/String;

.field private transferHandle:Lcom/google/common/io/protocol/ProtoBuf;

.field private transferListener:Lcom/google/uploader/client/TransferListener;

.field private final uploadStream:Lcom/google/uploader/client/DataStream;

.field private uploadUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/uploader/client/ResumableTransfer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/uploader/client/ResumableTransfer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;Ljava/lang/String;Lcom/google/uploader/client/HttpClient;J)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "startRequestMethod"    # Ljava/lang/String;
    .param p3, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p4, "body"    # Lcom/google/uploader/client/DataStream;
    .param p5, "metadata"    # Ljava/lang/String;
    .param p6, "httpClient"    # Lcom/google/uploader/client/HttpClient;
    .param p7, "idleTimeoutSecs"    # J

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I

    .line 205
    iput-object p1, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestUrl:Ljava/lang/String;

    .line 206
    iput-object p2, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMethod:Ljava/lang/String;

    .line 207
    iput-object p3, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestHeaders:Lcom/google/api/client/http/HttpHeaders;

    .line 208
    iput-object p5, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMetadata:Ljava/lang/String;

    .line 209
    iput-object p6, p0, Lcom/google/uploader/client/ResumableTransfer;->httpClient:Lcom/google/uploader/client/HttpClient;

    .line 210
    iput-object p4, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    .line 211
    iput-wide p7, p0, Lcom/google/uploader/client/ResumableTransfer;->idleTimeoutSecs:J

    .line 212
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/uploader/client/ResumableTransfer;->currentWaitSecs:D

    .line 213
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    .line 214
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->random:Ljava/util/Random;

    .line 215
    sget-object v0, Lcom/google/uploader/client/ResumableTransfer$ControlState;->IN_PROGRESS:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    iput-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->controlState:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    .line 216
    return-void
.end method

.method static synthetic access$000(Lcom/google/uploader/client/ResumableTransfer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/ResumableTransfer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/uploader/client/ResumableTransfer;)Lcom/google/uploader/client/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/ResumableTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->startNewUpload()Lcom/google/uploader/client/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/uploader/client/ResumableTransfer;Z)Lcom/google/uploader/client/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/ResumableTransfer;
    .param p1, "x1"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/uploader/client/ResumableTransfer;->resumeExistingUpload(Z)Lcom/google/uploader/client/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/uploader/client/ResumableTransfer;)Lcom/google/uploader/client/TransferListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/ResumableTransfer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    return-object v0
.end method

.method private declared-synchronized checkControlState()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 781
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->controlState:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/ResumableTransfer$ControlState;->PAUSED:Lcom/google/uploader/client/ResumableTransfer$ControlState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 784
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 785
    :catch_0
    move-exception v0

    goto :goto_0

    .line 790
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->controlState:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/ResumableTransfer$ControlState;->CANCELED:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    if-ne v0, v1, :cond_1

    .line 792
    new-instance v0, Lcom/google/uploader/client/TransferException;

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->CANCELED:Lcom/google/uploader/client/TransferException$Type;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 781
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 795
    :cond_1
    :try_start_3
    sget-boolean v0, Lcom/google/uploader/client/ResumableTransfer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->controlState:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/ResumableTransfer$ControlState;->IN_PROGRESS:Lcom/google/uploader/client/ResumableTransfer$ControlState;

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 796
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public static createNewTransfer(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;Ljava/lang/String;Lcom/google/uploader/client/HttpClient;Lcom/google/uploader/client/TransferOptions;)Lcom/google/uploader/client/ResumableTransfer;
    .locals 10
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p3, "body"    # Lcom/google/uploader/client/DataStream;
    .param p4, "metadata"    # Ljava/lang/String;
    .param p5, "httpClient"    # Lcom/google/uploader/client/HttpClient;
    .param p6, "options"    # Lcom/google/uploader/client/TransferOptions;

    .prologue
    .line 146
    new-instance v1, Lcom/google/uploader/client/ResumableTransfer;

    invoke-virtual/range {p6 .. p6}, Lcom/google/uploader/client/TransferOptions;->getIdleTimeoutSecs()J

    move-result-wide v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/uploader/client/ResumableTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;Ljava/lang/String;Lcom/google/uploader/client/HttpClient;J)V

    return-object v1
.end method

.method private createRequest(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/Transfer;
    .locals 6
    .param p1, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p2, "command"    # Ljava/lang/String;
    .param p3, "body"    # Lcom/google/uploader/client/DataStream;

    .prologue
    .line 725
    if-nez p1, :cond_1

    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 726
    .local v2, "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    :goto_0
    const-string v4, "X-Goog-Upload-Protocol"

    const-string v5, "resumable"

    invoke-virtual {v2, v4, v5}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 727
    const-string v4, "X-Goog-Upload-Command"

    invoke-virtual {v2, v4, p2}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 728
    const-string v4, "start"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestUrl:Ljava/lang/String;

    .line 729
    .local v3, "url":Ljava/lang/String;
    :goto_1
    const-string v4, "upload"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMethod:Ljava/lang/String;

    .line 730
    .local v0, "method":Ljava/lang/String;
    :goto_2
    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer;->httpClient:Lcom/google/uploader/client/HttpClient;

    invoke-interface {v4, v3, v0, v2, p3}, Lcom/google/uploader/client/HttpClient;->createTransfer(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/Transfer;

    move-result-object v1

    .line 732
    .local v1, "request":Lcom/google/uploader/client/Transfer;
    const-string v4, "start"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 733
    monitor-enter p0

    .line 734
    :try_start_0
    new-instance v4, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;

    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    invoke-direct {v4, p0, p0, v5}, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;-><init>(Lcom/google/uploader/client/ResumableTransfer;Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferListener;)V

    iget v5, p0, Lcom/google/uploader/client/ResumableTransfer;->progressThreshold:I

    invoke-interface {v1, v4, v5}, Lcom/google/uploader/client/Transfer;->attachListener(Lcom/google/uploader/client/TransferListener;I)V

    .line 736
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 738
    :cond_0
    return-object v1

    .line 725
    .end local v0    # "method":Ljava/lang/String;
    .end local v1    # "request":Lcom/google/uploader/client/Transfer;
    .end local v2    # "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpHeaders;->clone()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v2

    goto :goto_0

    .line 728
    .restart local v2    # "requestHeaders":Lcom/google/api/client/http/HttpHeaders;
    :cond_2
    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadUrl:Ljava/lang/String;

    goto :goto_1

    .line 729
    .restart local v3    # "url":Ljava/lang/String;
    :cond_3
    const-string v0, "PUT"

    goto :goto_2

    .line 736
    .restart local v0    # "method":Ljava/lang/String;
    .restart local v1    # "request":Lcom/google/uploader/client/Transfer;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private expBackoff(Lcom/google/uploader/client/TransferException;)V
    .locals 6
    .param p1, "ex"    # Lcom/google/uploader/client/TransferException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 803
    iget-wide v2, p0, Lcom/google/uploader/client/ResumableTransfer;->currentWaitSecs:D

    iget-wide v4, p0, Lcom/google/uploader/client/ResumableTransfer;->idleTimeoutSecs:J

    long-to-double v4, v4

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_0

    .line 804
    throw p1

    .line 808
    :cond_0
    iget-object v2, p0, Lcom/google/uploader/client/ResumableTransfer;->random:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    .line 810
    .local v0, "rand":D
    :try_start_0
    iget-wide v2, p0, Lcom/google/uploader/client/ResumableTransfer;->currentWaitSecs:D

    iget-wide v4, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    long-to-double v4, v4

    mul-double/2addr v4, v0

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/google/uploader/client/ResumableTransfer;->currentWaitSecs:D

    .line 811
    iget-wide v2, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    long-to-double v2, v2

    mul-double/2addr v2, v0

    double-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 816
    :goto_0
    iget-wide v2, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    iget-wide v4, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    .line 817
    return-void

    .line 812
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getDataStreamForNextChunk()Lcom/google/uploader/client/ResumableTransfer$Pair;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/uploader/client/ResumableTransfer$Pair",
            "<",
            "Lcom/google/uploader/client/DataStream;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 580
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->uploadStreamHasMoreData()Z

    move-result v3

    if-nez v3, :cond_0

    .line 581
    new-instance v3, Lcom/google/uploader/client/ResumableTransfer$Pair;

    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/google/uploader/client/ResumableTransfer$Pair;-><init>(Lcom/google/uploader/client/ResumableTransfer;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 607
    :goto_0
    return-object v3

    .line 584
    :cond_0
    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v3}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v6

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    .line 585
    new-instance v3, Lcom/google/uploader/client/ResumableTransfer$Pair;

    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/google/uploader/client/ResumableTransfer$Pair;-><init>(Lcom/google/uploader/client/ResumableTransfer;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 588
    :cond_1
    const/4 v0, 0x0

    .line 590
    .local v0, "chunkedStream":Lcom/google/uploader/client/DataStream;
    :try_start_0
    new-instance v0, Lcom/google/uploader/client/ChunkedDataStream;

    .end local v0    # "chunkedStream":Lcom/google/uploader/client/DataStream;
    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    iget v6, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I

    invoke-direct {v0, v3, v6}, Lcom/google/uploader/client/ChunkedDataStream;-><init>(Lcom/google/uploader/client/DataStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 596
    .restart local v0    # "chunkedStream":Lcom/google/uploader/client/DataStream;
    invoke-interface {v0}, Lcom/google/uploader/client/DataStream;->getSize()J

    move-result-wide v6

    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v3}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v8

    iget v3, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I

    int-to-long v10, v3

    div-long/2addr v8, v10

    iget v3, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I

    int-to-long v10, v3

    mul-long/2addr v8, v10

    cmp-long v3, v6, v8

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v3}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    invoke-interface {v0}, Lcom/google/uploader/client/DataStream;->getSize()J

    move-result-wide v8

    add-long/2addr v6, v8

    iget-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v3}, Lcom/google/uploader/client/DataStream;->getSize()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_3

    :cond_2
    move v2, v5

    .line 600
    .local v2, "isFinalChunk":Z
    :goto_1
    if-eqz v2, :cond_4

    .line 604
    new-instance v3, Lcom/google/uploader/client/ResumableTransfer$Pair;

    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Lcom/google/uploader/client/ResumableTransfer$Pair;-><init>(Lcom/google/uploader/client/ResumableTransfer;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 591
    .end local v0    # "chunkedStream":Lcom/google/uploader/client/DataStream;
    .end local v2    # "isFinalChunk":Z
    :catch_0
    move-exception v1

    .line 592
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/google/uploader/client/TransferException;

    sget-object v4, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v5, "Could not create chunked data stream."

    invoke-direct {v3, v4, v5}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v3

    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "chunkedStream":Lcom/google/uploader/client/DataStream;
    :cond_3
    move v2, v4

    .line 596
    goto :goto_1

    .line 607
    .restart local v2    # "isFinalChunk":Z
    :cond_4
    new-instance v3, Lcom/google/uploader/client/ResumableTransfer$Pair;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v3, p0, v0, v4}, Lcom/google/uploader/client/ResumableTransfer$Pair;-><init>(Lcom/google/uploader/client/ResumableTransfer;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private is4xxResponse(Lcom/google/uploader/client/HttpResponse;)Z
    .locals 2
    .param p1, "response"    # Lcom/google/uploader/client/HttpResponse;

    .prologue
    .line 769
    invoke-virtual {p1}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFinalResponse(Lcom/google/uploader/client/HttpResponse;)Z
    .locals 3
    .param p1, "response"    # Lcom/google/uploader/client/HttpResponse;

    .prologue
    .line 745
    invoke-virtual {p1}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    if-nez v1, :cond_0

    .line 746
    const/4 v1, 0x0

    .line 750
    :goto_0
    return v1

    .line 749
    :cond_0
    invoke-virtual {p1}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v1

    const-string v2, "X-Goog-Upload-Status"

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 750
    .local v0, "state":Ljava/lang/String;
    const-string v1, "final"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private isUploadSessionActive(Lcom/google/uploader/client/HttpResponse;)Z
    .locals 4
    .param p1, "response"    # Lcom/google/uploader/client/HttpResponse;

    .prologue
    const/4 v1, 0x0

    .line 757
    invoke-virtual {p1}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v2

    if-nez v2, :cond_1

    .line 762
    :cond_0
    :goto_0
    return v1

    .line 761
    :cond_1
    invoke-virtual {p1}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v2

    const-string v3, "X-Goog-Upload-Status"

    invoke-virtual {v2, v3}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 762
    .local v0, "state":Ljava/lang/String;
    const-string v2, "active"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private issueQueryRequest()Lcom/google/uploader/client/HttpResponse;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 497
    const/4 v3, 0x0

    .line 500
    .local v3, "queryResponse":Lcom/google/uploader/client/HttpResponse;
    :goto_0
    :try_start_0
    new-instance v5, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v5}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    const-string v6, "query"

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v7}, Lcom/google/uploader/client/ResumableTransfer;->sendRequest(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/HttpResponse;
    :try_end_0
    .catch Lcom/google/uploader/client/TransferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 510
    invoke-direct {p0, v3}, Lcom/google/uploader/client/ResumableTransfer;->isFinalResponse(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 573
    .end local v3    # "queryResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_0
    :goto_1
    return-object v3

    .line 501
    .restart local v3    # "queryResponse":Lcom/google/uploader/client/HttpResponse;
    :catch_0
    move-exception v2

    .line 502
    .local v2, "e":Lcom/google/uploader/client/TransferException;
    invoke-virtual {v2}, Lcom/google/uploader/client/TransferException;->isRecoverable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 503
    throw v2

    .line 506
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/uploader/client/ResumableTransfer;->expBackoff(Lcom/google/uploader/client/TransferException;)V

    goto :goto_0

    .line 514
    .end local v2    # "e":Lcom/google/uploader/client/TransferException;
    :cond_2
    invoke-direct {p0, v3}, Lcom/google/uploader/client/ResumableTransfer;->isUploadSessionActive(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 528
    const-wide/16 v0, 0x0

    .line 530
    .local v0, "confirmedOffset":J
    :try_start_1
    invoke-virtual {v3}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v5

    const-string v6, "X-Goog-Upload-Size-Received"

    invoke-virtual {v5, v6}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    .line 539
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v5}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-gez v5, :cond_4

    .line 541
    new-instance v4, Lcom/google/uploader/client/TransferException;

    sget-object v5, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "The server lost bytes that were previously committed. Our offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v7}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", server offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v4

    .line 518
    .end local v0    # "confirmedOffset":J
    :cond_3
    invoke-direct {p0, v3}, Lcom/google/uploader/client/ResumableTransfer;->is4xxResponse(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 522
    new-instance v5, Lcom/google/uploader/client/TransferException;

    sget-object v6, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-virtual {v3}, Lcom/google/uploader/client/HttpResponse;->toDebugString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/google/uploader/client/ResumableTransfer;->expBackoff(Lcom/google/uploader/client/TransferException;)V

    goto :goto_0

    .line 534
    .restart local v0    # "confirmedOffset":J
    :catch_1
    move-exception v2

    .line 535
    .local v2, "e":Ljava/lang/NumberFormatException;
    new-instance v4, Lcom/google/uploader/client/TransferException;

    sget-object v5, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v6, "Failed to parse X-Goog-Upload-Size-Received header"

    invoke-direct {v4, v5, v6, v2}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 547
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v5}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-gez v5, :cond_5

    .line 550
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v5}, Lcom/google/uploader/client/DataStream;->rewind()V

    .line 553
    :cond_5
    :goto_2
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v5}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    cmp-long v5, v6, v0

    if-gez v5, :cond_7

    .line 554
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->uploadStreamHasMoreData()Z

    move-result v5

    if-nez v5, :cond_6

    .line 555
    new-instance v4, Lcom/google/uploader/client/TransferException;

    sget-object v5, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Upload stream does not have more data but it should. Maybe the caller passed in a data stream to upload with a mark position that didn\'t match the transfer handle? Confirmed offset from server: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v7}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v4

    .line 563
    :cond_6
    :try_start_2
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    iget-object v6, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v6}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    sub-long v6, v0, v6

    invoke-interface {v5, v6, v7}, Lcom/google/uploader/client/DataStream;->skip(J)J

    .line 564
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v5}, Lcom/google/uploader/client/DataStream;->mark()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 565
    :catch_2
    move-exception v2

    .line 568
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Lcom/google/uploader/client/TransferException;

    sget-object v5, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v6, "Could not skip in the data stream."

    invoke-direct {v4, v5, v6, v2}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 572
    .end local v2    # "e":Ljava/io/IOException;
    :cond_7
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->markUploadProgress()V

    move-object v3, v4

    .line 573
    goto/16 :goto_1
.end method

.method private markUploadProgress()V
    .locals 4

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v0}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v2}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v0}, Lcom/google/uploader/client/DataStream;->mark()V

    .line 622
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->resetBackoff()V

    .line 623
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->transferHandle:Lcom/google/common/io/protocol/ProtoBuf;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v2}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 625
    :cond_0
    return-void
.end method

.method private resetBackoff()V
    .locals 2

    .prologue
    .line 773
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/uploader/client/ResumableTransfer;->backoffSecs:J

    .line 774
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/uploader/client/ResumableTransfer;->currentWaitSecs:D

    .line 775
    return-void
.end method

.method private resumeExistingUpload(Z)Lcom/google/uploader/client/HttpResponse;
    .locals 12
    .param p1, "sendQuery"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 426
    :goto_0
    if-eqz p1, :cond_2

    .line 427
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->issueQueryRequest()Lcom/google/uploader/client/HttpResponse;

    move-result-object v1

    .line 428
    .local v1, "finalResponse":Lcom/google/uploader/client/HttpResponse;
    if-eqz v1, :cond_1

    move-object v7, v1

    .line 482
    .end local v1    # "finalResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_0
    return-object v7

    .line 431
    .restart local v1    # "finalResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_1
    const/4 p1, 0x0

    .line 435
    .end local v1    # "finalResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_2
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->getDataStreamForNextChunk()Lcom/google/uploader/client/ResumableTransfer$Pair;

    move-result-object v5

    .line 436
    .local v5, "pair":Lcom/google/uploader/client/ResumableTransfer$Pair;, "Lcom/google/uploader/client/ResumableTransfer$Pair<Lcom/google/uploader/client/DataStream;Ljava/lang/Boolean;>;"
    iget-object v4, v5, Lcom/google/uploader/client/ResumableTransfer$Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/google/uploader/client/DataStream;

    .line 437
    .local v4, "nextRequestBody":Lcom/google/uploader/client/DataStream;
    iget-object v8, v5, Lcom/google/uploader/client/ResumableTransfer$Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 438
    .local v3, "isFinalChunk":Z
    const-string v6, ""

    .line 439
    .local v6, "uploadCommand":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->uploadStreamHasMoreData()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 440
    if-eqz v3, :cond_3

    .line 441
    const-string v6, "upload, finalize"

    .line 450
    :goto_1
    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 451
    .local v2, "headers":Lcom/google/api/client/http/HttpHeaders;
    const-string v8, "X-Goog-Upload-Offset"

    iget-object v9, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v9}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 452
    const/4 v7, 0x0

    .line 454
    .local v7, "uploadResponse":Lcom/google/uploader/client/HttpResponse;
    :try_start_0
    invoke-direct {p0, v2, v6, v4}, Lcom/google/uploader/client/ResumableTransfer;->sendRequest(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/HttpResponse;
    :try_end_0
    .catch Lcom/google/uploader/client/TransferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 465
    invoke-direct {p0, v7}, Lcom/google/uploader/client/ResumableTransfer;->isFinalResponse(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 469
    invoke-direct {p0, v7}, Lcom/google/uploader/client/ResumableTransfer;->isUploadSessionActive(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 470
    if-eqz v3, :cond_6

    .line 471
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v10, "Finalize call returned active state."

    invoke-direct {v8, v9, v10}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v8

    .line 443
    .end local v2    # "headers":Lcom/google/api/client/http/HttpHeaders;
    .end local v7    # "uploadResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_3
    const-string v6, "upload"

    goto :goto_1

    .line 446
    :cond_4
    const-string v6, "finalize"

    goto :goto_1

    .line 455
    .restart local v2    # "headers":Lcom/google/api/client/http/HttpHeaders;
    .restart local v7    # "uploadResponse":Lcom/google/uploader/client/HttpResponse;
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Lcom/google/uploader/client/TransferException;
    invoke-virtual {v0}, Lcom/google/uploader/client/TransferException;->isRecoverable()Z

    move-result v8

    if-nez v8, :cond_5

    .line 457
    throw v0

    .line 460
    :cond_5
    const/4 p1, 0x1

    .line 461
    invoke-direct {p0, v0}, Lcom/google/uploader/client/ResumableTransfer;->expBackoff(Lcom/google/uploader/client/TransferException;)V

    goto :goto_0

    .line 473
    .end local v0    # "e":Lcom/google/uploader/client/TransferException;
    :cond_6
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->markUploadProgress()V

    goto :goto_0

    .line 477
    :cond_7
    invoke-direct {p0, v7}, Lcom/google/uploader/client/ResumableTransfer;->is4xxResponse(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 481
    invoke-virtual {v7}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v8

    const/16 v9, 0x190

    if-ne v8, v9, :cond_0

    .line 486
    :cond_8
    const/4 p1, 0x1

    .line 487
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-virtual {v7}, Lcom/google/uploader/client/HttpResponse;->toDebugString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lcom/google/uploader/client/ResumableTransfer;->expBackoff(Lcom/google/uploader/client/TransferException;)V

    goto/16 :goto_0
.end method

.method private sendRequest(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/HttpResponse;
    .locals 7
    .param p1, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p2, "command"    # Ljava/lang/String;
    .param p3, "body"    # Lcom/google/uploader/client/DataStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 686
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->checkControlState()V

    .line 687
    invoke-direct {p0, p1, p2, p3}, Lcom/google/uploader/client/ResumableTransfer;->createRequest(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/Transfer;

    move-result-object v3

    .line 688
    .local v3, "request":Lcom/google/uploader/client/Transfer;
    const/4 v2, 0x0

    .line 690
    .local v2, "exceptionOrResponseFuture":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Lcom/google/uploader/client/TransferExceptionOrHttpResponse;>;"
    monitor-enter p0

    .line 691
    :try_start_0
    iput-object v3, p0, Lcom/google/uploader/client/ResumableTransfer;->httpRequest:Lcom/google/uploader/client/Transfer;

    .line 692
    invoke-interface {v3}, Lcom/google/uploader/client/Transfer;->send()Ljava/util/concurrent/Future;

    move-result-object v2

    .line 693
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 695
    :try_start_1
    invoke-interface {v2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    .line 704
    .local v1, "exceptionOrResponse":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    invoke-virtual {v1}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->hasTransferException()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 706
    invoke-virtual {v1}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->getTransferException()Lcom/google/uploader/client/TransferException;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/uploader/client/TransferException;->getType()Lcom/google/uploader/client/TransferException$Type;

    move-result-object v4

    sget-object v5, Lcom/google/uploader/client/TransferException$Type;->CANCELED:Lcom/google/uploader/client/TransferException$Type;

    if-eq v4, v5, :cond_0

    .line 707
    invoke-virtual {v1}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->getTransferException()Lcom/google/uploader/client/TransferException;

    move-result-object v4

    throw v4

    .line 693
    .end local v1    # "exceptionOrResponse":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 696
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Ljava/lang/InterruptedException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected error occurred: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 699
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 701
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected error occurred: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 711
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v1    # "exceptionOrResponse":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    :cond_0
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->checkControlState()V

    .line 713
    new-instance v4, Lcom/google/uploader/client/TransferException;

    sget-object v5, Lcom/google/uploader/client/TransferException$Type;->CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v6, ""

    invoke-direct {v4, v5, v6}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v4

    .line 715
    :cond_1
    invoke-virtual {v1}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->getHttpResponse()Lcom/google/uploader/client/HttpResponse;

    move-result-object v4

    return-object v4
.end method

.method private startNewUpload()Lcom/google/uploader/client/HttpResponse;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 316
    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v11, Lcom/google/uploader/client/ClientProto/ClientMessageTypes;->TRANSFER_HANDLE:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v11}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 317
    .local v1, "currentHandle":Lcom/google/common/io/protocol/ProtoBuf;
    const/4 v11, 0x1

    iget-object v12, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestUrl:Ljava/lang/String;

    invoke-virtual {v1, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 318
    const/4 v11, 0x7

    iget-wide v12, p0, Lcom/google/uploader/client/ResumableTransfer;->idleTimeoutSecs:J

    invoke-virtual {v1, v11, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 319
    const/4 v11, 0x3

    const/4 v12, 0x2

    invoke-virtual {v1, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    .line 321
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMetadata:Ljava/lang/String;

    if-eqz v11, :cond_0

    .line 322
    const/4 v11, 0x2

    iget-object v12, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMetadata:Ljava/lang/String;

    invoke-virtual {v1, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 324
    :cond_0
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestHeaders:Lcom/google/api/client/http/HttpHeaders;

    if-eqz v11, :cond_2

    .line 325
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestHeaders:Lcom/google/api/client/http/HttpHeaders;

    invoke-virtual {v11}, Lcom/google/api/client/http/HttpHeaders;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 326
    .local v4, "headerName":Ljava/lang/String;
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestHeaders:Lcom/google/api/client/http/HttpHeaders;

    invoke-virtual {v11, v4}, Lcom/google/api/client/http/HttpHeaders;->getHeaderStringValues(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 327
    .local v5, "headerValue":Ljava/lang/String;
    new-instance v3, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v11, Lcom/google/uploader/client/ClientProto/ClientMessageTypes;->HEADER:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v11}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 328
    .local v3, "header":Lcom/google/common/io/protocol/ProtoBuf;
    const/4 v11, 0x1

    invoke-virtual {v3, v11, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 329
    const/4 v11, 0x2

    invoke-virtual {v3, v11, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 331
    const/16 v11, 0x8

    invoke-virtual {v1, v11, v3}, Lcom/google/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    .line 335
    .end local v3    # "header":Lcom/google/common/io/protocol/ProtoBuf;
    .end local v4    # "headerName":Ljava/lang/String;
    .end local v5    # "headerValue":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_2
    iput-object v1, p0, Lcom/google/uploader/client/ResumableTransfer;->transferHandle:Lcom/google/common/io/protocol/ProtoBuf;

    .line 338
    monitor-enter p0

    .line 339
    :try_start_0
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    if-eqz v11, :cond_3

    .line 340
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    invoke-interface {v11, p0}, Lcom/google/uploader/client/TransferListener;->onStart(Lcom/google/uploader/client/Transfer;)V

    .line 342
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    invoke-direct {p0}, Lcom/google/uploader/client/ResumableTransfer;->resetBackoff()V

    .line 345
    const/4 v8, 0x0

    .line 348
    .local v8, "startResponse":Lcom/google/uploader/client/HttpResponse;
    :goto_1
    :try_start_1
    iget-object v12, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestHeaders:Lcom/google/api/client/http/HttpHeaders;

    const-string v13, "start"

    new-instance v14, Lcom/google/uploader/client/StringDataStream;

    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMetadata:Ljava/lang/String;

    if-nez v11, :cond_5

    const-string v11, ""

    :goto_2
    invoke-direct {v14, v11}, Lcom/google/uploader/client/StringDataStream;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v12, v13, v14}, Lcom/google/uploader/client/ResumableTransfer;->sendRequest(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/HttpResponse;
    :try_end_1
    .catch Lcom/google/uploader/client/TransferException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    .line 361
    invoke-direct {p0, v8}, Lcom/google/uploader/client/ResumableTransfer;->isFinalResponse(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 416
    .end local v8    # "startResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_4
    :goto_3
    return-object v8

    .line 342
    :catchall_0
    move-exception v11

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v11

    .line 348
    .restart local v8    # "startResponse":Lcom/google/uploader/client/HttpResponse;
    :cond_5
    :try_start_3
    iget-object v11, p0, Lcom/google/uploader/client/ResumableTransfer;->startRequestMetadata:Ljava/lang/String;
    :try_end_3
    .catch Lcom/google/uploader/client/TransferException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 352
    :catch_0
    move-exception v2

    .line 353
    .local v2, "e":Lcom/google/uploader/client/TransferException;
    invoke-virtual {v2}, Lcom/google/uploader/client/TransferException;->isRecoverable()Z

    move-result v11

    if-nez v11, :cond_6

    .line 354
    throw v2

    .line 357
    :cond_6
    invoke-direct {p0, v2}, Lcom/google/uploader/client/ResumableTransfer;->expBackoff(Lcom/google/uploader/client/TransferException;)V

    goto :goto_1

    .line 365
    .end local v2    # "e":Lcom/google/uploader/client/TransferException;
    :cond_7
    invoke-direct {p0, v8}, Lcom/google/uploader/client/ResumableTransfer;->isUploadSessionActive(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 379
    invoke-virtual {v8}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v11

    const-string v12, "X-Goog-Upload-URL"

    invoke-virtual {v11, v12}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 383
    .local v9, "url":Ljava/lang/String;
    :try_start_4
    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 384
    .local v10, "validUrl":Ljava/net/URL;
    iput-object v9, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadUrl:Ljava/lang/String;
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_1

    .line 391
    invoke-virtual {v8}, Lcom/google/uploader/client/HttpResponse;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v11

    const-string v12, "X-Goog-Upload-Chunk-Granularity"

    invoke-virtual {v11, v12}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 394
    .local v0, "chunkGranularityString":Ljava/lang/String;
    if-eqz v0, :cond_8

    .line 396
    :try_start_5
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    iput v11, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_2

    .line 405
    :cond_8
    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    .end local v1    # "currentHandle":Lcom/google/common/io/protocol/ProtoBuf;
    sget-object v11, Lcom/google/uploader/client/ClientProto/ClientMessageTypes;->TRANSFER_HANDLE:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v11}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 406
    .restart local v1    # "currentHandle":Lcom/google/common/io/protocol/ProtoBuf;
    const/4 v11, 0x1

    iget-object v12, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadUrl:Ljava/lang/String;

    invoke-virtual {v1, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 407
    const/16 v11, 0x9

    iget-object v12, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadUrl:Ljava/lang/String;

    invoke-virtual {v1, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 408
    const/4 v11, 0x7

    iget-wide v12, p0, Lcom/google/uploader/client/ResumableTransfer;->idleTimeoutSecs:J

    invoke-virtual {v1, v11, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 409
    const/4 v11, 0x3

    const/4 v12, 0x2

    invoke-virtual {v1, v11, v12}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    .line 411
    iget v11, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I

    const/4 v12, 0x1

    if-le v11, v12, :cond_9

    .line 412
    const/4 v11, 0x5

    iget v12, p0, Lcom/google/uploader/client/ResumableTransfer;->chunkGranularity:I

    int-to-long v12, v12

    invoke-virtual {v1, v11, v12, v13}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 415
    :cond_9
    iput-object v1, p0, Lcom/google/uploader/client/ResumableTransfer;->transferHandle:Lcom/google/common/io/protocol/ProtoBuf;

    .line 416
    const/4 v11, 0x0

    invoke-direct {p0, v11}, Lcom/google/uploader/client/ResumableTransfer;->resumeExistingUpload(Z)Lcom/google/uploader/client/HttpResponse;

    move-result-object v8

    goto :goto_3

    .line 369
    .end local v0    # "chunkGranularityString":Ljava/lang/String;
    .end local v9    # "url":Ljava/lang/String;
    .end local v10    # "validUrl":Ljava/net/URL;
    :cond_a
    invoke-direct {p0, v8}, Lcom/google/uploader/client/ResumableTransfer;->is4xxResponse(Lcom/google/uploader/client/HttpResponse;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 373
    new-instance v11, Lcom/google/uploader/client/TransferException;

    sget-object v12, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-virtual {v8}, Lcom/google/uploader/client/HttpResponse;->toDebugString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    invoke-direct {p0, v11}, Lcom/google/uploader/client/ResumableTransfer;->expBackoff(Lcom/google/uploader/client/TransferException;)V

    goto/16 :goto_1

    .line 385
    .restart local v9    # "url":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 386
    .local v2, "e":Ljava/net/MalformedURLException;
    new-instance v11, Lcom/google/uploader/client/TransferException;

    sget-object v12, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v13, "Server returned an invalid upload url."

    invoke-direct {v11, v12, v13}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v11

    .line 397
    .end local v2    # "e":Ljava/net/MalformedURLException;
    .restart local v0    # "chunkGranularityString":Ljava/lang/String;
    .restart local v10    # "validUrl":Ljava/net/URL;
    :catch_2
    move-exception v2

    .line 399
    .local v2, "e":Ljava/lang/NumberFormatException;
    new-instance v11, Lcom/google/uploader/client/TransferException;

    sget-object v12, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v13, "Server returned an invalid chunk granularity."

    invoke-direct {v11, v12, v13, v2}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11
.end method

.method private uploadStreamHasMoreData()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 612
    :try_start_0
    iget-object v1, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1}, Lcom/google/uploader/client/DataStream;->hasMoreData()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 613
    :catch_0
    move-exception v0

    .line 614
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/google/uploader/client/TransferException;

    sget-object v2, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v3, "Could not call hasMoreData() on upload stream."

    invoke-direct {v1, v2, v3, v0}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public declared-synchronized attachListener(Lcom/google/uploader/client/TransferListener;I)V
    .locals 2
    .param p1, "listener"    # Lcom/google/uploader/client/TransferListener;
    .param p2, "progressThreshold"    # I

    .prologue
    .line 262
    monitor-enter p0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "Progress threshold must be greater than 0"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 263
    iput-object p1, p0, Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    .line 264
    iput p2, p0, Lcom/google/uploader/client/ResumableTransfer;->progressThreshold:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    monitor-exit p0

    return-void

    .line 262
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getBytesUploaded()J
    .locals 2

    .prologue
    .line 673
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer;->uploadStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v0}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public send()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/TransferExceptionOrHttpResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    new-instance v1, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lcom/google/uploader/client/ResumableTransfer$1;

    invoke-direct {v2, p0}, Lcom/google/uploader/client/ResumableTransfer$1;-><init>(Lcom/google/uploader/client/ResumableTransfer;)V

    invoke-direct {v1, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 305
    .local v1, "future":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Lcom/google/uploader/client/TransferExceptionOrHttpResponse;>;"
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 306
    .local v0, "executor":Ljava/util/concurrent/ExecutorService;
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 307
    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 308
    return-object v1
.end method
