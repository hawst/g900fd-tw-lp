.class public Lcom/google/uploader/client/TransferException;
.super Ljava/lang/Exception;
.source "TransferException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/TransferException$Type;
    }
.end annotation


# instance fields
.field private final type:Lcom/google/uploader/client/TransferException$Type;


# direct methods
.method public constructor <init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Lcom/google/uploader/client/TransferException$Type;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "type"    # Lcom/google/uploader/client/TransferException$Type;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 67
    invoke-direct {p0, p2, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    iput-object p1, p0, Lcom/google/uploader/client/TransferException;->type:Lcom/google/uploader/client/TransferException$Type;

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "type"    # Lcom/google/uploader/client/TransferException$Type;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    return-void
.end method


# virtual methods
.method public getType()Lcom/google/uploader/client/TransferException$Type;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/uploader/client/TransferException;->type:Lcom/google/uploader/client/TransferException$Type;

    return-object v0
.end method

.method public isRecoverable()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/uploader/client/TransferException;->type:Lcom/google/uploader/client/TransferException$Type;

    invoke-virtual {v0}, Lcom/google/uploader/client/TransferException$Type;->isRecoverable()Z

    move-result v0

    return v0
.end method
