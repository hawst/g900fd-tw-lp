.class Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;
.super Ljava/lang/Object;
.source "HttpUrlConnectionHttpClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->send()Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/uploader/client/TransferExceptionOrHttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;


# direct methods
.method constructor <init>(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    .locals 5

    .prologue
    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    # invokes: Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->sendInternal()Lcom/google/uploader/client/HttpResponse;
    invoke-static {v2}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->access$000(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/HttpResponse;

    move-result-object v1

    .line 144
    .local v1, "response":Lcom/google/uploader/client/HttpResponse;
    iget-object v3, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    monitor-enter v3
    :try_end_0
    .catch Lcom/google/uploader/client/TransferException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :try_start_1
    iget-object v2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    # getter for: Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v2}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->access$100(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 146
    iget-object v2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    # getter for: Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v2}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->access$100(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v2

    iget-object v4, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    invoke-interface {v2, v4, v1}, Lcom/google/uploader/client/TransferListener;->onResponseReceived(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/HttpResponse;)V

    .line 149
    :cond_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :try_start_2
    new-instance v2, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    invoke-direct {v2, v1}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;-><init>(Lcom/google/uploader/client/HttpResponse;)V
    :try_end_2
    .catch Lcom/google/uploader/client/TransferException; {:try_start_2 .. :try_end_2} :catch_0

    .line 157
    .end local v1    # "response":Lcom/google/uploader/client/HttpResponse;
    :goto_0
    return-object v2

    .line 149
    .restart local v1    # "response":Lcom/google/uploader/client/HttpResponse;
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Lcom/google/uploader/client/TransferException; {:try_start_4 .. :try_end_4} :catch_0

    .line 151
    .end local v1    # "response":Lcom/google/uploader/client/HttpResponse;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lcom/google/uploader/client/TransferException;
    iget-object v3, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    monitor-enter v3

    .line 153
    :try_start_5
    iget-object v2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    # getter for: Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v2}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->access$100(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 154
    iget-object v2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    # getter for: Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v2}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->access$100(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v2

    iget-object v4, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->this$0:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    invoke-interface {v2, v4, v0}, Lcom/google/uploader/client/TransferListener;->onException(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferException;)V

    .line 156
    :cond_1
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 157
    new-instance v2, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    invoke-direct {v2, v0}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;-><init>(Lcom/google/uploader/client/TransferException;)V

    goto :goto_0

    .line 156
    :catchall_1
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v2
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;->call()Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    move-result-object v0

    return-object v0
.end method
