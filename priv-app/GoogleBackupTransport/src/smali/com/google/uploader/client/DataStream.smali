.class public interface abstract Lcom/google/uploader/client/DataStream;
.super Ljava/lang/Object;
.source "DataStream.java"


# virtual methods
.method public abstract getMarkPosition()J
.end method

.method public abstract getReadAheadLimit()J
.end method

.method public abstract getReadPosition()J
.end method

.method public abstract getSize()J
.end method

.method public abstract hasMoreData()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract mark()V
.end method

.method public abstract read([BII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract rewind()V
.end method

.method public abstract skip(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
