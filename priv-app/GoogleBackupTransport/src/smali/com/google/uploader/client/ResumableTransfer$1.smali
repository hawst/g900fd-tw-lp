.class Lcom/google/uploader/client/ResumableTransfer$1;
.super Ljava/lang/Object;
.source "ResumableTransfer.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/uploader/client/ResumableTransfer;->send()Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/uploader/client/TransferExceptionOrHttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/uploader/client/ResumableTransfer;


# direct methods
.method constructor <init>(Lcom/google/uploader/client/ResumableTransfer;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    .locals 8

    .prologue
    .line 280
    :try_start_0
    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    # getter for: Lcom/google/uploader/client/ResumableTransfer;->uploadUrl:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/uploader/client/ResumableTransfer;->access$000(Lcom/google/uploader/client/ResumableTransfer;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    # invokes: Lcom/google/uploader/client/ResumableTransfer;->startNewUpload()Lcom/google/uploader/client/HttpResponse;
    invoke-static {v4}, Lcom/google/uploader/client/ResumableTransfer;->access$100(Lcom/google/uploader/client/ResumableTransfer;)Lcom/google/uploader/client/HttpResponse;

    move-result-object v1

    .line 282
    .local v1, "response":Lcom/google/uploader/client/HttpResponse;
    :goto_0
    new-instance v2, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    invoke-direct {v2, v1}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;-><init>(Lcom/google/uploader/client/HttpResponse;)V
    :try_end_0
    .catch Lcom/google/uploader/client/TransferException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 289
    .end local v1    # "response":Lcom/google/uploader/client/HttpResponse;
    .local v2, "result":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    :goto_1
    iget-object v5, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    monitor-enter v5

    .line 290
    :try_start_1
    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    # getter for: Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v4}, Lcom/google/uploader/client/ResumableTransfer;->access$300(Lcom/google/uploader/client/ResumableTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 291
    invoke-virtual {v2}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->hasHttpResponse()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 292
    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    # getter for: Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v4}, Lcom/google/uploader/client/ResumableTransfer;->access$300(Lcom/google/uploader/client/ResumableTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v4

    iget-object v6, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    invoke-virtual {v2}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->getHttpResponse()Lcom/google/uploader/client/HttpResponse;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lcom/google/uploader/client/TransferListener;->onResponseReceived(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/HttpResponse;)V

    .line 299
    :cond_0
    :goto_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300
    return-object v2

    .line 280
    .end local v2    # "result":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    const/4 v5, 0x1

    # invokes: Lcom/google/uploader/client/ResumableTransfer;->resumeExistingUpload(Z)Lcom/google/uploader/client/HttpResponse;
    invoke-static {v4, v5}, Lcom/google/uploader/client/ResumableTransfer;->access$200(Lcom/google/uploader/client/ResumableTransfer;Z)Lcom/google/uploader/client/HttpResponse;
    :try_end_2
    .catch Lcom/google/uploader/client/TransferException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Lcom/google/uploader/client/TransferException;
    new-instance v2, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    invoke-direct {v2, v0}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;-><init>(Lcom/google/uploader/client/TransferException;)V

    .line 288
    .restart local v2    # "result":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    goto :goto_1

    .line 285
    .end local v0    # "e":Lcom/google/uploader/client/TransferException;
    .end local v2    # "result":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    :catch_1
    move-exception v3

    .line 286
    .local v3, "t":Ljava/lang/Throwable;
    new-instance v0, Lcom/google/uploader/client/TransferException;

    sget-object v4, Lcom/google/uploader/client/TransferException$Type;->UNKNOWN:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v0, v4, v3}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    .line 287
    .restart local v0    # "e":Lcom/google/uploader/client/TransferException;
    new-instance v2, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    invoke-direct {v2, v0}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;-><init>(Lcom/google/uploader/client/TransferException;)V

    .restart local v2    # "result":Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
    goto :goto_1

    .line 295
    .end local v0    # "e":Lcom/google/uploader/client/TransferException;
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    # getter for: Lcom/google/uploader/client/ResumableTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;
    invoke-static {v4}, Lcom/google/uploader/client/ResumableTransfer;->access$300(Lcom/google/uploader/client/ResumableTransfer;)Lcom/google/uploader/client/TransferListener;

    move-result-object v4

    iget-object v6, p0, Lcom/google/uploader/client/ResumableTransfer$1;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    invoke-virtual {v2}, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->getTransferException()Lcom/google/uploader/client/TransferException;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lcom/google/uploader/client/TransferListener;->onException(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferException;)V

    goto :goto_2

    .line 299
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/uploader/client/ResumableTransfer$1;->call()Lcom/google/uploader/client/TransferExceptionOrHttpResponse;

    move-result-object v0

    return-object v0
.end method
