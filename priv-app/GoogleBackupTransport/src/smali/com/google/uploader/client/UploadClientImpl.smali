.class public Lcom/google/uploader/client/UploadClientImpl;
.super Ljava/lang/Object;
.source "UploadClientImpl.java"

# interfaces
.implements Lcom/google/uploader/client/UploadClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/UploadClientImpl$1;,
        Lcom/google/uploader/client/UploadClientImpl$Builder;
    }
.end annotation


# instance fields
.field private final httpClient:Lcom/google/uploader/client/HttpClient;


# direct methods
.method private constructor <init>(Lcom/google/uploader/client/HttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lcom/google/uploader/client/HttpClient;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/uploader/client/UploadClientImpl;->httpClient:Lcom/google/uploader/client/HttpClient;

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/uploader/client/HttpClient;Lcom/google/uploader/client/UploadClientImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/uploader/client/HttpClient;
    .param p2, "x1"    # Lcom/google/uploader/client/UploadClientImpl$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/uploader/client/UploadClientImpl;-><init>(Lcom/google/uploader/client/HttpClient;)V

    return-void
.end method

.method public static newBuilder(Lcom/google/uploader/client/HttpClient;)Lcom/google/uploader/client/UploadClientImpl$Builder;
    .locals 2
    .param p0, "httpClient"    # Lcom/google/uploader/client/HttpClient;

    .prologue
    .line 37
    new-instance v0, Lcom/google/uploader/client/UploadClientImpl$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/uploader/client/UploadClientImpl$Builder;-><init>(Lcom/google/uploader/client/HttpClient;Lcom/google/uploader/client/UploadClientImpl$1;)V

    return-object v0
.end method


# virtual methods
.method public createTransfer(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;Ljava/lang/String;Lcom/google/uploader/client/TransferOptions;)Lcom/google/uploader/client/Transfer;
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p4, "body"    # Lcom/google/uploader/client/DataStream;
    .param p5, "metadata"    # Ljava/lang/String;
    .param p6, "options"    # Lcom/google/uploader/client/TransferOptions;

    .prologue
    .line 60
    const-string v0, "put"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "post"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 63
    iget-object v5, p0, Lcom/google/uploader/client/UploadClientImpl;->httpClient:Lcom/google/uploader/client/HttpClient;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/google/uploader/client/ResumableTransfer;->createNewTransfer(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;Ljava/lang/String;Lcom/google/uploader/client/HttpClient;Lcom/google/uploader/client/TransferOptions;)Lcom/google/uploader/client/ResumableTransfer;

    move-result-object v0

    return-object v0

    .line 60
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
