.class Landroid/support/v4/view/MyViewPager$PagerObserver;
.super Landroid/database/DataSetObserver;
.source "MyViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/MyViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PagerObserver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v4/view/MyViewPager;


# direct methods
.method private constructor <init>(Landroid/support/v4/view/MyViewPager;)V
    .locals 0

    .prologue
    .line 2836
    iput-object p1, p0, Landroid/support/v4/view/MyViewPager$PagerObserver;->this$0:Landroid/support/v4/view/MyViewPager;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/view/MyViewPager;Landroid/support/v4/view/MyViewPager$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v4/view/MyViewPager;
    .param p2, "x1"    # Landroid/support/v4/view/MyViewPager$1;

    .prologue
    .line 2836
    invoke-direct {p0, p1}, Landroid/support/v4/view/MyViewPager$PagerObserver;-><init>(Landroid/support/v4/view/MyViewPager;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 2839
    iget-object v0, p0, Landroid/support/v4/view/MyViewPager$PagerObserver;->this$0:Landroid/support/v4/view/MyViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/MyViewPager;->dataSetChanged()V

    .line 2840
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 2843
    iget-object v0, p0, Landroid/support/v4/view/MyViewPager$PagerObserver;->this$0:Landroid/support/v4/view/MyViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/MyViewPager;->dataSetChanged()V

    .line 2844
    return-void
.end method
