.class Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    check-cast p2, Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;->getService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    iput-object v1, v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 182
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSubModeStartFromMain:Z

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMode()I

    .line 185
    :cond_0
    # getter for: Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaseActivity onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onActivityServiceConnected()V

    .line 187
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 190
    # getter for: Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BaseActivity onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    return-void
.end method
