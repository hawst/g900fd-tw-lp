.class public Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;
    }
.end annotation


# static fields
.field public static final MSG_SHOW_DEL_DIALOG:I = 0x5002

.field public static final MSG_SHOW_SAVE_FILES_TOAST:I = 0x5001

.field public static final MSG_SHOW_SAVE_TOAST:I = 0x5000

.field private static final tag:Ljava/lang/String;


# instance fields
.field protected mActivityFinished:Z

.field protected mBitmapAddress:J

.field protected mBitmapCount:I

.field mCallback:Landroid/os/Handler$Callback;

.field private mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

.field private mDelDailogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

.field private mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

.field protected mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

.field protected mFromGallery:Z

.field mHandler:Landroid/os/Handler;

.field private mHomeKeyEventReceiver:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

.field private mHomeKeyListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

.field protected mIsUserEdit:Z

.field protected mMagicServiceIntent:Landroid/content/Intent;

.field protected mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

.field protected mNativeExifJPGBuff:J

.field protected mNativeExifJPGBuffSize:J

.field private mNeedFinishAfterDeleteSrcImage:Z

.field private mNormalFinished:Z

.field private mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field protected mOrientation:I

.field private mPopupToast:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

.field protected mPreDataAddress:J

.field protected mPreDataSize:I

.field private mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;

.field protected mSavePath:Ljava/lang/String;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field protected mSrcImageSize:[I

.field protected mSubModeStartFromMain:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPreDataAddress:J

    .line 50
    iput v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPreDataSize:I

    .line 52
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuff:J

    .line 53
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuffSize:J

    .line 55
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapAddress:J

    .line 56
    iput v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapCount:I

    .line 57
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSavePath:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSrcImageSize:[I

    .line 59
    iput v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mOrientation:I

    .line 61
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mIsUserEdit:Z

    .line 63
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mActivityFinished:Z

    .line 64
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    .line 65
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 66
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSubModeStartFromMain:Z

    .line 67
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mFromGallery:Z

    .line 69
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNormalFinished:Z

    .line 70
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNeedFinishAfterDeleteSrcImage:Z

    .line 72
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 76
    new-instance v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$1;-><init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    .line 178
    new-instance v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$2;-><init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 284
    new-instance v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$3;-><init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mCallback:Landroid/os/Handler$Callback;

    .line 292
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHandler:Landroid/os/Handler;

    .line 458
    new-instance v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;-><init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 559
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    .line 567
    new-instance v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$5;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$5;-><init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 43
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNeedFinishAfterDeleteSrcImage:Z

    return v0
.end method

.method private checkDelSefDailogNeedShow()Z
    .locals 4

    .prologue
    .line 490
    const-string v2, "MAGICSHOT_SPF_Key"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 491
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    const-string v2, "TYPE_DELETE_DAILOG_NEED_SHOW"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 493
    .local v0, "needShow":Z
    return v0
.end method

.method private checkNeedDeleteSrcImage()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 497
    const-string v2, "MAGICSHOT_SPF_Key"

    invoke-virtual {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 498
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    const-string v2, "TYPE_SRC_IMAGE_NEED_DELETE"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 500
    .local v0, "needDelete":Z
    return v0
.end method

.method private getExternalStorageFreeSpace()J
    .locals 2

    .prologue
    .line 601
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0

    return-wide v0
.end method

.method private prepareDelDialog()V
    .locals 2

    .prologue
    .line 553
    const/4 v0, 0x0

    sput v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    .line 554
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 555
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->setHandler(Landroid/os/Handler;)V

    .line 556
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->getHeightForDialog()V

    .line 557
    return-void
.end method

.method private registHomeKeyReceiver()V
    .locals 3

    .prologue
    .line 152
    new-instance v1, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;-><init>()V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyEventReceiver:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    .line 153
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyEventReceiver:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->setHomeKeyListener(Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;)V

    .line 154
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 155
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyEventReceiver:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 156
    return-void
.end method

.method private registPrivateModeReceiver()V
    .locals 3

    .prologue
    .line 159
    new-instance v1, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;-><init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;Lcom/arcsoft/magicshotstudio/activity/BaseActivity$1;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;

    .line 160
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 161
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 162
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 166
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 167
    return-void
.end method

.method private showDelDialog(I)V
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 561
    sput p1, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    .line 562
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 563
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->setCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 564
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->showDialog(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 565
    return-void
.end method


# virtual methods
.method public cancelDelDailog()V
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->dismiss()V

    .line 578
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 580
    :cond_0
    return-void
.end method

.method public deleteSefFile()V
    .locals 2

    .prologue
    .line 504
    const/4 v0, 0x0

    .line 505
    .local v0, "sefPath":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSEFFilePath()Ljava/lang/String;

    move-result-object v0

    .line 509
    :cond_0
    if-nez v0, :cond_1

    .line 514
    :goto_0
    return-void

    .line 513
    :cond_1
    invoke-static {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mActivityFinished:Z

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public exitActivity()V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mFromGallery:Z

    if-eqz v0, :cond_1

    .line 528
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->checkDelSefDailogNeedShow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 530
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    :cond_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->prepareDelDialog()V

    .line 532
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNeedFinishAfterDeleteSrcImage:Z

    goto :goto_0

    .line 534
    :cond_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->checkNeedDeleteSrcImage()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 535
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->deleteSefFile()V

    .line 536
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    goto :goto_0

    .line 538
    :cond_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    goto :goto_0
.end method

.method protected exitLogic()V
    .locals 5

    .prologue
    .line 313
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-nez v2, :cond_0

    .line 337
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getExitMode()Z

    move-result v1

    .line 318
    .local v1, "gotoMain":Z
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mFromGallery:Z

    .line 320
    .local v0, "fromGallery":Z
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "finish gotoMain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "finish fromGallery = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    if-eqz v0, :cond_1

    .line 324
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v3, "finish fromGallery unbindService"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 327
    :cond_1
    if-eqz v1, :cond_2

    .line 328
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMain()V

    .line 329
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v3, "finish gotoMain"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_0

    .line 332
    :cond_2
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v3, "finish releaseAndExitService"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->releaseAll()V

    .line 334
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->exitService()V

    goto :goto_0
.end method

.method public exitService()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 301
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->stopService(Landroid/content/Intent;)Z

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 304
    :cond_0
    return-void
.end method

.method public finish()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 205
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v3, "finish"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "name":Ljava/lang/String;
    const-class v2, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "bestPhotoName":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mActivityFinished:Z

    if-nez v2, :cond_0

    .line 209
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNormalFinished:Z

    .line 210
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 216
    :goto_0
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mActivityFinished:Z

    .line 218
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 219
    return-void

    .line 214
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->exitLogic()V

    goto :goto_0
.end method

.method public getExitDialog()Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getNativeExifJPGBuff()J
    .locals 2

    .prologue
    .line 410
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuff:J

    return-wide v0
.end method

.method public getNativeExifJPGBuffSize()J
    .locals 2

    .prologue
    .line 414
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuffSize:J

    return-wide v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x0

    return-object v0
.end method

.method protected handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const v8, 0x7f060053

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 422
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "Private"

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 423
    .local v1, "privateStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "Studio"

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 425
    .local v2, "studioStr":Ljava/lang/String;
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 449
    :goto_0
    return-void

    .line 427
    :pswitch_0
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isFromPrivateFolder()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 428
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 430
    :cond_0
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 434
    :pswitch_1
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isFromPrivateFolder()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 435
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 437
    :cond_1
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :pswitch_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    if-eqz v3, :cond_2

    .line 442
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->dismiss()V

    .line 443
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 445
    :cond_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 446
    .local v0, "height":I
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->showDelDialog(I)V

    goto :goto_0

    .line 425
    nop

    :pswitch_data_0
    .packed-switch 0x5000
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected initMode()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 361
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-nez v2, :cond_1

    .line 393
    :cond_0
    :goto_0
    return v0

    .line 365
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getPreDataAddress()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPreDataAddress:J

    .line 366
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getPreDataSize()I

    move-result v2

    iput v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPreDataSize:I

    .line 368
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getBitmapAddress()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapAddress:J

    .line 369
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getBitmapCount()I

    move-result v2

    iput v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapCount:I

    .line 370
    sget-object v2, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getIntentInfo mBitmapAddress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapAddress:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mBitmapCount: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getSavePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSavePath:Ljava/lang/String;

    .line 373
    const/4 v2, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSrcImageSize:[I

    .line 374
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getImageSize()[I

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSrcImageSize:[I

    .line 376
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getOrientation()I

    move-result v2

    iput v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mOrientation:I

    .line 378
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getNativeExifJPG()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuff:J

    .line 379
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getNativeExifJPGSize()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuffSize:J

    .line 381
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapAddress:J

    cmp-long v2, v8, v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mBitmapCount:I

    if-lt v2, v6, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSavePath:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSrcImageSize:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSrcImageSize:[I

    aget v2, v2, v1

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSrcImageSize:[I

    aget v2, v2, v6

    if-lez v2, :cond_0

    .line 387
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuff:J

    cmp-long v2, v8, v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNativeExifJPGBuffSize:J

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 388
    :cond_2
    sget-object v1, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v2, "mNativeExifJPGBuff == 0 || mNativeExifJPGBuffSize < 1"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 392
    :cond_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mFromGallery:Z

    move v0, v1

    .line 393
    goto/16 :goto_0
.end method

.method public isUserEdit()Z
    .locals 1

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mIsUserEdit:Z

    return v0
.end method

.method protected onActivityServiceConnected()V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 518
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 519
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x0

    sput v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    .line 521
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->dismiss()V

    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 524
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "arg0"    # Landroid/content/res/Configuration;

    .prologue
    .line 544
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 545
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->dismiss()V

    .line 547
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .line 548
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->prepareDelDialog()V

    .line 550
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 116
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    iput-boolean v8, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mActivityFinished:Z

    .line 118
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getExternalStorageFreeSpace()J

    move-result-wide v4

    const-wide/32 v6, 0x1400000

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 119
    const v3, 0x7f060056

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 120
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    .line 149
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->registHomeKeyReceiver()V

    .line 124
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->registPrivateModeReceiver()V

    .line 126
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 128
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setHandler(Landroid/os/Handler;)V

    .line 129
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 131
    .local v1, "modeName":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    .line 132
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 134
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    const-string v4, "mode_name"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "sub_mode_from_main"

    invoke-virtual {v3, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mSubModeStartFromMain:Z

    .line 137
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v3, v4, v9}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 138
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 140
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 141
    .local v2, "window":Landroid/view/Window;
    if-eqz v2, :cond_1

    .line 142
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 143
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 144
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 145
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 148
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPopupToast:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 241
    sget-object v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v1, "onDestroy function"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyEventReceiver:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mHomeKeyEventReceiver:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/activity/BaseActivity$PrivateModeReceiver;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 250
    :cond_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNormalFinished:Z

    if-eqz v0, :cond_2

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNormalFinished:Z

    .line 264
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 265
    return-void

    .line 261
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->releaseAll()V

    .line 262
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->exitService()V

    goto :goto_0
.end method

.method protected onHomeKeyLongPressed()V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method protected onHomeKeyPressed()V
    .locals 0

    .prologue
    .line 355
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 269
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 270
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 229
    sget-object v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v1, "dds onRestoreInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 231
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 173
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 235
    sget-object v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->tag:Ljava/lang/String;

    const-string v1, "dds onSaveInstanceState"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 237
    return-void
.end method

.method public releaseAll()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->releaseAll()V

    .line 310
    :cond_0
    return-void
.end method

.method public setUserEditFlag()V
    .locals 1

    .prologue
    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mIsUserEdit:Z

    .line 584
    return-void
.end method

.method public showExitDialog()V
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    .line 598
    :cond_0
    return-void
.end method

.method public showToast(I)V
    .locals 2
    .param p1, "resid"    # I

    .prologue
    .line 452
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mPopupToast:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showDefaultPopUpToast(Ljava/lang/String;)V

    .line 453
    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 456
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 457
    return-void
.end method

.method public startMain()V
    .locals 4

    .prologue
    .line 340
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 341
    .local v0, "intent":Landroid/content/Intent;
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mFromGallery:Z

    if-eqz v2, :cond_0

    .line 342
    const-string v2, "PackageName"

    const-string v3, "com.arcsoft.magicshotstudio"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v2, "lauch_mode"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 348
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSEFFilePath()Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "sefPath":Ljava/lang/String;
    const-string v2, "sef_file_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 351
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 352
    return-void

    .line 345
    .end local v1    # "sefPath":Ljava/lang/String;
    :cond_0
    const-string v2, "lauch_mode"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected startMode()I
    .locals 4

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->initMode()I

    move-result v0

    .line 400
    .local v0, "res":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "createdByLockscreen"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 401
    const-string v1, "createdByLockscreen"

    const-string v2, "createdByLockscreen"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 406
    :goto_0
    return v0

    .line 404
    :cond_0
    const-string v1, "createdByLockscreen"

    const-string v2, " no createdByLockscreen"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public superFinish()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNormalFinished:Z

    .line 223
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 224
    return-void
.end method

.method public unbindService()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 282
    return-void
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .locals 2
    .param p1, "conn"    # Landroid/content/ServiceConnection;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->hasBind()Z

    move-result v0

    .line 275
    .local v0, "bind":Z
    if-eqz v0, :cond_0

    .line 276
    invoke-super {p0, p1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 278
    :cond_0
    return-void
.end method
