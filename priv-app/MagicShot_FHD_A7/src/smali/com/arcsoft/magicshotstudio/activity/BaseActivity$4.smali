.class Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 6
    .param p1, "buttonId"    # I

    .prologue
    .line 462
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    # getter for: Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->access$200(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 463
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    # getter for: Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mDelDailog:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->access$200(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    move-result-object v3

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->dismiss()V

    .line 466
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    const-string v4, "MAGICSHOT_SPF_Key"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 467
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 468
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .line 470
    .local v1, "needDelete":Z
    packed-switch p1, :pswitch_data_0

    .line 479
    :goto_0
    :pswitch_0
    const-string v3, "TYPE_SRC_IMAGE_NEED_DELETE"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 480
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 482
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    # getter for: Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->mNeedFinishAfterDeleteSrcImage:Z
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->access$300(Lcom/arcsoft/magicshotstudio/activity/BaseActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 483
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    .line 486
    :cond_1
    return-void

    .line 474
    :pswitch_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity$4;->this$0:Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->deleteSefFile()V

    .line 475
    const/4 v1, 0x1

    goto :goto_0

    .line 470
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
