.class Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "PicMotion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/PicMotion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/PicMotion;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/PicMotion;Lcom/arcsoft/magicshotstudio/PicMotion$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/PicMotion$1;

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;-><init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 282
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$700(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$700(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResultFile()V

    .line 285
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 279
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 296
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 297
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$1100(Lcom/arcsoft/magicshotstudio/PicMotion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 298
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    .line 299
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 279
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 290
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 291
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$1100(Lcom/arcsoft/magicshotstudio/PicMotion;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 292
    return-void
.end method
