.class public Lcom/arcsoft/magicshotstudio/Main;
.super Landroid/app/Activity;
.source "Main.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;
    }
.end annotation


# static fields
.field public static final tag:Ljava/lang/String;


# instance fields
.field private final MAX_PROCESS:I

.field private mActionBarLayout:Landroid/view/View;

.field private mActivityFinished:Z

.field private mBackArrowLayout:Landroid/view/View;

.field private mBackKeyPressed:Z

.field private mBackgroundImageView:Landroid/widget/ImageView;

.field private mBestFaceButton:Landroid/widget/ImageView;

.field private mBestFaceLayout:Landroid/view/View;

.field private mBestFaceText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mBestPhotoButton:Landroid/widget/ImageView;

.field private mBestPhotoLayout:Landroid/view/View;

.field private mBestPhotoText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mButtonClickListener:Landroid/view/View$OnClickListener;

.field private mContentView:Landroid/view/View;

.field private mCurrentClickedLayoutId:I

.field private mDialogBuilder:Landroid/app/AlertDialog$Builder;

.field private mDramaShotButton:Landroid/widget/ImageView;

.field private mDramaShotLayout:Landroid/view/View;

.field private mDramaText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mEraserButton:Landroid/widget/ImageView;

.field private mEraserLayout:Landroid/view/View;

.field private mEraserText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mHandler:Landroid/os/Handler;

.field private mIsFromFrontCamera:Z

.field private mLastTTSFocusId:I

.field private mMagicServiceIntent:Landroid/content/Intent;

.field private mMagicShotIcon:Landroid/widget/ImageView;

.field private mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

.field private mMainListLayout:Landroid/view/View;

.field private mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

.field private mMainTitleText:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mModePressed:Z

.field private mNormalFinished:Z

.field private mNotSefDialog:Landroid/app/Dialog;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPicMotionButton:Landroid/widget/ImageView;

.field private mPicMotionLayout:Landroid/view/View;

.field private mPicMotionText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;

.field private mQuickClickProtect:J

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mWaitDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    .line 87
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mNormalFinished:Z

    .line 93
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mIsFromFrontCamera:Z

    .line 168
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mQuickClickProtect:J

    .line 169
    new-instance v0, Lcom/arcsoft/magicshotstudio/Main$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Main$1;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    .line 214
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z

    .line 215
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I

    .line 216
    new-instance v0, Lcom/arcsoft/magicshotstudio/Main$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Main$2;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 342
    new-instance v0, Lcom/arcsoft/magicshotstudio/Main$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Main$3;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 363
    const/16 v0, 0x64

    iput v0, p0, Lcom/arcsoft/magicshotstudio/Main;->MAX_PROCESS:I

    .line 364
    new-instance v0, Lcom/arcsoft/magicshotstudio/Main$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Main$4;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mHandler:Landroid/os/Handler;

    .line 440
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mActivityFinished:Z

    .line 544
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackKeyPressed:Z

    .line 588
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mLastTTSFocusId:I

    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/Main;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackKeyPressed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/Main;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mQuickClickProtect:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/Main;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackgroundImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/Main;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mQuickClickProtect:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/arcsoft/magicshotstudio/Main;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->showSEFNotSupportDialog()V

    return-void
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->showNotSEFDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/Main;Ljava/lang/Class;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # Ljava/lang/Class;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Main;->startModeActivity(Ljava/lang/Class;)V

    return-void
.end method

.method static synthetic access$302(Lcom/arcsoft/magicshotstudio/Main;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z

    return p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/Main;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I

    return v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/Main;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I

    return p1
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$602(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object p1
.end method

.method static synthetic access$702(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;)Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    return-object p1
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->setModeEnable()V

    return-void
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/Main;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$902(Lcom/arcsoft/magicshotstudio/Main;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private getAccessibilityFocusedHost()Landroid/view/View;
    .locals 9

    .prologue
    .line 778
    const/4 v4, 0x0

    .line 781
    .local v4, "hostView":Landroid/view/View;
    :try_start_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Main;->mContentView:Landroid/view/View;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getViewRootImpl"

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 783
    .local v3, "getViewRootImpl":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 784
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mContentView:Landroid/view/View;

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v3, v7, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 785
    .local v5, "rootImpl":Ljava/lang/Object;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getAccessibilityFocusedHost"

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 787
    .local v2, "getAccessibilityFocusedHost":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 788
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/view/View;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 792
    .end local v2    # "getAccessibilityFocusedHost":Ljava/lang/reflect/Method;
    .end local v3    # "getViewRootImpl":Ljava/lang/reflect/Method;
    .end local v5    # "rootImpl":Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 789
    :catch_0
    move-exception v1

    .line 790
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getExternalStorageFreeSpace()J
    .locals 2

    .prologue
    .line 813
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0

    return-wide v0
.end method

.method private initLayout(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->isFromFrontCamera()Z

    move-result v0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mIsFromFrontCamera:Z

    .line 97
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mIsFromFrontCamera:Z

    if-eqz v0, :cond_1

    .line 98
    const v0, 0x7f03001a

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->setContentView(I)V

    .line 102
    :goto_0
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoButton:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f0900b4

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceButton:Landroid/widget/ImageView;

    .line 104
    const v0, 0x7f0900bd

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserButton:Landroid/widget/ImageView;

    .line 105
    const v0, 0x7f0900ba

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotButton:Landroid/widget/ImageView;

    .line 106
    const v0, 0x7f0900c0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionButton:Landroid/widget/ImageView;

    .line 108
    const v0, 0x7f0900a7

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    .line 109
    const v0, 0x7f0900a8

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    .line 110
    const v0, 0x7f0900a9

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    .line 111
    const v0, 0x7f0900ab

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    .line 112
    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotLayout:Landroid/view/View;

    .line 113
    const v0, 0x7f0900ac

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionLayout:Landroid/view/View;

    .line 115
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 122
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 124
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 125
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 127
    const v0, 0x7f0900a5

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackgroundImageView:Landroid/widget/ImageView;

    .line 128
    const v0, 0x7f0900ae

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mActionBarLayout:Landroid/view/View;

    .line 129
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mActionBarLayout:Landroid/view/View;

    const v1, 0x7f0900b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackArrowLayout:Landroid/view/View;

    .line 130
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackArrowLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackArrowLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackArrowLayout:Landroid/view/View;

    invoke-static {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 134
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    const v1, 0x7f0900b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 135
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    const v1, 0x7f0900b6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 136
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    const v1, 0x7f0900bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 137
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotLayout:Landroid/view/View;

    const v1, 0x7f0900bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 138
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionLayout:Landroid/view/View;

    const v1, 0x7f0900c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 139
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mActionBarLayout:Landroid/view/View;

    const v1, 0x7f0900b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainTitleText:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 141
    const v0, 0x7f0900a6

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotIcon:Landroid/widget/ImageView;

    .line 142
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 143
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateBottomLayout(I)V

    .line 145
    :cond_0
    return-void

    .line 100
    :cond_1
    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->setContentView(I)V

    goto/16 :goto_0
.end method

.method private initNotSEFDialog()V
    .locals 4

    .prologue
    .line 717
    new-instance v0, Lcom/arcsoft/magicshotstudio/Main$5;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Main$5;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 727
    .local v0, "mOnClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lcom/arcsoft/magicshotstudio/Main$6;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/Main$6;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 736
    .local v1, "onCancelListener":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 737
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const/high16 v3, 0x7f060000

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 738
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v3, 0x7f06002f

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 739
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v3, 0x7f060014

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 740
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 741
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mNotSefDialog:Landroid/app/Dialog;

    .line 742
    return-void
.end method

.method private isFromFrontCamera()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 149
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return v3

    .line 152
    :cond_1
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 153
    .local v1, "opts":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 154
    const-string v5, "sef_file_name"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 155
    .local v2, "sefPath":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 158
    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 159
    sget-object v5, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FRONT_CAMERA_PIC_SIZE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/arcsoft/magicshotstudio/Main;->FRONT_CAMERA_PIC_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v7, v7, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/arcsoft/magicshotstudio/Main;->FRONT_CAMERA_PIC_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v7, v7, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget v5, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    sget-object v6, Lcom/arcsoft/magicshotstudio/Main;->FRONT_CAMERA_PIC_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v6, v6, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    if-ne v5, v6, :cond_0

    iget v5, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    sget-object v6, Lcom/arcsoft/magicshotstudio/Main;->FRONT_CAMERA_PIC_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v6, v6, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    if-ne v5, v6, :cond_0

    move v3, v4

    .line 163
    goto :goto_0
.end method

.method private isTalkBackOn()Z
    .locals 1

    .prologue
    .line 809
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->isTalkBackOn()Z

    move-result v0

    return v0
.end method

.method private registPrivateModeReceiver()V
    .locals 3

    .prologue
    .line 288
    new-instance v1, Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;-><init>(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/magicshotstudio/Main$1;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;

    .line 289
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 290
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 291
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 292
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 293
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 295
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/Main;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 296
    return-void
.end method

.method private setModeEnable()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestPhotoEnable:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 484
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestPhotoEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 485
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestPhotoEnable:Z

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setEnabled(Z)V

    .line 486
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestFaceEnable:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 487
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestFaceEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 488
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestFaceEnable:Z

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setEnabled(Z)V

    .line 489
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mEraserEnable:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 490
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mEraserEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 491
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mEraserEnable:Z

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setEnabled(Z)V

    .line 492
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mDramaEnable:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 493
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mDramaEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 494
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mDramaEnable:Z

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setEnabled(Z)V

    .line 496
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mMotionEnable:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 497
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mMotionEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 498
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iget-boolean v1, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mMotionEnable:Z

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setEnabled(Z)V

    .line 500
    :cond_0
    return-void
.end method

.method private showNotSEFDialog()V
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mNotSefDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mNotSefDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 748
    :cond_0
    return-void
.end method

.method private showSEFNotSupportDialog()V
    .locals 5

    .prologue
    .line 751
    new-instance v2, Lcom/arcsoft/magicshotstudio/Main$7;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/Main$7;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 760
    .local v2, "mOnClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v3, Lcom/arcsoft/magicshotstudio/Main$8;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/Main$8;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 768
    .local v3, "onCancelListener":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 769
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/high16 v4, 0x7f060000

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 770
    const v4, 0x7f060030

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 771
    const v4, 0x7f060014

    invoke-virtual {v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 772
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 773
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 774
    .local v1, "dialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 775
    return-void
.end method

.method private startDecodeFromGallery(Landroid/content/Intent;)V
    .locals 10
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 526
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 528
    .local v9, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 529
    .local v1, "uri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v0

    .line 531
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 532
    .local v8, "cursor":Landroid/database/Cursor;
    const-string v0, "_data"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 533
    .local v7, "_dataIndex":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 535
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 536
    .local v6, "SEFpath":Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 538
    const-string v0, "sef_file_name"

    invoke-virtual {v9, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 540
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0, v9}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->startDecode(Landroid/content/Intent;)V

    .line 541
    return-void
.end method

.method private startModeActivity(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "ModeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 245
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 246
    const-string v1, "sub_mode_from_main"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 249
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-nez v1, :cond_0

    .line 261
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 254
    const-string v1, "PackageName"

    const-string v2, "com.arcsoft.magicshotstudio"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Main;->unbindService(Landroid/content/ServiceConnection;)V

    .line 258
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->startActivity(Landroid/content/Intent;)V

    .line 259
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->finish()V

    goto :goto_0
.end method

.method private updateBottomLayout(I)V
    .locals 11
    .param p1, "orientation"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 612
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 613
    .local v6, "res":Landroid/content/res/Resources;
    const v7, 0x7f05001c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 614
    .local v3, "mainListWidth_p":I
    const v7, 0x7f05001b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 615
    .local v2, "mainListWidth_l":I
    const/4 v5, 0x0

    .line 616
    .local v5, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 617
    .restart local v5    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mIsFromFrontCamera:Z

    if-eqz v7, :cond_2

    .line 618
    if-ne p1, v9, :cond_1

    .line 619
    iput v3, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 620
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 625
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateFrontModeLayout(Landroid/view/View;I)V

    .line 626
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateFrontModeLayout(Landroid/view/View;I)V

    .line 645
    :goto_1
    return-void

    .line 621
    :cond_1
    if-ne p1, v10, :cond_0

    .line 622
    iput v2, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 623
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 628
    :cond_2
    const v7, 0x7f05001d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 629
    .local v0, "mainBottomPadding":I
    const v7, 0x7f05001e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 630
    .local v1, "mainLeftPadding":I
    const v7, 0x7f05001f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 631
    .local v4, "mainRightPadding":I
    if-ne p1, v9, :cond_4

    .line 632
    iput v3, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 633
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 634
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    invoke-virtual {v7, v1, v8, v4, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 639
    :cond_3
    :goto_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestPhotoLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateModeLayout(Landroid/view/View;I)V

    .line 640
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateModeLayout(Landroid/view/View;I)V

    .line 641
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateModeLayout(Landroid/view/View;I)V

    .line 642
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mDramaShotLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateModeLayout(Landroid/view/View;I)V

    .line 643
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mPicMotionLayout:Landroid/view/View;

    invoke-direct {p0, v7, p1}, Lcom/arcsoft/magicshotstudio/Main;->updateModeLayout(Landroid/view/View;I)V

    goto :goto_1

    .line 635
    :cond_4
    if-ne p1, v10, :cond_3

    .line 636
    iput v2, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 637
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainListLayout:Landroid/view/View;

    invoke-virtual {v7, v8, v8, v8, v0}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_2
.end method

.method private updateFrontModeLayout(Landroid/view/View;I)V
    .locals 8
    .param p1, "layout"    # Landroid/view/View;
    .param p2, "orientation"    # I

    .prologue
    .line 672
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 673
    .local v6, "res":Landroid/content/res/Resources;
    const v7, 0x7f050025

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 674
    .local v1, "mainModeSideMargin_l":I
    const v7, 0x7f050026

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 675
    .local v2, "mainModeSideMargin_p":I
    const v7, 0x7f050023

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 676
    .local v3, "mainModeWidth_l":I
    const v7, 0x7f050024

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 677
    .local v4, "mainModeWidth_p":I
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v0, v7, Landroid/util/DisplayMetrics;->density:F

    .line 678
    .local v0, "density":F
    const/4 v5, 0x0

    .line 679
    .local v5, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 680
    .restart local v5    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v7, 0x1

    if-ne p2, v7, :cond_1

    .line 681
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 682
    iput v2, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 683
    iput v2, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 684
    invoke-virtual {p1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    const/4 v7, 0x2

    if-ne p2, v7, :cond_0

    .line 686
    iput v3, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 687
    iput v1, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 688
    iput v1, v5, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 689
    invoke-virtual {p1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private updateModeLayout(Landroid/view/View;I)V
    .locals 7
    .param p1, "layout"    # Landroid/view/View;
    .param p2, "orientation"    # I

    .prologue
    const/4 v6, 0x0

    .line 648
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050021

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 649
    .local v2, "mainModeWidth_p":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050020

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 650
    .local v1, "mainModeWidth_l":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050022

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 651
    .local v0, "mainModeSideMargin":I
    const/4 v3, 0x0

    .line 652
    .local v3, "param":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v4, 0x1

    if-ne p2, v4, :cond_3

    .line 653
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 654
    .restart local v3    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iput v2, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 655
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    if-eq p1, v4, :cond_0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    if-ne p1, v4, :cond_1

    .line 656
    :cond_0
    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 657
    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 659
    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 669
    :cond_2
    :goto_0
    return-void

    .line 660
    :cond_3
    const/4 v4, 0x2

    if-ne p2, v4, :cond_2

    .line 661
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 662
    .restart local v3    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iput v1, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 663
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mBestFaceLayout:Landroid/view/View;

    if-eq p1, v4, :cond_4

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mEraserLayout:Landroid/view/View;

    if-ne p1, v4, :cond_5

    .line 664
    :cond_4
    iput v0, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 665
    iput v0, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 667
    :cond_5
    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 443
    sget-object v1, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v2, "Main finish in"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mActivityFinished:Z

    if-eqz v1, :cond_0

    .line 455
    :goto_0
    return-void

    .line 447
    :cond_0
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Main;->mNormalFinished:Z

    .line 448
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 449
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 450
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/Main;->setResult(ILandroid/content/Intent;)V

    .line 452
    :cond_1
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Main;->mActivityFinished:Z

    .line 453
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 454
    sget-object v1, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v2, "Main finish out"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 505
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onActivityResult "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    const/16 v0, 0x1000

    if-ne v0, p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 509
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onActivityResult (A)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 512
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onActivityResult (B)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setHandler(Landroid/os/Handler;)V

    .line 515
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onActivityResult (C)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->releaseDecodeData()V

    .line 518
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onActivityResult (D)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    invoke-direct {p0, p3}, Lcom/arcsoft/magicshotstudio/Main;->startDecodeFromGallery(Landroid/content/Intent;)V

    .line 522
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 523
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 568
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onKeyUp"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackKeyPressed:Z

    .line 570
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z

    if-eqz v0, :cond_0

    .line 577
    :goto_0
    return-void

    .line 573
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->releaseAndExitService()V

    .line 576
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 591
    sget-object v1, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v2, "onConfigurationChanged...."

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->isTalkBackOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 593
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->getAccessibilityFocusedHost()Landroid/view/View;

    move-result-object v0

    .line 594
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mLastTTSFocusId:I

    .line 598
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/Main;->updateBottomLayout(I)V

    .line 599
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z

    .line 600
    iput v3, p0, Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I

    .line 601
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->setModeEnable()V

    .line 602
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v1, :cond_1

    .line 603
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;

    .line 604
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 605
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackgroundImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 608
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 609
    return-void
.end method

.method public onContentChanged()V
    .locals 1

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->isTalkBackOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mLastTTSFocusId:I

    if-eqz v0, :cond_0

    .line 697
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mLastTTSFocusId:I

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->requestAccessibilityFocus(Landroid/view/View;)V

    .line 700
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onContentChanged()V

    .line 701
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 300
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "lauch_mode"

    invoke-virtual {v4, v5, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 301
    .local v0, "bFromGallery":Z
    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "PackageName"

    const-string v6, "com.arcsoft.magicshotstudio"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 307
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->getExternalStorageFreeSpace()J

    move-result-wide v4

    const-wide/32 v6, 0x1400000

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 308
    const v4, 0x7f060056

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/Main;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 309
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->finish()V

    .line 340
    :cond_0
    :goto_1
    return-void

    .line 304
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "PackageName"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 312
    :cond_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->registPrivateModeReceiver()V

    .line 313
    const-string v4, "MagicShotApplication"

    const-string v5, "Main onCreate2"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v2, v4, Landroid/content/res/Configuration;->orientation:I

    .line 315
    .local v2, "orientation":I
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/Main;->initLayout(I)V

    .line 316
    new-instance v4, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    invoke-direct {v4}, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;-><init>()V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    .line 317
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v8, v4, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestFaceEnable:Z

    .line 318
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v8, v4, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestPhotoEnable:Z

    .line 319
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v8, v4, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mDramaEnable:Z

    .line 320
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v8, v4, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mEraserEnable:Z

    .line 321
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v8, v4, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mMotionEnable:Z

    .line 322
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->setModeEnable()V

    .line 324
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Main;->initNotSEFDialog()V

    .line 326
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mHandler:Landroid/os/Handler;

    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setHandler(Landroid/os/Handler;)V

    .line 328
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicServiceIntent:Landroid/content/Intent;

    .line 329
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 331
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicServiceIntent:Landroid/content/Intent;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Main;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v4, v5, v9}, Lcom/arcsoft/magicshotstudio/Main;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 332
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/Main;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 333
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 334
    .local v3, "window":Landroid/view/Window;
    if-eqz v3, :cond_0

    .line 335
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 336
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v4, v4, 0x400

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 337
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 338
    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 459
    const-string v0, "MagicShotApplication"

    const-string v1, "Main onDestroy in"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mPrivateModeReceiver:Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 464
    :cond_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mNormalFinished:Z

    if-eqz v0, :cond_1

    .line 468
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mNormalFinished:Z

    .line 477
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 478
    const-string v0, "MagicShotApplication"

    const-string v1, "Main onDestroy out"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    return-void

    .line 475
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Main;->releaseAndExitService()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 547
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 548
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onKeyDown"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mBackKeyPressed:Z

    .line 551
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 557
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 558
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z

    .line 559
    iput v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I

    .line 560
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 430
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "Main onResume"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 432
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 436
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "Main onStop"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 438
    return-void
.end method

.method public releaseAndExitService()V
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->releaseAll()V

    .line 582
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->unbindService(Landroid/content/ServiceConnection;)V

    .line 583
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->stopService(Landroid/content/Intent;)Z

    .line 584
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 586
    :cond_0
    return-void
.end method

.method public requestAccessibilityFocus(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 796
    if-nez p1, :cond_0

    .line 806
    :goto_0
    return-void

    .line 800
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "requestAccessibilityFocus"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 801
    .local v1, "requestAccessibilityFocus":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 802
    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 803
    .end local v1    # "requestAccessibilityFocus":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 804
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public unbindService()V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Main;->unbindService(Landroid/content/ServiceConnection;)V

    .line 714
    return-void
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .locals 4
    .param p1, "conn"    # Landroid/content/ServiceConnection;

    .prologue
    .line 705
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->hasBind()Z

    move-result v0

    .line 706
    .local v0, "bind":Z
    sget-object v1, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unbindService...."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    if-eqz v0, :cond_0

    .line 708
    invoke-super {p0, p1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 710
    :cond_0
    return-void
.end method
