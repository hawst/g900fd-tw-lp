.class Lcom/arcsoft/magicshotstudio/Eraser$6;
.super Ljava/lang/Thread;
.source "Eraser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/Eraser;->saveResult()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 766
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v3

    const-string v4, "saveResult() mSaveThread start"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Eraser;->getSavePath()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3802(Lcom/arcsoft/magicshotstudio/Eraser;Ljava/lang/String;)Ljava/lang/String;

    .line 768
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3900(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->createFile(Ljava/lang/String;)V

    .line 769
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4000(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V

    .line 771
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/Eraser;->access$200(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getMoveObjIndexArray()[I

    move-result-object v2

    .line 772
    .local v2, "moveObjIndexArray":[I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/Eraser;->access$400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4100(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->ArcPiClear_SaveImage(Ljava/lang/String;[I)I

    .line 775
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArcPiClear saveImage is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4200(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4300(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x833

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4400(Lcom/arcsoft/magicshotstudio/Eraser;)I

    move-result v6

    invoke-static {v3, v4, v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->refreshDBRecords(Landroid/content/Context;Ljava/lang/String;II)Z

    .line 778
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mKeepSrcFiles:Z
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4500(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 779
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 780
    .local v0, "ArcPiClear_deleteSourceFile":J
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4600(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteSourceFile(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 781
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArcPiClear_deleteSourceFile is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    const/4 v4, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;
    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4602(Lcom/arcsoft/magicshotstudio/Eraser;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 784
    .end local v0    # "ArcPiClear_deleteSourceFile":J
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$6;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4700(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/util/concurrent/Semaphore;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 785
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v3

    const-string v4, "saveResult() mSaveThread end"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    return-void
.end method
