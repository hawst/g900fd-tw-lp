.class Lcom/arcsoft/magicshotstudio/Main$1;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Main;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 172
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBackKeyPressed:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Main;->access$000(Lcom/arcsoft/magicshotstudio/Main;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 174
    .local v0, "currTime":J
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mQuickClickProtect:J
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Main;->access$100(Lcom/arcsoft/magicshotstudio/Main;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 176
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect return ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .end local v0    # "currTime":J
    :cond_0
    :goto_0
    return-void

    .line 179
    .restart local v0    # "currTime":J
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mQuickClickProtect:J
    invoke-static {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$102(Lcom/arcsoft/magicshotstudio/Main;J)J

    .line 180
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect A ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 184
    :pswitch_1
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect startBestPhotoActivity ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    const-class v3, Lcom/arcsoft/magicshotstudio/BestPhoto;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->startModeActivity(Ljava/lang/Class;)V
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/Main;->access$200(Lcom/arcsoft/magicshotstudio/Main;Ljava/lang/Class;)V

    goto :goto_0

    .line 188
    :pswitch_2
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect startBestFaceActivity ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    const-class v3, Lcom/arcsoft/magicshotstudio/BestFace;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->startModeActivity(Ljava/lang/Class;)V
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/Main;->access$200(Lcom/arcsoft/magicshotstudio/Main;Ljava/lang/Class;)V

    goto :goto_0

    .line 192
    :pswitch_3
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect startEraserActivity ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    const-class v3, Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->startModeActivity(Ljava/lang/Class;)V
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/Main;->access$200(Lcom/arcsoft/magicshotstudio/Main;Ljava/lang/Class;)V

    goto :goto_0

    .line 196
    :pswitch_4
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect startDramaActivity ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    const-class v3, Lcom/arcsoft/magicshotstudio/Drama;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->startModeActivity(Ljava/lang/Class;)V
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/Main;->access$200(Lcom/arcsoft/magicshotstudio/Main;Ljava/lang/Class;)V

    goto :goto_0

    .line 200
    :pswitch_5
    const-string v2, "Main"

    const-string v3, "################## mQuickClickProtect startPicMotionActivity ##########"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    const-class v3, Lcom/arcsoft/magicshotstudio/PicMotion;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->startModeActivity(Ljava/lang/Class;)V
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/Main;->access$200(Lcom/arcsoft/magicshotstudio/Main;Ljava/lang/Class;)V

    goto :goto_0

    .line 204
    :pswitch_6
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Main;->releaseAndExitService()V

    .line 205
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$1;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Main;->finish()V

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x7f0900a8
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
