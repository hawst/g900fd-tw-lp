.class public Lcom/arcsoft/magicshotstudio/BestFace;
.super Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.source "BestFace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/BestFace$1;,
        Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;,
        Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;
    }
.end annotation


# static fields
.field public static final DEFAULT_SAVE_PATH:Ljava/lang/String; = "/mnt/sdcard/DCIM/Camera/BestFace.jpg"

.field private static final HANDLER_FINISH:I = 0x100

.field private static final HANDLER_SDCARD_CHANGED:I = 0x101

.field private static final tag:Ljava/lang/String;


# instance fields
.field private mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

.field private mFirstBitmap:Landroid/graphics/Bitmap;

.field private mFirstImage:Landroid/widget/ImageView;

.field private mIsFirstImageSetDone:Z

.field private mIsForQA:Z

.field private mKeepSrcFiles:Z

.field private mMainRelativeLayout:Landroid/widget/RelativeLayout;

.field private mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

.field private mProgressNum:I

.field mSDCardIntentFilter:Landroid/content/IntentFilter;

.field private mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

.field private mSelectPhotoNum:I

.field private mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;-><init>()V

    .line 42
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 43
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .line 44
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mMainRelativeLayout:Landroid/widget/RelativeLayout;

    .line 47
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsForQA:Z

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mKeepSrcFiles:Z

    .line 52
    iput v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSelectPhotoNum:I

    .line 57
    iput v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressNum:I

    .line 59
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsFirstImageSetDone:Z

    .line 321
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    .line 322
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardIntentFilter:Landroid/content/IntentFilter;

    .line 331
    return-void
.end method

.method private abnormalToastShow(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-static {}, Lcom/arcsoft/magicshotstudio/utils/ToastController;->closeToast()V

    .line 203
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/utils/ToastController;->makeToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 204
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    .line 206
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/BestFace;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestFace;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/BestFace;)Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestFace;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    return-object v0
.end method

.method private deleteOldDumpData()V
    .locals 10

    .prologue
    .line 68
    new-instance v3, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;

    const-string v7, ".nv21"

    invoke-direct {v3, p0, v7}, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;-><init>(Lcom/arcsoft/magicshotstudio/BestFace;Ljava/lang/String;)V

    .line 69
    .local v3, "nv21Filter":Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;
    new-instance v5, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;

    const-string v7, ".RGB16"

    invoke-direct {v5, p0, v7}, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;-><init>(Lcom/arcsoft/magicshotstudio/BestFace;Ljava/lang/String;)V

    .line 70
    .local v5, "rgbFilter":Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 71
    .local v0, "file":Ljava/io/File;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 72
    .local v6, "sdcard":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "nv21FileList":[Ljava/lang/String;
    invoke-virtual {v0, v5}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "rgbFileList":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 75
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, v2

    if-ge v1, v7, :cond_1

    .line 76
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v2, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 77
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v2, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 75
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 82
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    array-length v7, v4

    if-ge v1, v7, :cond_3

    .line 83
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v4, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 84
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v4, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 82
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 88
    .end local v1    # "i":I
    :cond_3
    return-void
.end method

.method private initSDCardActionReceiver()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 328
    new-instance v0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;-><init>(Lcom/arcsoft/magicshotstudio/BestFace;Lcom/arcsoft/magicshotstudio/BestFace$1;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    .line 329
    return-void
.end method

.method private setFirstImage()V
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstImage:Landroid/widget/ImageView;

    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 185
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsFirstImageSetDone:Z

    .line 186
    return-void

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->hideFirstImage()V

    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->hideFirstImage()V

    goto :goto_0
.end method


# virtual methods
.method public cancelDelDailog()V
    .locals 2

    .prologue
    .line 447
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->cancelDelDailog()V

    .line 448
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 449
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 310
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v1, :cond_0

    .line 311
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getProcessFinish()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 312
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 317
    :cond_0
    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->closeToast()V

    .line 299
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getIsActivityFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setIsActivityFinished(Z)V

    .line 304
    :cond_1
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    .line 305
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    const-string v1, "finish function"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFirstImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstBitmap:Landroid/graphics/Bitmap;

    .line 454
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getBestFaceSavePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;

    .line 439
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 440
    const-string v0, "/mnt/sdcard/DCIM/Camera/BestFace.jpg"

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 365
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->handleMessage(Landroid/os/Message;)V

    .line 366
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 368
    :sswitch_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    goto :goto_0

    .line 372
    :sswitch_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x800

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 379
    :sswitch_2
    sget-object v1, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    const-string v2, "Can not get the correct infomation from intent."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    goto :goto_0

    .line 385
    :sswitch_3
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 386
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v2, 0x7f060015

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/BestFace;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 388
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    goto :goto_0

    .line 392
    :sswitch_4
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_0

    .line 393
    iget v1, p1, Landroid/os/Message;->arg1:I

    mul-int/lit8 v1, v1, 0x46

    div-int/lit8 v0, v1, 0x64

    .line 394
    .local v0, "progress":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 395
    iput v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressNum:I

    goto :goto_0

    .line 404
    .end local v0    # "progress":I
    :sswitch_5
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->startMode()I

    goto :goto_0

    .line 408
    :sswitch_6
    const v1, 0x7f06002f

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;II)V

    .line 410
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    goto :goto_0

    .line 366
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_2
        0x11 -> :sswitch_3
        0x12 -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_6
        0x100 -> :sswitch_0
        0x101 -> :sswitch_1
    .end sparse-switch
.end method

.method public hideFirstImage()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 199
    :cond_0
    return-void
.end method

.method public hideProgressDialog()V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 422
    :cond_0
    return-void
.end method

.method protected onActivityServiceConnected()V
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onActivityServiceConnected()V

    .line 190
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsFirstImageSetDone:Z

    if-nez v0, :cond_0

    .line 191
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->setFirstImage()V

    .line 193
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsUserEdit:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->isSaveState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 286
    :cond_2
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 257
    :cond_0
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 258
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->deleteOldDumpData()V

    .line 64
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 262
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    const-string v1, "onDestroy function"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 264
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->unInit()V

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->unInitUI()V

    .line 270
    :cond_1
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onDestroy()V

    .line 271
    return-void
.end method

.method public onHomeKeyPressed()V
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mFromGallery:Z

    if-nez v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getProcessFinish()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onHomeKeyPressed()Z

    .line 434
    :cond_0
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onHomeKeyPressed()V

    .line 435
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 211
    invoke-super {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onPause()V

    .line 217
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onPause()V

    .line 220
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 224
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onResume()V

    .line 225
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onResume()V

    .line 228
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    if-nez v0, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestFace;->initSDCardActionReceiver()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 236
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    const-string v1, "onStart function"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStart()V

    .line 238
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;

    const-string v1, "onStop function"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onStop()V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/BestFace;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 249
    :cond_1
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStop()V

    .line 250
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 275
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onUserLeaveHint()V

    .line 276
    return-void
.end method

.method protected startMode()I
    .locals 20

    .prologue
    .line 112
    invoke-super/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMode()I

    move-result v19

    .line 113
    .local v19, "res":I
    if-eqz v19, :cond_1

    .line 114
    const-string v2, "samsung input photos have some issue"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/BestFace;->abnormalToastShow(Ljava/lang/String;)V

    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    .line 171
    :cond_0
    :goto_0
    return v19

    .line 121
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsForQA:Z

    if-eqz v2, :cond_2

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    if-nez v2, :cond_2

    .line 123
    new-instance v2, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .line 124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v3, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->APP_START_PROCESS_DISMISS:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 129
    :cond_2
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030004

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 130
    .local v5, "contentView":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/arcsoft/magicshotstudio/BestFace;->setContentView(Landroid/view/View;)V

    .line 131
    const v2, 0x7f090023

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mFirstImage:Landroid/widget/ImageView;

    .line 132
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 133
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/BestFace;->setFirstImage()V

    .line 139
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsForQA:Z

    if-eqz v2, :cond_3

    .line 140
    const v2, 0x7f090021

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mMainRelativeLayout:Landroid/widget/RelativeLayout;

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mMainRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->initShowTimeView(Landroid/widget/RelativeLayout;)V

    .line 144
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;

    if-nez v2, :cond_4

    .line 145
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/BestFace;->initSDCardActionReceiver()V

    .line 148
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-nez v2, :cond_0

    .line 149
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-direct {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v2, :cond_0

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsForQA:Z

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setEnablePerformanceShow(Z)V

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mKeepSrcFiles:Z

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setIfKeepSourceFile(Z)V

    .line 153
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsForQA:Z

    if-eqz v2, :cond_5

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setTimeRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord;)V

    .line 156
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v2, :cond_6

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mProgressNum:I

    invoke-virtual {v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setProgressDialog(Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;I)V

    .line 160
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mBitmapCount:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSelectPhotoNum:I

    .line 161
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSelectPhotoNum:I

    const/4 v3, 0x5

    if-lt v2, v3, :cond_7

    .line 162
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSelectPhotoNum:I

    .line 164
    :cond_7
    new-instance v14, Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSrcImageSize:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSrcImageSize:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-direct {v14, v2, v3}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    .line 165
    .local v14, "imgSize":Lcom/arcsoft/magicshotstudio/utils/MSize;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/BestFace;->getIntent()Landroid/content/Intent;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mBitmapAddress:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mPreDataAddress:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mOrientation:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mSelectPhotoNum:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mBitmapCount:I

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mNativeExifJPGBuff:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mNativeExifJPGBuffSize:J

    move-wide/from16 v17, v0

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v18}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->init(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;JJLjava/lang/String;IIILcom/arcsoft/magicshotstudio/utils/MSize;JJ)V

    goto/16 :goto_0

    .line 135
    .end local v14    # "imgSize":Lcom/arcsoft/magicshotstudio/utils/MSize;
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/BestFace;->mIsFirstImageSetDone:Z

    .line 136
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/BestFace;->hideFirstImage()V

    goto/16 :goto_1
.end method
