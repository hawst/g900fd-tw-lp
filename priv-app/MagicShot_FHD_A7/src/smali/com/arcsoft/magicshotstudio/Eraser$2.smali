.class Lcom/arcsoft/magicshotstudio/Eraser$2;
.super Ljava/lang/Thread;
.source "Eraser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/Eraser;->startEngineThread(IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;

.field final synthetic val$markImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field final synthetic val$mode:I

.field final synthetic val$photoNum:I

.field final synthetic val$realImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iput p2, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$photoNum:I

    iput p3, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$mode:I

    iput-object p4, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$realImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iput-object p5, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$markImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 401
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    move-result-object v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$photoNum:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$mode:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$realImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->val$markImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->initPiclearEngine(IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    .line 402
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPreDataAddress:J
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$500(Lcom/arcsoft/magicshotstudio/Eraser;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPreDataSize:I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$600(Lcom/arcsoft/magicshotstudio/Eraser;)I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->setPreprocessorData(JI)I

    .line 403
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mBitmapAddress:J
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$700(Lcom/arcsoft/magicshotstudio/Eraser;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSelectIndexes:[I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$800(Lcom/arcsoft/magicshotstudio/Eraser;)[I

    move-result-object v1

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSelectIndexes:[I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$800(Lcom/arcsoft/magicshotstudio/Eraser;)[I

    move-result-object v4

    array-length v4, v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->set_NV21(J[II)V

    .line 404
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mNativeExifJPGBuff:J
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$900(Lcom/arcsoft/magicshotstudio/Eraser;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mNativeExifJPGBuffSize:J
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1000(Lcom/arcsoft/magicshotstudio/Eraser;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->setExifInfo(JJ)V

    .line 405
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->getProcessResult()V

    .line 406
    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->LOCK_OBJ2:Ljava/lang/Object;

    monitor-enter v1

    .line 407
    :try_start_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$2;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1100(Lcom/arcsoft/magicshotstudio/Eraser;)Landroid/os/Handler;

    move-result-object v0

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 408
    monitor-exit v1

    .line 409
    return-void

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
