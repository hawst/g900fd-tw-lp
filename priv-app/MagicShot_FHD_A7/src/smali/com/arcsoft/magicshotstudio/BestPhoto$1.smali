.class Lcom/arcsoft/magicshotstudio/BestPhoto$1;
.super Ljava/lang/Object;
.source "BestPhoto.java"

# interfaces
.implements Landroid/support/v4/view/MyViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/BestPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/BestPhoto;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 154
    if-nez p1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$000(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->resetOtherPageZoom()V

    .line 157
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 160
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 164
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$100(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setCurrentCursor(I)V

    .line 165
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$100(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->isCurIndexBest()Z

    move-result v0

    .line 166
    .local v0, "isBest":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$200(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 167
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$100(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getCurSelectedIndex()I

    move-result v1

    if-eq p1, v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$200(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->changeSaveBtnState(Z)V

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->setUserEditFlag()V

    .line 171
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$200(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setBestIconSelected(Z)V

    .line 173
    :cond_1
    return-void
.end method
