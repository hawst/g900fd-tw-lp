.class Lcom/arcsoft/magicshotstudio/Main$4;
.super Landroid/os/Handler;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Main;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 367
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage...."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 425
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 370
    :pswitch_1
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "Can not get the correct infomation from intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 375
    :pswitch_2
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "mHandler MESSAGE_START_DECODE"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$1102(Lcom/arcsoft/magicshotstudio/Main;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 378
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    const v2, 0x7f060015

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/Main;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 380
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 381
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 382
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 386
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 392
    :pswitch_4
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "mHandler MESSAGE_DECODE_FIRSTPICTURE_OK"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 394
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$902(Lcom/arcsoft/magicshotstudio/Main;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 396
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$900(Lcom/arcsoft/magicshotstudio/Main;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBackgroundImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1000(Lcom/arcsoft/magicshotstudio/Main;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Main;->access$900(Lcom/arcsoft/magicshotstudio/Main;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 402
    :pswitch_5
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "mHandler MESSAGE_FINISH_DECODE"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getMainModeFlags()Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    move-result-object v1

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$702(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;)Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    .line 406
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->setModeEnable()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$800(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 407
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1100(Lcom/arcsoft/magicshotstudio/Main;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 412
    :pswitch_6
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "MESSAGE_SEF_NOT_SUPPORT...."

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->showSEFNotSupportDialog()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1200(Lcom/arcsoft/magicshotstudio/Main;)V

    goto/16 :goto_0

    .line 416
    :pswitch_7
    sget-object v0, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v1, "MESSAGE_NO_SEF_FILE...."

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->setModeEnable()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$800(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 422
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$4;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->showNotSEFDialog()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1300(Lcom/arcsoft/magicshotstudio/Main;)V

    goto/16 :goto_0

    .line 368
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
