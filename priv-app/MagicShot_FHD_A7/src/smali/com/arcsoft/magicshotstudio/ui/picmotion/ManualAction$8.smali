.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 1403
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 1406
    packed-switch p1, :pswitch_data_0

    .line 1428
    :goto_0
    return-void

    .line 1408
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1409
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 1411
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_0

    .line 1414
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1415
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 1417
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->exitWidthoutSave()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    goto :goto_0

    .line 1420
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1421
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 1423
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResult()V

    .line 1424
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->exitActivity()V

    .line 1425
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 1406
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
