.class Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;
.super Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$WindowRunnnable;
.source "FrameListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForKeyLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V
    .locals 1

    .prologue
    .line 6544
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$WindowRunnnable;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;

    .prologue
    .line 6544
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 6546
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mSelectedPosition:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$2200(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v3

    if-gez v3, :cond_1

    .line 6571
    :cond_0
    :goto_0
    return-void

    .line 6550
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mSelectedPosition:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$2200(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mFirstPosition:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3300(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v4

    sub-int v1, v3, v4

    .line 6551
    .local v1, "index":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 6553
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mDataChanged:Z
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 6554
    const/4 v0, 0x0

    .line 6556
    .local v0, "handled":Z
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 6557
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mSelectedPosition:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$2200(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mSelectedRowId:J
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)J

    move-result-wide v6

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->performLongPress(Landroid/view/View;IJ)Z
    invoke-static {v3, v2, v4, v6, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$4000(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Landroid/view/View;IJ)Z

    move-result v0

    .line 6560
    :cond_2
    if-eqz v0, :cond_0

    .line 6561
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v3, v8}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setPressed(Z)V

    .line 6562
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 6565
    .end local v0    # "handled":Z
    :cond_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForKeyLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v3, v8}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setPressed(Z)V

    .line 6567
    if-eqz v2, :cond_0

    .line 6568
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0
.end method
