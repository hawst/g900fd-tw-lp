.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
.super Landroid/view/View;
.source "ImageZoomView.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;
.implements Ljava/util/Observer;


# instance fields
.field private final mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

.field private mEnableDrawFaceRect:Z

.field private mEnableSetRotate:Z

.field private mFaceRectNinePatch:Landroid/graphics/NinePatch;

.field private mFaceRectsOnBitmap:[Landroid/graphics/RectF;

.field private mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

.field private mFaceRectsOnScreen:[Landroid/graphics/Rect;

.field private mFaceRotateDegree:I

.field private mIsFirstCalc:Z

.field private mIsFirstTime:Z

.field private mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

.field private mLastDirection:I

.field private mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mMinusBitmap:Landroid/graphics/Bitmap;

.field private mMinusPlusRect:[Landroid/graphics/Rect;

.field private mMinus_Plus_UseLeft:[Z

.field private mMinus_Plus_UseTop:[Z

.field private final mPaint:Landroid/graphics/Paint;

.field private mPatchBitmap:Landroid/graphics/Bitmap;

.field private mPlusBitmap:Landroid/graphics/Bitmap;

.field private mPortraitFitInBitmapRect:Landroid/graphics/Rect;

.field private final mRectDst:Landroid/graphics/Rect;

.field private final mRectSrc:Landroid/graphics/Rect;

.field private mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

.field private m_OffsetX:I

.field private m_OffsetY:I

.field private m_ScaleX:F

.field private m_ScaleY:F

.field private mbRemoved:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    .line 45
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    .line 146
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 147
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 148
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mIsFirstTime:Z

    .line 282
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    .line 319
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRotateDegree:I

    .line 320
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 321
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 322
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 323
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    .line 324
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseTop:[Z

    .line 325
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mIsFirstCalc:Z

    .line 327
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableSetRotate:Z

    .line 328
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableDrawFaceRect:Z

    .line 330
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    .line 331
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    .line 332
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    .line 333
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    .line 334
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 336
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetX:I

    .line 337
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetY:I

    .line 338
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleX:F

    .line 339
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleY:F

    .line 341
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    .line 342
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    .line 54
    return-void
.end method

.method private drawMinus_Plus(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "minusPlusRect"    # Landroid/graphics/Rect;
    .param p3, "index"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    array-length v0, v0

    if-ge p3, v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    aget-boolean v0, v0, p3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private faceRectFromPhotoToScreen()V
    .locals 15

    .prologue
    const/high16 v14, 0x3f800000    # 1.0f

    .line 391
    const/4 v5, 0x0

    .line 393
    .local v5, "length":I
    const/4 v11, 0x0

    .line 394
    .local v11, "tempTop":I
    const/4 v9, 0x0

    .line 395
    .local v9, "tempLeft":I
    const/4 v10, 0x0

    .line 396
    .local v10, "tempRight":I
    const/4 v8, 0x0

    .line 398
    .local v8, "tempBottom":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    if-eqz v12, :cond_0

    .line 399
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    array-length v5, v12

    .line 400
    if-gtz v5, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRotateDegree:I

    if-eqz v12, :cond_3

    iget-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableSetRotate:Z

    if-eqz v12, :cond_3

    .line 408
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    if-ge v4, v5, :cond_2

    .line 409
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v11, v12, Landroid/graphics/Rect;->top:I

    .line 410
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v9, v12, Landroid/graphics/Rect;->left:I

    .line 411
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v10, v12, Landroid/graphics/Rect;->right:I

    .line 412
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v8, v12, Landroid/graphics/Rect;->bottom:I

    .line 413
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRotateDegree:I

    sparse-switch v12, :sswitch_data_0

    .line 408
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 415
    :sswitch_0
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v9, v12, Landroid/graphics/Rect;->top:I

    .line 416
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v8

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 417
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v11

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 418
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v10, v12, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 421
    :sswitch_1
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v8

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 422
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v10

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 423
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v9

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 424
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v11

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 427
    :sswitch_2
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v10

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 428
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v11, v12, Landroid/graphics/Rect;->left:I

    .line 429
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v8, v12, Landroid/graphics/Rect;->right:I

    .line 430
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v9

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 437
    :cond_2
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableSetRotate:Z

    .line 444
    .end local v4    # "index":I
    :cond_3
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v12, :cond_0

    .line 448
    const/high16 v7, 0x3f800000    # 1.0f

    .line 449
    .local v7, "ratioW":F
    const/high16 v6, 0x3f800000    # 1.0f

    .line 450
    .local v6, "ratioH":F
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    mul-float v1, v12, v14

    .line 451
    .local v1, "bitmapWidth":F
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float v0, v12, v14

    .line 453
    .local v0, "bitmapHeight":F
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    if-eqz v12, :cond_4

    .line 454
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRotateDegree:I

    sparse-switch v12, :sswitch_data_1

    .line 470
    :cond_4
    :goto_3
    const/4 v4, 0x0

    .restart local v4    # "index":I
    :goto_4
    if-ge v4, v5, :cond_5

    .line 471
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->top:I

    int-to-float v13, v13

    div-float/2addr v13, v6

    iput v13, v12, Landroid/graphics/RectF;->top:F

    .line 472
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->left:I

    int-to-float v13, v13

    div-float/2addr v13, v7

    iput v13, v12, Landroid/graphics/RectF;->left:F

    .line 473
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->right:I

    int-to-float v13, v13

    div-float/2addr v13, v7

    iput v13, v12, Landroid/graphics/RectF;->right:F

    .line 474
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v13, v13

    div-float/2addr v13, v6

    iput v13, v12, Landroid/graphics/RectF;->bottom:F

    .line 470
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 457
    .end local v4    # "index":I
    :sswitch_3
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v7, v12, v1

    .line 458
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v6, v12, v0

    .line 459
    goto :goto_3

    .line 462
    :sswitch_4
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v7, v12, v1

    .line 463
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v6, v12, v0

    .line 464
    goto :goto_3

    .line 483
    .restart local v4    # "index":I
    :cond_5
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v5, :cond_6

    .line 484
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->top:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetY:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleY:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 485
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->left:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetX:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleX:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 486
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->right:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetX:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleX:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 487
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetY:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleY:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    .line 483
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 490
    :cond_6
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v12

    if-nez v12, :cond_0

    .line 491
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    div-int/lit8 v3, v12, 0x2

    .line 492
    .local v3, "half_icon_width":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    div-int/lit8 v2, v12, 0x2

    .line 493
    .local v2, "half_icon_height":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    invoke-direct {p0, v12, v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->updateMinusPlusPosition([Landroid/graphics/Rect;II)V

    .line 494
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    invoke-direct {p0, v12, v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->updateMinusPlusRect([Landroid/graphics/Rect;II)V

    goto/16 :goto_0

    .line 413
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch

    .line 454
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x5a -> :sswitch_3
        0xb4 -> :sswitch_4
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;
    .locals 10
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I
    .param p3, "bitmapWidth"    # I
    .param p4, "bitmapHeight"    # I

    .prologue
    .line 224
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->getRotateScreenAspectQuotient()F

    move-result v0

    .line 225
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v1

    .line 226
    .local v1, "panX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v2

    .line 227
    .local v2, "panY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v7

    int-to-float v8, p1

    mul-float/2addr v7, v8

    int-to-float v8, p3

    div-float v5, v7, v8

    .line 228
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v7

    int-to-float v8, p2

    mul-float/2addr v7, v8

    int-to-float v8, p4

    div-float v6, v7, v8

    .line 231
    .local v6, "zoomY":F
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 232
    .local v4, "rectSrc":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 233
    .local v3, "rectDst":Landroid/graphics/Rect;
    int-to-float v7, p3

    mul-float/2addr v7, v1

    int-to-float v8, p1

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v5

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 234
    int-to-float v7, p4

    mul-float/2addr v7, v2

    int-to-float v8, p2

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v6

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->top:I

    .line 235
    iget v7, v4, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    int-to-float v8, p1

    div-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->right:I

    .line 236
    iget v7, v4, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    int-to-float v8, p2

    div-float/2addr v8, v6

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->bottom:I

    .line 237
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getLeft()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 238
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getTop()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 239
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getBottom()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 240
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getRight()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->bottom:I

    .line 243
    iget v7, v4, Landroid/graphics/Rect;->left:I

    if-gez v7, :cond_0

    .line 244
    iget v7, v3, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->left:I

    neg-int v8, v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 245
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 247
    :cond_0
    iget v7, v4, Landroid/graphics/Rect;->right:I

    if-le v7, p3, :cond_1

    .line 248
    iget v7, v3, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, p3

    int-to-float v8, v8

    mul-float/2addr v8, v5

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 249
    iput p3, v4, Landroid/graphics/Rect;->right:I

    .line 251
    :cond_1
    iget v7, v4, Landroid/graphics/Rect;->top:I

    if-gez v7, :cond_2

    .line 252
    iget v7, v3, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->top:I

    neg-int v8, v8

    int-to-float v8, v8

    mul-float/2addr v8, v6

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 253
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/Rect;->top:I

    .line 255
    :cond_2
    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    if-le v7, p4, :cond_3

    .line 256
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, p4

    int-to-float v8, v8

    mul-float/2addr v8, v6

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->bottom:I

    .line 257
    iput p4, v4, Landroid/graphics/Rect;->bottom:I

    .line 260
    :cond_3
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v7
.end method

.method private releaseOldEdgeEffect(I)V
    .locals 3
    .param p1, "currentDirection"    # I

    .prologue
    .line 284
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    xor-int v0, p1, v2

    .line 285
    .local v0, "changed":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    and-int v1, v0, v2

    .line 286
    .local v1, "needRelease":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->onRelease(I)V

    .line 287
    return-void
.end method

.method private showBounceView(I)V
    .locals 3
    .param p1, "currentDirection"    # I

    .prologue
    const v2, 0x3ca3d70a    # 0.02f

    .line 290
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_4

    .line 291
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->onPull(FI)V

    .line 296
    :cond_0
    :goto_0
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    .line 297
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->onPull(FI)V

    .line 302
    :cond_1
    :goto_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_6

    .line 303
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->onPull(FI)V

    .line 308
    :cond_2
    :goto_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_7

    .line 309
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->onPull(FI)V

    .line 313
    :cond_3
    :goto_3
    return-void

    .line 293
    :cond_4
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 299
    :cond_5
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    goto :goto_1

    .line 305
    :cond_6
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    goto :goto_2

    .line 311
    :cond_7
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_3

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    goto :goto_3
.end method

.method private updateDrawRect()V
    .locals 15

    .prologue
    .line 158
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    if-eqz v11, :cond_4

    .line 159
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 161
    .local v0, "aspectQuotient":F
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getWidth()I

    move-result v8

    .line 162
    .local v8, "viewWidth":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getHeight()I

    move-result v7

    .line 163
    .local v7, "viewHeight":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 164
    .local v2, "bitmapWidth":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 166
    .local v1, "bitmapHeight":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v3

    .line 167
    .local v3, "panX":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v4

    .line 168
    .local v4, "panY":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v11, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v11

    int-to-float v12, v8

    mul-float/2addr v11, v12

    int-to-float v12, v2

    div-float v9, v11, v12

    .line 169
    .local v9, "zoomX":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v11, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v11

    int-to-float v12, v7

    mul-float/2addr v11, v12

    int-to-float v12, v1

    div-float v10, v11, v12

    .line 172
    .local v10, "zoomY":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v12, v2

    mul-float/2addr v12, v3

    int-to-float v13, v8

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v9

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 173
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v12, v1

    mul-float/2addr v12, v4

    int-to-float v13, v7

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v10

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 174
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    int-to-float v13, v8

    div-float/2addr v13, v9

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 175
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    int-to-float v13, v7

    div-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 176
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getLeft()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 177
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getTop()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 178
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getRight()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 179
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getBottom()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 182
    int-to-float v11, v2

    mul-float/2addr v11, v3

    int-to-float v12, v8

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v9

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetX:I

    .line 183
    int-to-float v11, v1

    mul-float/2addr v11, v4

    int-to-float v12, v7

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v10

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_OffsetY:I

    .line 185
    int-to-float v11, v8

    div-float/2addr v11, v9

    float-to-int v6, v11

    .line 186
    .local v6, "srcWidth":I
    int-to-float v11, v7

    div-float/2addr v11, v10

    float-to-int v5, v11

    .line 189
    .local v5, "srcHeight":I
    int-to-float v11, v6

    int-to-float v12, v8

    div-float/2addr v11, v12

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleX:F

    .line 190
    int-to-float v11, v5

    int-to-float v12, v7

    div-float/2addr v11, v12

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->m_ScaleY:F

    .line 193
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    if-gez v11, :cond_0

    .line 194
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    neg-int v13, v13

    int-to-float v13, v13

    mul-float/2addr v13, v9

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 195
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    const/4 v12, 0x0

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 197
    :cond_0
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    if-le v11, v2, :cond_1

    .line 198
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->right:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    sub-int/2addr v13, v2

    int-to-float v13, v13

    mul-float/2addr v13, v9

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 199
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 201
    :cond_1
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    if-gez v11, :cond_2

    .line 202
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    neg-int v13, v13

    int-to-float v13, v13

    mul-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 203
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    const/4 v12, 0x0

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 205
    :cond_2
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    if-le v11, v1, :cond_3

    .line 206
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->bottom:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v13, v1

    int-to-float v13, v13

    mul-float/2addr v13, v10

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 207
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iput v1, v11, Landroid/graphics/Rect;->bottom:I

    .line 209
    :cond_3
    iget-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mIsFirstTime:Z

    if-eqz v11, :cond_4

    .line 210
    if-ge v8, v7, :cond_5

    .line 211
    new-instance v11, Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 212
    invoke-direct {p0, v7, v8, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;

    move-result-object v11

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 217
    :goto_0
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mIsFirstTime:Z

    .line 220
    .end local v0    # "aspectQuotient":F
    .end local v1    # "bitmapHeight":I
    .end local v2    # "bitmapWidth":I
    .end local v3    # "panX":F
    .end local v4    # "panY":F
    .end local v5    # "srcHeight":I
    .end local v6    # "srcWidth":I
    .end local v7    # "viewHeight":I
    .end local v8    # "viewWidth":I
    .end local v9    # "zoomX":F
    .end local v10    # "zoomY":F
    :cond_4
    return-void

    .line 214
    .restart local v0    # "aspectQuotient":F
    .restart local v1    # "bitmapHeight":I
    .restart local v2    # "bitmapWidth":I
    .restart local v3    # "panX":F
    .restart local v4    # "panY":F
    .restart local v5    # "srcHeight":I
    .restart local v6    # "srcWidth":I
    .restart local v7    # "viewHeight":I
    .restart local v8    # "viewWidth":I
    .restart local v9    # "zoomX":F
    .restart local v10    # "zoomY":F
    :cond_5
    new-instance v11, Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 215
    invoke-direct {p0, v7, v8, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;

    move-result-object v11

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private updateMinusPlusPosition([Landroid/graphics/Rect;II)V
    .locals 4
    .param p1, "referenceRect"    # [Landroid/graphics/Rect;
    .param p2, "half_icon_width"    # I
    .param p3, "half_icon_height"    # I

    .prologue
    const/4 v3, 0x0

    .line 537
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mIsFirstCalc:Z

    if-eqz v1, :cond_3

    .line 538
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    array-length v1, v1

    array-length v2, p1

    if-lt v1, v2, :cond_2

    .line 539
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 540
    aget-object v1, p1, v0

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, p2

    div-int/lit8 v1, v1, 0x2

    if-gez v1, :cond_0

    .line 541
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    aput-boolean v3, v1, v0

    .line 543
    :cond_0
    aget-object v1, p1, v0

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p3

    div-int/lit8 v1, v1, 0x2

    if-gez v1, :cond_1

    .line 544
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseTop:[Z

    aput-boolean v3, v1, v0

    .line 539
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 549
    .end local v0    # "i":I
    :cond_2
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mIsFirstCalc:Z

    .line 551
    :cond_3
    return-void
.end method

.method private updateMinusPlusRect([Landroid/graphics/Rect;II)V
    .locals 3
    .param p1, "referenceRect"    # [Landroid/graphics/Rect;
    .param p2, "half_icon_width"    # I
    .param p3, "half_icon_height"    # I

    .prologue
    .line 502
    if-eqz p1, :cond_3

    array-length v1, p1

    if-lez v1, :cond_3

    .line 503
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 504
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 505
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 507
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 508
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 515
    :goto_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseTop:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_2

    .line 516
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 517
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 503
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 511
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 512
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 520
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 521
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 534
    .end local v0    # "i":I
    :cond_3
    return-void
.end method


# virtual methods
.method public getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    return-object v0
.end method

.method public getLandscapeFitInBitmapRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPortraitFitInBitmapRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRectChoosed(FF)I
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 565
    const/4 v1, -0x1

    .line 566
    .local v1, "index":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    if-eqz v2, :cond_0

    .line 567
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 568
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 570
    move v1, v0

    .line 571
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 572
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    aput-boolean v2, v5, v0

    .line 578
    .end local v0    # "i":I
    :cond_0
    if-gez v1, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_1

    .line 579
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 580
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 581
    move v1, v0

    .line 582
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 583
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    aget-boolean v5, v5, v0

    if-nez v5, :cond_4

    :goto_3
    aput-boolean v3, v2, v0

    .line 589
    .end local v0    # "i":I
    :cond_1
    return v1

    .restart local v0    # "i":I
    :cond_2
    move v2, v4

    .line 572
    goto :goto_1

    .line 567
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v3, v4

    .line 583
    goto :goto_3

    .line 579
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public initRect(Landroid/content/Context;III)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rect_ResID"    # I
    .param p3, "minus_ResID"    # I
    .param p4, "plus_ResID"    # I

    .prologue
    .line 372
    invoke-static {p1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    .line 373
    new-instance v0, Landroid/graphics/NinePatch;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    .line 376
    invoke-static {p1, p3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    .line 377
    invoke-static {p1, p4}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    .line 378
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 105
    const/high16 v2, -0x1000000

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 106
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    if-eqz v2, :cond_0

    .line 107
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->updateDrawRect()V

    .line 108
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 110
    :cond_0
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableDrawFaceRect:Z

    if-eqz v2, :cond_3

    .line 111
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->faceRectFromPhotoToScreen()V

    .line 112
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    if-eqz v2, :cond_3

    .line 113
    const/4 v1, 0x0

    .line 114
    .local v1, "length":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    if-eqz v2, :cond_3

    .line 115
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    array-length v1, v2

    .line 116
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 117
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_1

    .line 118
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    invoke-virtual {v2, p1, v3}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    invoke-direct {p0, p1, v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->drawMinus_Plus(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    .line 116
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_3
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 265
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 266
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    sub-int v1, p4, p2

    int-to-float v1, v1

    sub-int v2, p5, p3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->updateAspectQuotient(FFFF)V

    .line 268
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->notifyObservers()V

    .line 269
    return-void
.end method

.method public setDrawFaceRect(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 560
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableDrawFaceRect:Z

    .line 561
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->invalidate()V

    .line 562
    return-void
.end method

.method public setEdgeBound()V
    .locals 5

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->updateDrawRect()V

    .line 143
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->setEdgeBound(IIII)V

    .line 144
    return-void
.end method

.method public setEdgeView(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;)V
    .locals 0
    .param p1, "edgeView"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    .line 58
    return-void
.end method

.method public setFaceRect([Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "rects"    # [Landroid/graphics/Rect;

    .prologue
    const/4 v4, 0x1

    .line 345
    if-eqz p1, :cond_1

    .line 346
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 347
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    array-length v1, v2

    .line 348
    .local v1, "length":I
    new-array v2, v1, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 349
    new-array v2, v1, [Landroid/graphics/RectF;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 350
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    .line 351
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseTop:[Z

    .line 352
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    .line 353
    new-array v2, v1, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    .line 354
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 355
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, v2, v0

    .line 356
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    aput-object v3, v2, v0

    .line 357
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mbRemoved:[Z

    const/4 v3, 0x0

    aput-boolean v3, v2, v0

    .line 358
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, v2, v0

    .line 359
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    aput-boolean v4, v2, v0

    .line 360
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinus_Plus_UseTop:[Z

    aput-boolean v4, v2, v0

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 363
    :cond_0
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mEnableSetRotate:Z

    .line 365
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_1
    return-void
.end method

.method public setFaceRectRotateDegree(I)V
    .locals 0
    .param p1, "degree"    # I

    .prologue
    .line 368
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mFaceRotateDegree:I

    .line 369
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    .line 68
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->updateAspectQuotient(FFFF)V

    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->notifyObservers()V

    .line 72
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->invalidate()V

    .line 73
    return-void
.end method

.method public setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 0
    .param p1, "size"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 554
    if-eqz p1, :cond_0

    .line 555
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 557
    :cond_0
    return-void
.end method

.method public setZoomState(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;)V
    .locals 1
    .param p1, "state"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 85
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    .line 86
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->addObserver(Ljava/util/Observer;)V

    .line 88
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->invalidate()V

    .line 89
    return-void
.end method

.method public unInit()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 385
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 386
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 387
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 273
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getEffectDirection()I

    move-result v0

    .line 274
    .local v0, "direction":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->setEdgeBound()V

    .line 275
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->releaseOldEdgeEffect(I)V

    .line 276
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->showBounceView(I)V

    .line 277
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->mLastDirection:I

    .line 278
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->invalidate()V

    .line 279
    return-void
.end method
