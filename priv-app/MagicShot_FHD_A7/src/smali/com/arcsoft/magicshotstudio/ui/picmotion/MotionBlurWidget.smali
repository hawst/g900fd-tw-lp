.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;
.super Landroid/view/View;
.source "MotionBlurWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;
    }
.end annotation


# instance fields
.field private final DIRECTION_CLOCK_WISE:I

.field private final DIRECTION_COUNTER_CLOCK_WISE:I

.field private final INITIAL_POSITION_BOTTOM:I

.field private final INITIAL_POSITION_LEFT:I

.field private final INITIAL_POSITION_RIGHT:I

.field private final INITIAL_POSITION_TOP:I

.field private final TEST_CODE:Z

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mCircleCenterX:I

.field private mCircleCenterY:I

.field private mCircleRadius:I

.field private mCursorBound:Landroid/graphics/Rect;

.field private mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

.field private mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

.field private final mCursorOffset:I

.field private mCursorPressed:Z

.field private mCursorSize:I

.field private final mDirection:I

.field private final mInitialPosition:I

.field private mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

.field private mPaint:Landroid/graphics/Paint;

.field private mUserDegree:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    .line 23
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    .line 24
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    .line 26
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->INITIAL_POSITION_TOP:I

    .line 27
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->INITIAL_POSITION_LEFT:I

    .line 28
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->INITIAL_POSITION_RIGHT:I

    .line 29
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->INITIAL_POSITION_BOTTOM:I

    .line 31
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->DIRECTION_COUNTER_CLOCK_WISE:I

    .line 32
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->DIRECTION_CLOCK_WISE:I

    .line 34
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorPressed:Z

    .line 35
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    .line 36
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    .line 37
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 42
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    .line 44
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    .line 46
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->TEST_CODE:Z

    .line 47
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mPaint:Landroid/graphics/Paint;

    .line 62
    sget-object v1, Lcom/arcsoft/magicshotstudio/R$styleable;->CircleProgressBar:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 65
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    .line 66
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    .line 67
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 68
    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    .line 69
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mInitialPosition:I

    .line 70
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mDirection:I

    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 74
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->initUI()V

    .line 75
    return-void
.end method

.method private exangeUserDegreeToDrawDegree(I)I
    .locals 3
    .param p1, "userDegree"    # I

    .prologue
    .line 205
    move v1, p1

    .line 206
    .local v1, "res":I
    const/4 v0, 0x0

    .line 208
    .local v0, "exchange":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mDirection:I

    packed-switch v2, :pswitch_data_0

    .line 216
    :goto_0
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mInitialPosition:I

    packed-switch v2, :pswitch_data_1

    .line 230
    :goto_1
    :pswitch_0
    add-int/2addr v1, v0

    .line 232
    add-int/lit16 v2, v1, 0x2d0

    rem-int/lit16 v2, v2, 0x168

    return v2

    .line 210
    :pswitch_1
    neg-int v1, v1

    .line 211
    goto :goto_0

    .line 220
    :pswitch_2
    const/16 v0, 0x10e

    .line 221
    goto :goto_1

    .line 223
    :pswitch_3
    const/16 v0, 0x5a

    .line 224
    goto :goto_1

    .line 226
    :pswitch_4
    const/16 v0, 0xb4

    goto :goto_1

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    .line 216
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private initCursorPosition()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 115
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    .line 116
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mInitialPosition:I

    packed-switch v0, :pswitch_data_0

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 136
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 137
    return-void

    .line 118
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 119
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 122
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 123
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 126
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 127
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 130
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 131
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initUI()V
    .locals 6

    .prologue
    .line 97
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    .line 98
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    .line 99
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 103
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    .line 105
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    .line 106
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->initCursorPosition()V

    .line 108
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 112
    :cond_1
    return-void
.end method

.method private updateCursorPosition(Landroid/graphics/PointF;)V
    .locals 3
    .param p1, "rect"    # Landroid/graphics/PointF;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/PointF;->y:F

    float-to-int v1, v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 197
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 198
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 200
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 201
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 202
    return-void
.end method

.method private updateCursorPositionAndUserDegree(FF)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const-wide v10, 0x4066800000000000L    # 180.0

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    .line 161
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 163
    .local v2, "rectCursor":Landroid/graphics/PointF;
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    int-to-float v5, v5

    sub-float/2addr v5, p1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 164
    .local v3, "tempX":F
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    sub-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 165
    .local v4, "tempY":F
    mul-float v5, v3, v3

    mul-float v6, v4, v4

    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 167
    .local v0, "distance":F
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-float v5, v5

    mul-float/2addr v5, v3

    div-float v3, v5, v0

    .line 168
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-float v5, v5

    mul-float/2addr v5, v4

    div-float v4, v5, v0

    .line 170
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    int-to-float v5, v5

    cmpl-float v5, p1, v5

    if-lez v5, :cond_1

    .line 171
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    int-to-float v5, v5

    add-float/2addr v5, v3

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 172
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_0

    .line 173
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    sub-float/2addr v5, v4

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 174
    div-float v5, v3, v4

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    mul-double/2addr v6, v10

    div-double/2addr v6, v8

    double-to-int v1, v6

    .line 190
    .local v1, "drawDegree":I
    :goto_0
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->updateCursorPosition(Landroid/graphics/PointF;)V

    .line 191
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->updateUserDegreeFromDrawDegree(I)V

    .line 192
    return-void

    .line 176
    .end local v1    # "drawDegree":I
    :cond_0
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    add-float/2addr v5, v4

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 177
    div-float v5, v4, v3

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    mul-double/2addr v6, v10

    div-double/2addr v6, v8

    double-to-int v5, v6

    add-int/lit8 v1, v5, 0x5a

    .restart local v1    # "drawDegree":I
    goto :goto_0

    .line 180
    .end local v1    # "drawDegree":I
    :cond_1
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterX:I

    int-to-float v5, v5

    sub-float/2addr v5, v3

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 181
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    cmpg-float v5, p2, v5

    if-gez v5, :cond_2

    .line 182
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    sub-float/2addr v5, v4

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 183
    div-float v5, v4, v3

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    mul-double/2addr v6, v10

    div-double/2addr v6, v8

    double-to-int v5, v6

    add-int/lit16 v1, v5, 0x10e

    .restart local v1    # "drawDegree":I
    goto :goto_0

    .line 185
    .end local v1    # "drawDegree":I
    :cond_2
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleCenterY:I

    int-to-float v5, v5

    add-float/2addr v5, v4

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 186
    div-float v5, v3, v4

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    mul-double/2addr v6, v10

    div-double/2addr v6, v8

    double-to-int v5, v6

    add-int/lit16 v1, v5, 0xb4

    .restart local v1    # "drawDegree":I
    goto :goto_0
.end method

.method private updateUserDegreeFromDrawDegree(I)V
    .locals 3
    .param p1, "drawDegree"    # I

    .prologue
    .line 236
    move v1, p1

    .line 237
    .local v1, "res":I
    const/4 v0, 0x0

    .line 238
    .local v0, "exchange":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mInitialPosition:I

    packed-switch v2, :pswitch_data_0

    .line 252
    :goto_0
    :pswitch_0
    add-int/2addr v1, v0

    .line 254
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mDirection:I

    packed-switch v2, :pswitch_data_1

    .line 262
    :goto_1
    add-int/lit16 v2, v1, 0x2d0

    rem-int/lit16 v2, v2, 0x168

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    .line 263
    return-void

    .line 242
    :pswitch_1
    const/16 v0, 0x5a

    .line 243
    goto :goto_0

    .line 245
    :pswitch_2
    const/16 v0, 0x10e

    .line 246
    goto :goto_0

    .line 248
    :pswitch_3
    const/16 v0, 0xb4

    goto :goto_0

    .line 256
    :pswitch_4
    neg-int v1, v1

    .line 257
    goto :goto_1

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 254
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getCurDegree()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 145
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 146
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorPressed:Z

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 156
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v6, -0x80000000

    .line 80
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 81
    .local v2, "widthMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 82
    .local v0, "heightMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 83
    .local v3, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 85
    .local v1, "heightSize":I
    if-ne v2, v6, :cond_0

    .line 86
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    mul-int/lit8 v5, v5, 0x2

    add-int v3, v4, v5

    .line 89
    :cond_0
    if-ne v0, v6, :cond_1

    .line 90
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorOffset:I

    mul-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    .line 93
    :cond_1
    invoke-virtual {p0, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->setMeasuredDimension(II)V

    .line 94
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 270
    const-string v3, "TouchEvent"

    const-string v4, "OnTouchEvent"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 272
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 273
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 274
    .local v2, "y":F
    packed-switch v0, :pswitch_data_0

    .line 301
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->invalidate()V

    .line 302
    return v6

    .line 276
    :pswitch_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    float-to-int v4, v1

    float-to-int v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 277
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorPressed:Z

    goto :goto_0

    .line 281
    :pswitch_1
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorPressed:Z

    .line 282
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    if-eqz v3, :cond_0

    .line 283
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    invoke-interface {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;->doMotionBlur(I)V

    goto :goto_0

    .line 287
    :pswitch_2
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorPressed:Z

    .line 288
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    if-eqz v3, :cond_0

    .line 289
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    invoke-interface {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;->doMotionBlur(I)V

    goto :goto_0

    .line 293
    :pswitch_3
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorPressed:Z

    if-eqz v3, :cond_0

    .line 294
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->updateCursorPositionAndUserDegree(FF)V

    .line 295
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    if-eqz v3, :cond_0

    .line 296
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    invoke-interface {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;->getAngle(I)V

    goto :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public release()V
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    .line 351
    return-void
.end method

.method public setCursorPosition(I)V
    .locals 12
    .param p1, "userDegree"    # I

    .prologue
    const-wide v10, 0x4066800000000000L    # 180.0

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    .line 306
    const/4 v1, 0x0

    .line 307
    .local v1, "tempX":I
    const/4 v4, 0x0

    .line 308
    .local v4, "tempY":I
    const-wide/16 v2, 0x0

    .line 309
    .local v2, "rad":D
    add-int/lit16 v5, p1, 0x168

    rem-int/lit16 v5, v5, 0x168

    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    .line 310
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->exangeUserDegreeToDrawDegree(I)I

    move-result v0

    .line 312
    .local v0, "drawDegree":I
    if-ltz v0, :cond_1

    const/16 v5, 0x5a

    if-gt v0, v5, :cond_1

    .line 313
    int-to-double v6, v0

    mul-double/2addr v6, v8

    div-double v2, v6, v10

    .line 314
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 315
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v4, v6

    .line 316
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    add-int/2addr v6, v1

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 317
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    sub-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->top:I

    .line 338
    :goto_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->right:I

    .line 339
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorSize:I

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    .line 340
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawableNor:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 341
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorDrawablePre:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 343
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    if-eqz v5, :cond_0

    .line 344
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mUserDegree:I

    invoke-interface {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;->getAngle(I)V

    .line 346
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->invalidate()V

    .line 347
    return-void

    .line 318
    :cond_1
    const/16 v5, 0xb4

    if-gt v0, v5, :cond_2

    .line 319
    rsub-int v5, v0, 0xb4

    int-to-double v6, v5

    mul-double/2addr v6, v8

    div-double v2, v6, v10

    .line 320
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 321
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v4, v6

    .line 322
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    add-int/2addr v6, v1

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 323
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    add-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 324
    :cond_2
    const/16 v5, 0x10e

    if-gt v0, v5, :cond_3

    .line 325
    add-int/lit16 v5, v0, -0xb4

    int-to-double v6, v5

    mul-double/2addr v6, v8

    div-double v2, v6, v10

    .line 326
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 327
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v4, v6

    .line 328
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    sub-int/2addr v6, v1

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 329
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    add-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->top:I

    goto/16 :goto_0

    .line 331
    :cond_3
    rsub-int v5, v0, 0x168

    int-to-double v6, v5

    mul-double/2addr v6, v8

    div-double v2, v6, v10

    .line 332
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 333
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    int-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-int v4, v6

    .line 334
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    sub-int/2addr v6, v1

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 335
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCursorBound:Landroid/graphics/Rect;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mCircleRadius:I

    sub-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->top:I

    goto/16 :goto_0
.end method

.method public setListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->mListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;

    .line 141
    return-void
.end method
