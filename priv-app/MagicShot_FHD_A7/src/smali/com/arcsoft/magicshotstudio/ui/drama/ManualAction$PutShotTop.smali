.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PutShotTop"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1734
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1736
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-eqz v0, :cond_0

    .line 1737
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 1738
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1741
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->enableMaskbar(Z)V
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1742
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    const v1, 0x7f020051

    if-ne v0, v1, :cond_1

    .line 1743
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V
    invoke-static {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1748
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2900(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)I

    move-result v1

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->putShotOnTop(I)V
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V

    .line 1749
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 1750
    return-void

    .line 1745
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    goto :goto_0
.end method
