.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;
.super Ljava/lang/Object;
.source "HeadbarMgr.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 374
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 377
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 393
    :goto_0
    return-void

    .line 379
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_0

    .line 382
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->finishActivity()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    goto :goto_0

    .line 385
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->saveFiles()V

    .line 386
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->exitActivity()V

    .line 387
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 377
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
