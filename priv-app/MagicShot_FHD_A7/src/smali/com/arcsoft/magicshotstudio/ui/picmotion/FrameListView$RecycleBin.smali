.class Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;
.super Ljava/lang/Object;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecycleBin"
.end annotation


# instance fields
.field private mActiveViews:[Landroid/view/View;

.field private mCurrentScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstActivePosition:I

.field private mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

.field private mScrapViews:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mViewTypeCount:I

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 1

    .prologue
    .line 5893
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5896
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    return-void
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    .prologue
    .line 5893
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    return-object p1
.end method

.method private pruneScrapViews()V
    .locals 13

    .prologue
    .line 6137
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    array-length v3, v10

    .line 6138
    .local v3, "maxViews":I
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    .line 6139
    .local v9, "viewTypeCount":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 6141
    .local v5, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v9, :cond_1

    .line 6142
    aget-object v4, v5, v1

    .line 6143
    .local v4, "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 6144
    .local v6, "size":I
    sub-int v0, v6, v3

    .line 6146
    .local v0, "extras":I
    add-int/lit8 v6, v6, -0x1

    .line 6148
    const/4 v2, 0x0

    .local v2, "j":I
    move v7, v6

    .end local v6    # "size":I
    .local v7, "size":I
    :goto_1
    if-ge v2, v0, :cond_0

    .line 6149
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "size":I
    .restart local v6    # "size":I
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    const/4 v12, 0x0

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v11, v10, v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1600(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V

    .line 6148
    add-int/lit8 v2, v2, 0x1

    move v7, v6

    .end local v6    # "size":I
    .restart local v7    # "size":I
    goto :goto_1

    .line 6141
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6153
    .end local v0    # "extras":I
    .end local v2    # "j":I
    .end local v4    # "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v7    # "size":I
    :cond_1
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v10, :cond_3

    .line 6154
    const/4 v1, 0x0

    :goto_2
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v10

    if-ge v1, v10, :cond_3

    .line 6155
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 6156
    .local v8, "v":Landroid/view/View;
    invoke-static {v8}, Landroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 6157
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v1}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    .line 6158
    add-int/lit8 v1, v1, -0x1

    .line 6154
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6162
    .end local v8    # "v":Landroid/view/View;
    :cond_3
    return-void
.end method


# virtual methods
.method addScrapView(Landroid/view/View;I)V
    .locals 5
    .param p1, "scrap"    # Landroid/view/View;
    .param p2, "position"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 6043
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .line 6044
    .local v0, "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    if-nez v0, :cond_1

    .line 6081
    :cond_0
    :goto_0
    return-void

    .line 6048
    :cond_1
    iput p2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->scrappedFromPosition:I

    .line 6050
    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->viewType:I

    .line 6051
    .local v2, "viewType":I
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v1

    .line 6054
    .local v1, "scrapHasTransientState":Z
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v1, :cond_4

    .line 6055
    :cond_2
    if-eqz v1, :cond_0

    .line 6056
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v3, :cond_3

    .line 6057
    new-instance v3, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v3}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    .line 6060
    :cond_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3, p2, p1}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 6066
    :cond_4
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 6067
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6074
    :goto_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_5

    .line 6075
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 6078
    :cond_5
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    if-eqz v3, :cond_0

    .line 6079
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    invoke-interface {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_0

    .line 6069
    :cond_6
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v3, v3, v2

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method clear()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 5951
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 5952
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5953
    .local v2, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5955
    .local v3, "scrapCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 5956
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v6, v5, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1200(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V

    .line 5955
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5959
    .end local v0    # "i":I
    .end local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3    # "scrapCount":I
    :cond_0
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    .line 5960
    .local v4, "typeCount":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v4, :cond_2

    .line 5961
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v5, v0

    .line 5962
    .restart local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5964
    .restart local v3    # "scrapCount":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v3, :cond_1

    .line 5965
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v6, v5, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1300(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V

    .line 5964
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5960
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5970
    .end local v1    # "j":I
    .end local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3    # "scrapCount":I
    .end local v4    # "typeCount":I
    :cond_2
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v5, :cond_3

    .line 5971
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 5973
    :cond_3
    return-void
.end method

.method clearTransientStateViews()V
    .locals 1

    .prologue
    .line 6023
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v0, :cond_0

    .line 6024
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 6026
    :cond_0
    return-void
.end method

.method fillActiveViews(II)V
    .locals 4
    .param p1, "childCount"    # I
    .param p2, "firstActivePosition"    # I

    .prologue
    .line 5976
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    array-length v3, v3

    if-ge v3, p1, :cond_0

    .line 5977
    new-array v3, p1, [Landroid/view/View;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5980
    :cond_0
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mFirstActivePosition:I

    .line 5982
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5983
    .local v0, "activeViews":[Landroid/view/View;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_1

    .line 5984
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5988
    .local v1, "child":Landroid/view/View;
    aput-object v1, v0, v2

    .line 5983
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5990
    .end local v1    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method getActiveView(I)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 5993
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mFirstActivePosition:I

    sub-int v1, p1, v4

    .line 5994
    .local v1, "index":I
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5996
    .local v0, "activeViews":[Landroid/view/View;
    if-ltz v1, :cond_0

    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 5997
    aget-object v2, v0, v1

    .line 5998
    .local v2, "match":Landroid/view/View;
    aput-object v3, v0, v1

    .line 6003
    .end local v2    # "match":Landroid/view/View;
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v3

    goto :goto_0
.end method

.method getScrapView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 6029
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 6030
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    .line 6038
    :goto_0
    return-object v1

    .line 6032
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1400(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 6033
    .local v0, "whichScrap":I
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 6034
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 6038
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getTransientStateView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 6007
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v2, :cond_1

    .line 6019
    :cond_0
    :goto_0
    return-object v1

    .line 6011
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, p1}, Landroid/support/v4/util/SparseArrayCompat;->indexOfKey(I)I

    move-result v0

    .line 6012
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 6016
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 6017
    .local v1, "result":Landroid/view/View;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    goto :goto_0
.end method

.method public markChildrenDirty()V
    .locals 8

    .prologue
    .line 5919
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 5920
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5921
    .local v3, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 5923
    .local v4, "scrapCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 5924
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5923
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5927
    .end local v1    # "i":I
    .end local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4    # "scrapCount":I
    :cond_0
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    .line 5928
    .local v5, "typeCount":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v5, :cond_2

    .line 5929
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v3, v6, v1

    .line 5930
    .restart local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 5932
    .restart local v4    # "scrapCount":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    if-ge v2, v4, :cond_1

    .line 5933
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5932
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 5928
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5938
    .end local v2    # "j":I
    .end local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4    # "scrapCount":I
    .end local v5    # "typeCount":I
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v6, :cond_3

    .line 5939
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v0

    .line 5940
    .local v0, "count":I
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_3

    .line 5941
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5940
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5944
    .end local v0    # "count":I
    :cond_3
    return-void
.end method

.method reclaimScrapViews(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6165
    .local p1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 6166
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-interface {p1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 6176
    :cond_0
    return-void

    .line 6168
    :cond_1
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    .line 6169
    .local v3, "viewTypeCount":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 6171
    .local v2, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 6172
    aget-object v1, v2, v0

    .line 6173
    .local v1, "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 6171
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 5
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 6179
    .local p1, "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 6180
    .local v3, "size":I
    if-gtz v3, :cond_0

    .line 6181
    const/4 v2, 0x0

    .line 6194
    :goto_0
    return-object v2

    .line 6184
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 6185
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 6186
    .local v2, "scrapView":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .line 6188
    .local v1, "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    iget v4, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->scrappedFromPosition:I

    if-ne v4, p2, :cond_1

    .line 6189
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 6184
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6194
    .end local v1    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .end local v2    # "scrapView":Landroid/view/View;
    :cond_2
    add-int/lit8 v4, v3, -0x1

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    move-object v2, v4

    goto :goto_0
.end method

.method scrapActiveViews()V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 6085
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 6086
    .local v0, "activeViews":[Landroid/view/View;
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    if-le v10, v4, :cond_3

    .line 6088
    .local v4, "multipleScraps":Z
    :goto_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 6089
    .local v6, "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    array-length v1, v0

    .line 6091
    .local v1, "count":I
    add-int/lit8 v2, v1, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_7

    .line 6092
    aget-object v7, v0, v2

    .line 6093
    .local v7, "victim":Landroid/view/View;
    if-eqz v7, :cond_2

    .line 6094
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .line 6095
    .local v3, "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    iget v8, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->viewType:I

    .line 6097
    .local v8, "whichScrap":I
    aput-object v12, v0, v2

    .line 6099
    invoke-static {v7}, Landroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v5

    .line 6100
    .local v5, "scrapHasTransientState":Z
    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v5, :cond_4

    .line 6101
    :cond_0
    if-eqz v5, :cond_2

    .line 6102
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v10, v7, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1500(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V

    .line 6104
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v10, :cond_1

    .line 6105
    new-instance v10, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v10}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    .line 6108
    :cond_1
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mFirstActivePosition:I

    add-int/2addr v11, v2

    invoke-virtual {v10, v11, v7}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 6091
    .end local v3    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .end local v5    # "scrapHasTransientState":Z
    .end local v8    # "whichScrap":I
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v4    # "multipleScraps":Z
    .end local v6    # "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v7    # "victim":Landroid/view/View;
    :cond_3
    move v4, v9

    .line 6086
    goto :goto_0

    .line 6114
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .restart local v4    # "multipleScraps":Z
    .restart local v5    # "scrapHasTransientState":Z
    .restart local v6    # "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .restart local v7    # "victim":Landroid/view/View;
    .restart local v8    # "whichScrap":I
    :cond_4
    if-eqz v4, :cond_5

    .line 6115
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v6, v10, v8

    .line 6118
    :cond_5
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mFirstActivePosition:I

    add-int/2addr v10, v2

    iput v10, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->scrappedFromPosition:I

    .line 6119
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6123
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0xe

    if-lt v10, v11, :cond_6

    .line 6124
    invoke-virtual {v7, v12}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 6127
    :cond_6
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    if-eqz v10, :cond_2

    .line 6128
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    invoke-interface {v10, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_2

    .line 6133
    .end local v3    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .end local v5    # "scrapHasTransientState":Z
    .end local v7    # "victim":Landroid/view/View;
    .end local v8    # "whichScrap":I
    :cond_7
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->pruneScrapViews()V

    .line 6134
    return-void
.end method

.method public setViewTypeCount(I)V
    .locals 4
    .param p1, "viewTypeCount"    # I

    .prologue
    .line 5903
    const/4 v2, 0x1

    if-ge p1, v2, :cond_0

    .line 5904
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Can\'t have a viewTypeCount < 1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5908
    :cond_0
    new-array v1, p1, [Ljava/util/ArrayList;

    .line 5909
    .local v1, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 5910
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 5909
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5913
    :cond_1
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mViewTypeCount:I

    .line 5914
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5915
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5916
    return-void
.end method

.method public shouldRecycleViewType(I)Z
    .locals 1
    .param p1, "viewType"    # I

    .prologue
    .line 5947
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
