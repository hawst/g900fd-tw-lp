.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 1106
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v13, 0x2

    const/16 v12, 0x8

    const/4 v11, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1109
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isBtnClickEnable()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1376
    :goto_0
    return-void

    .line 1112
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1375
    :cond_1
    :goto_1
    :sswitch_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "onClick out "

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1114
    :sswitch_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1115
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1116
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    sget-object v9, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v9, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 1117
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showMaskActionbar()V
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1118
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1119
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1122
    :cond_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    const v9, 0x7f020051

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1123
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v9, 0x7f020067

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1126
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 1127
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-ne v9, v13, :cond_3

    move v6, v7

    :cond_3
    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V
    invoke-static {v8, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V

    .line 1129
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V

    .line 1130
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    goto :goto_1

    .line 1135
    :sswitch_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1136
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "press save button"

    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v4

    .line 1138
    .local v4, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1139
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v7, :cond_4

    .line 1140
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResultFile()V

    .line 1141
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/PicMotion;->getHandler()Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0x5000

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    .line 1142
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v7, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->changeSaveBtnState(Z)V

    goto/16 :goto_1

    .line 1144
    :cond_4
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    if-eqz v6, :cond_5

    .line 1145
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1147
    :cond_5
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    new-instance v7, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v7, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 1148
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1152
    :cond_6
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v7, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setBtnClickEnable(Z)V

    .line 1153
    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1154
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResult()V

    .line 1155
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->exitActivity()V

    .line 1156
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->getHandler()Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x5000

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1161
    .end local v4    # "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    :sswitch_3
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1162
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/PicMotion;->isUserEdit()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1163
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_1

    .line 1166
    :cond_7
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/PicMotion;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1167
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->exitWidthoutSave()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    goto/16 :goto_1

    .line 1174
    :sswitch_4
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1175
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->restoreLayoutClicked()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    goto/16 :goto_1

    .line 1178
    :sswitch_5
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1179
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->eraserLayoutClicked()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    goto/16 :goto_1

    .line 1182
    :sswitch_6
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1183
    :try_start_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mWitchCheckThreadHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1184
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainThreadHandler:Landroid/os/Handler;

    const v8, 0x9999

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1186
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v8, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$602(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z

    .line 1187
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->cancelMask()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1188
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 1189
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->resetEditView()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1190
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1191
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1192
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1194
    new-instance v2, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 1195
    .local v2, "result":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v6

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)I

    move-result v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)I

    move-result v9

    invoke-virtual {v6, v8, v9, v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->updateBlurInfo(IILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    .line 1196
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v2, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1198
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 1199
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateCurrentBlurInfo()V

    .line 1201
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->stopWork()V

    .line 1202
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    .line 1203
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1204
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1205
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v6

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1207
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v8, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z

    .line 1208
    monitor-exit v7

    goto/16 :goto_1

    .end local v2    # "result":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 1211
    :sswitch_7
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1212
    :try_start_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1213
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mWitchCheckThreadHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1215
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainThreadHandler:Landroid/os/Handler;

    const v8, 0x9999

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1218
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v8, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$602(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z

    .line 1219
    new-instance v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 1220
    .local v3, "retBitmap":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->confirmEdit(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v1

    .line 1221
    .local v1, "res":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EditDone result : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1225
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 1226
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateCurrentBlurInfo()V

    .line 1228
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->backToSelectFrame()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1229
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->resetEditView()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1230
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->getSeekBarValue()I

    move-result v8

    add-int/lit8 v8, v8, 0x1e

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)I

    .line 1231
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->getCurDegree()I

    move-result v5

    .line 1232
    .local v5, "uiDegree":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v9, 0x1

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->convertDegreeBetweenAlgAndUI(II)I
    invoke-static {v8, v5, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;II)I

    move-result v8

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)I

    .line 1235
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1236
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1237
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1239
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->stopWork()V

    .line 1240
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    .line 1241
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1243
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v6

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1245
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v6

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1247
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v8, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z
    invoke-static {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z

    .line 1250
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v6

    if-nez v6, :cond_8

    .line 1251
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->changeSaveBtnState(Z)V

    .line 1252
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->setUserEditFlag()V

    .line 1255
    .end local v1    # "res":I
    .end local v3    # "retBitmap":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    .end local v5    # "uiDegree":I
    :cond_8
    monitor-exit v7

    goto/16 :goto_1

    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v6

    .line 1258
    :sswitch_8
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1259
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1260
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-boolean v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->isUserEdit()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1261
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_1

    .line 1263
    :cond_9
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    goto/16 :goto_1

    .line 1267
    :sswitch_9
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1269
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v9

    const-string v10, "MAGICSHOT_SPF_Key"

    invoke-virtual {v9, v10, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 1270
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;

    .line 1271
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;

    move-result-object v8

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    if-eqz v8, :cond_a

    .line 1272
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "TYPE_PICMOTION_OBJECT"

    invoke-interface {v8, v9, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1273
    .local v0, "isFirstIn":Z
    if-eqz v0, :cond_d

    .line 1274
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "TYPE_PICMOTION_OBJECT"

    invoke-interface {v8, v9, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1275
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1276
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    new-instance v9, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3902(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 1277
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v8

    const/16 v9, 0x15

    invoke-virtual {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->show(I)V

    .line 1285
    .end local v0    # "isFirstIn":Z
    :cond_a
    :goto_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1286
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1287
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1288
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v8, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    .line 1289
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1291
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    sget-object v9, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION_OBJECT:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iput-object v9, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    .line 1292
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 1293
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    sget-object v9, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v9, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 1294
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showMaskActionbar()V
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1295
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 1296
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1299
    :cond_b
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    const v9, 0x7f020051

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1300
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v9, 0x7f020067

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1303
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 1304
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v13, :cond_e

    move v8, v7

    :goto_3
    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V
    invoke-static {v9, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V

    .line 1306
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v8, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V

    .line 1307
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 1308
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->restoreLayoutClicked()V
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1309
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v8

    aget-byte v6, v8, v6

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->putShotOnTop(I)V
    invoke-static {v7, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)V

    .line 1311
    :cond_c
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showActionBar()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    goto/16 :goto_1

    .line 1279
    .restart local v0    # "isFirstIn":Z
    :cond_d
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v8

    if-eqz v8, :cond_a

    .line 1280
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    goto/16 :goto_2

    .end local v0    # "isFirstIn":Z
    :cond_e
    move v8, v6

    .line 1304
    goto :goto_3

    .line 1315
    :sswitch_a
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1317
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v9

    const-string v10, "MAGICSHOT_SPF_Key"

    invoke-virtual {v9, v10, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 1318
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;

    .line 1319
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;

    move-result-object v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    if-eqz v8, :cond_f

    .line 1320
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "TYPE_PICMOTION_BlUR"

    invoke-interface {v8, v9, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1321
    .restart local v0    # "isFirstIn":Z
    if-eqz v0, :cond_11

    .line 1322
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "TYPE_PICMOTION_BlUR"

    invoke-interface {v8, v9, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1323
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1324
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    new-instance v9, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3902(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 1325
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v8

    const/16 v9, 0x14

    invoke-virtual {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->show(I)V

    .line 1333
    .end local v0    # "isFirstIn":Z
    :cond_f
    :goto_4
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1334
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1335
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1337
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    sget-object v9, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION_BLUR:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iput-object v9, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    .line 1338
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->startWork()V

    .line 1339
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1340
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    sget-object v9, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v9, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 1341
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showMaskActionbar()V
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1344
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    const v9, 0x7f020051

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1345
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v8, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v9, 0x7f020067

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1348
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v8, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 1349
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-ne v9, v13, :cond_12

    :goto_5
    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V
    invoke-static {v8, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V

    .line 1353
    :cond_10
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1354
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1355
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1356
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1357
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showActionBar()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    goto/16 :goto_1

    .line 1327
    .restart local v0    # "isFirstIn":Z
    :cond_11
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v8

    if-eqz v8, :cond_f

    .line 1328
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    goto/16 :goto_4

    .end local v0    # "isFirstIn":Z
    :cond_12
    move v7, v6

    .line 1349
    goto :goto_5

    .line 1360
    :sswitch_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x190

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1361
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v6, v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4202(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;J)J

    .line 1362
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->undo()V

    goto/16 :goto_1

    .line 1366
    :sswitch_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x190

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1367
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v6, v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4202(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;J)J

    .line 1368
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->redo()V

    goto/16 :goto_1

    .line 1112
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_8
        0x7f09004c -> :sswitch_3
        0x7f090053 -> :sswitch_2
        0x7f090059 -> :sswitch_0
        0x7f090075 -> :sswitch_1
        0x7f09007d -> :sswitch_6
        0x7f09007f -> :sswitch_7
        0x7f090089 -> :sswitch_b
        0x7f09008c -> :sswitch_c
        0x7f0900cc -> :sswitch_9
        0x7f0900cd -> :sswitch_a
        0x7f0900cf -> :sswitch_4
        0x7f0900d3 -> :sswitch_5
    .end sparse-switch
.end method
