.class public Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;
.super Landroid/widget/BaseAdapter;
.source "FrameAdapter_CheckedFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;
    }
.end annotation


# instance fields
.field mActivatedPostion:I

.field mContext:Landroid/content/Context;

.field mFrameArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field mInflater:Landroid/view/LayoutInflater;

.field mParent:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 25
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mInflater:Landroid/view/LayoutInflater;

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    .line 27
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public getActivatedPos()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 39
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 66
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 71
    iput-object p3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mParent:Landroid/view/ViewGroup;

    .line 73
    if-nez p2, :cond_1

    .line 74
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 75
    .local v6, "width":I
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 77
    .local v1, "height":I
    if-le v6, v1, :cond_0

    .line 78
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f03000f

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 82
    :goto_0
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;)V

    .line 83
    .local v2, "holder":Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;
    const v7, 0x7f09008f

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    .line 86
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 93
    .end local v1    # "height":I
    .end local v6    # "width":I
    :goto_1
    iget-object v7, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 95
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    if-eqz v7, :cond_2

    .line 96
    iget-object v8, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    :goto_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    invoke-virtual {p2, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 103
    const v7, 0x7f090090

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 105
    .local v4, "selectorView":Landroid/view/View;
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 106
    .local v3, "res":Landroid/content/res/Resources;
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    if-ne v7, p1, :cond_3

    .line 107
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/view/View;->setActivated(Z)V

    .line 108
    const v7, 0x7f060054

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "ck":Ljava/lang/String;
    invoke-virtual {p2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 116
    .end local v0    # "ck":Ljava/lang/String;
    :goto_3
    return-object p2

    .line 80
    .end local v2    # "holder":Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "selectorView":Landroid/view/View;
    .restart local v1    # "height":I
    .restart local v6    # "width":I
    :cond_0
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f030010

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 88
    .end local v1    # "height":I
    .end local v6    # "width":I
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;

    .restart local v2    # "holder":Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;
    goto :goto_1

    .line 98
    :cond_2
    iget-object v8, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mGreyFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 111
    .restart local v3    # "res":Landroid/content/res/Resources;
    .restart local v4    # "selectorView":Landroid/view/View;
    :cond_3
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/view/View;->setActivated(Z)V

    .line 112
    const v7, 0x7f060055

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "uck":Ljava/lang/String;
    invoke-virtual {p2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    return v0
.end method

.method public setActivated(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    .line 53
    return-void
.end method

.method public setFrameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;>;"
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    .line 32
    return-void
.end method
