.class public Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;
.super Landroid/view/ViewGroup;
.source "ScrollViewGroup.java"


# static fields
.field public static isMenuOpned:Z


# instance fields
.field private content_view:Landroid/view/View;

.field private distance:I

.field private duration:I

.field private scroller:Landroid/widget/Scroller;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-boolean v0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/16 v0, 0x12c

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/16 v0, 0x12c

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    .line 23
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    .line 24
    return-void
.end method


# virtual methods
.method public closeMenu()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 46
    sput-boolean v2, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    .line 47
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v1

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->distance:I

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 49
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->invalidate()V

    .line 50
    return-void
.end method

.method public closeMenu_1()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 53
    sput-boolean v2, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v1

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->distance:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 56
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->invalidate()V

    .line 57
    return-void
.end method

.method public closeMenu_2()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 61
    sput-boolean v2, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    .line 62
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getWidth()I

    move-result v3

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 63
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->invalidate()V

    .line 64
    return-void
.end method

.method public computeScroll()V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scrollTo(II)V

    .line 34
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->postInvalidate()V

    .line 36
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v3, 0x0

    .line 83
    if-eqz p1, :cond_0

    .line 84
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->content_view:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->content_view:Landroid/view/View;

    invoke-virtual {v0, v3, v3}, Landroid/view/View;->measure(II)V

    .line 86
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->content_view:Landroid/view/View;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getWidth()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/view/View;->layout(IIII)V

    .line 88
    :cond_0
    return-void
.end method

.method public setDistance(I)V
    .locals 0
    .param p1, "distance"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->distance:I

    .line 28
    return-void
.end method

.method public showMenu()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 39
    const/4 v0, 0x1

    sput-boolean v0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    .line 40
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v1

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->distance:I

    neg-int v3, v3

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 41
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->invalidate()V

    .line 42
    return-void
.end method

.method public slidingMenu()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getWidth()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    if-le v0, v1, :cond_1

    .line 68
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v3

    neg-int v3, v3

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 69
    sput-boolean v2, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    .line 76
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->invalidate()V

    .line 77
    return-void

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getWidth()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    if-gt v0, v1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v1

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->distance:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->getScrollX()I

    move-result v4

    add-int/2addr v3, v4

    neg-int v3, v3

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->duration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 74
    const/4 v0, 0x1

    sput-boolean v0, Lcom/arcsoft/magicshotstudio/ui/main/ScrollViewGroup;->isMenuOpned:Z

    goto :goto_0
.end method
