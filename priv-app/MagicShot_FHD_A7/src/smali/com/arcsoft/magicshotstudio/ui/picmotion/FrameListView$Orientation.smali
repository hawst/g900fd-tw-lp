.class public final enum Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;
.super Ljava/lang/Enum;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

.field public static final enum HORIZONTAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

.field public static final enum VERTICAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->HORIZONTAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    .line 140
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->VERTICAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    .line 138
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->HORIZONTAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->VERTICAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->$VALUES:[Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 138
    const-class v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    return-object v0
.end method

.method public static values()[Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->$VALUES:[Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    invoke-virtual {v0}, [Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    return-object v0
.end method
