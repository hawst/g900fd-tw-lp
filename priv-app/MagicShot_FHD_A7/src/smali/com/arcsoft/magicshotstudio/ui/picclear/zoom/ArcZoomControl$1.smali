.class Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;
.super Ljava/lang/Object;
.source "ArcZoomControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v4, 0x3e800000    # 0.25f

    const/4 v6, 0x0

    const/high16 v3, 0x40200000    # 2.5f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 286
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomInOrOut:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$000(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 287
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 288
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_1

    const/4 v0, 0x1

    .line 293
    .local v0, "isAtRest":Z
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v2

    mul-float/2addr v2, v4

    # -= operator for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$124(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 294
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 298
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$300(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$400(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->zoomWithoutEdgeEffect(FFF)V

    .line 301
    if-eqz v0, :cond_2

    .line 302
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$600(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$500(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 328
    :goto_1
    return-void

    .line 288
    .end local v0    # "isAtRest":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 304
    .restart local v0    # "isAtRest":Z
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 305
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 306
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 307
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    goto :goto_1

    .line 310
    .end local v0    # "isAtRest":Z
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 311
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v1

    const v2, 0x3727c5ac    # 1.0E-5f

    add-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    const/4 v0, 0x1

    .line 312
    .restart local v0    # "isAtRest":Z
    :goto_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v2

    mul-float/2addr v2, v4

    # += operator for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$116(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 313
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    .line 314
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 317
    :cond_4
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$300(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$400(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->zoomWithoutEdgeEffect(FFF)V

    .line 320
    if-nez v0, :cond_6

    .line 321
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$600(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$500(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 311
    .end local v0    # "isAtRest":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 323
    .restart local v0    # "isAtRest":Z
    :cond_6
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 324
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F
    invoke-static {v1, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$302(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    .line 325
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F
    invoke-static {v1, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->access$402(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F

    goto/16 :goto_1
.end method
