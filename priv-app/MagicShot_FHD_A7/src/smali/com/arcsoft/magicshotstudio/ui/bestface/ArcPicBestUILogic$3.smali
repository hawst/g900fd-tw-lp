.class Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;
.super Ljava/lang/Object;
.source "ArcPicBestUILogic.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showNoFaceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 0

    .prologue
    .line 699
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 703
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOperateEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 708
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 709
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    const/4 v1, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$302(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 712
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->dismissAllDialog()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    .line 713
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onSaveMergeResult()V

    goto :goto_0
.end method
