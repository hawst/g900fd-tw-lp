.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 2252
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2255
    const/4 v0, 0x0

    .line 2257
    .local v0, "consumed":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-eq v4, v5, :cond_4

    .line 2258
    const/4 v0, 0x1

    .line 2259
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)I

    move-result v4

    const v5, 0x7f09008a

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)I

    move-result v4

    const v5, 0x7f09008d

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2261
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v4, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3502(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z

    .line 2262
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I
    invoke-static {v4, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3602(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I

    .line 2263
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v5

    if-nez v5, :cond_3

    :goto_0
    invoke-virtual {v4, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    :cond_2
    move v1, v0

    .line 2323
    .end local v0    # "consumed":Z
    .local v1, "consumed":I
    :goto_1
    return v1

    .end local v1    # "consumed":I
    .restart local v0    # "consumed":Z
    :cond_3
    move v2, v3

    .line 2263
    goto :goto_0

    .line 2269
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_5

    .line 2270
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v4, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3502(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z

    .line 2271
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3602(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I

    .line 2273
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_2
    invoke-virtual {v5, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 2275
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 2317
    :cond_5
    :goto_3
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-eq v2, v4, :cond_6

    const/4 v4, 0x3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v4, v5, :cond_7

    .line 2319
    :cond_6
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v4, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3502(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z

    .line 2320
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I
    invoke-static {v4, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3602(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I

    .line 2321
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v5

    if-nez v5, :cond_9

    :goto_4
    invoke-virtual {v4, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    :cond_7
    move v1, v0

    .line 2323
    .restart local v1    # "consumed":I
    goto :goto_1

    .end local v1    # "consumed":I
    :cond_8
    move v4, v3

    .line 2273
    goto :goto_2

    .line 2277
    :sswitch_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2278
    const/4 v0, 0x1

    goto :goto_3

    .line 2287
    :sswitch_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2288
    const/4 v0, 0x1

    goto :goto_3

    .line 2292
    :sswitch_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2293
    const/4 v0, 0x1

    goto :goto_3

    .line 2297
    :sswitch_4
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2298
    const/4 v0, 0x1

    goto :goto_3

    .line 2302
    :sswitch_5
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2303
    const/4 v0, 0x1

    goto :goto_3

    .line 2307
    :sswitch_6
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2308
    const/4 v0, 0x1

    goto :goto_3

    :cond_9
    move v2, v3

    .line 2321
    goto :goto_4

    .line 2275
    :sswitch_data_0
    .sparse-switch
        0x7f09004c -> :sswitch_0
        0x7f090053 -> :sswitch_0
        0x7f090075 -> :sswitch_1
        0x7f09007f -> :sswitch_2
        0x7f090081 -> :sswitch_4
        0x7f090085 -> :sswitch_3
        0x7f090089 -> :sswitch_6
        0x7f09008c -> :sswitch_5
    .end sparse-switch
.end method
