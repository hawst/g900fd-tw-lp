.class Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;
.super Ljava/lang/Object;
.source "BounceView.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    .line 549
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v7, :cond_1

    .line 550
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v6

    const/high16 v3, 0x41200000    # 10.0f

    div-float v0, v2, v3

    .line 551
    .local v0, "f":F
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$100(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 552
    .local v1, "interpolation":F
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dX:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)F

    move-result v4

    sub-float v5, v6, v1

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    .line 553
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dY:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)F

    move-result v4

    sub-float v5, v6, v1

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    .line 554
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    iget v2, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v2, :cond_0

    .line 555
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->postInvalidate()V

    .line 556
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_2

    .line 557
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # operator++ for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$008(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)I

    .line 558
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v4, 0x5

    invoke-virtual {v2, v7, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 563
    .end local v0    # "f":F
    .end local v1    # "interpolation":F
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 560
    .restart local v0    # "f":F
    .restart local v1    # "interpolation":F
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    goto :goto_0
.end method
