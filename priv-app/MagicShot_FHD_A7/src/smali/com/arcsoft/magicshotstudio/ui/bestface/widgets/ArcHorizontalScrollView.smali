.class public Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "ArcHorizontalScrollView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_HorizontalScrollView"


# instance fields
.field private mCameraDegress:I

.field private mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

.field private mContext:Landroid/content/Context;

.field private mEnableScroll:Z

.field private mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

.field private mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mPicBestLayout:Landroid/widget/LinearLayout;

.field private mSelectPhotoNum:I

.field private mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mContext:Landroid/content/Context;

    .line 22
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 23
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mPicBestLayout:Landroid/widget/LinearLayout;

    .line 24
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    .line 25
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    .line 27
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCameraDegress:I

    .line 28
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mSelectPhotoNum:I

    .line 71
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mEnableScroll:Z

    .line 169
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    return-object v0
.end method

.method private showSelectViewInVisualRange(I)V
    .locals 5
    .param p1, "screenWidth"    # I

    .prologue
    const/4 v4, 0x0

    .line 214
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v2, :cond_0

    .line 215
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getCurrentCandidate()I

    move-result v1

    .line 216
    .local v1, "selectIndex":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 217
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 218
    .local v0, "location":[I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->getLocationOnScreen([I)V

    .line 219
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->getWidth()I

    move-result v2

    aget v3, v0, v4

    add-int/2addr v2, v3

    if-le v2, p1, :cond_1

    .line 220
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->getWidth()I

    move-result v2

    mul-int/2addr v2, v1

    invoke-virtual {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->scrollTo(II)V

    .line 228
    .end local v0    # "location":[I
    .end local v1    # "selectIndex":I
    :cond_0
    :goto_0
    return-void

    .line 222
    .restart local v0    # "location":[I
    .restart local v1    # "selectIndex":I
    :cond_1
    aget v2, v0, v4

    if-gez v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->getWidth()I

    move-result v2

    mul-int/2addr v2, v1

    invoke-virtual {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->scrollTo(II)V

    goto :goto_0
.end method

.method private viewRecyle()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 52
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v2, :cond_1

    .line 53
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 54
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 56
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aput-object v3, v2, v0

    .line 53
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v2, :cond_3

    .line 62
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 63
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v2, v2, v1

    if-eqz v2, :cond_2

    .line 65
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aput-object v3, v2, v1

    .line 62
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 69
    .end local v1    # "j":I
    :cond_3
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 232
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 233
    .local v1, "index":I
    const/4 v0, 0x0

    .line 234
    .local v0, "consumed":Z
    if-lez v1, :cond_0

    .line 235
    const/4 v0, 0x1

    .line 238
    :cond_0
    if-eqz v0, :cond_1

    .line 239
    const/4 v2, 0x1

    .line 241
    :goto_0
    return v2

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public hideGoodposeView(I)V
    .locals 3
    .param p1, "bestId"    # I

    .prologue
    .line 156
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v1, :cond_2

    .line 157
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 159
    if-ne p1, v0, :cond_1

    .line 160
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setVisibility(I)V

    .line 157
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setVisibility(I)V

    goto :goto_1

    .line 167
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method public init(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/content/Context;ILandroid/widget/LinearLayout;I)V
    .locals 1
    .param p1, "uiLogic"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "degree"    # I
    .param p4, "picBestLayout"    # Landroid/widget/LinearLayout;
    .param p5, "selectPhotoNum"    # I

    .prologue
    .line 37
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 38
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mContext:Landroid/content/Context;

    .line 39
    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCameraDegress:I

    .line 40
    iput-object p4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mPicBestLayout:Landroid/widget/LinearLayout;

    .line 41
    iput p5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mSelectPhotoNum:I

    .line 43
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mSelectPhotoNum:I

    new-array v0, v0, [Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    .line 44
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mSelectPhotoNum:I

    new-array v0, v0, [Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    .line 45
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 189
    if-nez p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v1, :cond_0

    .line 194
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    if-eq v1, p1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v1, :cond_4

    .line 196
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getCurrentCandidate()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 197
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->dealWithTheSameSelected()V

    goto :goto_0

    .line 200
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setCurrentCandidate(I)V

    .line 201
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->hideGoodposeView(I)V

    .line 202
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->setFocusFaceView(I)V

    .line 203
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->dealWithAnotherSelected(I)V

    .line 204
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->changeSaveBtnState(Z)V

    .line 205
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setUserEditFlag()V

    goto :goto_0

    .line 194
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 79
    invoke-super/range {p0 .. p5}, Landroid/widget/HorizontalScrollView;->onLayout(ZIIII)V

    .line 80
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mEnableScroll:Z

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    sub-int v0, p4, p2

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->showSelectViewInVisualRange(I)V

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mEnableScroll:Z

    .line 83
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getCurrentCandidate()I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mUiLogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getCurrentCandidate()I

    move-result v1

    aget-object v0, v0, v1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setFocusable(Z)V

    .line 88
    :cond_1
    return-void
.end method

.method public setCandidateFaceView(ILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;[I)V
    .locals 8
    .param p1, "currentPerson"    # I
    .param p2, "processResult"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .param p3, "candidateFaceNum"    # [I

    .prologue
    .line 93
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 95
    :cond_0
    const-string v5, "ArcSoft_BestFace_HorizontalScrollView"

    const-string v6, "setCandidateFaceView MERR_INVALID_PARAM"

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mPicBestLayout:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_2

    .line 99
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mPicBestLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 102
    :cond_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->viewRecyle()V

    .line 104
    if-eqz p3, :cond_8

    if-ltz p1, :cond_8

    array-length v5, p3

    if-ge p1, v5, :cond_8

    .line 106
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    aget v5, p3, p1

    if-ge v0, v5, :cond_8

    .line 107
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mContext:Landroid/content/Context;

    const v6, 0x7f030003

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 108
    .local v4, "templateView":Landroid/view/View;
    const v5, 0x7f09001f

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    .line 109
    .local v1, "imageView":Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    const v5, 0x7f090020

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    .line 110
    .local v2, "poseView":Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    const/4 v3, 0x0

    .line 112
    .local v3, "smallFaceData":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_3

    .line 113
    invoke-virtual {p2}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v5

    aget-object v5, v5, p1

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetFaceGroup()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->GetFaceImg()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 116
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v3, :cond_4

    .line 117
    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 119
    invoke-virtual {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    invoke-virtual {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 124
    :cond_4
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v5, :cond_5

    if-ltz v0, :cond_5

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v5, v5

    if-ge v0, v5, :cond_5

    .line 126
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aput-object v1, v5, v0

    .line 129
    :cond_5
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v5, :cond_6

    if-ltz v0, :cond_6

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v5, v5

    if-ge v0, v5, :cond_6

    .line 131
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mGoodPoseArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aput-object v2, v5, v0

    .line 134
    :cond_6
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mPicBestLayout:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_7

    .line 135
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mPicBestLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 106
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    .end local v0    # "i":I
    .end local v1    # "imageView":Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    .end local v2    # "poseView":Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    .end local v3    # "smallFaceData":Landroid/graphics/Bitmap;
    .end local v4    # "templateView":Landroid/view/View;
    :cond_8
    return-void
.end method

.method public setEnableScroll(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mEnableScroll:Z

    .line 75
    return-void
.end method

.method public setFocusFaceView(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 142
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    if-eqz v1, :cond_2

    .line 143
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 144
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 145
    if-ne v0, p1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setIsSelected(Z)V

    .line 143
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setIsSelected(Z)V

    goto :goto_1

    .line 153
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method public unInit()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->viewRecyle()V

    .line 49
    return-void
.end method
