.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1612
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 16
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1617
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1618
    .local v6, "currTime":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-wide v12, v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLastClickTime:J

    sub-long v12, v6, v12

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-wide v14, v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mClickInterval:J

    cmp-long v11, v12, v14

    if-gez v11, :cond_1

    .line 1619
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "mClickInterval < 400"

    invoke-static {v11, v12}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    :cond_0
    :goto_0
    return-void

    .line 1623
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v11, v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    move/from16 v0, p3

    if-gt v11, v0, :cond_2

    .line 1624
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "error occured,mFrameList.size() <= position"

    invoke-static {v11, v12}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1628
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v11

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 1629
    .local v8, "res":Landroid/content/res/Resources;
    const v11, 0x7f090063

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 1630
    .local v2, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v11

    if-nez v11, :cond_3

    const/4 v3, 0x1

    .line 1631
    .local v3, "checked":Z
    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1632
    if-eqz v3, :cond_4

    .line 1633
    const v11, 0x7f060054

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1634
    .local v4, "ck":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1640
    .end local v4    # "ck":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v12, v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    monitor-enter v12

    .line 1641
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v11, v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    .line 1642
    .local v5, "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    iput-boolean v3, v5, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    .line 1643
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v11, v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v11, v0, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1644
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1646
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkEditable()V
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 1647
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initCheckedFrameList()V
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 1648
    new-instance v9, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {v9, v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 1649
    .local v9, "task":Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;
    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Void;

    invoke-virtual {v9, v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1651
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v11

    if-nez v11, :cond_0

    .line 1652
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->changeSaveBtnState(Z)V

    .line 1653
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v11

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/Drama;->setUserEditFlag()V

    goto/16 :goto_0

    .line 1630
    .end local v3    # "checked":Z
    .end local v5    # "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    .end local v9    # "task":Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 1636
    .restart local v3    # "checked":Z
    :cond_4
    const v11, 0x7f060055

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1637
    .local v10, "uck":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1644
    .end local v10    # "uck":Ljava/lang/String;
    :catchall_0
    move-exception v11

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v11
.end method
