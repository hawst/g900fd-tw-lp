.class Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;
.super Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$WindowRunnnable;
.source "FrameListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V
    .locals 1

    .prologue
    .line 6519
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$WindowRunnnable;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;

    .prologue
    .line 6519
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 6522
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mMotionPosition:I
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v4

    .line 6523
    .local v4, "motionPosition":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mFirstPosition:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3300(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v6

    sub-int v6, v4, v6

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 6525
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 6526
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$1400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Landroid/widget/ListAdapter;

    move-result-object v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mMotionPosition:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 6528
    .local v2, "longPressId":J
    const/4 v1, 0x0

    .line 6529
    .local v1, "handled":Z
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->sameWindow()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mDataChanged:Z
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 6530
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->performLongPress(Landroid/view/View;IJ)Z
    invoke-static {v5, v0, v4, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$4000(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Landroid/view/View;IJ)Z

    move-result v1

    .line 6533
    :cond_0
    if-eqz v1, :cond_2

    .line 6534
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    const/4 v6, -0x1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mTouchMode:I
    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$402(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)I

    .line 6535
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setPressed(Z)V

    .line 6536
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 6541
    .end local v1    # "handled":Z
    .end local v2    # "longPressId":J
    :cond_1
    :goto_0
    return-void

    .line 6538
    .restart local v1    # "handled":Z
    .restart local v2    # "longPressId":J
    :cond_2
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForLongPress;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    const/4 v6, 0x2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mTouchMode:I
    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$402(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)I

    goto :goto_0
.end method
