.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;
.super Ljava/lang/Object;
.source "HeadbarMgr.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v10, 0x5000

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 244
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->isBtnClickEnable()Z

    move-result v7

    if-nez v7, :cond_1

    .line 328
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 247
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 248
    .local v2, "id":I
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 250
    :sswitch_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->hidePopupToast()V

    .line 251
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 252
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->isUserEdit()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBestCount()I

    move-result v7

    if-lez v7, :cond_2

    .line 254
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 256
    :cond_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->finishActivity()V
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    goto :goto_0

    .line 260
    :sswitch_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->hidePopupToast()V

    .line 261
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 262
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->isUserEdit()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBestCount()I

    move-result v7

    if-lez v7, :cond_3

    .line 264
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_0

    .line 266
    :cond_3
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->finishActivity()V
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    goto/16 :goto_0

    .line 270
    :sswitch_3
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->hidePopupToast()V

    .line 271
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v6

    .line 272
    .local v6, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 273
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    iget-boolean v7, v7, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    if-eqz v7, :cond_4

    .line 274
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->saveFiles()V

    .line 275
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7, v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->changeSaveBtnState(Z)V

    .line 276
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 280
    :cond_4
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getCurSavePaths()[Ljava/lang/String;

    move-result-object v1

    .line 281
    .local v1, "filePaths":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v7, v1

    if-lez v7, :cond_0

    .line 282
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    invoke-virtual {v7, v1}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFiles([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 286
    .end local v1    # "filePaths":[Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7, v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setBtnClickEnable(Z)V

    .line 287
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->saveFiles()V

    .line 288
    invoke-virtual {v6, v9}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 289
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->exitActivity()V

    .line 290
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getHandler()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 301
    .end local v6    # "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    :sswitch_4
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->isSelected()Z

    move-result v3

    .line 302
    .local v3, "isSelected":Z
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-nez v3, :cond_7

    move v7, v8

    :goto_1
    invoke-virtual {v10, v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setBestIconSelected(Z)V

    .line 303
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 304
    .local v4, "res":Landroid/content/res/Resources;
    const v7, 0x7f06000c

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 305
    .local v5, "save":Ljava/lang/String;
    const v7, 0x7f060012

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "disabled":Ljava/lang/String;
    if-nez v3, :cond_8

    .line 307
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setCurIndexToBest()V

    .line 308
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 309
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 310
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 323
    :cond_6
    :goto_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v7, v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->changeSaveBtnState(Z)V

    .line 324
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->setUserEditFlag()V

    goto/16 :goto_0

    .end local v0    # "disabled":Ljava/lang/String;
    .end local v4    # "res":Landroid/content/res/Resources;
    .end local v5    # "save":Ljava/lang/String;
    :cond_7
    move v7, v9

    .line 302
    goto :goto_1

    .line 312
    .restart local v0    # "disabled":Ljava/lang/String;
    .restart local v4    # "res":Landroid/content/res/Resources;
    .restart local v5    # "save":Ljava/lang/String;
    :cond_8
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setCurIndexToNormal()V

    .line 313
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBestCount()I

    move-result v7

    if-nez v7, :cond_6

    .line 314
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 315
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 316
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->isTalkBackOn()Z
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 317
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/RelativeLayout;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 319
    :cond_9
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 248
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_1
        0x7f090046 -> :sswitch_4
        0x7f09004c -> :sswitch_2
        0x7f090053 -> :sswitch_3
        0x7f090059 -> :sswitch_0
    .end sparse-switch
.end method
