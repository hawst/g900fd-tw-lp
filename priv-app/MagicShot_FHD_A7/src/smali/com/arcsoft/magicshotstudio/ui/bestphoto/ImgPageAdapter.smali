.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ImgPageAdapter.java"


# instance fields
.field private mListItem:Ljava/util/List;

.field private mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;)V
    .locals 1
    .param p1, "list"    # Ljava/util/List;
    .param p2, "callback"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 23
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mListItem:Ljava/util/List;

    .line 24
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 25
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 46
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mListItem:Ljava/util/List;

    if-nez v0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mListItem:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v6, -0x1

    .line 50
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 51
    .local v1, "context":Landroid/content/Context;
    const v3, 0x7f030007

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroid/view/ViewGroup;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .line 53
    .local v2, "photoView":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mListItem:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 54
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v2, p2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->setId(I)V

    .line 56
    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->initZoomView(Landroid/content/Context;)V

    .line 57
    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    const-string v3, "xsj"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "instantiateItem : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    if-eqz v3, :cond_0

    .line 60
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->setZoomStateCallback(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;)V

    .line 64
    :cond_0
    invoke-virtual {p1, v2, v6, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 75
    return-object v2
.end method

.method public bridge synthetic instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 40
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setZoomStateCallback(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 29
    return-void
.end method
