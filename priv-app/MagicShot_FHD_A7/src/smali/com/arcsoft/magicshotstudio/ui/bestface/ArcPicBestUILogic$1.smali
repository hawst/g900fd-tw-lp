.class Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;
.super Ljava/lang/Object;
.source "ArcPicBestUILogic.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    const/4 v1, 0x0

    .line 366
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 369
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 382
    :goto_0
    return-void

    .line 371
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_0

    .line 374
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 375
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    goto :goto_0

    .line 378
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onSaveMergeResult()V

    .line 379
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 369
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
