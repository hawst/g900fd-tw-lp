.class public Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
.super Ljava/lang/Object;
.source "ArcZoomControl.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field private static final ESPINON:F = 1.0E-6f

.field public static final MAX_DOUBLE_TAP_FACTOR:F = 2.5f

.field private static final MAX_ZOOM:F = 3.0f

.field private static final MIN_DOUBLE_TAP_FACTOR:F = 1.0f

.field private static final MIN_ZOOM:F = 1.0f


# instance fields
.field private mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

.field private mContext:Landroid/content/Context;

.field private mDoubleTapZoomInOrOut:Z

.field private final mDoubleTapZoomRunnable:Ljava/lang/Runnable;

.field private mDoubleZoomFactor:F

.field private mDoubleZoomInXPosition:F

.field private mDoubleZoomInYPosition:F

.field private final mHandler:Landroid/os/Handler;

.field private mPanMaxX:F

.field private mPanMaxY:F

.field private mPanMinX:F

.field private mPanMinY:F

.field private mPointF:Landroid/graphics/PointF;

.field private final mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    .line 19
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    .line 26
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mHandler:Landroid/os/Handler;

    .line 28
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    .line 276
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F

    .line 277
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F

    .line 278
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    .line 284
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    .line 337
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    .line 31
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    return p1
.end method

.method static synthetic access$116(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$124(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F

    return v0
.end method

.method static synthetic access$302(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F

    return p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F

    return v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F

    return p1
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private doubleTapZoomInWithAnimation()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 273
    return-void
.end method

.method private getMaxPanDelta(F)F
    .locals 3
    .param p1, "zoom"    # F

    .prologue
    .line 120
    const/4 v0, 0x0

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, p1, v2

    div-float/2addr v2, p1

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private limitPan()V
    .locals 12

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v9, 0x0

    .line 155
    const/4 v1, 0x0

    .line 157
    .local v1, "currentDirection":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 159
    .local v0, "aspectQuotient":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v6

    .line 160
    .local v6, "zoomX":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v7

    .line 162
    .local v7, "zoomY":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    sub-float v4, v10, v8

    .line 163
    .local v4, "panMinX":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    add-float v2, v10, v8

    .line 164
    .local v2, "panMaxX":F
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    sub-float v5, v10, v8

    .line 165
    .local v5, "panMinY":F
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    add-float v3, v10, v8

    .line 167
    .local v3, "panMaxY":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v8

    cmpg-float v8, v8, v4

    if-gez v8, :cond_0

    .line 168
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 169
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_0

    .line 170
    or-int/lit8 v1, v1, 0x1

    .line 175
    :cond_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v8

    cmpl-float v8, v8, v2

    if-lez v8, :cond_1

    .line 176
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 177
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1

    .line 178
    or-int/lit8 v1, v1, 0x4

    .line 183
    :cond_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v8

    cmpg-float v8, v8, v5

    if-gez v8, :cond_2

    .line 184
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 185
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    .line 186
    or-int/lit8 v1, v1, 0x2

    .line 191
    :cond_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v8

    cmpl-float v8, v8, v3

    if-lez v8, :cond_3

    .line 192
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 193
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_3

    .line 194
    or-int/lit8 v1, v1, 0x8

    .line 199
    :cond_3
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v8

    cmpl-float v8, v11, v8

    if-gtz v8, :cond_4

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v8

    cmpg-float v8, v11, v8

    if-gez v8, :cond_5

    .line 200
    :cond_4
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v8, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setEffectDirection(I)V

    .line 205
    :cond_5
    return-void
.end method

.method private limitPanWithoutEdgeEffect()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f000000    # 0.5f

    .line 208
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 210
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v5

    .line 211
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v6

    .line 213
    .local v6, "zoomY":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    sub-float v3, v8, v7

    .line 214
    .local v3, "panMinX":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    add-float v1, v8, v7

    .line 215
    .local v1, "panMaxX":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    sub-float v4, v8, v7

    .line 216
    .local v4, "panMinY":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    add-float v2, v8, v7

    .line 218
    .local v2, "panMaxY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v7

    cmpg-float v7, v7, v3

    if-gez v7, :cond_0

    .line 219
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 221
    :cond_0
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v7

    cmpl-float v7, v7, v1

    if-lez v7, :cond_1

    .line 222
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 224
    :cond_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v7

    cmpg-float v7, v7, v4

    if-gez v7, :cond_2

    .line 225
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 227
    :cond_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_3

    .line 228
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 231
    :cond_3
    return-void
.end method

.method private limitZoom()V
    .locals 4

    .prologue
    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 124
    const/16 v0, 0xf

    .line 125
    .local v0, "currentDirection":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setEffectDirection(I)V

    .line 127
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    .line 135
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->stopZoomEdgeEffect()V

    goto :goto_0
.end method

.method private limitZoomWithoutEdgeEffect()V
    .locals 3

    .prologue
    const/high16 v2, 0x40400000    # 3.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 234
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    goto :goto_0
.end method

.method private updatePanLimits()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 143
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 145
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v1

    .line 146
    .local v1, "zoomX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v2

    .line 148
    .local v2, "zoomY":F
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    sub-float v3, v4, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPanMinX:F

    .line 149
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    add-float/2addr v3, v4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPanMaxX:F

    .line 150
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    sub-float v3, v4, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPanMinY:F

    .line 151
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    add-float/2addr v3, v4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPanMaxY:F

    .line 152
    return-void
.end method


# virtual methods
.method public doubleTapZoom(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 256
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    .line 258
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    .line 259
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F

    .line 260
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F

    .line 261
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->doubleTapZoomInWithAnimation()V

    .line 269
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomFactor:F

    .line 264
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInXPosition:F

    .line 265
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleZoomInYPosition:F

    .line 266
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->doubleTapZoomInWithAnimation()V

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    goto :goto_0
.end method

.method public getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    return-object v0
.end method

.method public pan(FF)V
    .locals 4
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 86
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 88
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v3

    div-float/2addr p1, v3

    .line 89
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v3

    div-float/2addr p2, v3

    .line 91
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v3

    add-float v1, v3, p1

    .line 92
    .local v1, "newPanX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v3

    add-float v2, v3, p2

    .line 94
    .local v2, "newPanY":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 95
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 96
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setEffectOffset_X(F)V

    .line 97
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, p2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setEffectOffset_Y(F)V

    .line 99
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->limitPan()V

    .line 100
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    .line 101
    return-void
.end method

.method public panWithoutEdgeEffect(FF)V
    .locals 4
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 104
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 106
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v3

    div-float/2addr p1, v3

    .line 107
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v3

    div-float/2addr p2, v3

    .line 109
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v3

    add-float v1, v3, p1

    .line 110
    .local v1, "newPanX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v3

    add-float v2, v3, p2

    .line 112
    .local v2, "newPanY":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 113
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 115
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->limitPanWithoutEdgeEffect()V

    .line 116
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    .line 117
    return-void
.end method

.method public setAspectQuotient(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;)V
    .locals 1
    .param p1, "aspectQuotient"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->deleteObserver(Ljava/util/Observer;)V

    .line 39
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    .line 40
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->addObserver(Ljava/util/Observer;)V

    .line 41
    return-void
.end method

.method public setEdgeEffectEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 245
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setEdgeEffectEnabled(Z)V

    .line 246
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    .line 247
    return-void
.end method

.method public stopDoubleTapZoomRunnable()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 335
    :cond_0
    return-void
.end method

.method public stopZoomEdgeEffect()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->stopZoomEdgeEffect()V

    .line 140
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->limitZoom()V

    .line 252
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->limitPanWithoutEdgeEffect()V

    .line 253
    return-void
.end method

.method public updatePan(Landroid/view/MotionEvent;II)V
    .locals 12
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    .line 341
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v9

    const/high16 v10, 0x3f800000    # 1.0f

    cmpl-float v9, v9, v10

    if-lez v9, :cond_0

    .line 342
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->getZoomOutPoint()Landroid/graphics/Point;

    move-result-object v8

    .line 343
    .local v8, "zoomOutPoint":Landroid/graphics/Point;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v10, v8, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    int-to-float v11, p2

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 344
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v10, v8, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    int-to-float v11, p3

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->y:F

    .line 385
    .end local v8    # "zoomOutPoint":Landroid/graphics/Point;
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->updateZoomState()V

    .line 347
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->getMaxWidth()I

    move-result v1

    .line 348
    .local v1, "maxWidth":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->getMaxHeight()I

    move-result v0

    .line 349
    .local v0, "maxHeight":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->updateZoomPointX()I

    move-result v6

    .line 350
    .local v6, "zoomOffsetX":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->updateZoomPointY()I

    move-result v7

    .line 351
    .local v7, "zoomOffsetY":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/Eraser;->getRectDst()Landroid/graphics/Rect;

    move-result-object v5

    .line 352
    .local v5, "rectDst":Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, p2, v9

    div-int/lit8 v2, v9, 0x2

    .line 353
    .local v2, "offsetX":I
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v9, p3, v9

    div-int/lit8 v3, v9, 0x2

    .line 355
    .local v3, "offsetY":I
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 356
    .local v4, "pointAfterAdjust":Landroid/graphics/Point;
    if-gt v1, p2, :cond_1

    .line 357
    div-int/lit8 v9, p2, 0x2

    iput v9, v4, Landroid/graphics/Point;->x:I

    .line 369
    :goto_1
    if-gt v0, p3, :cond_4

    .line 370
    div-int/lit8 v9, p3, 0x2

    iput v9, v4, Landroid/graphics/Point;->y:I

    .line 382
    :goto_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v10, v4, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    int-to-float v11, p2

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 383
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v10, v4, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    int-to-float v11, p3

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 359
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    add-int v10, v2, v6

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_2

    .line 360
    add-int v9, v2, v6

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 362
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    sub-int v10, p2, v2

    sub-int/2addr v10, v6

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 363
    sub-int v9, p2, v2

    sub-int/2addr v9, v6

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 366
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v9, v9

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 372
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    add-int v10, v3, v7

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_5

    .line 373
    add-int v9, v3, v7

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2

    .line 375
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    sub-int v10, p3, v3

    sub-int/2addr v10, v7

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_6

    .line 376
    sub-int v9, p3, v3

    sub-int/2addr v9, v7

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2

    .line 379
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public zoom(FFF)V
    .locals 12
    .param p1, "f"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 48
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 50
    .local v0, "aspectQuotient":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v3

    .line 51
    .local v3, "prevZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v4

    .line 53
    .local v4, "prevZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoom()F

    move-result v6

    mul-float/2addr v6, p1

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    .line 54
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->limitZoom()V

    .line 56
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v1

    .line 57
    .local v1, "newZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v2

    .line 60
    .local v2, "newZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v6

    sub-float v7, p2, v11

    div-float v8, v10, v3

    div-float v9, v10, v1

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 61
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v6

    sub-float v7, p3, v11

    div-float v8, v10, v4

    div-float v9, v10, v2

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 63
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    .line 64
    return-void
.end method

.method public zoomWithoutEdgeEffect(FFF)V
    .locals 12
    .param p1, "f"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 67
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 69
    .local v0, "aspectQuotient":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v3

    .line 70
    .local v3, "prevZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v4

    .line 72
    .local v4, "prevZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    .line 73
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->limitZoomWithoutEdgeEffect()V

    .line 75
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v1

    .line 76
    .local v1, "newZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v2

    .line 79
    .local v2, "newZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v6

    sub-float v7, p2, v11

    div-float v8, v10, v3

    div-float v9, v10, v1

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 80
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v6

    sub-float v7, p3, v11

    div-float v8, v10, v4

    div-float v9, v10, v2

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 82
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    .line 83
    return-void
.end method
