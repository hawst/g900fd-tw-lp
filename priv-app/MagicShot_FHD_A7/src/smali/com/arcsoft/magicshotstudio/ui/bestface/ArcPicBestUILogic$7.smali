.class Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;
.super Landroid/os/Handler;
.source "ArcPicBestUILogic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 0

    .prologue
    .line 1068
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1071
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1088
    :cond_0
    :goto_0
    return-void

    .line 1073
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1074
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DstFile"

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1075
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIntent:Landroid/content/Intent;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/BestFace;->setResult(ILandroid/content/Intent;)V

    .line 1077
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->exitActivity()V

    goto :goto_0

    .line 1080
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1081
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onSaveMergeResult()V

    .line 1082
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 1071
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
