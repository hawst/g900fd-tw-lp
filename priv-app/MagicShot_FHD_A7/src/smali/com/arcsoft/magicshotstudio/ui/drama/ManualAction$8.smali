.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1483
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionDown(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->actionDown(II)V

    .line 1515
    return-void
.end method

.method public actionMove(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->actionMove(II)V

    .line 1528
    return-void
.end method

.method public actionUp()V
    .locals 1

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->actionUp()V

    .line 1519
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->reSetDirection()V

    .line 1520
    return-void
.end method

.method public dealMinZoom()V
    .locals 1

    .prologue
    .line 1535
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;->dealMinZoom()V

    .line 1536
    return-void
.end method

.method public endAllBounceView(Z)V
    .locals 2
    .param p1, "bInvalidate"    # Z

    .prologue
    .line 1493
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1494
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1495
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->endAllBounce(Landroid/graphics/Rect;Z)V

    .line 1497
    :cond_0
    return-void
.end method

.method public endSingleBounce(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1507
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1508
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1509
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->endSingleBounce(Landroid/graphics/Rect;I)V

    .line 1511
    :cond_0
    return-void
.end method

.method public getIsTouching()Z
    .locals 1

    .prologue
    .line 1531
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->getIsTouching()Z

    move-result v0

    return v0
.end method

.method public resetDirection()V
    .locals 1

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->reSetDirection()V

    .line 1524
    return-void
.end method

.method public startAllBounceView()V
    .locals 2

    .prologue
    .line 1486
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1487
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1488
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->startAllBounce(Landroid/graphics/Rect;)V

    .line 1490
    :cond_0
    return-void
.end method

.method public startBounce(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1500
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1501
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1502
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->startBounce(Landroid/graphics/Rect;I)V

    .line 1504
    :cond_0
    return-void
.end method
