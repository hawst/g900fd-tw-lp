.class public Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
.super Ljava/lang/Object;
.source "ShareDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;
    }
.end annotation


# instance fields
.field final VALID_CLICK_INTERVAL:I

.field mActCount:I

.field mActInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field mIntent:Landroid/content/Intent;

.field mLastTime:J

.field mUriPath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    .line 24
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    .line 25
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mActInfoList:Ljava/util/List;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mActCount:I

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mLastTime:J

    .line 32
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->VALID_CLICK_INTERVAL:I

    .line 33
    const-class v0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->tag:Ljava/lang/String;

    .line 44
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method private getContentUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 52
    const/4 v6, 0x0

    .line 53
    .local v6, "contentUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 55
    .local v7, "cursor":Landroid/database/Cursor;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    .line 56
    .local v2, "projection":[Ljava/lang/String;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 57
    .local v1, "mediaUri":Landroid/net/Uri;
    const-string v3, "_data=?"

    .line 58
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    aput-object p1, v4, v5

    .line 61
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 62
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 65
    .local v9, "id":I
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v10, v9

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 68
    .end local v9    # "id":I
    :cond_0
    if-eqz v7, :cond_1

    .line 69
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :cond_1
    if-eqz v7, :cond_2

    .line 75
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 79
    :cond_2
    :goto_0
    return-object v6

    .line 71
    :catch_0
    move-exception v8

    .line 72
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    if-eqz v7, :cond_2

    .line 75
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 74
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 75
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private showShareChooser()V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 112
    .local v0, "currTime":J
    iget-wide v8, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mLastTime:J

    sub-long v8, v0, v8

    const-wide/16 v10, 0x3e8

    cmp-long v7, v8, v10

    if-gez v7, :cond_0

    move v2, v5

    .line 113
    .local v2, "doubleClicked":Z
    :goto_0
    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mLastTime:J

    .line 114
    if-eqz v2, :cond_1

    .line 135
    :goto_1
    return-void

    .end local v2    # "doubleClicked":Z
    :cond_0
    move v2, v6

    .line 112
    goto :goto_0

    .line 121
    .restart local v2    # "doubleClicked":Z
    :cond_1
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    iput-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    .line 122
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v5, :cond_2

    move v3, v5

    .line 123
    .local v3, "multi":Z
    :goto_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    if-eqz v3, :cond_3

    const-string v5, "android.intent.action.SEND_MULTIPLE"

    :goto_3
    invoke-virtual {v7, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    const-string v7, "image/jpeg"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    if-eqz v3, :cond_4

    .line 127
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    const-string v6, "android.intent.extra.STREAM"

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 132
    :goto_4
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mContext:Landroid/content/Context;

    const v7, 0x7f060018

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v4

    .line 134
    .local v4, "shareChooser":Landroid/content/Intent;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .end local v3    # "multi":Z
    .end local v4    # "shareChooser":Landroid/content/Intent;
    :cond_2
    move v3, v6

    .line 122
    goto :goto_2

    .line 123
    .restart local v3    # "multi":Z
    :cond_3
    const-string v5, "android.intent.action.SEND"

    goto :goto_3

    .line 129
    :cond_4
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    const-string v8, "android.intent.extra.STREAM"

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Parcelable;

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_4
.end method


# virtual methods
.method public getPackageInfo()V
    .locals 7

    .prologue
    .line 138
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 139
    .local v4, "pm":Landroid/content/pm/PackageManager;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mIntent:Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 140
    .local v1, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v5, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v5, v4}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 142
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mActInfoList:Ljava/util/List;

    .line 143
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 144
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 145
    .local v3, "info":Landroid/content/pm/ResolveInfo;
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;)V

    .line 147
    .local v0, "actInfo":Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iput-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;->actName:Ljava/lang/String;

    .line 148
    invoke-virtual {v3, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;->labelName:Ljava/lang/String;

    .line 149
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;->packageName:Ljava/lang/String;

    .line 150
    iget v5, v3, Landroid/content/pm/ResolveInfo;->icon:I

    iput v5, v0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;->icon:I

    .line 152
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mActInfoList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 154
    .end local v0    # "actInfo":Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog$ActInfo;
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    :cond_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mActInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mActCount:I

    .line 155
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public shareFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "fName"    # Ljava/lang/String;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 88
    :cond_0
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->setUri(Landroid/net/Uri;)V

    .line 89
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->showShareChooser()V

    .line 90
    return-void
.end method

.method public shareFiles([Ljava/lang/String;)V
    .locals 5
    .param p1, "fNames"    # [Ljava/lang/String;

    .prologue
    .line 93
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 94
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->mUriPath:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 97
    :cond_0
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 99
    .local v3, "name":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->setUri(Landroid/net/Uri;)V

    .line 97
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->showShareChooser()V

    .line 103
    return-void
.end method
