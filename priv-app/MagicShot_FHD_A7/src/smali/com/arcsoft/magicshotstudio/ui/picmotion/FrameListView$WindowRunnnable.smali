.class Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;
.super Ljava/lang/Object;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WindowRunnnable"
.end annotation


# instance fields
.field private mOriginalAttachCount:I

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0

    .prologue
    .line 6432
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;

    .prologue
    .line 6432
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    return-void
.end method


# virtual methods
.method public rememberWindowAttachCount()V
    .locals 1

    .prologue
    .line 6436
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWindowAttachCount()I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$3000(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;->mOriginalAttachCount:I

    .line 6437
    return-void
.end method

.method public sameWindow()Z
    .locals 2

    .prologue
    .line 6440
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWindowAttachCount()I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$3100(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;->mOriginalAttachCount:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
