.class final enum Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
.super Ljava/lang/Enum;
.source "ManualAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "WorkStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

.field public static final enum DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

.field public static final enum PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

.field public static final enum SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

.field public static final enum UNKNOWN:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 197
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->UNKNOWN:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    const-string v1, "SELETEFRAME"

    invoke-direct {v0, v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    const-string v1, "DRAWMASK"

    invoke-direct {v0, v1, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    const-string v1, "PENDINGMASK"

    invoke-direct {v0, v1, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 196
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->UNKNOWN:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->$VALUES:[Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 196
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 196
    const-class v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    return-object v0
.end method

.method public static values()[Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->$VALUES:[Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    invoke-virtual {v0}, [Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    return-object v0
.end method
