.class Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;
.super Landroid/support/v4/view/AccessibilityDelegateCompat;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListItemAccessibilityDelegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V
    .locals 0

    .prologue
    .line 6595
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-direct {p0}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;

    .prologue
    .line 6595
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 4
    .param p1, "host"    # Landroid/view/View;
    .param p2, "info"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .prologue
    const/4 v3, 0x1

    .line 6598
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/AccessibilityDelegateCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 6600
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 6601
    .local v1, "position":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6604
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-nez v0, :cond_1

    .line 6629
    :cond_0
    :goto_0
    return-void

    .line 6609
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6613
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getSelectedItemPosition()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 6614
    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setSelected(Z)V

    .line 6615
    const/16 v2, 0x8

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 6620
    :goto_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 6621
    const/16 v2, 0x10

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 6622
    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClickable(Z)V

    .line 6625
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6626
    const/16 v2, 0x20

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 6627
    invoke-virtual {p2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setLongClickable(Z)V

    goto :goto_0

    .line 6617
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {p2, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    goto :goto_1
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 8
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 6633
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/AccessibilityDelegateCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 6680
    :goto_0
    return v4

    .line 6637
    :cond_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v6, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 6638
    .local v1, "position":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6641
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eq v1, v7, :cond_1

    if-nez v0, :cond_2

    :cond_1
    move v4, v5

    .line 6642
    goto :goto_0

    .line 6646
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    move v4, v5

    .line 6647
    goto :goto_0

    .line 6650
    :cond_4
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v6, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getItemIdAtPosition(I)J

    move-result-wide v2

    .line 6652
    .local v2, "id":J
    sparse-switch p2, :sswitch_data_0

    move v4, v5

    .line 6680
    goto :goto_0

    .line 6654
    :sswitch_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getSelectedItemPosition()I

    move-result v6

    if-ne v6, v1, :cond_5

    .line 6655
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setSelection(I)V

    goto :goto_0

    :cond_5
    move v4, v5

    .line 6658
    goto :goto_0

    .line 6661
    :sswitch_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getSelectedItemPosition()I

    move-result v6

    if-eq v6, v1, :cond_6

    .line 6662
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v5, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setSelection(I)V

    goto :goto_0

    :cond_6
    move v4, v5

    .line 6665
    goto :goto_0

    .line 6668
    :sswitch_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isClickable()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 6669
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4, p1, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v4

    goto :goto_0

    :cond_7
    move v4, v5

    .line 6671
    goto :goto_0

    .line 6674
    :sswitch_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isLongClickable()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 6675
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$ListItemAccessibilityDelegate;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->performLongPress(Landroid/view/View;IJ)Z
    invoke-static {v4, p1, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$4000(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Landroid/view/View;IJ)Z

    move-result v4

    goto :goto_0

    :cond_8
    move v4, v5

    .line 6677
    goto :goto_0

    .line 6652
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method
