.class public interface abstract Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$OnScrollListener;
.super Ljava/lang/Object;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnScrollListener"
.end annotation


# static fields
.field public static final SCROLL_STATE_FLING:I = 0x2

.field public static final SCROLL_STATE_IDLE:I = 0x0

.field public static final SCROLL_STATE_TOUCH_SCROLL:I = 0x1


# virtual methods
.method public abstract onScroll(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;III)V
.end method

.method public abstract onScrollStateChanged(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)V
.end method
