.class public interface abstract Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;
.super Ljava/lang/Object;
.source "ShowBounceViewListener.java"


# virtual methods
.method public abstract actionDown(II)V
.end method

.method public abstract actionMove(II)V
.end method

.method public abstract actionUp()V
.end method

.method public abstract dealMinZoom()V
.end method

.method public abstract endAllBounceView(Z)V
.end method

.method public abstract endSingleBounce(I)V
.end method

.method public abstract getIsTouching()Z
.end method

.method public abstract resetDirection()V
.end method

.method public abstract startAllBounceView()V
.end method

.method public abstract startBounce(I)V
.end method
