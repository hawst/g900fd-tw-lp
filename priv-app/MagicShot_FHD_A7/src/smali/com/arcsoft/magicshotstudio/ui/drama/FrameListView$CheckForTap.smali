.class final Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;
.super Ljava/lang/Object;
.source "FrameListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CheckForTap"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V
    .locals 0

    .prologue
    .line 6468
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;

    .prologue
    .line 6468
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 6471
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mTouchMode:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 6516
    :cond_0
    :goto_0
    return-void

    .line 6475
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mTouchMode:I
    invoke-static {v4, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$402(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)I

    .line 6477
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mMotionPosition:I
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mFirstPosition:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3300(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 6478
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v4

    if-nez v4, :cond_0

    .line 6479
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    const/4 v5, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mLayoutMode:I
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3502(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)I

    .line 6481
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mDataChanged:Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 6482
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setPressed(Z)V

    .line 6483
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 6485
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->layoutChildren()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3600(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    .line 6486
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mMotionPosition:I
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v5

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->positionSelector(ILandroid/view/View;)V
    invoke-static {v4, v5, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3700(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;ILandroid/view/View;)V

    .line 6487
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->refreshDrawableState()V

    .line 6489
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mMotionPosition:I
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v5

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->positionSelector(ILandroid/view/View;)V
    invoke-static {v4, v5, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3700(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;ILandroid/view/View;)V

    .line 6490
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->refreshDrawableState()V

    .line 6492
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->isLongClickable()Z

    move-result v2

    .line 6494
    .local v2, "longClickable":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3800(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 6495
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3800(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 6497
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_2

    instance-of v4, v1, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v4, :cond_2

    .line 6498
    if-eqz v2, :cond_3

    .line 6499
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v3

    .line 6500
    .local v3, "longPressTimeout":I
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 6507
    .end local v3    # "longPressTimeout":I
    :cond_2
    :goto_1
    if-eqz v2, :cond_4

    .line 6508
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->triggerCheckForLongPress()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3900(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    goto/16 :goto_0

    .line 6502
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    :cond_3
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1

    .line 6510
    :cond_4
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mTouchMode:I
    invoke-static {v4, v8}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$402(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)I

    goto/16 :goto_0

    .line 6513
    .end local v2    # "longClickable":Z
    :cond_5
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$CheckForTap;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mTouchMode:I
    invoke-static {v4, v8}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$402(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;I)I

    goto/16 :goto_0
.end method
