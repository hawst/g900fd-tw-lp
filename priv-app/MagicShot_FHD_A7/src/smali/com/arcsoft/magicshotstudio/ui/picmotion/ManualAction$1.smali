.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 252
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainLock:Ljava/lang/Object;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 253
    :try_start_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v7, 0x1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z
    invoke-static {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z

    .line 254
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mWitchCheckThreadHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 255
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainThreadHandler:Landroid/os/Handler;

    const v7, 0x9999

    invoke-virtual {v5, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 257
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "################### mSwithCheckRunnable run ##################"

    invoke-static {v5, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ProcessCheckedFrame()Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 259
    .local v2, "result":Landroid/graphics/Bitmap;
    if-nez v2, :cond_0

    .line 260
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "################### mSwithCheckRunnable ProcessCheckedFrame result ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    monitor-exit v6

    .line 277
    :goto_0
    return-void

    .line 264
    :cond_0
    new-instance v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;

    invoke-direct {v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;-><init>()V

    .line 265
    .local v3, "returnMask":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v5

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v7

    const/4 v8, 0x0

    aget-byte v7, v7, v8

    invoke-virtual {v5, v7, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getObjectMask(ILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;)I

    move-result v4

    .line 266
    .local v4, "success":I
    if-eqz v4, :cond_1

    .line 267
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "################### mSwithCheckRunnable getShotMask failed"

    invoke-static {v5, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    monitor-exit v6

    goto :goto_0

    .line 276
    .end local v2    # "result":Landroid/graphics/Bitmap;
    .end local v3    # "returnMask":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;
    .end local v4    # "success":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 271
    .restart local v2    # "result":Landroid/graphics/Bitmap;
    .restart local v3    # "returnMask":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;
    .restart local v4    # "success":I
    :cond_1
    :try_start_1
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v7, v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;->mMask:[B

    invoke-direct {v0, v5, v7, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;[BLandroid/graphics/Bitmap;)V

    .line 272
    .local v0, "motionMask":Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 273
    .local v1, "msg":Landroid/os/Message;
    const v5, 0x9999

    iput v5, v1, Landroid/os/Message;->what:I

    .line 274
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 275
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainThreadHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x64

    invoke-virtual {v5, v1, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 276
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
