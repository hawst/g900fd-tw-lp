.class Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;
.super Ljava/lang/Object;
.source "ArcPicBestUILogic.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 0

    .prologue
    .line 1612
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1615
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOperateEnable:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1400(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1630
    :cond_0
    :goto_0
    return v0

    .line 1618
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsTouchLocked:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 1620
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    .line 1621
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsTouchLocked:Z
    invoke-static {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1502(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Z)Z

    .line 1622
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1602(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;I)I

    .line 1625
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v0, v2, :cond_4

    .line 1626
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsTouchLocked:Z
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1502(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Z)Z

    .line 1627
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1602(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;I)I

    .line 1629
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurEvent:Landroid/view/MotionEvent;
    invoke-static {v0, p2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1702(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move v0, v1

    .line 1630
    goto :goto_0
.end method
