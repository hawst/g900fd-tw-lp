.class public Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
.super Ljava/lang/Object;
.source "ArcZoomController.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field public static final MAX_DOUBLE_TAP_FACTOR:F = 2.5f

.field private static final MAX_ZOOM:F = 3.0f

.field public static final MIN_DOUBLE_TAP_FACTOR:F = 1.0f

.field private static final MIN_ZOOM:F = 1.0f

.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_ArcZoomController"


# instance fields
.field private mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

.field private mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

.field private mDoubleTapZoomInOrOut:Z

.field private final mDoubleTapZoomRunnable:Ljava/lang/Runnable;

.field private mDoubleZoomFactor:F

.field private mDoubleZoomInXPosition:F

.field private mDoubleZoomInYPosition:F

.field private final mHandler:Landroid/os/Handler;

.field private mPanMaxX:F

.field private mPanMaxY:F

.field private mPanMinX:F

.field private mPanMinY:F

.field private mPointF:Landroid/graphics/PointF;

.field private final mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;


# direct methods
.method public constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 4
    .param p1, "uiLogic"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 22
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;

    .line 31
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomInOrOut:Z

    .line 312
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F

    .line 313
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F

    .line 314
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    .line 319
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    .line 371
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    .line 34
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomInOrOut:Z

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    .param p1, "x1"    # F

    .prologue
    .line 16
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    return p1
.end method

.method static synthetic access$216(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    .param p1, "x1"    # F

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$224(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    .param p1, "x1"    # F

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F

    return v0
.end method

.method static synthetic access$302(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    .param p1, "x1"    # F

    .prologue
    .line 16
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F

    return p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F

    return v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    .param p1, "x1"    # F

    .prologue
    .line 16
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F

    return p1
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private doubleTapZoomInWithAnimation()V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 309
    :cond_0
    return-void
.end method

.method private getMaxPanDelta(F)F
    .locals 3
    .param p1, "zoom"    # F

    .prologue
    .line 139
    const/4 v0, 0x0

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, p1, v2

    div-float/2addr v2, p1

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private limitPan()V
    .locals 12

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v9, 0x0

    .line 184
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v8, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    const/4 v1, 0x0

    .line 189
    .local v1, "currentDirection":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 191
    .local v0, "aspectQuotient":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v6

    .line 192
    .local v6, "zoomX":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v7

    .line 194
    .local v7, "zoomY":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    sub-float v4, v10, v8

    .line 195
    .local v4, "panMinX":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    add-float v2, v10, v8

    .line 196
    .local v2, "panMaxX":F
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    sub-float v5, v10, v8

    .line 197
    .local v5, "panMinY":F
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    add-float v3, v10, v8

    .line 199
    .local v3, "panMaxY":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v8

    cmpg-float v8, v8, v4

    if-gez v8, :cond_2

    .line 200
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 201
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    .line 202
    or-int/lit8 v1, v1, 0x1

    .line 205
    :cond_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v8

    cmpl-float v8, v8, v2

    if-lez v8, :cond_3

    .line 206
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 207
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_3

    .line 208
    or-int/lit8 v1, v1, 0x4

    .line 211
    :cond_3
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v8

    cmpg-float v8, v8, v5

    if-gez v8, :cond_4

    .line 212
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 213
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_4

    .line 214
    or-int/lit8 v1, v1, 0x2

    .line 217
    :cond_4
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v8

    cmpl-float v8, v8, v3

    if-lez v8, :cond_5

    .line 218
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 219
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v8

    cmpl-float v8, v8, v9

    if-lez v8, :cond_5

    .line 220
    or-int/lit8 v1, v1, 0x8

    .line 223
    :cond_5
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v8

    cmpl-float v8, v11, v8

    if-gtz v8, :cond_6

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v8

    cmpg-float v8, v11, v8

    if-gez v8, :cond_0

    .line 224
    :cond_6
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v8, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setEffectDirection(I)V

    goto/16 :goto_0
.end method

.method private limitPanWithoutEdgeEffect()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f000000    # 0.5f

    .line 229
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v7, :cond_1

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 235
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v5

    .line 236
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v6

    .line 238
    .local v6, "zoomY":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v7

    sub-float v3, v8, v7

    .line 239
    .local v3, "panMinX":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v7

    add-float v1, v8, v7

    .line 240
    .local v1, "panMaxX":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v7

    sub-float v4, v8, v7

    .line 241
    .local v4, "panMinY":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v7

    add-float v2, v8, v7

    .line 243
    .local v2, "panMaxY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v7

    cmpg-float v7, v7, v3

    if-gez v7, :cond_2

    .line 244
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 246
    :cond_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v7

    cmpl-float v7, v7, v1

    if-lez v7, :cond_3

    .line 247
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 249
    :cond_3
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v7

    cmpg-float v7, v7, v4

    if-gez v7, :cond_4

    .line 250
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 252
    :cond_4
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_0

    .line 253
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    goto :goto_0
.end method

.method private limitZoom()V
    .locals 4

    .prologue
    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 143
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 147
    :cond_0
    const/16 v0, 0xf

    .line 148
    .local v0, "currentDirection":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setEffectDirection(I)V

    .line 150
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setZoom(F)V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    .line 154
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setZoom(F)V

    .line 156
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->stopZoomEdgeEffect()V

    goto :goto_0
.end method

.method private limitZoomWithoutEdgeEffect()V
    .locals 3

    .prologue
    const/high16 v2, 0x40400000    # 3.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 259
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 264
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setZoom(F)V

    goto :goto_0

    .line 267
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setZoom(F)V

    goto :goto_0
.end method

.method private updatePanLimits()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 168
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v3, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 174
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v1

    .line 175
    .local v1, "zoomX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v2

    .line 177
    .local v2, "zoomY":F
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v3

    sub-float v3, v4, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPanMinX:F

    .line 178
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v3

    add-float/2addr v3, v4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPanMaxX:F

    .line 179
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v3

    sub-float v3, v4, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPanMinY:F

    .line 180
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getMaxPanDelta(F)F

    move-result v3

    add-float/2addr v3, v4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPanMaxY:F

    goto :goto_0
.end method


# virtual methods
.method public doubleTapZoom(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 289
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomInOrOut:Z

    .line 299
    :goto_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F

    .line 300
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F

    .line 301
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F

    .line 302
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->doubleTapZoomInWithAnimation()V

    goto :goto_0

    .line 296
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomInOrOut:Z

    goto :goto_1
.end method

.method public getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    return-object v0
.end method

.method public pan(FF)V
    .locals 4
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 97
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v3, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 103
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v3

    div-float/2addr p1, v3

    .line 104
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v3

    div-float/2addr p2, v3

    .line 106
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v3

    add-float v1, v3, p1

    .line 107
    .local v1, "newPanX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v3

    add-float v2, v3, p2

    .line 109
    .local v2, "newPanY":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 110
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 111
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setEffectOffset_X(F)V

    .line 112
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, p2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setEffectOffset_Y(F)V

    .line 114
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->limitPan()V

    .line 115
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->notifyObservers()V

    goto :goto_0
.end method

.method public panWithoutEdgeEffect(FF)V
    .locals 4
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 119
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v3, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 125
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v3

    div-float/2addr p1, v3

    .line 126
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v3

    div-float/2addr p2, v3

    .line 128
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v3

    add-float v1, v3, p1

    .line 129
    .local v1, "newPanX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v3

    add-float v2, v3, p2

    .line 131
    .local v2, "newPanY":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 132
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 134
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->limitPanWithoutEdgeEffect()V

    .line 135
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->notifyObservers()V

    goto :goto_0
.end method

.method public setAspectQuotient(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;)V
    .locals 1
    .param p1, "aspectQuotient"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->deleteObserver(Ljava/util/Observer;)V

    .line 42
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    .line 43
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->addObserver(Ljava/util/Observer;)V

    .line 44
    return-void
.end method

.method public setEdgeEffectEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 274
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v0, :cond_0

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setEdgeEffectEnabled(Z)V

    .line 279
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->notifyObservers()V

    goto :goto_0
.end method

.method public stopDoubleTapZoomRunnable()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 369
    :cond_0
    return-void
.end method

.method public stopZoomEdgeEffect()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->stopZoomEdgeEffect()V

    .line 165
    :cond_0
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->limitZoom()V

    .line 285
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->limitPanWithoutEdgeEffect()V

    .line 286
    return-void
.end method

.method public updatePan(Landroid/view/MotionEvent;II)V
    .locals 12
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    .line 375
    if-eqz p1, :cond_0

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    if-nez v9, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v9

    const/high16 v10, 0x3f800000    # 1.0f

    cmpl-float v9, v9, v10

    if-lez v9, :cond_2

    .line 380
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getZoomOutPoint()Landroid/graphics/Point;

    move-result-object v8

    .line 381
    .local v8, "zoomOutPoint":Landroid/graphics/Point;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    iget v10, v8, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    int-to-float v11, p2

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 382
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    iget v10, v8, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    int-to-float v11, p3

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 384
    .end local v8    # "zoomOutPoint":Landroid/graphics/Point;
    :cond_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->updateZoomState()V

    .line 385
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getMaxWidth()I

    move-result v1

    .line 386
    .local v1, "maxWidth":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getMaxHeight()I

    move-result v0

    .line 387
    .local v0, "maxHeight":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->updateZoomInPointX()I

    move-result v6

    .line 388
    .local v6, "zoomOffsetX":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->updateZoomInPointY()I

    move-result v7

    .line 389
    .local v7, "zoomOffsetY":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getRectDst()Landroid/graphics/Rect;

    move-result-object v5

    .line 390
    .local v5, "rectDst":Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, p2, v9

    div-int/lit8 v2, v9, 0x2

    .line 391
    .local v2, "offsetX":I
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v9, p3, v9

    div-int/lit8 v3, v9, 0x2

    .line 393
    .local v3, "offsetY":I
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 394
    .local v4, "pointAfterAdjust":Landroid/graphics/Point;
    if-gt v1, p2, :cond_3

    .line 395
    div-int/lit8 v9, p2, 0x2

    iput v9, v4, Landroid/graphics/Point;->x:I

    .line 404
    :goto_1
    if-gt v0, p3, :cond_6

    .line 405
    div-int/lit8 v9, p3, 0x2

    iput v9, v4, Landroid/graphics/Point;->y:I

    .line 414
    :goto_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    iget v10, v4, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    int-to-float v11, p2

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 415
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mPointF:Landroid/graphics/PointF;

    iget v10, v4, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    int-to-float v11, p3

    div-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 396
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    add-int v10, v2, v6

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    .line 397
    add-int v9, v2, v6

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 398
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    sub-int v10, p2, v2

    sub-int/2addr v10, v6

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_5

    .line 399
    sub-int v9, p2, v2

    sub-int/2addr v9, v6

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 401
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v9, v9

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 406
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    add-int v10, v3, v7

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_7

    .line 407
    add-int v9, v3, v7

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2

    .line 408
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    sub-int v10, p3, v3

    sub-int/2addr v10, v7

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_8

    .line 409
    sub-int v9, p3, v3

    sub-int/2addr v9, v7

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2

    .line 411
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public zoom(FFF)V
    .locals 12
    .param p1, "f"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 51
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v5, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 57
    .local v0, "aspectQuotient":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v3

    .line 58
    .local v3, "prevZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v4

    .line 60
    .local v4, "prevZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v6

    mul-float/2addr v6, p1

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setZoom(F)V

    .line 61
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->limitZoom()V

    .line 63
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v1

    .line 64
    .local v1, "newZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v2

    .line 67
    .local v2, "newZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v6

    sub-float v7, p2, v11

    div-float v8, v10, v3

    div-float v9, v10, v1

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 68
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v6

    sub-float v7, p3, v11

    div-float v8, v10, v4

    div-float v9, v10, v2

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 70
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->notifyObservers()V

    goto :goto_0
.end method

.method public zoomWithoutEdgeEffect(FFF)V
    .locals 12
    .param p1, "f"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 74
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-nez v5, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 80
    .local v0, "aspectQuotient":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v3

    .line 81
    .local v3, "prevZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v4

    .line 83
    .local v4, "prevZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setZoom(F)V

    .line 84
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->limitZoomWithoutEdgeEffect()V

    .line 86
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v1

    .line 87
    .local v1, "newZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v2

    .line 90
    .local v2, "newZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v6

    sub-float v7, p2, v11

    div-float v8, v10, v3

    div-float v9, v10, v1

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 91
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v6

    sub-float v7, p3, v11

    div-float v8, v10, v4

    div-float v9, v10, v2

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 93
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->notifyObservers()V

    goto :goto_0
.end method
