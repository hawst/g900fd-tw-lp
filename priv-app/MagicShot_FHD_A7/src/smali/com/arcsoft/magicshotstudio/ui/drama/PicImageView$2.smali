.class Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;
.super Ljava/lang/Object;
.source "PicImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->doMask([BZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

.field final synthetic val$grey:[B

.field final synthetic val$isBrush:Z


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;[BZ)V
    .locals 0

    .prologue
    .line 813
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->val$grey:[B

    iput-boolean p3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->val$isBrush:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 817
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->val$grey:[B

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->access$600(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)I

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->access$700(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)I

    move-result v3

    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->val$isBrush:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->processManualMask([BIIZ)Z

    .line 818
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchLock:Z

    .line 819
    return-void
.end method
