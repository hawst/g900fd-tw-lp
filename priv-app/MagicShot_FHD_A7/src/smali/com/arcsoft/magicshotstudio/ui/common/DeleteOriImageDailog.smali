.class public Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;
.super Ljava/lang/Object;
.source "DeleteOriImageDailog.java"


# static fields
.field public static final BUTTON_NO:I = -0x3

.field public static final BUTTON_YES:I = -0x1

.field public static mMessHeight:I


# instance fields
.field private mBuilder:Landroid/app/AlertDialog$Builder;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDailog:Landroid/app/AlertDialog;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mHandler:Landroid/os/Handler;

.field private mMessageLayout:Landroid/widget/LinearLayout;

.field private mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mTextView:Landroid/widget/TextView;

.field private mTextViewOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    sput v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    .line 33
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    .line 34
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    .line 35
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextView:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mSharedPref:Landroid/content/SharedPreferences;

    .line 37
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 57
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 69
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 78
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextViewOnClickListener:Landroid/view/View$OnClickListener;

    .line 47
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    .line 48
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->initSharedPreferences(Landroid/content/Context;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->initDailog(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    .line 33
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    .line 34
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    .line 35
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextView:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mSharedPref:Landroid/content/SharedPreferences;

    .line 37
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 57
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 69
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 78
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextViewOnClickListener:Landroid/view/View$OnClickListener;

    .line 41
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    .line 42
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->initSharedPreferences(Landroid/content/Context;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->initDailog(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mSharedPref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessageLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private initDailog(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 93
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 94
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f03000a

    const/4 v12, 0x0

    invoke-virtual {v1, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 95
    .local v8, "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 96
    .local v0, "config":Landroid/content/res/Configuration;
    iget v9, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v12, 0x2

    if-ne v9, v12, :cond_2

    move v3, v10

    .line 97
    .local v3, "isLandscape":Z
    :goto_0
    const v9, 0x7f090060

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessageLayout:Landroid/widget/LinearLayout;

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f050092

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 99
    .local v4, "largeFontMesHeight":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f050093

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 100
    .local v5, "largeFontMesHeight_l":I
    sget v9, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    if-eqz v9, :cond_1

    .line 101
    sget v9, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    if-le v9, v4, :cond_3

    .line 102
    if-eqz v3, :cond_0

    .line 104
    sput v5, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    .line 110
    :cond_0
    :goto_1
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 111
    .local v6, "param":Landroid/widget/RelativeLayout$LayoutParams;
    sget v9, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    iput v9, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 112
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessageLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    .end local v6    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const v9, 0x7f090063

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/CheckBox;

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    .line 116
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v9, v12}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 118
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v12}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const-string v9, "MAGICSHOT_SPF_Key"

    invoke-virtual {p1, v9, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 122
    .local v7, "sharedPref":Landroid/content/SharedPreferences;
    const-string v9, "TYPE_DELETE_DAILOG_NEED_SHOW"

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 123
    .local v2, "isChecked":Z
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    if-nez v2, :cond_4

    :goto_2
    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 125
    const v9, 0x7f090064

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextView:Landroid/widget/TextView;

    .line 126
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextView:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mTextViewOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v9, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 129
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    const v10, 0x7f060050

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 131
    return-void

    .end local v2    # "isChecked":Z
    .end local v3    # "isLandscape":Z
    .end local v4    # "largeFontMesHeight":I
    .end local v5    # "largeFontMesHeight_l":I
    .end local v7    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_2
    move v3, v11

    .line 96
    goto/16 :goto_0

    .line 107
    .restart local v3    # "isLandscape":Z
    .restart local v4    # "largeFontMesHeight":I
    .restart local v5    # "largeFontMesHeight_l":I
    :cond_3
    const/4 v9, -0x2

    sput v9, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessHeight:I

    goto :goto_1

    .restart local v2    # "isChecked":Z
    .restart local v7    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_4
    move v10, v11

    .line 123
    goto :goto_2
.end method

.method private initSharedPreferences(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-string v0, "MAGICSHOT_SPF_Key"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mSharedPref:Landroid/content/SharedPreferences;

    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 55
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 148
    :cond_0
    return-void
.end method

.method public getHeightForDialog()V
    .locals 4

    .prologue
    .line 168
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->showDialog(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 169
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessageLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mMessageLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$4;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    .line 184
    :cond_1
    return-void
.end method

.method public isCheckBoxChecked()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 163
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 142
    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mHandler:Landroid/os/Handler;

    .line 189
    return-void
.end method

.method public showDialog(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V
    .locals 2
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060014

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 135
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06002d

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 137
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mDailog:Landroid/app/AlertDialog;

    .line 138
    return-void
.end method
