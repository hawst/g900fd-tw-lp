.class Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;
.super Ljava/lang/Object;
.source "PicturesBestEngine.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->init(IIIIJJ[IIJJ)J
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProcessUpdateListener(I)V
    .locals 2
    .param p1, "process"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$402(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 141
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 142
    return-void
.end method

.method public onShowFirstImageListener(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFirstImage:Landroid/graphics/Bitmap;
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$502(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 146
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 147
    return-void
.end method
