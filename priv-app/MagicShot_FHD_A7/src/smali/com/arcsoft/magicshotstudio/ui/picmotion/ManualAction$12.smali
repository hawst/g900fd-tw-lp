.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 1913
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 18
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1918
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1919
    .local v8, "currTime":J
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-wide v14, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLastClickTime:J

    sub-long v14, v8, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-wide v0, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mClickInterval:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-gez v13, :cond_0

    .line 1920
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "mClickInterval < 400"

    invoke-static {v13, v14}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1982
    :goto_0
    return-void

    .line 1924
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iput-wide v8, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLastClickTime:J

    .line 1926
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    move/from16 v0, p3

    if-gt v13, v0, :cond_1

    .line 1927
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "error occured,mFrameList.size() <= position"

    invoke-static {v13, v14}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1930
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v14, 0x0

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v13, v14}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V

    .line 1931
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    move/from16 v0, p3

    if-ne v0, v13, :cond_2

    .line 1932
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "same position"

    invoke-static {v13, v14}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1936
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v14

    sub-int v5, v13, v14

    .line 1937
    .local v5, "childIndex":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1938
    .local v2, "PreView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v13

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 1939
    .local v11, "res":Landroid/content/res/Resources;
    if-eqz v2, :cond_3

    .line 1941
    const v13, 0x7f090063

    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 1942
    .local v4, "checkboxPre":Landroid/widget/CheckBox;
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1943
    const v13, 0x7f060055

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1944
    .local v12, "uck":Ljava/lang/String;
    invoke-virtual {v2, v12}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1948
    .end local v4    # "checkboxPre":Landroid/widget/CheckBox;
    .end local v12    # "uck":Ljava/lang/String;
    :cond_3
    const v13, 0x7f090063

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 1949
    .local v3, "checkbox":Landroid/widget/CheckBox;
    const/4 v13, 0x1

    invoke-virtual {v3, v13}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1950
    const v13, 0x7f060054

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1951
    .local v6, "ck":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1953
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v14, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    monitor-enter v14

    .line 1954
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget v15, v15, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    .line 1955
    .local v10, "predata":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    const/4 v13, 0x0

    iput-boolean v13, v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    .line 1956
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget v15, v15, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    invoke-virtual {v13, v15, v10}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1958
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    .line 1959
    .local v7, "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    const/4 v13, 0x1

    iput-boolean v13, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    .line 1960
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-virtual {v13, v0, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1961
    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1962
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    move/from16 v0, p3

    iput v0, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    .line 1963
    const/4 v13, 0x2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;
    invoke-static {v14}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$5100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    move-result-object v14

    iget v14, v14, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    if-eq v13, v14, :cond_4

    const/4 v13, 0x3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;
    invoke-static {v14}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$5100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    move-result-object v14

    iget v14, v14, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    if-ne v13, v14, :cond_5

    .line 1965
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v15, v15, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    sub-int v15, v15, p3

    int-to-byte v15, v15

    aput-byte v15, v13, v14

    .line 1970
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkEditable()V
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$5200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1971
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initCheckedFrameList()V
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$5300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 1973
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "################### mWitchCheckThreadHandler post ##################"

    invoke-static {v13, v14}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1980
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v13

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->requestCancelAuto()I

    .line 1981
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v13, v13, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mWitchCheckThreadHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v14, v14, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v13, v14}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1961
    .end local v7    # "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    .end local v10    # "predata":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    :catchall_0
    move-exception v13

    :try_start_1
    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v13

    .line 1967
    .restart local v7    # "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    .restart local v10    # "predata":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B
    invoke-static {v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v13

    const/4 v14, 0x0

    move/from16 v0, p3

    int-to-byte v15, v0

    aput-byte v15, v13, v14

    goto :goto_1
.end method
