.class public Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;
.super Ljava/util/Observable;
.source "ZoomState.java"


# static fields
.field public static final ZOOM_EDGE_EFFECT_DIRECTION:I = 0xf


# instance fields
.field private mEdgeEffectEnabled:Z

.field private mEffectDirection:I

.field private mEffectOffset_X:F

.field private mEffectOffset_Y:F

.field private mPanX:F

.field private mPanY:F

.field private mZoom:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 6
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    .line 8
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanX:F

    .line 10
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanY:F

    .line 53
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectOffset_X:F

    .line 54
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectOffset_Y:F

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectDirection:I

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEdgeEffectEnabled:Z

    return-void
.end method


# virtual methods
.method public getEdgeEffectEnabled()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEdgeEffectEnabled:Z

    return v0
.end method

.method public getEffectDirection()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectDirection:I

    return v0
.end method

.method public getEffectOffset_X()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectOffset_X:F

    return v0
.end method

.method public getEffectOffset_Y()F
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectOffset_Y:F

    return v0
.end method

.method public getPanX()F
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanX:F

    return v0
.end method

.method public getPanY()F
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanY:F

    return v0
.end method

.method public getZoom()F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    return v0
.end method

.method public getZoomX(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 25
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    mul-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getZoomY(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 29
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    div-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public setEdgeEffectEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEdgeEffectEnabled:Z

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectDirection:I

    .line 65
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setChanged()V

    .line 66
    return-void
.end method

.method public setEffectDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectDirection:I

    .line 75
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setChanged()V

    .line 76
    return-void
.end method

.method public setEffectOffset_X(F)V
    .locals 0
    .param p1, "dx"    # F

    .prologue
    .line 94
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectOffset_X:F

    .line 95
    return-void
.end method

.method public setEffectOffset_Y(F)V
    .locals 0
    .param p1, "dy"    # F

    .prologue
    .line 102
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectOffset_Y:F

    .line 103
    return-void
.end method

.method public setPanX(F)V
    .locals 1
    .param p1, "panX"    # F

    .prologue
    .line 33
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanX:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 34
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanX:F

    .line 35
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setChanged()V

    .line 37
    :cond_0
    return-void
.end method

.method public setPanY(F)V
    .locals 1
    .param p1, "panY"    # F

    .prologue
    .line 40
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanY:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 41
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mPanY:F

    .line 42
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setChanged()V

    .line 44
    :cond_0
    return-void
.end method

.method public setZoom(F)V
    .locals 1
    .param p1, "zoom"    # F

    .prologue
    .line 47
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 48
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mZoom:F

    .line 49
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setChanged()V

    .line 51
    :cond_0
    return-void
.end method

.method public stopPanEdgeEffect(I)V
    .locals 0
    .param p1, "side"    # I

    .prologue
    .line 87
    return-void
.end method

.method public stopZoomEdgeEffect()V
    .locals 2

    .prologue
    .line 79
    const/16 v0, 0xf

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectDirection:I

    if-ne v0, v1, :cond_0

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->mEffectDirection:I

    .line 82
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setChanged()V

    .line 84
    :cond_0
    return-void
.end method
