.class Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;
.super Landroid/os/Handler;
.source "PicturesBestEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 18
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 70
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 72
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onProcessImgStart()V

    goto :goto_0

    .line 76
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBFPerformanceData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    move-result-object v3

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;->mBFPerformance:[I

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->showBestFacePerformance([I)V

    .line 80
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onProcessImgFinish()V

    goto :goto_0

    .line 83
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onProcessImgNoFace()V

    goto :goto_0

    .line 86
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onPicEditStart()V

    goto :goto_0

    .line 89
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onPicEditFinish()V

    goto :goto_0

    .line 92
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onSaveDirect()V

    goto :goto_0

    .line 95
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onSaveMergeResult()V

    goto :goto_0

    .line 98
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onSetProcessBar(I)V

    goto/16 :goto_0

    .line 101
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFirstImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onShowFirstImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 105
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-result-object v2

    sget-object v3, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SELECT_TIME:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;->mSelectPerformance:[I

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->showSelectPerformance([I)V

    .line 110
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;->onSetProcessBar(I)V

    .line 111
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcWidth:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcHeight:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mScreenWidth:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mThumbWidth:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBitmapPtr:J
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPreProcessData:J
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBestIndex:[I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1200(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)[I

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mDegree:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v13

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    invoke-virtual/range {v3 .. v17}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->init(IIIIJJ[IIJJ)J

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->StartProcess()V

    goto/16 :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
