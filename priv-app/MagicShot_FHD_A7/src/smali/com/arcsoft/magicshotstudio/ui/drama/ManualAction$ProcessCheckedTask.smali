.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;
.super Landroid/os/AsyncTask;
.source "ManualAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessCheckedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 2097
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2100
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 2101
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ProcessCheckedFrame()Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2102
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 2103
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2097
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2108
    if-eqz p1, :cond_0

    .line 2109
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1702(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2110
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 2112
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2113
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2097
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
