.class public Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;
.super Ljava/lang/Object;
.source "PicImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TouchEventHelper"
.end annotation


# static fields
.field private static final TOUCH_SLOP:I = 0x32


# instance fields
.field private mLastMotionX:I

.field private mLastMotionY:I

.field private mLongPressRunnable:Ljava/lang/Runnable;

.field private mTapRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;


# direct methods
.method public constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 560
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561
    iput-boolean v0, p1, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    .line 562
    iput-boolean v0, p1, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    .line 563
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLongPressRunnable:Ljava/lang/Runnable;

    .line 570
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper$2;

    invoke-direct {v0, p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mTapRunnable:Ljava/lang/Runnable;

    .line 577
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x32

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 580
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 581
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 583
    .local v1, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 613
    :cond_0
    :goto_0
    return v6

    .line 585
    :pswitch_0
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLastMotionX:I

    .line 586
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLastMotionY:I

    .line 587
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v3, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    .line 588
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v3, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    .line 589
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 590
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mTapRunnable:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 591
    const-string v2, "ArcSoft_PicImageView"

    const-string v3, "dispatchTouchEvent down"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 594
    :pswitch_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-boolean v2, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    if-nez v2, :cond_0

    .line 595
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLastMotionX:I

    sub-int/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-gt v2, v4, :cond_1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLastMotionY:I

    sub-int/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v4, :cond_0

    .line 597
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v6, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    .line 598
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v3, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    .line 599
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v3, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTap:Z

    .line 600
    const-string v2, "ArcSoft_PicImageView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_MOVE mMoved: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mLongPressed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 602
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mTapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 603
    const-string v2, "ArcSoft_PicImageView"

    const-string v3, "dispatchTouchEvent MOVE"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 607
    :pswitch_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 608
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->mTapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 609
    const-string v2, "ArcSoft_PicImageView"

    const-string v3, "dispatchTouchEvent up"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v6, v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTap:Z

    goto/16 :goto_0

    .line 583
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
