.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1691
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1696
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mGreyFramebarOnItemClickListener onItemClick position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1723
    :goto_0
    return-void

    .line 1703
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v3, p3, :cond_1

    .line 1704
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "error occured,mFrameList.size() <= position"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1708
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v3

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1709
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->getActivatedPos()I

    move-result v3

    if-ne v3, p3, :cond_2

    .line 1710
    const v3, 0x7f060055

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1711
    .local v2, "uck":Ljava/lang/String;
    invoke-virtual {p2, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717
    .end local v2    # "uck":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iput-object v4, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    .line 1718
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getMaskIndexFromCheckedPosition(I)I
    invoke-static {v4, p3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I

    move-result v4

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I
    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2902(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I

    .line 1719
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setItemActivated(Landroid/view/View;I)V
    invoke-static {v3, p2, p3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$3100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Landroid/view/View;I)V

    .line 1721
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mPutShotTop:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;

    invoke-virtual {p2, v3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1722
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mPutShotTop:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;

    invoke-virtual {p2, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1713
    :cond_2
    const v3, 0x7f060054

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1714
    .local v0, "ck":Ljava/lang/String;
    invoke-virtual {p2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
