.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;
.super Ljava/util/Observable;
.source "ArcAspectQuotient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_AspectQuotient"


# instance fields
.field private mAspectQuotient:F

.field private mRotateScreenAspectQuotient:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 8
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mAspectQuotient:F

    .line 9
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mRotateScreenAspectQuotient:F

    return-void
.end method


# virtual methods
.method public get()F
    .locals 1

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mAspectQuotient:F

    return v0
.end method

.method public getRotateScreenAspectQuotient()F
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mRotateScreenAspectQuotient:F

    return v0
.end method

.method public updateAspectQuotient(FFFF)V
    .locals 3
    .param p1, "viewWidth"    # F
    .param p2, "viewHeight"    # F
    .param p3, "contentWidth"    # F
    .param p4, "contentHeight"    # F

    .prologue
    .line 21
    div-float v1, p3, p4

    div-float v2, p1, p2

    div-float v0, v1, v2

    .line 23
    .local v0, "aspectQuotient":F
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mAspectQuotient:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 24
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mAspectQuotient:F

    .line 25
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->updateRotateScreenAspectQuotient(FFFF)V

    .line 27
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->setChanged()V

    .line 29
    :cond_0
    return-void
.end method

.method public updateRotateScreenAspectQuotient(FFFF)V
    .locals 3
    .param p1, "viewWidth"    # F
    .param p2, "viewHeight"    # F
    .param p3, "contentWidth"    # F
    .param p4, "contentHeight"    # F

    .prologue
    .line 33
    div-float v1, p3, p4

    div-float v2, p2, p1

    div-float v0, v1, v2

    .line 35
    .local v0, "aspectQuotient":F
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mRotateScreenAspectQuotient:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 36
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->mRotateScreenAspectQuotient:F

    .line 38
    :cond_0
    return-void
.end method
