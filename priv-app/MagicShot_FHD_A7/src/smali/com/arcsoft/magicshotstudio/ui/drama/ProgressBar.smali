.class public Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;
.super Landroid/widget/ImageView;
.source "ProgressBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;
    }
.end annotation


# static fields
.field public static final DIRECTION_LEFT:I = 0x1

.field public static final DIRECTION_NONE:I = 0x0

.field public static final DIRECTION_RIGHT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PanoProgressBar"


# instance fields
.field private final mBackgroundPaint:Landroid/graphics/Paint;

.field private mDirection:I

.field private final mDoneAreaPaint:Landroid/graphics/Paint;

.field private mDrawBounds:Landroid/graphics/RectF;

.field private final mDstMask:Landroid/graphics/Rect;

.field private mHeight:F

.field private mIndicatorBitmap:Landroid/graphics/Bitmap;

.field private final mIndicatorPaint:Landroid/graphics/Paint;

.field private mIndicatorWidth:F

.field private mLeftMostProgress:F

.field private mListener:Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;

.field private mMaxProgress:F

.field private mProgress:F

.field private mProgressOffset:F

.field private mRightMostProgress:F

.field private final mSrcMask:Landroid/graphics/Rect;

.field private mWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0xff

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    .line 32
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mMaxProgress:F

    .line 33
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mLeftMostProgress:F

    .line 34
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mRightMostProgress:F

    .line 35
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgressOffset:F

    .line 36
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorWidth:F

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mBackgroundPaint:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDoneAreaPaint:Landroid/graphics/Paint;

    .line 40
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorPaint:Landroid/graphics/Paint;

    .line 44
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mListener:Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;

    .line 46
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorBitmap:Landroid/graphics/Bitmap;

    .line 47
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mSrcMask:Landroid/graphics/Rect;

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    .line 56
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDoneAreaPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDoneAreaPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 59
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 60
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 62
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 65
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDrawBounds:Landroid/graphics/RectF;

    .line 66
    return-void
.end method

.method private setDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 73
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    if-eq v0, p1, :cond_1

    .line 74
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    .line 75
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mListener:Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mListener:Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    invoke-interface {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;->onDirectionChange(I)V

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 80
    :cond_1
    return-void
.end method


# virtual methods
.method public getDirection()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 174
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    if-eqz v2, :cond_0

    .line 179
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mLeftMostProgress:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 180
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 181
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDrawBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 182
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDrawBounds:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 184
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mSrcMask:Landroid/graphics/Rect;

    iput v4, v2, Landroid/graphics/Rect;->left:I

    .line 185
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mSrcMask:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 186
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mSrcMask:Landroid/graphics/Rect;

    iput v4, v2, Landroid/graphics/Rect;->top:I

    .line 187
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mSrcMask:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 188
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mSrcMask:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDstMask:Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 193
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 194
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorWidth:F

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 195
    .local v0, "l":F
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    .line 204
    .end local v0    # "l":F
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 205
    return-void

    .line 197
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    .line 198
    .restart local v0    # "l":F
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorWidth:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v3, 0x0

    .line 107
    int-to-float v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    .line 108
    int-to-float v0, p2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mHeight:F

    .line 109
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDrawBounds:Landroid/graphics/RectF;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mHeight:F

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 110
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 164
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    .line 165
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgressOffset:F

    .line 166
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->setDirection(I)V

    .line 167
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 168
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 89
    return-void
.end method

.method public setDoneColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDoneAreaPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 94
    return-void
.end method

.method public setIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 99
    return-void
.end method

.method public setIndicatorWidth(F)V
    .locals 0
    .param p1, "w"    # F

    .prologue
    .line 117
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorWidth:F

    .line 118
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 119
    return-void
.end method

.method public setMaxProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 113
    int-to-float v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mMaxProgress:F

    .line 114
    return-void
.end method

.method public setOnDirectionChangeListener(Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mListener:Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar$OnDirectionChangeListener;

    .line 70
    return-void
.end method

.method public setProgress(I)V
    .locals 4
    .param p1, "progress"    # I

    .prologue
    const/4 v3, 0x1

    .line 139
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    if-nez v0, :cond_0

    .line 140
    if-le p1, v3, :cond_4

    .line 141
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->setRightIncreasing(Z)V

    .line 147
    :cond_0
    :goto_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    if-eqz v0, :cond_3

    .line 148
    int-to-float v0, p1

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mMaxProgress:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgressOffset:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    .line 150
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    const/4 v1, 0x0

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    .line 151
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 153
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mRightMostProgress:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mRightMostProgress:F

    .line 155
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mDirection:I

    if-ne v0, v3, :cond_2

    .line 157
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mLeftMostProgress:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgress:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mLeftMostProgress:F

    .line 159
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 161
    :cond_3
    return-void

    .line 142
    :cond_4
    const/4 v0, -0x1

    if-ge p1, v0, :cond_0

    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->setRightIncreasing(Z)V

    goto :goto_0
.end method

.method public setProgressBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mIndicatorBitmap:Landroid/graphics/Bitmap;

    .line 103
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 104
    return-void
.end method

.method public setRightIncreasing(Z)V
    .locals 1
    .param p1, "rightIncreasing"    # Z

    .prologue
    const/4 v0, 0x0

    .line 122
    if-eqz p1, :cond_0

    .line 123
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mLeftMostProgress:F

    .line 124
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mRightMostProgress:F

    .line 125
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgressOffset:F

    .line 126
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->setDirection(I)V

    .line 133
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->invalidate()V

    .line 134
    return-void

    .line 128
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mLeftMostProgress:F

    .line 129
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mRightMostProgress:F

    .line 130
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mWidth:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->mProgressOffset:F

    .line 131
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ProgressBar;->setDirection(I)V

    goto :goto_0
.end method
