.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;
.super Ljava/lang/Object;
.source "PhotoViewLayout.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDestRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getMaxSize()Lcom/arcsoft/magicshotstudio/utils/MSize;
    .locals 4

    .prologue
    const/high16 v3, 0x40200000    # 2.5f

    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "temp_x":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 151
    :cond_0
    const/4 v1, 0x0

    .line 152
    .local v1, "temp_y":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 153
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 156
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mMaxSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v2

    iput v0, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 157
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mMaxSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v2

    iput v1, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 159
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mMaxSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v2

    return-object v2
.end method

.method public getZoomOffsetPoint()Landroid/graphics/Point;
    .locals 6

    .prologue
    const v5, 0x3f2aaaab

    .line 164
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getWidth()I

    move-result v3

    .line 165
    .local v3, "viewWidth":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v3, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 167
    .local v0, "temp_x":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getHeight()I

    move-result v2

    .line 168
    .local v2, "viewHeight":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int v4, v2, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v4, v5

    float-to-int v1, v4

    .line 170
    .local v1, "temp_y":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOffsetPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 171
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOffsetPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v4

    return-object v4
.end method

.method public getZoomOutPoint()Landroid/graphics/Point;
    .locals 11

    .prologue
    .line 176
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v8

    iget v1, v8, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 177
    .local v1, "bitmapWidth":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v8

    iget v0, v8, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 178
    .local v0, "bitmapHeight":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getWidth()I

    move-result v5

    .line 179
    .local v5, "viewWidth":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getHeight()I

    move-result v4

    .line 180
    .local v4, "viewHeight":I
    if-le v5, v4, :cond_0

    .line 181
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getLandscapeFitInBitmapRect()Landroid/graphics/Rect;

    move-result-object v9

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$702(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 186
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->updateZoomState()V

    .line 188
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    mul-int/2addr v9, v1

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    div-int/2addr v9, v10

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$802(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;I)I

    .line 189
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int/2addr v9, v0

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    div-int/2addr v9, v10

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$902(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;I)I

    .line 191
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->getOffsetX()I

    move-result v8

    neg-int v2, v8

    .line 192
    .local v2, "offset_x":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->getOffsetY()I

    move-result v8

    neg-int v3, v8

    .line 194
    .local v3, "offset_y":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I

    move-result v10

    sub-int/2addr v9, v10

    mul-int/2addr v8, v9

    int-to-float v8, v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-int v6, v8

    .line 197
    .local v6, "x":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v3

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I

    move-result v10

    sub-int/2addr v9, v10

    mul-int/2addr v8, v9

    int-to-float v8, v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-int v7, v8

    .line 200
    .local v7, "y":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I

    move-result v8

    if-gt v8, v5, :cond_1

    .line 201
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v8

    div-int/lit8 v9, v5, 0x2

    iput v9, v8, Landroid/graphics/Point;->x:I

    .line 206
    :goto_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I

    move-result v8

    if-gt v8, v4, :cond_2

    .line 207
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v8

    div-int/lit8 v9, v4, 0x2

    iput v9, v8, Landroid/graphics/Point;->y:I

    .line 212
    :goto_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v8

    return-object v8

    .line 183
    .end local v2    # "offset_x":I
    .end local v3    # "offset_y":I
    .end local v6    # "x":I
    .end local v7    # "y":I
    :cond_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getPortraitFitInBitmapRect()Landroid/graphics/Rect;

    move-result-object v9

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$702(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    goto/16 :goto_0

    .line 204
    .restart local v2    # "offset_x":I
    .restart local v3    # "offset_y":I
    .restart local v6    # "x":I
    .restart local v7    # "y":I
    :cond_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v6

    iput v9, v8, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 210
    :cond_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v7

    iput v9, v8, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public startZoom()V
    .locals 2

    .prologue
    .line 217
    const-string v0, "xsj"

    const-string v1, "start Zoom layout"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;->startZoom()V

    .line 221
    :cond_0
    return-void
.end method

.method public stopZoom()V
    .locals 2

    .prologue
    .line 225
    const-string v0, "xsj"

    const-string v1, "stop Zoom layout"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$1100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;->stopZoom()V

    .line 229
    :cond_0
    return-void
.end method

.method public updateZoomState()V
    .locals 14

    .prologue
    const/high16 v13, 0x40000000    # 2.0f

    const/4 v12, 0x0

    .line 101
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    move-result-object v1

    .line 102
    .local v1, "mAspectQuotient":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;
    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 104
    .local v0, "aspectQuotient":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getWidth()I

    move-result v5

    .line 105
    .local v5, "viewWidth":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getHeight()I

    move-result v4

    .line 107
    .local v4, "viewHeight":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v2

    .line 108
    .local v2, "panX":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v3

    .line 109
    .local v3, "panY":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v8

    int-to-float v9, v5

    mul-float/2addr v8, v9

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v9, v9

    div-float v6, v8, v9

    .line 110
    .local v6, "zoomX":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v8

    int-to-float v9, v4

    mul-float/2addr v8, v9

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v9, v9

    div-float v7, v8, v9

    .line 113
    .local v7, "zoomY":F
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v9, v9

    mul-float/2addr v9, v2

    int-to-float v10, v5

    mul-float v11, v6, v13

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 114
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v9, v9

    mul-float/2addr v9, v3

    int-to-float v10, v4

    mul-float v11, v7, v13

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->top:I

    .line 115
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    int-to-float v10, v5

    div-float/2addr v10, v6

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 116
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v9

    iget v9, v9, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    int-to-float v10, v4

    div-float/2addr v10, v7

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->bottom:I

    .line 117
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iput v12, v8, Landroid/graphics/Rect;->left:I

    .line 118
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iput v12, v8, Landroid/graphics/Rect;->top:I

    .line 119
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iput v5, v8, Landroid/graphics/Rect;->right:I

    .line 120
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iput v4, v8, Landroid/graphics/Rect;->bottom:I

    .line 121
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->left:I

    if-gez v8, :cond_0

    .line 122
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v9, v8, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->left:I

    neg-int v10, v10

    int-to-float v10, v10

    mul-float/2addr v10, v6

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->left:I

    .line 123
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iput v12, v8, Landroid/graphics/Rect;->left:I

    .line 125
    :cond_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    if-le v8, v9, :cond_1

    .line 126
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v9, v8, Landroid/graphics/Rect;->right:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v11

    iget v11, v11, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    sub-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 127
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iput v9, v8, Landroid/graphics/Rect;->right:I

    .line 129
    :cond_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->top:I

    if-gez v8, :cond_2

    .line 130
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v9, v8, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->top:I

    neg-int v10, v10

    int-to-float v10, v10

    mul-float/2addr v10, v7

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->top:I

    .line 131
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iput v12, v8, Landroid/graphics/Rect;->top:I

    .line 133
    :cond_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    if-le v8, v9, :cond_3

    .line 134
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget v9, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v11}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v11

    iget v11, v11, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v7

    sub-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v8, Landroid/graphics/Rect;->bottom:I

    .line 135
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;

    move-result-object v8

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;
    invoke-static {v9}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v9

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    iput v9, v8, Landroid/graphics/Rect;->bottom:I

    .line 137
    :cond_3
    return-void
.end method
