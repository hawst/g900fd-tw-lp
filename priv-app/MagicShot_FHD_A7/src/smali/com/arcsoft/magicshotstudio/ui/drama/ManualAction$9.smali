.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;
.super Ljava/lang/Thread;
.source "ManualAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResultFile()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1570
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1576
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getSavePath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    .line 1578
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->createFile(Ljava/lang/String;)V

    .line 1579
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V

    .line 1581
    const/4 v1, 0x0

    .line 1582
    .local v1, "saveSucc":Z
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->saveFile(Ljava/lang/String;)I

    move-result v0

    .line 1583
    .local v0, "res":I
    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 1584
    :goto_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Drama.SaveResult saveSucc: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    if-eqz v1, :cond_0

    .line 1587
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    const/16 v5, 0x834

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget v6, v6, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    invoke-static {v3, v4, v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->refreshDBRecords(Landroid/content/Context;Ljava/lang/String;II)Z

    .line 1589
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iput-boolean v2, v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFileSaving:Z

    .line 1590
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1591
    return-void

    :cond_1
    move v1, v2

    .line 1583
    goto :goto_0
.end method
