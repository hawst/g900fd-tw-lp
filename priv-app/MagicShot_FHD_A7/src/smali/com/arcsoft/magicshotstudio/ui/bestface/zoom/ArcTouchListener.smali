.class public Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;
.super Ljava/lang/Object;
.source "ArcTouchListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;,
        Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_ArcTouchListener"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDownX:F

.field private mDownY:F

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mMidPoint:Landroid/graphics/PointF;

.field private mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

.field private mMultiPointX_1:F

.field private mMultiPointX_2:F

.field private mMultiPointY_1:F

.field private mMultiPointY_2:F

.field private mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

.field private volatile mOperateEnable:Z

.field private final mScaledMaximumFlingVelocity:I

.field private final mScaledTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mViewHeight:I

.field private mViewWidth:I

.field private mX:F

.field private mY:F

.field private mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

.field private oldDist:F

.field private panAfterPinchTimeout:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->UNDEFINED:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    .line 31
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMidPoint:Landroid/graphics/PointF;

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->oldDist:F

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->panAfterPinchTimeout:J

    .line 40
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mContext:Landroid/content/Context;

    .line 41
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    .line 56
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I

    .line 57
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mOperateEnable:Z

    .line 44
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    .line 46
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mScaledTouchSlop:I

    .line 47
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mScaledMaximumFlingVelocity:I

    .line 49
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->setGestureScanner()V

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I

    return v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    .prologue
    .line 16
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I

    return v0
.end method

.method private midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 275
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float v0, v2, v3

    .line 276
    .local v0, "x":F
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float v1, v2, v3

    .line 277
    .local v1, "y":F
    div-float v2, v0, v4

    div-float v3, v1, v4

    invoke-virtual {p1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 278
    return-void
.end method

.method private setGestureScanner()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 74
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 102
    return-void
.end method

.method private spacing(Landroid/view/MotionEvent;)F
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 269
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    .line 270
    .local v0, "x":F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    .line 271
    .local v1, "y":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    return v2
.end method


# virtual methods
.method public getOperateEnable()Z
    .locals 1

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mOperateEnable:Z

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 131
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 112
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 116
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    invoke-interface {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;->onSingleClick(Landroid/view/MotionEvent;)V

    .line 119
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 28
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 140
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v23

    move/from16 v0, v23

    and-int/lit16 v4, v0, 0xff

    .line 141
    .local v4, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v15

    .line 142
    .local v15, "x":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v19

    .line 144
    .local v19, "y":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I

    .line 145
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v23, v0

    if-nez v23, :cond_0

    .line 148
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 150
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 154
    packed-switch v4, :pswitch_data_0

    .line 260
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/VelocityTracker;->recycle()V

    .line 261
    const/16 v23, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 262
    sget-object v23, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->UNDEFINED:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    .line 265
    :cond_1
    :goto_0
    const/16 v23, 0x1

    return v23

    .line 156
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->setEdgeEffectEnabled(Z)V

    .line 157
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mDownX:F

    .line 158
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mDownY:F

    .line 159
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mX:F

    .line 160
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mY:F

    goto :goto_0

    .line 163
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_1

    .line 164
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->spacing(Landroid/view/MotionEvent;)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->oldDist:F

    .line 167
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointX_1:F

    .line 168
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointY_1:F

    .line 169
    const/16 v23, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointX_2:F

    .line 170
    const/16 v23, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointY_2:F

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMidPoint:Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 175
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->setEdgeEffectEnabled(Z)V

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v23, v0

    sget-object v24, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PAN:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_5

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 178
    .local v10, "now":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->panAfterPinchTimeout:J

    move-wide/from16 v24, v0

    cmp-long v23, v24, v10

    if-gez v23, :cond_2

    .line 190
    .end local v10    # "now":J
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/VelocityTracker;->recycle()V

    .line 191
    const/16 v23, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 193
    :pswitch_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_3

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->stopZoomEdgeEffect()V

    .line 196
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v23, v0

    sget-object v24, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PINCHZOOM:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_4

    .line 197
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    const-wide/16 v26, 0x64

    add-long v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->panAfterPinchTimeout:J

    .line 199
    :cond_4
    sget-object v23, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->UNDEFINED:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    goto/16 :goto_0

    .line 187
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v23, v0

    sget-object v24, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PINCHZOOM:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_2

    goto :goto_1

    .line 202
    :pswitch_5
    const/16 v23, 0x1

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_9

    .line 203
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mX:F

    move/from16 v23, v0

    sub-float v23, v15, v23

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v7, v23, v24

    .line 204
    .local v7, "dx":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mY:F

    move/from16 v23, v0

    sub-float v23, v19, v23

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v8, v23, v24

    .line 206
    .local v8, "dy":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v23, v0

    sget-object v24, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PAN:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_7

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    neg-float v0, v7

    move/from16 v24, v0

    neg-float v0, v8

    move/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->pan(FF)V

    .line 228
    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mX:F

    .line 229
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mY:F

    goto/16 :goto_0

    .line 208
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v23, v0

    sget-object v24, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PINCHZOOM:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_8

    .line 209
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->spacing(Landroid/view/MotionEvent;)F

    move-result v9

    .line 210
    .local v9, "newDist":F
    const/high16 v23, 0x41200000    # 10.0f

    cmpl-float v23, v9, v23

    if-lez v23, :cond_6

    .line 211
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->oldDist:F

    move/from16 v23, v0

    div-float v12, v9, v23

    .line 212
    .local v12, "scale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMidPoint:Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v23, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v18, v23, v24

    .line 213
    .local v18, "xx":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMidPoint:Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v23, v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v22, v23, v24

    .line 214
    .local v22, "yy":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v18

    move/from16 v2, v22

    invoke-virtual {v0, v12, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->zoom(FFF)V

    .line 215
    move-object/from16 v0, p0

    iput v9, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->oldDist:F

    goto :goto_2

    .line 218
    .end local v9    # "newDist":F
    .end local v12    # "scale":F
    .end local v18    # "xx":F
    .end local v22    # "yy":F
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mDownX:F

    move/from16 v23, v0

    sub-float v13, v23, v15

    .line 219
    .local v13, "scrollX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mDownY:F

    move/from16 v23, v0

    sub-float v14, v23, v19

    .line 221
    .local v14, "scrollY":F
    mul-float v23, v13, v13

    mul-float v24, v14, v14

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v5, v0

    .line 224
    .local v5, "dist":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mScaledTouchSlop:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    cmpl-float v23, v5, v23

    if-ltz v23, :cond_6

    .line 225
    sget-object v23, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PAN:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    goto/16 :goto_2

    .line 230
    .end local v5    # "dist":F
    .end local v7    # "dx":F
    .end local v8    # "dy":F
    .end local v13    # "scrollX":F
    .end local v14    # "scrollY":F
    :cond_9
    const/16 v23, 0x2

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1

    .line 231
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->spacing(Landroid/view/MotionEvent;)F

    move-result v6

    .line 232
    .local v6, "distance":F
    sget-object v23, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PAN:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    .line 233
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v16

    .line 234
    .local v16, "x1":F
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v20

    .line 235
    .local v20, "y1":F
    const/16 v23, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v17

    .line 236
    .local v17, "x2":F
    const/16 v23, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v21

    .line 238
    .local v21, "y2":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointX_1:F

    move/from16 v23, v0

    sub-float v23, v16, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointX_2:F

    move/from16 v24, v0

    sub-float v24, v17, v24

    add-float v23, v23, v24

    const/high16 v24, 0x40000000    # 2.0f

    div-float v23, v23, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v7, v23, v24

    .line 240
    .restart local v7    # "dx":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointY_1:F

    move/from16 v23, v0

    sub-float v23, v20, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointY_2:F

    move/from16 v24, v0

    sub-float v24, v21, v24

    add-float v23, v23, v24

    const/high16 v24, 0x40000000    # 2.0f

    div-float v23, v23, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v8, v23, v24

    .line 243
    .restart local v8    # "dy":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    if-eqz v23, :cond_a

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    neg-float v0, v7

    move/from16 v24, v0

    neg-float v0, v8

    move/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->pan(FF)V

    .line 247
    :cond_a
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointX_1:F

    .line 248
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointY_1:F

    .line 249
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointX_2:F

    .line 250
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMultiPointY_2:F

    .line 251
    sget-object v23, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;->PINCHZOOM:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMode:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$Mode;

    .line 252
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->oldDist:F

    move/from16 v23, v0

    div-float v12, v6, v23

    .line 253
    .restart local v12    # "scale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMidPoint:Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v18, v23, v24

    .line 254
    .restart local v18    # "xx":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMidPoint:Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v22, v23, v24

    .line 255
    .restart local v22    # "yy":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v18

    move/from16 v2, v22

    invoke-virtual {v0, v12, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->zoom(FFF)V

    .line 256
    move-object/from16 v0, p0

    iput v6, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->oldDist:F

    goto/16 :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public setOperateEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 282
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mOperateEnable:Z

    .line 283
    return-void
.end method

.method public setZoomControl(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)V
    .locals 0
    .param p1, "control"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .line 54
    return-void
.end method
