.class Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;
.super Ljava/lang/Object;
.source "PicImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)V
    .locals 0

    .prologue
    .line 796
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 799
    const-string v4, "ArcSoft_PicImageView"

    const-string v5, "mProcessManualMask run "

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getMaskAreas()[B

    move-result-object v0

    .line 808
    .local v0, "grey8":[B
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    const v5, -0x44eeeeef

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 809
    .local v1, "isBrush":Z
    :goto_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mImgwidth:I
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)I

    move-result v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mImgheight:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)I

    move-result v6

    invoke-virtual {v4, v0, v5, v6, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->processManualMask([BIIZ)Z

    move-result v2

    .line 811
    .local v2, "success":Z
    if-eqz v2, :cond_0

    .line 812
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v4, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveOperate([BZ)V

    .line 814
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iput-boolean v3, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mTouchLock:Z

    .line 815
    const-string v3, "ArcSoft_PicImageView"

    const-string v4, "mProcessManualMask run out"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    return-void

    .end local v1    # "isBrush":Z
    .end local v2    # "success":Z
    :cond_1
    move v1, v3

    .line 808
    goto :goto_0
.end method
