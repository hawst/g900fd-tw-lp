.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
.super Ljava/lang/Object;
.source "ArcSeekBarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;
    }
.end annotation


# instance fields
.field private final LONG_PRESSED_TIME:I

.field private final MSG_LONG_PRESSED_CHECK:I

.field mCallback:Landroid/os/Handler$Callback;

.field private mCurProgress:I

.field private mDownX:F

.field private mHandler:Landroid/os/Handler;

.field private mIsLongPressed:Z

.field private mIsWorking:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mSeekBar:Landroid/widget/ProgressBar;

.field private mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

.field private mSeekBarTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;


# direct methods
.method public constructor <init>(Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->LONG_PRESSED_TIME:I

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->MSG_LONG_PRESSED_CHECK:I

    .line 18
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsLongPressed:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mDownX:F

    .line 20
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCurProgress:I

    .line 22
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mLayout:Landroid/widget/RelativeLayout;

    .line 23
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    .line 24
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 25
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 27
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsWorking:Z

    .line 28
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    .line 30
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mHandler:Landroid/os/Handler;

    .line 32
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCallback:Landroid/os/Handler$Callback;

    .line 58
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mHandler:Landroid/os/Handler;

    .line 59
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mLayout:Landroid/widget/RelativeLayout;

    .line 60
    const v0, 0x7f0900c7

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    .line 61
    const v0, 0x7f0900c9

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 62
    const v0, 0x7f0900c8

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->showSeekBar()V

    return-void
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsLongPressed:Z

    return p1
.end method

.method static synthetic access$202(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    .param p1, "x1"    # I

    .prologue
    .line 14
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCurProgress:I

    return p1
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    return-object v0
.end method

.method private resetTouchEventState()V
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->hideSeekBar()V

    .line 133
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->endAdjust()V

    .line 136
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsLongPressed:Z

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mDownX:F

    .line 138
    return-void
.end method

.method private showSeekBar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 122
    return-void
.end method


# virtual methods
.method public getSeekBarValue()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v0

    return v0
.end method

.method public hideSeekBar()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 125
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 126
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 129
    return-void
.end method

.method public isWorking()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsWorking:Z

    return v0
.end method

.method public processSeekBarTouchEvent(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 70
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 72
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-le v5, v4, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->resetTouchEventState()V

    .line 116
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 80
    :pswitch_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->showSeekBar()V

    .line 81
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsLongPressed:Z

    .line 82
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v4

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCurProgress:I

    .line 83
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    if-eqz v4, :cond_1

    .line 84
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    invoke-interface {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->startAdjust()V

    .line 86
    :cond_1
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mDownX:F

    goto :goto_0

    .line 89
    :pswitch_1
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsLongPressed:Z

    if-eqz v5, :cond_3

    .line 90
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mDownX:F

    sub-float v0, v5, v3

    .line 91
    .local v0, "distance":F
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCurProgress:I

    float-to-int v6, v0

    div-int/lit8 v6, v6, 0x3

    sub-int v2, v5, v6

    .line 92
    .local v2, "value":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 94
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v1

    .line 95
    .local v1, "textValue":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v1, :cond_2

    move v1, v4

    .end local v1    # "textValue":I
    :cond_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 97
    .end local v0    # "distance":F
    .end local v2    # "value":I
    :cond_3
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mDownX:F

    goto :goto_0

    .line 101
    :pswitch_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    if-eqz v4, :cond_4

    .line 102
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->processWithSeekBarValue(I)V

    .line 103
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    invoke-interface {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->endAdjust()V

    .line 105
    :cond_4
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->resetTouchEventState()V

    goto :goto_0

    .line 109
    :pswitch_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    if-eqz v4, :cond_5

    .line 110
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->processWithSeekBarValue(I)V

    .line 111
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    invoke-interface {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->endAdjust()V

    .line 113
    :cond_5
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->resetTouchEventState()V

    goto/16 :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setSeekBarListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    .line 67
    return-void
.end method

.method public setSeekBarValue(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 154
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 155
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarValueView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    return-void
.end method

.method public startWork()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsWorking:Z

    .line 142
    return-void
.end method

.method public stopWork()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsWorking:Z

    .line 146
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->hideSeekBar()V

    .line 147
    return-void
.end method
