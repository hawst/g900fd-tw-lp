.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1284
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1305
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1294
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setAlpha(F)V

    .line 1295
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEraserState:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f060049

    .line 1297
    .local v0, "res":I
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1298
    :goto_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v1, v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    if-eqz v1, :cond_0

    .line 1299
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V
    invoke-static {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V

    .line 1301
    :cond_0
    return-void

    .line 1295
    .end local v0    # "res":I
    :cond_1
    const v0, 0x7f060048

    goto :goto_0

    .line 1297
    .restart local v0    # "res":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1291
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 1287
    return-void
.end method
