.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;
.super Landroid/os/AsyncTask;
.source "ManualAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessSeekBarTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 2398
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Integer;

    .prologue
    .line 2402
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 2403
    .local v0, "result":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->updateBlurInfo(IILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    .line 2404
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2405
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2398
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->doInBackground([Ljava/lang/Integer;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2410
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 2411
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2412
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2398
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
