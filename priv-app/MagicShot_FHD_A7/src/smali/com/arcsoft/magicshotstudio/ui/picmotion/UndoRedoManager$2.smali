.class Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;
.super Ljava/lang/Object;
.source "UndoRedoManager.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener",
        "<",
        "Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearDataFile()V
    .locals 3

    .prologue
    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUserDataDir:Ljava/io/File;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "picmotion_redo_data.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->doClearDataFile(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public loadDataFromFile()Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .locals 5

    .prologue
    .line 144
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUserDataDir:Ljava/io/File;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "picmotion_redo_data.dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    .local v0, "filePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I

    move-result v3

    # -= operator for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$620(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;I)I

    .line 146
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I

    move-result v4

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->doLoadDataFromFile(Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    invoke-static {v2, v0, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    move-result-object v1

    .line 147
    .local v1, "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    return-object v1
.end method

.method public bridge synthetic loadDataFromFile()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->loadDataFromFile()Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    move-result-object v0

    return-object v0
.end method

.method public saveDataToFile(Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;)V
    .locals 3
    .param p1, "data"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .prologue
    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUserDataDir:Ljava/io/File;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "picmotion_redo_data.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I

    move-result v2

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->doSaveDataToFile(Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;Ljava/lang/String;I)V
    invoke-static {v1, p1, v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;Ljava/lang/String;I)V

    .line 138
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I

    move-result v2

    # += operator for: Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->access$612(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;I)I

    .line 139
    return-void
.end method

.method public bridge synthetic saveDataToFile(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 131
    check-cast p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;->saveDataToFile(Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;)V

    return-void
.end method
