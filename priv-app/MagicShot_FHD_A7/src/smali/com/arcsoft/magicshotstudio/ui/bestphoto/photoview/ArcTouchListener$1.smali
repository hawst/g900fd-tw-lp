.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;
.super Ljava/lang/Object;
.source "ArcTouchListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->setGestureScanner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 83
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 84
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;->doubleTapUp(Landroid/view/MotionEvent;)V

    .line 86
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 87
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    move-result-object v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mViewWidth:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)I

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mViewHeight:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)I

    move-result v4

    invoke-virtual {v2, p1, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->updatePan(Landroid/view/MotionEvent;II)V

    .line 88
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mViewWidth:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 89
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mViewHeight:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 90
    .local v1, "y":F
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->doubleTapZoom(FF)V

    .line 92
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-interface {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;->singleTapUp(II)V

    .line 73
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
