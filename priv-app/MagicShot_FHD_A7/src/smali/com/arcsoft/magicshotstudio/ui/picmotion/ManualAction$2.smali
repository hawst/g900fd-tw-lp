.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 287
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    move v1, v2

    .line 322
    :goto_1
    return v1

    .line 289
    :pswitch_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 291
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "################### UPDATE_LARGE_BITMAP_AND_MASK mDrawMaskButtonsEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 295
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 296
    :try_start_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "################### UPDATE_LARGE_BITMAP_AND_MASK ##################"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "################### UPDATE_LARGE_BITMAP_AND_MASK ##################"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;

    .line 300
    .local v0, "motionMask":Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;->mBitmap:Landroid/graphics/Bitmap;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 301
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;->mMask:[B

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B
    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;[B)[B

    .line 303
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 304
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setMask([B)V

    .line 306
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateCurrentBlurInfo()V

    .line 308
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v3

    if-eqz v3, :cond_1

    .line 309
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->setCurData([BZ)V

    .line 310
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->resetUndoRedo()V
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 312
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v4, 0x0

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z
    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z

    .line 313
    monitor-exit v2

    goto/16 :goto_1

    .line 314
    .end local v0    # "motionMask":Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 316
    :pswitch_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 317
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateCurrentBlurInfo()V

    goto/16 :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x9998
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
