.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;
.super Ljava/lang/Object;
.source "SimpleImageZoomListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_SimpleImageZoomListener"


# instance fields
.field private final SENSIBILITY:F

.field private mDistance:F

.field private mIsHorizontalMoveWork:Z

.field private mIsMunualMode:Z

.field private mIsVerticalMoveWork:Z

.field private mIsZoomAction:Z

.field private mPoint0_X:F

.field private mPoint0_Y:F

.field private mPoint1_X:F

.field private mPoint1_Y:F

.field private mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

.field private mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

.field private mbIsZoomOperate:Z

.field private sX:F

.field private sX01:F

.field private sY:F

.field private sY01:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_X:F

    .line 11
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_Y:F

    .line 12
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_X:F

    .line 13
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_Y:F

    .line 14
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mDistance:F

    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->SENSIBILITY:F

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mbIsZoomOperate:Z

    .line 169
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsZoomAction:Z

    .line 216
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsHorizontalMoveWork:Z

    .line 217
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsVerticalMoveWork:Z

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    return-void
.end method

.method private getDistance(FFFF)F
    .locals 8
    .param p1, "x0"    # F
    .param p2, "y0"    # F
    .param p3, "x1"    # F
    .param p4, "y1"    # F

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 207
    sub-float v4, p1, p3

    float-to-double v4, v4

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 208
    .local v0, "dX2":D
    sub-float v4, p2, p4

    float-to-double v4, v4

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 209
    .local v2, "dY2":D
    add-double v4, v0, v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v4, v4

    return v4
.end method


# virtual methods
.method public dealMinZoom()V
    .locals 2

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsZoomAction:Z

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->minZoom()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->startAllBounceView()V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->endAllBounceView(Z)V

    goto :goto_0
.end method

.method public ismIsMunualMode()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsMunualMode:Z

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 21
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 28
    const-string v17, "ArcSoft_SimpleImageZoomListener"

    const-string v18, "SimpleImageZoomList onTouch"

    invoke-static/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 30
    .local v2, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v12

    .line 31
    .local v12, "pointNum":I
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsZoomAction:Z

    .line 32
    const-string v17, "ArcSoft_SimpleImageZoomListener"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "SimpleImageZoomList pointNum = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v12, v0, :cond_5

    .line 35
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsMunualMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    .line 37
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    .line 38
    .local v10, "mX":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    .line 39
    .local v11, "mY":F
    packed-switch v2, :pswitch_data_0

    .line 73
    .end local v10    # "mX":F
    .end local v11    # "mY":F
    :cond_0
    :goto_0
    const/16 v17, 0x0

    .line 166
    :goto_1
    return v17

    .line 41
    .restart local v10    # "mX":F
    .restart local v11    # "mY":F
    :pswitch_0
    move-object/from16 v0, p0

    iput v10, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX01:F

    .line 42
    move-object/from16 v0, p0

    iput v11, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY01:F

    .line 43
    move-object/from16 v0, p0

    iput v10, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX:F

    .line 44
    move-object/from16 v0, p0

    iput v11, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY:F

    .line 45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    float-to-int v0, v10

    move/from16 v18, v0

    float-to-int v0, v11

    move/from16 v19, v0

    invoke-interface/range {v17 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->actionDown(II)V

    .line 46
    const/16 v17, 0x0

    goto :goto_1

    .line 48
    :pswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX:F

    move/from16 v17, v0

    sub-float v17, v10, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getAspectQuotient()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoomX(F)F

    move-result v18

    div-float v3, v17, v18

    .line 49
    .local v3, "dX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY:F

    move/from16 v17, v0

    sub-float v17, v11, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getAspectQuotient()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoomY(F)F

    move-result v18

    div-float v6, v17, v18

    .line 50
    .local v6, "dY":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsHorizontalMoveWork:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getPanX()F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    mul-float v19, v19, v3

    sub-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->setPanX(F)V

    .line 53
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsVerticalMoveWork:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 54
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getPanY()F

    move-result v18

    const/high16 v19, 0x3f800000    # 1.0f

    mul-float v19, v19, v6

    sub-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->setPanY(F)V

    .line 56
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsHorizontalMoveWork:Z

    move/from16 v17, v0

    if-nez v17, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsVerticalMoveWork:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4

    .line 57
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->notifyObservers()V

    .line 62
    :cond_4
    move-object/from16 v0, p0

    iput v10, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX:F

    .line 63
    move-object/from16 v0, p0

    iput v11, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY:F

    goto/16 :goto_0

    .line 66
    .end local v3    # "dX":F
    .end local v6    # "dY":F
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->actionUp()V

    .line 67
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX01:F

    move/from16 v18, v0

    cmpl-float v17, v17, v18

    if-nez v17, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY01:F

    move/from16 v18, v0

    cmpl-float v17, v17, v18

    if-nez v17, :cond_0

    .line 68
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 75
    .end local v10    # "mX":F
    .end local v11    # "mY":F
    :cond_5
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v12, v0, :cond_6

    .line 77
    const/16 v17, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v13

    .line 78
    .local v13, "touch0_X":F
    const/16 v17, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v14

    .line 79
    .local v14, "touch0_Y":F
    const/16 v17, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v15

    .line 80
    .local v15, "touch1_X":F
    const/16 v17, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v16

    .line 81
    .local v16, "touch1_Y":F
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v13, v14, v15, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->getDistance(FFFF)F

    move-result v9

    .line 83
    .local v9, "distance":F
    sparse-switch v2, :sswitch_data_0

    .line 166
    .end local v9    # "distance":F
    .end local v13    # "touch0_X":F
    .end local v14    # "touch0_Y":F
    .end local v15    # "touch1_X":F
    .end local v16    # "touch1_Y":F
    :cond_6
    :goto_2
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 86
    .restart local v9    # "distance":F
    .restart local v13    # "touch0_X":F
    .restart local v14    # "touch0_Y":F
    .restart local v15    # "touch1_X":F
    .restart local v16    # "touch1_Y":F
    :sswitch_0
    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_X:F

    .line 87
    move-object/from16 v0, p0

    iput v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_Y:F

    .line 88
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_X:F

    .line 89
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_Y:F

    .line 90
    move-object/from16 v0, p0

    iput v9, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mDistance:F

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->resetDirection()V

    .line 92
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mbIsZoomOperate:Z

    goto :goto_2

    .line 96
    :sswitch_1
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX:F

    .line 97
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY:F

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->endAllBounceView(Z)V

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->resetDirection()V

    goto :goto_2

    .line 102
    :sswitch_2
    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sX:F

    .line 103
    move-object/from16 v0, p0

    iput v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->sY:F

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->endAllBounceView(Z)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;->resetDirection()V

    goto :goto_2

    .line 122
    :sswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_X:F

    move/from16 v17, v0

    sub-float v17, v13, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getAspectQuotient()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoomX(F)F

    move-result v18

    div-float v4, v17, v18

    .line 123
    .local v4, "dX0":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_Y:F

    move/from16 v17, v0

    sub-float v17, v14, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getAspectQuotient()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoomY(F)F

    move-result v18

    div-float v7, v17, v18

    .line 124
    .local v7, "dY0":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_X:F

    move/from16 v17, v0

    sub-float v17, v15, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getAspectQuotient()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoomX(F)F

    move-result v18

    div-float v5, v17, v18

    .line 125
    .local v5, "dX1":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_Y:F

    move/from16 v17, v0

    sub-float v17, v16, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getAspectQuotient()F

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoomY(F)F

    move-result v18

    div-float v8, v17, v18

    .line 127
    .local v8, "dY1":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsHorizontalMoveWork:Z

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getPanX()F

    move-result v18

    add-float v19, v4, v5

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    const/high16 v20, 0x3f800000    # 1.0f

    mul-float v19, v19, v20

    sub-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->setPanX(F)V

    .line 130
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsVerticalMoveWork:Z

    move/from16 v17, v0

    if-eqz v17, :cond_8

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getPanY()F

    move-result v18

    add-float v19, v7, v8

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    const/high16 v20, 0x3f800000    # 1.0f

    mul-float v19, v19, v20

    sub-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->setPanY(F)V

    .line 133
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsHorizontalMoveWork:Z

    move/from16 v17, v0

    if-nez v17, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsVerticalMoveWork:Z

    move/from16 v17, v0

    if-eqz v17, :cond_a

    .line 134
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->notifyObservers()V

    .line 141
    :cond_a
    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_X:F

    .line 142
    move-object/from16 v0, p0

    iput v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_Y:F

    .line 143
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_X:F

    .line 144
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_Y:F

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->getZoom()F

    move-result v18

    mul-float v18, v18, v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mDistance:F

    move/from16 v19, v0

    div-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->setZoom(F)V

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->notifyObservers()V

    .line 150
    move-object/from16 v0, p0

    iput v9, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mDistance:F

    .line 151
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsZoomAction:Z

    .line 158
    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_X:F

    .line 159
    move-object/from16 v0, p0

    iput v14, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint0_Y:F

    .line 160
    move-object/from16 v0, p0

    iput v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_X:F

    .line 161
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mPoint1_Y:F

    goto/16 :goto_2

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 83
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x5 -> :sswitch_0
        0x6 -> :sswitch_1
        0x105 -> :sswitch_0
        0x106 -> :sswitch_2
    .end sparse-switch
.end method

.method public setMoveActionWork(ZZ)V
    .locals 0
    .param p1, "isHorizontalMoveWork"    # Z
    .param p2, "isVerticalMoveWork"    # Z

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsHorizontalMoveWork:Z

    .line 220
    iput-boolean p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsVerticalMoveWork:Z

    .line 221
    return-void
.end method

.method public setShowBounceViewListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    .line 226
    return-void
.end method

.method public setZoomState(Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;)V
    .locals 0
    .param p1, "state"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    .line 214
    return-void
.end method

.method public setmIsMunualMode(Z)V
    .locals 0
    .param p1, "mIsMunualMode"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->mIsMunualMode:Z

    .line 194
    return-void
.end method
