.class public Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;
.super Ljava/lang/Object;
.source "PopupToast.java"


# static fields
.field public static final OFFSET_BELOW_BUTTON:I = 0x3

.field public static final PADDING_BOTTOM:I = 0x9

.field public static final PADDING_LEFT:I = 0x12

.field public static final PADDING_RIGHT:I = 0x12

.field public static final PADDING_TOP:I = 0x9

.field public static final TOAST_HEIGHT:I = 0x25

.field public static final TOAST_TEXTSIZE:I = 0xf

.field static mInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;


# instance fields
.field mColor:I

.field mContext:Landroid/content/Context;

.field mScreenDensity:F

.field mText:Landroid/widget/TextView;

.field mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/16 v0, 0xf5

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {v0, v0, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mColor:I

    .line 42
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    .line 43
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    .line 44
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mText:Landroid/widget/TextView;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mScreenDensity:F

    .line 48
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    .line 49
    new-instance v0, Landroid/widget/Toast;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    .line 50
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mText:Landroid/widget/TextView;

    .line 51
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mScreenDensity:F

    .line 52
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;
    .locals 1
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 23
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    .line 27
    :cond_0
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    return-object v0
.end method

.method private getToastPosition(Landroid/view/View;I[I)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "screenWith"    # I
    .param p3, "p"    # [I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 55
    invoke-virtual {p1, p3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 56
    move v1, p2

    .line 57
    .local v1, "nScreenWidth":I
    aget v4, p3, v7

    sub-int v4, v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    aput v4, p3, v7

    .line 58
    aget v4, p3, v6

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    aput v4, p3, v6

    .line 60
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050094

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 61
    .local v2, "offset":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    .line 62
    .local v0, "density":F
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v3, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 63
    .local v3, "screenHeight":I
    aget v4, p3, v7

    sub-int/2addr v4, v2

    aput v4, p3, v7

    .line 64
    aget v4, p3, v6

    div-int/lit8 v5, v3, 0x2

    if-ge v4, v5, :cond_0

    .line 65
    aget v4, p3, v6

    add-int/2addr v4, v2

    aput v4, p3, v6

    .line 67
    :cond_0
    aget v4, p3, v6

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v0

    float-to-int v5, v5

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x2

    aput v4, p3, v6

    .line 68
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 7

    .prologue
    .line 105
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    if-eqz v4, :cond_0

    .line 108
    :try_start_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mTN"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 109
    .local v1, "field":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 110
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 111
    .local v3, "object":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "hide"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 112
    .local v2, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "method":Ljava/lang/reflect/Method;
    .end local v3    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public showDefaultPopUpToast(Ljava/lang/String;)V
    .locals 5
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 92
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    if-eqz v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 94
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    .line 96
    :cond_0
    new-instance v2, Landroid/widget/Toast;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    .line 97
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 98
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03002c

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 99
    .local v0, "content":Landroid/view/View;
    const v2, 0x7f0900e2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mText:Landroid/widget/TextView;

    .line 100
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mText:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 102
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 103
    return-void
.end method

.method public showPopUpToast(Landroid/view/View;II)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "screenWith"    # I
    .param p3, "resId"    # I

    .prologue
    .line 71
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {p0, p1, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showPopUpToast(Landroid/view/View;ILjava/lang/String;)V

    .line 73
    return-void
.end method

.method public showPopUpToast(Landroid/view/View;ILjava/lang/String;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "screenWith"    # I
    .param p3, "str"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 76
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    if-eqz v3, :cond_0

    .line 77
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    .line 78
    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    .line 80
    :cond_0
    new-instance v3, Landroid/widget/Toast;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    .line 81
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 82
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03002c

    invoke-virtual {v1, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 83
    .local v0, "content":Landroid/view/View;
    const v3, 0x7f0900e2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mText:Landroid/widget/TextView;

    .line 84
    const/4 v3, 0x2

    new-array v2, v3, [I

    .line 85
    .local v2, "point":[I
    invoke-direct {p0, p1, p2, v2}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getToastPosition(Landroid/view/View;I[I)V

    .line 86
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mText:Landroid/widget/TextView;

    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3, v0}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 88
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    const/16 v4, 0x35

    const/4 v5, 0x0

    aget v5, v2, v5

    const/4 v6, 0x1

    aget v6, v2, v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 89
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 90
    return-void
.end method
