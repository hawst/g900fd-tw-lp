.class public Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.super Ljava/lang/Object;
.source "ManualAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$ProcessCheckedTask;,
        Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;,
        Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;
    }
.end annotation


# instance fields
.field private final MAX_SOUND_STREAM:I

.field ViewsShow:Z

.field private mActionBar:Landroid/widget/RelativeLayout;

.field private mActivity:Lcom/arcsoft/magicshotstudio/Drama;

.field mAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;

.field mAnimEraserState:Landroid/animation/Animator;

.field mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

.field mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

.field private mArrowClickLis:Landroid/view/View$OnClickListener;

.field private mBackArrowImageView:Landroid/widget/ImageView;

.field private mBackArrowLayout:Landroid/widget/RelativeLayout;

.field private mBackImageView:Landroid/widget/ImageView;

.field private mBackLayout:Landroid/widget/RelativeLayout;

.field private mBackLayoutGap3:Landroid/view/View;

.field private mBackTextView:Landroid/widget/TextView;

.field private mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

.field mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

.field mBrushEraser:I

.field mBrushEraserLayout:Landroid/widget/RelativeLayout;

.field private mBtnClickEnable:Z

.field mCancelLayout:Landroid/widget/RelativeLayout;

.field mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

.field mCheckedFrameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field mClickInterval:J

.field private mCurTouchedId:I

.field mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

.field private mDefaultActionBar:Landroid/widget/RelativeLayout;

.field private mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field mDoneLayout:Landroid/widget/RelativeLayout;

.field mDownArrowsButton:Landroid/widget/ImageView;

.field mDownArrowsLayout:Landroid/widget/RelativeLayout;

.field private mDownArrowsView:Landroid/widget/RelativeLayout;

.field private mDrawMaskButtonsEnable:Z

.field mEditEnable:Z

.field private mEditImageView:Landroid/widget/ImageView;

.field private mEditLayout:Landroid/widget/RelativeLayout;

.field private mEditLayoutGap3:Landroid/view/View;

.field mEditMaskBar:Landroid/widget/RelativeLayout;

.field mEditMaskBarShowAnim:Landroid/animation/Animator;

.field mEditMaskBarUnShowAnim:Landroid/animation/Animator;

.field private mEditTextView:Landroid/widget/TextView;

.field mEidtShowFrameList:Landroid/animation/AnimatorSet;

.field mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

.field private mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

.field mEraserSelector:Landroid/widget/ImageView;

.field private mEraserState:Z

.field private mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

.field mFileSaving:Z

.field mFramViewLinearLayout:Landroid/view/View;

.field mFrameEnable:Z

.field mFrameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field private mFrameMaskIndex:I

.field mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

.field mGreyFramebarOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field mIBBrushEraser:Landroid/widget/ImageView;

.field mIBRestore:Landroid/widget/ImageView;

.field mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

.field mIsFrameListShow:Z

.field private mIsTouchLocked:Z

.field private mLargeBitmap:Landroid/graphics/Bitmap;

.field mLastClickTime:J

.field private mManualDescriptionLayout:Landroid/widget/RelativeLayout;

.field mMaskActionBar:Landroid/widget/RelativeLayout;

.field private mMenuImageView:Landroid/widget/ImageView;

.field private mMenuLayout:Landroid/widget/RelativeLayout;

.field private mMenuLayoutGap3:Landroid/view/View;

.field private mMenuTextView:Landroid/widget/TextView;

.field private mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

.field mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnKeyListener:Landroid/view/View$OnKeyListener;

.field mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private volatile mOperateEnable:Z

.field mOrientation:I

.field private mProcessLock:Ljava/util/concurrent/Semaphore;

.field mProgressDialog:Landroid/app/Dialog;

.field mPutShotTop:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;

.field mRedoButton:Landroid/widget/ImageView;

.field mRedoLayout:Landroid/widget/RelativeLayout;

.field mRestoreLayout:Landroid/widget/RelativeLayout;

.field mRestoreSelector:Landroid/widget/ImageView;

.field mSaveEnable:Z

.field private mSaveImageView:Landroid/widget/ImageView;

.field private mSaveLayout:Landroid/widget/RelativeLayout;

.field private mSaveLayoutGap3:Landroid/view/View;

.field private mSaveLock:Ljava/util/concurrent/Semaphore;

.field mSavePath:Ljava/lang/String;

.field private mSaveTextView:Landroid/widget/TextView;

.field private mSaveThread:Ljava/lang/Thread;

.field private mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mSelectFlags:[B

.field mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

.field private mShotMask:[B

.field private mSrcImageHeight:I

.field private mSrcImageWidth:I

.field mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mThumbnailBitmaps:[Landroid/graphics/Bitmap;

.field private mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUnRedoLockedTime:J

.field mUndoButton:Landroid/widget/ImageView;

.field mUndoLayout:Landroid/widget/RelativeLayout;

.field private mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

.field mVisibility_Actionbar:I

.field mVisibility_Description:I

.field mVisibility_FramView:I

.field mVisibility_mDownArrows:I

.field mVisibility_mUpArrows:I

.field mVisibiltiy_mEditMaskBar:I

.field private mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

.field private mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

.field private mbIsAnimDefaultBarIn:Z

.field private final mbIsAnimationOpen:Z

.field mbIsSaveBtnOn:Z

.field mbShowEraserState:Z

.field mbShowSelectDescription:Z

.field private tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-class v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    .line 84
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    .line 85
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    .line 86
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    .line 87
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    .line 88
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 91
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    .line 94
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    .line 96
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    .line 97
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    .line 101
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 102
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsAnimationOpen:Z

    .line 105
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    .line 106
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    .line 118
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 120
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    .line 121
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 122
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    .line 124
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    .line 125
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditTextView:Landroid/widget/TextView;

    .line 126
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackTextView:Landroid/widget/TextView;

    .line 127
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    .line 129
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    .line 130
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    .line 133
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    .line 134
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    .line 137
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    .line 138
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->MAX_SOUND_STREAM:I

    .line 164
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    .line 165
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    .line 168
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    .line 176
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    .line 178
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    .line 181
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSrcImageWidth:I

    .line 182
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSrcImageHeight:I

    .line 183
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 184
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    .line 185
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    .line 186
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    .line 188
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 189
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    .line 191
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 570
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOperateEnable:Z

    .line 600
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    .line 742
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 837
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mArrowClickLis:Landroid/view/View$OnClickListener;

    .line 900
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 929
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J

    .line 932
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBtnClickEnable:Z

    .line 939
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 1104
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 1183
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I

    .line 1184
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    .line 1186
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEraserState:Z

    .line 1327
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditEnable:Z

    .line 1328
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveEnable:Z

    .line 1329
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameEnable:Z

    .line 1395
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsAnimDefaultBarIn:Z

    .line 1396
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 1397
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 1483
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$8;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    .line 1553
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFileSaving:Z

    .line 1554
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveThread:Ljava/lang/Thread;

    .line 1610
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLastClickTime:J

    .line 1611
    const-wide/16 v0, 0x190

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mClickInterval:J

    .line 1612
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$10;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1691
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$11;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mGreyFramebarOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1732
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mPutShotTop:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$PutShotTop;

    .line 1893
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    .line 2178
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowSelectDescription:Z

    .line 2179
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ViewsShow:Z

    .line 2180
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 2249
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z

    .line 2251
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I

    .line 2252
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$12;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method private ProcessCheckedFrame()Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 2069
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    monitor-enter v7

    .line 2070
    :try_start_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_1

    .line 2071
    :cond_0
    const/4 v6, 0x0

    monitor-exit v7

    .line 2094
    :goto_0
    return-object v6

    .line 2074
    :cond_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2076
    .local v1, "lenght":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 2077
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    .line 2078
    .local v5, "tmp":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getMaskIndexFromPosition(I)I

    move-result v2

    .line 2079
    .local v2, "maskIndex":I
    iget-boolean v6, v5, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    if-eqz v6, :cond_2

    .line 2080
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    const/4 v8, 0x1

    aput-byte v8, v6, v2

    .line 2076
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2082
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    const/4 v8, 0x0

    aput-byte v8, v6, v2

    goto :goto_2

    .line 2085
    .end local v0    # "i":I
    .end local v1    # "lenght":I
    .end local v2    # "maskIndex":I
    .end local v5    # "tmp":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v0    # "i":I
    .restart local v1    # "lenght":I
    :cond_3
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2087
    new-instance v4, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    invoke-direct {v4}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;-><init>()V

    .line 2088
    .local v4, "returnBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    const/4 v3, 0x0

    .line 2089
    .local v3, "res":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    if-eqz v6, :cond_4

    .line 2090
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    invoke-virtual {v6, v7, v4}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->applySelected([BLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v3

    .line 2093
    :cond_4
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getResultImageBySelect res:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094
    iget-object v6, v4, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private ShowFrameListAnim(Z)V
    .locals 4
    .param p1, "bShow"    # Z

    .prologue
    const/4 v1, 0x0

    .line 866
    if-eqz p1, :cond_4

    .line 867
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 868
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowSelectDescription:Z

    if-eqz v0, :cond_3

    const v0, 0x7f060003

    :goto_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V

    .line 869
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 873
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    .line 874
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 875
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 879
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 896
    :cond_2
    :goto_1
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    .line 897
    return-void

    :cond_3
    move v0, v1

    .line 868
    goto :goto_0

    .line 881
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 882
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 886
    :cond_5
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    .line 887
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 888
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->popUpToast(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showActionBar()V

    return-void
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->exitWidthoutSave()V

    return-void
.end method

.method static synthetic access$1400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->cancelMask()V

    return-void
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->resetEditView()V

    return-void
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ProcessCheckedFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->backToSelectFrame()V

    return-void
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J

    return-wide v0
.end method

.method static synthetic access$2002(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # J

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J

    return-wide p1
.end method

.method static synthetic access$202(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEraserState:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkEditable()V

    return-void
.end method

.method static synthetic access$2700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initCheckedFrameList()V

    return-void
.end method

.method static synthetic access$2800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I

    return v0
.end method

.method static synthetic access$2902(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I

    return p1
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getMaskIndexFromCheckedPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setItemActivated(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->enableMaskbar(Z)V

    return-void
.end method

.method static synthetic access$3300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->putShotOnTop(I)V

    return-void
.end method

.method static synthetic access$3400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z

    return v0
.end method

.method static synthetic access$3502(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsTouchLocked:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I

    return v0
.end method

.method static synthetic access$3602(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurTouchedId:I

    return p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showMaskActionbar()V

    return-void
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    return-object v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V

    return-void
.end method

.method private backToSelectFrame()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2044
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    .line 2045
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setItemActivated(Landroid/view/View;I)V

    .line 2046
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDefaultActionbar()V

    .line 2048
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2049
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2050
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2051
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2053
    return-void
.end method

.method private cancelMask()V
    .locals 3

    .prologue
    .line 706
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;-><init>()V

    .line 707
    .local v1, "retBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->cancelEdit(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 708
    .local v0, "res":I
    if-nez v0, :cond_0

    .line 709
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 712
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->backToSelectFrame()V

    .line 713
    return-void
.end method

.method private checkEditable()V
    .locals 4

    .prologue
    .line 1952
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setEditableState(Z)V

    .line 1954
    const/4 v2, 0x0

    .line 1955
    .local v2, "size":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1956
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1959
    :cond_0
    if-lez v2, :cond_1

    .line 1960
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1961
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    .line 1962
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    iget-boolean v3, v0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    if-eqz v3, :cond_2

    .line 1963
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setEditableState(Z)V

    .line 1968
    .end local v0    # "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 1960
    .restart local v0    # "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    .restart local v1    # "i":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private checkUndoRedoState()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->checkUndoEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 337
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->checkRedoEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 339
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V

    .line 341
    return-void

    .line 339
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearItemActivatedAndFocus()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1669
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    if-nez v3, :cond_1

    .line 1679
    :cond_0
    return-void

    .line 1673
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1674
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1675
    .local v0, "child":Landroid/view/View;
    const v3, 0x7f090090

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1676
    .local v2, "selectorView":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/view/View;->setPressed(Z)V

    .line 1677
    invoke-virtual {v2, v4}, Landroid/view/View;->setActivated(Z)V

    .line 1673
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private enableMaskbar(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 603
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    .line 605
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 606
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 607
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 609
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 610
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 612
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setEnabled(Z)V

    .line 613
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 614
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 615
    .local v1, "enableColor":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 616
    .local v0, "disableColor":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    if-eqz p1, :cond_0

    .end local v1    # "enableColor":I
    :goto_0
    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setTextColor(I)V

    .line 617
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 618
    return-void

    .restart local v1    # "enableColor":I
    :cond_0
    move v1, v0

    .line 616
    goto :goto_0
.end method

.method private exitWidthoutSave()V
    .locals 1

    .prologue
    .line 1549
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 1550
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    .line 1551
    return-void
.end method

.method private fetchAutoResult()V
    .locals 4

    .prologue
    .line 1863
    const/4 v0, 0x0

    .line 1864
    .local v0, "res":I
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;-><init>()V

    .line 1875
    .local v1, "rtBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    invoke-virtual {v2, v3, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->applySelected([BLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 1876
    if-eqz v0, :cond_0

    .line 1877
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "fail applySelected !!!!!!!"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1879
    :cond_0
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    .line 1880
    return-void
.end method

.method private fetchCandidate()V
    .locals 6

    .prologue
    .line 1833
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 1834
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v4, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1837
    const/4 v3, 0x0

    .line 1838
    .local v3, "width":I
    const/4 v0, 0x0

    .line 1840
    .local v0, "height":I
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSrcImageWidth:I

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSrcImageHeight:I

    if-le v4, v5, :cond_0

    .line 1841
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050032

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1842
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050033

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1848
    :goto_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    if-nez v4, :cond_1

    .line 1860
    :goto_1
    return-void

    .line 1844
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050034

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1845
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 1852
    :cond_1
    const/4 v1, 0x0

    .line 1853
    .local v1, "res":I
    new-instance v2, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;

    invoke-direct {v2}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;-><init>()V

    .line 1854
    .local v2, "returnThumbs":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-virtual {v4, v3, v0, v2}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->getCandidateThumbs(IILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;)I

    move-result v1

    .line 1855
    if-eqz v1, :cond_2

    .line 1856
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v5, "Drama getCandidateThumbs is null!"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1859
    :cond_2
    iget-object v4, v2, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;->mThumbs:[Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method private fetchSelectedFlags()V
    .locals 4

    .prologue
    .line 1883
    const/4 v0, 0x0

    .line 1884
    .local v0, "res":I
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;-><init>()V

    .line 1885
    .local v1, "rtSelected":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->getSelected(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;)I

    move-result v0

    .line 1886
    if-eqz v0, :cond_0

    .line 1887
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "Drama getSelected is null!"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1891
    :goto_0
    return-void

    .line 1890
    :cond_0
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;->mFlags:[B

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    goto :goto_0
.end method

.method private getMaskIndexFromCheckedPosition(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1754
    const/4 v0, -0x1

    .line 1755
    .local v0, "maskIndex":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    iget v1, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mPosition:I

    .line 1756
    .local v1, "num":I
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getMaskIndexFromPosition(I)I

    move-result v0

    .line 1757
    return v0
.end method

.method private getMaskIndexFromPosition(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1761
    const/4 v0, -0x1

    .line 1762
    .local v0, "maskIndex":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    iget v1, v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;->mDirection:I

    if-nez v1, :cond_0

    .line 1763
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, p1

    .line 1767
    :goto_0
    return v0

    .line 1765
    :cond_0
    move v0, p1

    goto :goto_0
.end method

.method private getShotMaskByIndex(I)[B
    .locals 4
    .param p1, "shotIndex"    # I

    .prologue
    .line 1682
    const/4 v0, 0x0

    .line 1683
    .local v0, "mask":[B
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;-><init>()V

    .line 1684
    .local v1, "returnMask":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-virtual {v3, p1, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->getObjectMask(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;)I

    move-result v2

    .line 1685
    .local v2, "success":I
    if-nez v2, :cond_0

    .line 1686
    iget-object v0, v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;->mMask:[B

    .line 1688
    :cond_0
    return-object v0
.end method

.method private hideDefaultBars()V
    .locals 2

    .prologue
    .line 1341
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1342
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1344
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1345
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 1348
    :cond_0
    return-void
.end method

.method private hideFrameCandidateListView()V
    .locals 2

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1369
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1372
    :cond_0
    return-void
.end method

.method private inflateRollWaitingView()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1784
    new-instance v3, Landroid/app/Dialog;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {v3, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    .line 1785
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1786
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f03002f

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1787
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0900e5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 1788
    .local v1, "pBar4":Landroid/widget/ProgressBar;
    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1790
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1791
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 1792
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/high16 v4, 0x7f040000

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 1794
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1795
    return-void
.end method

.method private initCheckedFrameList()V
    .locals 3

    .prologue
    .line 1896
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitCheckedFrameList()V

    .line 1897
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    .line 1898
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1900
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 1901
    .local v1, "iter":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1902
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    .line 1903
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    if-eqz v2, :cond_0

    .line 1904
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1907
    .end local v0    # "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    :cond_1
    return-void
.end method

.method private initFrameList()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1910
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitFrameList()V

    .line 1911
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    .line 1912
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1914
    const/4 v0, 0x0

    .line 1915
    .local v0, "arrayLength":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    .line 1916
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    array-length v0, v3

    .line 1919
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->getMotionDirection(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;)I

    .line 1920
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArcDrama mOrientation is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1921
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArcDrama Direction is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    iget v5, v5, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;->mDirection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1923
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_3

    .line 1924
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;-><init>()V

    .line 1926
    .local v1, "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;->mDirection:I

    if-nez v3, :cond_2

    .line 1927
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    add-int/lit8 v4, v0, -0x1

    sub-int/2addr v4, v2

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    add-int/lit8 v6, v0, -0x1

    sub-int/2addr v6, v2

    aget-object v5, v5, v6

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1930
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    add-int/lit8 v4, v0, -0x1

    sub-int/2addr v4, v2

    aget-object v3, v3, v4

    iput-object v3, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    .line 1931
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    add-int/lit8 v4, v0, -0x1

    sub-int/2addr v4, v2

    aget-byte v3, v3, v4

    if-ne v7, v3, :cond_1

    .line 1932
    iput-boolean v7, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    .line 1943
    :cond_1
    :goto_1
    iget-object v3, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->grey(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mGreyFrame:Landroid/graphics/Bitmap;

    .line 1944
    iput v2, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mPosition:I

    .line 1945
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1923
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1934
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;->mDirection:I

    if-ne v7, v3, :cond_1

    .line 1935
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v2

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1937
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v2

    iput-object v3, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    .line 1938
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectFlags:[B

    aget-byte v3, v3, v2

    if-ne v7, v3, :cond_1

    .line 1939
    iput-boolean v7, v1, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mChecked:Z

    goto :goto_1

    .line 1948
    .end local v1    # "data":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    :cond_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initCheckedFrameList()V

    .line 1949
    return-void
.end method

.method private initSound()V
    .locals 0

    .prologue
    .line 1774
    return-void
.end method

.method private isTalkBackOn()Z
    .locals 1

    .prologue
    .line 2328
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->isTalkBackOn()Z

    move-result v0

    return v0
.end method

.method private popUpToast(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 758
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    .line 760
    move-object v0, p1

    .line 761
    .local v0, "pressedView":Landroid/view/View;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->getWidth()I

    move-result v1

    .line 762
    .local v1, "screenWith":I
    const/4 v2, 0x0

    .line 764
    .local v2, "textRes":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 824
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 825
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v3, v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showPopUpToast(Landroid/view/View;II)V

    .line 828
    :cond_1
    const/4 v3, 0x1

    return v3

    .line 766
    :sswitch_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 769
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    .line 770
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v3, :cond_2

    const v2, 0x7f06000c

    .line 771
    :goto_1
    goto :goto_0

    .line 770
    :cond_2
    const v2, 0x7f060018

    goto :goto_1

    .line 773
    :sswitch_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 776
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    .line 777
    const v2, 0x7f06000b

    .line 778
    goto :goto_0

    .line 780
    :sswitch_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 783
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackImageView:Landroid/widget/ImageView;

    .line 784
    const v2, 0x7f06000e

    .line 785
    goto :goto_0

    .line 788
    :sswitch_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 791
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    .line 792
    const v2, 0x7f060007

    .line 793
    goto :goto_0

    .line 795
    :sswitch_4
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 798
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    .line 799
    const v2, 0x7f060008

    .line 800
    goto :goto_0

    .line 802
    :sswitch_5
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 805
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 806
    const v2, 0x7f060027

    .line 807
    goto :goto_0

    .line 809
    :sswitch_6
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 812
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    .line 813
    const v2, 0x7f060009

    .line 814
    goto :goto_0

    .line 816
    :sswitch_7
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 819
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    .line 820
    const v2, 0x7f06000a

    goto :goto_0

    .line 764
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_5
        0x7f09004c -> :sswitch_2
        0x7f090053 -> :sswitch_0
        0x7f090075 -> :sswitch_1
        0x7f090081 -> :sswitch_4
        0x7f090085 -> :sswitch_3
        0x7f090089 -> :sswitch_6
        0x7f09008c -> :sswitch_7
    .end sparse-switch
.end method

.method private putShotOnTop(I)V
    .locals 18
    .param p1, "shotIndex"    # I

    .prologue
    .line 2117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "putShotOnTop in "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "getResultImageByMask in "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 2121
    .local v12, "start":J
    new-instance v11, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;

    invoke-direct {v11}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;-><init>()V

    .line 2122
    .local v11, "returnMask":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    move/from16 v0, p1

    invoke-virtual {v2, v0, v11}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->getObjectMask(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;)I

    move-result v14

    .line 2123
    .local v14, "success":I
    if-eqz v14, :cond_0

    .line 2125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "putShotOnTop getShotMask failed"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    :goto_0
    return-void

    .line 2129
    :cond_0
    iget v5, v11, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;->mWidth:I

    .line 2130
    .local v5, "width":I
    iget v6, v11, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;->mHeight:I

    .line 2132
    .local v6, "height":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "test size match grey width = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "test size match grey height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2135
    new-instance v7, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    invoke-direct {v7}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;-><init>()V

    .line 2136
    .local v7, "returnBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-object v4, v11, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;->mMask:[B

    move/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->getResultImageByMask(I[BIILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v10

    .line 2139
    .local v10, "result":I
    if-nez v10, :cond_1

    .line 2140
    iget-object v2, v7, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    .line 2143
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "test size match Bitmap width = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "test size match Bitmap height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148
    iget-object v2, v11, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;->mMask:[B

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    .line 2149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    if-nez v2, :cond_2

    .line 2150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "test size match mShotMask = null"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2157
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 2161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setMask([B)V

    .line 2162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->setCurData([BZ)V

    .line 2163
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->resetUndoRedo()V

    .line 2165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2166
    .local v8, "end":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResultImageByMask out cost = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v8, v12

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "putShotOnTop out "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2152
    .end local v8    # "end":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "test size match mShotMask !!!!= null"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2006
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2007
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2009
    :cond_0
    const/4 p1, 0x0

    .line 2010
    return-void
.end method

.method private resetEditView()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2204
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowSelectDescription:Z

    .line 2205
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 2206
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V

    .line 2207
    return-void
.end method

.method private resetUndoRedo()V
    .locals 1

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->clear()V

    .line 1729
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkUndoRedoState()V

    .line 1730
    return-void
.end method

.method private setEditableState(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1971
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 1972
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1973
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1975
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V

    .line 1977
    return-void

    .line 1975
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setItemActivated(Landroid/view/View;I)V
    .locals 2
    .param p1, "item"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 1659
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->clearItemActivatedAndFocus()V

    .line 1660
    if-eqz p1, :cond_0

    .line 1661
    const v1, 0x7f090090

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1662
    .local v0, "selectorView":Landroid/view/View;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 1665
    .end local v0    # "selectorView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

    invoke-virtual {v1, p2}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->setActivated(I)V

    .line 1666
    return-void
.end method

.method private showActionBar()V
    .locals 1

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1427
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1428
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->animDefaultBarIn()V

    .line 1432
    :cond_0
    :goto_0
    return-void

    .line 1429
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1430
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    goto :goto_0
.end method

.method private showDefaultActionbar()V
    .locals 2

    .prologue
    .line 594
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->changeMode(Z)V

    .line 595
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 597
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 598
    return-void
.end method

.method private showDefaultBars()V
    .locals 2

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1333
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1335
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1338
    :cond_0
    return-void
.end method

.method private showDescription(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 2227
    if-gtz p1, :cond_1

    .line 2228
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2236
    :cond_0
    :goto_0
    return-void

    .line 2231
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 2233
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 2234
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method private showEditMaskBarAnim(Z)V
    .locals 2
    .param p1, "bShow"    # Z

    .prologue
    .line 853
    if-eqz p1, :cond_1

    .line 854
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 862
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method private showFrameCandidateListView()V
    .locals 2

    .prologue
    .line 1351
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1352
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1357
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1360
    :cond_0
    return-void
.end method

.method private showMaskActionbar()V
    .locals 2

    .prologue
    .line 581
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->changeMode(Z)V

    .line 583
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 584
    const v0, 0x7f020051

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    .line 585
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setRubberValid()V

    .line 587
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->enableMaskbar(Z)V

    .line 588
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;->setFrameList(Ljava/util/ArrayList;)V

    .line 589
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 590
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mGreyFramebarOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 591
    return-void
.end method

.method private switchRestoreAndEraserButtonFocus(Z)V
    .locals 5
    .param p1, "isEraserButtonFocused"    # Z

    .prologue
    const v4, 0x7f020067

    const v3, 0x7f020051

    const/4 v0, 0x0

    .line 716
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEraserState:Z

    .line 718
    if-eqz p1, :cond_1

    .line 719
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 720
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v2, 0x7f02003b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 722
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setRubberValid()V

    .line 723
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const v2, 0x7f060049

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 725
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    .line 736
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 738
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V

    .line 740
    return-void

    .line 727
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    const v2, 0x7f020040

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 729
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 730
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setPenValid()V

    .line 731
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const v2, 0x7f060048

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 733
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    goto :goto_0
.end method

.method private switchTalkBackMode(Z)V
    .locals 14
    .param p1, "isLandScape"    # Z

    .prologue
    .line 621
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 622
    .local v4, "res":Landroid/content/res/Resources;
    const v11, 0x7f06000b

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 623
    .local v1, "edit":Ljava/lang/String;
    const v11, 0x7f060012

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 624
    .local v0, "disabled":Ljava/lang/String;
    const v11, 0x7f060011

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 625
    .local v2, "notSelected":Ljava/lang/String;
    const v11, 0x7f060010

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 626
    .local v5, "selected":Ljava/lang/String;
    const v11, 0x7f06000e

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 627
    .local v6, "szCancel":Ljava/lang/String;
    const v11, 0x7f060008

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 628
    .local v9, "szRestore":Ljava/lang/String;
    const v11, 0x7f060007

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 629
    .local v8, "szEraser":Ljava/lang/String;
    const v11, 0x7f06000f

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 630
    .local v7, "szDone":Ljava/lang/String;
    const v11, 0x7f060009

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 631
    .local v10, "undo":Ljava/lang/String;
    const v11, 0x7f06000a

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 633
    .local v3, "redo":Ljava/lang/String;
    sget-object v11, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v11, v12, :cond_0

    .line 634
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 635
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 645
    :cond_0
    :goto_0
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 646
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v10}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 655
    :goto_1
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 656
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 665
    :goto_2
    const-string v11, "dds"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mDrawMaskButtonsEnable ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    if-eqz v11, :cond_a

    .line 667
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v6}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 668
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v7}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 670
    const-string v11, "dds"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mBrushEraser ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    const v12, 0x7f020051

    if-ne v11, v12, :cond_8

    .line 672
    const-string v11, "dds"

    const-string v12, "mBrushEraser = brush_button"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 674
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 675
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 703
    :goto_3
    return-void

    .line 637
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 638
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 640
    :cond_2
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 648
    :cond_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 649
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 651
    :cond_4
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v10}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 658
    :cond_5
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 659
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 661
    :cond_6
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 677
    :cond_7
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 678
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 681
    :cond_8
    const-string v11, "dds"

    const-string v12, "mBrushEraser = erase_button"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 683
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 684
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 686
    :cond_9
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 687
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 692
    :cond_a
    const-string v11, "dds"

    const-string v12, "mRestoreLayout mBrushEraserLayout mDoneLayout disable"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 694
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 695
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 696
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 698
    :cond_b
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 699
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 700
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v7}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method private unInitCheckedFrameList()V
    .locals 1

    .prologue
    .line 1980
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1986
    :cond_0
    :goto_0
    return-void

    .line 1984
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1985
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private unInitFrameList()V
    .locals 5

    .prologue
    .line 1989
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 2003
    :cond_0
    :goto_0
    return-void

    .line 1993
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1994
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 1995
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;

    .line 1996
    .local v2, "tmp":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    iget-object v3, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1997
    iget-object v3, v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;->mGreyFrame:Landroid/graphics/Bitmap;

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1994
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1999
    .end local v2    # "tmp":Lcom/arcsoft/magicshotstudio/ui/drama/FrameData;
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2000
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    .line 2002
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitCheckedFrameList()V

    goto :goto_0
.end method

.method private unInitSound()V
    .locals 0

    .prologue
    .line 1781
    return-void
.end method

.method private updateGapValue(ZF)V
    .locals 3
    .param p1, "isLandScape"    # Z
    .param p2, "mScreenDensity"    # F

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 198
    if-eqz p1, :cond_0

    .line 199
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 225
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public animDefaultBarIn()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1399
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-nez v5, :cond_0

    .line 1423
    :goto_0
    return-void

    .line 1403
    :cond_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    if-nez v5, :cond_1

    .line 1404
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 1405
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 1406
    .local v4, "move":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const-string v6, "translationY"

    new-array v7, v12, [F

    neg-int v8, v4

    int-to-float v8, v8

    aput v8, v7, v11

    aput v10, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1408
    .local v2, "deaultbarIn":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getHeight()I

    move-result v6

    add-int v4, v5, v6

    .line 1410
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const-string v6, "translationY"

    new-array v7, v12, [F

    int-to-float v8, v4

    aput v8, v7, v11

    aput v10, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1412
    .local v3, "framelistIn":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    const-string v6, "translationY"

    new-array v7, v12, [F

    int-to-float v8, v4

    aput v8, v7, v11

    aput v10, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1414
    .local v0, "AroowOut":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v6, "translationY"

    new-array v7, v12, [F

    int-to-float v8, v4

    aput v8, v7, v11

    aput v10, v7, v9

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1417
    .local v1, "TextOut":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1420
    .end local v0    # "AroowOut":Landroid/animation/Animator;
    .end local v1    # "TextOut":Landroid/animation/Animator;
    .end local v2    # "deaultbarIn":Landroid/animation/Animator;
    .end local v3    # "framelistIn":Landroid/animation/Animator;
    .end local v4    # "move":I
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 1422
    iput-boolean v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsAnimDefaultBarIn:Z

    goto :goto_0
.end method

.method public animDefaultBarOut()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1435
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-nez v5, :cond_0

    .line 1459
    :goto_0
    return-void

    .line 1439
    :cond_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    if-nez v5, :cond_1

    .line 1440
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 1441
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    .line 1442
    .local v4, "move":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const-string v6, "translationY"

    new-array v7, v12, [F

    aput v10, v7, v9

    neg-int v8, v4

    int-to-float v8, v8

    aput v8, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1444
    .local v2, "deaultbarOut":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getHeight()I

    move-result v6

    add-int v4, v5, v6

    .line 1446
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const-string v6, "translationY"

    new-array v7, v12, [F

    aput v10, v7, v9

    int-to-float v8, v4

    aput v8, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1448
    .local v3, "framelistOut":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    const-string v6, "translationY"

    new-array v7, v12, [F

    aput v10, v7, v9

    int-to-float v8, v4

    aput v8, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1450
    .local v0, "AroowOut":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v6, "translationY"

    new-array v7, v12, [F

    aput v10, v7, v9

    int-to-float v8, v4

    aput v8, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1453
    .local v1, "TextOut":Landroid/animation/Animator;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1456
    .end local v0    # "AroowOut":Landroid/animation/Animator;
    .end local v1    # "TextOut":Landroid/animation/Animator;
    .end local v2    # "deaultbarOut":Landroid/animation/Animator;
    .end local v3    # "framelistOut":Landroid/animation/Animator;
    .end local v4    # "move":I
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 1458
    iput-boolean v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsAnimDefaultBarIn:Z

    goto :goto_0
.end method

.method public changeMode(Z)V
    .locals 5
    .param p1, "isAuto2Manual"    # Z

    .prologue
    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 1135
    if-eqz p1, :cond_0

    .line 1137
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setImageIndex(I)V

    .line 1138
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setManualMask(Z)V

    .line 1140
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1141
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1142
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMaskActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1143
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1144
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1145
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1146
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 1147
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1149
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-string v2, "UserStack"

    invoke-virtual {v1, v2, v3}, Landroid/app/Application;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    .line 1179
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1180
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;->setmIsMunualMode(Z)V

    .line 1181
    return-void

    .line 1152
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->eraseAllMask()V

    .line 1153
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setManualMask(Z)V

    .line 1154
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getIsMaskShow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1155
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setImageIndex(I)V

    .line 1161
    :goto_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1162
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1163
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMaskActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1164
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1165
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1166
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1168
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    if-eqz v0, :cond_1

    .line 1169
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->clear()V

    .line 1170
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1171
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    .line 1175
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1176
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    goto :goto_0

    .line 1158
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setImageIndex(I)V

    goto :goto_1
.end method

.method public changeSaveBtnState(Z)V
    .locals 5
    .param p1, "isSaveState"    # Z

    .prologue
    const v3, 0x7f060018

    const v2, 0x7f06000c

    .line 1092
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1093
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const v1, 0x7f02000f

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1094
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1096
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 1097
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1099
    .local v0, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1101
    .end local v0    # "description":Ljava/lang/String;
    :cond_1
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    .line 1102
    return-void

    .line 1093
    :cond_2
    const v1, 0x7f020013

    goto :goto_0

    :cond_3
    move v1, v3

    .line 1094
    goto :goto_1

    :cond_4
    move v2, v3

    .line 1097
    goto :goto_2
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "defile"    # Ljava/lang/String;

    .prologue
    .line 1599
    if-nez p1, :cond_0

    .line 1600
    iget-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    .line 1602
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1603
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1604
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1606
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1607
    .local v0, "data":Landroid/net/Uri;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->sendBroadcast(Landroid/content/Intent;)V

    .line 1608
    return-void
.end method

.method public getApplyMask()[B
    .locals 1

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    return-object v0
.end method

.method public getOperateEnable()Z
    .locals 1

    .prologue
    .line 577
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOperateEnable:Z

    return v0
.end method

.method public getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    return-object v0
.end method

.method public hideAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2192
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2193
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2194
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2195
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2197
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2198
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2199
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowSelectDescription:Z

    .line 2200
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 2201
    return-void
.end method

.method public hidePopupToast()V
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->hide()V

    .line 835
    :cond_0
    return-void
.end method

.method public hideProgressDialog()V
    .locals 1

    .prologue
    .line 1804
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 1805
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1807
    :cond_0
    return-void
.end method

.method public initAnimation()V
    .locals 14

    .prologue
    .line 1241
    const/high16 v4, 0x42dc0000    # 110.0f

    .line 1242
    .local v4, "move_len":F
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    if-eqz v9, :cond_0

    .line 1243
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v4, v9

    .line 1246
    :cond_0
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_0

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    .line 1248
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_1

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    .line 1252
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    .line 1256
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v4, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1258
    .local v0, "downArrowMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    const-string v10, "rotation"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_2

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1260
    .local v1, "downArrowRotate":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v4, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1264
    .local v2, "downFrameListMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_3

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1266
    .local v3, "downText":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v9, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1269
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    .line 1273
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x1

    aput v4, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1275
    .local v5, "upArrowMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    const-string v10, "rotation"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_4

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1277
    .local v6, "upArrowRotate":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x1

    aput v4, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 1280
    .local v7, "upFrameListMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_5

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 1282
    .local v8, "upText":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v9, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1284
    new-instance v9, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;

    invoke-direct {v9, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$7;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    invoke-virtual {v8, v9}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1308
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v10, "rotationX"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_6

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    .line 1313
    return-void

    .line 1246
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1248
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1258
    :array_2
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data

    .line 1264
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1275
    :array_4
    .array-data 4
        0x43340000    # 180.0f
        0x43b40000    # 360.0f
    .end array-data

    .line 1280
    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1308
    :array_6
    .array-data 4
        0x42b40000    # 90.0f
        0x0
    .end array-data
.end method

.method public initDefaultActionbarUI()V
    .locals 9

    .prologue
    const v6, 0x7f06001c

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 352
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090041

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    .line 354
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    const v4, 0x7f090074

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 355
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v3, v6}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(I)V

    .line 356
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090053

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    .line 357
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090075

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    .line 358
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 359
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 360
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f09004c

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 361
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 362
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090059

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    .line 363
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090042

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    .line 364
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/Drama;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 366
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v3

    if-nez v3, :cond_0

    .line 367
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 369
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 371
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090077

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    .line 373
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090055

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    .line 375
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackImageView:Landroid/widget/ImageView;

    .line 377
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f09005c

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuImageView:Landroid/widget/ImageView;

    .line 380
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090043

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 384
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090057

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    .line 385
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090079

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditTextView:Landroid/widget/TextView;

    .line 386
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090051

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackTextView:Landroid/widget/TextView;

    .line 387
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f09005e

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    .line 390
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090058

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    .line 391
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f090052

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    .line 392
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f09007a

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    .line 393
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v4, 0x7f09005f

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    .line 395
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 396
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_1

    .line 397
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 398
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 399
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 400
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 402
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_2

    .line 403
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 404
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 405
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 407
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_3

    .line 408
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 410
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 413
    :cond_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_4

    .line 414
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 416
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 419
    :cond_4
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_5

    .line 420
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 422
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 426
    :cond_5
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    .line 427
    .local v2, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 428
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 441
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setEditEnable()V

    .line 443
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 444
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;

    .line 445
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    .line 446
    return-void

    .line 430
    :cond_6
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 431
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 433
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 434
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 435
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    .line 436
    .local v0, "density":F
    const/high16 v3, 0x41600000    # 14.0f

    mul-float/2addr v3, v0

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v3, v4

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 437
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public initDrawMaskActionbarUI()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 227
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mMaskActionBar:Landroid/widget/RelativeLayout;

    .line 229
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090070

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    .line 231
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 233
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09007d

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    .line 235
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090081

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    .line 237
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 238
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090085

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    .line 240
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 241
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09007f

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    .line 242
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090089

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    .line 243
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 244
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09008c

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    .line 245
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 246
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09008a

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    .line 247
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09008d

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    .line 249
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090083

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreSelector:Landroid/widget/ImageView;

    .line 250
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090087

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEraserSelector:Landroid/widget/ImageView;

    .line 251
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreSelector:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 252
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEraserSelector:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 254
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090086

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    .line 256
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090082

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    .line 259
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f09007e

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 260
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v3, 0x7f090080

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 261
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "cancel":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 263
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 264
    .local v1, "done":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 265
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v3, 0x7f020067

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 272
    const v2, 0x7f020051

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraser:I

    .line 273
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setRubberValid()V

    .line 274
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 275
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 277
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 278
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 280
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 281
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 283
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 285
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 286
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 288
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 291
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_3

    .line 292
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 294
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 297
    :cond_3
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    if-eqz v2, :cond_4

    .line 298
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 299
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 301
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 302
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 305
    :cond_4
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    if-eqz v2, :cond_5

    .line 306
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 307
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 309
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 310
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 313
    :cond_5
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter_CheckedFrame;

    .line 314
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    .line 315
    return-void
.end method

.method public initEditData(Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;)Z
    .locals 1
    .param p1, "callback"    # Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;

    .prologue
    .line 1810
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->fetchCandidate()V

    .line 1811
    const/16 v0, 0x5f

    invoke-interface {p1, v0}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;->progress(I)V

    .line 1812
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->fetchSelectedFlags()V

    .line 1813
    const/16 v0, 0x63

    invoke-interface {p1, v0}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;->progress(I)V

    .line 1814
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->fetchAutoResult()V

    .line 1815
    const/16 v0, 0x64

    invoke-interface {p1, v0}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;->progress(I)V

    .line 1816
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initFrameList()V

    .line 1818
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initManualModeUI(Landroid/app/Activity;[ILjava/lang/String;ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;)V
    .locals 8
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "diplaySize"    # [I
    .param p3, "savePath"    # Ljava/lang/String;
    .param p4, "ori"    # I
    .param p5, "engine"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 455
    new-instance v5, Ljava/util/concurrent/Semaphore;

    invoke-direct {v5, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 456
    sget-object v5, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    .line 457
    check-cast p1, Lcom/arcsoft/magicshotstudio/Drama;

    .end local p1    # "context":Landroid/app/Activity;
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    .line 458
    new-instance v5, Ljava/util/concurrent/Semaphore;

    invoke-direct {v5, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    .line 459
    iput p4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    .line 461
    iput-object p3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    .line 462
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f09003c

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .line 463
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v5, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setManual(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 465
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f09006b

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    .line 467
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f09006c

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    .line 469
    aget v5, p2, v6

    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSrcImageWidth:I

    .line 470
    aget v5, p2, v3

    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSrcImageHeight:I

    .line 472
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setVisibility(I)V

    .line 473
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f090066

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    .line 474
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    if-eqz v5, :cond_0

    .line 475
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->init()V

    .line 476
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->updateScreenSize()V

    .line 477
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->reset()V

    .line 480
    :cond_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f09006e

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    .line 482
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f09006a

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 484
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f090069

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    .line 487
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f09006f

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 489
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f090072

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    .line 491
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f090071

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    .line 494
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mArrowClickLis:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 496
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-direct {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    .line 497
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    invoke-direct {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    .line 498
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;->setZoomState(Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;)V

    .line 500
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setShowBounceViewListener(Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;)V

    .line 501
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOrientation:I

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setRotateDegress(I)V

    .line 502
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setImageZoomState(Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;)V

    .line 503
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 504
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;->setmIsMunualMode(Z)V

    .line 505
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;->setShowBounceViewListener(Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;)V

    .line 507
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initSound()V

    .line 509
    iput-object p5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    .line 510
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    const v7, 0x7f090067

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    .line 512
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initDefaultActionbarUI()V

    .line 513
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initDrawMaskActionbarUI()V

    .line 515
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 516
    .local v0, "currConfig":Landroid/content/res/Configuration;
    iget v5, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v5, v7, :cond_1

    .line 517
    .local v3, "landScape":Z
    :goto_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    .line 518
    .local v1, "density":F
    invoke-direct {p0, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateGapValue(ZF)V

    .line 519
    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V

    .line 520
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    .line 521
    .local v4, "observer":Landroid/view/ViewTreeObserver;
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$1;

    invoke-direct {v5, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 527
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->inflateRollWaitingView()V

    .line 528
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/Drama;->getExitDialog()Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 529
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-direct {v2, v5}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V

    .line 530
    .local v2, "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v5, v2}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setBtnOnClick(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 531
    return-void

    .end local v1    # "density":F
    .end local v2    # "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    .end local v3    # "landScape":Z
    .end local v4    # "observer":Landroid/view/ViewTreeObserver;
    :cond_1
    move v3, v6

    .line 516
    goto :goto_0
.end method

.method public isBtnClickEnable()Z
    .locals 1

    .prologue
    .line 937
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBtnClickEnable:Z

    return v0
.end method

.method public isSaveState()Z
    .locals 1

    .prologue
    .line 539
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    .line 540
    .local v0, "ret":Z
    return v0
.end method

.method public isSelectFrameView()Z
    .locals 3

    .prologue
    .line 534
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    move-result-object v1

    sget-object v2, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 535
    .local v0, "ret":Z
    :goto_0
    return v0

    .line 534
    .end local v0    # "ret":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2014
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z

    .line 2015
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v0, v1, :cond_1

    .line 2016
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->cancelMask()V

    .line 2017
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 2018
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-nez v0, :cond_0

    .line 2019
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V

    .line 2040
    :cond_0
    :goto_0
    return v2

    .line 2022
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v0, v1, :cond_2

    .line 2023
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->backToSelectFrame()V

    .line 2024
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 2025
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-nez v0, :cond_0

    .line 2026
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V

    goto :goto_0

    .line 2030
    :cond_2
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowSelectDescription:Z

    .line 2031
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    if-eqz v0, :cond_0

    .line 2033
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Drama;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 2037
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2056
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 2057
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    if-eqz v2, :cond_0

    .line 2058
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->updateScreenSize()V

    .line 2059
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/BounceView;->reset()V

    .line 2062
    :cond_0
    const/4 v2, 0x2

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    .line 2063
    .local v1, "landscape":Z
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Drama;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 2064
    .local v0, "density":F
    invoke-direct {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateGapValue(ZF)V

    .line 2065
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V

    .line 2066
    return-void

    .line 2062
    .end local v0    # "density":F
    .end local v1    # "landscape":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public processManualMask([BIIZ)Z
    .locals 16
    .param p1, "grey8"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isBrush"    # Z

    .prologue
    .line 1190
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 1191
    .local v12, "start":J
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v2, v0

    if-gtz v2, :cond_1

    .line 1192
    :cond_0
    const/4 v11, 0x0

    .line 1233
    :goto_0
    return v11

    .line 1195
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1196
    .local v14, "time":J
    const/4 v11, 0x0

    .line 1197
    .local v11, "success":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMode: grey8 length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " width:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isBrush :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    new-instance v8, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    invoke-direct {v8}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;-><init>()V

    .line 1202
    .local v8, "returnBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->applyMask(I[BIIZLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v10

    .line 1205
    .local v10, "result":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResultImageByMask result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMask() cost time = GetResultImageByMask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "processManualMode: A "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    if-nez v10, :cond_2

    iget-object v2, v8, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget-object v2, v8, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1220
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameMaskIndex:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getShotMaskByIndex(I)[B

    move-result-object v9

    .line 1221
    .local v9, "mask":[B
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShotMask:[B

    .line 1222
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "zdd processManualMask() cost time = getDefaultMaskByIndex "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v2, v9}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setMask([B)V

    .line 1227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "processManualMode: B "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "zdd processManualMask() cost time = setMask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMode out cost time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v12

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1213
    .end local v9    # "mask":[B
    :cond_3
    const/4 v11, 0x1

    .line 1214
    iget-object v2, v8, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 1215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMask() cost time = updateImage "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    goto/16 :goto_1
.end method

.method public redo()V
    .locals 4

    .prologue
    .line 318
    const-string v1, "xsj"

    const-string v2, "redo start "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->redo()Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;

    move-result-object v0

    .line 320
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;->mask:[B

    iget-boolean v3, v0, Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;->redo_isBrush:Z

    invoke-virtual {v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->doMask([BZ)V

    .line 322
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkUndoRedoState()V

    .line 323
    const-string v1, "xsj"

    const-string v2, "redo end "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public rememberVisibility()V
    .locals 1

    .prologue
    .line 2183
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_Actionbar:I

    .line 2184
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_FramView:I

    .line 2185
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_mDownArrows:I

    .line 2186
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibiltiy_mEditMaskBar:I

    .line 2187
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_mUpArrows:I

    .line 2188
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_Description:I

    .line 2189
    return-void
.end method

.method public restoreViewsVisibility()V
    .locals 2

    .prologue
    .line 2210
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_Actionbar:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2211
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_FramView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2212
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_mDownArrows:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2213
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibiltiy_mEditMaskBar:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2215
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_mUpArrows:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2217
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v0, v1, :cond_0

    .line 2218
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2223
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 2224
    return-void

    .line 2220
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mVisibility_Description:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public saveOperate([BZ)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "isBrush"    # Z

    .prologue
    .line 344
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->saveOperate([BZ)V

    .line 346
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkUndoRedoState()V

    .line 348
    :cond_0
    return-void
.end method

.method public saveResult()Z
    .locals 1

    .prologue
    .line 1541
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v0, :cond_0

    .line 1542
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResultFile()Z

    move-result v0

    .line 1545
    :goto_0
    return v0

    .line 1544
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 1545
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public saveResultFile()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1556
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFileSaving:Z

    if-eqz v2, :cond_0

    .line 1557
    const/4 v1, 0x0

    .line 1595
    :goto_0
    return v1

    .line 1559
    :cond_0
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFileSaving:Z

    .line 1561
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveThread:Ljava/lang/Thread;

    if-eqz v2, :cond_1

    .line 1563
    :try_start_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1567
    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveThread:Ljava/lang/Thread;

    .line 1570
    :cond_1
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$9;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveThread:Ljava/lang/Thread;

    .line 1593
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1594
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1564
    :catch_0
    move-exception v0

    .line 1565
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public setBtnClickEnable(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 934
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mBtnClickEnable:Z

    .line 935
    return-void
.end method

.method public setEditDisable()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 551
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 556
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 558
    :cond_1
    return-void
.end method

.method public setEditEnable()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 561
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 566
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 568
    :cond_1
    return-void
.end method

.method public setIsMoveActionWorkForZoomListener()V
    .locals 4

    .prologue
    .line 74
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->isMoveActionWork(Z)Z

    move-result v0

    .line 75
    .local v0, "isHorizontalMoveWork":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->isMoveActionWork(Z)Z

    move-result v1

    .line 76
    .local v1, "isVerticalMoveWork":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/SimpleImageZoomListener;->setMoveActionWork(ZZ)V

    .line 78
    return-void
.end method

.method public setOperateEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 573
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOperateEnable:Z

    .line 574
    return-void
.end method

.method public showProgressDialog()V
    .locals 1

    .prologue
    .line 1798
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 1799
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1801
    :cond_0
    return-void
.end method

.method public showTopActionBar()V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 452
    :cond_0
    return-void
.end method

.method public switchAllViews()V
    .locals 1

    .prologue
    .line 2239
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ViewsShow:Z

    if-eqz v0, :cond_0

    .line 2240
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->rememberVisibility()V

    .line 2241
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hideAllViews()V

    .line 2242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ViewsShow:Z

    .line 2247
    :goto_0
    return-void

    .line 2244
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->restoreViewsVisibility()V

    .line 2245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ViewsShow:Z

    goto :goto_0
.end method

.method public switchDefaultActionBar()V
    .locals 1

    .prologue
    .line 1375
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1377
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsAnimDefaultBarIn:Z

    if-eqz v0, :cond_1

    .line 1382
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->animDefaultBarOut()V

    .line 1393
    :cond_0
    :goto_0
    return-void

    .line 1389
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->animDefaultBarIn()V

    goto :goto_0
.end method

.method public unInit()V
    .locals 1

    .prologue
    .line 1462
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 1463
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1465
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    if-eqz v0, :cond_0

    .line 1466
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1467
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->uninit()I

    .line 1468
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1471
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1472
    return-void
.end method

.method public unInitAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1316
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1317
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    .line 1318
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1319
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    .line 1320
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 1321
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    .line 1323
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 1324
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 1325
    return-void
.end method

.method public unInitUI()V
    .locals 1

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    if-eqz v0, :cond_0

    .line 1476
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->unInit()V

    .line 1478
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitFrameList()V

    .line 1479
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitAnimation()V

    .line 1480
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitSound()V

    .line 1481
    return-void
.end method

.method public undo()V
    .locals 4

    .prologue
    .line 327
    const-string v1, "xsj"

    const-string v2, "undo start "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->undo()Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;

    move-result-object v0

    .line 329
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;->mask:[B

    iget-boolean v1, v0, Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;->undo_isBrush:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->doMask([BZ)V

    .line 331
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkUndoRedoState()V

    .line 332
    const-string v1, "xsj"

    const-string v2, "undo end "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void

    .line 329
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->updateResultImage(Landroid/graphics/Bitmap;)V

    .line 545
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setImageIndex(I)V

    .line 546
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->initMask()V

    .line 547
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->invalidate()V

    .line 548
    return-void
.end method

.method public updateImageAndFrameListView()V
    .locals 2

    .prologue
    .line 1822
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->checkEditable()V

    .line 1824
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1825
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;->setFrameList(Ljava/util/ArrayList;)V

    .line 1826
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/drama/FrameAdapter;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1827
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1829
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 1830
    return-void
.end method
