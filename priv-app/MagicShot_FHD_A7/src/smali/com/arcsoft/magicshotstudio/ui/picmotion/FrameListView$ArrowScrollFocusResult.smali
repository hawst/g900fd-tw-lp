.class Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;
.super Ljava/lang/Object;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArrowScrollFocusResult"
.end annotation


# instance fields
.field private mAmountToScroll:I

.field private mSelectedPosition:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 6574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;

    .prologue
    .line 6574
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;-><init>()V

    return-void
.end method


# virtual methods
.method public getAmountToScroll()I
    .locals 1

    .prologue
    .line 6591
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->mAmountToScroll:I

    return v0
.end method

.method public getSelectedPosition()I
    .locals 1

    .prologue
    .line 6587
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->mSelectedPosition:I

    return v0
.end method

.method populate(II)V
    .locals 0
    .param p1, "selectedPosition"    # I
    .param p2, "amountToScroll"    # I

    .prologue
    .line 6582
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->mSelectedPosition:I

    .line 6583
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->mAmountToScroll:I

    .line 6584
    return-void
.end method
