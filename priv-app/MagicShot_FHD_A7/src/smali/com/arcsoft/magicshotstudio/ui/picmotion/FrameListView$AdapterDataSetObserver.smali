.class Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field private mInstanceState:Landroid/os/Parcelable;

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 1

    .prologue
    .line 6274
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 6275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;

    .prologue
    .line 6274
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 6279
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    const/4 v1, 0x1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$502(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Z)Z

    .line 6280
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I

    move-result v1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1702(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I

    .line 6281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1802(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I

    .line 6285
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I

    move-result v0

    if-lez v0, :cond_0

    .line 6287
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 6288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 6293
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkFocus()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2100(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    .line 6294
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    .line 6295
    return-void

    .line 6290
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->rememberSyncState()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 6299
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    const/4 v1, 0x1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$502(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Z)Z

    .line 6301
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6304
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->mInstanceState:Landroid/os/Parcelable;

    .line 6308
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I

    move-result v1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1702(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I

    .line 6309
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I
    invoke-static {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1802(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I

    .line 6311
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2202(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I

    .line 6312
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J
    invoke-static {v0, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2302(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;J)J

    .line 6314
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2402(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I

    .line 6315
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J
    invoke-static {v0, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2502(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;J)J

    .line 6317
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z
    invoke-static {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2602(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Z)Z

    .line 6319
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkFocus()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2100(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    .line 6320
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    .line 6321
    return-void
.end method
