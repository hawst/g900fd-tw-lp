.class public Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
.super Landroid/app/Dialog;
.source "WaitingProgressDialog.java"


# static fields
.field public static final PROGRESS_MAX:I = 0x64


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const v0, 0x7f070003

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 22
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->init(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 16
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 17
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mBitmap:Landroid/graphics/Bitmap;

    .line 27
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->init(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 16
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 17
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mBitmap:Landroid/graphics/Bitmap;

    .line 33
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->init(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 37
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mContext:Landroid/content/Context;

    .line 38
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setContentView(I)V

    .line 40
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setCancelable(Z)V

    .line 41
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 43
    const v0, 0x7f090097

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 44
    const v0, 0x7f090096

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mTextView:Landroid/widget/TextView;

    .line 49
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->reset()V

    .line 51
    return-void
.end method


# virtual methods
.method public dimissDelay(J)V
    .locals 3
    .param p1, "delayMillis"    # J

    .prologue
    .line 61
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog$1;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/widget/ProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 67
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 77
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method

.method public show()V
    .locals 0

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->reset()V

    .line 72
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 73
    return-void
.end method

.method public updateProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    const/16 v0, 0x64

    .line 54
    if-gez p1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    if-lt p1, v0, :cond_1

    move p1, v0

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method
