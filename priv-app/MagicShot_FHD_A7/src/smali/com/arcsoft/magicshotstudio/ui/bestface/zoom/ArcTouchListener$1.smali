.class Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;
.super Ljava/lang/Object;
.source "ArcTouchListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->setGestureScanner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 90
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->getOperateEnable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 92
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mMySingleDoubleClickListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;->onDoubleClick(Landroid/view/MotionEvent;)V

    .line 94
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-result-object v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)I

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)I

    move-result v4

    invoke-virtual {v2, p1, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->updatePan(Landroid/view/MotionEvent;II)V

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewWidth:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 96
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mViewHeight:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 97
    .local v1, "y":F
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->doubleTapZoom(FF)V

    .line 99
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method
