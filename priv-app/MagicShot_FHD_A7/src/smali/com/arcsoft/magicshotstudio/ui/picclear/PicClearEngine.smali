.class public Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
.super Ljava/lang/Object;
.source "PicClearEngine.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# static fields
.field public static final HANDLER_BASIC:I = 0x1

.field private static final PROCESS_IMG_FINISH:I = 0x2

.field private static final PROCESS_IMG_GETRESULT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ArcPicClearEngine"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

.field private mProcessListener:Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;

    .line 30
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mHandler:Landroid/os/Handler;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;)Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public ArcPiClear_SaveImage(Ljava/lang/String;[I)I
    .locals 2
    .param p1, "savePath"    # Ljava/lang/String;
    .param p2, "moveObjectIndexArray"    # [I

    .prologue
    .line 194
    const/4 v0, 0x0

    .line 196
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v1, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_SaveImage(Ljava/lang/String;[I)I

    move-result v0

    .line 200
    :cond_0
    return v0
.end method

.method public ProcessManualObjectInMarkImage([BI)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "grey8"    # [B
    .param p2, "doFlag"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_ProcessManualObjectInMarkImage([BI)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getArcPiClear()Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    return-object v0
.end method

.method public getFirstImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetFirstImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMarkImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetMarkImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMaskRect()[Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x0

    .line 179
    .local v0, "maskRect":[Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    if-eqz v1, :cond_0

    .line 180
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetObjectArea()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/graphics/Rect;

    move-object v0, v1

    check-cast v0, [Landroid/graphics/Rect;

    .line 182
    :cond_0
    return-object v0
.end method

.method public getMovingObjBitmap()[Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "movingObjBitmap":[Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetMovingObjectBitmaps()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/graphics/Bitmap;

    move-object v0, v1

    check-cast v0, [Landroid/graphics/Bitmap;

    .line 190
    :cond_0
    return-object v0
.end method

.method public getNoMarkImage()[B
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetNoMaskImage()[B

    move-result-object v0

    return-object v0
.end method

.method public getProcessObjectInMarkImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_ProcessObjectInMarkImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getProcessObjectInMarkImage_ByRect(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "rectIndex"    # I

    .prologue
    .line 154
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_ProcessObjectInMarkImage_ByRect(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getProcessResult()I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetProcessResult()I

    move-result v0

    return v0
.end method

.method public getRealImageSize()[I
    .locals 3

    .prologue
    .line 99
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 100
    .local v0, "realImageSize":[I
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetCropImageSize()Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v2

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    aput v2, v0, v1

    .line 101
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetCropImageSize()Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v2

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    aput v2, v0, v1

    .line 102
    return-object v0
.end method

.method public getResult()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetResult()[B

    move-result-object v0

    return-object v0
.end method

.method public initPiclearEngine(IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 8
    .param p1, "photoNum"    # I
    .param p2, "mode"    # I
    .param p3, "realImageSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;
    .param p4, "markImageSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 52
    const-string v4, "ArcPicClearEngine"

    const-string v5, "initPiclearEngine in "

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 55
    .local v0, "initPiclearEnginetime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 56
    .local v2, "time":J
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_Init()I

    .line 57
    const-string v4, "ArcPicClearEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPiclear.ArcPiClear_Init() cost time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 60
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->set_UseRawData(Z)I

    .line 61
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v4, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_SetPhotoCount(I)V

    .line 62
    const-string v4, "ArcPicClearEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPiclear.ArcPiClear_SetPhotoCount() cost time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 65
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    iget v5, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iget v6, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    invoke-virtual {v4, v5, v6}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_SetPhotoSize(II)V

    .line 66
    const-string v4, "ArcPicClearEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPiclear.ArcPiClear_SetPhotoSize() cost time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 69
    iget v4, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-double v4, v4

    iget v6, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-double v6, v6

    div-double/2addr v4, v6

    iget v6, p4, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, p4, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 70
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    iget v5, p4, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iget v6, p4, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    invoke-virtual {v4, v5, v6}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_SetMarkImageSize(II)V

    .line 71
    const-string v4, "ArcPicClearEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPiclear.ArcPiClear_SetMarkImageSize() cost time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 74
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v4, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_SetMode(I)V

    .line 75
    const-string v4, "ArcPicClearEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mPiclear.ArcPiClear_SetMode() cost time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v4, "ArcPicClearEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initPiclearEngine cost time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public processImage(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 113
    .local v0, "ImageProcesstime":J
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 114
    .local v2, "filePath":Ljava/lang/String;
    const-string v5, "ArcPicClearEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file path = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    if-eqz v2, :cond_1

    .line 116
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v5, v2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_ImageProcess(Ljava/lang/String;)I

    move-result v4

    .line 117
    .local v4, "ret":I
    if-eqz v4, :cond_0

    .line 118
    const-string v5, "ArcPicClearEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "error in ArcPiClear_ImageProcess() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 122
    .end local v4    # "ret":I
    :cond_1
    const-string v5, "ArcPicClearEngine"

    const-string v6, "read file failed"

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    .end local v2    # "filePath":Ljava/lang/String;
    :cond_2
    const-string v5, "ArcPicClearEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cost_time decode time = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;

    invoke-interface {v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;->getProcessResult()V

    .line 129
    sget-object v6, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->LOCK_OBJ2:Ljava/lang/Object;

    monitor-enter v6

    .line 130
    :try_start_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 131
    monitor-exit v6

    .line 132
    return-void

    .line 131
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public setExifInfo(JJ)V
    .locals 1
    .param p1, "exifBuff"    # J
    .param p3, "exifBuffSize"    # J

    .prologue
    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->setExifInfo(JJ)I

    .line 82
    return-void
.end method

.method public setPreprocessorData(JI)I
    .locals 1
    .param p1, "address"    # J
    .param p3, "length"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->setPreprocessorData(JI)I

    move-result v0

    return v0
.end method

.method public setProcessListener(Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;

    .line 49
    return-void
.end method

.method public set_LogFileName(Ljava/lang/String;)I
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->set_LogFileName(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public set_LogOpen(Z)I
    .locals 1
    .param p1, "isOpen"    # Z

    .prologue
    .line 174
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->set_LogOpen(Z)I

    move-result v0

    return v0
.end method

.method public set_NV21(J[II)V
    .locals 1
    .param p1, "inputAds"    # J
    .param p3, "indexArray"    # [I
    .param p4, "length"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->set_NV21(J[II)I

    .line 136
    return-void
.end method

.method public unInit()V
    .locals 0

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->unInitPiclearEngine()V

    .line 167
    return-void
.end method

.method public unInitPiclearEngine()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->mPiclear:Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_UnInit()V

    .line 92
    :cond_0
    return-void
.end method
