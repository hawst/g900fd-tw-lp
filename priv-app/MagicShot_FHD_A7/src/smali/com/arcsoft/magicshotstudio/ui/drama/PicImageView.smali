.class public Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;
.super Landroid/view/View;
.source "PicImageView.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;,
        Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$OnSimpleGesture;
    }
.end annotation


# static fields
.field private static final Color_Mask:I = 0x0

.field private static final Color_MaskT:I = -0x44eeeeef

.field private static final DoubleTouchTime:Ljava/lang/Long;

.field private static final MidPenWidth:I = 0x32

.field public static final SHOW_AUTO_NO_MASK_BITMAP:I = 0x0

.field public static final SHOW_AUTO_WITH_MASK_BITMAP:I = 0x1

.field public static final SHOW_MANUAL_BITMAP:I = 0x2

.field public static final SHOW_RESULT_BITMAP:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ArcSoft_PicImageView"

.field public static final TOUCH_TOLERANCE:I = 0x4


# instance fields
.field mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

.field private mAspectQuotient:F

.field private mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

.field private mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

.field private mBitmap:Landroid/graphics/Bitmap;

.field mBitmapDrawHeight:F

.field mBitmapDrawWidth:F

.field private mBitmapMaskPaint:Landroid/graphics/Paint;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mBitmapSizeChanged:Z

.field private mBufferArray:[B

.field private mCameraDegress:I

.field private mContext:Landroid/content/Context;

.field private mCurrentDirection:I

.field mDoubleTapDx:F

.field mDoubleTapDy:F

.field private mDoubleZoomOut:F

.field private mFirstTouchTime:Ljava/lang/Long;

.field private mImageAction:Z

.field private mImageMove:Z

.field private mImgheight:I

.field private mImgwidth:I

.field private mIsAutoNoMaskBitmapNull:Z

.field private mIsRememberVisibility:Z

.field private mLastDirection:I

.field mLongPressed:Z

.field public mManualBitmap:Landroid/graphics/Bitmap;

.field private mManualMask:Z

.field private mMaskBitmap:Landroid/graphics/Bitmap;

.field private mMaskCanvas:Landroid/graphics/Canvas;

.field mMoved:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mPathDirty:Z

.field private mPathMask:Landroid/graphics/Path;

.field mProcessManualMask:Ljava/lang/Runnable;

.field private mPtDown:Landroid/graphics/Point;

.field private mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mRectDst:Landroid/graphics/Rect;

.field private mRectSrc:Landroid/graphics/Rect;

.field public mResultBitmap:Landroid/graphics/Bitmap;

.field private mSecondTouchTime:Ljava/lang/Long;

.field private mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

.field private mSimpleGestureDetector:Landroid/view/GestureDetector;

.field private mSyncObject:Ljava/lang/Object;

.field mTap:Z

.field mTouchHelper:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;

.field mTouchLock:Z

.field private mZoomInRate:F

.field private mZoomOutRate:F

.field private mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

.field private m_OffsetX:I

.field private m_OffsetY:I

.field private m_ScaleX:F

.field private m_ScaleY:F

.field private m_lastPenWidth:I

.field private mbIsZoom:Z

.field private mbIsZoomOut:Z

.field private mbShowMask:Z

.field private mbSingleDown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    const-wide/16 v0, 0x64

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->DoubleTouchTime:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I

    .line 47
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I

    .line 48
    const/16 v0, 0x32

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_lastPenWidth:I

    .line 49
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAspectQuotient:F

    .line 50
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    .line 51
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageMove:Z

    .line 52
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageAction:Z

    .line 53
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mFirstTouchTime:Ljava/lang/Long;

    .line 54
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSecondTouchTime:Ljava/lang/Long;

    .line 56
    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    .line 57
    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleY:F

    .line 58
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetX:I

    .line 59
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetY:I

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 64
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapMaskPaint:Landroid/graphics/Paint;

    .line 66
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mContext:Landroid/content/Context;

    .line 68
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    .line 69
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 70
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 71
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    .line 72
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathDirty:Z

    .line 73
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    .line 76
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSyncObject:Ljava/lang/Object;

    .line 80
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCameraDegress:I

    .line 85
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoom:Z

    .line 86
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoomOut:Z

    .line 87
    const v0, 0x3f59999a    # 0.85f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomOutRate:F

    .line 88
    const v0, 0x3f91eb85    # 1.14f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomInRate:F

    .line 89
    const v0, 0x40133333    # 2.3f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleZoomOut:F

    .line 175
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsAutoNoMaskBitmapNull:Z

    .line 214
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapSizeChanged:Z

    .line 474
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    .line 547
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    .line 548
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    .line 549
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTap:Z

    .line 550
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleTapDx:F

    .line 551
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleTapDy:F

    .line 617
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchHelper:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;

    .line 652
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    .line 653
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbShowMask:Z

    .line 654
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 656
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 657
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    .line 658
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    .line 659
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    .line 661
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchLock:Z

    .line 663
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbSingleDown:Z

    .line 795
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mProcessManualMask:Ljava/lang/Runnable;

    .line 876
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBufferArray:[B

    .line 1012
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    .line 1013
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    .line 1167
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawWidth:F

    .line 1168
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawHeight:F

    .line 105
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->init(Landroid/content/Context;)V

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    return-object v0
.end method

.method static synthetic access$202(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoom:Z

    return p1
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoomOut:Z

    return p1
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .prologue
    .line 30
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I

    return v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    .prologue
    .line 30
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I

    return v0
.end method

.method private adjustZoomState(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;FF)V
    .locals 7
    .param p1, "bitmapWidth"    # I
    .param p2, "bitmapHeight"    # I
    .param p3, "viewWidth"    # I
    .param p4, "viewHeight"    # I
    .param p5, "srcRect"    # Landroid/graphics/Rect;
    .param p6, "dstRect"    # Landroid/graphics/Rect;
    .param p7, "zoomX"    # F
    .param p8, "zoomY"    # F

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1171
    const/4 v0, 0x0

    .line 1172
    .local v0, "panX_new":F
    const/4 v1, 0x0

    .line 1174
    .local v1, "panY_new":F
    invoke-virtual {p6}, Landroid/graphics/Rect;->width()I

    move-result v2

    mul-int/2addr v2, p1

    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawWidth:F

    .line 1175
    invoke-virtual {p6}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v2, p2

    invoke-virtual {p5}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawHeight:F

    .line 1177
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawWidth:F

    int-to-float v3, p3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_3

    .line 1178
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-lez v2, :cond_2

    .line 1179
    int-to-float v2, p3

    mul-float v3, p7, v5

    div-float/2addr v2, v3

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 1180
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanX(F)V

    .line 1181
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v6, v2, Landroid/graphics/Rect;->left:I

    .line 1202
    :cond_0
    :goto_0
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawHeight:F

    int-to-float v3, p4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_6

    .line 1203
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-lez v2, :cond_5

    .line 1204
    int-to-float v2, p4

    mul-float v3, p8, v5

    div-float/2addr v2, v3

    int-to-float v3, p2

    div-float v1, v2, v3

    .line 1205
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanY(F)V

    .line 1206
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v6, v2, Landroid/graphics/Rect;->top:I

    .line 1227
    :cond_1
    :goto_1
    return-void

    .line 1183
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-ge v2, p1, :cond_0

    .line 1184
    int-to-float v2, p1

    int-to-float v3, p3

    div-float/2addr v3, p7

    sub-float/2addr v2, v3

    int-to-float v3, p3

    mul-float v4, p7, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 1185
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanX(F)V

    .line 1186
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput p1, v2, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 1190
    :cond_3
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-gez v2, :cond_4

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-ge v2, p1, :cond_4

    .line 1191
    int-to-float v2, p3

    mul-float v3, p7, v5

    div-float/2addr v2, v3

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 1192
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanX(F)V

    .line 1193
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v6, v2, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 1195
    :cond_4
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-le v2, p1, :cond_0

    .line 1196
    int-to-float v2, p1

    int-to-float v3, p3

    div-float/2addr v3, p7

    sub-float/2addr v2, v3

    int-to-float v3, p3

    mul-float v4, p7, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 1197
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanX(F)V

    .line 1198
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput p1, v2, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 1208
    :cond_5
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-ge v2, p2, :cond_1

    .line 1209
    int-to-float v2, p2

    int-to-float v3, p4

    div-float/2addr v3, p8

    sub-float/2addr v2, v3

    int-to-float v3, p4

    mul-float v4, p8, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    int-to-float v3, p2

    div-float v1, v2, v3

    .line 1210
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanY(F)V

    .line 1211
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput p2, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1215
    :cond_6
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-gez v2, :cond_7

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-ge v2, p2, :cond_7

    .line 1216
    int-to-float v2, p4

    mul-float v3, p8, v5

    div-float/2addr v2, v3

    int-to-float v3, p2

    div-float v1, v2, v3

    .line 1217
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanY(F)V

    .line 1218
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v6, v2, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    .line 1220
    :cond_7
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v2, p2, :cond_1

    .line 1221
    int-to-float v2, p2

    int-to-float v3, p4

    div-float/2addr v3, p8

    sub-float/2addr v2, v3

    int-to-float v3, p4

    mul-float v4, p8, v5

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    int-to-float v3, p2

    div-float v1, v2, v3

    .line 1222
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanY(F)V

    .line 1223
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput p2, v2, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1
.end method

.method private autoZoomIn()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 984
    const/high16 v3, 0x40000000    # 2.0f

    .line 987
    .local v3, "tempZoom":F
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getZoom()F

    move-result v4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomInRate:F

    mul-float v3, v4, v5

    .line 989
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleZoomOut:F

    div-float v0, v3, v4

    .line 990
    .local v0, "penTate":F
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleTapDx:F

    sub-float/2addr v4, v6

    mul-float/2addr v4, v0

    add-float v1, v6, v4

    .line 991
    .local v1, "tempPx":F
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleTapDy:F

    sub-float/2addr v4, v6

    mul-float/2addr v4, v0

    add-float v2, v6, v4

    .line 993
    .local v2, "tempPy":F
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setZoom(F)V

    .line 994
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanX(F)V

    .line 995
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setPanY(F)V

    .line 997
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mDoubleZoomOut:F

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    .line 998
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoom:Z

    .line 999
    :cond_0
    return-void
.end method

.method private autoZoomOut()V
    .locals 4

    .prologue
    .line 1003
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getMinZoom()F

    move-result v0

    .line 1004
    .local v0, "minZoomRat":F
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getZoom()F

    move-result v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomOutRate:F

    mul-float v1, v2, v3

    .line 1005
    .local v1, "tempZoom":F
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 1006
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setZoom(F)V

    .line 1007
    cmpg-float v2, v1, v0

    if-gtz v2, :cond_0

    .line 1008
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoom:Z

    .line 1010
    :cond_0
    return-void
.end method

.method private calculateAspectQuotient()V
    .locals 8

    .prologue
    .line 342
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_2

    .line 343
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 344
    .local v1, "bitmapWidth":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 345
    .local v0, "bitmapHeight":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getWidth()I

    move-result v3

    .line 346
    .local v3, "imageViewWidth":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getHeight()I

    move-result v2

    .line 347
    .local v2, "imageViewHeight":I
    const/4 v4, 0x0

    .line 348
    .local v4, "ratioBitmap":F
    const/4 v5, 0x0

    .line 349
    .local v5, "ratioImageView":F
    if-eqz v0, :cond_0

    .line 350
    int-to-float v6, v1

    int-to-float v7, v0

    div-float v4, v6, v7

    .line 352
    :cond_0
    if-eqz v2, :cond_1

    .line 353
    int-to-float v6, v3

    int-to-float v7, v2

    div-float v5, v6, v7

    .line 355
    :cond_1
    const/4 v6, 0x0

    cmpl-float v6, v6, v5

    if-eqz v6, :cond_2

    .line 356
    div-float v6, v4, v5

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAspectQuotient:F

    .line 359
    .end local v0    # "bitmapHeight":I
    .end local v1    # "bitmapWidth":I
    .end local v2    # "imageViewHeight":I
    .end local v3    # "imageViewWidth":I
    .end local v4    # "ratioBitmap":F
    .end local v5    # "ratioImageView":F
    :cond_2
    return-void
.end method

.method private getCurrentOffsetAndScale()V
    .locals 14

    .prologue
    const/high16 v13, 0x40000000    # 2.0f

    const/4 v12, 0x0

    .line 922
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getWidth()I

    move-result v7

    .line 923
    .local v7, "viewWidth":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getHeight()I

    move-result v6

    .line 924
    .local v6, "viewHeight":I
    move v1, v7

    .line 925
    .local v1, "bitmapWidth":I
    move v0, v6

    .line 927
    .local v0, "bitmapHeight":I
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v10, :cond_0

    .line 929
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 930
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 933
    :cond_0
    const/high16 v2, 0x3f000000    # 0.5f

    .line 934
    .local v2, "panX":F
    const/high16 v3, 0x3f000000    # 0.5f

    .line 935
    .local v3, "panY":F
    const/high16 v8, 0x3f800000    # 1.0f

    .line 936
    .local v8, "zoomX":F
    const/high16 v9, 0x3f800000    # 1.0f

    .line 937
    .local v9, "zoomY":F
    const/4 v5, 0x0

    .line 938
    .local v5, "srcWidth":I
    const/4 v4, 0x0

    .line 940
    .local v4, "srcHeight":I
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    if-eqz v10, :cond_1

    .line 941
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getPanX()F

    move-result v2

    .line 942
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getPanY()F

    move-result v3

    .line 943
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 944
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAspectQuotient:F

    invoke-virtual {v10, v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getZoomX(F)F

    move-result v10

    int-to-float v11, v7

    mul-float/2addr v10, v11

    int-to-float v11, v1

    div-float v8, v10, v11

    .line 945
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAspectQuotient:F

    invoke-virtual {v10, v11}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getZoomY(F)F

    move-result v10

    int-to-float v11, v6

    mul-float/2addr v10, v11

    int-to-float v11, v0

    div-float v9, v10, v11

    .line 950
    :cond_1
    cmpl-float v10, v12, v8

    if-eqz v10, :cond_2

    cmpl-float v10, v12, v9

    if-eqz v10, :cond_2

    .line 951
    int-to-float v10, v1

    mul-float/2addr v10, v2

    int-to-float v11, v7

    mul-float v12, v8, v13

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetX:I

    .line 952
    int-to-float v10, v0

    mul-float/2addr v10, v3

    int-to-float v11, v6

    mul-float v12, v9, v13

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetY:I

    .line 954
    int-to-float v10, v7

    div-float/2addr v10, v8

    float-to-int v5, v10

    .line 955
    int-to-float v10, v6

    div-float/2addr v10, v9

    float-to-int v4, v10

    .line 958
    :cond_2
    if-eqz v7, :cond_3

    if-eqz v6, :cond_3

    .line 959
    int-to-float v10, v5

    int-to-float v11, v7

    div-float/2addr v10, v11

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    .line 960
    int-to-float v10, v4

    int-to-float v11, v6

    div-float/2addr v10, v11

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleY:F

    .line 963
    :cond_3
    return-void
.end method

.method private handleFingerMask(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 666
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 667
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 668
    .local v1, "y":F
    const-string v2, "ArcSoft_PicImageView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doManualClear in x:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " y: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 690
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->invalidate()V

    .line 692
    return-void

    .line 671
    :pswitch_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbSingleDown:Z

    .line 672
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mFirstTouchTime:Ljava/lang/Long;

    .line 673
    invoke-direct {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->touch_start(FF)V

    goto :goto_0

    .line 676
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSecondTouchTime:Ljava/lang/Long;

    .line 677
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSecondTouchTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mFirstTouchTime:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->DoubleTouchTime:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbSingleDown:Z

    if-eqz v2, :cond_0

    .line 678
    invoke-direct {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->touch_move(FF)V

    goto :goto_0

    .line 681
    :pswitch_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSecondTouchTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mFirstTouchTime:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->DoubleTouchTime:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 682
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->touch_up()V

    goto :goto_0

    .line 686
    :pswitch_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->processMask()V

    goto :goto_0

    .line 669
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    const/16 v0, 0x32

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_lastPenWidth:I

    .line 110
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mContext:Landroid/content/Context;

    .line 111
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    const v1, -0x44eeeeef

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$OnSimpleGesture;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$OnSimpleGesture;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSimpleGestureDetector:Landroid/view/GestureDetector;

    .line 113
    return-void
.end method

.method private processMask()V
    .locals 4

    .prologue
    .line 791
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchLock:Z

    .line 792
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mProcessManualMask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 793
    return-void
.end method

.method private recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 302
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 305
    :cond_0
    const/4 p1, 0x0

    .line 306
    return-void
.end method

.method private resetMask()V
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getApplyMask()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->setMask([B)V

    .line 696
    return-void
.end method

.method private rotateBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 309
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 311
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCameraDegress:I

    if-eqz v0, :cond_1

    .line 316
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private showBounceView(I)V
    .locals 5
    .param p1, "currentDirection"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1256
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_4

    .line 1257
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->startBounce(I)V

    .line 1263
    :cond_0
    :goto_0
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_5

    .line 1264
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->startBounce(I)V

    .line 1270
    :cond_1
    :goto_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_6

    .line 1271
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->startBounce(I)V

    .line 1277
    :cond_2
    :goto_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_7

    .line 1278
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->startBounce(I)V

    .line 1283
    :cond_3
    :goto_3
    return-void

    .line 1259
    :cond_4
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->endSingleBounce(I)V

    goto :goto_0

    .line 1266
    :cond_5
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 1267
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->endSingleBounce(I)V

    goto :goto_1

    .line 1273
    :cond_6
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 1274
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->endSingleBounce(I)V

    goto :goto_2

    .line 1280
    :cond_7
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_3

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->endSingleBounce(I)V

    goto :goto_3
.end method

.method private showEditBar(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 476
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    if-nez v1, :cond_1

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 478
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    move-result-object v0

    .line 479
    .local v0, "workStatus":Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;
    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v1, v0, :cond_0

    .line 480
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    if-nez v1, :cond_2

    .line 482
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->rememberVisibility()V

    .line 483
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hideAllViews()V

    .line 484
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    goto :goto_0

    .line 485
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v2, v1, :cond_3

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 487
    :cond_3
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    if-eqz v1, :cond_0

    .line 488
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->restoreViewsVisibility()V

    .line 489
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    goto :goto_0
.end method

.method private switchDefaultActionbar(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 619
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchHelper:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;

    invoke-virtual {v1, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$TouchEventHelper;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 620
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    if-nez v1, :cond_1

    .line 621
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v1, :cond_0

    .line 622
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v2, v1, :cond_0

    .line 623
    const-string v1, "ArcSoft_PicImageView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMoved: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mLongPressed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    if-nez v1, :cond_0

    .line 625
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchDefaultActionBar()V

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    move-result-object v0

    .line 631
    .local v0, "workStatus":Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;
    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v1, v0, :cond_2

    .line 632
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v2, v1, :cond_0

    .line 633
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMoved:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLongPressed:Z

    if-nez v1, :cond_0

    .line 634
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTap:Z

    if-eqz v1, :cond_0

    .line 635
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTap:Z

    .line 636
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchAllViews()V

    goto :goto_0

    .line 640
    :cond_2
    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    if-ne v1, v0, :cond_0

    .line 641
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_3

    .line 642
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->rememberVisibility()V

    .line 643
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hideAllViews()V

    goto :goto_0

    .line 644
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v2, v1, :cond_4

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 646
    :cond_4
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->restoreViewsVisibility()V

    goto :goto_0
.end method

.method private touch_move(FF)V
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v10, 0x40800000    # 4.0f

    const/high16 v9, 0x40000000    # 2.0f

    .line 725
    const-string v6, "ArcSoft_PicImageView"

    const-string v7, "touch_move in"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathDirty:Z

    if-eqz v6, :cond_0

    .line 727
    const-string v6, "ArcSoft_PicImageView"

    const-string v7, "touch_move mPathDirty"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    :goto_0
    return-void

    .line 730
    :cond_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float v6, p1, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 731
    .local v0, "dx":F
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float v6, p2, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 733
    .local v1, "dy":F
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    mul-float/2addr v6, p1

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetX:I

    int-to-float v7, v7

    add-float v2, v6, v7

    .line 734
    .local v2, "imgMaskX":F
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleY:F

    mul-float/2addr v6, p2

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetY:I

    int-to-float v7, v7

    add-float v4, v6, v7

    .line 736
    .local v4, "imgMaskY":F
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetX:I

    int-to-float v7, v7

    add-float v3, v6, v7

    .line 737
    .local v3, "imgMaskX_LastTime":F
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleY:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetY:I

    int-to-float v7, v7

    add-float v5, v6, v7

    .line 739
    .local v5, "imgMaskY_LastTime":F
    const-string v6, "ArcSoft_PicImageView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "touch_move imgMaskX: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " imgMaskY: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    const-string v6, "ArcSoft_PicImageView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "touch_move imgMaskX_LastTime: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " imgMaskY_LastTime: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    cmpl-float v6, v0, v10

    if-gez v6, :cond_1

    cmpl-float v6, v1, v10

    if-ltz v6, :cond_4

    .line 745
    :cond_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    if-eqz v6, :cond_2

    .line 746
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    add-float v7, v2, v3

    div-float/2addr v7, v9

    add-float v8, v4, v5

    div-float/2addr v8, v9

    invoke-virtual {v6, v3, v5, v7, v8}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 749
    const-string v6, "ArcSoft_PicImageView"

    const-string v7, "touch_move 111111111111111"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    if-eqz v6, :cond_3

    .line 753
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 754
    const-string v6, "ArcSoft_PicImageView"

    const-string v7, "touch_move 22222222222222222"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    :cond_3
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    float-to-int v7, p1

    iput v7, v6, Landroid/graphics/Point;->x:I

    .line 758
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    float-to-int v7, p2

    iput v7, v6, Landroid/graphics/Point;->y:I

    .line 759
    const-string v6, "ArcSoft_PicImageView"

    const-string v7, "touch_move 333333333333"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    :cond_4
    const-string v6, "ArcSoft_PicImageView"

    const-string v7, "touch_move out"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private touch_start(FF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 700
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    float-to-int v4, p1

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 701
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    float-to-int v4, p2

    iput v4, v3, Landroid/graphics/Point;->y:I

    .line 702
    const-string v3, "ArcSoft_PicImageView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "touch_start in mPtDown.x:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mPtDown.y: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPtDown:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    mul-float/2addr v3, p1

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetX:I

    int-to-float v4, v4

    add-float v0, v3, v4

    .line 704
    .local v0, "imgMaskX":F
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleY:F

    mul-float/2addr v3, p2

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetY:I

    int-to-float v4, v4

    add-float v1, v3, v4

    .line 705
    .local v1, "imgMaskY":F
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_lastPenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 708
    .local v2, "maskPenWidth":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    if-eqz v3, :cond_0

    .line 709
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 712
    :cond_0
    const-string v3, "ArcSoft_PicImageView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "touch_start  imgMaskX:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " imgMaskY : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    if-nez v3, :cond_1

    .line 714
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    .line 717
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 718
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 719
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathDirty:Z

    .line 721
    const-string v3, "ArcSoft_PicImageView"

    const-string v4, "touch_start  out"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    return-void
.end method

.method private touch_up()V
    .locals 2

    .prologue
    .line 766
    const-string v0, "ArcSoft_PicImageView"

    const-string v1, "touch_up in"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathDirty:Z

    if-eqz v0, :cond_0

    .line 768
    const-string v0, "ArcSoft_PicImageView"

    const-string v1, "touch_move mPathDirty"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    :goto_0
    return-void

    .line 786
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->processMask()V

    .line 787
    const-string v0, "ArcSoft_PicImageView"

    const-string v1, "touch_up out"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public bitmapRecycle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 365
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 373
    :cond_1
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 378
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 379
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 381
    :cond_3
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 384
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    .line 385
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_5

    .line 386
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 388
    :cond_5
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    .line 390
    :cond_6
    return-void
.end method

.method public doMask([BZ)V
    .locals 4
    .param p1, "grey"    # [B
    .param p2, "isBrush"    # Z

    .prologue
    .line 812
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchLock:Z

    .line 813
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;[BZ)V

    .line 822
    .local v0, "doMask":Ljava/lang/Runnable;
    const-wide/16 v2, 0x32

    invoke-virtual {p0, v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 823
    return-void
.end method

.method public eraseAllMask()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 424
    :cond_0
    return-void
.end method

.method public getBitmapSizeChanged()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapSizeChanged:Z

    return v0
.end method

.method public getCurrentBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getIsAutoNoMaskBitmapNull()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsAutoNoMaskBitmapNull:Z

    return v0
.end method

.method public getIsMaskShow()Z
    .locals 1

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbShowMask:Z

    return v0
.end method

.method public getMaskAreas()[B
    .locals 8

    .prologue
    .line 879
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 882
    .local v0, "curr":J
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBufferArray:[B

    if-eqz v4, :cond_0

    .line 883
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBufferArray:[B

    .line 886
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_1

    .line 887
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBufferArray:[B

    .line 906
    :goto_0
    return-object v4

    .line 895
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 896
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int v3, v4, v5

    .line 897
    .local v3, "length":I
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 898
    .local v2, "dstBuffer":Ljava/nio/ByteBuffer;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v2}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 899
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBufferArray:[B

    .line 903
    .end local v2    # "dstBuffer":Ljava/nio/ByteBuffer;
    .end local v3    # "length":I
    :cond_2
    const-string v4, "ArcSoft_PicImageView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "zdd getMaskAreas() cost time ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBufferArray:[B

    goto :goto_0
.end method

.method public getOffsetX()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetX:I

    return v0
.end method

.method public getOffsetY()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_OffsetY:I

    return v0
.end method

.method public getRectForBounceView()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleX:F

    return v0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_ScaleY:F

    return v0
.end method

.method public initMask()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 221
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 222
    .local v0, "curr":J
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 268
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    .line 228
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v2, v3, :cond_1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 230
    :cond_1
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapSizeChanged:Z

    .line 231
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 232
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 234
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->eraseAllMask()V

    .line 245
    :goto_1
    const/16 v2, 0x32

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_lastPenWidth:I

    .line 247
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 248
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setDither(Z)V

    .line 249
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 250
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 251
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 252
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->m_lastPenWidth:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 253
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 254
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapMaskPaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 256
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    if-eqz v2, :cond_2

    .line 257
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    .line 259
    :cond_2
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    .line 261
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    if-nez v2, :cond_3

    .line 262
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    .line 264
    :cond_3
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathMask:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 265
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPathDirty:Z

    .line 267
    const-string v2, "ArcSoft_PicImageView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "zdd initMask() cost time ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 236
    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapSizeChanged:Z

    goto/16 :goto_1

    .line 239
    :cond_5
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 241
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->eraseAllMask()V

    goto/16 :goto_1
.end method

.method public isManualMask()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    return v0
.end method

.method public isMoveActionWork(Z)Z
    .locals 5
    .param p1, "isHorizontal"    # Z

    .prologue
    .line 1230
    const/4 v2, 0x1

    .line 1231
    .local v2, "work":Z
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getWidth()I

    move-result v1

    .line 1232
    .local v1, "viewWidth":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getHeight()I

    move-result v0

    .line 1233
    .local v0, "viewHeight":I
    if-eqz p1, :cond_1

    .line 1234
    int-to-float v3, v1

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawWidth:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 1235
    const/4 v2, 0x0

    .line 1243
    :cond_0
    :goto_0
    return v2

    .line 1239
    :cond_1
    int-to-float v3, v0

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapDrawHeight:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 1240
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public ismImageAction()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageAction:Z

    return v0
.end method

.method public ismImageMove()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageMove:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 22
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1016
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    if-eqz v4, :cond_e

    .line 1017
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoom:Z

    if-eqz v4, :cond_0

    .line 1018
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoomOut:Z

    if-eqz v4, :cond_f

    .line 1019
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->autoZoomOut()V

    .line 1022
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->notifyObservers()V

    .line 1023
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->invalidate()V

    .line 1026
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getWidth()I

    move-result v7

    .line 1027
    .local v7, "viewWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getHeight()I

    move-result v8

    .line 1028
    .local v8, "viewHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1029
    .local v5, "bitmapWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 1030
    .local v6, "bitmapHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getPanX()F

    move-result v14

    .line 1031
    .local v14, "panX":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getPanY()F

    move-result v15

    .line 1032
    .local v15, "panY":F
    const/4 v11, 0x0

    .line 1033
    .local v11, "zoomX":F
    const/4 v12, 0x0

    .line 1035
    .local v12, "zoomY":F
    const/4 v9, 0x0

    .line 1036
    .local v9, "srcRect":Landroid/graphics/Rect;
    const/4 v10, 0x0

    .line 1038
    .local v10, "dstRect":Landroid/graphics/Rect;
    if-eqz v5, :cond_1

    .line 1039
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAspectQuotient:F

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getZoomX(F)F

    move-result v4

    int-to-float v0, v7

    move/from16 v17, v0

    mul-float v4, v4, v17

    int-to-float v0, v5

    move/from16 v17, v0

    div-float v11, v4, v17

    .line 1042
    :cond_1
    if-eqz v6, :cond_2

    .line 1043
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAspectQuotient:F

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getZoomY(F)F

    move-result v4

    int-to-float v0, v8

    move/from16 v17, v0

    mul-float v4, v4, v17

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v12, v4, v17

    .line 1046
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v0, v5

    move/from16 v17, v0

    mul-float v17, v17, v14

    int-to-float v0, v7

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    mul-float v19, v19, v11

    div-float v18, v18, v19

    sub-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1047
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v0, v6

    move/from16 v17, v0

    mul-float v17, v17, v15

    int-to-float v0, v8

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    mul-float v19, v19, v12

    div-float v18, v18, v19

    sub-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 1048
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v7

    move/from16 v18, v0

    div-float v18, v18, v11

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 1049
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v8

    move/from16 v18, v0

    div-float v18, v18, v12

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x3fe0000000000000L    # 0.5

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1051
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getLeft()I

    move-result v17

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1052
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getTop()I

    move-result v17

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 1053
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getRight()I

    move-result v17

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 1054
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getBottom()I

    move-result v17

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1056
    new-instance v9, Landroid/graphics/Rect;

    .end local v9    # "srcRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    invoke-direct {v9, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1057
    .restart local v9    # "srcRect":Landroid/graphics/Rect;
    new-instance v10, Landroid/graphics/Rect;

    .end local v10    # "dstRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v10, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1059
    .restart local v10    # "dstRect":Landroid/graphics/Rect;
    iget v4, v9, Landroid/graphics/Rect;->left:I

    if-gez v4, :cond_10

    .line 1060
    iget v4, v10, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v11

    add-float v4, v4, v17

    float-to-int v4, v4

    iput v4, v10, Landroid/graphics/Rect;->left:I

    .line 1061
    const/4 v4, 0x0

    iput v4, v9, Landroid/graphics/Rect;->left:I

    .line 1062
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->isMoveActionWork(Z)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1063
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    or-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    .line 1070
    :cond_3
    :goto_1
    iget v4, v9, Landroid/graphics/Rect;->right:I

    if-le v4, v5, :cond_11

    .line 1071
    iget v4, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    sub-int v17, v17, v5

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v11

    sub-float v4, v4, v17

    float-to-int v4, v4

    iput v4, v10, Landroid/graphics/Rect;->right:I

    .line 1072
    iput v5, v9, Landroid/graphics/Rect;->right:I

    .line 1073
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->isMoveActionWork(Z)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1074
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    or-int/lit8 v4, v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    .line 1081
    :cond_4
    :goto_2
    iget v4, v9, Landroid/graphics/Rect;->top:I

    if-gez v4, :cond_12

    .line 1082
    iget v4, v10, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v12

    add-float v4, v4, v17

    float-to-int v4, v4

    iput v4, v10, Landroid/graphics/Rect;->top:I

    .line 1083
    const/4 v4, 0x0

    iput v4, v9, Landroid/graphics/Rect;->top:I

    .line 1084
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->isMoveActionWork(Z)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1085
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    or-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    .line 1092
    :cond_5
    :goto_3
    iget v4, v9, Landroid/graphics/Rect;->bottom:I

    if-le v4, v6, :cond_13

    .line 1093
    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    sub-int v17, v17, v6

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v12

    sub-float v4, v4, v17

    float-to-int v4, v4

    iput v4, v10, Landroid/graphics/Rect;->bottom:I

    .line 1094
    iput v6, v9, Landroid/graphics/Rect;->bottom:I

    .line 1095
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->isMoveActionWork(Z)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1096
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    or-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    :cond_6
    :goto_4
    move-object/from16 v4, p0

    .line 1103
    invoke-direct/range {v4 .. v12}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->adjustZoomState(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;FF)V

    .line 1104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setIsMoveActionWorkForZoomListener()V

    .line 1105
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getPanX()F

    move-result v14

    .line 1106
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->getPanY()F

    move-result v15

    .line 1108
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v0, v5

    move/from16 v17, v0

    mul-float v17, v17, v14

    int-to-float v0, v7

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    mul-float v19, v19, v11

    div-float v18, v18, v19

    sub-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v0, v6

    move/from16 v17, v0

    mul-float v17, v17, v15

    int-to-float v0, v8

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    mul-float v19, v19, v12

    div-float v18, v18, v19

    sub-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 1110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v7

    move/from16 v18, v0

    div-float v18, v18, v11

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 1111
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v8

    move/from16 v18, v0

    div-float v18, v18, v12

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-gez v4, :cond_7

    .line 1114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    iget v0, v4, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v11

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1115
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1117
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-le v4, v5, :cond_8

    .line 1118
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    iget v0, v4, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v18, v18, v5

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v11

    sub-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 1119
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 1121
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-gez v4, :cond_9

    .line 1122
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    iget v0, v4, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v12

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 1123
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    const/16 v17, 0x0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 1125
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    if-le v4, v6, :cond_a

    .line 1126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    sub-int v18, v18, v6

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v12

    sub-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1127
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    .line 1130
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v13, v4, Landroid/content/res/Configuration;->orientation:I

    .line 1131
    .local v13, "orientation":I
    const/4 v4, 0x1

    if-ne v4, v13, :cond_14

    const/16 v16, 0x1

    .line 1132
    .local v16, "portrait":Z
    :goto_5
    if-eqz v16, :cond_15

    .line 1133
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    sub-int v17, v8, v17

    div-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 1134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    sub-int v17, v8, v17

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 1141
    :goto_6
    const/high16 v4, -0x1000000

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1143
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_b

    .line 1144
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1147
    :cond_b
    const-string v4, "ArcSoft_PicImageView"

    const-string v17, "ArcSoft_DDS draw maskBitmap pos_1"

    move-object/from16 v0, v17

    invoke-static {v4, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_c

    .line 1149
    const-string v4, "ArcSoft_PicImageView"

    const-string v17, "ArcSoft_DDS draw maskBitmap pos_2"

    move-object/from16 v0, v17

    invoke-static {v4, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1155
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->getIsTouching()Z

    move-result v4

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbIsZoom:Z

    if-nez v4, :cond_d

    .line 1156
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->showBounceView(I)V

    .line 1157
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    .line 1159
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    invoke-interface {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;->dealMinZoom()V

    .line 1161
    .end local v5    # "bitmapWidth":I
    .end local v6    # "bitmapHeight":I
    .end local v7    # "viewWidth":I
    .end local v8    # "viewHeight":I
    .end local v9    # "srcRect":Landroid/graphics/Rect;
    .end local v10    # "dstRect":Landroid/graphics/Rect;
    .end local v11    # "zoomX":F
    .end local v12    # "zoomY":F
    .end local v13    # "orientation":I
    .end local v14    # "panX":F
    .end local v15    # "panY":F
    .end local v16    # "portrait":Z
    :cond_e
    return-void

    .line 1021
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->autoZoomIn()V

    goto/16 :goto_0

    .line 1066
    .restart local v5    # "bitmapWidth":I
    .restart local v6    # "bitmapHeight":I
    .restart local v7    # "viewWidth":I
    .restart local v8    # "viewHeight":I
    .restart local v9    # "srcRect":Landroid/graphics/Rect;
    .restart local v10    # "dstRect":Landroid/graphics/Rect;
    .restart local v11    # "zoomX":F
    .restart local v12    # "zoomY":F
    .restart local v14    # "panX":F
    .restart local v15    # "panY":F
    :cond_10
    iget v4, v9, Landroid/graphics/Rect;->left:I

    if-lez v4, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    .line 1067
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    xor-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    goto/16 :goto_1

    .line 1077
    :cond_11
    iget v4, v9, Landroid/graphics/Rect;->right:I

    if-ge v4, v5, :cond_4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_4

    .line 1078
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    xor-int/lit8 v4, v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    goto/16 :goto_2

    .line 1088
    :cond_12
    iget v4, v9, Landroid/graphics/Rect;->top:I

    if-lez v4, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_5

    .line 1089
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    xor-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    goto/16 :goto_3

    .line 1099
    :cond_13
    iget v4, v9, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v6, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_6

    .line 1100
    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    xor-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    goto/16 :goto_4

    .line 1131
    .restart local v13    # "orientation":I
    :cond_14
    const/16 v16, 0x0

    goto/16 :goto_5

    .line 1136
    .restart local v16    # "portrait":Z
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    sub-int v17, v7, v17

    div-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 1137
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRectDst:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    sub-int v17, v7, v17

    move/from16 v0, v17

    iput v0, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_6
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 967
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 968
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->calculateAspectQuotient()V

    .line 969
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 429
    const-string v3, "ArcSoft_PicImageView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTouchEvent,action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchLock:Z

    if-eqz v3, :cond_1

    .line 431
    const-string v2, "ArcSoft_PicImageView"

    const-string v3, "doManualClear mTouchLock return"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    if-eqz v2, :cond_0

    .line 433
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->restoreViewsVisibility()V

    .line 434
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsRememberVisibility:Z

    .line 471
    :cond_0
    :goto_0
    return v1

    .line 439
    :cond_1
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageAction:Z

    if-eqz v3, :cond_2

    .line 440
    const-string v1, "ArcSoft_PicImageView"

    const-string v3, "onTouchEvent,mImageAction return "

    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 441
    goto :goto_0

    .line 444
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getOperateEnable()Z

    move-result v3

    if-nez v3, :cond_3

    .line 445
    const-string v2, "ArcSoft_PicImageView"

    const-string v3, "onTouchEvent, getOperateEnable false return "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 449
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 451
    .local v0, "pointnum":I
    const-string v3, "ArcSoft_PicImageView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTouchEvent, pointnum "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    if-ne v2, v0, :cond_5

    .line 453
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getCurrentOffsetAndScale()V

    .line 455
    const-string v1, "ArcSoft_PicImageView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTouchEvent, single touch mManualMask: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    move-result-object v3

    if-ne v1, v3, :cond_4

    .line 457
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->handleFingerMask(Landroid/view/MotionEvent;)V

    .line 468
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSimpleGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 469
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->showEditBar(Landroid/view/MotionEvent;)V

    move v1, v2

    .line 471
    goto :goto_0

    .line 461
    :cond_5
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbSingleDown:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    if-eqz v3, :cond_6

    sget-object v3, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    move-result-object v4

    if-ne v3, v4, :cond_6

    .line 464
    :cond_6
    const-string v3, "ArcSoft_PicImageView"

    const-string v4, "onTouchEvent, mult touch "

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbSingleDown:Z

    goto :goto_1
.end method

.method public reSetDirection()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1251
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mLastDirection:I

    .line 1252
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCurrentDirection:I

    .line 1253
    return-void
.end method

.method public rotateMaskBitmap()V
    .locals 3

    .prologue
    .line 865
    const-string v0, "ArcSoft_PicImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rotateMaskBitmap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCameraDegress:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCameraDegress:I

    if-eqz v0, :cond_1

    .line 869
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 870
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    .line 872
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskCanvas:Landroid/graphics/Canvas;

    .line 874
    :cond_1
    return-void
.end method

.method public setImageIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 271
    packed-switch p1, :pswitch_data_0

    .line 297
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->calculateAspectQuotient()V

    .line 298
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->invalidate()V

    .line 299
    return-void

    .line 273
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->rotateBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 278
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->rotateBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 283
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->rotateBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 288
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->rotateBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 271
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setImageZoomState(Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;)V
    .locals 1
    .param p1, "zoomState"    # Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    .prologue
    .line 910
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 913
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    .line 915
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    if-eqz v0, :cond_1

    .line 916
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->addObserver(Ljava/util/Observer;)V

    .line 918
    :cond_1
    return-void
.end method

.method public setIsMaskShow(Z)V
    .locals 0
    .param p1, "bShowMask"    # Z

    .prologue
    .line 329
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mbShowMask:Z

    .line 330
    return-void
.end method

.method public setManual(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0
    .param p1, "action"    # Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .line 94
    return-void
.end method

.method public setManualMask(Z)V
    .locals 0
    .param p1, "ManualMask"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualMask:Z

    .line 127
    return-void
.end method

.method public setMask([B)V
    .locals 10
    .param p1, "mask"    # [B

    .prologue
    .line 826
    if-eqz p1, :cond_0

    array-length v6, p1

    if-gtz v6, :cond_1

    .line 862
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 832
    .local v4, "time":J
    array-length v6, p1

    int-to-long v0, v6

    .line 833
    .local v0, "arrayLength":J
    const/4 v3, 0x0

    .local v3, "k":I
    :goto_1
    int-to-long v6, v3

    cmp-long v6, v6, v0

    if-gez v6, :cond_3

    .line 834
    aget-byte v6, p1, v3

    if-ltz v6, :cond_2

    .line 835
    const/4 v6, 0x0

    aput-byte v6, p1, v3

    .line 833
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 837
    :cond_2
    const/16 v6, -0x45

    aput-byte v6, p1, v3

    goto :goto_2

    .line 845
    :cond_3
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_4

    if-eqz p1, :cond_4

    .line 846
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 847
    .local v2, "buffer":Ljava/nio/ByteBuffer;
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v2}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 849
    .end local v2    # "buffer":Ljava/nio/ByteBuffer;
    :cond_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->rotateMaskBitmap()V

    .line 850
    const/4 p1, 0x0

    .line 860
    const-string v6, "ArcSoft_PicImageView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "zdd setMask() cost time ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->invalidate()V

    goto :goto_0
.end method

.method public setPenValid()V
    .locals 2

    .prologue
    .line 410
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    const v1, -0x44eeeeef

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 411
    return-void
.end method

.method public setRealImageSize([I)V
    .locals 2
    .param p1, "imageSize"    # [I

    .prologue
    .line 337
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    const/4 v1, 0x0

    aget v1, p1, v1

    iput v1, v0, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 338
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    const/4 v1, 0x1

    aget v1, p1, v1

    iput v1, v0, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 339
    return-void
.end method

.method public setRotateDegress(I)V
    .locals 0
    .param p1, "degress"    # I

    .prologue
    .line 333
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mCameraDegress:I

    .line 334
    return-void
.end method

.method public setRubberValid()V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 416
    return-void
.end method

.method public setShowBounceViewListener(Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    .prologue
    .line 1247
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mShowBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/drama/ShowBounceViewListener;

    .line 1248
    return-void
.end method

.method public setmImageAction(Z)V
    .locals 0
    .param p1, "mImageAction"    # Z

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageAction:Z

    .line 157
    return-void
.end method

.method public setmImageMove(Z)V
    .locals 0
    .param p1, "mImageMove"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImageMove:Z

    .line 141
    return-void
.end method

.method public unInit()V
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 980
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->bitmapRecycle()V

    .line 981
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 973
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->invalidate()V

    .line 974
    return-void
.end method

.method public updateAutoModeBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "maskBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "noMaskBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 181
    const-string v0, "ArcSoft_PicImageView"

    const-string v1, "updateAutoModeBitmap() in"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mSyncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    if-eqz p2, :cond_1

    .line 184
    :try_start_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    const-string v0, "ArcSoft_PicImageView"

    const-string v2, "updateAutoModeBitmap() 1111"

    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 189
    :cond_0
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mIsAutoNoMaskBitmapNull:Z

    .line 193
    :cond_1
    if-eqz p1, :cond_3

    .line 194
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 195
    const-string v0, "ArcSoft_PicImageView"

    const-string v2, "updateAutoModeBitmap() 2222"

    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    .line 199
    :cond_2
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAutoWithMaskBitmap:Landroid/graphics/Bitmap;

    .line 201
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    const-string v0, "ArcSoft_PicImageView"

    const-string v1, "updateAutoModeBitmap() out"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    return-void

    .line 201
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public updateManualModeBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "manualBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    if-eq p1, v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    .line 211
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mManualBitmap:Landroid/graphics/Bitmap;

    .line 212
    return-void
.end method

.method public updateResultImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "resultBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 173
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    if-eq p1, v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    .line 169
    :cond_1
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    .line 171
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I

    .line 172
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mResultBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I

    goto :goto_0
.end method
