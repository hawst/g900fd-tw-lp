.class public Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;
.super Ljava/util/Observable;
.source "ImageZoomState.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcImageZoomState"


# instance fields
.field private ESPINON:F

.field private mAspectQuotient:F

.field private mMaxZoom:F

.field private mMinZoom:F

.field private mPanX:F

.field private mPanY:F

.field private mZoom:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v0, 0x3f000000    # 0.5f

    .line 5
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 7
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    .line 8
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanX:F

    .line 9
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanY:F

    .line 10
    const v0, 0x358637bd    # 1.0E-6f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mAspectQuotient:F

    .line 13
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMaxZoom:F

    .line 14
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    return-void
.end method


# virtual methods
.method public getAspectQuotient()F
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mAspectQuotient:F

    return v0
.end method

.method public getMaxZoom()F
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMaxZoom:F

    return v0
.end method

.method public getMinZoom()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    return v0
.end method

.method public getPanX()F
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanX:F

    return v0
.end method

.method public getPanY()F
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanY:F

    return v0
.end method

.method public getZoom()F
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    return v0
.end method

.method public getZoomX(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 76
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mAspectQuotient:F

    .line 77
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    mul-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getZoomY(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 81
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mAspectQuotient:F

    .line 82
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    div-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public minZoom()Z
    .locals 2

    .prologue
    .line 21
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    .line 25
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPanX(F)V
    .locals 2
    .param p1, "panX"    # F

    .prologue
    .line 52
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanX:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 56
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanX:F

    .line 57
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setChanged()V

    goto :goto_0
.end method

.method public setPanY(F)V
    .locals 2
    .param p1, "panY"    # F

    .prologue
    .line 66
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanY:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 70
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanY:F

    .line 71
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setChanged()V

    goto :goto_0
.end method

.method public setZoom(F)V
    .locals 3
    .param p1, "zoom"    # F

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 30
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 32
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMaxZoom:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    .line 33
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    .line 39
    :cond_0
    :goto_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->ESPINON:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 40
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanX:F

    .line 41
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mPanY:F

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->setChanged()V

    .line 45
    :cond_2
    return-void

    .line 35
    :cond_3
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 36
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mMinZoom:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageZoomState;->mZoom:F

    goto :goto_0
.end method
