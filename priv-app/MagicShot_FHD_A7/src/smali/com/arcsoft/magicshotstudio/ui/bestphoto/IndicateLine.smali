.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
.super Landroid/widget/LinearLayout;
.source "IndicateLine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "IndicateLine"


# instance fields
.field private IMAGE_HEIGHT:I

.field private IMAGE_MARGIN:I

.field private IMAGE_WIDTH:I

.field private mBestIndexArray:[Z

.field private mContext:Landroid/content/Context;

.field private mCount:I

.field private mCurSelectedIndex:I

.field private mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

.field private mCursorImgs:[Landroid/widget/ImageView;

.field private mCursorViews:[Landroid/widget/RelativeLayout;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mScreenDensity:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x15

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    .line 17
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_WIDTH:I

    .line 18
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_HEIGHT:I

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    .line 22
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    .line 23
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    .line 24
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    .line 27
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCount:I

    .line 28
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    .line 160
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 181
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 201
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    .line 43
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v3, 0x15

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    .line 17
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_WIDTH:I

    .line 18
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_HEIGHT:I

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    .line 22
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    .line 23
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    .line 24
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    .line 27
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCount:I

    .line 28
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    .line 160
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 181
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 201
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    .line 38
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v3, 0x15

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/4 v0, 0x3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    .line 17
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_WIDTH:I

    .line 18
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_HEIGHT:I

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    .line 22
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    .line 23
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    .line 24
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    .line 27
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCount:I

    .line 28
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    .line 160
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 181
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 201
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    .line 33
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getClickIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)[Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setBestIndex(IZ)V

    return-void
.end method

.method private getClickIndex(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 174
    const/4 v0, -0x1

    .line 175
    .local v0, "index":I
    if-eqz p1, :cond_0

    .line 176
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 178
    :cond_0
    return v0
.end method

.method private setBestIndex(IZ)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "bBest"    # Z

    .prologue
    .line 130
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    aput-boolean p2, v1, p1

    .line 132
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 133
    if-eq v0, p1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 132
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->updateUI()V

    .line 138
    return-void
.end method

.method private updateUI()V
    .locals 4

    .prologue
    .line 99
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCount:I

    if-ge v1, v3, :cond_4

    .line 100
    const/4 v2, 0x0

    .line 101
    .local v2, "resId":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    if-ne v3, v1, :cond_0

    const/4 v0, 0x1

    .line 103
    .local v0, "bCurrent":Z
    :goto_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_2

    .line 104
    if-eqz v0, :cond_1

    const v2, 0x7f0200bb

    .line 111
    :goto_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 99
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "bCurrent":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 104
    .restart local v0    # "bCurrent":Z
    :cond_1
    const v2, 0x7f0200bc

    goto :goto_2

    .line 107
    :cond_2
    if-eqz v0, :cond_3

    const v2, 0x7f0200bd

    :goto_3
    goto :goto_2

    :cond_3
    const v2, 0x7f0200be

    goto :goto_3

    .line 113
    .end local v0    # "bCurrent":Z
    .end local v2    # "resId":I
    :cond_4
    return-void
.end method


# virtual methods
.method public getBest()[Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    return-object v0
.end method

.method public getBestCount()I
    .locals 5

    .prologue
    .line 120
    const/4 v2, 0x0

    .line 121
    .local v2, "cnt":I
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    .local v0, "arr$":[Z
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-boolean v1, v0, v3

    .line 122
    .local v1, "b":Z
    if-eqz v1, :cond_0

    .line 123
    add-int/lit8 v2, v2, 0x1

    .line 121
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 126
    .end local v1    # "b":Z
    :cond_1
    return v2
.end method

.method public getCurSelectedIndex()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    return v0
.end method

.method public init(II)V
    .locals 9
    .param p1, "count"    # I
    .param p2, "defaultBestIndex"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x2

    const/4 v6, 0x1

    .line 47
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCount:I

    .line 48
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    .line 49
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    .line 50
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_WIDTH:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_WIDTH:I

    .line 51
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_HEIGHT:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mScreenDensity:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_HEIGHT:I

    .line 53
    new-array v3, p1, [Z

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    .line 54
    new-array v3, p1, [Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    .line 55
    new-array v3, p1, [Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    .line 58
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCount:I

    if-ge v0, v3, :cond_1

    .line 59
    if-ne p2, v0, :cond_0

    .line 60
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    aput-boolean v6, v3, v0

    .line 64
    :goto_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    new-instance v4, Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v0

    .line 65
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    aget-object v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 67
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    aget-object v3, v3, v0

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 68
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v0

    .line 69
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 70
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 71
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 72
    .local v2, "rl":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 73
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorImgs:[Landroid/widget/ImageView;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_WIDTH:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_HEIGHT:I

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 77
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 78
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->IMAGE_MARGIN:I

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 79
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorViews:[Landroid/widget/RelativeLayout;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 62
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "rl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    aput-boolean v8, v3, v0

    goto/16 :goto_1

    .line 82
    :cond_1
    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setCurrentCursor(I)V

    .line 83
    invoke-direct {p0, p2, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setBestIndex(IZ)V

    .line 85
    return-void
.end method

.method public isCurIndexBest()Z
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    aget-boolean v0, v0, v1

    return v0
.end method

.method public setCurIndexToBest()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 148
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->updateUI()V

    .line 149
    return-void
.end method

.method public setCurIndexToNormal()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    .line 153
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->updateUI()V

    .line 154
    return-void
.end method

.method public setCurrentCursor(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCurSelectedIndex:I

    .line 93
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->updateUI()V

    .line 96
    return-void
.end method

.method public setOnCursorClickedListener(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;)V
    .locals 0
    .param p1, "cursorClickListener"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    .line 206
    return-void
.end method
