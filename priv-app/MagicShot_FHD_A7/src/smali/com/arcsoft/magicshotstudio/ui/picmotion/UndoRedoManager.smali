.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
.super Ljava/lang/Object;
.source "UndoRedoManager.java"


# instance fields
.field private final MAX_STACK_COUNT:I

.field private final REDO_FILE_NAME:Ljava/lang/String;

.field private final UNDO_FILE_NAME:Ljava/lang/String;

.field private final USE_FILE_STORE:Z

.field private mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

.field private mDataSize:I

.field private mRedoDataFileLoadOffset:I

.field private mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/arcsoft/magicshotstudio/utils/UserStack",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;",
            ">;"
        }
    .end annotation
.end field

.field private mRedoStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;",
            ">;"
        }
    .end annotation
.end field

.field private mUndoDataFileLoadOffset:I

.field private mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/arcsoft/magicshotstudio/utils/UserStack",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;",
            ">;"
        }
    .end annotation
.end field

.field private mUndoStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;",
            ">;"
        }
    .end annotation
.end field

.field private mUserDataDir:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 4
    .param p1, "dataDir"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "picmotion_undo_data.dat"

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->UNDO_FILE_NAME:Ljava/lang/String;

    .line 15
    const-string v0, "picmotion_redo_data.dat"

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->REDO_FILE_NAME:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    .line 18
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    .line 20
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .line 21
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I

    .line 22
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    .line 23
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    .line 25
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUserDataDir:Ljava/io/File;

    .line 27
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->MAX_STACK_COUNT:I

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->USE_FILE_STORE:Z

    .line 104
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    .line 131
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    .line 32
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    .line 33
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    .line 36
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUserDataDir:Ljava/io/File;

    .line 37
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    invoke-virtual {v0, v3, v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->openFileStore(ILcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;)V

    .line 38
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    invoke-virtual {v0, v3, v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->openFileStore(ILcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;)V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUserDataDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    return v0
.end method

.method static synthetic access$112(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;I)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # I

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    return v0
.end method

.method static synthetic access$120(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;I)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # I

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->doSaveDataToFile(Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I

    return v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->doLoadDataFromFile(Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->doClearDataFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    return v0
.end method

.method static synthetic access$612(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;I)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # I

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    return v0
.end method

.method static synthetic access$620(Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;I)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .param p1, "x1"    # I

    .prologue
    .line 12
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    return v0
.end method

.method private doClearDataFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string v1, "xsj"

    const-string v2, "clearDataFile "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 222
    :cond_0
    return-void
.end method

.method private doLoadDataFromFile(Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .locals 16
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "dataSize"    # I
    .param p3, "offset"    # I

    .prologue
    .line 182
    const-string v9, "xsj"

    const-string v12, "loadDataFromFile start"

    invoke-static {v9, v12}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 184
    .local v10, "time":J
    const/4 v7, 0x0

    .line 185
    .local v7, "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    const/4 v5, -0x1

    .line 186
    .local v5, "firstByte":I
    const/4 v8, -0x1

    .line 187
    .local v8, "secondByte":I
    const/4 v6, -0x1

    .line 188
    .local v6, "res":I
    add-int/lit8 v9, p2, -0x2

    new-array v3, v9, [B

    .line 190
    .local v3, "buffer":[B
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v9, "r"

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .local v2, "acessFile":Ljava/io/RandomAccessFile;
    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 192
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v5

    .line 193
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v8

    .line 194
    invoke-virtual {v2, v3}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v6

    .line 195
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 204
    .end local v2    # "acessFile":Ljava/io/RandomAccessFile;
    :goto_0
    const/4 v9, -0x1

    if-eq v6, v9, :cond_0

    const/4 v9, -0x1

    if-eq v5, v9, :cond_0

    .line 205
    new-instance v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .end local v7    # "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    invoke-direct {v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;-><init>()V

    .line 206
    .restart local v7    # "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    const/4 v9, 0x1

    if-ne v5, v9, :cond_1

    const/4 v9, 0x1

    :goto_1
    iput-boolean v9, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->undo_isBrush:Z

    .line 207
    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    const/4 v9, 0x1

    :goto_2
    iput-boolean v9, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->redo_isBrush:Z

    .line 208
    iput-object v3, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->mask:[B

    .line 211
    :cond_0
    const-string v9, "xsj"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "loadDataFromFile end and cost time : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v10

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-object v7

    .line 196
    :catch_0
    move-exception v4

    .line 198
    .local v4, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v4

    .line 201
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 206
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    .line 207
    :cond_2
    const/4 v9, 0x0

    goto :goto_2
.end method

.method private doSaveDataToFile(Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;Ljava/lang/String;I)V
    .locals 8
    .param p1, "data"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "offset"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 160
    const-string v6, "xsj"

    const-string v7, "saveDataToFile "

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 163
    .local v2, "time":J
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v6, "rw"

    invoke-direct {v0, p2, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .local v0, "acessfile":Ljava/io/RandomAccessFile;
    invoke-virtual {v0, p3}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 165
    iget-boolean v6, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->undo_isBrush:Z

    if-eqz v6, :cond_0

    move v6, v4

    :goto_0
    invoke-virtual {v0, v6}, Ljava/io/RandomAccessFile;->write(I)V

    .line 166
    iget-boolean v6, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->redo_isBrush:Z

    if-eqz v6, :cond_1

    :goto_1
    invoke-virtual {v0, v4}, Ljava/io/RandomAccessFile;->write(I)V

    .line 167
    iget-object v4, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->mask:[B

    invoke-virtual {v0, v4}, Ljava/io/RandomAccessFile;->write([B)V

    .line 168
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 177
    .end local v0    # "acessfile":Ljava/io/RandomAccessFile;
    :goto_2
    const-string v4, "xsj"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveDataToFile End and cost time : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    return-void

    .restart local v0    # "acessfile":Ljava/io/RandomAccessFile;
    :cond_0
    move v6, v5

    .line 165
    goto :goto_0

    :cond_1
    move v4, v5

    .line 166
    goto :goto_1

    .line 169
    .end local v0    # "acessfile":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 172
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 174
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public checkRedoEnabled()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkUndoEnabled()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->clear()V

    .line 86
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->clear()V

    .line 88
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I

    .line 89
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoDataFileLoadOffset:I

    .line 90
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    .line 91
    return-void
.end method

.method public redo()Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .locals 3

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 66
    .local v0, "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->popData()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .line 69
    .restart local v0    # "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->pushData(Ljava/lang/Object;)V

    .line 70
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .line 73
    :cond_0
    return-object v0
.end method

.method public saveOperate([BZ)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "isBrush"    # Z

    .prologue
    .line 43
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    iput-boolean p2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->undo_isBrush:Z

    .line 44
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->pushData(Ljava/lang/Object;)V

    .line 45
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->setCurData([BZ)V

    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->clear()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoDataFileLoadOffset:I

    .line 48
    return-void
.end method

.method public setCurData([BZ)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "isBrush"    # Z

    .prologue
    .line 94
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .line 96
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    iput-object p1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->mask:[B

    .line 97
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    iput-boolean p2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->redo_isBrush:Z

    .line 98
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    iput-boolean p2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->undo_isBrush:Z

    .line 100
    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mDataSize:I

    .line 101
    return-void
.end method

.method public undo()Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 53
    .local v0, "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mUndoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->popData()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .line 56
    .restart local v0    # "result":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mRedoStack:Lcom/arcsoft/magicshotstudio/utils/UserStack;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->pushData(Ljava/lang/Object;)V

    .line 57
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->mCurOperateData:Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    .line 60
    :cond_0
    return-object v0
.end method
