.class public Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
.super Ljava/lang/Object;
.source "PicturesBestEngine.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;,
        Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;
    }
.end annotation


# static fields
.field private static final PIC_EDIT_FINISH:I = 0x5

.field private static final PIC_EDIT_START:I = 0x4

.field private static final PROCESS_BASIC_BASE:I = 0x1

.field private static final PROCESS_IMG_FINISH:I = 0x2

.field private static final PROCESS_IMG_NO_FACE:I = 0x3

.field private static final PROCESS_IMG_START:I = 0x1

.field private static final PROCESS_UPDATA:I = 0x8

.field private static final SAVE_DIRECT:I = 0x6

.field private static final SAVE_MERGE_RESULT:I = 0x7

.field private static final SELECT_FRAME_FINISH:I = 0xa

.field private static final SELECT_PROCESS:I = 0xf

.field private static final SHOW_FIRST_IMAGE:I = 0x9

.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_PicturesBestEngine"


# instance fields
.field private mBFPerformanceData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

.field private mBestIndex:[I

.field private mBitmapPtr:J

.field private mDegree:I

.field private mFaceId:I

.field private mFaceRectArray:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstImage:Landroid/graphics/Bitmap;

.field private mHandler:Landroid/os/Handler;

.field private mIsForQA:Z

.field private mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

.field private mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

.field private mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

.field private mPersonId:I

.field private mPersonNum:I

.field private mPreProcessData:J

.field private mProcess:I

.field private mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

.field private mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

.field private mProcessThread:Ljava/lang/Thread;

.field private mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

.field private mScreenWidth:I

.field mSelectData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;

.field private mSelectPhotoNum:I

.field private mSelectThread:Ljava/lang/Thread;

.field private mSrcHeight:I

.field private mSrcWidth:I

.field private mThumbWidth:I

.field private mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

.field private mTotalInputSize:J


# direct methods
.method public constructor <init>(Lcom/arcsoft/magicshotstudio/utils/TimeRecord;ZI)V
    .locals 6
    .param p1, "timeRecord"    # Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    .param p2, "isForQA"    # Z
    .param p3, "selectNum"    # I

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    .line 39
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    .line 40
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    .line 42
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .line 43
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    .line 44
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    .line 45
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    .line 47
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFirstImage:Landroid/graphics/Bitmap;

    .line 48
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;

    .line 50
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I

    .line 51
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I

    .line 52
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonId:I

    .line 53
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceId:I

    .line 55
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .line 56
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    .line 57
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectPhotoNum:I

    .line 58
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    .line 66
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    .line 125
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBFPerformanceData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    .line 486
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBestIndex:[I

    .line 487
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcWidth:I

    .line 488
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcHeight:I

    .line 489
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mScreenWidth:I

    .line 490
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mThumbWidth:I

    .line 491
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mDegree:I

    .line 492
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBitmapPtr:J

    .line 493
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPreProcessData:J

    .line 494
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTotalInputSize:J

    .line 537
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;

    .line 61
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .line 62
    iput-boolean p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    .line 63
    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectPhotoNum:I

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBitmapPtr:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBitmapPtr:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPreProcessData:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPreProcessData:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)[I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBestIndex:[I

    return-object v0
.end method

.method static synthetic access$1202(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # [I

    .prologue
    .line 23
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBestIndex:[I

    return-object p1
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mDegree:I

    return v0
.end method

.method static synthetic access$1302(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mDegree:I

    return p1
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I

    return v0
.end method

.method static synthetic access$1802(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I

    return p1
.end method

.method static synthetic access$1900(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectPhotoNum:I

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBFPerformanceData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # Ljava/util/Vector;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonId:I

    return v0
.end method

.method static synthetic access$2300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceId:I

    return v0
.end method

.method static synthetic access$2400(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;JJIZ)[I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # J
    .param p3, "x2"    # J
    .param p5, "x3"    # I
    .param p6, "x4"    # Z

    .prologue
    .line 23
    invoke-direct/range {p0 .. p6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getBestFiveInput(JJIZ)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2502(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTotalInputSize:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I

    return v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I

    return p1
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFirstImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFirstImage:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcWidth:I

    return v0
.end method

.method static synthetic access$602(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcWidth:I

    return p1
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcHeight:I

    return v0
.end method

.method static synthetic access$702(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcHeight:I

    return p1
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$802(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mScreenWidth:I

    return p1
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mThumbWidth:I

    return v0
.end method

.method static synthetic access$902(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mThumbWidth:I

    return p1
.end method

.method private getBestFiveInput(JJIZ)[I
    .locals 13
    .param p1, "bitmapPtr"    # J
    .param p3, "preData"    # J
    .param p5, "count"    # I
    .param p6, "forQA"    # Z

    .prologue
    .line 539
    new-instance v5, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;

    invoke-direct {v5}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;-><init>()V

    .line 541
    .local v5, "outScores":Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    if-eqz v0, :cond_0

    .line 542
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;

    .line 545
    :cond_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;

    move-wide v0, p1

    move-wide/from16 v2, p3

    move/from16 v4, p5

    move/from16 v7, p6

    invoke-static/range {v0 .. v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->doImageScoreWithRawData(JJILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;Z)I

    move-result v11

    .line 546
    .local v11, "ret":I
    if-eqz v11, :cond_2

    .line 547
    const/4 v9, 0x0

    .line 559
    :cond_1
    return-object v9

    .line 551
    :cond_2
    iget-object v0, v5, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;->mScores:[I

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->divideArray([II)[I

    move-result-object v8

    .line 553
    .local v8, "bestIndex":[I
    const/4 v0, 0x5

    new-array v9, v0, [I

    .line 555
    .local v9, "bestInputIndex":[I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    const/4 v0, 0x5

    if-ge v10, v0, :cond_1

    .line 556
    aget v0, v8, v10

    aput v0, v9, v10

    .line 555
    add-int/lit8 v10, v10, 0x1

    goto :goto_0
.end method

.method public static getImageScore([Ljava/lang/String;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;)I
    .locals 1
    .param p0, "images"    # [Ljava/lang/String;
    .param p1, "outScores"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;

    .prologue
    .line 481
    const/4 v0, -0x1

    .line 482
    .local v0, "ret":I
    invoke-static {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->doImageScore([Ljava/lang/String;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;)I

    move-result v0

    .line 483
    return v0
.end method


# virtual methods
.method public PicEditFinish()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 326
    return-void
.end method

.method public PicEditStart()V
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 322
    return-void
.end method

.method public SaveDirect()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 330
    return-void
.end method

.method public SaveMergeResult()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 334
    return-void
.end method

.method public StartMerge(II)V
    .locals 1
    .param p1, "personid"    # I
    .param p2, "faceid"    # I

    .prologue
    .line 410
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonId:I

    .line 411
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceId:I

    .line 412
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->notifyDirty()V

    .line 413
    return-void
.end method

.method public StartProcess()V
    .locals 3

    .prologue
    .line 227
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 229
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    .line 235
    :cond_0
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    .line 316
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 317
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 318
    return-void

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public StartSelectFrame(IIIIJJII)V
    .locals 13
    .param p1, "srcWidth"    # I
    .param p2, "srcHeight"    # I
    .param p3, "screenWidth"    # I
    .param p4, "thumbWidth"    # I
    .param p5, "bitmapPtr"    # J
    .param p7, "preData"    # J
    .param p9, "degree"    # I
    .param p10, "count"    # I

    .prologue
    .line 501
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    if-eqz v1, :cond_0

    .line 502
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SELECT_TIME:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 506
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    .line 508
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    .line 514
    :cond_1
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;

    move-object v2, p0

    move v3, p1

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p9

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;IIIIIJJI)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    .line 531
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 532
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 533
    return-void

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public checkFaceRectsNeeded()[Z
    .locals 5

    .prologue
    .line 391
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v3

    .line 392
    .local v3, "size":I
    new-array v2, v3, [Z

    .line 393
    .local v2, "res":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 394
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetFaceGroup()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    move-result-object v4

    array-length v1, v4

    .line 395
    .local v1, "length":I
    if-lez v1, :cond_0

    .line 396
    const/4 v4, 0x1

    aput-boolean v4, v2, v0

    .line 393
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_0
    const/4 v4, 0x0

    aput-boolean v4, v2, v0

    goto :goto_1

    .line 402
    .end local v1    # "length":I
    :cond_1
    return-object v2
.end method

.method public getAllPersonFaceInfo()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    return-object v0
.end method

.method public getMergeResult(IILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I
    .locals 1
    .param p1, "currentPerson"    # I
    .param p2, "currentFace"    # I
    .param p3, "out"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    invoke-virtual {v0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->applyMerge(IILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I

    move-result v0

    .line 351
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getPersonNum()I
    .locals 1

    .prologue
    .line 406
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I

    return v0
.end method

.method public getRectVector()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;

    return-object v0
.end method

.method public getReferenceImageId()I
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->getReferenceImageId()I

    move-result v0

    .line 367
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResultImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 359
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(IIIIJJ[IIJJ)J
    .locals 17
    .param p1, "imgW"    # I
    .param p2, "imgH"    # I
    .param p3, "displayWidth"    # I
    .param p4, "thumbWidth"    # I
    .param p5, "bitmapPtr"    # J
    .param p7, "preData"    # J
    .param p9, "bestIndex"    # [I
    .param p10, "degree"    # I
    .param p11, "exifBuff"    # J
    .param p13, "exifBuffSize"    # J

    .prologue
    .line 129
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    if-gtz p3, :cond_2

    .line 130
    :cond_0
    const-wide/16 v14, -0x1

    .line 173
    :cond_1
    :goto_0
    return-wide v14

    .line 133
    :cond_2
    const-wide/16 v14, 0x0

    .line 135
    .local v14, "ret":J
    new-instance v4, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    invoke-direct {v4}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    .line 136
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    .line 137
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->start()V

    .line 138
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    .line 150
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;

    .line 151
    new-instance v4, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    invoke-direct {v4}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    .line 153
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    if-eqz v4, :cond_4

    .line 154
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->setProcessBarCallBack(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;)V

    .line 156
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    if-eqz v4, :cond_3

    .line 157
    new-instance v4, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    invoke-direct {v4}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBFPerformanceData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    .line 160
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectPhotoNum:I

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBFPerformanceData:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p4

    move/from16 v10, p10

    invoke-virtual/range {v4 .. v12}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->init(IIIIIIZLcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;)I

    move-result v4

    int-to-long v14, v4

    .line 163
    :cond_4
    const-wide/16 v4, -0x1

    cmp-long v4, v4, v14

    if-eqz v4, :cond_1

    .line 167
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-wide/from16 v0, p11

    move-wide/from16 v2, p13

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->loadExifInfo(JJ)I

    move-result v4

    int-to-long v14, v4

    .line 168
    const-wide/16 v4, 0x0

    cmp-long v4, v14, v4

    if-nez v4, :cond_1

    .line 172
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    const-wide/16 v11, 0x2

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-object/from16 v10, p9

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->SetRawDataAddress(JJ[IJ)I

    move-result v4

    int-to-long v14, v4

    .line 173
    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 421
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 417
    return-void
.end method

.method public saveResult(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 337
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->saveResultImage(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_0
    return-void
.end method

.method public setDebugable(I)V
    .locals 1
    .param p1, "debugable"    # I

    .prologue
    .line 468
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->setDebugable(I)V

    .line 471
    :cond_0
    return-void
.end method

.method public setProcessListener(Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessListener:Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;

    .line 123
    return-void
.end method

.method public unInit()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 177
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    if-eqz v6, :cond_0

    .line 179
    :try_start_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_0
    iput-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectThread:Ljava/lang/Thread;

    .line 186
    :cond_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    if-eqz v6, :cond_1

    .line 188
    :try_start_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 192
    :goto_1
    iput-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessThread:Ljava/lang/Thread;

    .line 195
    :cond_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    if-eqz v6, :cond_2

    .line 196
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->terminate()V

    .line 197
    iput-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeTask:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;

    .line 200
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    if-eqz v6, :cond_3

    .line 201
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->uninit()V

    .line 204
    :cond_3
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    if-eqz v6, :cond_7

    .line 205
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v6

    array-length v5, v6

    .line 206
    .local v5, "personNum":I
    new-array v2, v5, [I

    .line 207
    .local v2, "faceNumArray":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v5, :cond_4

    .line 208
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v6

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetFaceGroup()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    move-result-object v6

    array-length v6, v6

    aput v6, v2, v3

    .line 207
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 180
    .end local v2    # "faceNumArray":[I
    .end local v3    # "i":I
    .end local v5    # "personNum":I
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 189
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 190
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 211
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "faceNumArray":[I
    .restart local v3    # "i":I
    .restart local v5    # "personNum":I
    :cond_4
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v5, :cond_7

    .line 212
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_4
    aget v6, v2, v3

    if-ge v4, v6, :cond_6

    .line 213
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v6

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetFaceGroup()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    move-result-object v6

    aget-object v6, v6, v4

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->GetFaceImg()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 214
    .local v1, "faceBitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_5

    .line 215
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 212
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 211
    .end local v1    # "faceBitmap":Landroid/graphics/Bitmap;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 222
    .end local v2    # "faceNumArray":[I
    .end local v3    # "i":I
    .end local v4    # "j":I
    .end local v5    # "personNum":I
    :cond_7
    iput-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .line 223
    iput-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFirstImage:Landroid/graphics/Bitmap;

    .line 224
    return-void
.end method
