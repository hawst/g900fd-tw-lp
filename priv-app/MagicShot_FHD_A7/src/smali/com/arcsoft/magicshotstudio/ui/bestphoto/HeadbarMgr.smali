.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
.super Ljava/lang/Object;
.source "HeadbarMgr.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field private mBestPhotoLayoutGap:Landroid/view/View;

.field private mBtnClickEnable:Z

.field private mCancelLayoutGap:Landroid/view/View;

.field private mCancelTextView:Landroid/widget/TextView;

.field private mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

.field private mContext:Landroid/content/Context;

.field private mCurTouchedId:I

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

.field private mImgBack:Landroid/widget/ImageView;

.field private mImgBackLayout:Landroid/widget/RelativeLayout;

.field private mImgBestIcon:Landroid/widget/ImageView;

.field private mImgBestIconLayout:Landroid/widget/RelativeLayout;

.field private mImgCancel:Landroid/widget/ImageView;

.field private mImgCancelLayout:Landroid/widget/RelativeLayout;

.field private mImgMenu:Landroid/widget/ImageView;

.field private mImgMenuLayout:Landroid/widget/RelativeLayout;

.field private mImgSave:Landroid/widget/ImageView;

.field private mImgSaveLayout:Landroid/widget/RelativeLayout;

.field private mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

.field private mIsTouchLocked:Z

.field private mLayoutHeadbar:Landroid/widget/RelativeLayout;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

.field private mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOrientation:I

.field private mSaveLayoutGap:Landroid/view/View;

.field private mSaveTextView:Landroid/widget/TextView;

.field private mSaveThread:Ljava/lang/Thread;

.field mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

.field private mShareTextView:Landroid/widget/TextView;

.field private mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

.field mbIsSaveBtnOn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v11, 0x7f06001a

    const/16 v10, 0x8

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v5, "HeadbarMgr"

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->TAG:Ljava/lang/String;

    .line 58
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveLayoutGap:Landroid/view/View;

    .line 59
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCancelLayoutGap:Landroid/view/View;

    .line 60
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mBestPhotoLayoutGap:Landroid/view/View;

    .line 62
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveTextView:Landroid/widget/TextView;

    .line 63
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCancelTextView:Landroid/widget/TextView;

    .line 64
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mShareTextView:Landroid/widget/TextView;

    .line 66
    iput-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    .line 68
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 70
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 72
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 73
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    .line 76
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    .line 78
    iput v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOrientation:I

    .line 206
    iput-boolean v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIsTouchLocked:Z

    .line 207
    iput v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCurTouchedId:I

    .line 208
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;

    invoke-direct {v5, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 233
    iput-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mBtnClickEnable:Z

    .line 240
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;

    invoke-direct {v5, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 362
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$3;

    invoke-direct {v5, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 370
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;

    invoke-direct {v5, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 449
    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveThread:Ljava/lang/Thread;

    .line 81
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    .line 82
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    .line 83
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 84
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 85
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v5, v6}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 88
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090041

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    .line 89
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    const v6, 0x7f090044

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 90
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v5, v11}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(I)V

    .line 92
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090042

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    .line 93
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 94
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v5

    if-nez v5, :cond_0

    .line 95
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 97
    :cond_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 99
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f09004c

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgCancelLayout:Landroid/widget/RelativeLayout;

    .line 100
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgCancelLayout:Landroid/widget/RelativeLayout;

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 101
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090053

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;

    .line 102
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 103
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090059

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgMenuLayout:Landroid/widget/RelativeLayout;

    .line 104
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090046

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIconLayout:Landroid/widget/RelativeLayout;

    .line 105
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIconLayout:Landroid/widget/RelativeLayout;

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 106
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIconLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 108
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090043

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBack:Landroid/widget/ImageView;

    .line 109
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f09004f

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgCancel:Landroid/widget/ImageView;

    .line 110
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090055

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    .line 111
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f09005c

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgMenu:Landroid/widget/ImageView;

    .line 112
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090048

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIcon:Landroid/widget/ImageView;

    .line 115
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgCancelLayout:Landroid/widget/RelativeLayout;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;

    aput-object v6, v5, v7

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgMenuLayout:Landroid/widget/RelativeLayout;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIconLayout:Landroid/widget/RelativeLayout;

    aput-object v7, v5, v6

    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setImgListeners([Landroid/widget/RelativeLayout;)V

    .line 116
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 121
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v4

    .line 122
    .local v4, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 123
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBack:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    :goto_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getExitDialog()Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 136
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-direct {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V

    .line 137
    .local v1, "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v5, v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setBtnOnClick(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 140
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090058

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveLayoutGap:Landroid/view/View;

    .line 141
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090052

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCancelLayoutGap:Landroid/view/View;

    .line 142
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f09004b

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mBestPhotoLayoutGap:Landroid/view/View;

    .line 143
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mBestPhotoLayoutGap:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090057

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveTextView:Landroid/widget/TextView;

    .line 146
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f090051

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCancelTextView:Landroid/widget/TextView;

    .line 147
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    const v6, 0x7f09005e

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mShareTextView:Landroid/widget/TextView;

    .line 149
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 150
    .local v2, "newConfig":Landroid/content/res/Configuration;
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 151
    return-void

    .line 125
    .end local v1    # "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    .end local v2    # "newConfig":Landroid/content/res/Configuration;
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBack:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 126
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 128
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 129
    .local v3, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v0, v5, Landroid/util/DisplayMetrics;->density:F

    .line 130
    .local v0, "density":F
    const/high16 v5, 0x41600000    # 14.0f

    mul-float/2addr v5, v0

    float-to-double v6, v5

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 131
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v5, v3}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIsTouchLocked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIsTouchLocked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCurTouchedId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->isTalkBackOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCurTouchedId:I

    return p1
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->popUpToast(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOrientation:I

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->finishActivity()V

    return-void
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private finishActivity()V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->finish()V

    .line 447
    return-void
.end method

.method private isTalkBackOn()Z
    .locals 1

    .prologue
    .line 585
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->isTalkBackOn()Z

    move-result v0

    return v0
.end method

.method private popUpToast(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 406
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    .line 408
    move-object v0, p1

    .line 409
    .local v0, "pressedView":Landroid/view/View;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v1

    .line 410
    .local v1, "screenWith":I
    const/4 v2, 0x0

    .line 411
    .local v2, "textRes":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 432
    :goto_0
    :sswitch_0
    if-eqz v2, :cond_0

    .line 433
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v3, v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showPopUpToast(Landroid/view/View;II)V

    .line 436
    :cond_0
    const/4 v3, 0x1

    return v3

    .line 413
    :sswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBack:Landroid/widget/ImageView;

    .line 414
    const v2, 0x7f060027

    .line 415
    goto :goto_0

    .line 417
    :sswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgCancel:Landroid/widget/ImageView;

    .line 418
    const v2, 0x7f06000e

    .line 419
    goto :goto_0

    .line 421
    :sswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    .line 422
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    if-eqz v3, :cond_1

    const v2, 0x7f06000c

    .line 423
    :goto_1
    goto :goto_0

    .line 422
    :cond_1
    const v2, 0x7f060018

    goto :goto_1

    .line 427
    :sswitch_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIcon:Landroid/widget/ImageView;

    .line 428
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_2

    const v2, 0x7f060051

    :goto_2
    goto :goto_0

    :cond_2
    const v2, 0x7f060052

    goto :goto_2

    .line 411
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_1
        0x7f090046 -> :sswitch_4
        0x7f09004c -> :sswitch_2
        0x7f090053 -> :sswitch_3
        0x7f090059 -> :sswitch_0
    .end sparse-switch
.end method

.method private varargs setImgListeners([Landroid/widget/RelativeLayout;)V
    .locals 5
    .param p1, "views"    # [Landroid/widget/RelativeLayout;

    .prologue
    .line 169
    move-object v0, p1

    .local v0, "arr$":[Landroid/widget/RelativeLayout;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 170
    .local v3, "vw":Landroid/widget/RelativeLayout;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 172
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    .end local v3    # "vw":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method private updateGapValue(ZF)V
    .locals 0
    .param p1, "isLandScape"    # Z
    .param p2, "mScreenDensity"    # F

    .prologue
    .line 553
    if-eqz p1, :cond_0

    .line 575
    :cond_0
    return-void
.end method


# virtual methods
.method public changeSaveBtnState(Z)V
    .locals 7
    .param p1, "isSaveState"    # Z

    .prologue
    const v5, 0x7f060018

    const v4, 0x7f06000c

    .line 333
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveTextView:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 334
    if-eqz p1, :cond_2

    .line 335
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    .line 336
    .local v0, "enabled":Z
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    const v3, 0x7f02000f

    :goto_0
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 342
    .end local v0    # "enabled":Z
    :goto_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(I)V

    .line 345
    :cond_0
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    .line 346
    if-eqz p1, :cond_4

    .line 347
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "sSave":Ljava/lang/String;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 355
    .end local v1    # "sSave":Ljava/lang/String;
    :goto_3
    return-void

    .line 336
    .restart local v0    # "enabled":Z
    :cond_1
    const v3, 0x7f020010

    goto :goto_0

    .line 340
    .end local v0    # "enabled":Z
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSave:Landroid/widget/ImageView;

    const v6, 0x7f020013

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    move v3, v5

    .line 342
    goto :goto_2

    .line 351
    :cond_4
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 353
    .local v2, "sShare":Ljava/lang/String;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "defile"    # Ljava/lang/String;

    .prologue
    .line 537
    if-nez p1, :cond_0

    .line 538
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getSavePath()Ljava/lang/String;

    move-result-object p1

    .line 540
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 541
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 542
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 544
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 545
    .local v0, "data":Landroid/net/Uri;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 546
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLayout()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mLayoutHeadbar:Landroid/widget/RelativeLayout;

    .line 189
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hidePopupToast()V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->hide()V

    .line 443
    :cond_0
    return-void
.end method

.method public isBtnClickEnable()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mBtnClickEnable:Z

    return v0
.end method

.method public isSaveState()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mbIsSaveBtnOn:Z

    .line 359
    .local v0, "ret":Z
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 578
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->hidePopupToast()V

    .line 579
    const/4 v2, 0x2

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 580
    .local v1, "landscape":Z
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 581
    .local v0, "density":F
    invoke-direct {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->updateGapValue(ZF)V

    .line 582
    return-void

    .line 579
    .end local v0    # "density":F
    .end local v1    # "landscape":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public saveFiles()V
    .locals 3

    .prologue
    .line 452
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    if-nez v1, :cond_2

    .line 453
    :cond_0
    const-string v1, "HeadbarMgr"

    const-string v2, "IndicateLine error"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_1
    :goto_0
    return-void

    .line 459
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBestCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 463
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveThread:Ljava/lang/Thread;

    if-eqz v1, :cond_3

    .line 465
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveThread:Ljava/lang/Thread;

    .line 472
    :cond_3
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveThread:Ljava/lang/Thread;

    .line 532
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->lockSaveProcess()V

    .line 533
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public setBestIconSelected(Z)V
    .locals 4
    .param p1, "isSelected"    # Z

    .prologue
    .line 193
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 194
    if-eqz p1, :cond_0

    .line 195
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060051

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "sOn":Ljava/lang/String;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIconLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 203
    .end local v1    # "sOn":Ljava/lang/String;
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060052

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "sOff":Ljava/lang/String;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mImgBestIconLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setBtnClickEnable(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mBtnClickEnable:Z

    .line 236
    return-void
.end method

.method public setParam(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;I)V
    .locals 0
    .param p1, "ind"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    .param p2, "can"    # Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    .param p3, "orientation"    # I

    .prologue
    .line 163
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    .line 164
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    .line 165
    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOrientation:I

    .line 166
    return-void
.end method

.method public setState(IZ)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 154
    .line 160
    return-void
.end method
