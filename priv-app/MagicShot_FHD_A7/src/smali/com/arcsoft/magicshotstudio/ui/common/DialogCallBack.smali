.class public Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
.super Ljava/lang/Object;
.source "DialogCallBack.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;
    }
.end annotation


# instance fields
.field private callback:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# direct methods
.method public constructor <init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V
    .locals 1
    .param p1, "callBack"    # Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;->callback:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 12
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;->callback:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 13
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;->callback:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-interface {v0, p2}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;->onClick(I)V

    .line 29
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;->callback:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;->onClick(I)V

    .line 23
    return-void
.end method
