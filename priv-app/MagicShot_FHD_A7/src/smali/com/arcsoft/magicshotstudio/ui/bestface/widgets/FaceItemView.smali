.class public Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
.super Landroid/widget/ImageView;
.source "FaceItemView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_FaceItemView"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->mBitmap:Landroid/graphics/Bitmap;

    .line 14
    return-void
.end method


# virtual methods
.method public bitmapRecycle()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->mBitmap:Landroid/graphics/Bitmap;

    .line 33
    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 17
    if-eqz p1, :cond_0

    .line 23
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 25
    :cond_0
    return-void
.end method

.method public setIsSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setSelected(Z)V

    .line 37
    return-void
.end method
