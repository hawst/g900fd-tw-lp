.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 1104
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 3
    .param p1, "buttonId"    # I

    .prologue
    .line 1107
    packed-switch p1, :pswitch_data_0

    .line 1131
    :cond_0
    :goto_0
    return-void

    .line 1109
    :pswitch_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1110
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 1112
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_0

    .line 1115
    :pswitch_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1116
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 1118
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->exitWidthoutSave()V
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    goto :goto_0

    .line 1121
    :pswitch_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1122
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 1124
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResult()Z

    move-result v0

    .line 1125
    .local v0, "success":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->exitActivity()V

    .line 1126
    if-eqz v0, :cond_0

    .line 1127
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$6;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x5000

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 1107
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
