.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;
.super Ljava/lang/Object;
.source "HeadbarMgr.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 212
    const/4 v0, 0x0

    .line 214
    .local v0, "consumed":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIsTouchLocked:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCurTouchedId:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 215
    const/4 v0, 0x1

    move v1, v0

    .line 229
    .end local v0    # "consumed":Z
    .local v1, "consumed":I
    :goto_0
    return v1

    .line 219
    .end local v1    # "consumed":I
    .restart local v0    # "consumed":Z
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 220
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIsTouchLocked:Z
    invoke-static {v2, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$002(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;Z)Z

    .line 221
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCurTouchedId:I
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;I)I

    .line 224
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v5, v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 226
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIsTouchLocked:Z
    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$002(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;Z)Z

    .line 227
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCurTouchedId:I
    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;I)I

    :cond_3
    move v1, v0

    .line 229
    .restart local v1    # "consumed":I
    goto :goto_0
.end method
