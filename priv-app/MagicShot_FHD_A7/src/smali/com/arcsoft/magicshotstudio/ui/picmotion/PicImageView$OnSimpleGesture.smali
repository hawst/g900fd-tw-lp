.class Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PicImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnSimpleGesture"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 515
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$100(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 516
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mbIsZoom:Z
    invoke-static {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$202(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;Z)Z

    .line 518
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 520
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getWidth()I

    move-result v1

    .line 521
    .local v1, "w":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getHeight()I

    move-result v0

    .line 522
    .local v0, "h":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    int-to-float v5, v1

    div-float/2addr v4, v5

    iput v4, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mDoubleTapDx:F

    .line 523
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    int-to-float v5, v0

    div-float/2addr v4, v5

    iput v4, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mDoubleTapDy:F

    .line 525
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 527
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iput v6, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mDoubleTapDx:F

    .line 528
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iput v6, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mDoubleTapDy:F

    .line 531
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    if-ne v1, v3, :cond_2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    if-le v0, v3, :cond_2

    .line 533
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iput v6, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mDoubleTapDy:F

    .line 534
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    if-le v1, v3, :cond_3

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mRectDst:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    if-ne v0, v3, :cond_3

    .line 536
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iput v6, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mDoubleTapDx:F

    .line 538
    :cond_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$100(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;->minZoom()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v2, 0x0

    :cond_4
    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mbIsZoomOut:Z
    invoke-static {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$402(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;Z)Z

    .line 541
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->invalidate()V

    .line 543
    .end local v0    # "h":I
    .end local v1    # "w":I
    :cond_5
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 502
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mManualMask:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v1, :cond_0

    .line 503
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchDefaultActionBar()V

    .line 505
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mManualMask:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 506
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    move-result-object v0

    .line 507
    .local v0, "workStatus":Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    if-ne v1, v0, :cond_1

    .line 508
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchAllViews()V

    .line 510
    .end local v0    # "workStatus":Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
    :cond_1
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method
