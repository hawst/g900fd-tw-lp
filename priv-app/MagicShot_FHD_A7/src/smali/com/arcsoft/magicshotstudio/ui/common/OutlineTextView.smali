.class public Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;
.super Landroid/widget/TextView;
.source "OutlineTextView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_OutlineTextView"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTextColor:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mTextColor:Landroid/content/res/ColorStateList;

    .line 26
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mTextColor:Landroid/content/res/ColorStateList;

    .line 32
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mTextColor:Landroid/content/res/ColorStateList;

    .line 38
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method private canMarquee()Z
    .locals 10

    .prologue
    .line 215
    const/4 v1, 0x0

    .line 217
    .local v1, "canmarquee":Z
    const-string v7, "ArcSoft_OutlineTextView"

    const-string v8, "canMarquee"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "canMarquee"

    const/4 v7, 0x0

    check-cast v7, [Ljava/lang/Class;

    invoke-virtual {v8, v9, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 220
    .local v0, "canMarquee":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 221
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    move-result v1

    .line 234
    .end local v0    # "canMarquee":Ljava/lang/reflect/Method;
    :goto_0
    return v1

    .line 222
    :catch_0
    move-exception v2

    .line 223
    .local v2, "e1":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 224
    .end local v2    # "e1":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v3

    .line 225
    .local v3, "e2":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 226
    .end local v3    # "e2":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v4

    .line 227
    .local v4, "e3":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 228
    .end local v4    # "e3":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v5

    .line 229
    .local v5, "e4":Ljava/lang/IllegalAccessException;
    invoke-virtual {v5}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 230
    .end local v5    # "e4":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v6

    .line 231
    .local v6, "e5":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v6}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private changeTextColor(Landroid/content/res/ColorStateList;)V
    .locals 7
    .param p1, "color"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 52
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "mTextColor"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 55
    .local v2, "mTextColor":Ljava/lang/reflect/Field;
    if-eqz v2, :cond_0

    .line 56
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 57
    invoke-virtual {v2, p0, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getDrawableState()[I

    move-result-object v4

    .line 61
    .local v4, "stateSet":[I
    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    .line 63
    .local v3, "stateColor":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "mCurTextColor"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 66
    .local v1, "mCurTextColor":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_1

    .line 67
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 68
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, p0, v5}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v1    # "mCurTextColor":Ljava/lang/reflect/Field;
    .end local v2    # "mTextColor":Ljava/lang/reflect/Field;
    .end local v3    # "stateColor":I
    .end local v4    # "stateSet":[I
    :cond_1
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setMarqueeFadeMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 173
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "mMarqueeFadeMode"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 175
    .local v1, "mMarqueeFadeMode":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 176
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 177
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 188
    .end local v1    # "mMarqueeFadeMode":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 182
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 185
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 101
    return-void
.end method

.method public isFocused()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "focus"
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/widget/TextView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 159
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->canMarquee()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setMarqueeRepeatLimit(I)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->startStopMarquee(Z)V

    .line 163
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 78
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    .local v0, "resource":Landroid/content/res/Resources;
    const v2, 0x7f04001f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 80
    .local v1, "states":Landroid/content/res/ColorStateList;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 81
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 82
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->changeTextColor(Landroid/content/res/ColorStateList;)V

    .line 83
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 85
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mTextColor:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_0

    .line 86
    const v2, 0x7f04001e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 91
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 92
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->changeTextColor(Landroid/content/res/ColorStateList;)V

    .line 93
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 94
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mTextColor:Landroid/content/res/ColorStateList;

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 145
    invoke-super {p0, p1}, Landroid/widget/TextView;->onWindowFocusChanged(Z)V

    .line 146
    const-string v0, "ArcSoft_OutlineTextView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged hasWindowFocus =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->canMarquee()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setMarqueeRepeatLimit(I)V

    .line 149
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->startStopMarquee(Z)V

    .line 154
    :cond_0
    return-void
.end method

.method public setColor(Landroid/content/res/ColorStateList;)V
    .locals 0
    .param p1, "color"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->mTextColor:Landroid/content/res/ColorStateList;

    .line 47
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->invalidate()V

    .line 48
    return-void
.end method

.method public setPressed(Z)V
    .locals 5
    .param p1, "pressed"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 197
    const-string v0, "ArcSoft_OutlineTextView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPressed pressed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    if-eqz p1, :cond_0

    .line 199
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setMarqueeRepeatLimit(I)V

    .line 200
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->startStopMarquee(Z)V

    .line 207
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->setPressed(Z)V

    .line 208
    return-void

    .line 202
    :cond_0
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->startStopMarquee(Z)V

    .line 204
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setMarqueeRepeatLimit(I)V

    .line 205
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->startStopMarquee(Z)V

    goto :goto_0
.end method

.method public startStopMarquee(Z)V
    .locals 12
    .param p1, "start"    # Z

    .prologue
    .line 117
    const-string v7, "ArcSoft_OutlineTextView"

    const-string v8, "startStopMarquee"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "startStopMarquee"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    sget-object v11, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 121
    .local v6, "startStopMarquee":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 122
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 123
    .local v5, "object":Ljava/lang/Boolean;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-virtual {v6, p0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 136
    .end local v5    # "object":Ljava/lang/Boolean;
    .end local v6    # "startStopMarquee":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e1":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 126
    .end local v0    # "e1":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 127
    .local v1, "e2":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 128
    .end local v1    # "e2":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v2

    .line 129
    .local v2, "e3":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 130
    .end local v2    # "e3":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v3

    .line 131
    .local v3, "e4":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 132
    .end local v3    # "e4":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v4

    .line 133
    .local v4, "e5":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method
