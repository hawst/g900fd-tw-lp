.class public Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;
.super Landroid/view/View;
.source "EdgeView.java"


# static fields
.field public static final BOTTOM:I = 0x4

.field public static final DIR_ALL:I = 0x0

.field public static final DIR_BOTTOM:I = 0x8

.field public static final DIR_LEFT:I = 0x1

.field public static final DIR_RIGHT:I = 0x4

.field public static final DIR_SHOW_ALL:I = 0x10

.field public static final DIR_TOP:I = 0x2

.field public static final LEFT:I = 0x2

.field public static final RIGHT:I = 0x8

.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_EdgeView"

.field public static final TOP:I = 0x1


# instance fields
.field private mEdgeBottom:I

.field private mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

.field private mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

.field private mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

.field private mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

.field private mEdgeLeft:I

.field private mEdgeRight:I

.field private mEdgeTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 28
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 29
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 30
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 32
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeLeft:I

    .line 33
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeRight:I

    .line 34
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeTop:I

    .line 35
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeBottom:I

    .line 39
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 40
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 41
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 42
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 43
    return-void
.end method

.method private drawBottomEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 220
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    const/4 v0, 0x0

    .line 236
    :goto_0
    return v0

    .line 224
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 231
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeRight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeBottom:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 232
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 234
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 235
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawLeftEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 172
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    .line 176
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 180
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeBottom:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 181
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 183
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 184
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawRightEdge(Landroid/graphics/Canvas;)Z
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 189
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    .line 193
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 194
    .local v1, "restoreCount":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->getPaddingRight()I

    move-result v4

    sub-int v2, v3, v4

    .line 196
    .local v2, "width":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeRight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeTop:I

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 197
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 199
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 200
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawTopEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 205
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    const/4 v0, 0x0

    .line 216
    :goto_0
    return v0

    .line 210
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 212
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeTop:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 214
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 215
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 143
    const/4 v1, 0x0

    .line 145
    .local v1, "needsInvalidate":Z
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeRight:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeLeft:I

    sub-int v2, v3, v4

    .line 146
    .local v2, "w":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeBottom:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeTop:I

    sub-int v0, v3, v4

    .line 147
    .local v0, "h":I
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 148
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_0

    .line 149
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->drawLeftEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 152
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_1

    .line 153
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->drawRightEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 156
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_2

    .line 157
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->drawTopEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 160
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_3

    .line 161
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->drawBottomEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 164
    :cond_3
    if-eqz v1, :cond_4

    .line 165
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->invalidate()V

    .line 169
    :cond_4
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 48
    if-nez p1, :cond_0

    .line 49
    :cond_0
    return-void
.end method

.method public onPull(FI)V
    .locals 2
    .param p1, "offset"    # F
    .param p2, "direction"    # I

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 75
    .local v0, "tempEdgeEffect":Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;
    packed-switch p2, :pswitch_data_0

    .line 92
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onPull(F)V

    .line 94
    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->invalidate()V

    .line 99
    :cond_0
    const/4 v0, 0x0

    .line 100
    return-void

    .line 77
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 78
    goto :goto_0

    .line 80
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 81
    goto :goto_0

    .line 83
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 84
    goto :goto_0

    .line 86
    :pswitch_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    .line 87
    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onRelease()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 104
    const/4 v0, 0x0

    .line 109
    .local v0, "more":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 110
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    .line 112
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 113
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    or-int/2addr v0, v1

    .line 115
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 116
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    or-int/2addr v0, v1

    .line 118
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 119
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_3
    or-int/2addr v0, v2

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->invalidate()V

    .line 124
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 110
    goto :goto_0

    :cond_2
    move v1, v3

    .line 113
    goto :goto_1

    :cond_3
    move v1, v3

    .line 116
    goto :goto_2

    :cond_4
    move v2, v3

    .line 119
    goto :goto_3
.end method

.method public onRelease(I)V
    .locals 1
    .param p1, "releaseSide"    # I

    .prologue
    .line 126
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 129
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 132
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 135
    :cond_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_3

    .line 136
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->onRelease()V

    .line 138
    :cond_3
    return-void
.end method

.method public setEdgeBound(IIII)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 52
    sub-int v1, p3, p1

    .line 53
    .local v1, "w":I
    sub-int v0, p4, p2

    .line 55
    .local v0, "h":I
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeLeft:I

    .line 56
    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeRight:I

    .line 57
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeTop:I

    .line 58
    iput p4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeBottom:I

    .line 60
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setSize(II)V

    .line 61
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setSize(II)V

    .line 62
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setSize(II)V

    .line 63
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setSize(II)V

    .line 65
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 66
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 67
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 68
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 69
    return-void
.end method
