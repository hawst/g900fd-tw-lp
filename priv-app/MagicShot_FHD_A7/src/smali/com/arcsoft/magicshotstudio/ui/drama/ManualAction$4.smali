.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 900
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 903
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 904
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 925
    :cond_0
    :goto_0
    return v2

    .line 906
    :sswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 908
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iput-boolean v1, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 909
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-nez v0, :cond_0

    .line 910
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    const v1, 0x7f060048

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V

    goto :goto_0

    .line 915
    :sswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 917
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iput-boolean v1, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 918
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-nez v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    const v1, 0x7f060049

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V

    goto :goto_0

    .line 904
    :sswitch_data_0
    .sparse-switch
        0x7f090083 -> :sswitch_0
        0x7f090087 -> :sswitch_1
    .end sparse-switch
.end method
