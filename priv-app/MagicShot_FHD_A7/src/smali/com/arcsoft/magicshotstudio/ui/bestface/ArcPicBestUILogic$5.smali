.class Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;
.super Ljava/lang/Thread;
.source "ArcPicBestUILogic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->saveResult()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 0

    .prologue
    .line 1008
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1012
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$700(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-result-object v0

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SAVE_FILE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 1016
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->createFile(Ljava/lang/String;)V

    .line 1017
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V

    .line 1018
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mInputNum:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$900(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)I

    move-result v0

    if-eq v3, v0, :cond_1

    .line 1019
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->saveResult(Ljava/lang/String;)V

    .line 1024
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x832

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCameraDegress:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1000(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->refreshDBRecords(Landroid/content/Context;Ljava/lang/String;II)Z

    .line 1025
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1026
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->access$1200(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1027
    return-void
.end method
