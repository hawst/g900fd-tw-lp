.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.super Landroid/widget/AdapterView;
.source "FrameListView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$MathUtils;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$WindowRunnnable;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;"
    }
.end annotation


# static fields
.field private static final CHECK_POSITION_SEARCH_DISTANCE:I = 0x14

.field private static final INVALID_POINTER:I = -0x1

.field private static final LAYOUT_FORCE_BOTTOM:I = 0x3

.field private static final LAYOUT_FORCE_TOP:I = 0x1

.field private static final LAYOUT_MOVE_SELECTION:I = 0x6

.field private static final LAYOUT_NORMAL:I = 0x0

.field private static final LAYOUT_SET_SELECTION:I = 0x2

.field private static final LAYOUT_SPECIFIC:I = 0x4

.field private static final LAYOUT_SYNC:I = 0x5

.field private static final MAX_SCROLL_FACTOR:F = 0.3f

.field private static final MIN_SCROLL_PREVIEW_PIXELS:I = 0xa

.field private static final NO_POSITION:I = -0x1

.field public static final STATE_NOTHING:[I

.field private static final SYNC_FIRST_POSITION:I = 0x1

.field private static final SYNC_MAX_DURATION_MILLIS:I = 0x64

.field private static final SYNC_SELECTED_POSITION:I = 0x0

.field private static final TOUCH_MODE_DONE_WAITING:I = 0x2

.field private static final TOUCH_MODE_DOWN:I = 0x0

.field private static final TOUCH_MODE_DRAGGING:I = 0x3

.field private static final TOUCH_MODE_FLINGING:I = 0x4

.field private static final TOUCH_MODE_OFF:I = 0x1

.field private static final TOUCH_MODE_ON:I = 0x0

.field private static final TOUCH_MODE_OVERSCROLL:I = 0x5

.field private static final TOUCH_MODE_REST:I = -0x1

.field private static final TOUCH_MODE_TAP:I = 0x1

.field private static final TOUCH_MODE_UNKNOWN:I = -0x1

.field private static final tag:Ljava/lang/String; = "FrameListView"


# instance fields
.field private mAccessibilityDelegate:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;

.field private mActivePointerId:I

.field private mAdapter:Landroid/widget/ListAdapter;

.field private mAreAllItemsSelectable:Z

.field private final mArrowScrollFocusResult:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

.field private mBlockLayoutRequests:Z

.field mCannotScrollDown:Z

.field mCannotScrollUp:Z

.field private mCheckStates:Landroid/util/SparseBooleanArray;

.field mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckedItemCount:I

.field private mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mDataChanged:Z

.field private mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

.field private mDesiredFocusableInTouchModeState:Z

.field private mDesiredFocusableState:Z

.field private mDrawSelectorOnTop:Z

.field private mEmptyView:Landroid/view/View;

.field private mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mFirstPosition:I

.field private final mFlingVelocity:I

.field private mHasStableIds:Z

.field private mInLayout:Z

.field private mIsAttached:Z

.field private mIsChildViewEnabled:Z

.field final mIsScrap:[Z

.field private mIsVertical:Z

.field private mItemCount:I

.field private mItemMargin:I

.field private mItemsCanFocus:Z

.field private mLastAccessibilityScrollEventFromIndex:I

.field private mLastAccessibilityScrollEventToIndex:I

.field private mLastScrollState:I

.field private mLastTouchMode:I

.field private mLastTouchPos:F

.field private mLayoutMode:I

.field private final mMaximumVelocity:I

.field private mMotionPosition:I

.field private mNeedSync:Z

.field private mNextSelectedPosition:I

.field private mNextSelectedRowId:J

.field private mOldItemCount:I

.field private mOldSelectedPosition:I

.field private mOldSelectedRowId:J

.field private mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

.field private mOverScroll:I

.field private final mOverscrollDistance:I

.field private mPendingCheckForKeyLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;

.field private mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

.field private mPendingCheckForTap:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

.field private mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

.field private mPerformClick:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

.field private final mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

.field private mResurrectToPosition:I

.field private mScrollbarOrientation:I

.field private final mScroller:Landroid/widget/Scroller;

.field private mSelectedPosition:I

.field private mSelectedRowId:J

.field private mSelectedStart:I

.field private mSelectionNotifier:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;

.field private mSelector:Landroid/graphics/drawable/Drawable;

.field private mSelectorPosition:I

.field private final mSelectorRect:Landroid/graphics/Rect;

.field private mSpecificStart:I

.field private mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mSyncHeight:J

.field private mSyncMode:I

.field private mSyncPosition:I

.field private mSyncRowId:J

.field private mTTSPosition:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchFrame:Landroid/graphics/Rect;

.field private mTouchMode:I

.field private mTouchModeReset:Ljava/lang/Runnable;

.field private mTouchRemainderPos:F

.field private final mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->STATE_NOTHING:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 314
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 318
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 319
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 322
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 159
    const/4 v6, 0x1

    new-array v6, v6, [Z

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    .line 3061
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollDown:Z

    .line 3062
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollUp:Z

    .line 5450
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTTSPosition:I

    .line 324
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 325
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 327
    const/4 v6, 0x0

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 328
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 329
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchMode:I

    .line 331
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    .line 333
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 335
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    .line 336
    const/4 v6, 0x0

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastScrollState:I

    .line 338
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v5

    .line 339
    .local v5, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchSlop:I

    .line 340
    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMaximumVelocity:I

    .line 341
    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFlingVelocity:I

    .line 342
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScaledOverscrollDistance(Landroid/view/ViewConfiguration;)I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverscrollDistance:I

    .line 344
    const/4 v6, 0x0

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 346
    new-instance v6, Landroid/widget/Scroller;

    invoke-direct {v6, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    .line 348
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    .line 351
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    .line 353
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    .line 355
    new-instance v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mArrowScrollFocusResult:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    .line 357
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    .line 359
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 360
    const/4 v6, 0x0

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 362
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 364
    const/4 v6, 0x0

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 365
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    .line 366
    const-wide/high16 v6, -0x8000000000000000L

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    .line 367
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 368
    const-wide/high16 v6, -0x8000000000000000L

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    .line 369
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedPosition:I

    .line 370
    const-wide/high16 v6, -0x8000000000000000L

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedRowId:J

    .line 372
    sget-object v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    .line 373
    const/4 v6, 0x0

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 374
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 375
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 377
    new-instance v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-direct {v6, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    .line 378
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    .line 380
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAreAllItemsSelectable:Z

    .line 382
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 383
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 385
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setClickable(Z)V

    .line 386
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setFocusableInTouchMode(Z)V

    .line 387
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setWillNotDraw(Z)V

    .line 388
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 389
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setWillNotDraw(Z)V

    .line 390
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setClipToPadding(Z)V

    .line 392
    const/4 v6, 0x1

    invoke-static {p0, v6}, Landroid/support/v4/view/ViewCompat;->setOverScrollMode(Landroid/view/View;I)V

    .line 396
    sget-object v6, Lcom/arcsoft/magicshotstudio/R$styleable;->FrameListView:[I

    const/4 v7, 0x0

    invoke-virtual {p1, p2, v6, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 397
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->initializeScrollbars(Landroid/content/res/TypedArray;)V

    .line 399
    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDrawSelectorOnTop:Z

    .line 402
    const/16 v6, 0xa

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 403
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 404
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 407
    :cond_0
    const/16 v6, 0x8

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 408
    .local v3, "orientation":I
    if-ltz v3, :cond_1

    .line 409
    invoke-static {}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->values()[Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    move-result-object v6

    aget-object v6, v6, v3

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setOrientation(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;)V

    .line 412
    :cond_1
    const/16 v6, 0xd

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 413
    .local v1, "choiceMode":I
    if-ltz v1, :cond_2

    .line 414
    invoke-static {}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->values()[Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setChoiceMode(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;)V

    .line 417
    :cond_2
    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    .line 418
    .local v4, "spacing":I
    if-ltz v4, :cond_3

    .line 419
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setItemMargin(I)V

    .line 422
    :cond_3
    const/16 v6, 0x9

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScrollbarOrientation:I

    .line 424
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 426
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateScrollbarsDirection()V

    .line 427
    return-void
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I

    return v0
.end method

.method static synthetic access$1702(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I

    return p1
.end method

.method static synthetic access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    return v0
.end method

.method static synthetic access$1802(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    return p1
.end method

.method static synthetic access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->rememberSyncState()V

    return-void
.end method

.method static synthetic access$2100(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkFocus()V

    return-void
.end method

.method static synthetic access$2200(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    return v0
.end method

.method static synthetic access$2202(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    return p1
.end method

.method static synthetic access$2300(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    return-wide v0
.end method

.method static synthetic access$2302(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # J

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    return-wide p1
.end method

.method static synthetic access$2402(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    return p1
.end method

.method static synthetic access$2502(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # J

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    return-wide p1
.end method

.method static synthetic access$2602(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fireOnSelected()V

    return-void
.end method

.method static synthetic access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->performAccessibilityActionsOnSelected()V

    return-void
.end method

.method static synthetic access$3000(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$3300(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    return v0
.end method

.method static synthetic access$3400(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    return v0
.end method

.method static synthetic access$3502(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    return p1
.end method

.method static synthetic access$3600(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    return-void
.end method

.method static synthetic access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;ILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->triggerCheckForLongPress()V

    return-void
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    return v0
.end method

.method static synthetic access$4000(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;IJ)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->performLongPress(Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    return p1
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$502(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$602(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    return-object p1
.end method

.method private adjustViewsStartOrEnd()V
    .locals 4

    .prologue
    .line 5327
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 5348
    :cond_0
    :goto_0
    return-void

    .line 5331
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5334
    .local v1, "firstChild":Landroid/view/View;
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 5335
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    sub-int v0, v2, v3

    .line 5340
    .local v0, "delta":I
    :goto_1
    if-gez v0, :cond_2

    .line 5342
    const/4 v0, 0x0

    .line 5345
    :cond_2
    if-eqz v0, :cond_0

    .line 5346
    neg-int v2, v0

    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetChildren(I)V

    goto :goto_0

    .line 5337
    .end local v0    # "delta":I
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    sub-int v0, v2, v3

    .restart local v0    # "delta":I
    goto :goto_1
.end method

.method private amountToScroll(II)I
    .locals 20
    .param p1, "direction"    # I
    .param p2, "nextSelectedPosition"    # I

    .prologue
    .line 2325
    invoke-direct/range {p0 .. p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 2327
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v12

    .line 2329
    .local v12, "numChildren":I
    const/16 v18, 0x82

    move/from16 v0, p1

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    const/16 v18, 0x42

    move/from16 v0, p1

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 2330
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v19

    sub-int v3, v18, v19

    .line 2333
    .local v3, "end":I
    :goto_0
    add-int/lit8 v8, v12, -0x1

    .line 2334
    .local v8, "indexToMakeVisible":I
    const/16 v18, -0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 2335
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v18, v0

    sub-int v8, p2, v18

    .line 2338
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v18, v0

    add-int v13, v18, v8

    .line 2339
    .local v13, "positionToMakeVisible":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 2341
    .local v15, "viewToMakeVisible":Landroid/view/View;
    move v6, v3

    .line 2342
    .local v6, "goalEnd":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ge v13, v0, :cond_2

    .line 2343
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getArrowScrollPreviewLength()I

    move-result v18

    sub-int v6, v6, v18

    .line 2346
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    invoke-virtual {v15}, Landroid/view/View;->getTop()I

    move-result v17

    .line 2348
    .local v17, "viewToMakeVisibleStart":I
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    invoke-virtual {v15}, Landroid/view/View;->getBottom()I

    move-result v16

    .line 2351
    .local v16, "viewToMakeVisibleEnd":I
    :goto_2
    move/from16 v0, v16

    if-gt v0, v6, :cond_6

    .line 2353
    const/16 v18, 0x0

    .line 2417
    .end local v3    # "end":I
    .end local v6    # "goalEnd":I
    :goto_3
    return v18

    .line 2330
    .end local v8    # "indexToMakeVisible":I
    .end local v13    # "positionToMakeVisible":I
    .end local v15    # "viewToMakeVisible":Landroid/view/View;
    .end local v16    # "viewToMakeVisibleEnd":I
    .end local v17    # "viewToMakeVisibleStart":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v19

    sub-int v3, v18, v19

    goto :goto_0

    .line 2346
    .restart local v3    # "end":I
    .restart local v6    # "goalEnd":I
    .restart local v8    # "indexToMakeVisible":I
    .restart local v13    # "positionToMakeVisible":I
    .restart local v15    # "viewToMakeVisible":Landroid/view/View;
    :cond_4
    invoke-virtual {v15}, Landroid/view/View;->getLeft()I

    move-result v17

    goto :goto_1

    .line 2348
    .restart local v17    # "viewToMakeVisibleStart":I
    :cond_5
    invoke-virtual {v15}, Landroid/view/View;->getRight()I

    move-result v16

    goto :goto_2

    .line 2356
    .restart local v16    # "viewToMakeVisibleEnd":I
    :cond_6
    const/16 v18, -0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-eq v0, v1, :cond_7

    sub-int v18, v6, v17

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getMaxScrollAmount()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_7

    .line 2359
    const/16 v18, 0x0

    goto :goto_3

    .line 2362
    :cond_7
    sub-int v2, v16, v6

    .line 2364
    .local v2, "amountToScroll":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v18, v0

    add-int v18, v18, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 2365
    add-int/lit8 v18, v12, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 2366
    .local v9, "lastChild":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v10

    .line 2369
    .local v10, "lastChildEnd":I
    :goto_4
    sub-int v11, v10, v3

    .line 2370
    .local v11, "max":I
    invoke-static {v2, v11}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 2373
    .end local v9    # "lastChild":Landroid/view/View;
    .end local v10    # "lastChildEnd":I
    .end local v11    # "max":I
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getMaxScrollAmount()I

    move-result v18

    move/from16 v0, v18

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v18

    goto :goto_3

    .line 2366
    .restart local v9    # "lastChild":Landroid/view/View;
    :cond_9
    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v10

    goto :goto_4

    .line 2375
    .end local v2    # "amountToScroll":I
    .end local v3    # "end":I
    .end local v6    # "goalEnd":I
    .end local v8    # "indexToMakeVisible":I
    .end local v9    # "lastChild":Landroid/view/View;
    .end local v13    # "positionToMakeVisible":I
    .end local v15    # "viewToMakeVisible":Landroid/view/View;
    .end local v16    # "viewToMakeVisibleEnd":I
    .end local v17    # "viewToMakeVisibleStart":I
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v14

    .line 2377
    .local v14, "start":I
    :goto_5
    const/4 v8, 0x0

    .line 2378
    .restart local v8    # "indexToMakeVisible":I
    const/16 v18, -0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-eq v0, v1, :cond_b

    .line 2379
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v18, v0

    sub-int v8, p2, v18

    .line 2382
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v18, v0

    add-int v13, v18, v8

    .line 2383
    .restart local v13    # "positionToMakeVisible":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    .line 2385
    .restart local v15    # "viewToMakeVisible":Landroid/view/View;
    move v7, v14

    .line 2386
    .local v7, "goalStart":I
    if-lez v13, :cond_c

    .line 2387
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getArrowScrollPreviewLength()I

    move-result v18

    add-int v7, v7, v18

    .line 2390
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    invoke-virtual {v15}, Landroid/view/View;->getTop()I

    move-result v17

    .line 2392
    .restart local v17    # "viewToMakeVisibleStart":I
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_f

    invoke-virtual {v15}, Landroid/view/View;->getBottom()I

    move-result v16

    .line 2395
    .restart local v16    # "viewToMakeVisibleEnd":I
    :goto_7
    move/from16 v0, v17

    if-lt v0, v7, :cond_10

    .line 2397
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 2375
    .end local v7    # "goalStart":I
    .end local v8    # "indexToMakeVisible":I
    .end local v13    # "positionToMakeVisible":I
    .end local v14    # "start":I
    .end local v15    # "viewToMakeVisible":Landroid/view/View;
    .end local v16    # "viewToMakeVisibleEnd":I
    .end local v17    # "viewToMakeVisibleStart":I
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v14

    goto :goto_5

    .line 2390
    .restart local v7    # "goalStart":I
    .restart local v8    # "indexToMakeVisible":I
    .restart local v13    # "positionToMakeVisible":I
    .restart local v14    # "start":I
    .restart local v15    # "viewToMakeVisible":Landroid/view/View;
    :cond_e
    invoke-virtual {v15}, Landroid/view/View;->getLeft()I

    move-result v17

    goto :goto_6

    .line 2392
    .restart local v17    # "viewToMakeVisibleStart":I
    :cond_f
    invoke-virtual {v15}, Landroid/view/View;->getRight()I

    move-result v16

    goto :goto_7

    .line 2400
    .restart local v16    # "viewToMakeVisibleEnd":I
    :cond_10
    const/16 v18, -0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-eq v0, v1, :cond_11

    sub-int v18, v16, v7

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getMaxScrollAmount()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_11

    .line 2403
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 2406
    :cond_11
    sub-int v2, v7, v17

    .line 2408
    .restart local v2    # "amountToScroll":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v18, v0

    if-nez v18, :cond_12

    .line 2409
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2410
    .local v4, "firstChild":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v18, v0

    if-eqz v18, :cond_13

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v5

    .line 2413
    .local v5, "firstChildStart":I
    :goto_8
    sub-int v11, v14, v5

    .line 2414
    .restart local v11    # "max":I
    invoke-static {v2, v11}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 2417
    .end local v4    # "firstChild":Landroid/view/View;
    .end local v5    # "firstChildStart":I
    .end local v11    # "max":I
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getMaxScrollAmount()I

    move-result v18

    move/from16 v0, v18

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v18

    goto/16 :goto_3

    .line 2410
    .restart local v4    # "firstChild":Landroid/view/View;
    :cond_13
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v5

    goto :goto_8
.end method

.method private amountToScrollToNewFocus(ILandroid/view/View;I)I
    .locals 7
    .param p1, "direction"    # I
    .param p2, "newFocus"    # Landroid/view/View;
    .param p3, "positionOfNewFocus"    # I

    .prologue
    .line 2434
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 2436
    const/4 v0, 0x0

    .line 2438
    .local v0, "amountToScroll":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v5}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2439
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2441
    const/16 v5, 0x21

    if-eq p1, v5, :cond_0

    const/16 v5, 0x11

    if-ne p1, v5, :cond_4

    .line 2442
    :cond_0
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v4

    .line 2443
    .local v4, "start":I
    :goto_0
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->top:I

    .line 2445
    .local v3, "newFocusStart":I
    :goto_1
    if-ge v3, v4, :cond_1

    .line 2446
    sub-int v0, v4, v3

    .line 2447
    if-lez p3, :cond_1

    .line 2448
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getArrowScrollPreviewLength()I

    move-result v5

    add-int/2addr v0, v5

    .line 2464
    .end local v3    # "newFocusStart":I
    .end local v4    # "start":I
    :cond_1
    :goto_2
    return v0

    .line 2442
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v4

    goto :goto_0

    .line 2443
    .restart local v4    # "start":I
    :cond_3
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 2452
    .end local v4    # "start":I
    :cond_4
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v6

    sub-int v1, v5, v6

    .line 2454
    .local v1, "end":I
    :goto_3
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    .line 2456
    .local v2, "newFocusEnd":I
    :goto_4
    if-le v2, v1, :cond_1

    .line 2457
    sub-int v0, v2, v1

    .line 2458
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v5, v5, -0x1

    if-ge p3, v5, :cond_1

    .line 2459
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getArrowScrollPreviewLength()I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_2

    .line 2452
    .end local v1    # "end":I
    .end local v2    # "newFocusEnd":I
    :cond_5
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v6

    sub-int v1, v5, v6

    goto :goto_3

    .line 2454
    .restart local v1    # "end":I
    :cond_6
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v5, Landroid/graphics/Rect;->right:I

    goto :goto_4
.end method

.method private arrowScroll(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v2, 0x0

    .line 1970
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 1973
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    .line 1975
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->arrowScrollImpl(I)Z

    move-result v0

    .line 1976
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 1977
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->playSoundEffect(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1982
    :cond_0
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    return v0

    .end local v0    # "handled":Z
    :catchall_0
    move-exception v1

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    throw v1
.end method

.method private arrowScrollFocused(I)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;
    .locals 23
    .param p1, "direction"    # I

    .prologue
    .line 2085
    invoke-direct/range {p0 .. p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 2087
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedView()Landroid/view/View;

    move-result-object v17

    .line 2091
    .local v17, "selectedView":Landroid/view/View;
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->hasFocus()Z

    move-result v21

    if-eqz v21, :cond_4

    .line 2092
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v11

    .line 2093
    .local v11, "oldFocus":Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-virtual {v0, v1, v11, v2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    .line 2127
    .end local v11    # "oldFocus":Landroid/view/View;
    .local v10, "newFocus":Landroid/view/View;
    :goto_0
    if-eqz v10, :cond_13

    .line 2128
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionOfNewFocus(Landroid/view/View;)I

    move-result v12

    .line 2132
    .local v12, "positionOfNewFocus":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-eq v12, v0, :cond_11

    .line 2133
    invoke-direct/range {p0 .. p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePositionOnScreen(I)I

    move-result v14

    .line 2135
    .local v14, "selectablePosition":I
    const/16 v21, 0x82

    move/from16 v0, p1

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    const/16 v21, 0x42

    move/from16 v0, p1

    move/from16 v1, v21

    if-ne v0, v1, :cond_f

    :cond_0
    const/4 v9, 0x1

    .line 2137
    .local v9, "movingForward":Z
    :goto_1
    const/16 v21, 0x21

    move/from16 v0, p1

    move/from16 v1, v21

    if-eq v0, v1, :cond_1

    const/16 v21, 0x11

    move/from16 v0, p1

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    :cond_1
    const/4 v8, 0x1

    .line 2140
    .local v8, "movingBackward":Z
    :goto_2
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v14, v0, :cond_11

    if-eqz v9, :cond_2

    if-lt v14, v12, :cond_3

    :cond_2
    if-eqz v8, :cond_11

    if-le v14, v12, :cond_11

    .line 2143
    :cond_3
    const/16 v21, 0x0

    .line 2166
    .end local v8    # "movingBackward":Z
    .end local v9    # "movingForward":Z
    .end local v12    # "positionOfNewFocus":I
    .end local v14    # "selectablePosition":I
    :goto_3
    return-object v21

    .line 2095
    .end local v10    # "newFocus":Landroid/view/View;
    :cond_4
    const/16 v21, 0x82

    move/from16 v0, p1

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    const/16 v21, 0x42

    move/from16 v0, p1

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 2096
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v18

    .line 2099
    .local v18, "start":I
    :goto_4
    if-eqz v17, :cond_8

    .line 2100
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_7

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getTop()I

    move-result v16

    .line 2105
    .local v16, "selectedStart":I
    :goto_5
    move/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 2120
    .end local v16    # "selectedStart":I
    .end local v18    # "start":I
    .local v13, "searchPoint":I
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_d

    const/16 v19, 0x0

    .line 2121
    .local v19, "x":I
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_e

    move/from16 v20, v13

    .line 2122
    .local v20, "y":I
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2124
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    move/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v10

    .restart local v10    # "newFocus":Landroid/view/View;
    goto/16 :goto_0

    .line 2096
    .end local v10    # "newFocus":Landroid/view/View;
    .end local v13    # "searchPoint":I
    .end local v19    # "x":I
    .end local v20    # "y":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v18

    goto :goto_4

    .line 2100
    .restart local v18    # "start":I
    :cond_7
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLeft()I

    move-result v16

    goto :goto_5

    .line 2102
    :cond_8
    move/from16 v16, v18

    .restart local v16    # "selectedStart":I
    goto :goto_5

    .line 2107
    .end local v16    # "selectedStart":I
    .end local v18    # "start":I
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v22

    sub-int v5, v21, v22

    .line 2111
    .local v5, "end":I
    :goto_9
    if-eqz v17, :cond_c

    .line 2112
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v21, v0

    if-eqz v21, :cond_b

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getBottom()I

    move-result v15

    .line 2117
    .local v15, "selectedEnd":I
    :goto_a
    invoke-static {v15, v5}, Ljava/lang/Math;->min(II)I

    move-result v13

    .restart local v13    # "searchPoint":I
    goto :goto_6

    .line 2107
    .end local v5    # "end":I
    .end local v13    # "searchPoint":I
    .end local v15    # "selectedEnd":I
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v22

    sub-int v5, v21, v22

    goto :goto_9

    .line 2112
    .restart local v5    # "end":I
    :cond_b
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getRight()I

    move-result v15

    goto :goto_a

    .line 2114
    :cond_c
    move v15, v5

    .restart local v15    # "selectedEnd":I
    goto :goto_a

    .end local v5    # "end":I
    .end local v15    # "selectedEnd":I
    .restart local v13    # "searchPoint":I
    :cond_d
    move/from16 v19, v13

    .line 2120
    goto :goto_7

    .line 2121
    .restart local v19    # "x":I
    :cond_e
    const/16 v20, 0x0

    goto :goto_8

    .line 2135
    .end local v13    # "searchPoint":I
    .end local v19    # "x":I
    .restart local v10    # "newFocus":Landroid/view/View;
    .restart local v12    # "positionOfNewFocus":I
    .restart local v14    # "selectablePosition":I
    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 2137
    .restart local v9    # "movingForward":Z
    :cond_10
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 2147
    .end local v9    # "movingForward":Z
    .end local v14    # "selectablePosition":I
    :cond_11
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v10, v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->amountToScrollToNewFocus(ILandroid/view/View;I)I

    move-result v6

    .line 2149
    .local v6, "focusScroll":I
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getMaxScrollAmount()I

    move-result v7

    .line 2150
    .local v7, "maxScrollAmount":I
    if-ge v6, v7, :cond_12

    .line 2152
    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 2153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mArrowScrollFocusResult:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->populate(II)V

    .line 2154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mArrowScrollFocusResult:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    goto/16 :goto_3

    .line 2155
    :cond_12
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->distanceToView(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    if-ge v0, v7, :cond_13

    .line 2160
    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 2161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mArrowScrollFocusResult:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->populate(II)V

    .line 2162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mArrowScrollFocusResult:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    move-object/from16 v21, v0

    goto/16 :goto_3

    .line 2166
    .end local v6    # "focusScroll":I
    .end local v7    # "maxScrollAmount":I
    .end local v12    # "positionOfNewFocus":I
    :cond_13
    const/16 v21, 0x0

    goto/16 :goto_3
.end method

.method private arrowScrollImpl(I)Z
    .locals 11
    .param p1, "direction"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 2225
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 2227
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v7

    if-gtz v7, :cond_1

    .line 2308
    :cond_0
    :goto_0
    return v9

    .line 2231
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 2232
    .local v6, "selectedView":Landroid/view/View;
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 2234
    .local v5, "selectedPos":I
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePositionOnScreen(I)I

    move-result v4

    .line 2235
    .local v4, "nextSelectedPosition":I
    invoke-direct {p0, p1, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->amountToScroll(II)I

    move-result v0

    .line 2238
    .local v0, "amountToScroll":I
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    if-eqz v7, :cond_c

    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->arrowScrollFocused(I)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;

    move-result-object v1

    .line 2239
    .local v1, "focusResult":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;
    :goto_1
    if-eqz v1, :cond_2

    .line 2240
    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->getSelectedPosition()I

    move-result v4

    .line 2241
    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;->getAmountToScroll()I

    move-result v0

    .line 2244
    :cond_2
    if-eqz v1, :cond_d

    move v3, v8

    .line 2245
    .local v3, "needToRedraw":Z
    :goto_2
    if-eq v4, v10, :cond_4

    .line 2246
    if-eqz v1, :cond_e

    move v7, v8

    :goto_3
    invoke-direct {p0, v6, p1, v4, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleNewSelectionChange(Landroid/view/View;IIZ)V

    .line 2248
    invoke-direct {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 2249
    invoke-direct {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 2251
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 2252
    move v5, v4

    .line 2254
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    if-eqz v7, :cond_3

    if-nez v1, :cond_3

    .line 2257
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 2258
    .local v2, "focused":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 2259
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 2263
    .end local v2    # "focused":Landroid/view/View;
    :cond_3
    const/4 v3, 0x1

    .line 2264
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkSelectionChanged()V

    .line 2267
    :cond_4
    if-lez v0, :cond_6

    .line 2268
    const/16 v7, 0x21

    if-eq p1, v7, :cond_5

    const/16 v7, 0x11

    if-ne p1, v7, :cond_f

    .end local v0    # "amountToScroll":I
    :cond_5
    :goto_4
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    .line 2270
    const/4 v3, 0x1

    .line 2275
    :cond_6
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    if-eqz v7, :cond_8

    if-nez v1, :cond_8

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Landroid/view/View;->hasFocus()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2277
    invoke-virtual {v6}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 2278
    .restart local v2    # "focused":Landroid/view/View;
    invoke-direct {p0, v2, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->distanceToView(Landroid/view/View;)I

    move-result v7

    if-lez v7, :cond_8

    .line 2279
    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 2284
    .end local v2    # "focused":Landroid/view/View;
    :cond_8
    if-ne v4, v10, :cond_9

    if-eqz v6, :cond_9

    invoke-direct {p0, v6, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2286
    const/4 v6, 0x0

    .line 2287
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hideSelector()V

    .line 2291
    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 2294
    :cond_9
    if-eqz v3, :cond_0

    .line 2295
    if-eqz v6, :cond_a

    .line 2296
    invoke-direct {p0, v5, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    .line 2297
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v7

    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 2300
    :cond_a
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    move-result v7

    if-nez v7, :cond_b

    .line 2301
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    .line 2304
    :cond_b
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    move v9, v8

    .line 2305
    goto/16 :goto_0

    .line 2238
    .end local v1    # "focusResult":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;
    .end local v3    # "needToRedraw":Z
    .restart local v0    # "amountToScroll":I
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1

    .restart local v1    # "focusResult":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ArrowScrollFocusResult;
    :cond_d
    move v3, v9

    .line 2244
    goto/16 :goto_2

    .restart local v3    # "needToRedraw":Z
    :cond_e
    move v7, v9

    .line 2246
    goto/16 :goto_3

    .line 2268
    :cond_f
    neg-int v0, v0

    goto :goto_4
.end method

.method private awakenScrollbarsInternal()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .prologue
    .line 3208
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 3209
    invoke-super {p0}, Landroid/widget/AdapterView;->awakenScrollBars()Z

    move-result v0

    .line 3211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cancelCheckForLongPress()V
    .locals 1

    .prologue
    .line 3054
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    if-nez v0, :cond_0

    .line 3059
    :goto_0
    return-void

    .line 3058
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private cancelCheckForTap()V
    .locals 1

    .prologue
    .line 3035
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForTap:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

    if-nez v0, :cond_0

    .line 3040
    :goto_0
    return-void

    .line 3039
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForTap:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private checkFocus()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 6232
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6233
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    move v1, v3

    .line 6238
    .local v1, "focusable":Z
    :goto_0
    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDesiredFocusableInTouchModeState:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    invoke-super {p0, v2}, Landroid/widget/AdapterView;->setFocusableInTouchMode(Z)V

    .line 6239
    if-eqz v1, :cond_3

    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDesiredFocusableState:Z

    if-eqz v2, :cond_3

    :goto_2
    invoke-super {p0, v3}, Landroid/widget/AdapterView;->setFocusable(Z)V

    .line 6241
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 6242
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateEmptyStatus()V

    .line 6244
    :cond_0
    return-void

    .end local v1    # "focusable":Z
    :cond_1
    move v1, v4

    .line 6233
    goto :goto_0

    .restart local v1    # "focusable":Z
    :cond_2
    move v2, v4

    .line 6238
    goto :goto_1

    :cond_3
    move v3, v4

    .line 6239
    goto :goto_2
.end method

.method private checkSelectionChanged()V
    .locals 4

    .prologue
    .line 3482
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedPosition:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedRowId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 3483
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->selectionChanged()V

    .line 3484
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedPosition:I

    .line 3485
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedRowId:J

    .line 3487
    :cond_1
    return-void
.end method

.method private cloneCheckStates()Landroid/util/SparseBooleanArray;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 5352
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-nez v2, :cond_1

    .line 5353
    const/4 v0, 0x0

    .line 5368
    :cond_0
    :goto_0
    return-object v0

    .line 5358
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    .line 5359
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->clone()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .local v0, "checkedStates":Landroid/util/SparseBooleanArray;
    goto :goto_0

    .line 5361
    .end local v0    # "checkedStates":Landroid/util/SparseBooleanArray;
    :cond_2
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 5363
    .restart local v0    # "checkedStates":Landroid/util/SparseBooleanArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 5364
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 5363
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private contentFits()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2998
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v0

    .line 2999
    .local v0, "childCount":I
    if-nez v0, :cond_1

    .line 3014
    :cond_0
    :goto_0
    return v3

    .line 3003
    :cond_1
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-eq v0, v5, :cond_2

    move v3, v4

    .line 3004
    goto :goto_0

    .line 3007
    :cond_2
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3008
    .local v1, "first":Landroid/view/View;
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 3010
    .local v2, "last":Landroid/view/View;
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_4

    .line 3011
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v6

    if-lt v5, v6, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_0

    :cond_3
    move v3, v4

    goto :goto_0

    .line 3014
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v6

    if-lt v5, v6, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_0

    :cond_5
    move v3, v4

    goto :goto_0
.end method

.method private correctTooHigh(I)V
    .locals 10
    .param p1, "childCount"    # I

    .prologue
    .line 5217
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v8, p1

    add-int/lit8 v6, v8, -0x1

    .line 5218
    .local v6, "lastPosition":I
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v6, v8, :cond_0

    if-nez p1, :cond_1

    .line 5267
    :cond_0
    :goto_0
    return-void

    .line 5223
    :cond_1
    add-int/lit8 v8, p1, -0x1

    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 5227
    .local v4, "lastChild":Landroid/view/View;
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_4

    .line 5228
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v5

    .line 5234
    .local v5, "lastEnd":I
    :goto_1
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v7

    .line 5235
    .local v7, "start":I
    :goto_2
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_6

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v9

    sub-int v0, v8, v9

    .line 5240
    .local v0, "end":I
    :goto_3
    sub-int v1, v0, v5

    .line 5242
    .local v1, "endOffset":I
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 5243
    .local v2, "firstChild":Landroid/view/View;
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_7

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 5247
    .local v3, "firstStart":I
    :goto_4
    if-lez v1, :cond_0

    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-gtz v8, :cond_2

    if-ge v3, v7, :cond_0

    .line 5248
    :cond_2
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-nez v8, :cond_3

    .line 5250
    sub-int v8, v7, v3

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5254
    :cond_3
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetChildren(I)V

    .line 5256
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-lez v8, :cond_0

    .line 5257
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_8

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 5261
    :goto_5
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/lit8 v8, v8, -0x1

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    sub-int v9, v3, v9

    invoke-direct {p0, v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBefore(II)Landroid/view/View;

    .line 5264
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    goto :goto_0

    .line 5230
    .end local v0    # "end":I
    .end local v1    # "endOffset":I
    .end local v2    # "firstChild":Landroid/view/View;
    .end local v3    # "firstStart":I
    .end local v5    # "lastEnd":I
    .end local v7    # "start":I
    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v5

    .restart local v5    # "lastEnd":I
    goto :goto_1

    .line 5234
    :cond_5
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v7

    goto :goto_2

    .line 5235
    .restart local v7    # "start":I
    :cond_6
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v9

    sub-int v0, v8, v9

    goto :goto_3

    .line 5243
    .restart local v0    # "end":I
    .restart local v1    # "endOffset":I
    .restart local v2    # "firstChild":Landroid/view/View;
    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_4

    .line 5257
    .restart local v3    # "firstStart":I
    :cond_8
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_5
.end method

.method private correctTooLow(I)V
    .locals 10
    .param p1, "childCount"    # I

    .prologue
    .line 5272
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-nez v8, :cond_0

    if-nez p1, :cond_1

    .line 5324
    :cond_0
    :goto_0
    return-void

    .line 5276
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5277
    .local v1, "first":Landroid/view/View;
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 5279
    .local v2, "firstStart":I
    :goto_1
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v6

    .line 5282
    .local v6, "start":I
    :goto_2
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_6

    .line 5283
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v9

    sub-int v0, v8, v9

    .line 5290
    .local v0, "end":I
    :goto_3
    sub-int v7, v2, v6

    .line 5292
    .local v7, "startOffset":I
    add-int/lit8 v8, p1, -0x1

    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5293
    .local v3, "last":Landroid/view/View;
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 5295
    .local v4, "lastEnd":I
    :goto_4
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v8, p1

    add-int/lit8 v5, v8, -0x1

    .line 5300
    .local v5, "lastPosition":I
    if-lez v7, :cond_0

    .line 5301
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-lt v5, v8, :cond_2

    if-le v4, v0, :cond_9

    .line 5302
    :cond_2
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v5, v8, :cond_3

    .line 5304
    sub-int v8, v4, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 5308
    :cond_3
    neg-int v8, v7

    invoke-direct {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetChildren(I)V

    .line 5310
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ge v5, v8, :cond_0

    .line 5311
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_8

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 5315
    :goto_5
    add-int/lit8 v8, v5, 0x1

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    add-int/2addr v9, v4

    invoke-direct {p0, v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillAfter(II)Landroid/view/View;

    .line 5318
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    goto :goto_0

    .line 5277
    .end local v0    # "end":I
    .end local v2    # "firstStart":I
    .end local v3    # "last":Landroid/view/View;
    .end local v4    # "lastEnd":I
    .end local v5    # "lastPosition":I
    .end local v6    # "start":I
    .end local v7    # "startOffset":I
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    goto :goto_1

    .line 5279
    .restart local v2    # "firstStart":I
    :cond_5
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v6

    goto :goto_2

    .line 5285
    .restart local v6    # "start":I
    :cond_6
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v9

    sub-int v0, v8, v9

    .restart local v0    # "end":I
    goto :goto_3

    .line 5293
    .restart local v3    # "last":Landroid/view/View;
    .restart local v7    # "startOffset":I
    :cond_7
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    goto :goto_4

    .line 5311
    .restart local v4    # "lastEnd":I
    .restart local v5    # "lastPosition":I
    :cond_8
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    goto :goto_5

    .line 5320
    :cond_9
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v5, v8, :cond_0

    .line 5321
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    goto/16 :goto_0
.end method

.method private createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 5570
    new-instance v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/widget/AdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method private distanceToView(Landroid/view/View;)I
    .locals 7
    .param p1, "descendant"    # Landroid/view/View;

    .prologue
    .line 2475
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v5}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2476
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2478
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v2

    .line 2479
    .local v2, "start":I
    :goto_0
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v6

    sub-int v1, v5, v6

    .line 2482
    .local v1, "end":I
    :goto_1
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v4, v5, Landroid/graphics/Rect;->top:I

    .line 2483
    .local v4, "viewStart":I
    :goto_2
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->bottom:I

    .line 2485
    .local v3, "viewEnd":I
    :goto_3
    const/4 v0, 0x0

    .line 2486
    .local v0, "distance":I
    if-ge v3, v2, :cond_5

    .line 2487
    sub-int v0, v2, v3

    .line 2492
    :cond_0
    :goto_4
    return v0

    .line 2478
    .end local v0    # "distance":I
    .end local v1    # "end":I
    .end local v2    # "start":I
    .end local v3    # "viewEnd":I
    .end local v4    # "viewStart":I
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v2

    goto :goto_0

    .line 2479
    .restart local v2    # "start":I
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v6

    sub-int v1, v5, v6

    goto :goto_1

    .line 2482
    .restart local v1    # "end":I
    :cond_3
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v4, v5, Landroid/graphics/Rect;->left:I

    goto :goto_2

    .line 2483
    .restart local v4    # "viewStart":I
    :cond_4
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v5, Landroid/graphics/Rect;->right:I

    goto :goto_3

    .line 2488
    .restart local v0    # "distance":I
    .restart local v3    # "viewEnd":I
    :cond_5
    if-le v4, v1, :cond_0

    .line 2489
    sub-int v0, v4, v1

    goto :goto_4
.end method

.method private drawEndEdge(Landroid/graphics/Canvas;)Z
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x0

    .line 3289
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3290
    const/4 v1, 0x0

    .line 3320
    :goto_0
    return v1

    .line 3293
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 3306
    .local v2, "restoreCount":I
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v4, :cond_1

    .line 3307
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v3

    .line 3308
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v0

    .line 3309
    .local v0, "height":I
    neg-int v4, v3

    int-to-float v4, v4

    int-to-float v5, v0

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3310
    const/high16 v4, 0x43340000    # 180.0f

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 3318
    .end local v0    # "height":I
    :goto_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    .line 3319
    .local v1, "needsInvalidate":Z
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    .line 3313
    .end local v1    # "needsInvalidate":Z
    .end local v3    # "width":I
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v3

    .line 3314
    .restart local v3    # "width":I
    int-to-float v4, v3

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3315
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    goto :goto_1
.end method

.method private drawSelector(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3324
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3325
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 3326
    .local v0, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3327
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3329
    .end local v0    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method private drawStartEdge(Landroid/graphics/Canvas;)Z
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3269
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3270
    const/4 v1, 0x0

    .line 3285
    :goto_0
    return v1

    .line 3273
    :cond_0
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v3, :cond_1

    .line 3274
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    goto :goto_0

    .line 3277
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 3278
    .local v2, "restoreCount":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v4

    sub-int v0, v3, v4

    .line 3280
    .local v0, "height":I
    const/4 v3, 0x0

    int-to-float v4, v0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3281
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3283
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    .line 3284
    .local v1, "needsInvalidate":Z
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private fillAfter(II)Landroid/view/View;
    .locals 7
    .param p1, "pos"    # I
    .param p2, "nextOffset"    # I

    .prologue
    const/4 v4, 0x1

    .line 5042
    const/4 v3, 0x0

    .line 5044
    .local v3, "selectedView":Landroid/view/View;
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v1

    .line 5046
    .local v1, "end":I
    :goto_0
    if-ge p2, v1, :cond_4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-ge p1, v5, :cond_4

    .line 5047
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ne p1, v5, :cond_2

    move v2, v4

    .line 5049
    .local v2, "selected":Z
    :goto_1
    invoke-direct {p0, p1, p2, v4, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 5051
    .local v0, "child":Landroid/view/View;
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_3

    .line 5052
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    add-int p2, v5, v6

    .line 5057
    :goto_2
    if-eqz v2, :cond_0

    .line 5058
    move-object v3, v0

    .line 5061
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 5062
    goto :goto_0

    .line 5044
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "end":I
    .end local v2    # "selected":Z
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v1

    goto :goto_0

    .line 5047
    .restart local v1    # "end":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 5054
    .restart local v0    # "child":Landroid/view/View;
    .restart local v2    # "selected":Z
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    add-int p2, v5, v6

    goto :goto_2

    .line 5064
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "selected":Z
    :cond_4
    return-object v3
.end method

.method private fillBefore(II)Landroid/view/View;
    .locals 7
    .param p1, "pos"    # I
    .param p2, "nextOffset"    # I

    .prologue
    const/4 v4, 0x0

    .line 5015
    const/4 v2, 0x0

    .line 5017
    .local v2, "selectedView":Landroid/view/View;
    const/4 v3, 0x0

    .line 5019
    .local v3, "start":I
    :goto_0
    if-lez p2, :cond_3

    if-ltz p1, :cond_3

    .line 5020
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ne p1, v5, :cond_1

    const/4 v1, 0x1

    .line 5021
    .local v1, "isSelected":Z
    :goto_1
    invoke-direct {p0, p1, p2, v4, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 5023
    .local v0, "child":Landroid/view/View;
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_2

    .line 5024
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    sub-int p2, v5, v6

    .line 5029
    :goto_2
    if-eqz v1, :cond_0

    .line 5030
    move-object v2, v0

    .line 5033
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 5034
    goto :goto_0

    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "isSelected":Z
    :cond_1
    move v1, v4

    .line 5020
    goto :goto_1

    .line 5026
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "isSelected":Z
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    sub-int p2, v5, v6

    goto :goto_2

    .line 5036
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "isSelected":Z
    :cond_3
    add-int/lit8 v4, p1, 0x1

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5038
    return-object v2
.end method

.method private fillBeforeAndAfter(Landroid/view/View;I)V
    .locals 4
    .param p1, "selected"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 5146
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    .line 5149
    .local v0, "itemMargin":I
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v3, :cond_0

    .line 5150
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v2, v3, v0

    .line 5155
    .local v2, "offsetBefore":I
    :goto_0
    add-int/lit8 v3, p2, -0x1

    invoke-direct {p0, v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBefore(II)Landroid/view/View;

    .line 5157
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    .line 5160
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v3, :cond_1

    .line 5161
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v3

    add-int v1, v3, v0

    .line 5166
    .local v1, "offsetAfter":I
    :goto_1
    add-int/lit8 v3, p2, 0x1

    invoke-direct {p0, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillAfter(II)Landroid/view/View;

    .line 5167
    return-void

    .line 5152
    .end local v1    # "offsetAfter":I
    .end local v2    # "offsetBefore":I
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v2, v3, v0

    .restart local v2    # "offsetBefore":I
    goto :goto_0

    .line 5163
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    add-int v1, v3, v0

    .restart local v1    # "offsetAfter":I
    goto :goto_1
.end method

.method private fillFromMiddle(II)Landroid/view/View;
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v5, 0x1

    .line 5121
    sub-int v4, p2, p1

    .line 5122
    .local v4, "size":I
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reconcileSelectedPosition()I

    move-result v0

    .line 5124
    .local v0, "position":I
    invoke-direct {p0, v0, p1, v5, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 5125
    .local v1, "selected":Landroid/view/View;
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5127
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_1

    .line 5128
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 5129
    .local v2, "selectedHeight":I
    if-gt v2, v4, :cond_0

    .line 5130
    sub-int v5, v4, v2

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v1, v5}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 5139
    .end local v2    # "selectedHeight":I
    :cond_0
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    .line 5140
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->correctTooHigh(I)V

    .line 5142
    return-object v1

    .line 5133
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 5134
    .local v3, "selectedWidth":I
    if-gt v3, v4, :cond_0

    .line 5135
    sub-int v5, v4, v3

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v1, v5}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_0
.end method

.method private fillFromOffset(I)Landroid/view/View;
    .locals 2
    .param p1, "nextOffset"    # I

    .prologue
    .line 5110
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5111
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5113
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-gez v0, :cond_0

    .line 5114
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5117
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    invoke-direct {p0, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillAfter(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private fillFromSelection(III)Landroid/view/View;
    .locals 8
    .param p1, "selectedTop"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v7, 0x1

    .line 5170
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 5173
    .local v3, "selectedPosition":I
    invoke-direct {p0, v3, p1, v7, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 5175
    .local v1, "selected":Landroid/view/View;
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    .line 5176
    .local v4, "selectedStart":I
    :goto_0
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v7, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 5179
    .local v2, "selectedEnd":I
    :goto_1
    if-le v2, p3, :cond_3

    .line 5182
    sub-int v5, v4, p2

    .line 5186
    .local v5, "spaceAbove":I
    sub-int v6, v2, p3

    .line 5188
    .local v6, "spaceBelow":I
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 5191
    .local v0, "offset":I
    neg-int v7, v0

    invoke-virtual {v1, v7}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 5208
    .end local v0    # "offset":I
    .end local v5    # "spaceAbove":I
    .end local v6    # "spaceBelow":I
    :cond_0
    :goto_2
    invoke-direct {p0, v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    .line 5209
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->correctTooHigh(I)V

    .line 5211
    return-object v1

    .line 5175
    .end local v2    # "selectedEnd":I
    .end local v4    # "selectedStart":I
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_0

    .line 5176
    .restart local v4    # "selectedStart":I
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    goto :goto_1

    .line 5192
    .restart local v2    # "selectedEnd":I
    :cond_3
    if-ge v4, p2, :cond_0

    .line 5195
    sub-int v5, p2, v4

    .line 5199
    .restart local v5    # "spaceAbove":I
    sub-int v6, p3, v2

    .line 5201
    .restart local v6    # "spaceBelow":I
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 5204
    .restart local v0    # "offset":I
    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_2
.end method

.method private fillSpecific(II)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    const/4 v8, 0x1

    .line 5068
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ne p1, v9, :cond_1

    move v7, v8

    .line 5069
    .local v7, "tempIsSelected":Z
    :goto_0
    invoke-direct {p0, p1, p2, v8, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v6

    .line 5072
    .local v6, "temp":Landroid/view/View;
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5074
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    .line 5077
    .local v3, "itemMargin":I
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_2

    .line 5078
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int v5, v8, v3

    .line 5082
    .local v5, "offsetBefore":I
    :goto_1
    add-int/lit8 v8, p1, -0x1

    invoke-direct {p0, v8, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBefore(II)Landroid/view/View;

    move-result-object v1

    .line 5085
    .local v1, "before":Landroid/view/View;
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    .line 5088
    iget-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_3

    .line 5089
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v8

    add-int v4, v8, v3

    .line 5093
    .local v4, "offsetAfter":I
    :goto_2
    add-int/lit8 v8, p1, 0x1

    invoke-direct {p0, v8, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillAfter(II)Landroid/view/View;

    move-result-object v0

    .line 5095
    .local v0, "after":Landroid/view/View;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v2

    .line 5096
    .local v2, "childCount":I
    if-lez v2, :cond_0

    .line 5097
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->correctTooHigh(I)V

    .line 5100
    :cond_0
    if-eqz v7, :cond_4

    .line 5105
    .end local v6    # "temp":Landroid/view/View;
    :goto_3
    return-object v6

    .line 5068
    .end local v0    # "after":Landroid/view/View;
    .end local v1    # "before":Landroid/view/View;
    .end local v2    # "childCount":I
    .end local v3    # "itemMargin":I
    .end local v4    # "offsetAfter":I
    .end local v5    # "offsetBefore":I
    .end local v7    # "tempIsSelected":Z
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 5080
    .restart local v3    # "itemMargin":I
    .restart local v6    # "temp":Landroid/view/View;
    .restart local v7    # "tempIsSelected":Z
    :cond_2
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int v5, v8, v3

    .restart local v5    # "offsetBefore":I
    goto :goto_1

    .line 5091
    .restart local v1    # "before":Landroid/view/View;
    :cond_3
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v8

    add-int v4, v8, v3

    .restart local v4    # "offsetAfter":I
    goto :goto_2

    .line 5102
    .restart local v0    # "after":Landroid/view/View;
    .restart local v2    # "childCount":I
    :cond_4
    if-eqz v1, :cond_5

    move-object v6, v1

    .line 5103
    goto :goto_3

    :cond_5
    move-object v6, v0

    .line 5105
    goto :goto_3
.end method

.method private findAccessibilityFocusedChild(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .param p1, "focusedView"    # Landroid/view/View;

    .prologue
    .line 6806
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 6807
    .local v0, "viewParent":Landroid/view/ViewParent;
    :goto_0
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    if-eq v0, p0, :cond_0

    move-object p1, v0

    .line 6808
    check-cast p1, Landroid/view/View;

    .line 6809
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 6811
    :cond_0
    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 6812
    const/4 p1, 0x0

    .line 6814
    .end local p1    # "focusedView":Landroid/view/View;
    :cond_1
    return-object p1
.end method

.method private findClosestMotionRowOrColumn(I)I
    .locals 3
    .param p1, "motionPos"    # I

    .prologue
    const/4 v2, -0x1

    .line 2975
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v0

    .line 2976
    .local v0, "childCount":I
    if-nez v0, :cond_1

    move v1, v2

    .line 2984
    :cond_0
    :goto_0
    return v1

    .line 2980
    :cond_1
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findMotionRowOrColumn(I)I

    move-result v1

    .line 2981
    .local v1, "motionRow":I
    if-ne v1, v2, :cond_0

    .line 2984
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method private findMotionRowOrColumn(I)I
    .locals 5
    .param p1, "motionPos"    # I

    .prologue
    const/4 v3, -0x1

    .line 2957
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v0

    .line 2958
    .local v0, "childCount":I
    if-nez v0, :cond_1

    .line 2971
    :cond_0
    :goto_0
    return v3

    .line 2962
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 2963
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2965
    .local v2, "v":Landroid/view/View;
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v4

    if-le p1, v4, :cond_3

    :cond_2
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v4, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    if-gt p1, v4, :cond_4

    .line 2967
    :cond_3
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v3, v1

    goto :goto_0

    .line 2962
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private findSyncPosition()I
    .locals 20

    .prologue
    .line 5372
    move-object/from16 v0, p0

    iget v10, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 5374
    .local v10, "itemCount":I
    if-nez v10, :cond_1

    .line 5375
    const/4 v13, -0x1

    .line 5448
    :cond_0
    :goto_0
    return v13

    .line 5378
    :cond_1
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 5381
    .local v8, "idToMatch":J
    const-wide/high16 v16, -0x8000000000000000L

    cmp-long v16, v8, v16

    if-nez v16, :cond_2

    .line 5382
    const/4 v13, -0x1

    goto :goto_0

    .line 5386
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 5387
    .local v13, "seed":I
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 5388
    add-int/lit8 v16, v10, -0x1

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 5390
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x64

    add-long v4, v16, v18

    .line 5395
    .local v4, "endTime":J
    move v3, v13

    .line 5398
    .local v3, "first":I
    move v11, v13

    .line 5401
    .local v11, "last":I
    const/4 v12, 0x0

    .line 5411
    .local v12, "next":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 5412
    .local v2, "adapter":Landroid/widget/ListAdapter;
    if-nez v2, :cond_5

    .line 5413
    const/4 v13, -0x1

    goto :goto_0

    .line 5431
    .local v6, "hitFirst":Z
    .local v7, "hitLast":Z
    .local v14, "rowId":J
    :cond_3
    if-nez v6, :cond_4

    if-eqz v12, :cond_9

    if-nez v7, :cond_9

    .line 5433
    :cond_4
    add-int/lit8 v11, v11, 0x1

    .line 5434
    move v13, v11

    .line 5437
    const/4 v12, 0x0

    .line 5416
    .end local v6    # "hitFirst":Z
    .end local v7    # "hitLast":Z
    .end local v14    # "rowId":J
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    cmp-long v16, v16, v4

    if-gtz v16, :cond_6

    .line 5417
    invoke-interface {v2, v13}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v14

    .line 5418
    .restart local v14    # "rowId":J
    cmp-long v16, v14, v8

    if-eqz v16, :cond_0

    .line 5423
    add-int/lit8 v16, v10, -0x1

    move/from16 v0, v16

    if-ne v11, v0, :cond_7

    const/4 v7, 0x1

    .line 5424
    .restart local v7    # "hitLast":Z
    :goto_2
    if-nez v3, :cond_8

    const/4 v6, 0x1

    .line 5426
    .restart local v6    # "hitFirst":Z
    :goto_3
    if-eqz v7, :cond_3

    if-eqz v6, :cond_3

    .line 5448
    .end local v6    # "hitFirst":Z
    .end local v7    # "hitLast":Z
    .end local v14    # "rowId":J
    :cond_6
    const/4 v13, -0x1

    goto :goto_0

    .line 5423
    .restart local v14    # "rowId":J
    :cond_7
    const/4 v7, 0x0

    goto :goto_2

    .line 5424
    .restart local v7    # "hitLast":Z
    :cond_8
    const/4 v6, 0x0

    goto :goto_3

    .line 5438
    .restart local v6    # "hitFirst":Z
    :cond_9
    if-nez v7, :cond_a

    if-nez v12, :cond_5

    if-nez v6, :cond_5

    .line 5440
    :cond_a
    add-int/lit8 v3, v3, -0x1

    .line 5441
    move v13, v3

    .line 5444
    const/4 v12, 0x1

    goto :goto_1
.end method

.method private finishEdgeGlows()V
    .locals 1

    .prologue
    .line 3259
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v0, :cond_0

    .line 3260
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    .line 3263
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v0, :cond_1

    .line 3264
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->finish()V

    .line 3266
    :cond_1
    return-void
.end method

.method private fireOnSelected()V
    .locals 6

    .prologue
    .line 3512
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    .line 3513
    .local v0, "listener":Landroid/widget/AdapterView$OnItemSelectedListener;
    if-nez v0, :cond_0

    .line 3525
    :goto_0
    return-void

    .line 3517
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedItemPosition()I

    move-result v3

    .line 3518
    .local v3, "selection":I
    if-ltz v3, :cond_1

    .line 3519
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 3520
    .local v2, "v":Landroid/view/View;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    .line 3523
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-interface {v0, p0}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    goto :goto_0
.end method

.method private forceValidFocusDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1789
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x21

    if-eq p1, v0, :cond_0

    const/16 v0, 0x82

    if-eq p1, v0, :cond_0

    .line 1790
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Focus direction must be one of {View.FOCUS_UP, View.FOCUS_DOWN} for vertical orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1792
    :cond_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v0, :cond_1

    const/16 v0, 0x11

    if-eq p1, v0, :cond_1

    const/16 v0, 0x42

    if-eq p1, v0, :cond_1

    .line 1793
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Focus direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT} for vertical orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1796
    :cond_1
    return-void
.end method

.method private forceValidInnerFocusDirection(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1799
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    .line 1800
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT} for vertical orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802
    :cond_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v0, :cond_1

    const/16 v0, 0x21

    if-eq p1, v0, :cond_1

    const/16 v0, 0x82

    if-eq p1, v0, :cond_1

    .line 1803
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {View.FOCUS_UP, View.FOCUS_DOWN} for horizontal orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1806
    :cond_1
    return-void
.end method

.method private getAccessibilityFocusedHost()Landroid/view/View;
    .locals 9

    .prologue
    .line 6686
    const/4 v4, 0x0

    .line 6688
    .local v4, "hostView":Landroid/view/View;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getViewRootImpl"

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 6693
    .local v3, "getViewRootImpl":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6695
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedHost getViewRootImpl = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6697
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v3, p0, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 6700
    .local v5, "rootImpl":Ljava/lang/Object;
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedHost rootImp = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6702
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getAccessibilityFocusedHost"

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 6705
    .local v2, "getAccessibilityFocusedHost":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6706
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedHost getAccessibilityFocusedHost = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6710
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/view/View;

    move-object v4, v0

    .line 6712
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedHost hostView = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6716
    .end local v2    # "getAccessibilityFocusedHost":Ljava/lang/reflect/Method;
    .end local v3    # "getViewRootImpl":Ljava/lang/reflect/Method;
    .end local v5    # "rootImpl":Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 6713
    :catch_0
    move-exception v1

    .line 6714
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getAccessibilityFocusedVirtualView()Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 9

    .prologue
    .line 6720
    const/4 v4, 0x0

    .line 6722
    .local v4, "mAccessibilityFocusedVirtualView":Landroid/view/accessibility/AccessibilityNodeInfo;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getViewRootImpl"

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 6727
    .local v3, "getViewRootImpl":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6729
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedVirtualView getViewRootImpl = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6731
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v3, p0, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 6734
    .local v5, "rootImpl":Ljava/lang/Object;
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedVirtualView rootImp = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6737
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "getAccessibilityFocusedVirtualView"

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 6740
    .local v2, "getAccessibilityFocusedVirtualView":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6741
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedVirtualView  = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6743
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    move-object v4, v0

    .line 6745
    const-string v6, "FrameListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessibilityFocusedVirtualView mAccessibilityFocusedVirtualView = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6751
    .end local v2    # "getAccessibilityFocusedVirtualView":Ljava/lang/reflect/Method;
    .end local v3    # "getViewRootImpl":Ljava/lang/reflect/Method;
    .end local v5    # "rootImpl":Ljava/lang/Object;
    :goto_0
    return-object v4

    .line 6747
    :catch_0
    move-exception v1

    .line 6748
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getArrowScrollPreviewLength()I
    .locals 3

    .prologue
    .line 2190
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getVerticalFadingEdgeLength()I

    move-result v0

    .line 2193
    .local v0, "fadingEdgeLength":I
    :goto_0
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    const/16 v2, 0xa

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    return v1

    .line 2190
    .end local v0    # "fadingEdgeLength":I
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHorizontalFadingEdgeLength()I

    move-result v0

    goto :goto_0
.end method

.method private getChildHeightMeasureSpec(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)I
    .locals 5
    .param p1, "lp"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 4639
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->height:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    .line 4640
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 4645
    :goto_0
    return v1

    .line 4641
    :cond_0
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v1, :cond_1

    .line 4642
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v2

    sub-int v0, v1, v2

    .line 4643
    .local v0, "maxHeight":I
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0

    .line 4645
    .end local v0    # "maxHeight":I
    :cond_1
    iget v1, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->height:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0
.end method

.method private getChildWidthMeasureSpec(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)I
    .locals 5
    .param p1, "lp"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 4628
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v1, :cond_0

    iget v1, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->width:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    .line 4629
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 4634
    :goto_0
    return v1

    .line 4630
    :cond_0
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v1, :cond_1

    .line 4631
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v2

    sub-int v0, v1, v2

    .line 4632
    .local v0, "maxWidth":I
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0

    .line 4634
    .end local v0    # "maxWidth":I
    :cond_1
    iget v1, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->width:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_0
.end method

.method private final getCurrVelocity()F
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 3199
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 3200
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v0

    .line 3203
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .locals 8
    .param p0, "source"    # Landroid/graphics/Rect;
    .param p1, "dest"    # Landroid/graphics/Rect;
    .param p2, "direction"    # I

    .prologue
    .line 2907
    sparse-switch p2, :sswitch_data_0

    .line 2945
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2909
    :sswitch_0
    iget v4, p0, Landroid/graphics/Rect;->right:I

    .line 2910
    .local v4, "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2911
    .local v5, "sY":I
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 2912
    .local v0, "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2950
    .local v1, "dY":I
    :goto_0
    sub-int v2, v0, v4

    .line 2951
    .local v2, "deltaX":I
    sub-int v3, v1, v5

    .line 2953
    .local v3, "deltaY":I
    mul-int v6, v3, v3

    mul-int v7, v2, v2

    add-int/2addr v6, v7

    return v6

    .line 2916
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v2    # "deltaX":I
    .end local v3    # "deltaY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_1
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2917
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->bottom:I

    .line 2918
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2919
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 2920
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2923
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_2
    iget v4, p0, Landroid/graphics/Rect;->left:I

    .line 2924
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2925
    .restart local v5    # "sY":I
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 2926
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2927
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2930
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_3
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2931
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->top:I

    .line 2932
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2933
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 2934
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2938
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_4
    iget v6, p0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2939
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2940
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2941
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2942
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2907
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method private getScaledOverscrollDistance(Landroid/view/ViewConfiguration;)I
    .locals 2
    .param p1, "vc"    # Landroid/view/ViewConfiguration;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 2990
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 2991
    const/4 v0, 0x0

    .line 2994
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v0

    goto :goto_0
.end method

.method private handleDataChanged()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x5

    const/4 v5, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 4407
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4408
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->confirmCheckedPositionsById()V

    .line 4411
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->clearTransientStateViews()V

    .line 4413
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 4414
    .local v0, "itemCount":I
    if-lez v0, :cond_9

    .line 4419
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    if-eqz v3, :cond_1

    .line 4421
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 4422
    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    .line 4424
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncMode:I

    packed-switch v3, :pswitch_data_0

    .line 4473
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v3

    if-nez v3, :cond_8

    .line 4475
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedItemPosition()I

    move-result v1

    .line 4478
    .local v1, "newPos":I
    if-lt v1, v0, :cond_2

    .line 4479
    add-int/lit8 v1, v0, -0x1

    .line 4481
    :cond_2
    if-gez v1, :cond_3

    .line 4482
    const/4 v1, 0x0

    .line 4486
    :cond_3
    invoke-direct {p0, v1, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 4488
    .local v2, "selectablePos":I
    if-ltz v2, :cond_7

    .line 4489
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 4518
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :cond_4
    :goto_0
    return-void

    .line 4426
    :pswitch_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 4431
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 4432
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v4, v0, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    goto :goto_0

    .line 4438
    :cond_5
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findSyncPosition()I

    move-result v1

    .line 4439
    .restart local v1    # "newPos":I
    if-ltz v1, :cond_1

    .line 4441
    invoke-direct {p0, v1, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 4442
    .restart local v2    # "selectablePos":I
    if-ne v2, v1, :cond_1

    .line 4444
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 4446
    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncHeight:J

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v3

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    .line 4449
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 4457
    :goto_1
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 4453
    :cond_6
    const/4 v3, 0x2

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    goto :goto_1

    .line 4466
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :pswitch_1
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 4467
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v4, v0, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    goto :goto_0

    .line 4493
    .restart local v1    # "newPos":I
    .restart local v2    # "selectablePos":I
    :cond_7
    invoke-direct {p0, v1, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 4494
    if-ltz v2, :cond_9

    .line 4495
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 4501
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :cond_8
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    if-gez v3, :cond_4

    .line 4508
    :cond_9
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 4509
    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 4510
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    .line 4511
    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    .line 4512
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    .line 4513
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 4514
    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    .line 4515
    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    .line 4517
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkSelectionChanged()V

    goto :goto_0

    .line 4424
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleDragChange(I)V
    .locals 9
    .param p1, "delta"    # I

    .prologue
    const/4 v0, 0x1

    .line 2782
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    .line 2783
    .local v6, "parent":Landroid/view/ViewParent;
    if-eqz v6, :cond_0

    .line 2784
    invoke-interface {v6, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2788
    :cond_0
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    if-ltz v7, :cond_4

    .line 2789
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v1, v7, v8

    .line 2796
    .local v1, "motionIndex":I
    :goto_0
    const/4 v3, 0x0

    .line 2797
    .local v3, "motionViewPrevStart":I
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2798
    .local v2, "motionView":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 2799
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v7, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 2802
    :cond_1
    :goto_1
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    .line 2803
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollDown:Z

    if-nez v7, :cond_2

    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollUp:Z

    if-eqz v7, :cond_6

    .line 2805
    .local v0, "atEdge":Z
    :cond_2
    :goto_2
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2806
    if-eqz v2, :cond_3

    .line 2807
    iget-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v7, :cond_7

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    .line 2810
    .local v4, "motionViewRealStart":I
    :goto_3
    if-eqz v0, :cond_3

    .line 2811
    neg-int v7, p1

    sub-int v8, v4, v3

    sub-int v5, v7, v8

    .line 2812
    .local v5, "overscroll":I
    invoke-direct {p0, p1, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateOverScrollState(II)V

    .line 2815
    .end local v4    # "motionViewRealStart":I
    .end local v5    # "overscroll":I
    :cond_3
    return-void

    .line 2793
    .end local v0    # "atEdge":Z
    .end local v1    # "motionIndex":I
    .end local v2    # "motionView":Landroid/view/View;
    .end local v3    # "motionViewPrevStart":I
    :cond_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v7

    div-int/lit8 v1, v7, 0x2

    .restart local v1    # "motionIndex":I
    goto :goto_0

    .line 2799
    .restart local v2    # "motionView":Landroid/view/View;
    .restart local v3    # "motionViewPrevStart":I
    :cond_5
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_1

    .line 2803
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 2807
    .restart local v0    # "atEdge":Z
    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_3
.end method

.method private handleFocusWithinItem(I)Z
    .locals 7
    .param p1, "direction"    # I

    .prologue
    .line 1918
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidInnerFocusDirection(I)V

    .line 1920
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v3

    .line 1922
    .local v3, "numChildren":I
    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    if-eqz v5, :cond_1

    if-lez v3, :cond_1

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 1923
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedView()Landroid/view/View;

    move-result-object v4

    .line 1925
    .local v4, "selectedView":Landroid/view/View;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v5

    if-eqz v5, :cond_1

    instance-of v5, v4, Landroid/view/ViewGroup;

    if-eqz v5, :cond_1

    .line 1928
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1929
    .local v0, "currentFocus":Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v5

    check-cast v4, Landroid/view/ViewGroup;

    .end local v4    # "selectedView":Landroid/view/View;
    invoke-virtual {v5, v4, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 1932
    .local v2, "nextFocus":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1934
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v5}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 1935
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1936
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1938
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, v5}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1939
    const/4 v5, 0x1

    .line 1957
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v2    # "nextFocus":Landroid/view/View;
    :goto_0
    return v5

    .line 1948
    .restart local v0    # "currentFocus":Landroid/view/View;
    .restart local v2    # "nextFocus":Landroid/view/View;
    :cond_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getRootView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v6, v5, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 1951
    .local v1, "globalNextFocus":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 1952
    invoke-direct {p0, v1, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v5

    goto :goto_0

    .line 1957
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v1    # "globalNextFocus":Landroid/view/View;
    .end local v2    # "nextFocus":Landroid/view/View;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private handleKeyEvent(IILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "keyCode"    # I
    .param p2, "count"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2517
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    if-nez v0, :cond_1

    .line 2518
    :cond_0
    const/4 v9, 0x0

    .line 2691
    :goto_0
    return v9

    .line 2521
    :cond_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-eqz v0, :cond_2

    .line 2522
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    .line 2526
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFocusedChild()Landroid/view/View;

    move-result-object v8

    .line 2527
    .local v8, "focusview":Landroid/view/View;
    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 2528
    .local v3, "position":I
    const/4 v0, -0x1

    if-eq v3, v0, :cond_3

    .line 2529
    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 2532
    :cond_3
    const/4 v9, 0x0

    .line 2533
    .local v9, "handled":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    .line 2535
    .local v6, "action":I
    const/4 v0, 0x1

    if-eq v6, v0, :cond_4

    .line 2536
    sparse-switch p1, :sswitch_data_0

    .line 2657
    :cond_4
    :goto_1
    if-eqz v9, :cond_28

    .line 2658
    const/4 v9, 0x1

    goto :goto_0

    .line 2538
    :sswitch_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_5

    .line 2539
    const/16 v0, 0x21

    invoke-direct {p0, p3, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v9

    goto :goto_1

    .line 2540
    :cond_5
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2541
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v9

    .line 2542
    if-nez v9, :cond_4

    .line 2543
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleFocusWithinItem(I)Z

    move-result v9

    goto :goto_1

    .line 2549
    :sswitch_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_6

    .line 2550
    const/16 v0, 0x82

    invoke-direct {p0, p3, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v9

    goto :goto_1

    .line 2551
    :cond_6
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2552
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v9

    .line 2553
    if-nez v9, :cond_4

    .line 2554
    const/16 v0, 0x82

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleFocusWithinItem(I)Z

    move-result v9

    goto :goto_1

    .line 2562
    :sswitch_2
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-nez v0, :cond_7

    .line 2564
    const/4 v9, 0x1

    .line 2565
    goto :goto_1

    .line 2567
    :cond_7
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v0, :cond_8

    .line 2568
    const/16 v0, 0x11

    invoke-direct {p0, p3, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v9

    goto :goto_1

    .line 2569
    :cond_8
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2570
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleFocusWithinItem(I)Z

    move-result v9

    goto :goto_1

    .line 2576
    :sswitch_3
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_9

    .line 2578
    const/4 v9, 0x1

    .line 2579
    goto :goto_1

    .line 2581
    :cond_9
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v0, :cond_a

    .line 2582
    const/16 v0, 0x42

    invoke-direct {p0, p3, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyScroll(Landroid/view/KeyEvent;II)Z

    move-result v9

    goto :goto_1

    .line 2583
    :cond_a
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2584
    const/16 v0, 0x42

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleFocusWithinItem(I)Z

    move-result v9

    goto :goto_1

    .line 2590
    :sswitch_4
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2591
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v9

    .line 2592
    if-nez v9, :cond_b

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_b

    .line 2594
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->keyPressed()V

    .line 2595
    const/4 v9, 0x1

    .line 2598
    :cond_b
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 2599
    .local v2, "selectView":Landroid/view/View;
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 2600
    const/4 v0, -0x1

    if-eq v3, v0, :cond_4

    .line 2601
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    .line 2602
    .local v4, "id":J
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 2604
    const/4 v9, 0x1

    goto/16 :goto_1

    .line 2610
    .end local v2    # "selectView":Landroid/view/View;
    .end local v4    # "id":J
    :sswitch_5
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2611
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_e

    const/16 v0, 0x82

    :goto_2
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->pageScroll(I)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_c
    const/4 v9, 0x1

    .line 2618
    :cond_d
    :goto_3
    const/4 v9, 0x1

    .line 2619
    goto/16 :goto_1

    .line 2611
    :cond_e
    const/16 v0, 0x42

    goto :goto_2

    :cond_f
    const/4 v9, 0x0

    goto :goto_3

    .line 2613
    :cond_10
    const/4 v0, 0x1

    invoke-static {p3, v0}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2614
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_11

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x21

    :goto_4
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fullScroll(I)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_11
    const/4 v9, 0x1

    :goto_5
    goto :goto_3

    :cond_12
    const/16 v0, 0x11

    goto :goto_4

    :cond_13
    const/4 v9, 0x0

    goto :goto_5

    .line 2622
    :sswitch_6
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2623
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_14

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_15

    const/16 v0, 0x21

    :goto_6
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->pageScroll(I)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v9, 0x1

    :goto_7
    goto/16 :goto_1

    :cond_15
    const/16 v0, 0x11

    goto :goto_6

    :cond_16
    const/4 v9, 0x0

    goto :goto_7

    .line 2625
    :cond_17
    const/4 v0, 0x2

    invoke-static {p3, v0}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2626
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_18

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_19

    const/16 v0, 0x21

    :goto_8
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fullScroll(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_18
    const/4 v9, 0x1

    :goto_9
    goto/16 :goto_1

    :cond_19
    const/16 v0, 0x11

    goto :goto_8

    :cond_1a
    const/4 v9, 0x0

    goto :goto_9

    .line 2632
    :sswitch_7
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 2633
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_1b

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_1c

    const/16 v0, 0x82

    :goto_a
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->pageScroll(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    :cond_1b
    const/4 v9, 0x1

    :goto_b
    goto/16 :goto_1

    :cond_1c
    const/16 v0, 0x42

    goto :goto_a

    :cond_1d
    const/4 v9, 0x0

    goto :goto_b

    .line 2635
    :cond_1e
    const/4 v0, 0x2

    invoke-static {p3, v0}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2636
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_1f

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_20

    const/16 v0, 0x82

    :goto_c
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fullScroll(I)Z

    move-result v0

    if-eqz v0, :cond_21

    :cond_1f
    const/4 v9, 0x1

    :goto_d
    goto/16 :goto_1

    :cond_20
    const/16 v0, 0x42

    goto :goto_c

    :cond_21
    const/4 v9, 0x0

    goto :goto_d

    .line 2642
    :sswitch_8
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2643
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_23

    const/16 v0, 0x21

    :goto_e
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fullScroll(I)Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_22
    const/4 v9, 0x1

    :goto_f
    goto/16 :goto_1

    :cond_23
    const/16 v0, 0x11

    goto :goto_e

    :cond_24
    const/4 v9, 0x0

    goto :goto_f

    .line 2649
    :sswitch_9
    invoke-static {p3}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2650
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v0

    if-nez v0, :cond_25

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_26

    const/16 v0, 0x82

    :goto_10
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fullScroll(I)Z

    move-result v0

    if-eqz v0, :cond_27

    :cond_25
    const/4 v9, 0x1

    :goto_11
    goto/16 :goto_1

    :cond_26
    const/16 v0, 0x42

    goto :goto_10

    :cond_27
    const/4 v9, 0x0

    goto :goto_11

    .line 2661
    :cond_28
    packed-switch v6, :pswitch_data_0

    .line 2691
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 2663
    :pswitch_0
    invoke-super {p0, p1, p3}, Landroid/widget/AdapterView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v9

    .line 2664
    goto/16 :goto_0

    .line 2667
    :pswitch_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_29

    .line 2668
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 2671
    :cond_29
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_2b

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ltz v0, :cond_2b

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_2b

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2b

    .line 2675
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 2676
    .local v7, "child":Landroid/view/View;
    if-eqz v7, :cond_2a

    .line 2677
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget-wide v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    invoke-virtual {p0, v7, v0, v10, v11}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 2678
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/view/View;->setPressed(Z)V

    .line 2681
    :cond_2a
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setPressed(Z)V

    .line 2682
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 2685
    .end local v7    # "child":Landroid/view/View;
    :cond_2b
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 2688
    :pswitch_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AdapterView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v9

    goto/16 :goto_0

    .line 2536
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x3e -> :sswitch_5
        0x42 -> :sswitch_4
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_8
        0x7b -> :sswitch_9
    .end sparse-switch

    .line 2661
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleKeyScroll(Landroid/view/KeyEvent;II)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "count"    # I
    .param p3, "direction"    # I

    .prologue
    .line 2496
    const/4 v1, 0x0

    .line 2498
    .local v1, "handled":Z
    invoke-static {p1}, Landroid/support/v4/view/KeyEventCompat;->hasNoModifiers(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2499
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v1

    .line 2500
    if-nez v1, :cond_2

    move v0, p2

    .line 2501
    .end local p2    # "count":I
    .local v0, "count":I
    :goto_0
    add-int/lit8 p2, v0, -0x1

    .end local v0    # "count":I
    .restart local p2    # "count":I
    if-lez v0, :cond_2

    .line 2502
    invoke-direct {p0, p3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->arrowScroll(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2503
    const/4 v1, 0x1

    move v0, p2

    .end local p2    # "count":I
    .restart local v0    # "count":I
    goto :goto_0

    .line 2509
    .end local v0    # "count":I
    .restart local p2    # "count":I
    :cond_0
    const/4 v2, 0x2

    invoke-static {p1, v2}, Landroid/support/v4/view/KeyEventCompat;->hasModifiers(Landroid/view/KeyEvent;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2510
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelectionIfNeeded()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, p3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fullScroll(I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v1, 0x1

    .line 2513
    :cond_2
    :goto_1
    return v1

    .line 2510
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private handleNewSelectionChange(Landroid/view/View;IIZ)V
    .locals 10
    .param p1, "selectedView"    # Landroid/view/View;
    .param p2, "direction"    # I
    .param p3, "newSelectedPosition"    # I
    .param p4, "newFocusAssigned"    # Z

    .prologue
    .line 2002
    invoke-direct {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 2004
    const/4 v8, -0x1

    if-ne p3, v8, :cond_0

    .line 2005
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "newSelectedPosition needs to be valid"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2012
    :cond_0
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v4, v8, v9

    .line 2013
    .local v4, "selectedIndex":I
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v2, p3, v8

    .line 2015
    .local v2, "nextSelectedIndex":I
    const/4 v7, 0x0

    .line 2019
    .local v7, "topSelected":Z
    const/16 v8, 0x21

    if-eq p2, v8, :cond_1

    const/16 v8, 0x11

    if-ne p2, v8, :cond_4

    .line 2020
    :cond_1
    move v6, v2

    .line 2021
    .local v6, "startViewIndex":I
    move v1, v4

    .line 2022
    .local v1, "endViewIndex":I
    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2023
    .local v5, "startView":Landroid/view/View;
    move-object v0, p1

    .line 2024
    .local v0, "endView":Landroid/view/View;
    const/4 v7, 0x1

    .line 2032
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v3

    .line 2035
    .local v3, "numChildren":I
    if-eqz v5, :cond_2

    .line 2036
    if-nez p4, :cond_5

    if-eqz v7, :cond_5

    const/4 v8, 0x1

    :goto_1
    invoke-virtual {v5, v8}, Landroid/view/View;->setSelected(Z)V

    .line 2037
    invoke-direct {p0, v5, v6, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureAndAdjustDown(Landroid/view/View;II)V

    .line 2041
    :cond_2
    if-eqz v0, :cond_3

    .line 2042
    if-nez p4, :cond_6

    if-nez v7, :cond_6

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {v0, v8}, Landroid/view/View;->setSelected(Z)V

    .line 2043
    invoke-direct {p0, v0, v1, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureAndAdjustDown(Landroid/view/View;II)V

    .line 2045
    :cond_3
    return-void

    .line 2026
    .end local v0    # "endView":Landroid/view/View;
    .end local v1    # "endViewIndex":I
    .end local v3    # "numChildren":I
    .end local v5    # "startView":Landroid/view/View;
    .end local v6    # "startViewIndex":I
    :cond_4
    move v6, v4

    .line 2027
    .restart local v6    # "startViewIndex":I
    move v1, v2

    .line 2028
    .restart local v1    # "endViewIndex":I
    move-object v5, p1

    .line 2029
    .restart local v5    # "startView":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "endView":Landroid/view/View;
    goto :goto_0

    .line 2036
    .restart local v3    # "numChildren":I
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 2042
    :cond_6
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private handleOverScrollChange(I)V
    .locals 4
    .param p1, "delta"    # I

    .prologue
    .line 2861
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 2862
    .local v1, "oldOverScroll":I
    sub-int v0, v1, p1

    .line 2864
    .local v0, "newOverScroll":I
    neg-int v2, p1

    .line 2865
    .local v2, "overScrollDistance":I
    if-gez v0, :cond_0

    if-gez v1, :cond_1

    :cond_0
    if-lez v0, :cond_5

    if-gtz v1, :cond_5

    .line 2867
    :cond_1
    neg-int v2, v1

    .line 2868
    add-int/2addr p1, v2

    .line 2873
    :goto_0
    if-eqz v2, :cond_2

    .line 2874
    invoke-direct {p0, p1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateOverScrollState(II)V

    .line 2877
    :cond_2
    if-eqz p1, :cond_4

    .line 2878
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eqz v3, :cond_3

    .line 2879
    const/4 v3, 0x0

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 2880
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 2883
    :cond_3
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    .line 2884
    const/4 v3, 0x3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 2888
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    float-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findClosestMotionRowOrColumn(I)I

    move-result v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    .line 2889
    const/4 v3, 0x0

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    .line 2891
    :cond_4
    return-void

    .line 2870
    :cond_5
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private hideSelector()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 3360
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-eq v0, v2, :cond_2

    .line 3361
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 3362
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 3365
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-eq v0, v1, :cond_1

    .line 3366
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 3369
    :cond_1
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 3370
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 3372
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 3374
    :cond_2
    return-void
.end method

.method private initOrResetVelocityTracker()V
    .locals 1

    .prologue
    .line 2696
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2697
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2701
    :goto_0
    return-void

    .line 2699
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method private initVelocityTrackerIfNotExists()V
    .locals 1

    .prologue
    .line 2704
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2705
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2707
    :cond_0
    return-void
.end method

.method private invokeOnItemScrollListener()V
    .locals 4

    .prologue
    .line 2720
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2721
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;->onScroll(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;III)V

    .line 2726
    :cond_0
    return-void
.end method

.method private isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 1778
    if-ne p1, p2, :cond_1

    .line 1784
    :cond_0
    :goto_0
    return v1

    .line 1782
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1784
    .local v0, "theParent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/view/View;

    .end local v0    # "theParent":Landroid/view/ViewParent;
    invoke-direct {p0, v0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private keyPressed()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 3427
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isClickable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 3469
    :cond_0
    :goto_0
    return-void

    .line 3431
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 3432
    .local v3, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 3434
    .local v4, "selectorRect":Landroid/graphics/Rect;
    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isFocused()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->touchModeDrawsInPressedState()Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3437
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3439
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 3440
    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3444
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 3447
    :cond_3
    invoke-virtual {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setPressed(Z)V

    .line 3449
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isLongClickable()Z

    move-result v2

    .line 3450
    .local v2, "longClickable":Z
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3451
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_4

    instance-of v5, v1, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v5, :cond_4

    .line 3452
    if-eqz v2, :cond_6

    .line 3453
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 3460
    :cond_4
    :goto_1
    if-eqz v2, :cond_0

    iget-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-nez v5, :cond_0

    .line 3461
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForKeyLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;

    if-nez v5, :cond_5

    .line 3462
    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForKeyLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;

    .line 3465
    :cond_5
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForKeyLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;->rememberWindowAttachCount()V

    .line 3466
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForKeyLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForKeyLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {p0, v5, v6, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 3456
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    :cond_6
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1
.end method

.method private layoutChildren()V
    .locals 34

    .prologue
    .line 3893
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v2

    if-nez v2, :cond_1

    .line 4195
    :cond_0
    :goto_0
    return-void

    .line 3897
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 3898
    .local v13, "blockLayoutRequests":Z
    if-nez v13, :cond_0

    .line 3899
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 3905
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    .line 3907
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_2

    .line 3908
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resetState()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4190
    if-nez v13, :cond_0

    .line 4191
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 4192
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    goto :goto_0

    .line 3912
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v6

    .line 3913
    .local v6, "start":I
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v32

    sub-int v7, v2, v32

    .line 3916
    .local v7, "end":I
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v15

    .line 3917
    .local v15, "childCount":I
    const/16 v24, 0x0

    .line 3918
    .local v24, "index":I
    const/4 v5, 0x0

    .line 3920
    .local v5, "delta":I
    const/16 v19, 0x0

    .line 3921
    .local v19, "focusLayoutRestoreView":Landroid/view/View;
    const/4 v10, 0x0

    .line 3922
    .local v10, "accessibilityFocusLayoutRestoreNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    const/4 v11, 0x0

    .line 3923
    .local v11, "accessibilityFocusLayoutRestoreView":Landroid/view/View;
    const/4 v12, -0x1

    .line 3925
    .local v12, "accessibilityFocusPosition":I
    const/16 v31, 0x0

    .line 3926
    .local v31, "selected":Landroid/view/View;
    const/4 v3, 0x0

    .line 3927
    .local v3, "oldSelected":Landroid/view/View;
    const/4 v4, 0x0

    .line 3928
    .local v4, "newSelected":Landroid/view/View;
    const/16 v27, 0x0

    .line 3930
    .local v27, "oldFirstChild":Landroid/view/View;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    packed-switch v2, :pswitch_data_0

    .line 3948
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v32, v0

    sub-int v24, v2, v32

    .line 3949
    if-ltz v24, :cond_3

    move/from16 v0, v24

    if-ge v0, v15, :cond_3

    .line 3950
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 3954
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v27

    .line 3956
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    if-ltz v2, :cond_4

    .line 3957
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v32, v0

    sub-int v5, v2, v32

    .line 3961
    :cond_4
    add-int v2, v24, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3964
    :cond_5
    :goto_3
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    move/from16 v16, v0

    .line 3965
    .local v16, "dataChanged":Z
    if-eqz v16, :cond_6

    .line 3966
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleDataChanged()V

    .line 3971
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-nez v2, :cond_9

    .line 3972
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resetState()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4190
    if-nez v13, :cond_0

    .line 4191
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 4192
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    goto/16 :goto_0

    .line 3912
    .end local v3    # "oldSelected":Landroid/view/View;
    .end local v4    # "newSelected":Landroid/view/View;
    .end local v5    # "delta":I
    .end local v6    # "start":I
    .end local v7    # "end":I
    .end local v10    # "accessibilityFocusLayoutRestoreNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v11    # "accessibilityFocusLayoutRestoreView":Landroid/view/View;
    .end local v12    # "accessibilityFocusPosition":I
    .end local v15    # "childCount":I
    .end local v16    # "dataChanged":Z
    .end local v19    # "focusLayoutRestoreView":Landroid/view/View;
    .end local v24    # "index":I
    .end local v27    # "oldFirstChild":Landroid/view/View;
    .end local v31    # "selected":Landroid/view/View;
    :cond_7
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v6

    goto/16 :goto_1

    .line 3913
    .restart local v6    # "start":I
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v32

    sub-int v7, v2, v32

    goto/16 :goto_2

    .line 3932
    .restart local v3    # "oldSelected":Landroid/view/View;
    .restart local v4    # "newSelected":Landroid/view/View;
    .restart local v5    # "delta":I
    .restart local v7    # "end":I
    .restart local v10    # "accessibilityFocusLayoutRestoreNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v11    # "accessibilityFocusLayoutRestoreView":Landroid/view/View;
    .restart local v12    # "accessibilityFocusPosition":I
    .restart local v15    # "childCount":I
    .restart local v19    # "focusLayoutRestoreView":Landroid/view/View;
    .restart local v24    # "index":I
    .restart local v27    # "oldFirstChild":Landroid/view/View;
    .restart local v31    # "selected":Landroid/view/View;
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v32, v0

    sub-int v24, v2, v32

    .line 3933
    if-ltz v24, :cond_5

    move/from16 v0, v24

    if-ge v0, v15, :cond_5

    .line 3934
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    goto :goto_3

    .line 3974
    .restart local v16    # "dataChanged":Z
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Landroid/widget/ListAdapter;->getCount()I

    move-result v32

    move/from16 v0, v32

    if-eq v2, v0, :cond_b

    .line 3975
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "The content of the adapter has changed but FrameListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in FrameListView("

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getId()I

    move-result v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ", "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ") with Adapter("

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ")]"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4190
    .end local v3    # "oldSelected":Landroid/view/View;
    .end local v4    # "newSelected":Landroid/view/View;
    .end local v5    # "delta":I
    .end local v6    # "start":I
    .end local v7    # "end":I
    .end local v10    # "accessibilityFocusLayoutRestoreNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    .end local v11    # "accessibilityFocusLayoutRestoreView":Landroid/view/View;
    .end local v12    # "accessibilityFocusPosition":I
    .end local v15    # "childCount":I
    .end local v16    # "dataChanged":Z
    .end local v19    # "focusLayoutRestoreView":Landroid/view/View;
    .end local v24    # "index":I
    .end local v27    # "oldFirstChild":Landroid/view/View;
    .end local v31    # "selected":Landroid/view/View;
    :catchall_0
    move-exception v2

    if-nez v13, :cond_a

    .line 4191
    const/16 v32, 0x0

    move/from16 v0, v32

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 4192
    const/16 v32, 0x0

    move/from16 v0, v32

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    :cond_a
    throw v2

    .line 3982
    .restart local v3    # "oldSelected":Landroid/view/View;
    .restart local v4    # "newSelected":Landroid/view/View;
    .restart local v5    # "delta":I
    .restart local v6    # "start":I
    .restart local v7    # "end":I
    .restart local v10    # "accessibilityFocusLayoutRestoreNode":Landroid/view/accessibility/AccessibilityNodeInfo;
    .restart local v11    # "accessibilityFocusLayoutRestoreView":Landroid/view/View;
    .restart local v12    # "accessibilityFocusPosition":I
    .restart local v15    # "childCount":I
    .restart local v16    # "dataChanged":Z
    .restart local v19    # "focusLayoutRestoreView":Landroid/view/View;
    .restart local v24    # "index":I
    .restart local v27    # "oldFirstChild":Landroid/view/View;
    .restart local v31    # "selected":Landroid/view/View;
    :cond_b
    :try_start_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 3985
    const/16 v18, 0x0

    .line 3989
    .local v18, "focusLayoutRestoreDirectChild":Landroid/view/View;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v17, v0

    .line 3990
    .local v17, "firstPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    move-object/from16 v29, v0

    .line 3992
    .local v29, "recycleBin":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;
    if-eqz v16, :cond_c

    .line 3993
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_4
    move/from16 v0, v23

    if-ge v0, v15, :cond_d

    .line 3994
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    add-int v32, v17, v23

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3993
    add-int/lit8 v23, v23, 0x1

    goto :goto_4

    .line 3997
    .end local v23    # "i":I
    :cond_c
    move-object/from16 v0, v29

    move/from16 v1, v17

    invoke-virtual {v0, v15, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->fillActiveViews(II)V

    .line 4004
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFocusedChild()Landroid/view/View;

    move-result-object v22

    .line 4005
    .local v22, "focusedChild":Landroid/view/View;
    if-eqz v22, :cond_f

    .line 4008
    if-nez v16, :cond_e

    .line 4009
    move-object/from16 v18, v22

    .line 4012
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findFocus()Landroid/view/View;

    move-result-object v19

    .line 4013
    if-eqz v19, :cond_e

    .line 4015
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 4019
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestFocus()Z

    .line 4027
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAccessibilityFocusedHost()Landroid/view/View;

    move-result-object v9

    .line 4028
    .local v9, "accessFocusedView":Landroid/view/View;
    if-eqz v9, :cond_10

    .line 4029
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findAccessibilityFocusedChild(Landroid/view/View;)Landroid/view/View;

    move-result-object v8

    .line 4030
    .local v8, "accessFocusedChild":Landroid/view/View;
    if-eqz v8, :cond_10

    .line 4031
    if-nez v16, :cond_18

    .line 4035
    move-object v11, v9

    .line 4036
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAccessibilityFocusedVirtualView()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v10

    .line 4044
    .end local v8    # "accessFocusedChild":Landroid/view/View;
    :cond_10
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->detachAllViewsFromParent()V

    .line 4046
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    packed-switch v2, :pswitch_data_1

    .line 4083
    if-nez v15, :cond_1b

    .line 4084
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(I)I

    move-result v28

    .line 4085
    .local v28, "position":I
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 4086
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillFromOffset(I)Landroid/view/View;

    move-result-object v31

    .line 4110
    .end local v28    # "position":I
    :goto_6
    invoke-virtual/range {v29 .. v29}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->scrapActiveViews()V

    .line 4112
    if-eqz v31, :cond_26

    .line 4113
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    if-eqz v2, :cond_24

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_24

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_24

    .line 4114
    move-object/from16 v0, v31

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_11

    if-eqz v19, :cond_11

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->requestFocus()Z

    move-result v2

    if-nez v2, :cond_12

    :cond_11
    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->requestFocus()Z

    move-result v2

    if-eqz v2, :cond_22

    :cond_12
    const/16 v20, 0x1

    .line 4118
    .local v20, "focusWasTaken":Z
    :goto_7
    if-nez v20, :cond_23

    .line 4122
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFocusedChild()Landroid/view/View;

    move-result-object v21

    .line 4123
    .local v21, "focused":Landroid/view/View;
    if-eqz v21, :cond_13

    .line 4124
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->clearFocus()V

    .line 4127
    :cond_13
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    .line 4136
    .end local v20    # "focusWasTaken":Z
    .end local v21    # "focused":Landroid/view/View;
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_25

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getTop()I

    move-result v2

    :goto_9
    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 4157
    :cond_14
    :goto_a
    if-eqz v10, :cond_29

    .line 4158
    const/16 v2, 0x40

    invoke-virtual {v10, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->performAction(I)Z

    .line 4174
    :cond_15
    :goto_b
    if-eqz v19, :cond_16

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_16

    .line 4176
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 4179
    :cond_16
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 4180
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 4181
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 4183
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 4184
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-lez v2, :cond_17

    .line 4185
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkSelectionChanged()V

    .line 4188
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4190
    if-nez v13, :cond_0

    .line 4191
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 4192
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    goto/16 :goto_0

    .line 4040
    .restart local v8    # "accessFocusedChild":Landroid/view/View;
    :cond_18
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPositionForView(Landroid/view/View;)I

    move-result v12

    goto/16 :goto_5

    .line 4048
    .end local v8    # "accessFocusedChild":Landroid/view/View;
    :pswitch_2
    if-eqz v4, :cond_1a

    .line 4049
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_19

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v25

    .line 4052
    .local v25, "newSelectedStart":I
    :goto_c
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1, v6, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillFromSelection(III)Landroid/view/View;

    move-result-object v31

    .line 4053
    goto/16 :goto_6

    .line 4049
    .end local v25    # "newSelectedStart":I
    :cond_19
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v25

    goto :goto_c

    .line 4054
    :cond_1a
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillFromMiddle(II)Landroid/view/View;

    move-result-object v31

    .line 4057
    goto/16 :goto_6

    .line 4060
    :pswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v31

    .line 4061
    goto/16 :goto_6

    .line 4064
    :pswitch_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBefore(II)Landroid/view/View;

    move-result-object v31

    .line 4065
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    goto/16 :goto_6

    .line 4069
    :pswitch_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 4070
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillFromOffset(I)Landroid/view/View;

    move-result-object v31

    .line 4071
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    goto/16 :goto_6

    .line 4075
    :pswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reconcileSelectedPosition()I

    move-result v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v31

    .line 4076
    goto/16 :goto_6

    :pswitch_7
    move-object/from16 v2, p0

    .line 4079
    invoke-direct/range {v2 .. v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;

    move-result-object v31

    .line 4080
    goto/16 :goto_6

    .line 4088
    :cond_1b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ltz v2, :cond_1e

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v2, v0, :cond_1e

    .line 4089
    move/from16 v26, v6

    .line 4090
    .local v26, "offset":I
    if-eqz v3, :cond_1c

    .line 4091
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_1d

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v26

    .line 4093
    :cond_1c
    :goto_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v31

    .line 4094
    goto/16 :goto_6

    .line 4091
    :cond_1d
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v26

    goto :goto_d

    .line 4094
    .end local v26    # "offset":I
    :cond_1e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v2, v0, :cond_21

    .line 4095
    move/from16 v26, v6

    .line 4096
    .restart local v26    # "offset":I
    if-eqz v27, :cond_1f

    .line 4097
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_20

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getTop()I

    move-result v26

    .line 4100
    :cond_1f
    :goto_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v31

    .line 4101
    goto/16 :goto_6

    .line 4097
    :cond_20
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLeft()I

    move-result v26

    goto :goto_e

    .line 4102
    .end local v26    # "offset":I
    :cond_21
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v31

    goto/16 :goto_6

    .line 4114
    :cond_22
    const/16 v20, 0x0

    goto/16 :goto_7

    .line 4129
    .restart local v20    # "focusWasTaken":Z
    :cond_23
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 4130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_8

    .line 4133
    .end local v20    # "focusWasTaken":Z
    :cond_24
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    goto/16 :goto_8

    .line 4136
    :cond_25
    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getLeft()I

    move-result v2

    goto/16 :goto_9

    .line 4138
    :cond_26
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    if-lez v2, :cond_28

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    const/16 v32, 0x3

    move/from16 v0, v32

    if-ge v2, v0, :cond_28

    .line 4139
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v32, v0

    sub-int v2, v2, v32

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 4141
    .local v14, "child":Landroid/view/View;
    if-eqz v14, :cond_27

    .line 4142
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v14}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    .line 4151
    .end local v14    # "child":Landroid/view/View;
    :cond_27
    :goto_f
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_14

    if-eqz v19, :cond_14

    .line 4152
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_a

    .line 4145
    :cond_28
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 4146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_f

    .line 4160
    :cond_29
    if-eqz v11, :cond_2a

    .line 4161
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestAccessibilityFocus(Landroid/view/View;)V

    goto/16 :goto_b

    .line 4162
    :cond_2a
    const/4 v2, -0x1

    if-eq v12, v2, :cond_15

    .line 4164
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v2, v12, v2

    const/16 v32, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v33

    add-int/lit8 v33, v33, -0x1

    move/from16 v0, v32

    move/from16 v1, v33

    invoke-static {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$MathUtils;->constrain(III)I

    move-result v28

    .line 4166
    .restart local v28    # "position":I
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v30

    .line 4167
    .local v30, "restoreView":Landroid/view/View;
    if-eqz v30, :cond_15

    .line 4168
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestAccessibilityFocus(Landroid/view/View;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_b

    .line 3930
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 4046
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method private lookForSelectablePosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3536
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v0

    return v0
.end method

.method private lookForSelectablePosition(IZ)I
    .locals 4
    .param p1, "position"    # I
    .param p2, "lookDown"    # Z

    .prologue
    const/4 v2, -0x1

    .line 3540
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 3541
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3569
    :cond_0
    :goto_0
    return v2

    .line 3545
    :cond_1
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 3546
    .local v1, "itemCount":I
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAreAllItemsSelectable:Z

    if-nez v3, :cond_4

    .line 3547
    if-eqz p2, :cond_2

    .line 3548
    const/4 v3, 0x0

    invoke-static {v3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 3549
    :goto_1
    if-ge p1, v1, :cond_3

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3550
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 3553
    :cond_2
    add-int/lit8 v3, v1, -0x1

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 3554
    :goto_2
    if-ltz p1, :cond_3

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3555
    add-int/lit8 p1, p1, -0x1

    goto :goto_2

    .line 3559
    :cond_3
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    move v2, p1

    .line 3563
    goto :goto_0

    .line 3565
    :cond_4
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    move v2, p1

    .line 3569
    goto :goto_0
.end method

.method private lookForSelectablePositionOnScreen(I)I
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v6, -0x1

    .line 3584
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 3586
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 3587
    .local v1, "firstPosition":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 3589
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/16 v7, 0x82

    if-eq p1, v7, :cond_0

    const/16 v7, 0x42

    if-ne p1, v7, :cond_6

    .line 3590
    :cond_0
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-eq v7, v6, :cond_2

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    add-int/lit8 v5, v7, 0x1

    .line 3593
    .local v5, "startPos":I
    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    if-lt v5, v7, :cond_3

    move v4, v6

    .line 3631
    :cond_1
    :goto_1
    return v4

    .end local v5    # "startPos":I
    :cond_2
    move v5, v1

    .line 3590
    goto :goto_0

    .line 3597
    .restart local v5    # "startPos":I
    :cond_3
    if-ge v5, v1, :cond_4

    .line 3598
    move v5, v1

    .line 3601
    :cond_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getLastVisiblePosition()I

    move-result v3

    .line 3603
    .local v3, "lastVisiblePos":I
    move v4, v5

    .local v4, "pos":I
    :goto_2
    if-gt v4, v3, :cond_c

    .line 3604
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_5

    sub-int v7, v4, v1

    invoke-virtual {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 3603
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 3610
    .end local v3    # "lastVisiblePos":I
    .end local v4    # "pos":I
    .end local v5    # "startPos":I
    :cond_6
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v2, v7, -0x1

    .line 3612
    .local v2, "last":I
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-eq v7, v6, :cond_8

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    add-int/lit8 v5, v7, -0x1

    .line 3615
    .restart local v5    # "startPos":I
    :goto_3
    if-ltz v5, :cond_7

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    if-lt v5, v7, :cond_9

    :cond_7
    move v4, v6

    .line 3616
    goto :goto_1

    .line 3612
    .end local v5    # "startPos":I
    :cond_8
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v5, v7, -0x1

    goto :goto_3

    .line 3619
    .restart local v5    # "startPos":I
    :cond_9
    if-le v5, v2, :cond_a

    .line 3620
    move v5, v2

    .line 3623
    :cond_a
    move v4, v5

    .restart local v4    # "pos":I
    :goto_4
    if-lt v4, v1, :cond_c

    .line 3624
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_b

    sub-int v7, v4, v1

    invoke-virtual {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_1

    .line 3623
    :cond_b
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .end local v2    # "last":I
    :cond_c
    move v4, v6

    .line 3631
    goto :goto_1
.end method

.method private makeAndAddView(IIZZ)Landroid/view/View;
    .locals 13
    .param p1, "position"    # I
    .param p2, "offset"    # I
    .param p3, "flow"    # Z
    .param p4, "selected"    # Z

    .prologue
    .line 4880
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_0

    .line 4881
    move v3, p2

    .line 4882
    .local v3, "top":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v4

    .line 4888
    .local v4, "left":I
    :goto_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-nez v0, :cond_1

    .line 4890
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->getActiveView(I)Landroid/view/View;

    move-result-object v1

    .line 4891
    .local v1, "activeChild":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 4894
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setupChild(Landroid/view/View;IIIZZZ)V

    .line 4906
    .end local v1    # "activeChild":Landroid/view/View;
    :goto_1
    return-object v1

    .line 4884
    .end local v3    # "top":I
    .end local v4    # "left":I
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v3

    .line 4885
    .restart local v3    # "top":I
    move v4, p2

    .restart local v4    # "left":I
    goto :goto_0

    .line 4901
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v6

    .line 4904
    .local v6, "child":Landroid/view/View;
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    const/4 v2, 0x0

    aget-boolean v12, v0, v2

    move-object v5, p0

    move v7, p1

    move v8, v3

    move v9, v4

    move/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setupChild(Landroid/view/View;IIIZZZ)V

    move-object v1, v6

    .line 4906
    goto :goto_1
.end method

.method private maybeScroll(I)V
    .locals 2
    .param p1, "delta"    # I

    .prologue
    .line 2772
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2773
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleDragChange(I)V

    .line 2777
    :cond_0
    :goto_0
    return-void

    .line 2774
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2775
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleOverScrollChange(I)V

    goto :goto_0
.end method

.method private maybeStartScrolling(I)Z
    .locals 7
    .param p1, "delta"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2740
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eqz v5, :cond_0

    move v0, v3

    .line 2741
    .local v0, "isOverScroll":Z
    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchSlop:I

    if-gt v5, v6, :cond_1

    if-nez v0, :cond_1

    .line 2768
    :goto_1
    return v4

    .end local v0    # "isOverScroll":Z
    :cond_0
    move v0, v4

    .line 2740
    goto :goto_0

    .line 2745
    .restart local v0    # "isOverScroll":Z
    :cond_1
    if-eqz v0, :cond_4

    .line 2746
    const/4 v5, 0x5

    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 2753
    :goto_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 2754
    .local v2, "parent":Landroid/view/ViewParent;
    if-eqz v2, :cond_2

    .line 2755
    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2758
    :cond_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cancelCheckForLongPress()V

    .line 2760
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setPressed(Z)V

    .line 2761
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2762
    .local v1, "motionView":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 2763
    invoke-virtual {v1, v4}, Landroid/view/View;->setPressed(Z)V

    .line 2766
    :cond_3
    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    move v4, v3

    .line 2768
    goto :goto_1

    .line 2748
    .end local v1    # "motionView":Landroid/view/View;
    .end local v2    # "parent":Landroid/view/ViewParent;
    :cond_4
    const/4 v5, 0x3

    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    goto :goto_2
.end method

.method private measureAndAdjustDown(Landroid/view/View;II)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "childIndex"    # I
    .param p3, "numChildren"    # I

    .prologue
    .line 2056
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 2057
    .local v2, "oldHeight":I
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureChild(Landroid/view/View;)V

    .line 2059
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 2071
    :cond_0
    return-void

    .line 2064
    :cond_1
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->relayoutMeasuredChild(Landroid/view/View;)V

    .line 2067
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v0, v3, v2

    .line 2068
    .local v0, "heightDelta":I
    add-int/lit8 v1, p2, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_0

    .line 2069
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 2068
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private measureChild(Landroid/view/View;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 4650
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureChild(Landroid/view/View;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)V

    .line 4651
    return-void
.end method

.method private measureChild(Landroid/view/View;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "lp"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .prologue
    .line 4654
    invoke-direct {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildWidthMeasureSpec(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)I

    move-result v1

    .line 4655
    .local v1, "widthSpec":I
    invoke-direct {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildHeightMeasureSpec(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)I

    move-result v0

    .line 4656
    .local v0, "heightSpec":I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 4657
    return-void
.end method

.method private measureHeightOfChildren(IIIII)I
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxHeight"    # I
    .param p5, "disallowPartialChildPosition"    # I

    .prologue
    .line 4723
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v7

    .line 4724
    .local v7, "paddingTop":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v6

    .line 4726
    .local v6, "paddingBottom":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 4727
    .local v1, "adapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_1

    .line 4728
    add-int v8, v7, v6

    .line 4782
    :cond_0
    :goto_0
    return v8

    .line 4732
    :cond_1
    add-int v10, v7, v6

    .line 4733
    .local v10, "returnedHeight":I
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    .line 4737
    .local v5, "itemMargin":I
    const/4 v8, 0x0

    .line 4742
    .local v8, "prevHeightWithoutPartialChild":I
    const/4 v12, -0x1

    move/from16 v0, p3

    if-ne v0, v12, :cond_2

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    add-int/lit8 p3, v12, -0x1

    .line 4743
    :cond_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    .line 4744
    .local v9, "recycleBin":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleOnMeasure()Z

    move-result v11

    .line 4745
    .local v11, "shouldRecycle":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    .line 4747
    .local v4, "isScrap":[Z
    move v3, p2

    .local v3, "i":I
    :goto_1
    move/from16 v0, p3

    if-gt v3, v0, :cond_8

    .line 4748
    invoke-direct {p0, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v2

    .line 4750
    .local v2, "child":Landroid/view/View;
    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureScrapChild(Landroid/view/View;II)V

    .line 4752
    if-lez v3, :cond_3

    .line 4754
    add-int/2addr v10, v5

    .line 4758
    :cond_3
    if-eqz v11, :cond_4

    .line 4759
    const/4 v12, -0x1

    invoke-virtual {v9, v2, v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 4762
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v10, v12

    .line 4764
    move/from16 v0, p4

    if-lt v10, v0, :cond_6

    .line 4767
    if-ltz p5, :cond_5

    move/from16 v0, p5

    if-le v3, v0, :cond_5

    if-lez v8, :cond_5

    move/from16 v0, p4

    if-ne v10, v0, :cond_0

    :cond_5
    move/from16 v8, p4

    goto :goto_0

    .line 4775
    :cond_6
    if-ltz p5, :cond_7

    move/from16 v0, p5

    if-lt v3, v0, :cond_7

    .line 4776
    move v8, v10

    .line 4747
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v2    # "child":Landroid/view/View;
    :cond_8
    move v8, v10

    .line 4782
    goto :goto_0
.end method

.method private measureScrapChild(Landroid/view/View;II)V
    .locals 4
    .param p1, "scrapChild"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "secondaryMeasureSpec"    # I

    .prologue
    .line 4672
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .line 4673
    .local v1, "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    if-nez v1, :cond_0

    .line 4674
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->generateDefaultLayoutParams()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    move-result-object v1

    .line 4675
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4678
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v3

    iput v3, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->viewType:I

    .line 4679
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->forceAdd:Z

    .line 4683
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v3, :cond_1

    .line 4684
    move v2, p3

    .line 4685
    .local v2, "widthMeasureSpec":I
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildHeightMeasureSpec(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)I

    move-result v0

    .line 4691
    .local v0, "heightMeasureSpec":I
    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 4692
    return-void

    .line 4687
    .end local v0    # "heightMeasureSpec":I
    .end local v2    # "widthMeasureSpec":I
    :cond_1
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildWidthMeasureSpec(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)I

    move-result v2

    .line 4688
    .restart local v2    # "widthMeasureSpec":I
    move v0, p3

    .restart local v0    # "heightMeasureSpec":I
    goto :goto_0
.end method

.method private measureWidthOfChildren(IIIII)I
    .locals 13
    .param p1, "heightMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxWidth"    # I
    .param p5, "disallowPartialChildPosition"    # I

    .prologue
    .line 4814
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v6

    .line 4815
    .local v6, "paddingLeft":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v7

    .line 4817
    .local v7, "paddingRight":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 4818
    .local v1, "adapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_1

    .line 4819
    add-int v8, v6, v7

    .line 4873
    :cond_0
    :goto_0
    return v8

    .line 4823
    :cond_1
    add-int v10, v6, v7

    .line 4824
    .local v10, "returnedWidth":I
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    .line 4828
    .local v5, "itemMargin":I
    const/4 v8, 0x0

    .line 4833
    .local v8, "prevWidthWithoutPartialChild":I
    const/4 v12, -0x1

    move/from16 v0, p3

    if-ne v0, v12, :cond_2

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    add-int/lit8 p3, v12, -0x1

    .line 4834
    :cond_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    .line 4835
    .local v9, "recycleBin":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleOnMeasure()Z

    move-result v11

    .line 4836
    .local v11, "shouldRecycle":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    .line 4838
    .local v4, "isScrap":[Z
    move v3, p2

    .local v3, "i":I
    :goto_1
    move/from16 v0, p3

    if-gt v3, v0, :cond_8

    .line 4839
    invoke-direct {p0, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v2

    .line 4841
    .local v2, "child":Landroid/view/View;
    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureScrapChild(Landroid/view/View;II)V

    .line 4843
    if-lez v3, :cond_3

    .line 4845
    add-int/2addr v10, v5

    .line 4849
    :cond_3
    if-eqz v11, :cond_4

    .line 4850
    const/4 v12, -0x1

    invoke-virtual {v9, v2, v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 4853
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v10, v12

    .line 4855
    move/from16 v0, p4

    if-lt v10, v0, :cond_6

    .line 4858
    if-ltz p5, :cond_5

    move/from16 v0, p5

    if-le v3, v0, :cond_5

    if-lez v8, :cond_5

    move/from16 v0, p4

    if-ne v10, v0, :cond_0

    :cond_5
    move/from16 v8, p4

    goto :goto_0

    .line 4866
    :cond_6
    if-ltz p5, :cond_7

    move/from16 v0, p5

    if-lt v3, v0, :cond_7

    .line 4867
    move v8, v10

    .line 4838
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v2    # "child":Landroid/view/View;
    :cond_8
    move v8, v10

    .line 4873
    goto :goto_0
.end method

.method private moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;
    .locals 20
    .param p1, "oldSelected"    # Landroid/view/View;
    .param p2, "newSelected"    # Landroid/view/View;
    .param p3, "delta"    # I
    .param p4, "start"    # I
    .param p5, "end"    # I

    .prologue
    .line 4217
    move-object/from16 v0, p0

    iget v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 4219
    .local v13, "selectedPosition":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v10

    .line 4220
    .local v10, "oldSelectedStart":I
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getBottom()I

    move-result v9

    .line 4222
    .local v9, "oldSelectedEnd":I
    :goto_1
    const/4 v11, 0x0

    .line 4224
    .local v11, "selected":Landroid/view/View;
    if-lez p3, :cond_6

    .line 4246
    add-int/lit8 v17, v13, -0x1

    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object p1

    .line 4248
    move-object/from16 v0, p0

    iget v5, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    .line 4251
    .local v5, "itemMargin":I
    add-int v17, v9, v5

    const/16 v18, 0x1

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v13, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    .line 4253
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v14

    .line 4254
    .local v14, "selectedStart":I
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v12

    .line 4257
    .local v12, "selectedEnd":I
    :goto_3
    move/from16 v0, p5

    if-le v12, v0, :cond_0

    .line 4259
    sub-int v16, v14, p4

    .line 4262
    .local v16, "spaceBefore":I
    sub-int v15, v12, p5

    .line 4265
    .local v15, "spaceAfter":I
    sub-int v17, p5, p4

    div-int/lit8 v4, v17, 0x2

    .line 4266
    .local v4, "halfSpace":I
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 4267
    .local v8, "offset":I
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 4269
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_5

    .line 4270
    neg-int v0, v8

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4271
    neg-int v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4279
    .end local v4    # "halfSpace":I
    .end local v8    # "offset":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_0
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x2

    sub-int v18, v14, v5

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBefore(II)Landroid/view/View;

    .line 4280
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->adjustViewsStartOrEnd()V

    .line 4281
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    add-int v18, v12, v5

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillAfter(II)Landroid/view/View;

    .line 4367
    .end local v5    # "itemMargin":I
    :goto_5
    return-object v11

    .line 4219
    .end local v9    # "oldSelectedEnd":I
    .end local v10    # "oldSelectedStart":I
    .end local v11    # "selected":Landroid/view/View;
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v10

    goto/16 :goto_0

    .line 4220
    .restart local v10    # "oldSelectedStart":I
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRight()I

    move-result v9

    goto/16 :goto_1

    .line 4253
    .restart local v5    # "itemMargin":I
    .restart local v9    # "oldSelectedEnd":I
    .restart local v11    # "selected":Landroid/view/View;
    :cond_3
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v14

    goto :goto_2

    .line 4254
    .restart local v14    # "selectedStart":I
    :cond_4
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v12

    goto :goto_3

    .line 4273
    .restart local v4    # "halfSpace":I
    .restart local v8    # "offset":I
    .restart local v12    # "selectedEnd":I
    .restart local v15    # "spaceAfter":I
    .restart local v16    # "spaceBefore":I
    :cond_5
    neg-int v0, v8

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 4274
    neg-int v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_4

    .line 4282
    .end local v4    # "halfSpace":I
    .end local v5    # "itemMargin":I
    .end local v8    # "offset":I
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_6
    if-gez p3, :cond_d

    .line 4303
    if-eqz p2, :cond_9

    .line 4305
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_8

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    move-result v7

    .line 4306
    .local v7, "newSelectedStart":I
    :goto_6
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v7, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    .line 4313
    .end local v7    # "newSelectedStart":I
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_a

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v14

    .line 4314
    .restart local v14    # "selectedStart":I
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_b

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v12

    .line 4317
    .restart local v12    # "selectedEnd":I
    :goto_9
    move/from16 v0, p4

    if-ge v14, v0, :cond_7

    .line 4319
    sub-int v16, p4, v14

    .line 4322
    .restart local v16    # "spaceBefore":I
    sub-int v15, p5, v12

    .line 4325
    .restart local v15    # "spaceAfter":I
    sub-int v17, p5, p4

    div-int/lit8 v4, v17, 0x2

    .line 4326
    .restart local v4    # "halfSpace":I
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 4327
    .restart local v8    # "offset":I
    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 4329
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_c

    .line 4330
    invoke-virtual {v11, v8}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4337
    .end local v4    # "halfSpace":I
    .end local v8    # "offset":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_7
    :goto_a
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    goto/16 :goto_5

    .line 4305
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLeft()I

    move-result v7

    goto :goto_6

    .line 4310
    :cond_9
    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v10, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    goto :goto_7

    .line 4313
    :cond_a
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v14

    goto :goto_8

    .line 4314
    .restart local v14    # "selectedStart":I
    :cond_b
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v12

    goto :goto_9

    .line 4332
    .restart local v4    # "halfSpace":I
    .restart local v8    # "offset":I
    .restart local v12    # "selectedEnd":I
    .restart local v15    # "spaceAfter":I
    .restart local v16    # "spaceBefore":I
    :cond_c
    invoke-virtual {v11, v8}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_a

    .line 4343
    .end local v4    # "halfSpace":I
    .end local v8    # "offset":I
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    .end local v15    # "spaceAfter":I
    .end local v16    # "spaceBefore":I
    :cond_d
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v10, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->makeAndAddView(IIZZ)Landroid/view/View;

    move-result-object v11

    .line 4345
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v14

    .line 4346
    .restart local v14    # "selectedStart":I
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_10

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v12

    .line 4349
    .restart local v12    # "selectedEnd":I
    :goto_c
    move/from16 v0, p4

    if-ge v10, v0, :cond_e

    .line 4352
    move v6, v12

    .line 4353
    .local v6, "newEnd":I
    add-int/lit8 v17, p4, 0x14

    move/from16 v0, v17

    if-ge v6, v0, :cond_e

    .line 4355
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_11

    .line 4356
    sub-int v17, p4, v14

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4364
    .end local v6    # "newEnd":I
    :cond_e
    :goto_d
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    goto/16 :goto_5

    .line 4345
    .end local v12    # "selectedEnd":I
    .end local v14    # "selectedStart":I
    :cond_f
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v14

    goto :goto_b

    .line 4346
    .restart local v14    # "selectedStart":I
    :cond_10
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v12

    goto :goto_c

    .line 4358
    .restart local v6    # "newEnd":I
    .restart local v12    # "selectedEnd":I
    :cond_11
    sub-int v17, p4, v14

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_d
.end method

.method private obtainView(I[Z)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "isScrap"    # [Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5453
    aput-boolean v4, p2, v4

    .line 5455
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->getTransientStateView(I)Landroid/view/View;

    move-result-object v2

    .line 5456
    .local v2, "scrapView":Landroid/view/View;
    if-eqz v2, :cond_1

    move-object v0, v2

    .line 5504
    :cond_0
    :goto_0
    return-object v0

    .line 5460
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v2

    .line 5463
    if-eqz v2, :cond_8

    .line 5464
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 5465
    .local v0, "child":Landroid/view/View;
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTTSPosition:I

    if-eq v3, p1, :cond_2

    .line 5466
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->clearAccessibilityFocus(Landroid/view/View;)V

    .line 5468
    :cond_2
    if-eq v0, v2, :cond_7

    .line 5469
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v3, v2, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 5477
    :goto_1
    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->getImportantForAccessibility(Landroid/view/View;)I

    move-result v3

    if-nez v3, :cond_3

    .line 5478
    invoke-static {v0, v5}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 5481
    :cond_3
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z

    if-eqz v3, :cond_5

    .line 5482
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .line 5484
    .local v1, "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    if-nez v1, :cond_9

    .line 5485
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->generateDefaultLayoutParams()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    move-result-object v1

    .line 5490
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->id:J

    .line 5492
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5495
    .end local v1    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    :cond_5
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAccessibilityDelegate:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;

    if-nez v3, :cond_6

    .line 5496
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;

    invoke-direct {v3, p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAccessibilityDelegate:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;

    .line 5499
    :cond_6
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAccessibilityDelegate:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ListItemAccessibilityDelegate;

    invoke-static {v0, v3}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 5500
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTTSPosition:I

    if-ne v3, p1, :cond_0

    .line 5502
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestAccessibilityFocus(Landroid/view/View;)V

    goto :goto_0

    .line 5471
    :cond_7
    aput-boolean v5, p2, v4

    goto :goto_1

    .line 5474
    .end local v0    # "child":Landroid/view/View;
    :cond_8
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p1, v6, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "child":Landroid/view/View;
    goto :goto_1

    .line 5486
    .restart local v1    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    :cond_9
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 5487
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    move-result-object v1

    goto :goto_2
.end method

.method private offsetChildren(I)V
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 4202
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 4204
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 4205
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4207
    .local v0, "child":Landroid/view/View;
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v3, :cond_0

    .line 4208
    invoke-virtual {v0, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 4204
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4210
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_1

    .line 4213
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private overScrollByInternal(IIIIIIIIZ)Z
    .locals 2
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I
    .param p3, "scrollX"    # I
    .param p4, "scrollY"    # I
    .param p5, "scrollRangeX"    # I
    .param p6, "scrollRangeY"    # I
    .param p7, "maxOverScrollX"    # I
    .param p8, "maxOverScrollY"    # I
    .param p9, "isTouchEvent"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 1077
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 1078
    const/4 v0, 0x0

    .line 1081
    :goto_0
    return v0

    :cond_0
    invoke-super/range {p0 .. p9}, Landroid/widget/AdapterView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    goto :goto_0
.end method

.method private performAccessibilityActionsOnSelected()V
    .locals 2

    .prologue
    .line 3528
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedItemPosition()I

    move-result v0

    .line 3529
    .local v0, "position":I
    if-ltz v0, :cond_0

    .line 3531
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->sendAccessibilityEvent(I)V

    .line 3533
    :cond_0
    return-void
.end method

.method private performLongPress(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "longPressPosition"    # I
    .param p3, "longPressId"    # J

    .prologue
    .line 5645
    const/4 v6, 0x0

    .line 5647
    .local v6, "handled":Z
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    .line 5648
    .local v0, "listener":Landroid/widget/AdapterView$OnItemLongClickListener;
    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    .line 5649
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 5653
    :cond_0
    if-nez v6, :cond_1

    .line 5654
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 5655
    invoke-super {p0, p0}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 5658
    :cond_1
    if-eqz v6, :cond_2

    .line 5659
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->performHapticFeedback(I)Z

    .line 5662
    :cond_2
    return v6
.end method

.method private positionOfNewFocus(Landroid/view/View;)I
    .locals 5
    .param p1, "newFocus"    # Landroid/view/View;

    .prologue
    .line 2201
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v2

    .line 2203
    .local v2, "numChildren":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2204
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2205
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2206
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v3, v1

    return v3

    .line 2203
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2210
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "newFocus is not a child of any of the children of the list!"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private positionSelector(ILandroid/view/View;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "selected"    # Landroid/view/View;

    .prologue
    const/4 v6, -0x1

    .line 3341
    if-eq p1, v6, :cond_0

    .line 3342
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    .line 3345
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 3348
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsChildViewEnabled:Z

    .line 3349
    .local v0, "isChildViewEnabled":Z
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eq v1, v0, :cond_1

    .line 3350
    if-nez v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsChildViewEnabled:Z

    .line 3352
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedItemPosition()I

    move-result v1

    if-eq v1, v6, :cond_1

    .line 3353
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->refreshDrawableState()V

    .line 3356
    :cond_1
    return-void

    .line 3350
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private reconcileSelectedPosition()I
    .locals 2

    .prologue
    .line 4521
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 4522
    .local v0, "position":I
    if-gez v0, :cond_0

    .line 4523
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 4526
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 4527
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4529
    return v0
.end method

.method private recycleVelocityTracker()V
    .locals 1

    .prologue
    .line 2710
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2711
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2712
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2714
    :cond_0
    return-void
.end method

.method private relayoutMeasuredChild(Landroid/view/View;)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 4660
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 4661
    .local v5, "w":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 4663
    .local v4, "h":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v1

    .line 4664
    .local v1, "childLeft":I
    add-int v2, v1, v5

    .line 4665
    .local v2, "childRight":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4666
    .local v3, "childTop":I
    add-int v0, v3, v4

    .line 4668
    .local v0, "childBottom":I
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/view/View;->layout(IIII)V

    .line 4669
    return-void
.end method

.method private rememberSyncState()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5531
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 5567
    :goto_0
    return-void

    .line 5535
    :cond_0
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 5537
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ltz v2, :cond_3

    .line 5538
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5540
    .local v1, "child":Landroid/view/View;
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 5541
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 5543
    if-eqz v1, :cond_1

    .line 5544
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    :goto_1
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 5547
    :cond_1
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncMode:I

    goto :goto_0

    .line 5544
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    goto :goto_1

    .line 5550
    .end local v1    # "child":Landroid/view/View;
    :cond_3
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5551
    .restart local v1    # "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 5553
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-ltz v2, :cond_5

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 5554
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 5559
    :goto_2
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 5561
    if-eqz v1, :cond_4

    .line 5562
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    :goto_3
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 5565
    :cond_4
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncMode:I

    goto :goto_0

    .line 5556
    :cond_5
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    goto :goto_2

    .line 5562
    :cond_6
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    goto :goto_3
.end method

.method private reportScrollStateChange(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 2729
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastScrollState:I

    if-ne p1, v0, :cond_1

    .line 2737
    :cond_0
    :goto_0
    return-void

    .line 2733
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2734
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastScrollState:I

    .line 2735
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    invoke-interface {v0, p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;->onScrollStateChanged(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;I)V

    goto :goto_0
.end method

.method private scrollListItemsBy(I)Z
    .locals 32
    .param p1, "incrementalDelta"    # I

    .prologue
    .line 3064
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v5

    .line 3065
    .local v5, "childCount":I
    if-nez v5, :cond_0

    .line 3066
    const/16 v30, 0x1

    .line 3194
    :goto_0
    return v30

    .line 3069
    :cond_0
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 3070
    .local v14, "first":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_a

    invoke-virtual {v14}, Landroid/view/View;->getTop()I

    move-result v16

    .line 3072
    .local v16, "firstStart":I
    :goto_1
    add-int/lit8 v30, v5, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 3073
    .local v19, "last":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_b

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getBottom()I

    move-result v20

    .line 3075
    .local v20, "lastEnd":I
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v25

    .line 3076
    .local v25, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v21

    .line 3077
    .local v21, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v22

    .line 3078
    .local v22, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v23

    .line 3081
    .local v23, "paddingRight":I
    const/16 v24, 0x0

    .line 3083
    .local v24, "paddingStart":I
    rsub-int/lit8 v28, v16, 0x0

    .line 3085
    .local v28, "spaceBefore":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v13

    .line 3087
    .local v13, "end":I
    :goto_3
    sub-int v27, v20, v13

    .line 3090
    .local v27, "spaceAfter":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_d

    .line 3091
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v30

    sub-int v30, v30, v21

    sub-int v26, v30, v25

    .line 3096
    .local v26, "size":I
    :goto_4
    if-gez p1, :cond_e

    .line 3097
    add-int/lit8 v30, v26, -0x1

    move/from16 v0, v30

    neg-int v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 3102
    :goto_5
    move-object/from16 v0, p0

    iget v15, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 3104
    .local v15, "firstPosition":I
    if-nez v15, :cond_f

    if-ltz v16, :cond_f

    if-ltz p1, :cond_f

    const/16 v30, 0x1

    :goto_6
    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollDown:Z

    .line 3106
    add-int v30, v15, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    move/from16 v31, v0

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_10

    move/from16 v0, v20

    if-gt v0, v13, :cond_10

    if-gtz p1, :cond_10

    const/16 v30, 0x1

    :goto_7
    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollUp:Z

    .line 3109
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollDown:Z

    move/from16 v30, v0

    if-nez v30, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCannotScrollUp:Z

    move/from16 v30, v0

    if-eqz v30, :cond_1

    .line 3113
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v18

    .line 3114
    .local v18, "inTouchMode":Z
    if-eqz v18, :cond_2

    .line 3115
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hideSelector()V

    .line 3118
    :cond_2
    const/16 v29, 0x0

    .line 3119
    .local v29, "start":I
    const/4 v11, 0x0

    .line 3121
    .local v11, "count":I
    if-gez p1, :cond_11

    const/4 v12, 0x1

    .line 3122
    .local v12, "down":Z
    :goto_8
    if-eqz v12, :cond_14

    .line 3123
    move/from16 v0, p1

    neg-int v0, v0

    move/from16 v30, v0

    add-int/lit8 v10, v30, 0x0

    .line 3125
    .local v10, "childrenStart":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_9
    move/from16 v0, v17

    if-ge v0, v5, :cond_3

    .line 3126
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3127
    .local v4, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_12

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v6

    .line 3129
    .local v6, "childEnd":I
    :goto_a
    if-lt v6, v10, :cond_13

    .line 3153
    .end local v4    # "child":Landroid/view/View;
    .end local v6    # "childEnd":I
    .end local v10    # "childrenStart":I
    :cond_3
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 3155
    if-lez v11, :cond_4

    .line 3156
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1, v11}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->detachViewsFromParent(II)V

    .line 3161
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    move-result v30

    if-nez v30, :cond_5

    .line 3162
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    .line 3165
    :cond_5
    invoke-direct/range {p0 .. p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetChildren(I)V

    .line 3167
    if-eqz v12, :cond_6

    .line 3168
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v30, v0

    add-int v30, v30, v11

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 3171
    :cond_6
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 3172
    .local v3, "absIncrementalDelta":I
    move/from16 v0, v28

    if-lt v0, v3, :cond_7

    move/from16 v0, v27

    if-ge v0, v3, :cond_8

    .line 3173
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillGap(Z)V

    .line 3176
    :cond_8
    if-nez v18, :cond_16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v30, v0

    const/16 v31, -0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_16

    .line 3177
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v31, v0

    sub-int v7, v30, v31

    .line 3178
    .local v7, "childIndex":I
    if-ltz v7, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v30

    move/from16 v0, v30

    if-ge v7, v0, :cond_9

    .line 3179
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    .line 3190
    .end local v7    # "childIndex":I
    :cond_9
    :goto_b
    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    .line 3192
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    .line 3194
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 3070
    .end local v3    # "absIncrementalDelta":I
    .end local v11    # "count":I
    .end local v12    # "down":Z
    .end local v13    # "end":I
    .end local v15    # "firstPosition":I
    .end local v16    # "firstStart":I
    .end local v17    # "i":I
    .end local v18    # "inTouchMode":Z
    .end local v19    # "last":Landroid/view/View;
    .end local v20    # "lastEnd":I
    .end local v21    # "paddingBottom":I
    .end local v22    # "paddingLeft":I
    .end local v23    # "paddingRight":I
    .end local v24    # "paddingStart":I
    .end local v25    # "paddingTop":I
    .end local v26    # "size":I
    .end local v27    # "spaceAfter":I
    .end local v28    # "spaceBefore":I
    .end local v29    # "start":I
    :cond_a
    invoke-virtual {v14}, Landroid/view/View;->getLeft()I

    move-result v16

    goto/16 :goto_1

    .line 3073
    .restart local v16    # "firstStart":I
    .restart local v19    # "last":Landroid/view/View;
    :cond_b
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getRight()I

    move-result v20

    goto/16 :goto_2

    .line 3085
    .restart local v20    # "lastEnd":I
    .restart local v21    # "paddingBottom":I
    .restart local v22    # "paddingLeft":I
    .restart local v23    # "paddingRight":I
    .restart local v24    # "paddingStart":I
    .restart local v25    # "paddingTop":I
    .restart local v28    # "spaceBefore":I
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v13

    goto/16 :goto_3

    .line 3093
    .restart local v13    # "end":I
    .restart local v27    # "spaceAfter":I
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v30

    sub-int v30, v30, v23

    sub-int v26, v30, v22

    .restart local v26    # "size":I
    goto/16 :goto_4

    .line 3099
    :cond_e
    add-int/lit8 v30, v26, -0x1

    move/from16 v0, v30

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto/16 :goto_5

    .line 3104
    .restart local v15    # "firstPosition":I
    :cond_f
    const/16 v30, 0x0

    goto/16 :goto_6

    .line 3106
    :cond_10
    const/16 v30, 0x0

    goto/16 :goto_7

    .line 3121
    .restart local v11    # "count":I
    .restart local v18    # "inTouchMode":Z
    .restart local v29    # "start":I
    :cond_11
    const/4 v12, 0x0

    goto/16 :goto_8

    .line 3127
    .restart local v4    # "child":Landroid/view/View;
    .restart local v10    # "childrenStart":I
    .restart local v12    # "down":Z
    .restart local v17    # "i":I
    :cond_12
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v6

    goto/16 :goto_a

    .line 3133
    .restart local v6    # "childEnd":I
    :cond_13
    add-int/lit8 v11, v11, 0x1

    .line 3134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    move-object/from16 v30, v0

    add-int v31, v15, v17

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3125
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_9

    .line 3137
    .end local v4    # "child":Landroid/view/View;
    .end local v6    # "childEnd":I
    .end local v10    # "childrenStart":I
    .end local v17    # "i":I
    :cond_14
    sub-int v9, v13, p1

    .line 3139
    .local v9, "childrenEnd":I
    add-int/lit8 v17, v5, -0x1

    .restart local v17    # "i":I
    :goto_c
    if-ltz v17, :cond_3

    .line 3140
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3141
    .restart local v4    # "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v30, v0

    if-eqz v30, :cond_15

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v8

    .line 3143
    .local v8, "childStart":I
    :goto_d
    if-le v8, v9, :cond_3

    .line 3147
    move/from16 v29, v17

    .line 3148
    add-int/lit8 v11, v11, 0x1

    .line 3149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    move-object/from16 v30, v0

    add-int v31, v15, v17

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3139
    add-int/lit8 v17, v17, -0x1

    goto :goto_c

    .line 3141
    .end local v8    # "childStart":I
    :cond_15
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v8

    goto :goto_d

    .line 3181
    .end local v4    # "child":Landroid/view/View;
    .end local v9    # "childrenEnd":I
    .restart local v3    # "absIncrementalDelta":I
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    move/from16 v30, v0

    const/16 v31, -0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_17

    .line 3182
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    move/from16 v31, v0

    sub-int v7, v30, v31

    .line 3183
    .restart local v7    # "childIndex":I
    if-ltz v7, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v30

    move/from16 v0, v30

    if-ge v7, v0, :cond_9

    .line 3184
    const/16 v30, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    goto/16 :goto_b

    .line 3187
    .end local v7    # "childIndex":I
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_b
.end method

.method private selectionChanged()V
    .locals 3

    .prologue
    .line 3490
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    .line 3491
    .local v0, "listener":Landroid/widget/AdapterView$OnItemSelectedListener;
    if-nez v0, :cond_0

    .line 3509
    :goto_0
    return-void

    .line 3495
    :cond_0
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    if-eqz v1, :cond_3

    .line 3500
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectionNotifier:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;

    if-nez v1, :cond_2

    .line 3501
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectionNotifier:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;

    .line 3504
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectionNotifier:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 3506
    :cond_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fireOnSelected()V

    .line 3507
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->performAccessibilityActionsOnSelected()V

    goto :goto_0
.end method

.method private setNextSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 3402
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    .line 3403
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    .line 3406
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncMode:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 3407
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 3408
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 3410
    :cond_0
    return-void
.end method

.method private setSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 3377
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 3378
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedRowId:J

    .line 3379
    return-void
.end method

.method private setSelectionInt(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 3382
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 3383
    const/4 v0, 0x0

    .line 3385
    .local v0, "awakeScrollbars":Z
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    .line 3386
    .local v1, "selectedPosition":I
    if-ltz v1, :cond_0

    .line 3387
    add-int/lit8 v2, v1, -0x1

    if-ne p1, v2, :cond_2

    .line 3388
    const/4 v0, 0x1

    .line 3394
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    .line 3396
    if-eqz v0, :cond_1

    .line 3397
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    .line 3399
    :cond_1
    return-void

    .line 3389
    :cond_2
    add-int/lit8 v2, v1, 0x1

    if-ne p1, v2, :cond_0

    .line 3390
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setupChild(Landroid/view/View;IIIZZZ)V
    .locals 19
    .param p1, "child"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "top"    # I
    .param p4, "left"    # I
    .param p5, "flow"    # Z
    .param p6, "selected"    # Z
    .param p7, "recycled"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 4912
    if-eqz p6, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->shouldShowSelector()Z

    move-result v17

    if-eqz v17, :cond_5

    const/4 v10, 0x1

    .line 4913
    .local v10, "isSelected":Z
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    move-result v17

    move/from16 v0, v17

    if-eq v10, v0, :cond_6

    const/4 v15, 0x1

    .line 4914
    .local v15, "updateChildSelected":Z
    :goto_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 4916
    .local v13, "touchMode":I
    if-lez v13, :cond_7

    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v13, v0, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    move/from16 v17, v0

    move/from16 v0, v17

    move/from16 v1, p2

    if-ne v0, v1, :cond_7

    const/4 v9, 0x1

    .line 4919
    .local v9, "isPressed":Z
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isPressed()Z

    move-result v17

    move/from16 v0, v17

    if-eq v9, v0, :cond_8

    const/4 v14, 0x1

    .line 4920
    .local v14, "updateChildPressed":Z
    :goto_3
    if-eqz p7, :cond_0

    if-nez v15, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v17

    if-eqz v17, :cond_9

    :cond_0
    const/4 v12, 0x1

    .line 4923
    .local v12, "needToMeasure":Z
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    .line 4924
    .local v11, "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    if-nez v11, :cond_1

    .line 4925
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->generateDefaultLayoutParams()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    move-result-object v11

    .line 4928
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v17

    move/from16 v0, v17

    iput v0, v11, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->viewType:I

    .line 4930
    if-eqz p7, :cond_b

    iget-boolean v0, v11, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->forceAdd:Z

    move/from16 v17, v0

    if-nez v17, :cond_b

    .line 4931
    if-eqz p5, :cond_a

    const/16 v17, -0x1

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v11}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 4937
    :goto_6
    if-eqz v15, :cond_2

    .line 4938
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setSelected(Z)V

    .line 4941
    :cond_2
    if-eqz v14, :cond_3

    .line 4942
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->setPressed(Z)V

    .line 4945
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    move-object/from16 v17, v0

    sget-object v18, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual/range {v17 .. v18}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 4946
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/widget/Checkable;

    move/from16 v17, v0

    if-eqz v17, :cond_d

    move-object/from16 v17, p1

    .line 4947
    check-cast v17, Landroid/widget/Checkable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    invoke-interface/range {v17 .. v18}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 4954
    :cond_4
    :goto_7
    if-eqz v12, :cond_e

    .line 4955
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureChild(Landroid/view/View;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;)V

    .line 4960
    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v16

    .line 4961
    .local v16, "w":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 4963
    .local v8, "h":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    if-nez p5, :cond_f

    sub-int v7, p3, v8

    .line 4964
    .local v7, "childTop":I
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    move/from16 v17, v0

    if-nez v17, :cond_10

    if-nez p5, :cond_10

    sub-int v5, p4, v16

    .line 4966
    .local v5, "childLeft":I
    :goto_a
    if-eqz v12, :cond_11

    .line 4967
    add-int v6, v5, v16

    .line 4968
    .local v6, "childRight":I
    add-int v4, v7, v8

    .line 4970
    .local v4, "childBottom":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v6, v4}, Landroid/view/View;->layout(IIII)V

    .line 4975
    .end local v4    # "childBottom":I
    .end local v6    # "childRight":I
    :goto_b
    return-void

    .line 4912
    .end local v5    # "childLeft":I
    .end local v7    # "childTop":I
    .end local v8    # "h":I
    .end local v9    # "isPressed":Z
    .end local v10    # "isSelected":Z
    .end local v11    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .end local v12    # "needToMeasure":Z
    .end local v13    # "touchMode":I
    .end local v14    # "updateChildPressed":Z
    .end local v15    # "updateChildSelected":Z
    .end local v16    # "w":I
    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 4913
    .restart local v10    # "isSelected":Z
    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 4916
    .restart local v13    # "touchMode":I
    .restart local v15    # "updateChildSelected":Z
    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 4919
    .restart local v9    # "isPressed":Z
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 4920
    .restart local v14    # "updateChildPressed":Z
    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_4

    .line 4931
    .restart local v11    # "lp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .restart local v12    # "needToMeasure":Z
    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_5

    .line 4933
    :cond_b
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v11, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;->forceAdd:Z

    .line 4934
    if-eqz p5, :cond_c

    const/16 v17, -0x1

    :goto_c
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v11, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto/16 :goto_6

    :cond_c
    const/16 v17, 0x0

    goto :goto_c

    .line 4948
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    move/from16 v17, v0

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_4

    .line 4950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    goto/16 :goto_7

    .line 4957
    :cond_e
    invoke-virtual/range {p0 .. p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cleanupLayoutState(Landroid/view/View;)V

    goto/16 :goto_8

    .restart local v8    # "h":I
    .restart local v16    # "w":I
    :cond_f
    move/from16 v7, p3

    .line 4963
    goto :goto_9

    .restart local v7    # "childTop":I
    :cond_10
    move/from16 v5, p4

    .line 4964
    goto :goto_a

    .line 4972
    .restart local v5    # "childLeft":I
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v17

    sub-int v17, v5, v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 4973
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v17

    sub-int v17, v7, v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto/16 :goto_b
.end method

.method private shouldShowSelector()Z
    .locals 1

    .prologue
    .line 3337
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->touchModeDrawsInPressedState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private touchModeDrawsInPressedState()Z
    .locals 1

    .prologue
    .line 3413
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    packed-switch v0, :pswitch_data_0

    .line 3418
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 3416
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 3413
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private triggerCheckForLongPress()V
    .locals 4

    .prologue
    .line 3043
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    if-nez v0, :cond_0

    .line 3044
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    .line 3047
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;->rememberWindowAttachCount()V

    .line 3049
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForLongPress:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3051
    return-void
.end method

.method private triggerCheckForTap()V
    .locals 4

    .prologue
    .line 3027
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForTap:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

    if-nez v0, :cond_0

    .line 3028
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForTap:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

    .line 3031
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingCheckForTap:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$CheckForTap;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3032
    return-void
.end method

.method private updateEmptyStatus()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 6247
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 6249
    .local v0, "isEmpty":Z
    :goto_0
    if-eqz v0, :cond_4

    .line 6250
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 6251
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6252
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    .line 6262
    :goto_1
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-eqz v1, :cond_1

    .line 6263
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getBottom()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layout(IIII)V

    .line 6272
    :cond_1
    :goto_2
    return-void

    .end local v0    # "isEmpty":Z
    :cond_2
    move v0, v1

    .line 6247
    goto :goto_0

    .line 6256
    .restart local v0    # "isEmpty":Z
    :cond_3
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    goto :goto_1

    .line 6266
    :cond_4
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEmptyView:Landroid/view/View;

    if-eqz v2, :cond_5

    .line 6267
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 6270
    :cond_5
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    goto :goto_2
.end method

.method private updateOnScreenCheckedViews()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 5575
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5576
    .local v2, "firstPos":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 5578
    .local v1, "count":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v7, 0xb

    if-lt v6, v7, :cond_1

    const/4 v5, 0x1

    .line 5581
    .local v5, "useActivated":Z
    :goto_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_3

    .line 5582
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5583
    .local v0, "child":Landroid/view/View;
    add-int v4, v2, v3

    .line 5585
    .local v4, "position":I
    instance-of v6, v0, Landroid/widget/Checkable;

    if-eqz v6, :cond_2

    .line 5586
    check-cast v0, Landroid/widget/Checkable;

    .end local v0    # "child":Landroid/view/View;
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    invoke-interface {v0, v6}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 5581
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 5578
    .end local v3    # "i":I
    .end local v4    # "position":I
    .end local v5    # "useActivated":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 5587
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    .restart local v4    # "position":I
    .restart local v5    # "useActivated":Z
    :cond_2
    if-eqz v5, :cond_0

    .line 5588
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/view/View;->setActivated(Z)V

    goto :goto_2

    .line 5591
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "position":I
    :cond_3
    return-void
.end method

.method private updateOverScrollState(II)V
    .locals 12
    .param p1, "delta"    # I
    .param p2, "overscroll"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 2818
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_4

    move v1, v5

    :goto_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_5

    move v2, p2

    :goto_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_6

    move v3, v5

    :goto_2
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_7

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    :goto_3
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_8

    move v7, v5

    :goto_4
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_9

    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverscrollDistance:I

    :goto_5
    move-object v0, p0

    move v6, v5

    invoke-direct/range {v0 .. v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->overScrollByInternal(IIIIIIIIZ)Z

    .line 2827
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverscrollDistance:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2829
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2830
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2834
    :cond_0
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v10

    .line 2835
    .local v10, "overscrollMode":I
    if-eqz v10, :cond_1

    if-ne v10, v9, :cond_3

    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->contentFits()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2837
    :cond_1
    const/4 v0, 0x5

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 2839
    int-to-float v1, p2

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v0

    :goto_6
    int-to-float v0, v0

    div-float v11, v1, v0

    .line 2840
    .local v11, "pull":F
    if-lez p1, :cond_b

    .line 2841
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, v11}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    .line 2843
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2844
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    .line 2854
    :cond_2
    :goto_7
    if-eqz p1, :cond_3

    .line 2855
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 2858
    .end local v11    # "pull":F
    :cond_3
    return-void

    .end local v10    # "overscrollMode":I
    :cond_4
    move v1, p2

    .line 2818
    goto :goto_0

    :cond_5
    move v2, v5

    goto :goto_1

    :cond_6
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    goto :goto_2

    :cond_7
    move v4, v5

    goto :goto_3

    :cond_8
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverscrollDistance:I

    goto :goto_4

    :cond_9
    move v8, v5

    goto :goto_5

    .line 2839
    .restart local v10    # "overscrollMode":I
    :cond_a
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v0

    goto :goto_6

    .line 2846
    .restart local v11    # "pull":F
    :cond_b
    if-gez p1, :cond_2

    .line 2847
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0, v11}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    .line 2849
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2850
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    goto :goto_7
.end method

.method private updateScrollbarsDirection()V
    .locals 1

    .prologue
    .line 3020
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScrollbarOrientation:I

    if-eqz v0, :cond_0

    .line 3021
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setHorizontalScrollBarEnabled(Z)V

    .line 3022
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVerticalScrollBarEnabled(Z)V

    .line 3024
    :cond_0
    return-void

    .line 3021
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSelectorState()V
    .locals 2

    .prologue
    .line 3472
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 3473
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->shouldShowSelector()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3474
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 3479
    :cond_0
    :goto_0
    return-void

    .line 3476
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->STATE_NOTHING:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method

.method private useDefaultSelector()V
    .locals 2

    .prologue
    .line 3332
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 3334
    return-void
.end method


# virtual methods
.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 3676
    invoke-super {p0}, Landroid/widget/AdapterView;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 5681
    instance-of v0, p1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    return v0
.end method

.method public clearAccessibilityFocus(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 6755
    const-string v2, "FrameListView"

    const-string v3, "clearAccessibilityFocus in  "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6756
    if-nez p1, :cond_0

    .line 6772
    :goto_0
    return-void

    .line 6760
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "clearAccessibilityFocus"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 6763
    .local v0, "clearAccessibilityFocus":Ljava/lang/reflect/Method;
    const-string v2, "FrameListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearAccessibilityFocus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6765
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6766
    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6771
    .end local v0    # "clearAccessibilityFocus":Ljava/lang/reflect/Method;
    :goto_1
    const-string v2, "FrameListView"

    const-string v3, "clearAccessibilityFocus out  "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6767
    :catch_0
    move-exception v1

    .line 6768
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public clearChoices()V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 750
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v0, :cond_1

    .line 751
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 754
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 755
    return-void
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1159
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v4

    .line 1160
    .local v4, "count":I
    if-nez v4, :cond_1

    move v5, v6

    .line 1182
    :cond_0
    :goto_0
    return v5

    .line 1164
    :cond_1
    mul-int/lit8 v5, v4, 0x64

    .line 1166
    .local v5, "extent":I
    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1167
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1169
    .local v1, "childLeft":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1170
    .local v3, "childWidth":I
    if-lez v3, :cond_2

    .line 1171
    mul-int/lit8 v6, v1, 0x64

    div-int/2addr v6, v3

    add-int/2addr v5, v6

    .line 1174
    :cond_2
    add-int/lit8 v6, v4, -0x1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1175
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    .line 1177
    .local v2, "childRight":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1178
    if-lez v3, :cond_0

    .line 1179
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v6

    sub-int v6, v2, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v3

    sub-int/2addr v5, v6

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1207
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 1208
    .local v4, "firstPosition":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 1210
    .local v1, "childCount":I
    if-ltz v4, :cond_0

    if-nez v1, :cond_1

    .line 1222
    :cond_0
    :goto_0
    return v5

    .line 1214
    :cond_1
    invoke-virtual {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1215
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1217
    .local v2, "childLeft":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1218
    .local v3, "childWidth":I
    if-lez v3, :cond_0

    .line 1219
    mul-int/lit8 v6, v4, 0x64

    mul-int/lit8 v7, v2, 0x64

    div-int/2addr v7, v3

    sub-int/2addr v6, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 1239
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1241
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eqz v1, :cond_0

    .line 1243
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1246
    :cond_0
    return v0
.end method

.method public computeScroll()V
    .locals 8

    .prologue
    .line 3217
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v6

    if-nez v6, :cond_0

    .line 3256
    :goto_0
    return-void

    .line 3222
    :cond_0
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v6, :cond_1

    .line 3223
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    .line 3228
    .local v4, "pos":I
    :goto_1
    int-to-float v6, v4

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    sub-float/2addr v6, v7

    float-to-int v0, v6

    .line 3229
    .local v0, "diff":I
    int-to-float v6, v4

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    .line 3231
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    move-result v5

    .line 3233
    .local v5, "stopped":Z
    if-nez v5, :cond_2

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    .line 3234
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0

    .line 3225
    .end local v0    # "diff":I
    .end local v4    # "pos":I
    .end local v5    # "stopped":Z
    :cond_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v4

    .restart local v4    # "pos":I
    goto :goto_1

    .line 3236
    .restart local v0    # "diff":I
    .restart local v5    # "stopped":Z
    :cond_2
    if-eqz v5, :cond_4

    .line 3237
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v3

    .line 3238
    .local v3, "overScrollMode":I
    const/4 v6, 0x2

    if-eq v3, v6, :cond_3

    .line 3239
    if-lez v0, :cond_5

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 3242
    .local v1, "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    :goto_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getCurrVelocity()F

    move-result v6

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    move-result v2

    .line 3245
    .local v2, "needsInvalidate":Z
    if-eqz v2, :cond_3

    .line 3246
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 3250
    .end local v1    # "edge":Landroid/support/v4/widget/EdgeEffectCompat;
    .end local v2    # "needsInvalidate":Z
    :cond_3
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    .line 3253
    .end local v3    # "overScrollMode":I
    :cond_4
    const/4 v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 3254
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    goto :goto_0

    .line 3239
    .restart local v3    # "overScrollMode":I
    :cond_5
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_2
.end method

.method protected computeVerticalScrollExtent()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1131
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v4

    .line 1132
    .local v4, "count":I
    if-nez v4, :cond_1

    move v5, v6

    .line 1154
    :cond_0
    :goto_0
    return v5

    .line 1136
    :cond_1
    mul-int/lit8 v5, v4, 0x64

    .line 1138
    .local v5, "extent":I
    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1139
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1141
    .local v3, "childTop":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1142
    .local v2, "childHeight":I
    if-lez v2, :cond_2

    .line 1143
    mul-int/lit8 v6, v3, 0x64

    div-int/2addr v6, v2

    add-int/2addr v5, v6

    .line 1146
    :cond_2
    add-int/lit8 v6, v4, -0x1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1147
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 1149
    .local v1, "childBottom":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1150
    if-lez v2, :cond_0

    .line 1151
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v6

    sub-int v6, v1, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v2

    sub-int/2addr v5, v6

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1187
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 1188
    .local v4, "firstPosition":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 1190
    .local v1, "childCount":I
    if-ltz v4, :cond_0

    if-nez v1, :cond_1

    .line 1202
    :cond_0
    :goto_0
    return v5

    .line 1194
    :cond_1
    invoke-virtual {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1195
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 1197
    .local v3, "childTop":I
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1198
    .local v2, "childHeight":I
    if-lez v2, :cond_0

    .line 1199
    mul-int/lit8 v6, v4, 0x64

    mul-int/lit8 v7, v3, 0x64

    div-int/2addr v7, v2

    sub-int/2addr v6, v7

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_0
.end method

.method protected computeVerticalScrollRange()I
    .locals 3

    .prologue
    .line 1227
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1229
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eqz v1, :cond_0

    .line 1231
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1234
    :cond_0
    return v0
.end method

.method confirmCheckedPositionsById()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 4372
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12}, Landroid/util/SparseBooleanArray;->clear()V

    .line 4374
    const/4 v0, 0x0

    .local v0, "checkedIndex":I
    :goto_0
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v12

    if-ge v0, v12, :cond_4

    .line 4375
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v0}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 4376
    .local v4, "id":J
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v0}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 4378
    .local v3, "lastPos":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v12, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v6

    .line 4379
    .local v6, "lastPosId":J
    cmp-long v12, v4, v6

    if-eqz v12, :cond_3

    .line 4381
    const/4 v12, 0x0

    add-int/lit8 v13, v3, -0x14

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 4382
    .local v11, "start":I
    add-int/lit8 v12, v3, 0x14

    iget v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4383
    .local v1, "end":I
    const/4 v2, 0x0

    .line 4385
    .local v2, "found":Z
    move v10, v11

    .local v10, "searchPos":I
    :goto_1
    if-ge v10, v1, :cond_0

    .line 4386
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v12, v10}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v8

    .line 4387
    .local v8, "searchId":J
    cmp-long v12, v4, v8

    if-nez v12, :cond_2

    .line 4388
    const/4 v2, 0x1

    .line 4389
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12, v10, v14}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 4390
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v12, v0, v13}, Landroid/support/v4/util/LongSparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 4395
    .end local v8    # "searchId":J
    :cond_0
    if-nez v2, :cond_1

    .line 4396
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v4, v5}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    .line 4397
    add-int/lit8 v0, v0, -0x1

    .line 4398
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    add-int/lit8 v12, v12, -0x1

    iput v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 4374
    .end local v1    # "end":I
    .end local v2    # "found":Z
    .end local v10    # "searchPos":I
    .end local v11    # "start":I
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4385
    .restart local v1    # "end":I
    .restart local v2    # "found":Z
    .restart local v8    # "searchId":J
    .restart local v10    # "searchPos":I
    .restart local v11    # "start":I
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 4401
    .end local v1    # "end":I
    .end local v2    # "found":Z
    .end local v8    # "searchId":J
    .end local v10    # "searchPos":I
    .end local v11    # "start":I
    :cond_3
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12, v3, v14}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_2

    .line 4404
    .end local v3    # "lastPos":I
    .end local v4    # "id":J
    .end local v6    # "lastPosId":J
    :cond_4
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3681
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDrawSelectorOnTop:Z

    .line 3682
    .local v0, "drawSelectorOnTop":Z
    if-nez v0, :cond_0

    .line 3683
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 3686
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 3688
    if-eqz v0, :cond_1

    .line 3689
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 3691
    :cond_1
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3772
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 3773
    .local v1, "handled":Z
    if-nez v1, :cond_0

    .line 3775
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 3776
    .local v0, "focused":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 3779
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {p0, v2, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 3783
    .end local v0    # "focused":Landroid/view/View;
    :cond_0
    return v1
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 3790
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3695
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->draw(Landroid/graphics/Canvas;)V

    .line 3697
    const/4 v0, 0x0

    .line 3699
    .local v0, "needsInvalidate":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v1, :cond_0

    .line 3700
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->drawStartEdge(Landroid/graphics/Canvas;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 3703
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v1, :cond_1

    .line 3704
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->drawEndEdge(Landroid/graphics/Canvas;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 3707
    :cond_1
    if-eqz v0, :cond_2

    .line 3708
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 3710
    :cond_2
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 3636
    invoke-super {p0}, Landroid/widget/AdapterView;->drawableStateChanged()V

    .line 3637
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    .line 3638
    return-void
.end method

.method fillGap(Z)V
    .locals 7
    .param p1, "down"    # Z

    .prologue
    const/4 v4, 0x0

    .line 4978
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v0

    .line 4980
    .local v0, "childCount":I
    if-eqz p1, :cond_2

    .line 4982
    const/4 v5, 0x0

    .line 4985
    .local v5, "paddingStart":I
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v6, :cond_1

    .line 4986
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 4991
    .local v3, "lastEnd":I
    :goto_0
    if-lez v0, :cond_0

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    add-int v4, v3, v6

    .line 4992
    .local v4, "offset":I
    :cond_0
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v6, v0

    invoke-direct {p0, v6, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillAfter(II)Landroid/view/View;

    .line 4993
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->correctTooHigh(I)V

    .line 5012
    .end local v3    # "lastEnd":I
    .end local v5    # "paddingStart":I
    :goto_1
    return-void

    .line 4988
    .end local v4    # "offset":I
    .restart local v5    # "paddingStart":I
    :cond_1
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v3

    .restart local v3    # "lastEnd":I
    goto :goto_0

    .line 4998
    .end local v3    # "lastEnd":I
    .end local v5    # "paddingStart":I
    :cond_2
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v6, :cond_3

    .line 5000
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v1

    .line 5001
    .local v1, "end":I
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v2

    .line 5008
    .local v2, "firstStart":I
    :goto_2
    if-lez v0, :cond_4

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    sub-int v4, v2, v6

    .line 5009
    .restart local v4    # "offset":I
    :goto_3
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/lit8 v6, v6, -0x1

    invoke-direct {p0, v6, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fillBefore(II)Landroid/view/View;

    .line 5010
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->correctTooLow(I)V

    goto :goto_1

    .line 5004
    .end local v1    # "end":I
    .end local v2    # "firstStart":I
    .end local v4    # "offset":I
    :cond_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v1

    .line 5005
    .restart local v1    # "end":I
    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v2

    .restart local v2    # "firstStart":I
    goto :goto_2

    :cond_4
    move v4, v1

    .line 5008
    goto :goto_3
.end method

.method fullScroll(I)Z
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x1

    .line 1871
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 1873
    const/4 v0, 0x0

    .line 1874
    .local v0, "moved":Z
    const/16 v2, 0x21

    if-eq p1, v2, :cond_0

    const/16 v2, 0x11

    if-ne p1, v2, :cond_4

    .line 1875
    :cond_0
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-eqz v2, :cond_2

    .line 1876
    const/4 v2, 0x0

    invoke-direct {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v1

    .line 1877
    .local v1, "position":I
    if-ltz v1, :cond_1

    .line 1878
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1879
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectionInt(I)V

    .line 1880
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    .line 1883
    :cond_1
    const/4 v0, 0x1

    .line 1898
    .end local v1    # "position":I
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1899
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    .line 1900
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    .line 1903
    :cond_3
    return v0

    .line 1885
    :cond_4
    const/16 v2, 0x82

    if-eq p1, v2, :cond_5

    const/16 v2, 0x42

    if-ne p1, v2, :cond_2

    .line 1886
    :cond_5
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    .line 1887
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-direct {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v1

    .line 1888
    .restart local v1    # "position":I
    if-ltz v1, :cond_6

    .line 1889
    const/4 v2, 0x3

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1890
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectionInt(I)V

    .line 1891
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    .line 1894
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->generateDefaultLayoutParams()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, -0x2

    .line 5667
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_0

    .line 5668
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    invoke-direct {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;-><init>(II)V

    .line 5670
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;-><init>(II)V

    goto :goto_0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 5686
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 5676
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getCheckedItemCount()I
    .locals 1

    .prologue
    .line 592
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    return v0
.end method

.method public getCheckedItemIds()[J
    .locals 6

    .prologue
    .line 658
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v4, :cond_2

    .line 660
    :cond_0
    const/4 v4, 0x0

    new-array v3, v4, [J

    .line 671
    :cond_1
    return-object v3

    .line 663
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 664
    .local v2, "idStates":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    invoke-virtual {v2}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    .line 665
    .local v0, "count":I
    new-array v3, v0, [J

    .line 667
    .local v3, "ids":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 668
    invoke-virtual {v2, v1}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCheckedItemPosition()I
    .locals 2

    .prologue
    .line 624
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->SINGLE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 626
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    .line 629
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 646
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChoiceMode()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    return-object v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 5691
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 865
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    return v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 855
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    return v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 898
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 900
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 903
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 904
    invoke-virtual {p0, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 908
    :goto_0
    return-void

    .line 906
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public getItemMargin()I
    .locals 1

    .prologue
    .line 458
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    return v0
.end method

.method public getItemsCanFocus()Z
    .locals 1

    .prologue
    .line 479
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 860
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getMaxScrollAmount()I
    .locals 3

    .prologue
    .line 2174
    const/4 v0, 0x0

    .line 2175
    .local v0, "offset":I
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v1, :cond_0

    .line 2176
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v0

    .line 2181
    :goto_0
    const v1, 0x3e99999a    # 0.3f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    return v1

    .line 2178
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getOrientation()Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;
    .locals 1

    .prologue
    .line 445
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->VERTICAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->HORIZONTAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    goto :goto_0
.end method

.method public getPositionForView(Landroid/view/View;)I
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, -0x1

    .line 870
    move-object v0, p1

    .line 871
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-nez v6, :cond_1

    .line 893
    :cond_0
    :goto_0
    return v5

    .line 876
    :cond_1
    :goto_1
    :try_start_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_2

    .line 877
    move-object v0, v4

    goto :goto_1

    .line 879
    .end local v4    # "v":Landroid/view/View;
    :catch_0
    move-exception v2

    .line 881
    .local v2, "e":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 885
    .end local v2    # "e":Ljava/lang/ClassCastException;
    .restart local v4    # "v":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 886
    .local v1, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v1, :cond_0

    .line 887
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 888
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v5, v3

    goto :goto_0

    .line 886
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public getSelectedItemId()J
    .locals 2

    .prologue
    .line 575
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedRowId:J

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 567
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNextSelectedPosition:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 3721
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-ltz v0, :cond_0

    .line 3722
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3724
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected initializeScrollbars(Landroid/content/res/TypedArray;)V
    .locals 7
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    .line 6819
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "initializeScrollbarsInternal"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/res/TypedArray;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 6823
    .local v1, "initializeScrollbarsInternal":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6824
    const-string v2, "FrameListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initializeScrollbars initializeScrollbarsInternal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6826
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6830
    .end local v1    # "initializeScrollbarsInternal":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 6827
    :catch_0
    move-exception v0

    .line 6828
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isItemChecked(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 607
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 611
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 975
    invoke-super {p0}, Landroid/widget/AdapterView;->onAttachedToWindow()V

    .line 977
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 978
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 980
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    if-nez v1, :cond_0

    .line 981
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    .line 982
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 985
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 986
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I

    .line 987
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 990
    :cond_0
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    .line 991
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 6
    .param p1, "extraSpace"    # I

    .prologue
    .line 3643
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsChildViewEnabled:Z

    if-eqz v4, :cond_1

    .line 3645
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 3671
    :cond_0
    :goto_0
    return-object v3

    .line 3651
    :cond_1
    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->ENABLED_STATE_SET:[I

    const/4 v5, 0x0

    aget v1, v4, v5

    .line 3656
    .local v1, "enabledState":I
    add-int/lit8 v4, p1, 0x1

    invoke-super {p0, v4}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 3657
    .local v3, "state":[I
    const/4 v0, -0x1

    .line 3658
    .local v0, "enabledPos":I
    array-length v4, v3

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_2

    .line 3659
    aget v4, v3, v2

    if-ne v4, v1, :cond_3

    .line 3660
    move v0, v2

    .line 3666
    :cond_2
    if-ltz v0, :cond_0

    .line 3667
    add-int/lit8 v4, v0, 0x1

    array-length v5, v3

    sub-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v4, v3, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 3658
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 995
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    .line 998
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->clear()V

    .line 1000
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1001
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1003
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 1004
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1005
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    .line 1008
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPerformClick:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

    if-eqz v1, :cond_1

    .line 1009
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPerformClick:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1012
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 1013
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1014
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1017
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    .line 1018
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 15
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 912
    invoke-super/range {p0 .. p3}, Landroid/widget/AdapterView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 914
    if-eqz p1, :cond_1

    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-gez v12, :cond_1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v12

    if-nez v12, :cond_1

    .line 915
    iget-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    if-nez v12, :cond_0

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v12, :cond_0

    .line 918
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 919
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    iput v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I

    .line 920
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v12}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    iput v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 923
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelection()Z

    .line 926
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 927
    .local v2, "adapter":Landroid/widget/ListAdapter;
    const/4 v5, -0x1

    .line 928
    .local v5, "closetChildIndex":I
    const/4 v4, 0x0

    .line 930
    .local v4, "closestChildStart":I
    if-eqz v2, :cond_6

    if-eqz p1, :cond_6

    if-eqz p3, :cond_6

    .line 931
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScrollX()I

    move-result v12

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScrollY()I

    move-result v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/graphics/Rect;->offset(II)V

    .line 935
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v13

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v13, v14

    if-ge v12, v13, :cond_2

    .line 936
    const/4 v12, 0x0

    iput v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 937
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    .line 942
    :cond_2
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTempRect:Landroid/graphics/Rect;

    .line 943
    .local v11, "otherRect":Landroid/graphics/Rect;
    const v9, 0x7fffffff

    .line 944
    .local v9, "minDistance":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v3

    .line 945
    .local v3, "childCount":I
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 947
    .local v7, "firstPosition":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v3, :cond_6

    .line 949
    add-int v12, v7, v8

    invoke-interface {v2, v12}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v12

    if-nez v12, :cond_4

    .line 947
    :cond_3
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 953
    :cond_4
    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 954
    .local v10, "other":Landroid/view/View;
    invoke-virtual {v10, v11}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 955
    invoke-virtual {p0, v10, v11}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 956
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-static {v0, v11, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    move-result v6

    .line 958
    .local v6, "distance":I
    if-ge v6, v9, :cond_3

    .line 959
    move v9, v6

    .line 960
    move v5, v8

    .line 961
    iget-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v12, :cond_5

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v4

    :goto_2
    goto :goto_1

    :cond_5
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_2

    .line 966
    .end local v3    # "childCount":I
    .end local v6    # "distance":I
    .end local v7    # "firstPosition":I
    .end local v8    # "i":I
    .end local v9    # "minDistance":I
    .end local v10    # "other":Landroid/view/View;
    .end local v11    # "otherRect":Landroid/graphics/Rect;
    :cond_6
    if-ltz v5, :cond_7

    .line 967
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v12, v5

    invoke-virtual {p0, v12, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectionFromOffset(II)V

    .line 971
    :goto_3
    return-void

    .line 969
    :cond_7
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    goto :goto_3
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1709
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1710
    const-class v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1711
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 3
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1716
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1717
    const-class v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1719
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    .line 1721
    .local v0, "infoCompat":Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1722
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFirstVisiblePosition()I

    move-result v1

    if-lez v1, :cond_0

    .line 1723
    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 1726
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 1727
    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 1730
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x1

    const/4 v11, -0x1

    const/4 v8, 0x0

    .line 1287
    iget-boolean v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    if-nez v10, :cond_1

    .line 1362
    :cond_0
    :goto_0
    return v8

    .line 1291
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    and-int/lit16 v0, v10, 0xff

    .line 1292
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1294
    :pswitch_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->initOrResetVelocityTracker()V

    .line 1295
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1297
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v10}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1299
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 1300
    .local v6, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 1302
    .local v7, "y":F
    iget-boolean v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v10, :cond_2

    .end local v7    # "y":F
    :goto_1
    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    .line 1304
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    float-to-int v10, v10

    invoke-direct {p0, v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findMotionRowOrColumn(I)I

    move-result v4

    .line 1306
    .local v4, "motionPosition":I
    invoke-static {p1, v8}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v10

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    .line 1307
    const/4 v10, 0x0

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    .line 1309
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_3

    move v8, v9

    .line 1310
    goto :goto_0

    .end local v4    # "motionPosition":I
    .restart local v7    # "y":F
    :cond_2
    move v7, v6

    .line 1302
    goto :goto_1

    .line 1311
    .end local v7    # "y":F
    .restart local v4    # "motionPosition":I
    :cond_3
    if-ltz v4, :cond_0

    .line 1312
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    .line 1313
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    goto :goto_0

    .line 1319
    .end local v4    # "motionPosition":I
    .end local v6    # "x":F
    :pswitch_1
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    if-nez v10, :cond_0

    .line 1323
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->initVelocityTrackerIfNotExists()V

    .line 1324
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v10, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1326
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    invoke-static {p1, v10}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 1327
    .local v3, "index":I
    if-gez v3, :cond_4

    .line 1328
    const-string v9, "FrameListView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - did FrameListView receive an inconsistent "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "event stream?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1335
    :cond_4
    iget-boolean v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v10, :cond_5

    .line 1336
    invoke-static {p1, v3}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 1341
    .local v5, "pos":F
    :goto_2
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    sub-float v10, v5, v10

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    add-float v2, v10, v11

    .line 1342
    .local v2, "diff":F
    float-to-int v1, v2

    .line 1343
    .local v1, "delta":I
    int-to-float v10, v1

    sub-float v10, v2, v10

    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    .line 1345
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->maybeStartScrolling(I)Z

    move-result v10

    if-eqz v10, :cond_0

    move v8, v9

    .line 1346
    goto/16 :goto_0

    .line 1338
    .end local v1    # "delta":I
    .end local v2    # "diff":F
    .end local v5    # "pos":F
    :cond_5
    invoke-static {p1, v3}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v5

    .restart local v5    # "pos":F
    goto :goto_2

    .line 1354
    .end local v3    # "index":I
    .end local v5    # "pos":F
    :pswitch_2
    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    .line 1355
    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1356
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleVelocityTracker()V

    .line 1357
    invoke-direct {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    goto/16 :goto_0

    .line 1292
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1672
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "repeatCount"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1677
    invoke-direct {p0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1682
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->handleKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 3863
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    .line 3865
    if-eqz p1, :cond_1

    .line 3866
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v0

    .line 3867
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 3868
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->forceLayout()V

    .line 3867
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3871
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->markChildrenDirty()V

    .line 3874
    .end local v0    # "childCount":I
    .end local v2    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    .line 3876
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    .line 3878
    sub-int v4, p4, p2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v5

    sub-int v3, v4, v5

    .line 3879
    .local v3, "width":I
    sub-int v4, p5, p3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v5

    sub-int v1, v4, v5

    .line 3881
    .local v1, "height":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v4, :cond_2

    .line 3882
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v4, :cond_3

    .line 3883
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 3884
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v3, v1}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 3890
    :cond_2
    :goto_1
    return-void

    .line 3886
    :cond_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v1, v3}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    .line 3887
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v4, v1, v3}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 19
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 3794
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 3795
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->useDefaultSelector()V

    .line 3798
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v18

    .line 3799
    .local v18, "widthMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v16

    .line 3800
    .local v16, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 3801
    .local v11, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 3803
    .local v6, "heightSize":I
    const/4 v15, 0x0

    .line 3804
    .local v15, "childWidth":I
    const/4 v14, 0x0

    .line 3806
    .local v14, "childHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 3807
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-lez v2, :cond_2

    if-eqz v18, :cond_1

    if-nez v16, :cond_2

    .line 3809
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v13

    .line 3811
    .local v13, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_8

    move/from16 v17, p1

    .line 3814
    .local v17, "secondaryMeasureSpec":I
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v13, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureScrapChild(Landroid/view/View;II)V

    .line 3816
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 3817
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    .line 3819
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleOnMeasure()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3820
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    const/4 v3, -0x1

    invoke-virtual {v2, v13, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3824
    .end local v13    # "child":Landroid/view/View;
    .end local v17    # "secondaryMeasureSpec":I
    :cond_2
    if-nez v18, :cond_3

    .line 3825
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int v11, v2, v15

    .line 3826
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 3827
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getVerticalScrollbarWidth()I

    move-result v2

    add-int/2addr v11, v2

    .line 3831
    :cond_3
    if-nez v16, :cond_4

    .line 3832
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    add-int v6, v2, v14

    .line 3833
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v2, :cond_4

    .line 3834
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHorizontalScrollbarHeight()I

    move-result v2

    add-int/2addr v6, v2

    .line 3838
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_5

    const/high16 v2, -0x80000000

    move/from16 v0, v16

    if-ne v0, v2, :cond_5

    .line 3839
    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v7, -0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureHeightOfChildren(IIIII)I

    move-result v6

    .line 3842
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_9

    const/4 v13, 0x0

    .line 3843
    .restart local v13    # "child":Landroid/view/View;
    :goto_2
    if-eqz v13, :cond_5

    .line 3844
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    .line 3848
    .end local v13    # "child":Landroid/view/View;
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v2, :cond_6

    const/high16 v2, -0x80000000

    move/from16 v0, v18

    if-ne v0, v2, :cond_6

    .line 3849
    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v12, -0x1

    move-object/from16 v7, p0

    move/from16 v8, p2

    invoke-direct/range {v7 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->measureWidthOfChildren(IIIII)I

    move-result v11

    .line 3852
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_a

    const/4 v13, 0x0

    .line 3853
    .restart local v13    # "child":Landroid/view/View;
    :goto_3
    if-eqz v13, :cond_6

    .line 3854
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 3858
    .end local v13    # "child":Landroid/view/View;
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setMeasuredDimension(II)V

    .line 3859
    return-void

    .line 3806
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    goto/16 :goto_0

    .restart local v13    # "child":Landroid/view/View;
    :cond_8
    move/from16 v17, p2

    .line 3811
    goto/16 :goto_1

    .line 3842
    .end local v13    # "child":Landroid/view/View;
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v13

    goto :goto_2

    .line 3852
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsScrap:[Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v13

    goto :goto_3
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 4
    .param p1, "scrollX"    # I
    .param p2, "scrollY"    # I
    .param p3, "clampedX"    # Z
    .param p4, "clampedY"    # Z

    .prologue
    .line 1053
    const/4 v0, 0x0

    .line 1055
    .local v0, "needsInvalidate":Z
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eq v1, p2, :cond_2

    .line 1056
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScrollX()I

    move-result v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->onScrollChanged(IIII)V

    .line 1057
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 1058
    const/4 v0, 0x1

    .line 1065
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1066
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    .line 1067
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    .line 1069
    :cond_1
    return-void

    .line 1059
    :cond_2
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eq v1, p1, :cond_0

    .line 1060
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getScrollY()I

    move-result v3

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->onScrollChanged(IIII)V

    .line 1061
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 1062
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 8
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 5769
    move-object v0, p1

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    .line 5770
    .local v0, "ss":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;
    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 5772
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 5773
    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->height:I

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncHeight:J

    .line 5775
    iget-wide v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->selectedId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_3

    .line 5776
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 5777
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    .line 5778
    iget-wide v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->selectedId:J

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 5779
    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 5780
    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 5781
    const/4 v1, 0x0

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncMode:I

    .line 5797
    :cond_0
    :goto_0
    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    if-eqz v1, :cond_1

    .line 5798
    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 5801
    :cond_1
    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_2

    .line 5802
    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 5805
    :cond_2
    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkedItemCount:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 5807
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    .line 5808
    return-void

    .line 5782
    :cond_3
    iget-wide v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    .line 5783
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 5786
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 5788
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    .line 5789
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 5790
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    .line 5791
    iget-wide v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 5792
    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 5793
    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 5794
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncMode:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 13

    .prologue
    .line 5696
    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v9

    .line 5697
    .local v9, "superState":Landroid/os/Parcelable;
    new-instance v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    invoke-direct {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 5699
    .local v8, "ss":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    if-eqz v10, :cond_0

    .line 5700
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    iget-wide v10, v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->selectedId:J

    iput-wide v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->selectedId:J

    .line 5701
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    iget-wide v10, v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    iput-wide v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    .line 5702
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    .line 5703
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    .line 5704
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->height:I

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->height:I

    .line 5764
    :goto_0
    return-object v8

    .line 5709
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v10

    if-lez v10, :cond_2

    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-lez v10, :cond_2

    const/4 v3, 0x1

    .line 5710
    .local v3, "haveChildren":Z
    :goto_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedItemId()J

    move-result-wide v6

    .line 5711
    .local v6, "selectedId":J
    iput-wide v6, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->selectedId:J

    .line 5712
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v10

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->height:I

    .line 5714
    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-ltz v10, :cond_3

    .line 5715
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    .line 5716
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getSelectedItemPosition()I

    move-result v10

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    .line 5717
    const-wide/16 v10, -0x1

    iput-wide v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    .line 5747
    :goto_2
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v10, :cond_1

    .line 5748
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cloneCheckStates()Landroid/util/SparseBooleanArray;

    move-result-object v10

    iput-object v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkState:Landroid/util/SparseBooleanArray;

    .line 5751
    :cond_1
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v10, :cond_8

    .line 5752
    new-instance v5, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v5}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 5754
    .local v5, "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v10}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v1

    .line 5755
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    if-ge v4, v1, :cond_7

    .line 5756
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v10, v4}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v12, v4}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v5, v10, v11, v12}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 5755
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 5709
    .end local v1    # "count":I
    .end local v3    # "haveChildren":Z
    .end local v4    # "i":I
    .end local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    .end local v6    # "selectedId":J
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 5718
    .restart local v3    # "haveChildren":Z
    .restart local v6    # "selectedId":J
    :cond_3
    if-eqz v3, :cond_6

    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-lez v10, :cond_6

    .line 5731
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5732
    .local v0, "child":Landroid/view/View;
    iget-boolean v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v10, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v10

    :goto_4
    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    .line 5734
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5735
    .local v2, "firstPos":I
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-lt v2, v10, :cond_4

    .line 5736
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v2, v10, -0x1

    .line 5739
    :cond_4
    iput v2, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    .line 5740
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v10, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v10

    iput-wide v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    goto :goto_2

    .line 5732
    .end local v2    # "firstPos":I
    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v10

    goto :goto_4

    .line 5742
    .end local v0    # "child":Landroid/view/View;
    :cond_6
    const/4 v10, 0x0

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->viewStart:I

    .line 5743
    const-wide/16 v10, -0x1

    iput-wide v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->firstId:J

    .line 5744
    const/4 v10, 0x0

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->position:I

    goto :goto_2

    .line 5759
    .restart local v1    # "count":I
    .restart local v4    # "i":I
    .restart local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_7
    iput-object v5, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkIdState:Landroid/support/v4/util/LongSparseArray;

    .line 5762
    .end local v1    # "count":I
    .end local v4    # "i":I
    .end local v5    # "idState":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/Integer;>;"
    :cond_8
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    iput v10, v8, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;->checkedItemCount:I

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 26
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1367
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1370
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isClickable()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 1641
    :goto_0
    return v2

    .line 1370
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1373
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsAttached:Z

    if-nez v2, :cond_3

    .line 1374
    const/4 v2, 0x0

    goto :goto_0

    .line 1377
    :cond_3
    const/16 v20, 0x0

    .line 1379
    .local v20, "needsInvalidate":Z
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->initVelocityTrackerIfNotExists()V

    .line 1380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1382
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v11, v2, 0xff

    .line 1383
    .local v11, "action":I
    packed-switch v11, :pswitch_data_0

    .line 1637
    :cond_4
    :goto_1
    if-eqz v20, :cond_5

    .line 1638
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 1641
    :cond_5
    const/4 v2, 0x1

    goto :goto_0

    .line 1385
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-nez v2, :cond_4

    .line 1389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 1390
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1392
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    .line 1393
    .local v24, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    .line 1395
    .local v25, "y":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_6

    move/from16 v2, v25

    :goto_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    .line 1397
    move/from16 v0, v24

    float-to-int v2, v0

    move/from16 v0, v25

    float-to-int v3, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->pointToPosition(II)I

    move-result v18

    .line 1399
    .local v18, "motionPosition":I
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    .line 1400
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    .line 1402
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-nez v2, :cond_4

    .line 1406
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    .line 1407
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1408
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    .line 1409
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    float-to-int v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->findMotionRowOrColumn(I)I

    move-result v18

    .line 1410
    const/4 v2, 0x1

    goto/16 :goto_0

    .end local v18    # "motionPosition":I
    :cond_6
    move/from16 v2, v24

    .line 1395
    goto :goto_2

    .line 1411
    .restart local v18    # "motionPosition":I
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    if-ltz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1412
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1413
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->triggerCheckForTap()V

    .line 1416
    :cond_8
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    goto/16 :goto_1

    .line 1422
    .end local v18    # "motionPosition":I
    .end local v24    # "x":F
    .end local v25    # "y":F
    :pswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v17

    .line 1423
    .local v17, "index":I
    if-gez v17, :cond_9

    .line 1424
    const-string v2, "FrameListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - did FrameListView receive an inconsistent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "event stream?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1431
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_b

    .line 1432
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v22

    .line 1437
    .local v22, "pos":F
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-eqz v2, :cond_a

    .line 1440
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    .line 1443
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    sub-float v2, v22, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    add-float v15, v2, v3

    .line 1444
    .local v15, "diff":F
    float-to-int v14, v15

    .line 1445
    .local v14, "delta":I
    int-to-float v2, v14

    sub-float v2, v15, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchRemainderPos:F

    .line 1447
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    packed-switch v2, :pswitch_data_1

    :pswitch_2
    goto/16 :goto_1

    .line 1453
    :pswitch_3
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->maybeStartScrolling(I)Z

    goto/16 :goto_1

    .line 1434
    .end local v14    # "delta":I
    .end local v15    # "diff":F
    .end local v22    # "pos":F
    :cond_b
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v22

    .restart local v22    # "pos":F
    goto :goto_3

    .line 1458
    .restart local v14    # "delta":I
    .restart local v15    # "diff":F
    :pswitch_4
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    .line 1459
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->maybeScroll(I)V

    goto/16 :goto_1

    .line 1467
    .end local v14    # "delta":I
    .end local v15    # "diff":F
    .end local v17    # "index":I
    .end local v22    # "pos":F
    :pswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cancelCheckForTap()V

    .line 1468
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1469
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    .line 1471
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setPressed(Z)V

    .line 1472
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 1473
    .local v19, "motionView":Landroid/view/View;
    if-eqz v19, :cond_c

    .line 1474
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1477
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_d

    .line 1478
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v3

    or-int v20, v2, v3

    .line 1481
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleVelocityTracker()V

    goto/16 :goto_1

    .line 1486
    .end local v19    # "motionView":Landroid/view/View;
    :pswitch_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    packed-switch v2, :pswitch_data_2

    .line 1623
    :goto_4
    :pswitch_7
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cancelCheckForTap()V

    .line 1624
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cancelCheckForLongPress()V

    .line 1625
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setPressed(Z)V

    .line 1627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v2, :cond_e

    .line 1628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->onRelease()Z

    move-result v3

    or-int/2addr v2, v3

    or-int v20, v20, v2

    .line 1631
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleVelocityTracker()V

    goto/16 :goto_1

    .line 1490
    :pswitch_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    move/from16 v18, v0

    .line 1491
    .restart local v18    # "motionPosition":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v2, v18, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 1493
    .local v12, "child":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    .line 1494
    .restart local v24    # "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    .line 1496
    .restart local v25    # "y":F
    const/16 v16, 0x0

    .line 1497
    .local v16, "inList":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_16

    .line 1498
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v24, v2

    if-lez v2, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v24, v2

    if-gez v2, :cond_15

    const/16 v16, 0x1

    .line 1503
    :goto_5
    if-eqz v12, :cond_14

    if-eqz v16, :cond_14

    .line 1504
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    if-eqz v2, :cond_f

    .line 1505
    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1508
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPerformClick:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

    if-nez v2, :cond_10

    .line 1509
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPerformClick:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

    .line 1512
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPerformClick:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;

    move-object/from16 v21, v0

    .line 1513
    .local v21, "performClick":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;
    move/from16 v0, v18

    move-object/from16 v1, v21

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;->mClickMotionPosition:I

    .line 1514
    invoke-virtual/range {v21 .. v21}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;->rememberWindowAttachCount()V

    .line 1516
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 1518
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1a

    .line 1519
    :cond_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    if-nez v2, :cond_18

    .line 1520
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cancelCheckForTap()V

    .line 1525
    :goto_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1527
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-nez v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1528
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1530
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setPressed(Z)V

    .line 1531
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMotionPosition:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->positionSelector(ILandroid/view/View;)V

    .line 1532
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_12

    .line 1535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 1536
    .local v13, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_12

    instance-of v2, v13, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v2, :cond_12

    .line 1537
    check-cast v13, Landroid/graphics/drawable/TransitionDrawable;

    .end local v13    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v13}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 1541
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    if-eqz v2, :cond_13

    .line 1542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1545
    :cond_13
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v2, v0, v12, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Landroid/view/View;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    .line 1561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchModeReset:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1572
    .end local v21    # "performClick":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;
    :cond_14
    :goto_7
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1573
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    goto/16 :goto_4

    .line 1498
    :cond_15
    const/16 v16, 0x0

    goto/16 :goto_5

    .line 1500
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v25, v2

    if-lez v2, :cond_17

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v25, v2

    if-gez v2, :cond_17

    const/16 v16, 0x1

    :goto_8
    goto/16 :goto_5

    :cond_17
    const/16 v16, 0x0

    goto :goto_8

    .line 1522
    .restart local v21    # "performClick":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;
    :cond_18
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->cancelCheckForLongPress()V

    goto/16 :goto_6

    .line 1564
    :cond_19
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1565
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    goto :goto_7

    .line 1567
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1568
    invoke-virtual/range {v21 .. v21}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;->run()V

    goto :goto_7

    .line 1579
    .end local v12    # "child":Landroid/view/View;
    .end local v16    # "inList":Z
    .end local v18    # "motionPosition":I
    .end local v21    # "performClick":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$PerformClick;
    .end local v24    # "x":F
    .end local v25    # "y":F
    :pswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->contentFits()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1580
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1581
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 1585
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mMaximumVelocity:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1588
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_1c

    .line 1589
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    invoke-static {v2, v3}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v23

    .line 1596
    .local v23, "velocity":F
    :goto_9
    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFlingVelocity:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_23

    .line 1597
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1598
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    .line 1600
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v5, :cond_1d

    const/4 v5, 0x0

    :goto_a
    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v6, :cond_1e

    .end local v23    # "velocity":F
    :goto_b
    move/from16 v0, v23

    float-to-int v6, v0

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v7, :cond_1f

    const/4 v7, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v8, :cond_20

    const/4 v8, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v9, :cond_21

    const/high16 v9, -0x80000000

    :goto_e
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v10, :cond_22

    const v10, 0x7fffffff

    :goto_f
    invoke-virtual/range {v2 .. v10}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1608
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchPos:F

    .line 1609
    const/16 v20, 0x1

    goto/16 :goto_4

    .line 1592
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mActivePointerId:I

    invoke-static {v2, v3}, Landroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v23

    .restart local v23    # "velocity":F
    goto :goto_9

    :cond_1d
    move/from16 v5, v23

    .line 1600
    goto :goto_a

    :cond_1e
    const/16 v23, 0x0

    goto :goto_b

    .end local v23    # "velocity":F
    :cond_1f
    const/high16 v7, -0x80000000

    goto :goto_c

    :cond_20
    const v8, 0x7fffffff

    goto :goto_d

    :cond_21
    const/4 v9, 0x0

    goto :goto_e

    :cond_22
    const/4 v10, 0x0

    goto :goto_f

    .line 1611
    .restart local v23    # "velocity":F
    :cond_23
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1612
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 1618
    .end local v23    # "velocity":F
    :pswitch_a
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1619
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 1383
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 1447
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 1486
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_a
    .end packed-switch
.end method

.method public onTouchModeChanged(Z)V
    .locals 2
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 1646
    if-eqz p1, :cond_2

    .line 1648
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hideSelector()V

    .line 1653
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1654
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    .line 1657
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    .line 1668
    :cond_1
    :goto_0
    return-void

    .line 1659
    :cond_2
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 1660
    .local v0, "touchMode":I
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1661
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    if-eqz v1, :cond_1

    .line 1662
    const/4 v1, 0x0

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 1663
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->finishEdgeGlows()V

    .line 1664
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5
    .param p1, "hasWindowFocus"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1022
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onWindowFocusChanged(Z)V

    .line 1024
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 1026
    .local v0, "touchMode":I
    :goto_0
    if-nez p1, :cond_2

    .line 1027
    if-ne v0, v2, :cond_0

    .line 1029
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 1048
    :cond_0
    :goto_1
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchMode:I

    .line 1049
    return-void

    .end local v0    # "touchMode":I
    :cond_1
    move v0, v2

    .line 1024
    goto :goto_0

    .line 1033
    .restart local v0    # "touchMode":I
    :cond_2
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchMode:I

    if-eq v0, v3, :cond_0

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastTouchMode:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1035
    if-ne v0, v2, :cond_3

    .line 1037
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelection()Z

    goto :goto_1

    .line 1041
    :cond_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->hideSelector()V

    .line 1042
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1043
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->layoutChildren()V

    goto :goto_1
.end method

.method pageScroll(I)Z
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1818
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->forceValidFocusDirection(I)V

    .line 1820
    const/4 v0, 0x0

    .line 1821
    .local v0, "forward":Z
    const/4 v1, -0x1

    .line 1823
    .local v1, "nextPage":I
    const/16 v5, 0x21

    if-eq p1, v5, :cond_0

    const/16 v5, 0x11

    if-ne p1, v5, :cond_3

    .line 1824
    :cond_0
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1830
    :cond_1
    :goto_0
    if-gez v1, :cond_5

    .line 1857
    :cond_2
    :goto_1
    return v3

    .line 1825
    :cond_3
    const/16 v5, 0x82

    if-eq p1, v5, :cond_4

    const/16 v5, 0x42

    if-ne p1, v5, :cond_1

    .line 1826
    :cond_4
    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v7

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1827
    const/4 v0, 0x1

    goto :goto_0

    .line 1834
    :cond_5
    invoke-direct {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 1835
    .local v2, "position":I
    if-ltz v2, :cond_2

    .line 1836
    const/4 v3, 0x4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1837
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v3, :cond_9

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v3

    :goto_2
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 1839
    if-eqz v0, :cond_6

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v5

    sub-int/2addr v3, v5

    if-le v2, v3, :cond_6

    .line 1840
    const/4 v3, 0x3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1843
    :cond_6
    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 1844
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 1847
    :cond_7
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectionInt(I)V

    .line 1848
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    .line 1850
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->awakenScrollbarsInternal()Z

    move-result v3

    if-nez v3, :cond_8

    .line 1851
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    :cond_8
    move v3, v4

    .line 1854
    goto :goto_1

    .line 1837
    :cond_9
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v3

    goto :goto_2
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 5
    .param p1, "action"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1735
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1771
    :goto_0
    return v1

    .line 1739
    :cond_0
    sparse-switch p1, :sswitch_data_0

    move v1, v2

    .line 1771
    goto :goto_0

    .line 1741
    :sswitch_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_2

    .line 1743
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_1

    .line 1744
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v3

    sub-int v0, v2, v3

    .line 1750
    .local v0, "viewportSize":I
    :goto_1
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    goto :goto_0

    .line 1746
    .end local v0    # "viewportSize":I
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v3

    sub-int v0, v2, v3

    .restart local v0    # "viewportSize":I
    goto :goto_1

    .end local v0    # "viewportSize":I
    :cond_2
    move v1, v2

    .line 1753
    goto :goto_0

    .line 1756
    :sswitch_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    if-lez v3, :cond_4

    .line 1758
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 1759
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v3

    sub-int v0, v2, v3

    .line 1765
    .restart local v0    # "viewportSize":I
    :goto_2
    neg-int v2, v0

    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    goto :goto_0

    .line 1761
    .end local v0    # "viewportSize":I
    :cond_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v3

    sub-int v0, v2, v3

    .restart local v0    # "viewportSize":I
    goto :goto_2

    .end local v0    # "viewportSize":I
    :cond_4
    move v1, v2

    .line 1768
    goto :goto_0

    .line 1739
    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5595
    const/4 v1, 0x0

    .line 5597
    .local v1, "checkedStateChanged":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->MULTIPLE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-nez v4, :cond_6

    .line 5598
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, p2, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v2

    .line 5599
    .local v0, "checked":Z
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 5601
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5602
    if-eqz v0, :cond_4

    .line 5603
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 5609
    :cond_0
    :goto_1
    if-eqz v0, :cond_5

    .line 5610
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 5615
    :goto_2
    const/4 v1, 0x1

    .line 5635
    .end local v0    # "checked":Z
    :cond_1
    :goto_3
    if-eqz v1, :cond_2

    .line 5636
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateOnScreenCheckedViews()V

    .line 5639
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v2

    return v2

    :cond_3
    move v0, v3

    .line 5598
    goto :goto_0

    .line 5605
    .restart local v0    # "checked":Z
    :cond_4
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    goto :goto_1

    .line 5612
    :cond_5
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    goto :goto_2

    .line 5616
    .end local v0    # "checked":Z
    :cond_6
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->SINGLE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-nez v4, :cond_1

    .line 5617
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, p2, v3}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v4

    if-nez v4, :cond_9

    move v0, v2

    .line 5618
    .restart local v0    # "checked":Z
    :goto_4
    if-eqz v0, :cond_a

    .line 5619
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    .line 5620
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p2, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 5622
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 5623
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 5624
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 5627
    :cond_7
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 5632
    :cond_8
    :goto_5
    const/4 v1, 0x1

    goto :goto_3

    .end local v0    # "checked":Z
    :cond_9
    move v0, v3

    .line 5617
    goto :goto_4

    .line 5628
    .restart local v0    # "checked":Z
    :cond_a
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 5629
    :cond_b
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    goto :goto_5
.end method

.method public pointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1108
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 1109
    .local v2, "frame":Landroid/graphics/Rect;
    if-nez v2, :cond_0

    .line 1110
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 1111
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 1114
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 1115
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 1116
    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1118
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 1119
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1121
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1122
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    add-int/2addr v4, v3

    .line 1126
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 1115
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 1126
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method protected recycleOnMeasure()Z
    .locals 1

    .prologue
    .line 4198
    const/4 v0, 0x1

    return v0
.end method

.method public requestAccessibilityFocus(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 6775
    const-string v2, "FrameListView"

    const-string v3, "requestAccessibilityFocus in  "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6776
    if-nez p1, :cond_0

    .line 6792
    :goto_0
    return-void

    .line 6780
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "requestAccessibilityFocus"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 6783
    .local v1, "requestAccessibilityFocus":Ljava/lang/reflect/Method;
    const-string v2, "FrameListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestAccessibilityFocus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6785
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 6786
    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6791
    .end local v1    # "requestAccessibilityFocus":Ljava/lang/reflect/Method;
    :goto_1
    const-string v2, "FrameListView"

    const-string v3, "requestAccessibilityFocus out  "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6787
    :catch_0
    move-exception v0

    .line 6788
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1, "disallowIntercept"    # Z

    .prologue
    .line 1278
    if-eqz p1, :cond_0

    .line 1279
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->recycleVelocityTracker()V

    .line 1282
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->requestDisallowInterceptTouchEvent(Z)V

    .line 1283
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 3714
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    if-nez v0, :cond_0

    .line 3715
    invoke-super {p0}, Landroid/widget/AdapterView;->requestLayout()V

    .line 3717
    :cond_0
    return-void
.end method

.method resetState()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 5508
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 5509
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->removeAllViewsInLayout()V

    .line 5511
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedStart:I

    .line 5512
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 5513
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 5514
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    .line 5515
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mPendingSync:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SavedState;

    .line 5516
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedPosition:I

    .line 5517
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedRowId:J

    .line 5519
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOverScroll:I

    .line 5521
    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 5522
    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 5524
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorPosition:I

    .line 5525
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 5527
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invalidate()V

    .line 5528
    return-void
.end method

.method resurrectSelection()Z
    .locals 15

    .prologue
    .line 4533
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v1

    .line 4534
    .local v1, "childCount":I
    if-gtz v1, :cond_0

    .line 4535
    const/4 v13, 0x0

    .line 4611
    :goto_0
    return v13

    .line 4538
    :cond_0
    const/4 v10, 0x0

    .line 4541
    .local v10, "selectedStart":I
    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v13, :cond_2

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v11

    .line 4542
    .local v11, "start":I
    :goto_1
    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v13, :cond_3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getHeight()I

    move-result v13

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingBottom()I

    move-result v14

    sub-int v5, v13, v14

    .line 4545
    .local v5, "end":I
    :goto_2
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    .line 4546
    .local v6, "firstPosition":I
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 4547
    .local v12, "toPosition":I
    const/4 v4, 0x1

    .line 4549
    .local v4, "down":Z
    if-lt v12, v6, :cond_5

    add-int v13, v6, v1

    if-ge v12, v13, :cond_5

    .line 4550
    move v9, v12

    .line 4552
    .local v9, "selectedPosition":I
    iget v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v13, v9, v13

    invoke-virtual {p0, v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 4553
    .local v8, "selected":Landroid/view/View;
    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v13, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v10

    .line 4595
    .end local v8    # "selected":Landroid/view/View;
    :cond_1
    :goto_3
    const/4 v13, -0x1

    iput v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    .line 4596
    const/4 v13, -0x1

    iput v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mTouchMode:I

    .line 4597
    const/4 v13, 0x0

    invoke-direct {p0, v13}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->reportScrollStateChange(I)V

    .line 4599
    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 4601
    invoke-direct {p0, v9, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(IZ)I

    move-result v9

    .line 4602
    if-lt v9, v6, :cond_e

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getLastVisiblePosition()I

    move-result v13

    if-gt v9, v13, :cond_e

    .line 4603
    const/4 v13, 0x4

    iput v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 4604
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    .line 4605
    invoke-direct {p0, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectionInt(I)V

    .line 4606
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    .line 4611
    :goto_4
    if-ltz v9, :cond_f

    const/4 v13, 0x1

    goto :goto_0

    .line 4541
    .end local v4    # "down":Z
    .end local v5    # "end":I
    .end local v6    # "firstPosition":I
    .end local v9    # "selectedPosition":I
    .end local v11    # "start":I
    .end local v12    # "toPosition":I
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v11

    goto :goto_1

    .line 4542
    .restart local v11    # "start":I
    :cond_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getWidth()I

    move-result v13

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingRight()I

    move-result v14

    sub-int v5, v13, v14

    goto :goto_2

    .line 4553
    .restart local v4    # "down":Z
    .restart local v5    # "end":I
    .restart local v6    # "firstPosition":I
    .restart local v8    # "selected":Landroid/view/View;
    .restart local v9    # "selectedPosition":I
    .restart local v12    # "toPosition":I
    :cond_4
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v10

    goto :goto_3

    .line 4554
    .end local v8    # "selected":Landroid/view/View;
    .end local v9    # "selectedPosition":I
    :cond_5
    if-ge v12, v6, :cond_9

    .line 4556
    move v9, v6

    .line 4558
    .restart local v9    # "selectedPosition":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_5
    if-ge v7, v1, :cond_1

    .line 4559
    invoke-virtual {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4560
    .local v0, "child":Landroid/view/View;
    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v13, :cond_7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4562
    .local v3, "childStart":I
    :goto_6
    if-nez v7, :cond_6

    .line 4564
    move v10, v3

    .line 4567
    :cond_6
    if-lt v3, v11, :cond_8

    .line 4569
    add-int v9, v6, v7

    .line 4570
    move v10, v3

    .line 4571
    goto :goto_3

    .line 4560
    .end local v3    # "childStart":I
    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_6

    .line 4558
    .restart local v3    # "childStart":I
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 4575
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "childStart":I
    .end local v7    # "i":I
    .end local v9    # "selectedPosition":I
    :cond_9
    add-int v13, v6, v1

    add-int/lit8 v9, v13, -0x1

    .line 4576
    .restart local v9    # "selectedPosition":I
    const/4 v4, 0x0

    .line 4578
    add-int/lit8 v7, v1, -0x1

    .restart local v7    # "i":I
    :goto_7
    if-ltz v7, :cond_1

    .line 4579
    invoke-virtual {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4580
    .restart local v0    # "child":Landroid/view/View;
    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v13, :cond_b

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    .line 4581
    .restart local v3    # "childStart":I
    :goto_8
    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v13, :cond_c

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 4583
    .local v2, "childEnd":I
    :goto_9
    add-int/lit8 v13, v1, -0x1

    if-ne v7, v13, :cond_a

    .line 4584
    move v10, v3

    .line 4587
    :cond_a
    if-gt v2, v5, :cond_d

    .line 4588
    add-int v9, v6, v7

    .line 4589
    move v10, v3

    .line 4590
    goto/16 :goto_3

    .line 4580
    .end local v2    # "childEnd":I
    .end local v3    # "childStart":I
    :cond_b
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    goto :goto_8

    .line 4581
    .restart local v3    # "childStart":I
    :cond_c
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    goto :goto_9

    .line 4578
    .restart local v2    # "childEnd":I
    :cond_d
    add-int/lit8 v7, v7, -0x1

    goto :goto_7

    .line 4608
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childEnd":I
    .end local v3    # "childStart":I
    .end local v7    # "i":I
    :cond_e
    const/4 v9, -0x1

    goto :goto_4

    .line 4611
    :cond_f
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method resurrectSelectionIfNeeded()Z
    .locals 1

    .prologue
    .line 4619
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelectedPosition:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resurrectSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4620
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    .line 4621
    const/4 v0, 0x1

    .line 4624
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scrollBy(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 3766
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->scrollListItemsBy(I)Z

    .line 3767
    return-void
.end method

.method public sendAccessibilityEvent(I)V
    .locals 3
    .param p1, "eventType"    # I

    .prologue
    .line 1690
    const/16 v2, 0x1000

    if-ne p1, v2, :cond_1

    .line 1691
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getFirstVisiblePosition()I

    move-result v0

    .line 1692
    .local v0, "firstVisiblePosition":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getLastVisiblePosition()I

    move-result v1

    .line 1694
    .local v1, "lastVisiblePosition":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastAccessibilityScrollEventFromIndex:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastAccessibilityScrollEventToIndex:I

    if-ne v2, v1, :cond_0

    .line 1704
    .end local v0    # "firstVisiblePosition":I
    .end local v1    # "lastVisiblePosition":I
    :goto_0
    return-void

    .line 1698
    .restart local v0    # "firstVisiblePosition":I
    .restart local v1    # "lastVisiblePosition":I
    :cond_0
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastAccessibilityScrollEventFromIndex:I

    .line 1699
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLastAccessibilityScrollEventToIndex:I

    .line 1703
    .end local v0    # "firstVisiblePosition":I
    .end local v1    # "lastVisiblePosition":I
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 92
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 6
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 796
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 797
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 800
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resetState()V

    .line 801
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->clear()V

    .line 803
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 804
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 806
    const/4 v1, -0x1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedPosition:I

    .line 807
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldSelectedRowId:J

    .line 809
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v1, :cond_1

    .line 810
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    .line 813
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v1, :cond_2

    .line 814
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 817
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_5

    .line 818
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOldItemCount:I

    .line 819
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 821
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    .line 822
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataSetObserver:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 824
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->setViewTypeCount(I)V

    .line 826
    invoke-interface {p1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z

    .line 827
    invoke-interface {p1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAreAllItemsSelectable:Z

    .line 829
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-nez v1, :cond_3

    .line 831
    new-instance v1, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v1}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 834
    :cond_3
    invoke-direct {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(I)I

    move-result v0

    .line 835
    .local v0, "position":I
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectedPositionInt(I)V

    .line 836
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 838
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    if-nez v1, :cond_4

    .line 839
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkSelectionChanged()V

    .line 849
    .end local v0    # "position":I
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkFocus()V

    .line 850
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    .line 851
    return-void

    .line 842
    :cond_5
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemCount:I

    .line 843
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mHasStableIds:Z

    .line 844
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAreAllItemsSelectable:Z

    .line 846
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->checkSelectionChanged()V

    goto :goto_0
.end method

.method public setChoiceMode(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;)V
    .locals 2
    .param p1, "choiceMode"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    .prologue
    .line 776
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    .line 778
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 779
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-nez v0, :cond_0

    .line 780
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 783
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 784
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    .line 787
    :cond_1
    return-void
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0
    .param p1, "drawSelectorOnTop"    # Z

    .prologue
    .line 517
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDrawSelectorOnTop:Z

    .line 518
    return-void
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 0
    .param p1, "emptyView"    # Landroid/view/View;

    .prologue
    .line 6200
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    .line 6201
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEmptyView:Landroid/view/View;

    .line 6202
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateEmptyStatus()V

    .line 6203
    return-void
.end method

.method public setFocusable(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6207
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6208
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v1, v3

    .line 6210
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDesiredFocusableState:Z

    .line 6211
    if-nez p1, :cond_1

    .line 6212
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDesiredFocusableInTouchModeState:Z

    .line 6215
    :cond_1
    if-eqz p1, :cond_3

    if-nez v1, :cond_3

    :goto_1
    invoke-super {p0, v3}, Landroid/widget/AdapterView;->setFocusable(Z)V

    .line 6216
    return-void

    .end local v1    # "empty":Z
    :cond_2
    move v1, v2

    .line 6208
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_3
    move v3, v2

    .line 6215
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 6220
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6221
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v1, v3

    .line 6223
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDesiredFocusableInTouchModeState:Z

    .line 6224
    if-eqz p1, :cond_1

    .line 6225
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDesiredFocusableState:Z

    .line 6228
    :cond_1
    if-eqz p1, :cond_3

    if-nez v1, :cond_3

    :goto_1
    invoke-super {p0, v3}, Landroid/widget/AdapterView;->setFocusableInTouchMode(Z)V

    .line 6229
    return-void

    .end local v1    # "empty":Z
    :cond_2
    move v1, v2

    .line 6221
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_3
    move v3, v2

    .line 6228
    goto :goto_1
.end method

.method public setItemChecked(IZ)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "value"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 683
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->NONE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-nez v4, :cond_1

    .line 740
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mChoiceMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    sget-object v5, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->MULTIPLE:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$ChoiceMode;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-nez v4, :cond_6

    .line 688
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 689
    .local v0, "oldValue":Z
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 691
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 692
    if-eqz p2, :cond_4

    .line 693
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 699
    :cond_2
    :goto_1
    if-eq v0, p2, :cond_3

    .line 700
    if-eqz p2, :cond_5

    .line 701
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    .line 735
    .end local v0    # "oldValue":Z
    :cond_3
    :goto_2
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mInLayout:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mBlockLayoutRequests:Z

    if-nez v3, :cond_0

    .line 736
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z

    .line 737
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->rememberSyncState()V

    .line 738
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    goto :goto_0

    .line 695
    .restart local v0    # "oldValue":Z
    :cond_4
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/util/LongSparseArray;->delete(J)V

    goto :goto_1

    .line 703
    :cond_5
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    goto :goto_2

    .line 707
    .end local v0    # "oldValue":Z
    :cond_6
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v4

    if-eqz v4, :cond_a

    move v1, v2

    .line 711
    .local v1, "updateIds":Z
    :goto_3
    if-nez p2, :cond_7

    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isItemChecked(I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 712
    :cond_7
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->clear()V

    .line 714
    if-eqz v1, :cond_8

    .line 715
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v4}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 721
    :cond_8
    if-eqz p2, :cond_b

    .line 722
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, p1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 724
    if-eqz v1, :cond_9

    .line 725
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedIdStates:Landroid/support/v4/util/LongSparseArray;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 728
    :cond_9
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    goto :goto_2

    .end local v1    # "updateIds":Z
    :cond_a
    move v1, v3

    .line 707
    goto :goto_3

    .line 729
    .restart local v1    # "updateIds":Z
    :cond_b
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 730
    :cond_c
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mCheckedItemCount:I

    goto :goto_2
.end method

.method public setItemMargin(I)V
    .locals 1
    .param p1, "itemMargin"    # I

    .prologue
    .line 449
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    if-ne v0, p1, :cond_0

    .line 455
    :goto_0
    return-void

    .line 453
    :cond_0
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemMargin:I

    .line 454
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    goto :goto_0
.end method

.method public setItemsCanFocus(Z)V
    .locals 1
    .param p1, "itemsCanFocus"    # Z

    .prologue
    .line 468
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mItemsCanFocus:Z

    .line 469
    if-nez p1, :cond_0

    .line 470
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setDescendantFocusability(I)V

    .line 472
    :cond_0
    return-void
.end method

.method public setOnScrollListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mOnScrollListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$OnScrollListener;

    .line 489
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->invokeOnItemScrollListener()V

    .line 490
    return-void
.end method

.method public setOrientation(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;)V
    .locals 2
    .param p1, "orientation"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    .prologue
    .line 430
    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->VERTICAL:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;

    invoke-virtual {p1, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$Orientation;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 431
    .local v0, "isVertical":Z
    :goto_0
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-ne v1, v0, :cond_1

    .line 442
    :goto_1
    return-void

    .line 430
    .end local v0    # "isVertical":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 435
    .restart local v0    # "isVertical":Z
    :cond_1
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    .line 437
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateScrollbarsDirection()V

    .line 438
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->resetState()V

    .line 439
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->clear()V

    .line 441
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    goto :goto_1
.end method

.method public setOverScrollMode(I)V
    .locals 4
    .param p1, "mode"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1088
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_0

    .line 1105
    :goto_0
    return-void

    .line 1092
    :cond_0
    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    .line 1093
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-nez v1, :cond_1

    .line 1094
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1096
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 1097
    new-instance v1, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 1104
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setOverScrollMode(I)V

    goto :goto_0

    .line 1100
    :cond_2
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mStartEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    .line 1101
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mEndEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_1
.end method

.method public setRecyclerListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;)V
    .locals 1
    .param p1, "l"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    .prologue
    .line 504
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mRecycler:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->mRecyclerListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;->access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecycleBin;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;)Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$RecyclerListener;

    .line 505
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3730
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelectionFromOffset(II)V

    .line 3731
    return-void
.end method

.method public setSelectionFromOffset(II)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    .line 3734
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    .line 3763
    :cond_0
    :goto_0
    return-void

    .line 3738
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3739
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->lookForSelectablePosition(I)I

    move-result p1

    .line 3740
    if-ltz p1, :cond_2

    .line 3741
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setNextSelectedPositionInt(I)V

    .line 3747
    :cond_2
    :goto_1
    if-ltz p1, :cond_0

    .line 3748
    const/4 v0, 0x4

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mLayoutMode:I

    .line 3750
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mIsVertical:Z

    if-eqz v0, :cond_5

    .line 3751
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, p2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    .line 3756
    :goto_2
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mNeedSync:Z

    if-eqz v0, :cond_3

    .line 3757
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncPosition:I

    .line 3758
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSyncRowId:J

    .line 3761
    :cond_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->requestLayout()V

    goto :goto_0

    .line 3744
    :cond_4
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mResurrectToPosition:I

    goto :goto_1

    .line 3753
    :cond_5
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSpecificStart:I

    goto :goto_2
.end method

.method public setSelector(I)V
    .locals 1
    .param p1, "resID"    # I

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 529
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "selector"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 539
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 541
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 544
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 545
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 546
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 548
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 549
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->updateSelectorState()V

    .line 550
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 7
    .param p1, "originalView"    # Landroid/view/View;

    .prologue
    .line 1251
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 1252
    .local v3, "longPressPosition":I
    if-ltz v3, :cond_2

    .line 1253
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1254
    .local v4, "longPressId":J
    const/4 v6, 0x0

    .line 1256
    .local v6, "handled":Z
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getOnItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    .line 1257
    .local v0, "listener":Landroid/widget/AdapterView$OnItemLongClickListener;
    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v2, p1

    .line 1258
    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 1262
    :cond_0
    if-nez v6, :cond_1

    .line 1263
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mFirstPosition:I

    sub-int v1, v3, v1

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1267
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 1273
    .end local v0    # "listener":Landroid/widget/AdapterView$OnItemLongClickListener;
    .end local v4    # "longPressId":J
    .end local v6    # "handled":Z
    :cond_1
    :goto_0
    return v6

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method
