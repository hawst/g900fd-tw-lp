.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;
.implements Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$MotionMask;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;,
        Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
    }
.end annotation


# static fields
.field private static final BLUR_SIZE_ADJUST_INCREMENT:I = 0x1e

.field private static final UPDATE_BG_BITMAP:I = 0x9998

.field private static final UPDATE_LARGE_BITMAP_AND_MASK:I = 0x9999


# instance fields
.field private final FLAG_FROM_ALG_TO_UI:I

.field private final FLAG_FROM_UI_TO_ALG:I

.field private final MAX_SOUND_STREAM:I

.field ViewsShow:Z

.field private mActionBar:Landroid/widget/RelativeLayout;

.field private mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

.field mAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;

.field mAnimEraserState:Landroid/animation/Animator;

.field mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

.field mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

.field private mArrowClickLis:Landroid/view/View$OnClickListener;

.field private mBackArrowImageView:Landroid/widget/ImageView;

.field private mBackArrowLayout:Landroid/widget/RelativeLayout;

.field private mBackImageView:Landroid/widget/ImageView;

.field private mBackLayout:Landroid/widget/RelativeLayout;

.field private mBackLayoutGap3:Landroid/view/View;

.field private mBackTextView:Landroid/widget/TextView;

.field private mBlurAngleTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

.field mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

.field mBrushEraser:I

.field mBrushEraserLayout:Landroid/widget/RelativeLayout;

.field private mBtnClickEnable:Z

.field mCancelLayout:Landroid/widget/RelativeLayout;

.field mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;

.field mCheckedFrameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field mClickInterval:J

.field private mContentView:Landroid/view/View;

.field mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

.field private mCurTouchedId:I

.field mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

.field private mCurrentBlurDegree:I

.field private mCurrentBlurSize:I

.field private mDefaultActionBar:Landroid/widget/RelativeLayout;

.field private mDensity:F

.field private mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDividerInCancelLeft:Landroid/widget/ImageView;

.field mDoneLayout:Landroid/widget/RelativeLayout;

.field mDownArrowsButton:Landroid/widget/ImageView;

.field mDownArrowsLayout:Landroid/widget/RelativeLayout;

.field private mDownArrowsView:Landroid/widget/RelativeLayout;

.field private mDrawMaskButtonsEnable:Z

.field mEditEnable:Z

.field private mEditImageView:Landroid/widget/ImageView;

.field private mEditLayout:Landroid/widget/RelativeLayout;

.field private mEditLayoutGap3:Landroid/view/View;

.field mEditMaskBar:Landroid/widget/RelativeLayout;

.field mEditMaskBarShowAnim:Landroid/animation/Animator;

.field mEditMaskBarUnShowAnim:Landroid/animation/Animator;

.field private mEditTextView:Landroid/widget/TextView;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field mEidtShowFrameList:Landroid/animation/AnimatorSet;

.field mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

.field private mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

.field mEraserSelector:Landroid/widget/ImageView;

.field private mEraserState:Z

.field private mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

.field mFileSaving:Z

.field mFramViewLinearLayout:Landroid/view/View;

.field mFrameEnable:Z

.field mFrameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field private mFrameMaskIndex:I

.field mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

.field private mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

.field mIBBrushEraser:Landroid/widget/ImageView;

.field mIBRestore:Landroid/widget/ImageView;

.field mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

.field private mIsChangeFrame:Z

.field mIsFrameListShow:Z

.field private mIsTouchLocked:Z

.field private final mLandscapeSideMargin:I

.field private final mLandscapewidth:I

.field private mLargeBitmap:Landroid/graphics/Bitmap;

.field mLastClickTime:J

.field private mMainLock:Ljava/lang/Object;

.field mMainTHCallback:Landroid/os/Handler$Callback;

.field mMainThreadHandler:Landroid/os/Handler;

.field private mManualDescriptionLayout:Landroid/widget/RelativeLayout;

.field mMaskActionBar:Landroid/widget/RelativeLayout;

.field private mMenuLayout:Landroid/widget/RelativeLayout;

.field private mMenuLayoutGap3:Landroid/view/View;

.field private mMenuTextView:Landroid/widget/TextView;

.field private mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

.field mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnKeyListener:Landroid/view/View$OnKeyListener;

.field mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private volatile mOperateEnable:Z

.field mOrientation:I

.field private mPicMotionBottom_Layout:Landroid/view/View;

.field private mPicMotionTextBlur:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mPicMotionTextObject:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

.field private mPicMotion_Motion_Blur_Layout:Landroid/view/View;

.field private mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

.field private mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

.field private mPicMotion_Obiect_Layout:Landroid/view/View;

.field mPreposition:I

.field private mProcessLock:Ljava/util/concurrent/Semaphore;

.field mProgressDialog:Landroid/app/Dialog;

.field private mRedoButton:Landroid/widget/ImageView;

.field mRedoLayout:Landroid/widget/RelativeLayout;

.field mRestoreLayout:Landroid/widget/RelativeLayout;

.field mRestoreSelector:Landroid/widget/ImageView;

.field mSaveEnable:Z

.field private mSaveImageView:Landroid/widget/ImageView;

.field private mSaveLayout:Landroid/widget/RelativeLayout;

.field private mSaveLayoutGap3:Landroid/view/View;

.field private mSaveLock:Ljava/util/concurrent/Semaphore;

.field mSavePath:Ljava/lang/String;

.field private mSaveTextView:Landroid/widget/TextView;

.field private mSaveThread:Ljava/lang/Thread;

.field private mSeekBarLayout:Landroid/widget/RelativeLayout;

.field private mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

.field private mSeekBarTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mSeekMotionBlurText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mSelectIdx:[B

.field mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mShotMask:[B

.field private mSrcImageHeight:I

.field private mSrcImageWidth:I

.field mSwithCheckRunnable:Ljava/lang/Runnable;

.field mSwithCheckThread:Landroid/os/HandlerThread;

.field mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mThumbnailBitmaps:[Landroid/graphics/Bitmap;

.field private mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUnRedoLockedTime:J

.field private mUndoButton:Landroid/widget/ImageView;

.field mUndoLayout:Landroid/widget/RelativeLayout;

.field private mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

.field private final mVierticalSideMargin:I

.field private final mVierticalwidth:I

.field mVisibility_ANGLE_EDIT_IMG_Layout:I

.field mVisibility_ANGLE_IMG_Layout:I

.field mVisibility_Actionbar:I

.field mVisibility_Blur_Layout:I

.field mVisibility_Description:I

.field mVisibility_EditMaskBar:I

.field mVisibility_FramView:I

.field mVisibility_MotionBlurArea:I

.field mVisibility_Motion_Blur_List_Layout:I

.field mVisibility_Obiect_Layout:I

.field mVisibility_mDownArrows:I

.field mVisibility_mUpArrows:I

.field mWitchCheckThreadHandler:Landroid/os/Handler;

.field private mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

.field private mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

.field private m_MotionBlurArea:Landroid/widget/RelativeLayout;

.field private m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

.field private mbIsAnimDefaultBarIn:Z

.field private final mbIsAnimationOpen:Z

.field mbIsSaveBtnOn:Z

.field mbShowEraserState:Z

.field mbShowSelectDescription:Z

.field private tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-class v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    .line 90
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    .line 91
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBlurAngleTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 93
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    .line 94
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    .line 95
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    .line 96
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    .line 98
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    .line 99
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    .line 100
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    .line 101
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    .line 102
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    .line 103
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 104
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDividerInCancelLeft:Landroid/widget/ImageView;

    .line 105
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionTextBlur:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 106
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionTextObject:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 109
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsFrameListShow:Z

    .line 112
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    .line 114
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    .line 115
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    .line 117
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    .line 119
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 121
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsAnimationOpen:Z

    .line 123
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .line 124
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    .line 135
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 137
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

    .line 138
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    .line 139
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    .line 141
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    .line 142
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarLayout:Landroid/widget/RelativeLayout;

    .line 143
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekMotionBlurText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 145
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    .line 146
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 147
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    .line 149
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    .line 150
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditTextView:Landroid/widget/TextView;

    .line 151
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackTextView:Landroid/widget/TextView;

    .line 152
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    .line 154
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    .line 155
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    .line 158
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    .line 159
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    .line 162
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    .line 163
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->MAX_SOUND_STREAM:I

    .line 183
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    .line 184
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    .line 190
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 191
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mContentView:Landroid/view/View;

    .line 206
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->UNKNOWN:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    .line 207
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    .line 209
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    .line 212
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSrcImageWidth:I

    .line 213
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSrcImageHeight:I

    .line 214
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 215
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    .line 216
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    .line 217
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    .line 218
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 220
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDensity:F

    .line 222
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I

    .line 223
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I

    .line 229
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    .line 230
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    .line 231
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    .line 233
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainLock:Ljava/lang/Object;

    .line 236
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z

    .line 238
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 249
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckRunnable:Ljava/lang/Runnable;

    .line 280
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckThread:Landroid/os/HandlerThread;

    .line 281
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mWitchCheckThreadHandler:Landroid/os/Handler;

    .line 282
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainThreadHandler:Landroid/os/Handler;

    .line 284
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainTHCallback:Landroid/os/Handler$Callback;

    .line 761
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOperateEnable:Z

    .line 783
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    .line 921
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1015
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$5;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$5;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mArrowClickLis:Landroid/view/View$OnClickListener;

    .line 1076
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$6;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$6;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1095
    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J

    .line 1097
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBtnClickEnable:Z

    .line 1106
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$7;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 1403
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$8;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 1475
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameMaskIndex:I

    .line 1476
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    .line 1478
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEraserState:Z

    .line 1619
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsAnimDefaultBarIn:Z

    .line 1620
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 1621
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 1676
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditEnable:Z

    .line 1677
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveEnable:Z

    .line 1678
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameEnable:Z

    .line 1801
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    .line 1868
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFileSaving:Z

    .line 1869
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveThread:Ljava/lang/Thread;

    .line 1910
    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLastClickTime:J

    .line 1911
    const-wide/16 v0, 0x190

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mClickInterval:J

    .line 1912
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    .line 1913
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$12;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2133
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    .line 2326
    const/16 v0, 0xa2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLandscapeSideMargin:I

    .line 2327
    const/16 v0, 0x58

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVierticalSideMargin:I

    .line 2328
    const/16 v0, 0x68

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLandscapewidth:I

    .line 2329
    const/16 v0, 0x4c

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVierticalwidth:I

    .line 2493
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowSelectDescription:Z

    .line 2494
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ViewsShow:Z

    .line 2495
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowEraserState:Z

    .line 2605
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsTouchLocked:Z

    .line 2606
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurTouchedId:I

    .line 2607
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$13;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$13;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 2757
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->FLAG_FROM_ALG_TO_UI:I

    .line 2758
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->FLAG_FROM_UI_TO_ALG:I

    return-void
.end method

.method private ProcessCheckedFrame()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 2382
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 2383
    .local v1, "returnBitmap":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    const/4 v0, 0x0

    .line 2384
    .local v0, "res":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    if-eqz v2, :cond_0

    .line 2389
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->requestAutoResult()I

    .line 2390
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    invoke-virtual {v2, v3, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->applySelected([BLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 2393
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResultImageBySelect res:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    return-object v2
.end method

.method private ShowFrameListAnim(Z)V
    .locals 4
    .param p1, "bShow"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1044
    if-eqz p1, :cond_4

    .line 1045
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowSelectDescription:Z

    if-eqz v0, :cond_3

    const v0, 0x7f060003

    :goto_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showDescription(I)V

    .line 1046
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1050
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    .line 1051
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1052
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1056
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1073
    :cond_2
    :goto_1
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsFrameListShow:Z

    .line 1074
    return-void

    :cond_3
    move v0, v1

    .line 1045
    goto :goto_0

    .line 1058
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 1059
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1063
    :cond_5
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    .line 1064
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1065
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->resetUndoRedo()V

    return-void
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->popUpToast(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ShowFrameListAnim(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->restoreLayoutClicked()V

    return-void
.end method

.method static synthetic access$1400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->eraserLayoutClicked()V

    return-void
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showMaskActionbar()V

    return-void
.end method

.method static synthetic access$1700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->exitWidthoutSave()V

    return-void
.end method

.method static synthetic access$2200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->cancelMask()V

    return-void
.end method

.method static synthetic access$2300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->resetEditView()V

    return-void
.end method

.method static synthetic access$2400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I

    return v0
.end method

.method static synthetic access$2702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I

    return p1
.end method

.method static synthetic access$2800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I

    return v0
.end method

.method static synthetic access$2802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I

    return p1
.end method

.method static synthetic access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ProcessCheckedFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->backToSelectFrame()V

    return-void
.end method

.method static synthetic access$3400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;II)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->convertDegreeBetweenAlgAndUI(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$3600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSharedPref:Landroid/content/SharedPreferences;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Landroid/content/SharedPreferences$Editor;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditor:Landroid/content/SharedPreferences$Editor;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    return-object p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    return-object v0
.end method

.method static synthetic access$4000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->putShotOnTop(I)V

    return-void
.end method

.method static synthetic access$4100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showActionBar()V

    return-void
.end method

.method static synthetic access$4200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J

    return-wide v0
.end method

.method static synthetic access$4202(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # J

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUnRedoLockedTime:J

    return-wide p1
.end method

.method static synthetic access$4300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEraserState:Z

    return v0
.end method

.method static synthetic access$4700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showDescription(I)V

    return-void
.end method

.method static synthetic access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkEditable()V

    return-void
.end method

.method static synthetic access$5300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initCheckedFrameList()V

    return-void
.end method

.method static synthetic access$5400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsTouchLocked:Z

    return v0
.end method

.method static synthetic access$5402(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsTouchLocked:Z

    return p1
.end method

.method static synthetic access$5500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurTouchedId:I

    return v0
.end method

.method static synthetic access$5502(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurTouchedId:I

    return p1
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    return v0
.end method

.method static synthetic access$602(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    return p1
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$702(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)[B
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    return-object v0
.end method

.method static synthetic access$802(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .param p1, "x1"    # [B

    .prologue
    .line 76
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    return-object p1
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    return-object v0
.end method

.method private backToSelectFrame()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2317
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 2318
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setItemActivated(Landroid/view/View;I)V

    .line 2319
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showDefaultActionbar()V

    .line 2321
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2322
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2323
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2324
    return-void
.end method

.method private cancelMask()V
    .locals 3

    .prologue
    .line 885
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 886
    .local v1, "retBitmap":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->cancelEdit(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 887
    .local v0, "res":I
    if-nez v0, :cond_0

    .line 888
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 891
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->backToSelectFrame()V

    .line 892
    return-void
.end method

.method private checkEditable()V
    .locals 4

    .prologue
    .line 2206
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setEditableState(Z)V

    .line 2208
    const/4 v2, 0x0

    .line 2209
    .local v2, "size":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 2210
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2213
    :cond_0
    if-lez v2, :cond_1

    .line 2214
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2215
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    .line 2216
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    iget-boolean v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    if-eqz v3, :cond_2

    .line 2217
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setEditableState(Z)V

    .line 2222
    .end local v0    # "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 2214
    .restart local v0    # "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    .restart local v1    # "i":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private checkUndoRedoState()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->checkUndoEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 474
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->checkRedoEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 476
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V

    .line 478
    return-void

    .line 476
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearItemActivatedAndFocus()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1996
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    if-nez v3, :cond_1

    .line 2006
    :cond_0
    return-void

    .line 2000
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2001
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2002
    .local v0, "child":Landroid/view/View;
    const v3, 0x7f090090

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2003
    .local v2, "selectorView":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/view/View;->setPressed(Z)V

    .line 2004
    invoke-virtual {v2, v4}, Landroid/view/View;->setActivated(Z)V

    .line 2000
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private convertDegreeBetweenAlgAndUI(II)I
    .locals 3
    .param p1, "degree"    # I
    .param p2, "flag"    # I

    .prologue
    const/4 v0, -0x1

    .line 2761
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    const/16 v2, 0xb4

    if-ne v1, v2, :cond_1

    .line 2762
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2778
    :goto_0
    return v0

    .line 2764
    :pswitch_0
    rsub-int v0, p1, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 2766
    :pswitch_1
    rsub-int v0, p1, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 2772
    :cond_1
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 2774
    :pswitch_2
    add-int/lit8 v0, p1, 0x5a

    rem-int/lit16 v0, v0, 0x168

    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 2776
    :pswitch_3
    add-int/lit8 v0, p1, 0x5a

    rem-int/lit16 v0, v0, 0x168

    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 2762
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2772
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private enableMaskbar(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 786
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    .line 788
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 789
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 790
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 792
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 793
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 795
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setEnabled(Z)V

    .line 796
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 797
    return-void
.end method

.method private eraserLayoutClicked()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2715
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 2716
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    if-eqz v0, :cond_1

    .line 2717
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V

    .line 2718
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowEraserState:Z

    .line 2719
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsFrameListShow:Z

    if-nez v0, :cond_0

    .line 2720
    const v0, 0x7f060049

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showDescription(I)V

    .line 2722
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setRubberValid()V

    .line 2725
    :cond_1
    return-void
.end method

.method private exitWidthoutSave()V
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1865
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    .line 1866
    return-void
.end method

.method private fetchAutoResult()V
    .locals 4

    .prologue
    .line 2108
    const/4 v0, 0x0

    .line 2109
    .local v0, "res":I
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 2110
    .local v1, "rtBitmap":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getAutoResult(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 2111
    if-eqz v0, :cond_0

    .line 2112
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "Picmotion getCandidateThumbs is null!"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    invoke-virtual {v2, v3, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->applySelected([BLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 2115
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateCurrentBlurInfo()V

    .line 2116
    if-eqz v0, :cond_1

    .line 2117
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "fail applySelected !!!!!!!"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    :cond_1
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    .line 2120
    return-void
.end method

.method private fetchCandidate()V
    .locals 6

    .prologue
    .line 2078
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 2079
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v4, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2082
    const/4 v3, 0x0

    .line 2083
    .local v3, "width":I
    const/4 v0, 0x0

    .line 2085
    .local v0, "height":I
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSrcImageWidth:I

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSrcImageHeight:I

    if-le v4, v5, :cond_0

    .line 2086
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050032

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2087
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050033

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2093
    :goto_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    if-nez v4, :cond_1

    .line 2105
    :goto_1
    return-void

    .line 2089
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050034

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2090
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 2097
    :cond_1
    const/4 v1, 0x0

    .line 2098
    .local v1, "res":I
    new-instance v2, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;

    invoke-direct {v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;-><init>()V

    .line 2099
    .local v2, "returnThumbs":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v4, v3, v0, v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getCandidateThumbs(IILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;)I

    move-result v1

    .line 2100
    if-eqz v1, :cond_2

    .line 2101
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v5, "Drama getCandidateThumbs is null!"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2104
    :cond_2
    iget-object v4, v2, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;->mThumbs:[Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method private fetchSelectedFlags()V
    .locals 4

    .prologue
    .line 2123
    const/4 v0, 0x0

    .line 2124
    .local v0, "res":I
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;-><init>()V

    .line 2125
    .local v1, "rtSelected":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getSelected(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;)I

    move-result v0

    .line 2126
    if-eqz v0, :cond_0

    .line 2127
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "Picmotion getSelected is null!"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2131
    :goto_0
    return-void

    .line 2130
    :cond_0
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;->mFlags:[B

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    goto :goto_0
.end method

.method private getCurrentBlurInfo()V
    .locals 2

    .prologue
    .line 2728
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    if-eqz v1, :cond_0

    .line 2730
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;-><init>()V

    .line 2731
    .local v0, "blurInfo":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getBlurInfo(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;)I

    .line 2732
    iget v1, v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;->mBlurDegree:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I

    .line 2733
    iget v1, v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;->mBlurSize:I

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I

    .line 2735
    .end local v0    # "blurInfo":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;
    :cond_0
    return-void
.end method

.method private getShotMaskByIndex(I)[B
    .locals 4
    .param p1, "shotIndex"    # I

    .prologue
    .line 2009
    const/4 v0, 0x0

    .line 2010
    .local v0, "mask":[B
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;-><init>()V

    .line 2011
    .local v1, "returnMask":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v3, p1, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getObjectMask(ILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;)I

    move-result v2

    .line 2012
    .local v2, "success":I
    if-nez v2, :cond_0

    .line 2013
    iget-object v0, v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;->mMask:[B

    .line 2015
    :cond_0
    return-object v0
.end method

.method private hideDefaultBars()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1705
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1706
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1709
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1713
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1714
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1715
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1716
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1720
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1721
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1722
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1723
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1726
    :cond_2
    return-void
.end method

.method private hideFrameCandidateListView()V
    .locals 2

    .prologue
    .line 1741
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1742
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1747
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1750
    :cond_0
    return-void
.end method

.method private inflateRollWaitingView()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2033
    new-instance v3, Landroid/app/Dialog;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {v3, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    .line 2034
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2035
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f03002f

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2036
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0900e5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 2037
    .local v1, "pBar4":Landroid/widget/ProgressBar;
    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2039
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 2040
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v3, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 2041
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/high16 v4, 0x7f040000

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 2043
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 2044
    return-void
.end method

.method private initCheckedFrameList()V
    .locals 3

    .prologue
    .line 2136
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInitCheckedFrameList()V

    .line 2137
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    .line 2138
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2140
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 2141
    .local v1, "iter":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2142
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    .line 2143
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    if-eqz v2, :cond_0

    .line 2144
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2147
    .end local v0    # "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    :cond_1
    return-void
.end method

.method private initFrameList()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 2150
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInitFrameList()V

    .line 2151
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    .line 2152
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 2154
    const/4 v0, 0x0

    .line 2155
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v5, :cond_0

    .line 2156
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    array-length v0, v5

    .line 2158
    :cond_0
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "arrayLength is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getMotionDirection(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;)I

    .line 2160
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ArcDrama mOrientation is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ArcDrama Direction is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    iget v7, v7, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getBestImageIndex()I

    move-result v1

    .line 2163
    .local v1, "bestIndex":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_5

    .line 2164
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    invoke-direct {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;-><init>()V

    .line 2166
    .local v2, "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    iget v6, v6, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    if-eq v5, v6, :cond_1

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    iget v6, v6, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    if-ne v5, v6, :cond_3

    .line 2168
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    add-int/lit8 v6, v0, -0x1

    sub-int/2addr v6, v3

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    add-int/lit8 v8, v0, -0x1

    sub-int/2addr v8, v3

    aget-object v7, v7, v8

    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v7

    aput-object v7, v5, v6

    .line 2171
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    add-int/lit8 v6, v0, -0x1

    sub-int/2addr v6, v3

    aget-object v5, v5, v6

    iput-object v5, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    .line 2179
    add-int/lit8 v5, v0, -0x1

    sub-int v4, v5, v1

    .line 2180
    .local v4, "rightIndex":I
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    .line 2181
    if-ne v4, v3, :cond_2

    .line 2182
    iput-boolean v9, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    .line 2183
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    int-to-byte v6, v1

    aput-byte v6, v5, v10

    .line 2196
    .end local v4    # "rightIndex":I
    :cond_2
    :goto_1
    iget-object v5, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->grey(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mGreyFrame:Landroid/graphics/Bitmap;

    .line 2197
    iput v3, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mPosition:I

    .line 2198
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2163
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2185
    :cond_3
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    iget v5, v5, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMotion:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    iget v5, v5, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;->mDirection:I

    if-ne v9, v5, :cond_2

    .line 2187
    :cond_4
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v3

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v5, v3

    .line 2189
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mThumbnailBitmaps:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v3

    iput-object v5, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    .line 2190
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPreposition:I

    .line 2191
    if-ne v1, v3, :cond_2

    .line 2192
    iput-boolean v9, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    .line 2193
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectIdx:[B

    int-to-byte v6, v1

    aput-byte v6, v5, v10

    goto :goto_1

    .line 2201
    .end local v2    # "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    :cond_5
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkEditable()V

    .line 2202
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initCheckedFrameList()V

    .line 2203
    return-void
.end method

.method private initSound()V
    .locals 0

    .prologue
    .line 2023
    return-void
.end method

.method private isTalkBackOn()Z
    .locals 1

    .prologue
    .line 2817
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->isTalkBackOn()Z

    move-result v0

    return v0
.end method

.method private popUpToast(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 937
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    .line 939
    move-object v0, p1

    .line 940
    .local v0, "pressedView":Landroid/view/View;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->getWidth()I

    move-result v1

    .line 941
    .local v1, "screenWith":I
    const/4 v2, 0x0

    .line 943
    .local v2, "textRes":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 1002
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 1003
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v3, v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showPopUpToast(Landroid/view/View;II)V

    .line 1006
    :cond_1
    const/4 v3, 0x1

    return v3

    .line 945
    :sswitch_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 948
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    .line 949
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v3, :cond_2

    const v2, 0x7f06000c

    .line 950
    :goto_1
    goto :goto_0

    .line 949
    :cond_2
    const v2, 0x7f060018

    goto :goto_1

    .line 952
    :sswitch_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 955
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    .line 956
    const v2, 0x7f06000b

    .line 957
    goto :goto_0

    .line 959
    :sswitch_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 962
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackImageView:Landroid/widget/ImageView;

    .line 963
    const v2, 0x7f06000e

    .line 964
    goto :goto_0

    .line 966
    :sswitch_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 969
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 970
    const v2, 0x7f060027

    .line 971
    goto :goto_0

    .line 973
    :sswitch_4
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 976
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    .line 977
    const v2, 0x7f060009

    .line 978
    goto :goto_0

    .line 980
    :sswitch_5
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 983
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    .line 984
    const v2, 0x7f06000a

    .line 985
    goto :goto_0

    .line 987
    :sswitch_6
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 990
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    .line 991
    const v2, 0x7f060008

    .line 992
    goto :goto_0

    .line 994
    :sswitch_7
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 997
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    .line 998
    const v2, 0x7f060007

    goto :goto_0

    .line 943
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_3
        0x7f09004c -> :sswitch_2
        0x7f090053 -> :sswitch_0
        0x7f090075 -> :sswitch_1
        0x7f090089 -> :sswitch_4
        0x7f09008c -> :sswitch_5
        0x7f0900cf -> :sswitch_6
        0x7f0900d3 -> :sswitch_7
    .end sparse-switch
.end method

.method private putShotOnTop(I)V
    .locals 12
    .param p1, "shotIndex"    # I

    .prologue
    .line 2417
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v9, "putShotOnTop in "

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2418
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v9, "getResultImageByMask in "

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2420
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2422
    .local v4, "start":J
    new-instance v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;

    invoke-direct {v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;-><init>()V

    .line 2423
    .local v3, "returnMask":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v8, p1, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->getObjectMask(ILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;)I

    move-result v6

    .line 2424
    .local v6, "success":I
    if-eqz v6, :cond_0

    .line 2425
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v9, "putShotOnTop getShotMask failed"

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2472
    :goto_0
    return-void

    .line 2429
    :cond_0
    iget v7, v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;->mWidth:I

    .line 2430
    .local v7, "width":I
    iget v2, v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;->mHeight:I

    .line 2432
    .local v2, "height":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "test size match grey width = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2433
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "test size match grey height = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "test size match Bitmap width = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "test size match Bitmap height = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2448
    iget-object v8, v3, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;->mMask:[B

    iput-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    .line 2449
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    if-nez v8, :cond_2

    .line 2450
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v9, "test size match mShotMask = null"

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461
    :goto_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    invoke-virtual {v8, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setMask([B)V

    .line 2462
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    if-eqz v8, :cond_1

    .line 2464
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->setCurData([BZ)V

    .line 2465
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->resetUndoRedo()V

    .line 2467
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2468
    .local v0, "end":J
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getResultImageByMask out cost = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-long v10, v0, v4

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v9, "putShotOnTop out "

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2452
    .end local v0    # "end":J
    :cond_2
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v9, "test size match mShotMask !!!!= null"

    invoke-static {v8, v9}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 2260
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2261
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2263
    :cond_0
    const/4 p1, 0x0

    .line 2264
    return-void
.end method

.method private resetEditView()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2535
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowSelectDescription:Z

    .line 2536
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowEraserState:Z

    .line 2537
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showDescription(I)V

    .line 2538
    return-void
.end method

.method private resetUndoRedo()V
    .locals 1

    .prologue
    .line 2475
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->clear()V

    .line 2476
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkUndoRedoState()V

    .line 2477
    return-void
.end method

.method private restoreLayoutClicked()V
    .locals 1

    .prologue
    .line 2703
    sget-object v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 2704
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    if-eqz v0, :cond_1

    .line 2705
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V

    .line 2706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowEraserState:Z

    .line 2707
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsFrameListShow:Z

    if-nez v0, :cond_0

    .line 2708
    const v0, 0x7f060048

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showDescription(I)V

    .line 2710
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setPenValid()V

    .line 2712
    :cond_1
    return-void
.end method

.method private setEditableState(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 2225
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 2226
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2227
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2229
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V

    .line 2231
    return-void

    .line 2229
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setItemActivated(Landroid/view/View;I)V
    .locals 2
    .param p1, "item"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 1986
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->clearItemActivatedAndFocus()V

    .line 1987
    if-eqz p1, :cond_0

    .line 1988
    const v1, 0x7f090090

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1989
    .local v0, "selectorView":Landroid/view/View;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 1992
    .end local v0    # "selectorView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;

    invoke-virtual {v1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->setActivated(I)V

    .line 1993
    return-void
.end method

.method private showActionBar()V
    .locals 1

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1646
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1647
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->animDefaultBarIn()V

    .line 1651
    :cond_0
    :goto_0
    return-void

    .line 1648
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1649
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    goto :goto_0
.end method

.method private showDefaultActionbar()V
    .locals 2

    .prologue
    .line 777
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->changeMode(Z)V

    .line 778
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 780
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 781
    return-void
.end method

.method private showDefaultBars()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1681
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1682
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1684
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1688
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1689
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1690
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1691
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1695
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1696
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1697
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1698
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1702
    :cond_2
    return-void
.end method

.method private showDescription(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 2572
    if-gtz p1, :cond_1

    .line 2573
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2582
    :cond_0
    :goto_0
    return-void

    .line 2576
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2577
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 2579
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 2580
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method private showEditMaskBarAnim(Z)V
    .locals 2
    .param p1, "bShow"    # Z

    .prologue
    .line 1031
    if-eqz p1, :cond_1

    .line 1032
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1033
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1040
    :cond_0
    :goto_0
    return-void

    .line 1036
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1037
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method private showFrameCandidateListView()V
    .locals 2

    .prologue
    .line 1729
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1730
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1735
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1738
    :cond_0
    return-void
.end method

.method private showMaskActionbar()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 772
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->changeMode(Z)V

    .line 773
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->enableMaskbar(Z)V

    .line 774
    return-void
.end method

.method private switchRestoreAndEraserButtonFocus(Z)V
    .locals 5
    .param p1, "isEraserButtonFocused"    # Z

    .prologue
    const v4, 0x7f020067

    const v3, 0x7f020051

    const/4 v0, 0x0

    .line 895
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEraserState:Z

    .line 897
    if-eqz p1, :cond_1

    .line 898
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 899
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v2, 0x7f02003b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 901
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setRubberValid()V

    .line 902
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const v2, 0x7f060049

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 904
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraser:I

    .line 915
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 917
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V

    .line 919
    return-void

    .line 906
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    const v2, 0x7f020040

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 908
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 909
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setPenValid()V

    .line 910
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const v2, 0x7f060048

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 912
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraser:I

    goto :goto_0
.end method

.method private switchTalkBackMode(Z)V
    .locals 14
    .param p1, "isLandScape"    # Z

    .prologue
    .line 800
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 801
    .local v4, "res":Landroid/content/res/Resources;
    const v11, 0x7f06000b

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 802
    .local v1, "edit":Ljava/lang/String;
    const v11, 0x7f060012

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 803
    .local v0, "disabled":Ljava/lang/String;
    const v11, 0x7f060011

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 804
    .local v2, "notSelected":Ljava/lang/String;
    const v11, 0x7f060010

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 805
    .local v5, "selected":Ljava/lang/String;
    const v11, 0x7f06000e

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 806
    .local v6, "szCancel":Ljava/lang/String;
    const v11, 0x7f060008

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 807
    .local v9, "szRestore":Ljava/lang/String;
    const v11, 0x7f060007

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 808
    .local v8, "szEraser":Ljava/lang/String;
    const v11, 0x7f06000f

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 809
    .local v7, "szDone":Ljava/lang/String;
    const v11, 0x7f060009

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 810
    .local v10, "undo":Ljava/lang/String;
    const v11, 0x7f06000a

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 812
    .local v3, "redo":Ljava/lang/String;
    sget-object v11, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    if-ne v11, v12, :cond_0

    .line 813
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 814
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 824
    :cond_0
    :goto_0
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 825
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v10}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 834
    :goto_1
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 835
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 844
    :goto_2
    const-string v11, "dds"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mDrawMaskButtonsEnable ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iget-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    if-eqz v11, :cond_a

    .line 846
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v6}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 847
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v7}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 849
    const-string v11, "dds"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mBrushEraser ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraser:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraser:I

    const v12, 0x7f020051

    if-ne v11, v12, :cond_8

    .line 851
    const-string v11, "dds"

    const-string v12, "mBrushEraser = brush_button"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 853
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 854
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 882
    :goto_3
    return-void

    .line 816
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 817
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 819
    :cond_2
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 827
    :cond_3
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 828
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 830
    :cond_4
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v10}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 837
    :cond_5
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 838
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 840
    :cond_6
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 856
    :cond_7
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 857
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 860
    :cond_8
    const-string v11, "dds"

    const-string v12, "mBrushEraser = erase_button"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 862
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 863
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 865
    :cond_9
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 866
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 871
    :cond_a
    const-string v11, "dds"

    const-string v12, "mRestoreLayout mBrushEraserLayout mDoneLayout disable"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isTalkBackOn()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 873
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 874
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 875
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 877
    :cond_b
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v9}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 878
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v8}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 879
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v11, v7}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method private unInitCheckedFrameList()V
    .locals 1

    .prologue
    .line 2234
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2240
    :cond_0
    :goto_0
    return-void

    .line 2238
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private unInitFrameList()V
    .locals 5

    .prologue
    .line 2243
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 2257
    :cond_0
    :goto_0
    return-void

    .line 2247
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2248
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 2249
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    .line 2250
    .local v2, "tmp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    iget-object v3, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 2251
    iget-object v3, v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mGreyFrame:Landroid/graphics/Bitmap;

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 2248
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2253
    .end local v2    # "tmp":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2254
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    .line 2256
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInitCheckedFrameList()V

    goto :goto_0
.end method

.method private unInitSound()V
    .locals 0

    .prologue
    .line 2030
    return-void
.end method

.method private updateBottomLayout(ZF)V
    .locals 10
    .param p1, "isLandscape"    # Z
    .param p2, "density"    # F

    .prologue
    const/high16 v9, 0x43220000    # 162.0f

    const/high16 v8, 0x42d00000    # 104.0f

    const/high16 v7, 0x42b00000    # 88.0f

    const/high16 v6, 0x42980000    # 76.0f

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 2332
    const/4 v0, 0x0

    .line 2333
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_0

    .line 2334
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2335
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    mul-float v1, v9, p2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2336
    mul-float v1, v8, p2

    float-to-double v2, v1

    add-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2337
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2339
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2340
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    mul-float v1, v9, p2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2341
    mul-float v1, v8, p2

    float-to-double v2, v1

    add-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2342
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2343
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2357
    :goto_0
    return-void

    .line 2345
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2346
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    mul-float v1, v7, p2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2347
    mul-float v1, v6, p2

    float-to-double v2, v1

    add-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2348
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2350
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2351
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    mul-float v1, v7, p2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2352
    mul-float v1, v6, p2

    float-to-double v2, v1

    add-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2353
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2354
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private updateGapValue(ZF)V
    .locals 4
    .param p1, "isLandScape"    # Z
    .param p2, "mScreenDensity"    # F

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 333
    const/4 v0, 0x0

    .line 334
    .local v0, "param":Landroid/view/ViewGroup$LayoutParams;
    if-eqz p1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setVisibility(I)V

    .line 336
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setVisibility(I)V

    .line 339
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 341
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 344
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 346
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 347
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 360
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 351
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 352
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 353
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 355
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 356
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 357
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 358
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public animDefaultBarIn()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1623
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-nez v3, :cond_0

    .line 1642
    :goto_0
    return-void

    .line 1627
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_1

    .line 1628
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 1629
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 1630
    .local v2, "move":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const-string v4, "translationY"

    new-array v5, v10, [F

    neg-int v6, v2

    int-to-float v6, v6

    aput v6, v5, v9

    aput v8, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1632
    .local v1, "deaultbarIn":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1633
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    const-string v4, "translationY"

    new-array v5, v10, [F

    int-to-float v6, v2

    aput v6, v5, v9

    aput v8, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1636
    .local v0, "bottonListIn":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1639
    .end local v0    # "bottonListIn":Landroid/animation/Animator;
    .end local v1    # "deaultbarIn":Landroid/animation/Animator;
    .end local v2    # "move":I
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 1641
    iput-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsAnimDefaultBarIn:Z

    goto :goto_0
.end method

.method public animDefaultBarOut()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1654
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-nez v3, :cond_0

    .line 1673
    :goto_0
    return-void

    .line 1658
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_1

    .line 1659
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 1660
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 1661
    .local v2, "move":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    const-string v4, "translationY"

    new-array v5, v10, [F

    aput v8, v5, v7

    neg-int v6, v2

    int-to-float v6, v6

    aput v6, v5, v9

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1663
    .local v1, "deaultbarOut":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1664
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    const-string v4, "translationY"

    new-array v5, v10, [F

    aput v8, v5, v7

    int-to-float v6, v2

    aput v6, v5, v9

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1667
    .local v0, "bottonListOut":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1670
    .end local v0    # "bottonListOut":Landroid/animation/Animator;
    .end local v1    # "deaultbarOut":Landroid/animation/Animator;
    .end local v2    # "move":I
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 1672
    iput-boolean v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsAnimDefaultBarIn:Z

    goto :goto_0
.end method

.method public changeMode(Z)V
    .locals 5
    .param p1, "isAuto2Manual"    # Z

    .prologue
    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 1432
    if-eqz p1, :cond_1

    .line 1434
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setImageIndex(I)V

    .line 1435
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setManualMask(Z)V

    .line 1437
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1438
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1439
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMaskActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1440
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1441
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1442
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1443
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1445
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-string v2, "UserStack"

    invoke-virtual {v1, v2, v3}, Landroid/app/Application;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    .line 1471
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1472
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->setmIsMunualMode(Z)V

    .line 1473
    return-void

    .line 1448
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->eraseAllMask()V

    .line 1449
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setManualMask(Z)V

    .line 1450
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getIsMaskShow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1451
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setImageIndex(I)V

    .line 1456
    :goto_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1457
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1458
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMaskActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1459
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1460
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1461
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1462
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    if-eqz v0, :cond_0

    .line 1463
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->clear()V

    .line 1464
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1465
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1467
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    goto :goto_0

    .line 1453
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setImageIndex(I)V

    goto :goto_1
.end method

.method public changeSaveBtnState(Z)V
    .locals 5
    .param p1, "isSaveState"    # Z

    .prologue
    const v3, 0x7f060018

    const v2, 0x7f06000c

    .line 1391
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1392
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const v1, 0x7f02000f

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1393
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1395
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 1396
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1398
    .local v0, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1400
    .end local v0    # "description":Ljava/lang/String;
    :cond_1
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    .line 1401
    return-void

    .line 1392
    :cond_2
    const v1, 0x7f020013

    goto :goto_0

    :cond_3
    move v1, v3

    .line 1393
    goto :goto_1

    :cond_4
    move v2, v3

    .line 1396
    goto :goto_2
.end method

.method public doMotionBlur(I)V
    .locals 4
    .param p1, "lDegree"    # I

    .prologue
    .line 2682
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->convertDegreeBetweenAlgAndUI(II)I

    move-result v0

    .line 2683
    .local v0, "algDegree":I
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 2684
    .local v1, "result":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v0, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->updateBlurInfo(IILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    .line 2685
    iget-object v2, v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    .line 2686
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 2687
    return-void
.end method

.method public endAdjust()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2796
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    if-eqz v0, :cond_0

    .line 2798
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2799
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->setVisibility(I)V

    .line 2801
    :cond_0
    return-void
.end method

.method public getAngle(I)V
    .locals 3
    .param p1, "lDegree"    # I

    .prologue
    .line 2691
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBlurAngleTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2692
    return-void
.end method

.method public getApplyMask()[B
    .locals 1

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    return-object v0
.end method

.method public getOperateEnable()Z
    .locals 1

    .prologue
    .line 768
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOperateEnable:Z

    return v0
.end method

.method public getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    return-object v0
.end method

.method public hideAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 2514
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2515
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2516
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2517
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2518
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2519
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2522
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2523
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2524
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2525
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2526
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2527
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2528
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2530
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowSelectDescription:Z

    .line 2531
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowEraserState:Z

    .line 2532
    return-void
.end method

.method public hideHelpView()V
    .locals 1

    .prologue
    .line 2811
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v0, :cond_0

    .line 2812
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    .line 2814
    :cond_0
    return-void
.end method

.method public hidePopupToast()V
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    if-eqz v0, :cond_0

    .line 1011
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->hide()V

    .line 1013
    :cond_0
    return-void
.end method

.method public hideProgressDialog()V
    .locals 1

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 2054
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2056
    :cond_0
    return-void
.end method

.method public initAnimation()V
    .locals 14

    .prologue
    .line 1533
    const/high16 v4, 0x42dc0000    # 110.0f

    .line 1534
    .local v4, "move_len":F
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    if-eqz v9, :cond_0

    .line 1535
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v4, v9

    .line 1538
    :cond_0
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_0

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarShowAnim:Landroid/animation/Animator;

    .line 1540
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_1

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBarUnShowAnim:Landroid/animation/Animator;

    .line 1544
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    .line 1548
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v4, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1550
    .local v0, "downArrowMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    const-string v10, "rotation"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_2

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1552
    .local v1, "downArrowRotate":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v4, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1556
    .local v2, "downFrameListMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_3

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1558
    .local v3, "downText":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v9, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1561
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    .line 1565
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x1

    aput v4, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1567
    .local v5, "upArrowMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    const-string v10, "rotation"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_4

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1569
    .local v6, "upArrowRotate":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x1

    aput v4, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 1572
    .local v7, "upFrameListMove":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_5

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 1574
    .local v8, "upText":Landroid/animation/Animator;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v9, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1576
    new-instance v9, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$9;

    invoke-direct {v9, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$9;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    invoke-virtual {v8, v9}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1600
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const-string v10, "rotationX"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_6

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    .line 1605
    return-void

    .line 1538
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1540
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1550
    :array_2
    .array-data 4
        0x0
        0x43340000    # 180.0f
    .end array-data

    .line 1556
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1567
    :array_4
    .array-data 4
        0x43340000    # 180.0f
        0x43b40000    # 360.0f
    .end array-data

    .line 1572
    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1600
    :array_6
    .array-data 4
        0x42b40000    # 90.0f
        0x0
    .end array-data
.end method

.method public initDefaultActionbarUI()V
    .locals 9

    .prologue
    const v6, 0x7f06001e

    const/16 v8, 0x8

    const/4 v5, 0x0

    .line 488
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090041

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    .line 490
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    const v4, 0x7f090074

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 491
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v3, v6}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(I)V

    .line 492
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090053

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    .line 493
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 494
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090075

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    .line 495
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f09004c

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 496
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 497
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090059

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    .line 498
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090042

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    .line 499
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 500
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 501
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v3

    if-nez v3, :cond_0

    .line 502
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 504
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090077

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    .line 506
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090055

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveImageView:Landroid/widget/ImageView;

    .line 508
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackImageView:Landroid/widget/ImageView;

    .line 511
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090043

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 514
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090057

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveTextView:Landroid/widget/TextView;

    .line 515
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090079

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditTextView:Landroid/widget/TextView;

    .line 516
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090051

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackTextView:Landroid/widget/TextView;

    .line 517
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f09005e

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuTextView:Landroid/widget/TextView;

    .line 520
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090058

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayoutGap3:Landroid/view/View;

    .line 521
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f090052

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayoutGap3:Landroid/view/View;

    .line 522
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f09007a

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayoutGap3:Landroid/view/View;

    .line 523
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f09005f

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayoutGap3:Landroid/view/View;

    .line 525
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v4, 0x7f09004d

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDividerInCancelLeft:Landroid/widget/ImageView;

    .line 526
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDividerInCancelLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 528
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_1

    .line 529
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 530
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 531
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 532
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 534
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_2

    .line 535
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 536
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 537
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 539
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_3

    .line 540
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 542
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 545
    :cond_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_4

    .line 546
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 547
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 548
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 551
    :cond_4
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_5

    .line 552
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 554
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 558
    :cond_5
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    .line 559
    .local v2, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 560
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 575
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setEditEnable()V

    .line 577
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 578
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;

    .line 579
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    .line 580
    return-void

    .line 562
    :cond_6
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 563
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_7

    .line 564
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 565
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 566
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 568
    :cond_7
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 569
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    .line 570
    .local v0, "density":F
    const/high16 v3, 0x41600000    # 14.0f

    mul-float/2addr v3, v0

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v3, v4

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 571
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public initDrawMaskActionbarUI()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 362
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900ce

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMaskActionBar:Landroid/widget/RelativeLayout;

    .line 364
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900c4

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    .line 366
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 368
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f09007d

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    .line 370
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900cf

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    .line 372
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 373
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900d3

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    .line 375
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 376
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f09007f

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    .line 377
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900d1

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreSelector:Landroid/widget/ImageView;

    .line 378
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900d5

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEraserSelector:Landroid/widget/ImageView;

    .line 379
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreSelector:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 380
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEraserSelector:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 381
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f090089

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    .line 382
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 383
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f09008c

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    .line 384
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 385
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f09008a

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    .line 386
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f09008d

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    .line 388
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900d4

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    .line 390
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f0900d0

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    .line 393
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f09007e

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 394
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v3, 0x7f090080

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 395
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 396
    .local v0, "cancel":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 397
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "done":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 399
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVCancel:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTVDone:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v3, 0x7f020067

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 405
    const v2, 0x7f020051

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraser:I

    .line 406
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setRubberValid()V

    .line 407
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 408
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 410
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 411
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 413
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 414
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 416
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBrushEraserLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 418
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 419
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 421
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRestoreLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 424
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_3

    .line 425
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 426
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 427
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDoneLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 430
    :cond_3
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    if-eqz v2, :cond_4

    .line 431
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 432
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 433
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 434
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 435
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 438
    :cond_4
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    if-eqz v2, :cond_5

    .line 439
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 440
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 441
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 442
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 443
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 446
    :cond_5
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;

    .line 447
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCheckedFrameList:Ljava/util/ArrayList;

    .line 448
    return-void
.end method

.method public initEditData()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2059
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->fetchCandidate()V

    .line 2060
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->fetchSelectedFlags()V

    .line 2061
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initFrameList()V

    .line 2062
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->fetchAutoResult()V

    .line 2064
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2065
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->setFrameList(Ljava/util/ArrayList;)V

    .line 2066
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAdapter:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2067
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2069
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 2071
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2072
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2074
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public initManualModeUI(Landroid/app/Activity;Landroid/view/View;[ILjava/lang/String;ILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)V
    .locals 7
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "contentView"    # Landroid/view/View;
    .param p3, "diplaySize"    # [I
    .param p4, "savePath"    # Ljava/lang/String;
    .param p5, "ori"    # I
    .param p6, "engineJNI"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 595
    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    .line 596
    check-cast p1, Lcom/arcsoft/magicshotstudio/PicMotion;

    .end local p1    # "context":Landroid/app/Activity;
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    .line 597
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mContentView:Landroid/view/View;

    .line 598
    iput p5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    .line 599
    new-instance v4, Ljava/util/concurrent/Semaphore;

    invoke-direct {v4, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 600
    new-instance v4, Ljava/util/concurrent/Semaphore;

    invoke-direct {v4, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    .line 602
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 604
    iput-object p4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    .line 606
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f09003c

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    .line 607
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v4, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setManual(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 609
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f09006b

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    .line 611
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f09006c

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    .line 613
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900dc

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBlurAngleTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 614
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900a7

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    .line 615
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900cc

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    .line 616
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900cd

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    .line 617
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900ca

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    .line 618
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900c5

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    .line 620
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900df

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionTextBlur:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 621
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900e1

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionTextObject:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 622
    sget-object v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    .line 624
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900c6

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarLayout:Landroid/widget/RelativeLayout;

    .line 625
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;-><init>(Landroid/widget/RelativeLayout;)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    .line 626
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    invoke-virtual {v4, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->setSeekBarListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;)V

    .line 627
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900c8

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekMotionBlurText:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 629
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f0900d8

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    .line 631
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    const v6, 0x7f0900d9

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    .line 632
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    invoke-virtual {v4, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->setListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget$MotionBlurProgressListener;)V

    .line 635
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 636
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 637
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLongClickable(Z)V

    .line 638
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 639
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 643
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 644
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 645
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLongClickable(Z)V

    .line 646
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 647
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 651
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    .line 652
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 653
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLongClickable(Z)V

    .line 654
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 655
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_blur_edit_angle_View:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 658
    :cond_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarLayout:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_3

    .line 663
    :cond_3
    aget v4, p3, v5

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSrcImageWidth:I

    .line 664
    aget v4, p3, v2

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSrcImageHeight:I

    .line 666
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDensity:F

    .line 669
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f090066

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .line 670
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    if-eqz v4, :cond_4

    .line 671
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->init()V

    .line 672
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->updateScreenSize()V

    .line 673
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    .line 676
    :cond_4
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f09006e

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    .line 678
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f09006a

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 680
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f090069

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    .line 683
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f09006f

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSelectDescriptionTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 685
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f090072

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsButton:Landroid/widget/ImageView;

    .line 687
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f090071

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    .line 690
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsLayout:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mArrowClickLis:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 692
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    invoke-direct {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;-><init>()V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    .line 693
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    invoke-direct {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;-><init>()V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    .line 694
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->setZoomState(Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;)V

    .line 696
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setShowBounceViewListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;)V

    .line 697
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setRotateDegress(I)V

    .line 698
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomState:Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setImageZoomState(Lcom/arcsoft/magicshotstudio/ui/picmotion/ImageZoomState;)V

    .line 699
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 700
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->setmIsMunualMode(Z)V

    .line 701
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceViewListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->setShowBounceViewListener(Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;)V

    .line 703
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initSound()V

    .line 704
    iput-object p6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    .line 705
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    const v6, 0x7f090067

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    .line 707
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initDefaultActionbarUI()V

    .line 708
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initDrawMaskActionbarUI()V

    .line 710
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 711
    .local v0, "currConfig":Landroid/content/res/Configuration;
    iget v4, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_5

    .line 712
    .local v2, "landScape":Z
    :goto_0
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDensity:F

    invoke-direct {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateGapValue(ZF)V

    .line 713
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V

    .line 714
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    .line 715
    .local v3, "observer":Landroid/view/ViewTreeObserver;
    new-instance v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$3;

    invoke-direct {v4, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 721
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->inflateRollWaitingView()V

    .line 722
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getExitDialog()Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 723
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-direct {v1, v4}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V

    .line 724
    .local v1, "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v4, v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setBtnOnClick(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 726
    new-instance v4, Landroid/os/HandlerThread;

    const-string v5, "SWITCH_CHECK_TREAD"

    invoke-direct {v4, v5}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckThread:Landroid/os/HandlerThread;

    .line 727
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->start()V

    .line 728
    new-instance v4, Landroid/os/Handler;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mWitchCheckThreadHandler:Landroid/os/Handler;

    .line 729
    new-instance v4, Landroid/os/Handler;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/PicMotion;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainTHCallback:Landroid/os/Handler$Callback;

    invoke-direct {v4, v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mMainThreadHandler:Landroid/os/Handler;

    .line 731
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDensity:F

    invoke-direct {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateBottomLayout(ZF)V

    .line 732
    return-void

    .end local v1    # "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    .end local v2    # "landScape":Z
    .end local v3    # "observer":Landroid/view/ViewTreeObserver;
    :cond_5
    move v2, v5

    .line 711
    goto :goto_0
.end method

.method public isActionBarShown()Z
    .locals 1

    .prologue
    .line 2585
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    return v0
.end method

.method public isBtnClickEnable()Z
    .locals 1

    .prologue
    .line 1104
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBtnClickEnable:Z

    return v0
.end method

.method public isHelpViewShown()Z
    .locals 1

    .prologue
    .line 2804
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v0, :cond_0

    .line 2805
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v0

    .line 2807
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSaveState()Z
    .locals 1

    .prologue
    .line 1381
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsSaveBtnOn:Z

    .line 1382
    .local v0, "ret":Z
    return v0
.end method

.method public isSeekBarManagerWorking()Z
    .locals 1

    .prologue
    .line 2568
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->isWorking()Z

    move-result v0

    return v0
.end method

.method public isSelectFrameView()Z
    .locals 3

    .prologue
    .line 1386
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->getWorkStatus()Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    move-result-object v1

    sget-object v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->SELETEFRAME:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1387
    .local v0, "ret":Z
    :goto_0
    return v0

    .line 1386
    .end local v0    # "ret":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2267
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDrawMaskButtonsEnable:Z

    .line 2269
    sget-object v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION_OBJECT:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    if-ne v2, v3, :cond_2

    .line 2271
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2272
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameView:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v2, v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->setVisibility(I)V

    .line 2282
    :cond_0
    :goto_0
    sget-object v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    .line 2283
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2284
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2285
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2287
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->stopWork()V

    .line 2289
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    sget-object v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    if-ne v2, v3, :cond_3

    .line 2290
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->cancelMask()V

    .line 2291
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 2292
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsFrameListShow:Z

    if-nez v1, :cond_1

    .line 2293
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ShowFrameListAnim(Z)V

    .line 2313
    :cond_1
    :goto_1
    return v0

    .line 2274
    :cond_2
    sget-object v2, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION_BLUR:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    if-ne v2, v3, :cond_0

    .line 2276
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2277
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2278
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 2296
    :cond_3
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    sget-object v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    if-ne v2, v3, :cond_4

    .line 2297
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->backToSelectFrame()V

    .line 2298
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setOperateEnable(Z)V

    .line 2299
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsFrameListShow:Z

    if-nez v1, :cond_1

    .line 2300
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ShowFrameListAnim(Z)V

    goto :goto_1

    .line 2305
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    move v0, v1

    .line 2313
    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2360
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 2361
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    if-eqz v2, :cond_0

    .line 2362
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->updateScreenSize()V

    .line 2363
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    .line 2366
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2367
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->getHelpType()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->updateHelpConfig(ILandroid/content/res/Configuration;)V

    .line 2370
    :cond_1
    const/4 v2, 0x2

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v3, :cond_3

    const/4 v1, 0x1

    .line 2371
    .local v1, "landscape":Z
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/PicMotion;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 2372
    .local v0, "density":F
    invoke-direct {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateGapValue(ZF)V

    .line 2373
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->switchTalkBackMode(Z)V

    .line 2375
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 2377
    invoke-direct {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateBottomLayout(ZF)V

    .line 2379
    :cond_2
    return-void

    .line 2370
    .end local v0    # "density":F
    .end local v1    # "landscape":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public processManualMask([BIIZ)Z
    .locals 16
    .param p1, "grey8"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isBrush"    # Z

    .prologue
    .line 1482
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 1483
    .local v12, "start":J
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v2, v0

    if-lez v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mIsChangeFrame:Z

    if-eqz v2, :cond_1

    .line 1484
    :cond_0
    const/4 v11, 0x0

    .line 1525
    :goto_0
    return v11

    .line 1487
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1488
    .local v14, "time":J
    const/4 v11, 0x0

    .line 1489
    .local v11, "success":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMode: grey8 length:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " width:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isBrush :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    new-instance v8, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    invoke-direct {v8}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;-><init>()V

    .line 1494
    .local v8, "returnBitmap":Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameMaskIndex:I

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->applyMask(I[BIIZLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v10

    .line 1497
    .local v10, "result":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResultImageByMask result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMask() cost time = GetResultImageByMask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1500
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "processManualMode: A "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    if-nez v10, :cond_2

    iget-object v2, v8, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget-object v2, v8, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1512
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFrameMaskIndex:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->getShotMaskByIndex(I)[B

    move-result-object v9

    .line 1513
    .local v9, "mask":[B
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mShotMask:[B

    .line 1514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "zdd processManualMask() cost time = getDefaultMaskByIndex "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1518
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v2, v9}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setMask([B)V

    .line 1519
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v3, "processManualMode: B "

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "zdd processManualMask() cost time = setMask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMode out cost time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v12

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1505
    .end local v9    # "mask":[B
    :cond_3
    const/4 v11, 0x1

    .line 1506
    iget-object v2, v8, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 1507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processManualMask() cost time = updateImage "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v14

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1509
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    goto/16 :goto_1
.end method

.method public processSeekBarTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2564
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->processSeekBarTouchEvent(Landroid/view/MotionEvent;)V

    .line 2565
    return-void
.end method

.method public processWithSeekBarValue(I)V
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 2696
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processWithSeekBarValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2697
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    .line 2698
    .local v0, "task":Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$ProcessSeekBarTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2699
    return-void
.end method

.method public redo()V
    .locals 4

    .prologue
    .line 451
    const-string v1, "xsj"

    const-string v2, "redo start "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->redo()Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    move-result-object v0

    .line 453
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    if-eqz v0, :cond_0

    .line 455
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->mask:[B

    iget-boolean v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->redo_isBrush:Z

    invoke-virtual {v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->doMask([BZ)V

    .line 457
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkUndoRedoState()V

    .line 458
    const-string v1, "xsj"

    const-string v2, "redo end "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    return-void
.end method

.method public rememberVisibility()V
    .locals 1

    .prologue
    .line 2498
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Actionbar:I

    .line 2499
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_FramView:I

    .line 2500
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_mDownArrows:I

    .line 2501
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_mUpArrows:I

    .line 2502
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Description:I

    .line 2505
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Obiect_Layout:I

    .line 2506
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Blur_Layout:I

    .line 2507
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_ANGLE_IMG_Layout:I

    .line 2508
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Motion_Blur_List_Layout:I

    .line 2509
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_EditMaskBar:I

    .line 2510
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_MotionBlurArea:I

    .line 2511
    return-void
.end method

.method public restoreViewsVisibility()V
    .locals 2

    .prologue
    .line 2541
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Actionbar:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2542
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFramViewLinearLayout:Landroid/view/View;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_FramView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2543
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_mDownArrows:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2544
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mManualDescriptionLayout:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_mUpArrows:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2546
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Obiect_Layout:Landroid/view/View;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Obiect_Layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2547
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_Layout:Landroid/view/View;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Blur_Layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2548
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_ANGLE_IMG_Layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2549
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_List_Layout:Landroid/view/View;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Motion_Blur_List_Layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2550
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_EditMaskBar:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2552
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurArea:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_MotionBlurArea:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2554
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;->DRAWMASK:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkStatus;

    if-ne v0, v1, :cond_0

    .line 2555
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    .line 2560
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbShowEraserState:Z

    .line 2561
    return-void

    .line 2557
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDescriptionView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mVisibility_Description:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public saveOperate([BZ)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "isBrush"    # Z

    .prologue
    .line 481
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->saveOperate([BZ)V

    .line 483
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkUndoRedoState()V

    .line 485
    :cond_0
    return-void
.end method

.method public saveResult()V
    .locals 0

    .prologue
    .line 1859
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResultFile()V

    .line 1860
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1861
    return-void
.end method

.method public saveResultFile()V
    .locals 2

    .prologue
    .line 1871
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFileSaving:Z

    if-eqz v1, :cond_0

    .line 1908
    :goto_0
    return-void

    .line 1874
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFileSaving:Z

    .line 1876
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    .line 1878
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1882
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveThread:Ljava/lang/Thread;

    .line 1885
    :cond_1
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveThread:Ljava/lang/Thread;

    .line 1906
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1907
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1879
    :catch_0
    move-exception v0

    .line 1880
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public setBtnClickEnable(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 1100
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBtnClickEnable:Z

    .line 1101
    return-void
.end method

.method public setEditDisable()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 742
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 746
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 747
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 749
    :cond_1
    return-void
.end method

.method public setEditEnable()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 752
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 755
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 756
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 757
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEditLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 759
    :cond_1
    return-void
.end method

.method public setIsMoveActionWorkForZoomListener()V
    .locals 4

    .prologue
    .line 82
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->isMoveActionWork(Z)Z

    move-result v0

    .line 83
    .local v0, "isHorizontalMoveWork":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->isMoveActionWork(Z)Z

    move-result v1

    .line 84
    .local v1, "isVerticalMoveWork":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->setMoveActionWork(ZZ)V

    .line 86
    return-void
.end method

.method public setOperateEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 764
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOperateEnable:Z

    .line 765
    return-void
.end method

.method public showBottomLayout()V
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotionBottom_Layout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 592
    :cond_0
    return-void
.end method

.method public showProgressDialog()V
    .locals 1

    .prologue
    .line 2047
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 2048
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2050
    :cond_0
    return-void
.end method

.method public showTopActionBar()V
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 586
    :cond_0
    return-void
.end method

.method public startAdjust()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2786
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    if-eqz v0, :cond_0

    .line 2788
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mPicMotion_Motion_Blur_ANGLE_IMG_Layout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2789
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->setVisibility(I)V

    .line 2791
    :cond_0
    return-void
.end method

.method public switchAllViews()V
    .locals 2

    .prologue
    .line 2589
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ViewsShow:Z

    if-eqz v0, :cond_1

    .line 2590
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->rememberVisibility()V

    .line 2591
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hideAllViews()V

    .line 2592
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ViewsShow:Z

    .line 2603
    :cond_0
    :goto_0
    return-void

    .line 2594
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->restoreViewsVisibility()V

    .line 2595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->ViewsShow:Z

    .line 2596
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurMode:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    sget-object v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;->PICMOTION_BLUR:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$WorkMode;

    if-ne v0, v1, :cond_0

    .line 2597
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->endAdjust()V

    .line 2598
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    if-eqz v0, :cond_0

    .line 2599
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->hideSeekBar()V

    goto :goto_0
.end method

.method public switchDefaultActionBar()V
    .locals 1

    .prologue
    .line 1753
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1755
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mbIsAnimDefaultBarIn:Z

    if-eqz v0, :cond_1

    .line 1761
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->animDefaultBarOut()V

    .line 1771
    :cond_0
    :goto_0
    return-void

    .line 1768
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->animDefaultBarIn()V

    goto :goto_0
.end method

.method public unInit()V
    .locals 3

    .prologue
    .line 1774
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1775
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    if-eqz v1, :cond_0

    .line 1776
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1777
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->uninit()I

    .line 1778
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1781
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v2, "################### mSwithCheckThread.join may be ANR occured... ################### "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1782
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 1783
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSwithCheckThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1787
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;

    const-string v2, "################### mSwithCheckThread.join finished... #######"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1788
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mProcessLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1789
    return-void

    .line 1784
    :catch_0
    move-exception v0

    .line 1785
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public unInitAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1608
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1609
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtShowFrameList:Landroid/animation/AnimatorSet;

    .line 1610
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1611
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEidtUnShowFrameList:Landroid/animation/AnimatorSet;

    .line 1612
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 1613
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimEraserState:Landroid/animation/Animator;

    .line 1615
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 1616
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 1617
    return-void
.end method

.method public unInitUI()V
    .locals 1

    .prologue
    .line 1792
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 1793
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    if-eqz v0, :cond_0

    .line 1794
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->unInit()V

    .line 1796
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInitFrameList()V

    .line 1797
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInitAnimation()V

    .line 1798
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInitSound()V

    .line 1799
    return-void
.end method

.method public undo()V
    .locals 4

    .prologue
    .line 462
    const-string v1, "xsj"

    const-string v2, "undo start "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mUndoRedoManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/UndoRedoManager;->undo()Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;

    move-result-object v0

    .line 465
    .local v0, "data":Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;
    if-eqz v0, :cond_0

    .line 466
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->mask:[B

    iget-boolean v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/OperateData;->undo_isBrush:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->doMask([BZ)V

    .line 468
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->checkUndoRedoState()V

    .line 469
    const-string v1, "xsj"

    const-string v2, "undo end "

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    return-void

    .line 466
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateCurrentBlurInfo()V
    .locals 3

    .prologue
    .line 2738
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->getCurrentBlurInfo()V

    .line 2739
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    if-eqz v1, :cond_0

    .line 2741
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurDegree:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->convertDegreeBetweenAlgAndUI(II)I

    move-result v0

    .line 2742
    .local v0, "uiDegree":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->m_MotionBlurWidget:Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/MotionBlurWidget;->setCursorPosition(I)V

    .line 2745
    .end local v0    # "uiDegree":I
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    if-eqz v1, :cond_1

    .line 2747
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSeekBarManager:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mCurrentBlurSize:I

    add-int/lit8 v2, v2, -0x1e

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->setSeekBarValue(I)V

    .line 2749
    :cond_1
    return-void
.end method

.method public updateImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    .line 735
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->updateResultImage(Landroid/graphics/Bitmap;)V

    .line 736
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->setImageIndex(I)V

    .line 737
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->initMask()V

    .line 738
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->invalidate()V

    .line 739
    return-void
.end method
