.class public interface abstract Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;
.super Ljava/lang/Object;
.source "ArcZoomControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ZoomStateListener"
.end annotation


# virtual methods
.method public abstract getDestRect()Landroid/graphics/Rect;
.end method

.method public abstract getMaxSize()Lcom/arcsoft/magicshotstudio/utils/MSize;
.end method

.method public abstract getZoomOffsetPoint()Landroid/graphics/Point;
.end method

.method public abstract getZoomOutPoint()Landroid/graphics/Point;
.end method

.method public abstract startZoom()V
.end method

.method public abstract stopZoom()V
.end method

.method public abstract updateZoomState()V
.end method
