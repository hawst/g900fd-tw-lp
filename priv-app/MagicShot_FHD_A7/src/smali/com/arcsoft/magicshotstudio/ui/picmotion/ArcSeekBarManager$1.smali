.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;
.super Ljava/lang/Object;
.source "ArcSeekBarManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 35
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 46
    :cond_0
    :goto_0
    return v2

    .line 38
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->showSeekBar()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)V

    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mIsLongPressed:Z
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->access$102(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;Z)Z

    .line 40
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mCurProgress:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->access$202(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;I)I

    .line 41
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->mSeekBarListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;->access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ArcSeekBarManager$SeekBarListener;->startAdjust()V

    goto :goto_0

    .line 35
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
