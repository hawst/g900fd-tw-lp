.class Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V
    .locals 0

    .prologue
    .line 939
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v8, 0x190

    const/16 v7, 0x5000

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 942
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isBtnClickEnable()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1088
    :goto_0
    return-void

    .line 945
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 1087
    :cond_1
    :goto_1
    :sswitch_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "onClick out "

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 947
    :sswitch_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 948
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditImageView:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 949
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    sget-object v7, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;->PENDINGMASK:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    iput-object v7, v6, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mCurrWork:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$WorkStatus;

    .line 950
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showMaskActionbar()V
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 951
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 952
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDownArrowsView:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/widget/RelativeLayout;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 955
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBRestore:Landroid/widget/ImageView;

    const v7, 0x7f020051

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 956
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v6, v6, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIBBrushEraser:Landroid/widget/ImageView;

    const v7, 0x7f020067

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 959
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v6, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 960
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    move v4, v5

    :cond_3
    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchTalkBackMode(Z)V
    invoke-static {v6, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$900(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 962
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 963
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEditMaskBar:Landroid/widget/RelativeLayout;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 964
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showActionBar()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    goto :goto_1

    .line 968
    :sswitch_2
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 969
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "press save button"

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    .line 971
    .local v2, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 972
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v5, v5, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v5, :cond_5

    .line 973
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResultFile()Z

    move-result v3

    .line 974
    .local v3, "success":Z
    if-eqz v3, :cond_4

    .line 975
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v5

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/Drama;->getHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    .line 977
    :cond_4
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v5, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->changeSaveBtnState(Z)V

    goto/16 :goto_1

    .line 979
    .end local v3    # "success":Z
    :cond_5
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    if-eqz v4, :cond_6

    .line 980
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 982
    :cond_6
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    new-instance v5, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v5, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 983
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 987
    :cond_7
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v5, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setBtnClickEnable(Z)V

    .line 988
    invoke-virtual {v2, v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 989
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResult()Z

    move-result v3

    .line 990
    .restart local v3    # "success":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->exitActivity()V

    .line 991
    if-eqz v3, :cond_1

    .line 992
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 997
    .end local v2    # "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .end local v3    # "success":Z
    :sswitch_3
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 998
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v5, v5, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v5

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/Drama;->isUserEdit()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 999
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_1

    .line 1002
    :cond_8
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v5

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/Drama;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1003
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->exitWidthoutSave()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    goto/16 :goto_1

    .line 1018
    :sswitch_4
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$202(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z

    .line 1019
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->cancelMask()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 1020
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setOperateEnable(Z)V

    .line 1021
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1022
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->resetEditView()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    goto/16 :goto_1

    .line 1025
    :sswitch_5
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1026
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V
    invoke-static {v6, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1027
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iput-boolean v5, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 1028
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-nez v4, :cond_1

    .line 1029
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    const v5, 0x7f060048

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V

    goto/16 :goto_1

    .line 1036
    :sswitch_6
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1037
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->switchRestoreAndEraserButtonFocus(Z)V
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1038
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iput-boolean v5, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbShowEraserState:Z

    .line 1039
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mIsFrameListShow:Z

    if-nez v4, :cond_1

    .line 1040
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    const v5, 0x7f060049

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showDescription(I)V
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;I)V

    goto/16 :goto_1

    .line 1047
    :sswitch_7
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1048
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mDrawMaskButtonsEnable:Z
    invoke-static {v6, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$202(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)Z

    .line 1049
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;-><init>()V

    .line 1050
    .local v1, "retBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1600(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->confirmEdit(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 1051
    .local v0, "result":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EditDone result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ProcessCheckedFrame()Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v6

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1702(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1054
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mLargeBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1700(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImage(Landroid/graphics/Bitmap;)V

    .line 1055
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->backToSelectFrame()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1900(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 1056
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->ShowFrameListAnim(Z)V
    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$100(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;Z)V

    .line 1057
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->resetEditView()V
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1500(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)V

    .line 1058
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->changeSaveBtnState(Z)V

    .line 1059
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->setUserEditFlag()V

    goto/16 :goto_1

    .line 1063
    .end local v0    # "result":I
    .end local v1    # "retBitmap":Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;
    :sswitch_8
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 1064
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1065
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mbIsSaveBtnOn:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->isUserEdit()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1066
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$1200(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_1

    .line 1068
    :cond_9
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/Drama;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$800(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)Lcom/arcsoft/magicshotstudio/Drama;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    goto/16 :goto_1

    .line 1072
    :sswitch_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v4, v4, v8

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUndoButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1073
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v4, v6, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2002(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;J)J

    .line 1074
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->undo()V

    goto/16 :goto_1

    .line 1078
    :sswitch_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2000(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v4, v4, v8

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mRedoButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1079
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->mUnRedoLockedTime:J
    invoke-static {v4, v6, v7}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->access$2002(Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;J)J

    .line 1080
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->redo()V

    goto/16 :goto_1

    .line 945
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_8
        0x7f09004c -> :sswitch_3
        0x7f090053 -> :sswitch_2
        0x7f090059 -> :sswitch_0
        0x7f090075 -> :sswitch_1
        0x7f09007d -> :sswitch_4
        0x7f09007f -> :sswitch_7
        0x7f090081 -> :sswitch_5
        0x7f090085 -> :sswitch_6
        0x7f090089 -> :sswitch_9
        0x7f09008c -> :sswitch_a
    .end sparse-switch
.end method
