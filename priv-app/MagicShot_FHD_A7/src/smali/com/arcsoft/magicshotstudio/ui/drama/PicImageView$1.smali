.class Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;
.super Ljava/lang/Object;
.source "PicImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)V
    .locals 0

    .prologue
    .line 795
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 798
    const-string v4, "ArcSoft_PicImageView"

    const-string v5, "mProcessManualMask run "

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->getMaskAreas()[B

    move-result-object v0

    .line 800
    .local v0, "grey8":[B
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    const v5, -0x44eeeeef

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 801
    .local v1, "isBrush":Z
    :goto_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgwidth:I
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->access$600(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)I

    move-result v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mImgheight:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->access$700(Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;)I

    move-result v6

    invoke-virtual {v4, v0, v5, v6, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->processManualMask([BIIZ)Z

    move-result v2

    .line 802
    .local v2, "success":Z
    if-eqz v2, :cond_0

    .line 803
    const-string v4, "ArcSoft_PicImageView"

    const-string v5, "mAction.saveOperate in"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v4, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveOperate([BZ)V

    .line 806
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;

    iput-boolean v3, v4, Lcom/arcsoft/magicshotstudio/ui/drama/PicImageView;->mTouchLock:Z

    .line 807
    const-string v3, "ArcSoft_PicImageView"

    const-string v4, "mProcessManualMask run out"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    return-void

    .end local v1    # "isBrush":Z
    .end local v2    # "success":Z
    :cond_1
    move v1, v3

    .line 800
    goto :goto_0
.end method
