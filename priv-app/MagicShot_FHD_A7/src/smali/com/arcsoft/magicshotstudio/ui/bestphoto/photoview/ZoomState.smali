.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
.super Ljava/util/Observable;
.source "ZoomState.java"


# static fields
.field public static final ZOOM_EDGE_EFFECT_DIRECTION:I = 0xf


# instance fields
.field private mEdgeEffectEnabled:Z

.field private mEffectDirection:I

.field private mEffectOffset_X:F

.field private mEffectOffset_Y:F

.field private mIsZooming:Z

.field private mPanX:F

.field private mPanY:F

.field private mZoom:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 6
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    .line 8
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanX:F

    .line 10
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanY:F

    .line 12
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mIsZooming:Z

    .line 67
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectOffset_X:F

    .line 68
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectOffset_Y:F

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectDirection:I

    .line 70
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEdgeEffectEnabled:Z

    return-void
.end method


# virtual methods
.method public getEdgeEffectEnabled()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEdgeEffectEnabled:Z

    return v0
.end method

.method public getEffectDirection()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectDirection:I

    return v0
.end method

.method public getEffectOffset_X()F
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectOffset_X:F

    return v0
.end method

.method public getEffectOffset_Y()F
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectOffset_Y:F

    return v0
.end method

.method public getPanX()F
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanX:F

    return v0
.end method

.method public getPanY()F
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanY:F

    return v0
.end method

.method public getZoom()F
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    return v0
.end method

.method public getZoomX(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 39
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    mul-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getZoomY(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 43
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    div-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public isZooming()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mIsZooming:Z

    return v0
.end method

.method public setEdgeEffectEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEdgeEffectEnabled:Z

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectDirection:I

    .line 79
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setChanged()V

    .line 80
    return-void
.end method

.method public setEffectDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectDirection:I

    .line 89
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setChanged()V

    .line 90
    return-void
.end method

.method public setEffectOffset_X(F)V
    .locals 0
    .param p1, "dx"    # F

    .prologue
    .line 108
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectOffset_X:F

    .line 109
    return-void
.end method

.method public setEffectOffset_Y(F)V
    .locals 0
    .param p1, "dy"    # F

    .prologue
    .line 116
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectOffset_Y:F

    .line 117
    return-void
.end method

.method public setPanX(F)V
    .locals 1
    .param p1, "panX"    # F

    .prologue
    .line 47
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanX:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 48
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanX:F

    .line 49
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setChanged()V

    .line 51
    :cond_0
    return-void
.end method

.method public setPanY(F)V
    .locals 1
    .param p1, "panY"    # F

    .prologue
    .line 54
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanY:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 55
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mPanY:F

    .line 56
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setChanged()V

    .line 58
    :cond_0
    return-void
.end method

.method public setZoom(F)V
    .locals 1
    .param p1, "zoom"    # F

    .prologue
    .line 61
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 62
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mZoom:F

    .line 63
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setChanged()V

    .line 65
    :cond_0
    return-void
.end method

.method public startZooming()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mIsZooming:Z

    .line 28
    return-void
.end method

.method public stopPanEdgeEffect(I)V
    .locals 0
    .param p1, "side"    # I

    .prologue
    .line 101
    return-void
.end method

.method public stopZoomEdgeEffect()V
    .locals 2

    .prologue
    .line 93
    const/16 v0, 0xf

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectDirection:I

    if-ne v0, v1, :cond_0

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mEffectDirection:I

    .line 96
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setChanged()V

    .line 98
    :cond_0
    return-void
.end method

.method public stopZooming()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->mIsZooming:Z

    .line 32
    return-void
.end method
