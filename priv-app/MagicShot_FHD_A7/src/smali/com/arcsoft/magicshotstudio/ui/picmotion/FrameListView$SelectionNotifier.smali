.class Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;
.super Ljava/lang/Object;
.source "FrameListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionNotifier"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V
    .locals 0

    .prologue
    .line 6415
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$1;

    .prologue
    .line 6415
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 6418
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mDataChanged:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6422
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$1400(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6423
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->post(Ljava/lang/Runnable;)Z

    .line 6429
    :cond_0
    :goto_0
    return-void

    .line 6426
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->fireOnSelected()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2800(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    .line 6427
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView$SelectionNotifier;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->performAccessibilityActionsOnSelected()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;->access$2900(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameListView;)V

    goto :goto_0
.end method
