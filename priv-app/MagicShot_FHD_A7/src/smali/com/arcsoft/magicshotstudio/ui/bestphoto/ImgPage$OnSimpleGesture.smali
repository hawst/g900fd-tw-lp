.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ImgPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnSimpleGesture"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v8, 0x42480000    # 50.0f

    const/4 v3, 0x0

    .line 125
    const-string v4, "dds225"

    const-string v5, "PhotoViewLayout onFling..."

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v3

    .line 131
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItemView()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    move-result-object v2

    .line 132
    .local v2, "photoView":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    if-eqz v2, :cond_0

    .line 136
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->isZoomed()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/16 v6, 0x7d

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 141
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    const/4 v1, 0x1

    .line 142
    .local v1, "directionX":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->isImageDisplayCompletely()Z

    move-result v0

    .line 147
    .local v0, "completeDisplay":Z
    const-string v4, "dds225"

    const-string v5, "PhotoViewLayout completeDisplay..."

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x43480000    # 200.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 149
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float/2addr v4, v5

    cmpl-float v4, v4, v8

    if-lez v4, :cond_4

    .line 150
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->isArriveRightEdge()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 151
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->nextPhoto()V

    .line 154
    :cond_2
    if-eqz v0, :cond_0

    .line 155
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->nextPhoto()V

    goto :goto_0

    .end local v0    # "completeDisplay":Z
    .end local v1    # "directionX":Z
    :cond_3
    move v1, v3

    .line 141
    goto :goto_1

    .line 157
    .restart local v0    # "completeDisplay":Z
    .restart local v1    # "directionX":Z
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float/2addr v4, v5

    cmpl-float v4, v4, v8

    if-lez v4, :cond_0

    .line 158
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->isArriveLeftEdge()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 159
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->prevPhoto()V

    .line 162
    :cond_5
    if-eqz v0, :cond_0

    .line 163
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->prevPhoto()V

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 119
    const-string v0, "dds225"

    const-string v1, "PhotoViewLayout onScroll..."

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const/4 v0, 0x0

    return v0
.end method
