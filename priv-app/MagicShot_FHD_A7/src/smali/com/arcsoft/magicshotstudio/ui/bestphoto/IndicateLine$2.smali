.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;
.super Ljava/lang/Object;
.source "IndicateLine.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 184
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getClickIndex(Landroid/view/View;)I
    invoke-static {v4, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;Landroid/view/View;)I

    move-result v1

    .line 185
    .local v1, "index":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mBestIndexArray:[Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)[Z

    move-result-object v4

    aget-boolean v4, v4, v1

    if-nez v4, :cond_0

    move v0, v3

    .line 186
    .local v0, "bBest":Z
    :goto_0
    if-nez v0, :cond_1

    .line 197
    :goto_1
    return v2

    .end local v0    # "bBest":Z
    :cond_0
    move v0, v2

    .line 185
    goto :goto_0

    .line 188
    .restart local v0    # "bBest":Z
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setBestIndex(IZ)V
    invoke-static {v4, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;IZ)V

    .line 189
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;->onCursorClicked(I)V

    .line 191
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 192
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBestCount()I

    move-result v4

    if-gtz v4, :cond_3

    .line 193
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;->onNoItemSelected(Z)V

    :cond_2
    :goto_2
    move v2, v3

    .line 197
    goto :goto_1

    .line 195
    :cond_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->mCursorClickListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;->onNoItemSelected(Z)V

    goto :goto_2
.end method
