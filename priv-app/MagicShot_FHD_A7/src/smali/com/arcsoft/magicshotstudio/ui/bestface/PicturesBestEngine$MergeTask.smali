.class Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;
.super Ljava/lang/Thread;
.source "PicturesBestEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MergeTask"
.end annotation


# instance fields
.field private volatile mActive:Z

.field private volatile mDirty:Z

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V
    .locals 1

    .prologue
    .line 424
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 425
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mActive:Z

    .line 426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mDirty:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$1;

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized notifyDirty()V
    .locals 1

    .prologue
    .line 455
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mDirty:Z

    .line 456
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    monitor-exit p0

    return-void

    .line 455
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 430
    :goto_0
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mActive:Z

    if-eqz v2, :cond_3

    .line 431
    monitor-enter p0

    .line 432
    :try_start_0
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mActive:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mDirty:Z

    if-nez v2, :cond_0

    .line 433
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 434
    monitor-exit p0

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 438
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonId:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2200(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceId:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2300(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getMergeResult(IILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I

    .line 439
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->getResultFaceInfo()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v0

    .line 440
    .local v0, "faceInfo":Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v2

    if-nez v2, :cond_1

    .line 441
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2102(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Ljava/util/Vector;)Ljava/util/Vector;

    .line 444
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 445
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1800(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 446
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->getResFaceRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 445
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 449
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 450
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mDirty:Z

    goto :goto_0

    .line 452
    .end local v0    # "faceInfo":Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .end local v1    # "i":I
    :cond_3
    return-void
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 460
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$MergeTask;->mActive:Z

    .line 461
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    monitor-exit p0

    return-void

    .line 460
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
