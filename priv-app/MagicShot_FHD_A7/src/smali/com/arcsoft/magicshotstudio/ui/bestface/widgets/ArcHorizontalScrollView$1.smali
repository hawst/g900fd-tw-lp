.class Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;
.super Ljava/lang/Object;
.source "ArcHorizontalScrollView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 173
    if-eqz p2, :cond_1

    .line 174
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 175
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 176
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    move-result-object v1

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    move-result-object v1

    aget-object v1, v1, v0

    if-eq v1, p1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->mCandidateFaceArray:[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;)[Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;

    move-result-object v1

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/FaceItemView;->setSelected(Z)V

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "i":I
    :cond_1
    return-void
.end method
