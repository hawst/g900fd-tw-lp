.class public Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
.super Ljava/util/Observable;
.source "ArcZoomState.java"


# static fields
.field public static final ZOOM_EDGE_EFFECT_DIRECTION:I = 0xf


# instance fields
.field private mEdgeEffectEnabled:Z

.field private mEffectDirection:I

.field private mEffectOffset_X:F

.field private mEffectOffset_Y:F

.field private mPanX:F

.field private mPanY:F

.field private mZoom:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 6
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    .line 7
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanX:F

    .line 8
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanY:F

    .line 51
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectOffset_X:F

    .line 52
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectOffset_Y:F

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectDirection:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEdgeEffectEnabled:Z

    return-void
.end method


# virtual methods
.method public getEdgeEffectEnabled()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEdgeEffectEnabled:Z

    return v0
.end method

.method public getEffectDirection()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectDirection:I

    return v0
.end method

.method public getEffectOffset_X()F
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectOffset_X:F

    return v0
.end method

.method public getEffectOffset_Y()F
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectOffset_Y:F

    return v0
.end method

.method public getPanX()F
    .locals 1

    .prologue
    .line 11
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanX:F

    return v0
.end method

.method public getPanY()F
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanY:F

    return v0
.end method

.method public getZoom()F
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    return v0
.end method

.method public getZoomX(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 23
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    mul-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getZoomY(F)F
    .locals 2
    .param p1, "aspectQuotient"    # F

    .prologue
    .line 27
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    div-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public setEdgeEffectEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEdgeEffectEnabled:Z

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectDirection:I

    .line 63
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setChanged()V

    .line 64
    return-void
.end method

.method public setEffectDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectDirection:I

    .line 72
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setChanged()V

    .line 73
    return-void
.end method

.method public setEffectOffset_X(F)V
    .locals 0
    .param p1, "dx"    # F

    .prologue
    .line 90
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectOffset_X:F

    .line 91
    return-void
.end method

.method public setEffectOffset_Y(F)V
    .locals 0
    .param p1, "dy"    # F

    .prologue
    .line 98
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectOffset_Y:F

    .line 99
    return-void
.end method

.method public setPanX(F)V
    .locals 1
    .param p1, "panX"    # F

    .prologue
    .line 31
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanX:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 32
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanX:F

    .line 33
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setChanged()V

    .line 35
    :cond_0
    return-void
.end method

.method public setPanY(F)V
    .locals 1
    .param p1, "panY"    # F

    .prologue
    .line 38
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanY:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 39
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mPanY:F

    .line 40
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setChanged()V

    .line 42
    :cond_0
    return-void
.end method

.method public setZoom(F)V
    .locals 1
    .param p1, "zoom"    # F

    .prologue
    .line 45
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 46
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mZoom:F

    .line 47
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setChanged()V

    .line 49
    :cond_0
    return-void
.end method

.method public stopPanEdgeEffect(I)V
    .locals 0
    .param p1, "side"    # I

    .prologue
    .line 83
    return-void
.end method

.method public stopZoomEdgeEffect()V
    .locals 2

    .prologue
    .line 76
    const/16 v0, 0xf

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectDirection:I

    if-ne v0, v1, :cond_0

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->mEffectDirection:I

    .line 78
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setChanged()V

    .line 80
    :cond_0
    return-void
.end method
