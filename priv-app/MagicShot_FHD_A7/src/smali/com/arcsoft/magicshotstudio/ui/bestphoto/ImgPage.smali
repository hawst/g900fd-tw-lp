.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;
.super Landroid/support/v4/view/MyViewPager;
.source "ImgPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;
    }
.end annotation


# instance fields
.field private mDetector:Landroid/view/GestureDetector;

.field private mIsIntercept:Z

.field private mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/support/v4/view/MyViewPager;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mIsIntercept:Z

    .line 57
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 23
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->init(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/MyViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mIsIntercept:Z

    .line 57
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 28
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->init(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method static synthetic access$002(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mIsIntercept:Z

    return p1
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItemView()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentItemView()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItem()I

    move-result v0

    .line 86
    .local v0, "currentItem":I
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .line 88
    .local v1, "photoView":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    return-object v1
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage$OnSimpleGesture;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mDetector:Landroid/view/GestureDetector;

    .line 33
    return-void
.end method

.method private isIntercept()Z
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 75
    .local v0, "bIsIntercept":Z
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItem()I

    move-result v1

    .line 76
    .local v1, "currentItem":I
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .line 77
    .local v2, "photoView":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    if-eqz v2, :cond_0

    .line 78
    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->isZoomed()Z

    move-result v0

    .line 81
    :cond_0
    return v0
.end method


# virtual methods
.method public nextPhoto()V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItem()I

    move-result v0

    .line 100
    .local v0, "currentItem":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 101
    add-int/lit8 v1, v0, 0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setCurrentItem(IZ)V

    .line 103
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 38
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mIsIntercept:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->isIntercept()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/view/MyViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public prevPhoto()V
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItem()I

    move-result v0

    .line 93
    .local v0, "currentItem":I
    if-lez v0, :cond_0

    .line 94
    add-int/lit8 v1, v0, -0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setCurrentItem(IZ)V

    .line 96
    :cond_0
    return-void
.end method

.method public resetOtherPageZoom()V
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 107
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .line 108
    .local v1, "photoView":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->isZoomed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItem()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 110
    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->resetZoomState()V

    .line 106
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    .end local v1    # "photoView":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    :cond_1
    return-void
.end method

.method public setAdapterRes(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "res":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    invoke-direct {v0, p1, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;-><init>(Ljava/util/List;Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;)V

    .line 54
    .local v0, "adapter":Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPageAdapter;
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    goto :goto_0
.end method
