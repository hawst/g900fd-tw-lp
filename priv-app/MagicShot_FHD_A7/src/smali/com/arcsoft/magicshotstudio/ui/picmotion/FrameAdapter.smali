.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;
.super Landroid/widget/BaseAdapter;
.source "FrameAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mFrameArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field mInflater:Landroid/view/LayoutInflater;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 22
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 23
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mContext:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 35
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 49
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    .line 55
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->getCount()I

    move-result v7

    if-gtz v7, :cond_0

    move-object v7, v8

    .line 97
    :goto_0
    return-object v7

    .line 60
    :cond_0
    if-nez p2, :cond_2

    .line 61
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 62
    .local v6, "width":I
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 64
    .local v2, "height":I
    if-le v6, v2, :cond_1

    .line 65
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030026

    invoke-virtual {v7, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 70
    :goto_1
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;)V

    .line 71
    .local v3, "holder":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;
    const v7, 0x7f090063

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;->check:Landroid/widget/CheckBox;

    .line 72
    const v7, 0x7f09008f

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;->frame:Landroid/widget/ImageView;

    .line 75
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 80
    .end local v2    # "height":I
    .end local v6    # "width":I
    :goto_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-boolean v0, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    .line 81
    .local v0, "bCheck":Z
    iget-object v7, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;->check:Landroid/widget/CheckBox;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 82
    iget-object v7, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;->check:Landroid/widget/CheckBox;

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 84
    iget-object v7, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;->frame:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 85
    iget-object v8, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;->frame:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v7, v7, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 87
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 89
    .local v4, "res":Landroid/content/res/Resources;
    if-eqz v0, :cond_3

    .line 90
    const v7, 0x7f060054

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "ck":Ljava/lang/String;
    invoke-virtual {p2, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .end local v1    # "ck":Ljava/lang/String;
    :goto_3
    move-object v7, p2

    .line 97
    goto/16 :goto_0

    .line 67
    .end local v0    # "bCheck":Z
    .end local v3    # "holder":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;
    .end local v4    # "res":Landroid/content/res/Resources;
    .restart local v2    # "height":I
    .restart local v6    # "width":I
    :cond_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030027

    invoke-virtual {v7, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    .line 77
    .end local v2    # "height":I
    .end local v6    # "width":I
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;

    .restart local v3    # "holder":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter$ViewHolder;
    goto :goto_2

    .line 93
    .restart local v0    # "bCheck":Z
    .restart local v4    # "res":Landroid/content/res/Resources;
    :cond_3
    const v7, 0x7f060055

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 94
    .local v5, "uck":Ljava/lang/String;
    invoke-virtual {p2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public setFrameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;>;"
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter;->mFrameArray:Ljava/util/ArrayList;

    .line 28
    return-void
.end method
