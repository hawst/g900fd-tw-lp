.class public Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field forceAdd:Z

.field id:J

.field scrappedFromPosition:I

.field viewType:I


# direct methods
.method public constructor <init>(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 5839
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 5819
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->id:J

    .line 5841
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->width:I

    if-ne v0, v3, :cond_0

    .line 5842
    const-string v0, "FrameListView"

    const-string v1, "Constructing LayoutParams with width FILL_PARENT does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5845
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->width:I

    .line 5848
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    .line 5849
    const-string v0, "FrameListView"

    const-string v1, "Constructing LayoutParams with height FILL_PARENT does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5852
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->height:I

    .line 5854
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 5857
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 5819
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->id:J

    .line 5859
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->width:I

    if-ne v0, v3, :cond_0

    .line 5860
    const-string v0, "FrameListView"

    const-string v1, "Inflation setting LayoutParams width to MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5863
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->width:I

    .line 5866
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    .line 5867
    const-string v0, "FrameListView"

    const-string v1, "Inflation setting LayoutParams height to MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5870
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->height:I

    .line 5872
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1, "other"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 5875
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5819
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->id:J

    .line 5877
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->width:I

    if-ne v0, v3, :cond_0

    .line 5878
    const-string v0, "FrameListView"

    const-string v1, "Constructing LayoutParams with width MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5881
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->width:I

    .line 5884
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    .line 5885
    const-string v0, "FrameListView"

    const-string v1, "Constructing LayoutParams with height MATCH_PARENT - does not make much sense as the view might change orientation. Falling back to WRAP_CONTENT"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5888
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$LayoutParams;->height:I

    .line 5890
    :cond_1
    return-void
.end method
