.class public Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
.super Ljava/lang/Object;
.source "HelpView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# instance fields
.field private final BEST_FACE_TEXT_LINES_H:I

.field private final BEST_FACE_TEXT_LINES_V:I

.field private final BEST_PHOTO_TEXT_LINES_H:I

.field private final BEST_PHOTO_TEXT_LINES_V:I

.field private final DRAMA_SHOT_TEXT_LINES_H:I

.field private final DRAMA_SHOT_TEXT_LINES_V:I

.field private final ERASER_TEXT_LINES_H:I

.field private final ERASER_TEXT_LINES_V:I

.field private final PICMOTION_BLUR_TEXT_LINES_H:I

.field private final PICMOTION_BLUR_TEXT_LINES_V:I

.field private final PICMOTION_OBJECT_TEXT_LINES_H:I

.field private final PICMOTION_OBJECT_TEXT_LINES_V:I

.field private mBestFaceHelpImgHeightH:I

.field private mBestFaceHelpImgHeightV:I

.field private mBestFaceHelpImgLeftMargin:I

.field private mBestFaceHelpImgTopMarginH:I

.field private mBestFaceHelpImgTopMarginV:I

.field private mBestFaceHelpImgWidthH:I

.field private mBestFaceHelpImgWidthV:I

.field private mBestFaceHelpTextTopMarginH:I

.field private mBestFaceHelpTextTopMarginV:I

.field private mBestFaceHelpTextWidthH:I

.field private mBestFaceHelpTextWidthV:I

.field private mBestPhotoHelpImgHeightH:I

.field private mBestPhotoHelpImgHeightV:I

.field private mBestPhotoHelpImgLeftMargin:I

.field private mBestPhotoHelpImgTopMarginH:I

.field private mBestPhotoHelpImgTopMarginV:I

.field private mBestPhotoHelpImgWidthH:I

.field private mBestPhotoHelpImgWidthV:I

.field private mBestPhotoHelpTextTopMarginH:I

.field private mBestPhotoHelpTextTopMarginV:I

.field private mBestPhotoHelpTextWidthH:I

.field private mBestPhotoHelpTextWidthV:I

.field private mContext:Landroid/content/Context;

.field private mCurrentHelpType:I

.field private mDramaHelpImgHeightH:I

.field private mDramaHelpImgHeightV:I

.field private mDramaHelpImgWidth:I

.field private mDramaHelpTextTopMarginH:I

.field private mDramaHelpTextTopMarginV:I

.field private mDramaHelpTextWidthH:I

.field private mDramaHelpTextWidthV:I

.field private mEraserHelpImgHeightH:I

.field private mEraserHelpImgHeightV:I

.field private mEraserHelpImgLeftMargin:I

.field private mEraserHelpImgTopMarginH:I

.field private mEraserHelpImgTopMarginV:I

.field private mEraserHelpImgWidthH:I

.field private mEraserHelpImgWidthV:I

.field private mEraserHelpTextTopMarginH:I

.field private mEraserHelpTextTopMarginV:I

.field private mEraserHelpTextWidthH:I

.field private mEraserHelpTextWidthV:I

.field private mHandler:Landroid/os/Handler;

.field private mHelpContentView:Landroid/view/View;

.field private mHelpImg:Landroid/widget/ImageView;

.field private mHelpText:Landroid/widget/TextView;

.field private mHelpWindow:Landroid/app/Dialog;

.field private mInitalConfig:Landroid/content/res/Configuration;

.field mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOkButton:Landroid/view/View;

.field private mOkButtonBottomMargin:I

.field private mOkButtonTopMargin:I

.field private mPicMotionBlurHelpImgHeightH:I

.field private mPicMotionBlurHelpImgHeightV:I

.field private mPicMotionBlurHelpImgLeftMargin:I

.field private mPicMotionBlurHelpImgTopMarginH:I

.field private mPicMotionBlurHelpImgTopMarginV:I

.field private mPicMotionBlurHelpImgWidthH:I

.field private mPicMotionBlurHelpImgWidthV:I

.field private mPicMotionBlurHelpTextTopMarginH:I

.field private mPicMotionBlurHelpTextTopMarginV:I

.field private mPicMotionBlurHelpTextWidthH:I

.field private mPicMotionBlurHelpTextWidthV:I

.field private mPicMotionObjectHelpImgHeightH:I

.field private mPicMotionObjectHelpImgHeightV:I

.field private mPicMotionObjectHelpImgWidth:I

.field private mPicMotionObjectHelpTextTopMarginH:I

.field private mPicMotionObjectHelpTextTopMarginV:I

.field private mPicMotionObjectHelpTextWidthH:I

.field private mPicMotionObjectHelpTextWidthV:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x6

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    .line 24
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mContext:Landroid/content/Context;

    .line 25
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mInitalConfig:Landroid/content/res/Configuration;

    .line 26
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpContentView:Landroid/view/View;

    .line 27
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    .line 28
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    .line 29
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    .line 31
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButtonTopMargin:I

    .line 32
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButtonBottomMargin:I

    .line 34
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgTopMarginV:I

    .line 35
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgTopMarginH:I

    .line 36
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgHeightV:I

    .line 37
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgHeightH:I

    .line 38
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgWidthV:I

    .line 39
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgWidthH:I

    .line 40
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgLeftMargin:I

    .line 41
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextTopMarginV:I

    .line 42
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextTopMarginH:I

    .line 43
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextWidthV:I

    .line 44
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextWidthH:I

    .line 46
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgTopMarginV:I

    .line 47
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgTopMarginH:I

    .line 48
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgHeightV:I

    .line 49
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgHeightH:I

    .line 50
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgWidthV:I

    .line 51
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgWidthH:I

    .line 52
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgLeftMargin:I

    .line 53
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextTopMarginV:I

    .line 54
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextTopMarginH:I

    .line 55
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextWidthV:I

    .line 56
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextWidthH:I

    .line 58
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgHeightV:I

    .line 59
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgHeightH:I

    .line 60
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgWidth:I

    .line 61
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextTopMarginV:I

    .line 62
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextTopMarginH:I

    .line 63
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextWidthV:I

    .line 64
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextWidthH:I

    .line 66
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgTopMarginV:I

    .line 67
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgTopMarginH:I

    .line 68
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgWidthV:I

    .line 69
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgWidthH:I

    .line 70
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgHeightV:I

    .line 71
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgHeightH:I

    .line 72
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgLeftMargin:I

    .line 73
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextTopMarginV:I

    .line 74
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextTopMarginH:I

    .line 75
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextWidthV:I

    .line 76
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextWidthH:I

    .line 78
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgTopMarginV:I

    .line 79
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgTopMarginH:I

    .line 80
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgLeftMargin:I

    .line 81
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgHeightV:I

    .line 82
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgHeightH:I

    .line 83
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgWidthV:I

    .line 84
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgWidthH:I

    .line 85
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextTopMarginV:I

    .line 86
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextTopMarginH:I

    .line 87
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextWidthV:I

    .line 88
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextWidthH:I

    .line 90
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgHeightV:I

    .line 91
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgHeightH:I

    .line 92
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgWidth:I

    .line 93
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextTopMarginV:I

    .line 94
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextTopMarginH:I

    .line 95
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextWidthV:I

    .line 96
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextWidthH:I

    .line 102
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->BEST_PHOTO_TEXT_LINES_H:I

    .line 103
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->BEST_PHOTO_TEXT_LINES_V:I

    .line 104
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->BEST_FACE_TEXT_LINES_H:I

    .line 105
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->BEST_FACE_TEXT_LINES_V:I

    .line 106
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->DRAMA_SHOT_TEXT_LINES_H:I

    .line 107
    const/16 v0, 0x9

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->DRAMA_SHOT_TEXT_LINES_V:I

    .line 108
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->ERASER_TEXT_LINES_H:I

    .line 109
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->ERASER_TEXT_LINES_V:I

    .line 110
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->PICMOTION_BLUR_TEXT_LINES_H:I

    .line 111
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->PICMOTION_BLUR_TEXT_LINES_V:I

    .line 112
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->PICMOTION_OBJECT_TEXT_LINES_H:I

    .line 113
    const/16 v0, 0xc

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->PICMOTION_OBJECT_TEXT_LINES_V:I

    .line 218
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHandler:Landroid/os/Handler;

    .line 238
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 542
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mCurrentHelpType:I

    .line 116
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mContext:Landroid/content/Context;

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mInitalConfig:Landroid/content/res/Configuration;

    .line 118
    new-instance v0, Landroid/app/Dialog;

    const v1, 0x7f070007

    invoke-direct {v0, p1, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    .line 119
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030016

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpContentView:Landroid/view/View;

    .line 120
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpContentView:Landroid/view/View;

    const v1, 0x7f090099

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    .line 121
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpContentView:Landroid/view/View;

    const v1, 0x7f090098

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    .line 122
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpContentView:Landroid/view/View;

    const v1, 0x7f09009a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    .line 123
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->getDimensionPixels()V

    .line 125
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;Z)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->getMaxLines(Z)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    return-object v0
.end method

.method private getDimensionPixels()V
    .locals 2

    .prologue
    .line 548
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 549
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f050050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButtonTopMargin:I

    .line 550
    const v1, 0x7f05004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButtonBottomMargin:I

    .line 552
    const v1, 0x7f050051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgTopMarginV:I

    .line 553
    const v1, 0x7f050052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgTopMarginH:I

    .line 554
    const v1, 0x7f050053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgHeightV:I

    .line 555
    const v1, 0x7f050054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgHeightH:I

    .line 556
    const v1, 0x7f050055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgWidthV:I

    .line 557
    const v1, 0x7f050056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgWidthH:I

    .line 558
    const v1, 0x7f050057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgLeftMargin:I

    .line 559
    const v1, 0x7f050058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextTopMarginV:I

    .line 560
    const v1, 0x7f050059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextTopMarginH:I

    .line 561
    const v1, 0x7f05005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextWidthV:I

    .line 562
    const v1, 0x7f05005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextWidthH:I

    .line 564
    const v1, 0x7f05005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgTopMarginV:I

    .line 565
    const v1, 0x7f05005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgTopMarginH:I

    .line 566
    const v1, 0x7f05005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgHeightV:I

    .line 567
    const v1, 0x7f05005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgHeightH:I

    .line 568
    const v1, 0x7f050060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgWidthV:I

    .line 569
    const v1, 0x7f050061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgWidthH:I

    .line 570
    const v1, 0x7f050062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgLeftMargin:I

    .line 571
    const v1, 0x7f050063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextTopMarginV:I

    .line 572
    const v1, 0x7f050064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextTopMarginH:I

    .line 573
    const v1, 0x7f050065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextWidthV:I

    .line 574
    const v1, 0x7f050066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextWidthH:I

    .line 576
    const v1, 0x7f050067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgHeightV:I

    .line 577
    const v1, 0x7f050068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgHeightH:I

    .line 578
    const v1, 0x7f050069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgWidth:I

    .line 579
    const v1, 0x7f05006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextTopMarginV:I

    .line 580
    const v1, 0x7f05006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextTopMarginH:I

    .line 581
    const v1, 0x7f05006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextWidthV:I

    .line 582
    const v1, 0x7f05006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextWidthH:I

    .line 584
    const v1, 0x7f05006e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgTopMarginV:I

    .line 585
    const v1, 0x7f05006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgTopMarginH:I

    .line 586
    const v1, 0x7f050070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgWidthV:I

    .line 587
    const v1, 0x7f050071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgWidthH:I

    .line 588
    const v1, 0x7f050072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgHeightV:I

    .line 589
    const v1, 0x7f050073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgHeightH:I

    .line 590
    const v1, 0x7f050074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgLeftMargin:I

    .line 591
    const v1, 0x7f050075

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextTopMarginV:I

    .line 592
    const v1, 0x7f050076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextTopMarginH:I

    .line 593
    const v1, 0x7f050077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextWidthV:I

    .line 594
    const v1, 0x7f050078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextWidthH:I

    .line 596
    const v1, 0x7f050079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgTopMarginV:I

    .line 597
    const v1, 0x7f05007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgTopMarginH:I

    .line 598
    const v1, 0x7f05007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgLeftMargin:I

    .line 599
    const v1, 0x7f05007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgHeightV:I

    .line 600
    const v1, 0x7f05007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgHeightH:I

    .line 601
    const v1, 0x7f05007e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgWidthV:I

    .line 602
    const v1, 0x7f05007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgWidthH:I

    .line 603
    const v1, 0x7f050080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextTopMarginV:I

    .line 604
    const v1, 0x7f050081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextTopMarginH:I

    .line 605
    const v1, 0x7f050082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextWidthV:I

    .line 606
    const v1, 0x7f050083

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextWidthH:I

    .line 608
    const v1, 0x7f050084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgHeightV:I

    .line 609
    const v1, 0x7f050085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgHeightH:I

    .line 610
    const v1, 0x7f050086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgWidth:I

    .line 611
    const v1, 0x7f050087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextTopMarginV:I

    .line 612
    const v1, 0x7f050088

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextTopMarginH:I

    .line 613
    const v1, 0x7f050089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextWidthV:I

    .line 614
    const v1, 0x7f05008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextWidthH:I

    .line 615
    return-void
.end method

.method private getMaxLines(Z)I
    .locals 2
    .param p1, "isLandscape"    # Z

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "maxlines":I
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mCurrentHelpType:I

    packed-switch v1, :pswitch_data_0

    .line 215
    :cond_0
    :goto_0
    return v0

    .line 179
    :pswitch_0
    const/4 v0, 0x6

    .line 180
    if-eqz p1, :cond_0

    .line 181
    const/4 v0, 0x2

    goto :goto_0

    .line 185
    :pswitch_1
    const/4 v0, 0x6

    .line 186
    if-eqz p1, :cond_0

    .line 187
    const/4 v0, 0x2

    goto :goto_0

    .line 191
    :pswitch_2
    const/16 v0, 0x9

    .line 192
    if-eqz p1, :cond_0

    .line 193
    const/4 v0, 0x4

    goto :goto_0

    .line 197
    :pswitch_3
    const/4 v0, 0x6

    .line 198
    if-eqz p1, :cond_0

    .line 199
    const/4 v0, 0x2

    goto :goto_0

    .line 203
    :pswitch_4
    const/4 v0, 0x6

    .line 204
    if-eqz p1, :cond_0

    .line 205
    const/4 v0, 0x2

    goto :goto_0

    .line 209
    :pswitch_5
    const/16 v0, 0xc

    .line 210
    if-eqz p1, :cond_0

    .line 211
    const/4 v0, 0x4

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private initHelpConfig(ILandroid/content/res/Configuration;)V
    .locals 7
    .param p1, "helpType"    # I
    .param p2, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0xa

    const/4 v2, 0x0

    .line 249
    const/4 v3, 0x2

    iget v4, p2, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_2

    const/4 v0, 0x1

    .line 250
    .local v0, "isLandscape":Z
    :goto_0
    const/4 v1, 0x0

    .line 251
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 252
    .restart local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 253
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 254
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 255
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButtonBottomMargin:I

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 256
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 257
    const/16 v3, 0x12

    if-eq p1, v3, :cond_0

    const/16 v3, 0x15

    if-ne p1, v3, :cond_1

    .line 258
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 259
    .restart local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 260
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 261
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButtonTopMargin:I

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 262
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 263
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 265
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 290
    :goto_1
    return-void

    .end local v0    # "isLandscape":Z
    .end local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    move v0, v2

    .line 249
    goto :goto_0

    .line 267
    .restart local v0    # "isLandscape":Z
    .restart local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->setBestFaceParams(Z)V

    goto :goto_1

    .line 271
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->setBestPhotoParams(Z)V

    goto :goto_1

    .line 275
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->setDramaParams(Z)V

    goto :goto_1

    .line 279
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->setEraserParams(Z)V

    goto :goto_1

    .line 283
    :pswitch_4
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->setPicMotionBlurParams(Z)V

    goto :goto_1

    .line 287
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->setPicMotionObjectParams(Z)V

    goto :goto_1

    .line 265
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setBestPhotoParams(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v3, 0xe

    .line 437
    if-eqz p1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020072

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 442
    :goto_0
    const/4 v0, 0x0

    .line 443
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 444
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 445
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 446
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 447
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 448
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 450
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 451
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 452
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 453
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgLeftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 454
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 455
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgHeightH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 456
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 472
    :goto_1
    return-void

    .line 440
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020071

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 458
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 459
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 460
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpTextWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 461
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 462
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 464
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 465
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 466
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 467
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 468
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 469
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestPhotoHelpImgHeightV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 470
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private setDramaParams(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v3, 0xc

    .line 403
    if-eqz p1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020074

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 408
    :goto_0
    const/4 v0, 0x0

    .line 409
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 411
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 412
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 413
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 414
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 416
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 417
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 418
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgWidth:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 419
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgHeightH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 420
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 434
    :goto_1
    return-void

    .line 406
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020073

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 422
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 423
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 424
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpTextWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 425
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 426
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 428
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 429
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 430
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgWidth:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 431
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mDramaHelpImgHeightV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 432
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private setEraserParams(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v3, 0xe

    .line 365
    if-eqz p1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 370
    :goto_0
    const/4 v0, 0x0

    .line 371
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 372
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 373
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 374
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 375
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 378
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 379
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 380
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 381
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgLeftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 382
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 383
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgHeightH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 384
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    :goto_1
    return-void

    .line 368
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020075

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 386
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 387
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 388
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpTextWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 389
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 390
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 392
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 393
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 394
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 395
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 396
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 397
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mEraserHelpImgHeightV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 398
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private setPicMotionBlurParams(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v3, 0xe

    .line 327
    if-eqz p1, :cond_0

    .line 328
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020078

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 332
    :goto_0
    const/4 v0, 0x0

    .line 333
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 334
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 335
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 336
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 337
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 338
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 340
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 341
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 342
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 343
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgLeftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 344
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 345
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgHeightH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 346
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 362
    :goto_1
    return-void

    .line 330
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020077

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 348
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 349
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 350
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpTextWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 351
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 352
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 354
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 355
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 356
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 357
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 358
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 359
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionBlurHelpImgHeightV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 360
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private setPicMotionObjectParams(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v3, 0xc

    .line 293
    if-eqz p1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020074

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 298
    :goto_0
    const/4 v0, 0x0

    .line 299
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 301
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 302
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 303
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 304
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 306
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 307
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgWidth:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 308
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgHeightH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 309
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 310
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 324
    :goto_1
    return-void

    .line 296
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020073

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 312
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 313
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 314
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpTextWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 315
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 318
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 319
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgWidth:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 320
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mPicMotionObjectHelpImgHeightV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 321
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 322
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method


# virtual methods
.method public getHelpType()I
    .locals 1

    .prologue
    .line 544
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mCurrentHelpType:I

    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 527
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mCurrentHelpType:I

    .line 528
    return-void
.end method

.method public isShown()Z
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 534
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    .line 540
    return-void
.end method

.method public setBestFaceParams(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v3, 0xe

    .line 475
    if-eqz p1, :cond_0

    .line 476
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f020070

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 480
    :goto_0
    const/4 v0, 0x0

    .line 481
    .local v0, "param":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    .line 482
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 483
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 484
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 485
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 486
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 488
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 489
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 490
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgTopMarginH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 491
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgLeftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 492
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgWidthH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 493
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgHeightH:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 494
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 510
    :goto_1
    return-void

    .line 478
    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v2, 0x7f02006f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 496
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 497
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 498
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpTextWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 499
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 500
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 502
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 503
    .restart local v0    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 504
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 505
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgTopMarginV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 506
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgWidthV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 507
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mBestFaceHelpImgHeightV:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 508
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public show(I)V
    .locals 2
    .param p1, "helpType"    # I

    .prologue
    const v1, 0x7f020073

    .line 136
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mCurrentHelpType:I

    .line 137
    packed-switch p1, :pswitch_data_0

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mInitalConfig:Landroid/content/res/Configuration;

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->initHelpConfig(ILandroid/content/res/Configuration;)V

    .line 164
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpContentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 166
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$1;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 172
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 173
    return-void

    .line 139
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v1, 0x7f02006f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 140
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const v1, 0x7f06003f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 143
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v1, 0x7f020071

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 144
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const v1, 0x7f060043

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 147
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 148
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const v1, 0x7f060040

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 151
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v1, 0x7f020075

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 152
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const v1, 0x7f060044

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 155
    :pswitch_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 156
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const v1, 0x7f06004b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 159
    :pswitch_5
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 160
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    const v1, 0x7f060041

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public updateHelpConfig(ILandroid/content/res/Configuration;)V
    .locals 2
    .param p1, "helpType"    # I
    .param p2, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 513
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->initHelpConfig(ILandroid/content/res/Configuration;)V

    .line 514
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$4;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 520
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 521
    return-void
.end method
