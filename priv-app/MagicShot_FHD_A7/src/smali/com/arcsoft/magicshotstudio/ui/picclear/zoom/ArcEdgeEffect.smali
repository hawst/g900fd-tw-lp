.class public Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;
.super Ljava/lang/Object;
.source "ArcEdgeEffect.java"


# static fields
.field private static final EPSILON:F = 0.001f

.field private static final HELD_EDGE_ALPHA:F = 0.7f

.field private static final HELD_EDGE_SCALE_Y:F = 0.5f

.field private static final HELD_GLOW_ALPHA:F = 0.5f

.field private static final HELD_GLOW_SCALE_Y:F = 0.5f

.field private static final MAX_ALPHA:F = 0.8f

.field private static final MAX_GLOW_HEIGHT:F = 4.0f

.field private static final MIN_VELOCITY:I = 0x64

.field private static final PULL_DECAY_TIME:I = 0x3e8

.field private static final PULL_DISTANCE_ALPHA_GLOW_FACTOR:F = 1.1f

.field private static final PULL_DISTANCE_EDGE_FACTOR:I = 0x7

.field private static final PULL_DISTANCE_GLOW_FACTOR:I = 0x7

.field private static final PULL_EDGE_BEGIN:F = 0.6f

.field private static final PULL_GLOW_BEGIN:F = 1.0f

.field private static final PULL_TIME:I = 0xa7

.field private static final RECEDE_TIME:I = 0x3e8

.field private static final STATE_ABSORB:I = 0x2

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PULL:I = 0x1

.field private static final STATE_PULL_DECAY:I = 0x4

.field private static final STATE_RECEDE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ArcEdgeEffect"

.field private static final VELOCITY_EDGE_FACTOR:I = 0x8

.field private static final VELOCITY_GLOW_FACTOR:I = 0x10


# instance fields
.field private final MIN_WIDTH:I

.field private mDuration:F

.field private final mEdge:Landroid/graphics/drawable/Drawable;

.field private mEdgeAlpha:F

.field private mEdgeAlphaFinish:F

.field private mEdgeAlphaStart:F

.field private mEdgeScaleY:F

.field private mEdgeScaleYFinish:F

.field private mEdgeScaleYStart:F

.field private final mGlow:Landroid/graphics/drawable/Drawable;

.field private mGlowAlpha:F

.field private mGlowAlphaFinish:F

.field private mGlowAlphaStart:F

.field private mGlowScaleY:F

.field private mGlowScaleYFinish:F

.field private mGlowScaleYStart:F

.field private mHeight:I

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private mMinWidth:I

.field private mPullDistance:F

.field private mStartTime:J

.field private mState:I

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/16 v1, 0x12c

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->MIN_WIDTH:I

    .line 86
    const/4 v1, 0x0

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 100
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    .line 101
    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    .line 103
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x43960000    # 300.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mMinWidth:I

    .line 105
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 106
    return-void
.end method

.method private update()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/high16 v10, 0x447a0000    # 1000.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 338
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 339
    .local v4, "time":J
    iget-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    sub-long v6, v4, v6

    long-to-float v3, v6

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    div-float/2addr v3, v6

    invoke-static {v3, v9}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 341
    .local v2, "t":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 343
    .local v1, "interp":F
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaFinish:F

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v1

    add-float/2addr v3, v6

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    .line 345
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v1

    add-float/2addr v3, v6

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    .line 347
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaFinish:F

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v1

    add-float/2addr v3, v6

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    .line 349
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v1

    add-float/2addr v3, v6

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    .line 352
    const v3, 0x3f7fbe77    # 0.999f

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_0

    .line 353
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    packed-switch v3, :pswitch_data_0

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 355
    :pswitch_0
    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 356
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    .line 357
    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    .line 359
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    .line 360
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    .line 361
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    .line 362
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    .line 365
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaFinish:F

    .line 366
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    .line 367
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaFinish:F

    .line 368
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    goto :goto_0

    .line 371
    :pswitch_1
    const/4 v3, 0x4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 372
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    .line 373
    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    .line 375
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    .line 376
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    .line 377
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    .line 378
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    .line 381
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaFinish:F

    .line 382
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    .line 383
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaFinish:F

    .line 384
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    goto :goto_0

    .line 389
    :pswitch_2
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    cmpl-float v3, v3, v8

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    mul-float/2addr v3, v6

    div-float v0, v9, v3

    .line 391
    .local v0, "factor":F
    :goto_1
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v1

    mul-float/2addr v6, v0

    add-float/2addr v3, v6

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    .line 394
    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    goto :goto_0

    .line 389
    .end local v0    # "factor":F
    :cond_1
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_1

    .line 397
    :pswitch_3
    const/4 v3, 0x0

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    goto :goto_0

    .line 353
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)Z
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->update()V

    .line 301
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 302
    .local v1, "edgeHeight":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 303
    .local v3, "edgeWidth":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 304
    .local v5, "glowHeight":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 306
    .local v7, "glowWidth":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    const/high16 v10, 0x437f0000    # 255.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 308
    int-to-float v8, v5

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    mul-float/2addr v8, v9

    int-to-float v9, v5

    mul-float/2addr v8, v9

    int-to-float v9, v7

    div-float/2addr v8, v9

    const v9, 0x3f19999a    # 0.6f

    mul-float/2addr v8, v9

    int-to-float v9, v5

    const/high16 v10, 0x40800000    # 4.0f

    mul-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    float-to-int v4, v8

    .line 310
    .local v4, "glowBottom":I
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mMinWidth:I

    if-ge v8, v9, :cond_0

    .line 312
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mMinWidth:I

    sub-int/2addr v8, v9

    div-int/lit8 v6, v8, 0x2

    .line 313
    .local v6, "glowLeft":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    sub-int/2addr v10, v6

    invoke-virtual {v8, v6, v9, v10, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 319
    .end local v6    # "glowLeft":I
    :goto_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 321
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    const/high16 v10, 0x437f0000    # 255.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    invoke-virtual {v8, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 323
    int-to-float v8, v1

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    mul-float/2addr v8, v9

    float-to-int v0, v8

    .line 324
    .local v0, "edgeBottom":I
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mMinWidth:I

    if-ge v8, v9, :cond_1

    .line 326
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mMinWidth:I

    sub-int/2addr v8, v9

    div-int/lit8 v2, v8, 0x2

    .line 327
    .local v2, "edgeLeft":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    sub-int/2addr v10, v2

    invoke-virtual {v8, v2, v9, v10, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 332
    .end local v2    # "edgeLeft":I
    :goto_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 334
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    :goto_2
    return v8

    .line 316
    .end local v0    # "edgeBottom":I
    :cond_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlow:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    invoke-virtual {v8, v9, v10, v11, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 330
    .restart local v0    # "edgeBottom":I
    :cond_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdge:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    invoke-virtual {v8, v9, v10, v11, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    .line 334
    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 149
    return-void
.end method

.method public isFinished()Z
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAbsorb(I)V
    .locals 5
    .param p1, "velocity"    # I

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    .line 253
    const/4 v0, 0x2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 254
    const/16 v0, 0x64

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 256
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    .line 257
    const v0, 0x3dcccccd    # 0.1f

    int-to-float v1, p1

    const v2, 0x3cf5c28f    # 0.03f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    .line 261
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    .line 262
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    .line 265
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    .line 266
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    .line 270
    const/4 v0, 0x0

    mul-int/lit8 v1, p1, 0x8

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaFinish:F

    .line 273
    mul-int/lit8 v0, p1, 0x8

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    .line 280
    const v0, 0x3ccccccd    # 0.025f

    div-int/lit8 v1, p1, 0x64

    mul-int/2addr v1, p1

    int-to-float v1, v1

    const v2, 0x391d4952    # 1.5E-4f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    const/high16 v1, 0x3fe00000    # 1.75f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    .line 283
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    mul-int/lit8 v1, p1, 0x10

    int-to-float v1, v1

    const v2, 0x3727c5ac    # 1.0E-5f

    mul-float/2addr v1, v2

    const v2, 0x3f4ccccd    # 0.8f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaFinish:F

    .line 285
    return-void
.end method

.method public onPull(F)V
    .locals 11
    .param p1, "deltaDistance"    # F

    .prologue
    const/4 v10, 0x1

    const/high16 v9, 0x40e00000    # 7.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const v8, 0x3f4ccccd    # 0.8f

    const/4 v7, 0x0

    .line 165
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    .line 166
    .local v2, "now":J
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    sub-long v4, v2, v4

    long-to-float v4, v4

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 208
    :goto_0
    return-void

    .line 169
    :cond_0
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    if-eq v4, v10, :cond_1

    .line 170
    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    .line 172
    :cond_1
    iput v10, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 174
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    .line 175
    const/high16 v4, 0x43270000    # 167.0f

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    .line 177
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mPullDistance:F

    add-float/2addr v4, p1

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mPullDistance:F

    .line 178
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mPullDistance:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 180
    .local v0, "distance":F
    const v4, 0x3f19999a    # 0.6f

    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    .line 182
    const/high16 v4, 0x3f000000    # 0.5f

    mul-float v5, v0, v9

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    .line 185
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3f8ccccd    # 1.1f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-static {v8, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    .line 190
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 191
    .local v1, "glowChange":F
    cmpl-float v4, p1, v7

    if-lez v4, :cond_2

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mPullDistance:F

    cmpg-float v4, v4, v7

    if-gez v4, :cond_2

    .line 192
    neg-float v1, v1

    .line 194
    :cond_2
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mPullDistance:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_3

    .line 195
    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    .line 199
    :cond_3
    const/high16 v4, 0x40800000    # 4.0f

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    mul-float v6, v1, v9

    add-float/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    .line 204
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaFinish:F

    .line 205
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    .line 206
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaFinish:F

    .line 207
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    goto/16 :goto_0
.end method

.method public onRelease()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 217
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mPullDistance:F

    .line 219
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 236
    :goto_0
    return-void

    .line 223
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mState:I

    .line 224
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlpha:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaStart:F

    .line 225
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleY:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYStart:F

    .line 226
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlpha:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaStart:F

    .line 227
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleY:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYStart:F

    .line 229
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeAlphaFinish:F

    .line 230
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mEdgeScaleYFinish:F

    .line 231
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowAlphaFinish:F

    .line 232
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mGlowScaleYFinish:F

    .line 234
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mStartTime:J

    .line 235
    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mDuration:F

    goto :goto_0
.end method

.method public setMinWidth(I)V
    .locals 0
    .param p1, "minWidth"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mMinWidth:I

    .line 116
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mWidth:I

    .line 128
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->mHeight:I

    .line 129
    return-void
.end method
