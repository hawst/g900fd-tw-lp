.class Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;
.super Ljava/lang/Thread;
.source "PicturesBestEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->StartSelectFrame(IIIIJJII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

.field final synthetic val$bitmapPtr:J

.field final synthetic val$count:I

.field final synthetic val$degree:I

.field final synthetic val$preData:J

.field final synthetic val$screenWidth:I

.field final synthetic val$srcHeight:I

.field final synthetic val$srcWidth:I

.field final synthetic val$thumbWidth:I


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;IIIIIJJI)V
    .locals 1

    .prologue
    .line 514
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$srcWidth:I

    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$srcHeight:I

    iput p4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$screenWidth:I

    iput p5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$thumbWidth:I

    iput p6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$degree:I

    iput-wide p7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$bitmapPtr:J

    iput-wide p9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$preData:J

    iput p11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$count:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/16 v8, 0xa

    .line 517
    invoke-static {v8}, Landroid/os/Process;->setThreadPriority(I)V

    .line 518
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$srcWidth:I

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcWidth:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$602(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 519
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$srcHeight:I

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSrcHeight:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$702(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 520
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$screenWidth:I

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mScreenWidth:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$802(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 521
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$thumbWidth:I

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mThumbWidth:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$902(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 522
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$degree:I

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mDegree:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1302(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 523
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$bitmapPtr:J

    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$preData:J

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$count:I

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mIsForQA:Z
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Z

    move-result v7

    # invokes: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getBestFiveInput(JJIZ)[I
    invoke-static/range {v1 .. v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2400(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;JJIZ)[I

    move-result-object v1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBestIndex:[I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1202(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;[I)[I

    .line 524
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$bitmapPtr:J

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mBitmapPtr:J
    invoke-static {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1002(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;J)J

    .line 525
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$count:I

    int-to-long v2, v1

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mTotalInputSize:J
    invoke-static {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2502(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;J)J

    .line 526
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->val$preData:J

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPreProcessData:J
    invoke-static {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1102(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;J)J

    .line 527
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    const/16 v1, 0xf

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcess:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$402(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 528
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$4;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 529
    return-void
.end method
