.class public Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;
.super Landroid/view/View;
.source "ImageZoomView.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;
.implements Ljava/util/Observer;


# static fields
.field private static final ALPHA_TIME:I = 0x1e

.field private static final ANIMATION_SPEED:I = 0x1a

.field private static final MAX_ALPHA:I = 0xff

.field private static final MIN_ALPHA:I = 0x0

.field private static final TYPE_FADE_IN:I = 0x1

.field private static final TYPE_FADE_NONE:I = 0x0

.field private static final TYPE_FADE_OUT:I = 0x2


# instance fields
.field private mAlpha:I

.field private final mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCurMovingObjIndex:I

.field private mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

.field private mEnableDrawFaceRect:Z

.field private mEnableSetRotate:Z

.field private mFaceRectNinePatch:Landroid/graphics/NinePatch;

.field private mFaceRectsOnBitmap:[Landroid/graphics/RectF;

.field private mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

.field private mFaceRectsOnScreen:[Landroid/graphics/Rect;

.field private mFaceRotateDegree:I

.field private mFadeAnimationType:I

.field private mIsFirstCalc:Z

.field private mIsFirstTime:Z

.field private mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

.field private mLastDirection:I

.field private mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mMinusAlpha:I

.field private mMinusBitmap:Landroid/graphics/Bitmap;

.field private mMinusPaint:Landroid/graphics/Paint;

.field private mMinusPlusRect:[Landroid/graphics/Rect;

.field private mMinus_Plus_UseLeft:[Z

.field private mMinus_Plus_UseTop:[Z

.field private mMovingObjBitmapsOnOriginalPhotos:[Landroid/graphics/Bitmap;

.field private mMovingObjectPaint:Landroid/graphics/Paint;

.field private mOffsetRect:[Landroid/graphics/Rect;

.field private mOldbRemoved:[Z

.field private final mPaint:Landroid/graphics/Paint;

.field private mPatchBitmap:Landroid/graphics/Bitmap;

.field private mPlusAlpha:I

.field private mPlusBitmap:Landroid/graphics/Bitmap;

.field private mPlusPaint:Landroid/graphics/Paint;

.field private mPortraitFitInBitmapRect:Landroid/graphics/Rect;

.field private final mRectDst:Landroid/graphics/Rect;

.field private final mRectSrc:Landroid/graphics/Rect;

.field private mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

.field private m_OffsetX:I

.field private m_OffsetY:I

.field private m_ScaleX:F

.field private m_ScaleY:F

.field private mbRemoved:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    .line 45
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    .line 53
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    .line 365
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 366
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 367
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mIsFirstTime:Z

    .line 501
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    .line 538
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRotateDegree:I

    .line 539
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 540
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 541
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 542
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    .line 543
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseTop:[Z

    .line 544
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mIsFirstCalc:Z

    .line 546
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableSetRotate:Z

    .line 547
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableDrawFaceRect:Z

    .line 549
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    .line 550
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    .line 551
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    .line 552
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    .line 553
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 555
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetX:I

    .line 556
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetY:I

    .line 557
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleX:F

    .line 558
    iput v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleY:F

    .line 560
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    .line 561
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOldbRemoved:[Z

    .line 562
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    .line 563
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOffsetRect:[Landroid/graphics/Rect;

    .line 565
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjBitmapsOnOriginalPhotos:[Landroid/graphics/Bitmap;

    .line 566
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mCurMovingObjIndex:I

    .line 568
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    .line 569
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPaint:Landroid/graphics/Paint;

    .line 570
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusPaint:Landroid/graphics/Paint;

    .line 582
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFadeAnimationType:I

    .line 584
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    .line 585
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    .line 586
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    .line 62
    return-void
.end method

.method private adjustAlpha(I)V
    .locals 3
    .param p1, "movObjIndex"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0xff

    .line 185
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjBitmapsOnOriginalPhotos:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjBitmapsOnOriginalPhotos:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mCurMovingObjIndex:I

    if-ne p1, v0, :cond_4

    .line 191
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFadeAnimationType:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 193
    :pswitch_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    if-ge v0, v1, :cond_2

    .line 194
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    add-int/lit8 v0, v0, 0x1a

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    .line 195
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 202
    :pswitch_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    if-lez v0, :cond_3

    .line 203
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    add-int/lit8 v0, v0, -0x1a

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    .line 204
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_5

    .line 214
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 216
    :cond_5
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationTimer(I)V
    .locals 4
    .param p1, "movObjIndex"    # I

    .prologue
    const-wide/16 v2, 0x1e

    .line 173
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mCurMovingObjIndex:I

    if-ne p1, v0, :cond_1

    .line 174
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    const/16 v1, 0xff

    if-ge v0, v1, :cond_0

    .line 175
    invoke-virtual {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->postInvalidateDelayed(J)V

    .line 178
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    if-lez v0, :cond_1

    .line 179
    invoke-virtual {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->postInvalidateDelayed(J)V

    .line 182
    :cond_1
    return-void
.end method

.method private autoAdjustMinusPlusRectOutside(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p1, "iconRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    .line 291
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 293
    .local v0, "offRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-nez v1, :cond_2

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-gez v1, :cond_2

    .line 294
    iget v1, p1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 299
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_3

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-gez v1, :cond_3

    .line 300
    iget v1, p1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 305
    :cond_1
    :goto_1
    return-object v0

    .line 295
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    iget v1, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 296
    iget v1, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 301
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_1

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 302
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1
.end method

.method private drawMinus_Plus(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "minusPlusRect"    # Landroid/graphics/Rect;
    .param p3, "index"    # I

    .prologue
    const/16 v4, 0xff

    const/4 v1, 0x0

    .line 309
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mCurMovingObjIndex:I

    if-ne p3, v0, :cond_0

    .line 310
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFadeAnimationType:I

    packed-switch v0, :pswitch_data_0

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    array-length v0, v0

    if-ge p3, v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v0, v0, p3

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 352
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 358
    :cond_1
    :goto_0
    return-void

    .line 312
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    array-length v0, v0

    if-ge p3, v0, :cond_1

    .line 313
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    add-int/lit8 v0, v0, -0x1a

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    .line 314
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 316
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 317
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 320
    :cond_2
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    add-int/lit8 v0, v0, 0x1a

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    .line 321
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 323
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 330
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    array-length v0, v0

    if-ge p3, v0, :cond_1

    .line 331
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    add-int/lit8 v0, v0, -0x1a

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    .line 332
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 334
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 335
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 338
    :cond_3
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    add-int/lit8 v0, v0, 0x1a

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    .line 339
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 341
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 354
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 355
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private drawMovingObject(Landroid/graphics/Canvas;I)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "index"    # I

    .prologue
    .line 223
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjBitmapsOnOriginalPhotos:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p2

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v2, v2, p2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 224
    return-void
.end method

.method private faceRectFromPhotoToScreen()V
    .locals 15

    .prologue
    const/high16 v14, 0x3f800000    # 1.0f

    .line 674
    const/4 v5, 0x0

    .line 676
    .local v5, "length":I
    const/4 v11, 0x0

    .line 677
    .local v11, "tempTop":I
    const/4 v9, 0x0

    .line 678
    .local v9, "tempLeft":I
    const/4 v10, 0x0

    .line 679
    .local v10, "tempRight":I
    const/4 v8, 0x0

    .line 681
    .local v8, "tempBottom":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    if-eqz v12, :cond_0

    .line 682
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    array-length v5, v12

    .line 683
    if-gtz v5, :cond_1

    .line 780
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRotateDegree:I

    if-eqz v12, :cond_3

    iget-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableSetRotate:Z

    if-eqz v12, :cond_3

    .line 691
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    if-ge v4, v5, :cond_2

    .line 692
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v11, v12, Landroid/graphics/Rect;->top:I

    .line 693
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v9, v12, Landroid/graphics/Rect;->left:I

    .line 694
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v10, v12, Landroid/graphics/Rect;->right:I

    .line 695
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget v8, v12, Landroid/graphics/Rect;->bottom:I

    .line 696
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRotateDegree:I

    sparse-switch v12, :sswitch_data_0

    .line 691
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 698
    :sswitch_0
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v9, v12, Landroid/graphics/Rect;->top:I

    .line 699
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v8

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 700
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v11

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 701
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v10, v12, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 704
    :sswitch_1
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v8

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 705
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v10

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 706
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v9

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 707
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v13, v11

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 710
    :sswitch_2
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v10

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 711
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v11, v12, Landroid/graphics/Rect;->left:I

    .line 712
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iput v8, v12, Landroid/graphics/Rect;->right:I

    .line 713
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v13, v13, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v13, v9

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 720
    :cond_2
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableSetRotate:Z

    .line 727
    .end local v4    # "index":I
    :cond_3
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v12, :cond_0

    .line 731
    const/high16 v7, 0x3f800000    # 1.0f

    .line 732
    .local v7, "ratioW":F
    const/high16 v6, 0x3f800000    # 1.0f

    .line 733
    .local v6, "ratioH":F
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    mul-float v1, v12, v14

    .line 734
    .local v1, "bitmapWidth":F
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float v0, v12, v14

    .line 736
    .local v0, "bitmapHeight":F
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    if-eqz v12, :cond_4

    .line 737
    iget v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRotateDegree:I

    sparse-switch v12, :sswitch_data_1

    .line 753
    :cond_4
    :goto_3
    const/4 v4, 0x0

    .restart local v4    # "index":I
    :goto_4
    if-ge v4, v5, :cond_5

    .line 754
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->top:I

    int-to-float v13, v13

    div-float/2addr v13, v6

    iput v13, v12, Landroid/graphics/RectF;->top:F

    .line 755
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->left:I

    int-to-float v13, v13

    div-float/2addr v13, v7

    iput v13, v12, Landroid/graphics/RectF;->left:F

    .line 756
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->right:I

    int-to-float v13, v13

    div-float/2addr v13, v7

    iput v13, v12, Landroid/graphics/RectF;->right:F

    .line 757
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v13, v13

    div-float/2addr v13, v6

    iput v13, v12, Landroid/graphics/RectF;->bottom:F

    .line 753
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 740
    .end local v4    # "index":I
    :sswitch_3
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v7, v12, v1

    .line 741
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v6, v12, v0

    .line 742
    goto :goto_3

    .line 745
    :sswitch_4
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v7, v12, v1

    .line 746
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v12, v12, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v12, v12

    mul-float/2addr v12, v14

    div-float v6, v12, v0

    .line 747
    goto :goto_3

    .line 766
    .restart local v4    # "index":I
    :cond_5
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v5, :cond_6

    .line 767
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->top:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetY:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleY:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 768
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->left:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetX:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleX:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 769
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->right:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetX:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleX:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 770
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v12, v12, v4

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v13, v13, v4

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetY:I

    int-to-float v14, v14

    sub-float/2addr v13, v14

    iget v14, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleY:F

    div-float/2addr v13, v14

    float-to-int v13, v13

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    .line 766
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 773
    :cond_6
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v12

    if-nez v12, :cond_0

    .line 774
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    div-int/lit8 v3, v12, 0x2

    .line 775
    .local v3, "half_icon_width":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    div-int/lit8 v2, v12, 0x2

    .line 776
    .local v2, "half_icon_height":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    invoke-direct {p0, v12, v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->updateMinusPlusPosition([Landroid/graphics/Rect;II)V

    .line 777
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    invoke-direct {p0, v12, v3, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->updateMinusPlusRect([Landroid/graphics/Rect;II)V

    goto/16 :goto_0

    .line 696
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch

    .line 737
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x5a -> :sswitch_3
        0xb4 -> :sswitch_4
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;
    .locals 10
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I
    .param p3, "bitmapWidth"    # I
    .param p4, "bitmapHeight"    # I

    .prologue
    .line 443
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->getRotateScreenAspectQuotient()F

    move-result v0

    .line 444
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v1

    .line 445
    .local v1, "panX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v2

    .line 446
    .local v2, "panY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v7

    int-to-float v8, p1

    mul-float/2addr v7, v8

    int-to-float v8, p3

    div-float v5, v7, v8

    .line 447
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v7

    int-to-float v8, p2

    mul-float/2addr v7, v8

    int-to-float v8, p4

    div-float v6, v7, v8

    .line 450
    .local v6, "zoomY":F
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 451
    .local v4, "rectSrc":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 452
    .local v3, "rectDst":Landroid/graphics/Rect;
    int-to-float v7, p3

    mul-float/2addr v7, v1

    int-to-float v8, p1

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v5

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 453
    int-to-float v7, p4

    mul-float/2addr v7, v2

    int-to-float v8, p2

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v6

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->top:I

    .line 454
    iget v7, v4, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    int-to-float v8, p1

    div-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->right:I

    .line 455
    iget v7, v4, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    int-to-float v8, p2

    div-float/2addr v8, v6

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->bottom:I

    .line 456
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getLeft()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 457
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getTop()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 458
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getBottom()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 459
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getRight()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->bottom:I

    .line 462
    iget v7, v4, Landroid/graphics/Rect;->left:I

    if-gez v7, :cond_0

    .line 463
    iget v7, v3, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->left:I

    neg-int v8, v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 464
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 466
    :cond_0
    iget v7, v4, Landroid/graphics/Rect;->right:I

    if-le v7, p3, :cond_1

    .line 467
    iget v7, v3, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, p3

    int-to-float v8, v8

    mul-float/2addr v8, v5

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 468
    iput p3, v4, Landroid/graphics/Rect;->right:I

    .line 470
    :cond_1
    iget v7, v4, Landroid/graphics/Rect;->top:I

    if-gez v7, :cond_2

    .line 471
    iget v7, v3, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->top:I

    neg-int v8, v8

    int-to-float v8, v8

    mul-float/2addr v8, v6

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 472
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/Rect;->top:I

    .line 474
    :cond_2
    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    if-le v7, p4, :cond_3

    .line 475
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, p4

    int-to-float v8, v8

    mul-float/2addr v8, v6

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->bottom:I

    .line 476
    iput p4, v4, Landroid/graphics/Rect;->bottom:I

    .line 479
    :cond_3
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v7
.end method

.method private isMinusPlusRectOutOfScreen(Landroid/graphics/Rect;)Z
    .locals 3
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v0, 0x1

    .line 155
    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-gez v1, :cond_0

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-ltz v1, :cond_3

    :cond_0
    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-gez v1, :cond_1

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-gt v1, v2, :cond_3

    :cond_1
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v1, v2, :cond_2

    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-ltz v1, :cond_3

    :cond_2
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v1, v2, :cond_4

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-le v1, v2, :cond_4

    .line 168
    :cond_3
    :goto_0
    return v0

    .line 159
    :cond_4
    iget v1, p1, Landroid/graphics/Rect;->left:I

    if-ltz v1, :cond_3

    .line 161
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-gt v1, v2, :cond_3

    .line 163
    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-ltz v1, :cond_3

    .line 165
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-gt v1, v2, :cond_3

    .line 168
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseOldEdgeEffect(I)V
    .locals 3
    .param p1, "currentDirection"    # I

    .prologue
    .line 503
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    xor-int v0, p1, v2

    .line 504
    .local v0, "changed":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    and-int v1, v0, v2

    .line 505
    .local v1, "needRelease":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->onRelease(I)V

    .line 506
    return-void
.end method

.method private showBounceView(I)V
    .locals 3
    .param p1, "currentDirection"    # I

    .prologue
    const v2, 0x3ca3d70a    # 0.02f

    .line 509
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_4

    .line 510
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->onPull(FI)V

    .line 515
    :cond_0
    :goto_0
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    .line 516
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->onPull(FI)V

    .line 521
    :cond_1
    :goto_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_6

    .line 522
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->onPull(FI)V

    .line 527
    :cond_2
    :goto_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_7

    .line 528
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->onPull(FI)V

    .line 532
    :cond_3
    :goto_3
    return-void

    .line 512
    :cond_4
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 518
    :cond_5
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    goto :goto_1

    .line 524
    :cond_6
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    goto :goto_2

    .line 530
    :cond_7
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_3

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    goto :goto_3
.end method

.method private updateDrawRect()V
    .locals 15

    .prologue
    .line 377
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    if-eqz v11, :cond_4

    .line 378
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 380
    .local v0, "aspectQuotient":F
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v8

    .line 381
    .local v8, "viewWidth":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v7

    .line 382
    .local v7, "viewHeight":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 383
    .local v2, "bitmapWidth":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 385
    .local v1, "bitmapHeight":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v3

    .line 386
    .local v3, "panX":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v4

    .line 387
    .local v4, "panY":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v11, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v11

    int-to-float v12, v8

    mul-float/2addr v11, v12

    int-to-float v12, v2

    div-float v9, v11, v12

    .line 388
    .local v9, "zoomX":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v11, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v11

    int-to-float v12, v7

    mul-float/2addr v11, v12

    int-to-float v12, v1

    div-float v10, v11, v12

    .line 391
    .local v10, "zoomY":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v12, v2

    mul-float/2addr v12, v3

    int-to-float v13, v8

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v9

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 392
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v12, v1

    mul-float/2addr v12, v4

    int-to-float v13, v7

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v10

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 393
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    int-to-float v13, v8

    div-float/2addr v13, v9

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 394
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    int-to-float v13, v7

    div-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 395
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getLeft()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 396
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getTop()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 397
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getRight()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 398
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getBottom()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 401
    int-to-float v11, v2

    mul-float/2addr v11, v3

    int-to-float v12, v8

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v9

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetX:I

    .line 402
    int-to-float v11, v1

    mul-float/2addr v11, v4

    int-to-float v12, v7

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v10

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_OffsetY:I

    .line 404
    int-to-float v11, v8

    div-float/2addr v11, v9

    float-to-int v6, v11

    .line 405
    .local v6, "srcWidth":I
    int-to-float v11, v7

    div-float/2addr v11, v10

    float-to-int v5, v11

    .line 408
    .local v5, "srcHeight":I
    int-to-float v11, v6

    int-to-float v12, v8

    div-float/2addr v11, v12

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleX:F

    .line 409
    int-to-float v11, v5

    int-to-float v12, v7

    div-float/2addr v11, v12

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->m_ScaleY:F

    .line 412
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    if-gez v11, :cond_0

    .line 413
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    neg-int v13, v13

    int-to-float v13, v13

    mul-float/2addr v13, v9

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 414
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    const/4 v12, 0x0

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 416
    :cond_0
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    if-le v11, v2, :cond_1

    .line 417
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->right:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    sub-int/2addr v13, v2

    int-to-float v13, v13

    mul-float/2addr v13, v9

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 418
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 420
    :cond_1
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    if-gez v11, :cond_2

    .line 421
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    neg-int v13, v13

    int-to-float v13, v13

    mul-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 422
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    const/4 v12, 0x0

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 424
    :cond_2
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    if-le v11, v1, :cond_3

    .line 425
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->bottom:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v13, v1

    int-to-float v13, v13

    mul-float/2addr v13, v10

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 426
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iput v1, v11, Landroid/graphics/Rect;->bottom:I

    .line 428
    :cond_3
    iget-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mIsFirstTime:Z

    if-eqz v11, :cond_4

    .line 429
    if-ge v8, v7, :cond_5

    .line 430
    new-instance v11, Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 431
    invoke-direct {p0, v7, v8, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;

    move-result-object v11

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 436
    :goto_0
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mIsFirstTime:Z

    .line 439
    .end local v0    # "aspectQuotient":F
    .end local v1    # "bitmapHeight":I
    .end local v2    # "bitmapWidth":I
    .end local v3    # "panX":F
    .end local v4    # "panY":F
    .end local v5    # "srcHeight":I
    .end local v6    # "srcWidth":I
    .end local v7    # "viewHeight":I
    .end local v8    # "viewWidth":I
    .end local v9    # "zoomX":F
    .end local v10    # "zoomY":F
    :cond_4
    return-void

    .line 433
    .restart local v0    # "aspectQuotient":F
    .restart local v1    # "bitmapHeight":I
    .restart local v2    # "bitmapWidth":I
    .restart local v3    # "panX":F
    .restart local v4    # "panY":F
    .restart local v5    # "srcHeight":I
    .restart local v6    # "srcWidth":I
    .restart local v7    # "viewHeight":I
    .restart local v8    # "viewWidth":I
    .restart local v9    # "zoomX":F
    .restart local v10    # "zoomY":F
    :cond_5
    new-instance v11, Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 434
    invoke-direct {p0, v7, v8, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;

    move-result-object v11

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private updateIconRectWithOffset(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p1, "src"    # Landroid/graphics/Rect;
    .param p2, "offset"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    .line 259
    if-nez p2, :cond_0

    .line 287
    .end local p1    # "src":Landroid/graphics/Rect;
    :goto_0
    return-object p1

    .line 263
    .restart local p1    # "src":Landroid/graphics/Rect;
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 265
    .local v0, "result":Landroid/graphics/Rect;
    iget v1, p2, Landroid/graphics/Rect;->left:I

    if-lez v1, :cond_1

    .line 266
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 267
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 276
    :goto_1
    iget v1, p2, Landroid/graphics/Rect;->top:I

    if-lez v1, :cond_3

    .line 277
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 278
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :goto_2
    move-object p1, v0

    .line 287
    goto :goto_0

    .line 268
    :cond_1
    iget v1, p2, Landroid/graphics/Rect;->right:I

    if-lez v1, :cond_2

    .line 269
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 270
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 272
    :cond_2
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 273
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 279
    :cond_3
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    if-lez v1, :cond_4

    .line 280
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 281
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 283
    :cond_4
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 284
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_2
.end method

.method private updateMinusPlusPosition([Landroid/graphics/Rect;II)V
    .locals 4
    .param p1, "referenceRect"    # [Landroid/graphics/Rect;
    .param p2, "half_icon_width"    # I
    .param p3, "half_icon_height"    # I

    .prologue
    const/4 v3, 0x0

    .line 818
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mIsFirstCalc:Z

    if-eqz v1, :cond_3

    .line 819
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    array-length v1, v1

    array-length v2, p1

    if-lt v1, v2, :cond_2

    .line 820
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 821
    aget-object v1, p1, v0

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, p2

    div-int/lit8 v1, v1, 0x2

    if-gez v1, :cond_0

    .line 822
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    aput-boolean v3, v1, v0

    .line 824
    :cond_0
    aget-object v1, p1, v0

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p3

    div-int/lit8 v1, v1, 0x2

    if-gez v1, :cond_1

    .line 825
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseTop:[Z

    aput-boolean v3, v1, v0

    .line 820
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 830
    .end local v0    # "i":I
    :cond_2
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mIsFirstCalc:Z

    .line 832
    :cond_3
    return-void
.end method

.method private updateMinusPlusRect([Landroid/graphics/Rect;II)V
    .locals 3
    .param p1, "referenceRect"    # [Landroid/graphics/Rect;
    .param p2, "half_icon_width"    # I
    .param p3, "half_icon_height"    # I

    .prologue
    .line 783
    if-eqz p1, :cond_3

    array-length v1, p1

    if-lez v1, :cond_3

    .line 784
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 785
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 786
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 787
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 788
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 789
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 796
    :goto_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseTop:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_2

    .line 797
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 798
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 784
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 792
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 793
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, p2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 801
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 802
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, p3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 815
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method private updateRectWithOffset(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3
    .param p1, "src"    # Landroid/graphics/Rect;
    .param p2, "offset"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    .line 244
    if-nez p2, :cond_0

    .line 255
    .end local p1    # "src":Landroid/graphics/Rect;
    :goto_0
    return-object p1

    .line 248
    .restart local p1    # "src":Landroid/graphics/Rect;
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 250
    .local v0, "result":Landroid/graphics/Rect;
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 251
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 252
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 253
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    move-object p1, v0

    .line 255
    goto :goto_0
.end method


# virtual methods
.method public getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    return-object v0
.end method

.method public getLandscapeFitInBitmapRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getMoveObjIndexArray()[I
    .locals 7

    .prologue
    .line 619
    const/4 v3, 0x0

    .line 621
    .local v3, "indexArray":[I
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    array-length v5, v5

    if-gtz v5, :cond_0

    .line 622
    const/4 v5, 0x0

    .line 643
    :goto_0
    return-object v5

    .line 625
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 626
    .local v1, "changed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    array-length v5, v5

    if-ge v2, v5, :cond_3

    .line 627
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v5, v5, v2

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOldbRemoved:[Z

    aget-boolean v6, v6, v2

    if-ne v5, v6, :cond_2

    const/4 v0, 0x0

    .line 628
    .local v0, "bChanged":Z
    :goto_2
    if-eqz v0, :cond_1

    .line 629
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    :cond_1
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOldbRemoved:[Z

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v6, v6, v2

    aput-boolean v6, v5, v2

    .line 626
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 627
    .end local v0    # "bChanged":Z
    :cond_2
    const/4 v0, 0x1

    goto :goto_2

    .line 634
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 635
    .local v4, "length":I
    if-lez v4, :cond_4

    .line 636
    new-array v3, v4, [I

    .line 639
    :cond_4
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v4, :cond_5

    .line 640
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v2

    .line 639
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    move-object v5, v3

    .line 643
    goto :goto_0
.end method

.method public getPortraitFitInBitmapRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRectChoosed(FF)I
    .locals 13
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 846
    const/4 v4, -0x1

    .line 847
    .local v4, "index":I
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v8, :cond_0

    .line 848
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v8, v8

    if-ge v2, v8, :cond_0

    .line 849
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v8, v8, v2

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v8, v8, v2

    float-to-int v11, p1

    float-to-int v12, p2

    invoke-virtual {v8, v11, v12}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 850
    move v4, v2

    .line 855
    .end local v2    # "i":I
    :cond_0
    if-gez v4, :cond_7

    .line 856
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 857
    .local v7, "targetRectList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;>;"
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    if-eqz v8, :cond_3

    .line 858
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    array-length v8, v8

    if-ge v2, v8, :cond_3

    .line 859
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v8, v8, v2

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v8, v8, v2

    float-to-int v11, p1

    float-to-int v12, p2

    invoke-virtual {v8, v11, v12}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 860
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 861
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v11, v11, v2

    invoke-interface {v5, v8, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 862
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 858
    .end local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 848
    .end local v7    # "targetRectList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;>;"
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 867
    .end local v2    # "i":I
    .restart local v7    # "targetRectList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;>;"
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-ne v8, v9, :cond_4

    .line 868
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 869
    .restart local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 870
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_2

    .line 872
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    :cond_4
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-le v8, v9, :cond_7

    .line 873
    const v0, 0x7fffffff

    .line 874
    .local v0, "area":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_7

    .line 875
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 876
    .restart local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 877
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    .line 878
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    mul-int/2addr v8, v11

    if-lt v0, v8, :cond_5

    .line 879
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v11

    mul-int v0, v8, v11

    .line 880
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_4

    .line 874
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 887
    .end local v0    # "area":I
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    .end local v7    # "targetRectList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;>;"
    :cond_7
    const/4 v8, -0x1

    if-eq v4, v8, :cond_8

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    array-length v8, v8

    if-ge v4, v8, :cond_8

    .line 888
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v8, v8, v4

    if-nez v8, :cond_9

    move v8, v9

    :goto_5
    aput-boolean v8, v11, v4

    .line 890
    :cond_8
    return v4

    :cond_9
    move v8, v10

    .line 888
    goto :goto_5
.end method

.method public initRect(Landroid/content/Context;III)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rect_ResID"    # I
    .param p3, "minus_ResID"    # I
    .param p4, "plus_ResID"    # I

    .prologue
    .line 651
    invoke-static {p1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    .line 652
    new-instance v0, Landroid/graphics/NinePatch;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    .line 655
    invoke-static {p1, p3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    .line 656
    invoke-static {p1, p4}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    .line 658
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    .line 659
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPaint:Landroid/graphics/Paint;

    .line 660
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusPaint:Landroid/graphics/Paint;

    .line 661
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 113
    const/high16 v2, -0x1000000

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 114
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    if-eqz v2, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->updateDrawRect()V

    .line 116
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 119
    :cond_0
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableDrawFaceRect:Z

    if-eqz v2, :cond_1

    .line 120
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->faceRectFromPhotoToScreen()V

    .line 122
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    if-nez v2, :cond_2

    .line 152
    :cond_1
    return-void

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    array-length v1, v2

    .line 128
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 129
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->adjustAlpha(I)V

    .line 130
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->animationTimer(I)V

    .line 131
    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->drawMovingObject(Landroid/graphics/Canvas;I)V

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 136
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->isMinusPlusRectOutOfScreen(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 137
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOffsetRect:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    .line 138
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOffsetRect:[Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    invoke-direct {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->autoAdjustMinusPlusRectOutside(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    aput-object v3, v2, v0

    .line 141
    :cond_4
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mCurMovingObjIndex:I

    if-ne v0, v2, :cond_7

    .line 142
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 143
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjectPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, p1, v3, v4}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 147
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 148
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOffsetRect:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->updateIconRectWithOffset(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {p0, p1, v2, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->drawMinus_Plus(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    .line 135
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 144
    :cond_7
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_5

    .line 145
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectNinePatch:Landroid/graphics/NinePatch;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v3, v3, v0

    invoke-virtual {v2, p1, v3}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 484
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 485
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    sub-int v1, p4, p2

    int-to-float v1, v1

    sub-int v2, p5, p3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->updateAspectQuotient(FFFF)V

    .line 487
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->notifyObservers()V

    .line 488
    return-void
.end method

.method public setCurMovingObjIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/16 v2, 0xff

    const/4 v1, 0x0

    .line 227
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mCurMovingObjIndex:I

    .line 228
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFadeAnimationType:I

    .line 230
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    .line 231
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    .line 232
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    .line 240
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->invalidate()V

    .line 241
    return-void

    .line 234
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFadeAnimationType:I

    .line 235
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAlpha:I

    .line 236
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusAlpha:I

    .line 237
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusAlpha:I

    goto :goto_0
.end method

.method public setDrawFaceRect(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 841
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableDrawFaceRect:Z

    .line 842
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->invalidate()V

    .line 843
    return-void
.end method

.method public setEdgeBound()V
    .locals 5

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->updateDrawRect()V

    .line 362
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mRectDst:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->setEdgeBound(IIII)V

    .line 363
    return-void
.end method

.method public setEdgeView(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;)V
    .locals 0
    .param p1, "edgeView"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    .line 66
    return-void
.end method

.method public setFaceRect([Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "rects"    # [Landroid/graphics/Rect;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 589
    if-eqz p1, :cond_1

    .line 590
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 591
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    array-length v1, v2

    .line 592
    .local v1, "length":I
    new-array v2, v1, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 593
    new-array v2, v1, [Landroid/graphics/RectF;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 594
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    .line 595
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseTop:[Z

    .line 596
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    .line 597
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOldbRemoved:[Z

    .line 598
    new-array v2, v1, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    .line 599
    new-array v2, v1, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOffsetRect:[Landroid/graphics/Rect;

    .line 600
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 601
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, v2, v0

    .line 602
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    aput-object v3, v2, v0

    .line 603
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mbRemoved:[Z

    aput-boolean v5, v2, v0

    .line 604
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mOldbRemoved:[Z

    aput-boolean v5, v2, v0

    .line 605
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusPlusRect:[Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, v2, v0

    .line 606
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseLeft:[Z

    aput-boolean v4, v2, v0

    .line 607
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinus_Plus_UseTop:[Z

    aput-boolean v4, v2, v0

    .line 600
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 610
    :cond_0
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mEnableSetRotate:Z

    .line 612
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_1
    return-void
.end method

.method public setFaceRectRotateDegree(I)V
    .locals 0
    .param p1, "degree"    # I

    .prologue
    .line 647
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mFaceRotateDegree:I

    .line 648
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->updateAspectQuotient(FFFF)V

    .line 78
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->notifyObservers()V

    .line 80
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->invalidate()V

    .line 81
    return-void
.end method

.method public setMovingObjBitmaps([Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmaps"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 615
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMovingObjBitmapsOnOriginalPhotos:[Landroid/graphics/Bitmap;

    .line 616
    return-void
.end method

.method public setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 0
    .param p1, "size"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 835
    if-eqz p1, :cond_0

    .line 836
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 838
    :cond_0
    return-void
.end method

.method public setZoomState(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;)V
    .locals 1
    .param p1, "state"    # Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 93
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    .line 94
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->addObserver(Ljava/util/Observer;)V

    .line 96
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->invalidate()V

    .line 97
    return-void
.end method

.method public unInit()V
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPatchBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 668
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mMinusBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 669
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mPlusBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 670
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 492
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getEffectDirection()I

    move-result v0

    .line 493
    .local v0, "direction":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setEdgeBound()V

    .line 494
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->releaseOldEdgeEffect(I)V

    .line 495
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->showBounceView(I)V

    .line 496
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->mLastDirection:I

    .line 497
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->invalidate()V

    .line 498
    return-void
.end method
