.class Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;
.super Ljava/lang/Object;
.source "ArcZoomController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v4, 0x3e800000    # 0.25f

    const/4 v6, 0x0

    const/high16 v3, 0x40200000    # 2.5f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 321
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomInOrOut:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 322
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 323
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_2

    const/4 v0, 0x1

    .line 325
    .local v0, "isAtRest":Z
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v2

    mul-float/2addr v2, v4

    # -= operator for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$224(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 326
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 330
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->zoomWithoutEdgeEffect(FFF)V

    .line 332
    if-eqz v0, :cond_3

    .line 333
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 362
    .end local v0    # "isAtRest":Z
    :cond_1
    :goto_1
    return-void

    .line 323
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 335
    .restart local v0    # "isAtRest":Z
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 336
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanX(F)V

    .line 337
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->setPanY(F)V

    .line 338
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->notifyObservers()V

    goto :goto_1

    .line 341
    .end local v0    # "isAtRest":Z
    :cond_4
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 345
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoom()F

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 346
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-nez v1, :cond_6

    const/4 v0, 0x1

    .line 347
    .restart local v0    # "isAtRest":Z
    :goto_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v2

    mul-float/2addr v2, v4

    # += operator for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$216(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 348
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    .line 349
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 352
    :cond_5
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->zoomWithoutEdgeEffect(FFF)V

    .line 354
    if-nez v0, :cond_7

    .line 355
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 346
    .end local v0    # "isAtRest":Z
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 357
    .restart local v0    # "isAtRest":Z
    :cond_7
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$202(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 358
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInXPosition:F
    invoke-static {v1, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$302(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    .line 359
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->mDoubleZoomInYPosition:F
    invoke-static {v1, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->access$402(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;F)F

    goto/16 :goto_1
.end method
