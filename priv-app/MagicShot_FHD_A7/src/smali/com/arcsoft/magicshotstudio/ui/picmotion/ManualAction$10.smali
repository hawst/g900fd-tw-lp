.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;
.super Ljava/lang/Object;
.source "ManualAction.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/picmotion/ShowBounceViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 1801
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionDown(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->actionDown(II)V

    .line 1833
    return-void
.end method

.method public actionMove(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1845
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->actionMove(II)V

    .line 1846
    return-void
.end method

.method public actionUp()V
    .locals 1

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->actionUp()V

    .line 1837
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->reSetDirection()V

    .line 1838
    return-void
.end method

.method public dealMinZoom()V
    .locals 1

    .prologue
    .line 1853
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mZoomListener:Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4900(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/SimpleImageZoomListener;->dealMinZoom()V

    .line 1854
    return-void
.end method

.method public endAllBounceView(Z)V
    .locals 2
    .param p1, "bInvalidate"    # Z

    .prologue
    .line 1811
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1812
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1813
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->endAllBounce(Landroid/graphics/Rect;Z)V

    .line 1815
    :cond_0
    return-void
.end method

.method public endSingleBounce(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1825
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1826
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1827
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->endSingleBounce(Landroid/graphics/Rect;I)V

    .line 1829
    :cond_0
    return-void
.end method

.method public getIsTouching()Z
    .locals 1

    .prologue
    .line 1849
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->getIsTouching()Z

    move-result v0

    return v0
.end method

.method public resetDirection()V
    .locals 1

    .prologue
    .line 1841
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->reSetDirection()V

    .line 1842
    return-void
.end method

.method public startAllBounceView()V
    .locals 2

    .prologue
    .line 1804
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1805
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1806
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startAllBounce(Landroid/graphics/Rect;)V

    .line 1808
    :cond_0
    return-void
.end method

.method public startBounce(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 1818
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/PicImageView;->getRectForBounceView()Landroid/graphics/Rect;

    move-result-object v0

    .line 1819
    .local v0, "imageRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_0

    .line 1820
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$10;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mBounceView:Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$4800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startBounce(Landroid/graphics/Rect;I)V

    .line 1822
    :cond_0
    return-void
.end method
