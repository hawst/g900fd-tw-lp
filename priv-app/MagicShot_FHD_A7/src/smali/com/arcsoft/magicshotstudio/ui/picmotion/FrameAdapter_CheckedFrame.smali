.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;
.super Landroid/widget/BaseAdapter;
.source "FrameAdapter_CheckedFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;
    }
.end annotation


# instance fields
.field mActivatedPostion:I

.field mContext:Landroid/content/Context;

.field mFrameArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;",
            ">;"
        }
    .end annotation
.end field

.field mInflater:Landroid/view/LayoutInflater;

.field mParent:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 25
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mInflater:Landroid/view/LayoutInflater;

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    .line 27
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 39
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 62
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 67
    iput-object p3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mParent:Landroid/view/ViewGroup;

    .line 69
    if-nez p2, :cond_1

    .line 70
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 71
    .local v3, "width":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 73
    .local v0, "height":I
    if-le v3, v0, :cond_0

    .line 74
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030024

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 78
    :goto_0
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;)V

    .line 79
    .local v1, "holder":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;
    const v4, 0x7f09008f

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    .line 82
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 89
    .end local v0    # "height":I
    .end local v3    # "width":I
    :goto_1
    iget-object v4, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 91
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    if-eqz v4, :cond_2

    .line 92
    iget-object v5, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 97
    :goto_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-boolean v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    invoke-virtual {p2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 99
    const v4, 0x7f090090

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 102
    .local v2, "selectorView":Landroid/view/View;
    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    if-ne v4, p1, :cond_3

    .line 103
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/view/View;->setActivated(Z)V

    .line 108
    :goto_3
    return-object p2

    .line 76
    .end local v1    # "holder":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;
    .end local v2    # "selectorView":Landroid/view/View;
    .restart local v0    # "height":I
    .restart local v3    # "width":I
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030025

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 84
    .end local v0    # "height":I
    .end local v3    # "width":I
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;

    .restart local v1    # "holder":Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;
    goto :goto_1

    .line 94
    :cond_2
    iget-object v5, v1, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame$ViewHolder;->frame:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mGreyFrame:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 105
    .restart local v2    # "selectorView":Landroid/view/View;
    :cond_3
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setActivated(Z)V

    goto :goto_3
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;

    iget-boolean v0, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;->mChecked:Z

    return v0
.end method

.method public setActivated(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mActivatedPostion:I

    .line 53
    return-void
.end method

.method public setFrameList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameData;>;"
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/FrameAdapter_CheckedFrame;->mFrameArray:Ljava/util/ArrayList;

    .line 32
    return-void
.end method
