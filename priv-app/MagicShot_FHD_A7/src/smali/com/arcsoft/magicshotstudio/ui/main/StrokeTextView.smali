.class public Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;
.super Landroid/widget/TextView;
.source "StrokeTextView.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mRestoreColor:I

.field private strokeColor:Ljava/lang/Integer;

.field private strokeJoin:Landroid/graphics/Paint$Join;

.field private strokeMiter:F

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mContext:Landroid/content/Context;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mRestoreColor:I

    .line 26
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mContext:Landroid/content/Context;

    .line 27
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->init(Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mContext:Landroid/content/Context;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mRestoreColor:I

    .line 32
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mContext:Landroid/content/Context;

    .line 33
    invoke-direct {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->init(Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mContext:Landroid/content/Context;

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mRestoreColor:I

    .line 38
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mContext:Landroid/content/Context;

    .line 39
    invoke-direct {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->init(Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 44
    if-eqz p1, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/arcsoft/magicshotstudio/R$styleable;->StrokeTextView:[I

    invoke-virtual {v5, p1, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 46
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 47
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    .line 48
    .local v4, "strokeWidth":F
    const/high16 v5, -0x1000000

    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 49
    .local v1, "strokeColor":I
    const/4 v5, 0x1

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    .line 51
    .local v3, "strokeMiter":F
    const/4 v2, 0x0

    .line 52
    .local v2, "strokeJoin":Landroid/graphics/Paint$Join;
    const/4 v5, 0x3

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 63
    :goto_0
    invoke-virtual {p0, v4, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->setStroke(FILandroid/graphics/Paint$Join;F)V

    .line 65
    .end local v1    # "strokeColor":I
    .end local v2    # "strokeJoin":Landroid/graphics/Paint$Join;
    .end local v3    # "strokeMiter":F
    .end local v4    # "strokeWidth":F
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_1
    return-void

    .line 54
    .restart local v0    # "a":Landroid/content/res/TypedArray;
    .restart local v1    # "strokeColor":I
    .restart local v2    # "strokeJoin":Landroid/graphics/Paint$Join;
    .restart local v3    # "strokeMiter":F
    .restart local v4    # "strokeWidth":F
    :pswitch_0
    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    .line 55
    goto :goto_0

    .line 57
    :pswitch_1
    sget-object v2, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    .line 58
    goto :goto_0

    .line 60
    :pswitch_2
    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getRestoreColor()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mRestoreColor:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getCurrentTextColor()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mRestoreColor:I

    .line 82
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeColor:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 84
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 85
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeJoin:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 86
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeMiter:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeMiter(F)V

    .line 87
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeColor:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->setTextViewColor(I)V

    .line 88
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeWidth:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 89
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 90
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 91
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 92
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->mRestoreColor:I

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->setTextViewColor(I)V

    .line 93
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 95
    :cond_0
    return-void
.end method

.method public setStroke(FI)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "color"    # I

    .prologue
    .line 76
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->setStroke(FILandroid/graphics/Paint$Join;F)V

    .line 77
    return-void
.end method

.method public setStroke(FILandroid/graphics/Paint$Join;F)V
    .locals 1
    .param p1, "width"    # F
    .param p2, "color"    # I
    .param p3, "join"    # Landroid/graphics/Paint$Join;
    .param p4, "miter"    # F

    .prologue
    .line 69
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeWidth:F

    .line 70
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeColor:Ljava/lang/Integer;

    .line 71
    iput-object p3, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeJoin:Landroid/graphics/Paint$Join;

    .line 72
    iput p4, p0, Lcom/arcsoft/magicshotstudio/ui/main/StrokeTextView;->strokeMiter:F

    .line 73
    return-void
.end method

.method public setTextColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 117
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 0
    .param p1, "colors"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 120
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 121
    return-void
.end method

.method public setTextViewColor(I)V
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 104
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "mTextColor"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 106
    .local v1, "mTextColor":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 107
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 108
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v1    # "mTextColor":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
