.class Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;
.super Ljava/lang/Object;
.source "UndoRedoManager.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener",
        "<",
        "Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearDataFile()V
    .locals 3

    .prologue
    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUserDataDir:Ljava/io/File;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "drama_undo_data.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->doClearDataFile(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public loadDataFromFile()Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;
    .locals 5

    .prologue
    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUserDataDir:Ljava/io/File;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "drama_undo_data.dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "filePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mDataSize:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)I

    move-result v3

    # -= operator for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUndoDataFileLoadOffset:I
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$120(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;I)I

    .line 119
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mDataSize:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)I

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUndoDataFileLoadOffset:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$100(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)I

    move-result v4

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->doLoadDataFromFile(Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;
    invoke-static {v2, v0, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$400(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;Ljava/lang/String;II)Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;

    move-result-object v1

    .line 120
    .local v1, "result":Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;
    return-object v1
.end method

.method public bridge synthetic loadDataFromFile()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->loadDataFromFile()Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;

    move-result-object v0

    return-object v0
.end method

.method public saveDataToFile(Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;)V
    .locals 3
    .param p1, "data"    # Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;

    .prologue
    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUserDataDir:Ljava/io/File;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$000(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "drama_undo_data.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUndoDataFileLoadOffset:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$100(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)I

    move-result v2

    # invokes: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->doSaveDataToFile(Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;Ljava/lang/String;I)V
    invoke-static {v1, p1, v0, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$200(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;Ljava/lang/String;I)V

    .line 111
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mDataSize:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$300(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;)I

    move-result v2

    # += operator for: Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->mUndoDataFileLoadOffset:I
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;->access$112(Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager;I)I

    .line 112
    return-void
.end method

.method public bridge synthetic saveDataToFile(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 104
    check-cast p1, Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/UndoRedoManager$1;->saveDataToFile(Lcom/arcsoft/magicshotstudio/ui/drama/OperateData;)V

    return-void
.end method
