.class public Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;
.super Landroid/view/View;
.source "ArcImageView.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;
.implements Ljava/util/Observer;


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_ImageView"


# instance fields
.field private mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

.field private final mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mCirclePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

.field private mEdited:Z

.field private mEnableDrawFaceRect:Z

.field private mEnableSetRotate:Z

.field private mFaceRectsOnBitmap:[Landroid/graphics/RectF;

.field private mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

.field private mFaceRectsOnScreen:[Landroid/graphics/Rect;

.field private mFaceRotateDegree:I

.field private mFocusCircleColor:I

.field private mFocusIndex:I

.field private mFocusNinePatch:Landroid/graphics/NinePatch;

.field private mFocusPatchBitmap:Landroid/graphics/Bitmap;

.field private mIsFirstTime:Z

.field private mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

.field private mLastDirection:I

.field private mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mNormalCircleColor:I

.field private mNormalNinePatch:Landroid/graphics/NinePatch;

.field private mNormalPatchBitmap:Landroid/graphics/Bitmap;

.field private final mPaint:Landroid/graphics/Paint;

.field private mPortraitFitInBitmapRect:Landroid/graphics/Rect;

.field private mRadiusType:[I

.field private final mRectDst:Landroid/graphics/Rect;

.field private final mRectSrc:Landroid/graphics/Rect;

.field private mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

.field private m_OffsetX:I

.field private m_OffsetY:I

.field private m_ScaleX:F

.field private m_ScaleY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPaint:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    .line 37
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 38
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 39
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    .line 40
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusPatchBitmap:Landroid/graphics/Bitmap;

    .line 41
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalNinePatch:Landroid/graphics/NinePatch;

    .line 42
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusNinePatch:Landroid/graphics/NinePatch;

    .line 43
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 44
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableDrawFaceRect:Z

    .line 46
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetX:I

    .line 47
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetY:I

    .line 48
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    .line 49
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    .line 51
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    .line 52
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 54
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 55
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 57
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableSetRotate:Z

    .line 59
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .line 60
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 63
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalCircleColor:I

    .line 64
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusCircleColor:I

    .line 142
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    .line 310
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusIndex:I

    .line 370
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    .line 514
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 515
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 516
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mIsFirstTime:Z

    .line 552
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    .line 68
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 72
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPaint:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    .line 37
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 38
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 39
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    .line 40
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusPatchBitmap:Landroid/graphics/Bitmap;

    .line 41
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalNinePatch:Landroid/graphics/NinePatch;

    .line 42
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusNinePatch:Landroid/graphics/NinePatch;

    .line 43
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 44
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableDrawFaceRect:Z

    .line 46
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetX:I

    .line 47
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetY:I

    .line 48
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    .line 49
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    .line 51
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    .line 52
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 54
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 55
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 57
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableSetRotate:Z

    .line 59
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .line 60
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 63
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalCircleColor:I

    .line 64
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusCircleColor:I

    .line 142
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    .line 310
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusIndex:I

    .line 370
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    .line 514
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 515
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 516
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mIsFirstTime:Z

    .line 552
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    .line 73
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPaint:Landroid/graphics/Paint;

    .line 31
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    .line 37
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 38
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 39
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    .line 40
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusPatchBitmap:Landroid/graphics/Bitmap;

    .line 41
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalNinePatch:Landroid/graphics/NinePatch;

    .line 42
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusNinePatch:Landroid/graphics/NinePatch;

    .line 43
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 44
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableDrawFaceRect:Z

    .line 46
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetX:I

    .line 47
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetY:I

    .line 48
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    .line 49
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    .line 51
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    .line 52
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 54
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 55
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 57
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableSetRotate:Z

    .line 59
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .line 60
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 63
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalCircleColor:I

    .line 64
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusCircleColor:I

    .line 142
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    .line 310
    const/4 v0, -0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusIndex:I

    .line 370
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    .line 514
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 515
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 516
    iput-boolean v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mIsFirstTime:Z

    .line 552
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    .line 78
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    .line 79
    return-void
.end method

.method private computeRadius(Landroid/graphics/Rect;I)I
    .locals 9
    .param p1, "faceRect"    # Landroid/graphics/Rect;
    .param p2, "index"    # I

    .prologue
    const/4 v8, 0x1

    .line 372
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 373
    .local v3, "w":I
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    int-to-double v6, v3

    mul-double/2addr v4, v6

    double-to-int v0, v4

    .line 374
    .local v0, "diameter":I
    div-int/lit8 v2, v0, 0x2

    .line 375
    .local v2, "radius":I
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    if-eqz v4, :cond_3

    .line 376
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    const/4 v5, -0x1

    aput v5, v4, p2

    .line 377
    int-to-float v4, v0

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    div-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_1

    .line 378
    div-int/lit8 v2, v3, 0x2

    .line 379
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    aput v8, v4, p2

    .line 396
    :cond_0
    :goto_0
    return v2

    .line 381
    :cond_1
    sub-int v4, v0, v3

    div-int/lit8 v1, v4, 0x2

    .line 382
    .local v1, "offset":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v4, v4, p2

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v5, v1

    cmpg-float v4, v4, v5

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v4, v4, p2

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v5, v1

    cmpg-float v4, v4, v5

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v5, v5, p2

    iget v5, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v5, v1

    cmpg-float v4, v4, v5

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v5, v5, p2

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v5, v1

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 387
    :cond_2
    div-int/lit8 v2, v3, 0x2

    .line 388
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    aput v8, v4, p2

    goto :goto_0

    .line 392
    .end local v1    # "offset":I
    :cond_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    aget v4, v4, p2

    if-ne v4, v8, :cond_0

    .line 393
    div-int/lit8 v2, v3, 0x2

    goto :goto_0
.end method

.method private faceRectFromPhotoToScreen()V
    .locals 12

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    .line 212
    const/4 v3, 0x0

    .line 214
    .local v3, "length":I
    const/4 v8, 0x0

    .line 215
    .local v8, "tempTop":I
    const/4 v6, 0x0

    .line 216
    .local v6, "tempLeft":I
    const/4 v7, 0x0

    .line 217
    .local v7, "tempRight":I
    const/4 v5, 0x0

    .line 219
    .local v5, "tempBottom":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    if-eqz v9, :cond_0

    .line 220
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    array-length v3, v9

    .line 221
    if-gtz v3, :cond_1

    .line 308
    :cond_0
    return-void

    .line 228
    :cond_1
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    if-eqz v9, :cond_3

    iget-boolean v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableSetRotate:Z

    if-eqz v9, :cond_3

    .line 229
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 230
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget v8, v9, Landroid/graphics/Rect;->top:I

    .line 231
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget v6, v9, Landroid/graphics/Rect;->left:I

    .line 232
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget v7, v9, Landroid/graphics/Rect;->right:I

    .line 233
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget v5, v9, Landroid/graphics/Rect;->bottom:I

    .line 234
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    sparse-switch v9, :sswitch_data_0

    .line 229
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 236
    :sswitch_0
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iput v6, v9, Landroid/graphics/Rect;->top:I

    .line 237
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v10, v5

    iput v10, v9, Landroid/graphics/Rect;->left:I

    .line 238
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v10, v8

    iput v10, v9, Landroid/graphics/Rect;->right:I

    .line 239
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iput v7, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 242
    :sswitch_1
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v10, v5

    iput v10, v9, Landroid/graphics/Rect;->top:I

    .line 243
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v10, v7

    iput v10, v9, Landroid/graphics/Rect;->left:I

    .line 244
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v10, v6

    iput v10, v9, Landroid/graphics/Rect;->right:I

    .line 245
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    sub-int/2addr v10, v8

    iput v10, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 248
    :sswitch_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v10, v7

    iput v10, v9, Landroid/graphics/Rect;->top:I

    .line 249
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iput v8, v9, Landroid/graphics/Rect;->left:I

    .line 250
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iput v5, v9, Landroid/graphics/Rect;->right:I

    .line 251
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v10, v10, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    sub-int/2addr v10, v6

    iput v10, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 258
    :cond_2
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableSetRotate:Z

    .line 265
    .end local v2    # "index":I
    :cond_3
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_0

    .line 269
    const/high16 v4, 0x3f800000    # 1.0f

    .line 270
    .local v4, "ratio":F
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    mul-float v1, v9, v11

    .line 271
    .local v1, "bitmapWidth":F
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float v0, v9, v11

    .line 273
    .local v0, "bitmapHeight":F
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    if-eqz v9, :cond_4

    .line 274
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    sparse-switch v9, :sswitch_data_1

    .line 288
    :cond_4
    :goto_2
    const/4 v2, 0x0

    .restart local v2    # "index":I
    :goto_3
    if-ge v2, v3, :cond_5

    .line 289
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    div-float/2addr v10, v4

    iput v10, v9, Landroid/graphics/RectF;->top:F

    .line 290
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    div-float/2addr v10, v4

    iput v10, v9, Landroid/graphics/RectF;->left:F

    .line 291
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v10, v10

    div-float/2addr v10, v4

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 292
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v10, v10

    div-float/2addr v10, v4

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    .line 288
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 277
    .end local v2    # "index":I
    :sswitch_3
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v9, v9

    mul-float/2addr v9, v11

    div-float v4, v9, v0

    .line 278
    goto :goto_2

    .line 281
    :sswitch_4
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v9, v9, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v9, v9

    mul-float/2addr v9, v11

    div-float v4, v9, v1

    .line 282
    goto :goto_2

    .line 301
    .restart local v2    # "index":I
    :cond_5
    const/4 v2, 0x0

    :goto_4
    if-ge v2, v3, :cond_0

    .line 302
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetY:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    div-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, v9, Landroid/graphics/Rect;->top:I

    .line 303
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetX:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    div-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, v9, Landroid/graphics/Rect;->left:I

    .line 304
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetX:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    div-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, v9, Landroid/graphics/Rect;->right:I

    .line 305
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v9, v9, v2

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetY:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    div-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, v9, Landroid/graphics/Rect;->bottom:I

    .line 301
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 234
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch

    .line 274
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x5a -> :sswitch_3
        0xb4 -> :sswitch_4
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;
    .locals 10
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I
    .param p3, "bitmapWidth"    # I
    .param p4, "bitmapHeight"    # I

    .prologue
    .line 475
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->getRotateScreenAspectQuotient()F

    move-result v0

    .line 476
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v1

    .line 477
    .local v1, "panX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v2

    .line 478
    .local v2, "panY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v7

    int-to-float v8, p1

    mul-float/2addr v7, v8

    int-to-float v8, p3

    div-float v5, v7, v8

    .line 479
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v7

    int-to-float v8, p2

    mul-float/2addr v7, v8

    int-to-float v8, p4

    div-float v6, v7, v8

    .line 482
    .local v6, "zoomY":F
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 483
    .local v4, "rectSrc":Landroid/graphics/Rect;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 484
    .local v3, "rectDst":Landroid/graphics/Rect;
    int-to-float v7, p3

    mul-float/2addr v7, v1

    int-to-float v8, p1

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v5

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 485
    int-to-float v7, p4

    mul-float/2addr v7, v2

    int-to-float v8, p2

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v6

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->top:I

    .line 486
    iget v7, v4, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    int-to-float v8, p1

    div-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->right:I

    .line 487
    iget v7, v4, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    int-to-float v8, p2

    div-float/2addr v8, v6

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v4, Landroid/graphics/Rect;->bottom:I

    .line 488
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getLeft()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 489
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getTop()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 490
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getBottom()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 491
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getRight()I

    move-result v7

    iput v7, v3, Landroid/graphics/Rect;->bottom:I

    .line 494
    iget v7, v4, Landroid/graphics/Rect;->left:I

    if-gez v7, :cond_0

    .line 495
    iget v7, v3, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->left:I

    neg-int v8, v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->left:I

    .line 496
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/Rect;->left:I

    .line 498
    :cond_0
    iget v7, v4, Landroid/graphics/Rect;->right:I

    if-le v7, p3, :cond_1

    .line 499
    iget v7, v3, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v8, p3

    int-to-float v8, v8

    mul-float/2addr v8, v5

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->right:I

    .line 500
    iput p3, v4, Landroid/graphics/Rect;->right:I

    .line 502
    :cond_1
    iget v7, v4, Landroid/graphics/Rect;->top:I

    if-gez v7, :cond_2

    .line 503
    iget v7, v3, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->top:I

    neg-int v8, v8

    int-to-float v8, v8

    mul-float/2addr v8, v6

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->top:I

    .line 504
    const/4 v7, 0x0

    iput v7, v4, Landroid/graphics/Rect;->top:I

    .line 506
    :cond_2
    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    if-le v7, p4, :cond_3

    .line 507
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, p4

    int-to-float v8, v8

    mul-float/2addr v8, v6

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v3, Landroid/graphics/Rect;->bottom:I

    .line 508
    iput p4, v4, Landroid/graphics/Rect;->bottom:I

    .line 511
    :cond_3
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v7
.end method

.method private releaseOldEdgeEffect(I)V
    .locals 3
    .param p1, "currentDirection"    # I

    .prologue
    .line 555
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    xor-int v0, p1, v2

    .line 556
    .local v0, "changed":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    and-int v1, v0, v2

    .line 557
    .local v1, "needRelease":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    if-eqz v2, :cond_0

    .line 558
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->onRelease(I)V

    .line 560
    :cond_0
    return-void
.end method

.method private showBounceView(I)V
    .locals 3
    .param p1, "currentDirection"    # I

    .prologue
    const v2, 0x3ca3d70a    # 0.02f

    .line 563
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    if-nez v0, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_5

    .line 568
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->onPull(FI)V

    .line 573
    :cond_2
    :goto_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_6

    .line 574
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->onPull(FI)V

    .line 579
    :cond_3
    :goto_2
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_7

    .line 580
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->onPull(FI)V

    .line 585
    :cond_4
    :goto_3
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_8

    .line 586
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->onPull(FI)V

    goto :goto_0

    .line 570
    :cond_5
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    goto :goto_1

    .line 576
    :cond_6
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_3

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    goto :goto_2

    .line 582
    :cond_7
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_4

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    goto :goto_3

    .line 588
    :cond_8
    and-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private updateDrawRect()V
    .locals 15

    .prologue
    .line 407
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v11, :cond_4

    .line 408
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 410
    .local v0, "aspectQuotient":F
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getWidth()I

    move-result v8

    .line 411
    .local v8, "viewWidth":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getHeight()I

    move-result v7

    .line 412
    .local v7, "viewHeight":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 413
    .local v2, "bitmapWidth":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 415
    .local v1, "bitmapHeight":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v3

    .line 416
    .local v3, "panX":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v4

    .line 417
    .local v4, "panY":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v11, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v11

    int-to-float v12, v8

    mul-float/2addr v11, v12

    int-to-float v12, v2

    div-float v9, v11, v12

    .line 418
    .local v9, "zoomX":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v11, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v11

    int-to-float v12, v7

    mul-float/2addr v11, v12

    int-to-float v12, v1

    div-float v10, v11, v12

    .line 421
    .local v10, "zoomY":F
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v12, v2

    mul-float/2addr v12, v3

    int-to-float v13, v8

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v9

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 422
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v12, v1

    mul-float/2addr v12, v4

    int-to-float v13, v7

    const/high16 v14, 0x40000000    # 2.0f

    mul-float/2addr v14, v10

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 423
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    int-to-float v13, v8

    div-float/2addr v13, v9

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 424
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    int-to-float v13, v7

    div-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 425
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getLeft()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 426
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getTop()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 427
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getRight()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 428
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getBottom()I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 431
    int-to-float v11, v2

    mul-float/2addr v11, v3

    int-to-float v12, v8

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v9

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetX:I

    .line 432
    int-to-float v11, v1

    mul-float/2addr v11, v4

    int-to-float v12, v7

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v10

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_OffsetY:I

    .line 434
    int-to-float v11, v8

    div-float/2addr v11, v9

    float-to-int v6, v11

    .line 435
    .local v6, "srcWidth":I
    int-to-float v11, v7

    div-float/2addr v11, v10

    float-to-int v5, v11

    .line 438
    .local v5, "srcHeight":I
    int-to-float v11, v6

    int-to-float v12, v8

    div-float/2addr v11, v12

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleX:F

    .line 439
    int-to-float v11, v5

    int-to-float v12, v7

    div-float/2addr v11, v12

    iput v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->m_ScaleY:F

    .line 442
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    if-gez v11, :cond_0

    .line 443
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    neg-int v13, v13

    int-to-float v13, v13

    mul-float/2addr v13, v9

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 444
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    const/4 v12, 0x0

    iput v12, v11, Landroid/graphics/Rect;->left:I

    .line 446
    :cond_0
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    if-le v11, v2, :cond_1

    .line 447
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->right:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    sub-int/2addr v13, v2

    int-to-float v13, v13

    mul-float/2addr v13, v9

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 448
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 450
    :cond_1
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    if-gez v11, :cond_2

    .line 451
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    neg-int v13, v13

    int-to-float v13, v13

    mul-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 452
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    const/4 v12, 0x0

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 454
    :cond_2
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    if-le v11, v1, :cond_3

    .line 455
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->bottom:I

    int-to-float v12, v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v13, v1

    int-to-float v13, v13

    mul-float/2addr v13, v10

    sub-float/2addr v12, v13

    float-to-int v12, v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 456
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iput v1, v11, Landroid/graphics/Rect;->bottom:I

    .line 459
    :cond_3
    iget-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mIsFirstTime:Z

    if-eqz v11, :cond_4

    .line 460
    if-ge v8, v7, :cond_5

    .line 461
    new-instance v11, Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    .line 462
    invoke-direct {p0, v7, v8, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;

    move-result-object v11

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 467
    :goto_0
    const-string v11, "ArcSoft_BestFace_ImageView"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mPortraitFitInBitmapRect = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v11, "ArcSoft_BestFace_ImageView"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mLandscapeFitInBitmapRect = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mIsFirstTime:Z

    .line 472
    .end local v0    # "aspectQuotient":F
    .end local v1    # "bitmapHeight":I
    .end local v2    # "bitmapWidth":I
    .end local v3    # "panX":F
    .end local v4    # "panY":F
    .end local v5    # "srcHeight":I
    .end local v6    # "srcWidth":I
    .end local v7    # "viewHeight":I
    .end local v8    # "viewWidth":I
    .end local v9    # "zoomX":F
    .end local v10    # "zoomY":F
    :cond_4
    return-void

    .line 464
    .restart local v0    # "aspectQuotient":F
    .restart local v1    # "bitmapHeight":I
    .restart local v2    # "bitmapWidth":I
    .restart local v3    # "panX":F
    .restart local v4    # "panY":F
    .restart local v5    # "srcHeight":I
    .restart local v6    # "srcWidth":I
    .restart local v7    # "viewHeight":I
    .restart local v8    # "viewWidth":I
    .restart local v9    # "zoomX":F
    .restart local v10    # "zoomY":F
    :cond_5
    new-instance v11, Landroid/graphics/Rect;

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    .line 465
    invoke-direct {p0, v7, v8, v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getRotateScreenFitInBitmapRect(IIII)Landroid/graphics/Rect;

    move-result-object v11

    iput-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    goto :goto_0
.end method


# virtual methods
.method public bitmapRecycle(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 189
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 191
    const/4 p1, 0x0

    .line 193
    :cond_0
    return-void
.end method

.method public getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    return-object v0
.end method

.method public getLandscapeFitInBitmapRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLandscapeFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPortraitFitInBitmapRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPortraitFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRectChoosed(FF)I
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 318
    const/4 v1, -0x1

    .line 319
    .local v1, "index":I
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    if-eqz v2, :cond_0

    .line 320
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 321
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 324
    move v1, v0

    .line 329
    .end local v0    # "i":I
    :cond_0
    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusIndex:I

    .line 330
    return v1

    .line 320
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public init(Landroid/content/Context;IILcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "normalResID"    # I
    .param p3, "focusResID"    # I
    .param p4, "uiLogic"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    const/4 v4, 0x0

    .line 86
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    .line 87
    invoke-static {p1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    .line 88
    new-instance v1, Landroid/graphics/NinePatch;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalNinePatch:Landroid/graphics/NinePatch;

    .line 91
    invoke-static {p1, p3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusPatchBitmap:Landroid/graphics/Bitmap;

    .line 92
    new-instance v1, Landroid/graphics/NinePatch;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusPatchBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusPatchBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusNinePatch:Landroid/graphics/NinePatch;

    .line 94
    iput-object p4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .line 96
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 97
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 98
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050091

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 99
    .local v0, "width":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 100
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalCircleColor:I

    .line 101
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusCircleColor:I

    .line 102
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 335
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v6, :cond_3

    .line 336
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->updateDrawRect()V

    .line 337
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectSrc:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 338
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableDrawFaceRect:Z

    if-eqz v6, :cond_3

    .line 339
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->faceRectFromPhotoToScreen()V

    .line 340
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalNinePatch:Landroid/graphics/NinePatch;

    if-eqz v6, :cond_3

    .line 341
    const/4 v3, 0x0

    .line 342
    .local v3, "length":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    if-eqz v6, :cond_0

    .line 343
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    array-length v3, v6

    .line 346
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 347
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v6, v6, v2

    invoke-direct {p0, v6, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->computeRadius(Landroid/graphics/Rect;I)I

    move-result v4

    .line 348
    .local v4, "radius":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 349
    .local v5, "w":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/Rect;->left:I

    div-int/lit8 v7, v5, 0x2

    add-int v0, v6, v7

    .line 350
    .local v0, "cx":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/Rect;->top:I

    div-int/lit8 v7, v5, 0x2

    add-int v1, v6, v7

    .line 351
    .local v1, "cy":I
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusIndex:I

    if-ne v6, v2, :cond_1

    .line 352
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusCircleColor:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 356
    :goto_1
    int-to-float v6, v0

    int-to-float v7, v1

    int-to-float v8, v4

    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 346
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 354
    :cond_1
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mCirclePaint:Landroid/graphics/Paint;

    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalCircleColor:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 358
    .end local v0    # "cx":I
    .end local v1    # "cy":I
    .end local v4    # "radius":I
    .end local v5    # "w":I
    :cond_2
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    if-eqz v6, :cond_3

    .line 359
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    .line 364
    .end local v2    # "i":I
    .end local v3    # "length":I
    :cond_3
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 529
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 530
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    sub-int v1, p4, p2

    int-to-float v1, v1

    sub-int v2, p5, p3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->updateAspectQuotient(FFFF)V

    .line 533
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->notifyObservers()V

    .line 535
    :cond_0
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 168
    if-eqz p1, :cond_0

    .line 169
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    if-eq v0, p1, :cond_0

    .line 170
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->bitmapRecycle(Landroid/graphics/Bitmap;)V

    .line 171
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    .line 172
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->updateAspectQuotient(FFFF)V

    .line 174
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->notifyObservers()V

    .line 175
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->invalidate()V

    .line 178
    :cond_0
    return-void
.end method

.method public setDrawFaceRect(Z)V
    .locals 0
    .param p1, "isShow"    # Z

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableDrawFaceRect:Z

    .line 158
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->invalidate()V

    .line 159
    return-void
.end method

.method public setEdgeBound()V
    .locals 5

    .prologue
    .line 400
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->updateDrawRect()V

    .line 401
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRectDst:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;->setEdgeBound(IIII)V

    .line 404
    :cond_0
    return-void
.end method

.method public setEdgeView(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;)V
    .locals 0
    .param p1, "edgeView"    # Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .line 83
    return-void
.end method

.method public setEdited()V
    .locals 3

    .prologue
    .line 144
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    if-eqz v1, :cond_1

    .line 145
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    .line 150
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public setFaceRect([Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "rects"    # [Landroid/graphics/Rect;

    .prologue
    const/4 v4, 0x1

    .line 122
    if-eqz p1, :cond_3

    .line 123
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    .line 124
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnOriginalPhotos:[Landroid/graphics/Rect;

    array-length v1, v2

    .line 125
    .local v1, "length":I
    new-array v2, v1, [Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    .line 126
    new-array v2, v1, [Landroid/graphics/RectF;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    .line 127
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 128
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnScreen:[Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, v2, v0

    .line 129
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRectsOnBitmap:[Landroid/graphics/RectF;

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    aput-object v3, v2, v0

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    if-nez v2, :cond_2

    .line 132
    new-array v2, v1, [I

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    .line 133
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 134
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mRadiusType:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 136
    :cond_1
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEdited:Z

    .line 138
    :cond_2
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mEnableSetRotate:Z

    .line 140
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_3
    return-void
.end method

.method public setFaceRectRotateDegree(I)V
    .locals 0
    .param p1, "degree"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFaceRotateDegree:I

    .line 154
    return-void
.end method

.method public setFocusIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 313
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mFocusIndex:I

    .line 314
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->invalidate()V

    .line 315
    return-void
.end method

.method public setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 0
    .param p1, "size"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 162
    if-eqz p1, :cond_0

    .line 163
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 165
    :cond_0
    return-void
.end method

.method public setZoomState(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;)V
    .locals 1
    .param p1, "state"    # Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 200
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 201
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->addObserver(Ljava/util/Observer;)V

    .line 203
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->invalidate()V

    .line 204
    return-void
.end method

.method public unInit()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->bitmapRecycle(Landroid/graphics/Bitmap;)V

    .line 182
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mNormalPatchBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->bitmapRecycle(Landroid/graphics/Bitmap;)V

    .line 183
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->deleteObserver(Ljava/util/Observer;)V

    .line 186
    :cond_0
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 539
    const/4 v0, 0x0

    .line 540
    .local v0, "direction":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    if-eqz v1, :cond_0

    .line 541
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getEffectDirection()I

    move-result v0

    .line 544
    :cond_0
    const-string v1, "ArcSoft_BestFace_ImageView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update direction = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setEdgeBound()V

    .line 546
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->releaseOldEdgeEffect(I)V

    .line 547
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->showBounceView(I)V

    .line 548
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->mLastDirection:I

    .line 549
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->invalidate()V

    .line 550
    return-void
.end method
