.class Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;
.super Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$WindowRunnnable;
.source "FrameListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PerformClick"
.end annotation


# instance fields
.field mClickMotionPosition:I

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V
    .locals 1

    .prologue
    .line 6444
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$WindowRunnnable;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$1;

    .prologue
    .line 6444
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;-><init>(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 6449
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mDataChanged:Z
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$500(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 6465
    :cond_0
    :goto_0
    return-void

    .line 6453
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$1400(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6454
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->mClickMotionPosition:I

    .line 6456
    .local v2, "motionPosition":I
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mItemCount:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$1800(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6460
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->mFirstPosition:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->access$3300(Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;)I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 6461
    .local v1, "child":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 6462
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView$PerformClick;->this$0:Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/arcsoft/magicshotstudio/ui/drama/FrameListView;->performItemClick(Landroid/view/View;IJ)Z

    goto :goto_0
.end method
