.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
.super Landroid/widget/RelativeLayout;
.source "PhotoViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;
    }
.end annotation


# instance fields
.field private mBitmapDrawHeight:I

.field private mBitmapDrawWidth:I

.field private mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

.field private mFitInBitmapRect:Landroid/graphics/Rect;

.field private mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

.field private mMaxSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mOffsetPoint:Landroid/graphics/Point;

.field private mOutPoint:Landroid/graphics/Point;

.field private mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

.field private mRectDst:Landroid/graphics/Rect;

.field private mRectSrc:Landroid/graphics/Rect;

.field private mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

.field private mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

.field private mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

.field private mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    .line 24
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    .line 27
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    .line 28
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    const/16 v1, 0x780

    const/16 v2, 0x438

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;

    .line 31
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;

    .line 34
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {v0, v3, v3}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mMaxSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 35
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOffsetPoint:Landroid/graphics/Point;

    .line 36
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v3, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;

    .line 38
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I

    .line 39
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I

    .line 41
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 98
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOutPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Lcom/arcsoft/magicshotstudio/utils/MSize;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mMaxSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mOffsetPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$702(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mFitInBitmapRect:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I

    return v0
.end method

.method static synthetic access$802(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawWidth:I

    return p1
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;

    .prologue
    .line 21
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I

    return v0
.end method

.method static synthetic access$902(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mBitmapDrawHeight:I

    return p1
.end method


# virtual methods
.method public getOffsetX()I
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 238
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    neg-float v0, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getOffsetY()I
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 247
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    neg-float v0, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public initZoomView(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const v0, 0x7f09003c

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    .line 66
    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    .line 67
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .line 68
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    .line 69
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->setZoomControl(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)V

    .line 71
    instance-of v0, p1, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    check-cast p1, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->setMyTapUpCallback(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->setZoomState(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;)V

    .line 77
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->setEdgeView(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;)V

    .line 78
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->setAspectQuotient(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;)V

    .line 79
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->resetZoomState()V

    .line 80
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->setZoomStateListener(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;)V

    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 83
    return-void
.end method

.method public isArriveLeftEdge()Z
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->isLeftEdge()Z

    move-result v0

    return v0
.end method

.method public isArriveRightEdge()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->isRightEdge()Z

    move-result v0

    return v0
.end method

.method public isImageDisplayCompletely()Z
    .locals 2

    .prologue
    .line 273
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->getEdgeBound()Landroid/graphics/Rect;

    move-result-object v0

    .line 274
    .local v0, "rect":Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->left:I

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isZoomed()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->isZoomed()Z

    move-result v0

    return v0
.end method

.method public resetZoomState()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 86
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 87
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 88
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    .line 89
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    .line 90
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "src"    # Landroid/graphics/Bitmap;

    .prologue
    .line 93
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 94
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    .line 95
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mImageView:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ImageZoomView;->setImage(Landroid/graphics/Bitmap;)V

    .line 96
    return-void
.end method

.method public setTapUpCallback(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;)V
    .locals 1
    .param p1, "mCallback"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener;->setMyTapUpCallback(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;)V

    .line 257
    :cond_0
    return-void
.end method

.method public setZoomStateCallback(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout;->mZoomStateCallback:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/PhotoViewLayout$ZoomStateCallback;

    .line 50
    return-void
.end method
