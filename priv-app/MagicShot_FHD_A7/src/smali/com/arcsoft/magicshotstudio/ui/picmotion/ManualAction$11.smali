.class Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;
.super Ljava/lang/Thread;
.source "ManualAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResultFile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)V
    .locals 0

    .prologue
    .line 1885
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1891
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/PicMotion;->getSavePath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    .line 1892
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->createFile(Ljava/lang/String;)V

    .line 1893
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V

    .line 1894
    const/4 v1, 0x0

    .line 1895
    .local v1, "saveSucc":Z
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$500(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->saveFile(Ljava/lang/String;)I

    move-result v0

    .line 1896
    .local v0, "res":I
    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 1897
    :goto_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->tag:Ljava/lang/String;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Drama.SaveResult saveSucc: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1899
    if-eqz v1, :cond_0

    .line 1900
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mActivity:Lcom/arcsoft/magicshotstudio/PicMotion;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$1800(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Lcom/arcsoft/magicshotstudio/PicMotion;

    move-result-object v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSavePath:Ljava/lang/String;

    const/16 v5, 0x835

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget v6, v6, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mOrientation:I

    invoke-static {v3, v4, v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->refreshDBRecords(Landroid/content/Context;Ljava/lang/String;II)Z

    .line 1902
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iput-boolean v2, v3, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mFileSaving:Z

    .line 1903
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction$11;->this$0:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->mSaveLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->access$5000(Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1904
    return-void

    :cond_1
    move v1, v2

    .line 1896
    goto :goto_0
.end method
