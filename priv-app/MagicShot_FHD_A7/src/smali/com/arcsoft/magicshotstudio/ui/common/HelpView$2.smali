.class Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;
.super Landroid/os/Handler;
.source "HelpView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 222
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 223
    iget v2, p1, Landroid/os/Message;->what:I

    .line 224
    .local v2, "lineCount":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$200(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f05008b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 225
    .local v3, "lineHeight":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$000(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/widget/TextView;

    move-result-object v4

    mul-int v5, v2, v3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setHeight(I)V

    .line 228
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$200(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 229
    .local v0, "configuration":Landroid/content/res/Configuration;
    const/4 v4, 0x2

    iget v5, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v5, :cond_0

    const/4 v1, 0x1

    .line 230
    .local v1, "isLandscape":Z
    :goto_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$000(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # invokes: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->getMaxLines(Z)I
    invoke-static {v5, v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$300(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;Z)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 232
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$400(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/app/Dialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    .line 233
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mHelpWindow:Landroid/app/Dialog;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$400(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/app/Dialog;

    move-result-object v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 234
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/common/HelpView$2;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->mOkButton:Landroid/view/View;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->access$500(Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 235
    return-void

    .line 229
    .end local v1    # "isLandscape":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
