.class public Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
.super Ljava/lang/Object;
.source "ArcPicBestUILogic.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
.implements Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# static fields
.field private static final CANDIDATE_FACE_WIDTH:I = 0x73

.field private static final HANDLER_FINISH:I = 0x2

.field private static final HANDLER_MESSAGE_BASE:I = 0x1

.field private static final NOFACE_SAVE_BUTTON:I = 0x3

.field private static RANDOM_DATA:[I = null

.field private static final STATE_BASIC_BASE:I = 0x1

.field private static final STATE_LARGE_PHOTO:I = 0x1

.field private static final STATE_NO_PERSON:I = 0x4

.field private static final STATE_PIC_CONFIRM:I = 0x3

.field private static final STATE_PIC_EDIT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_ArcPicBestUILogic"

.field private static final THUMB_WIDTH:I = 0xe6

.field private static final TextView_HEIGHT:I = 0x2f

.field private static final TextView_PADDING_LEFT:I = 0x18

.field private static final TextView_PADDING_RIGHT:I = 0x18


# instance fields
.field private bitmapDrawHeight:I

.field private bitmapDrawWidth:I

.field field:Ljava/lang/reflect/Field;

.field hideMethod:Ljava/lang/reflect/Method;

.field private mActionBarBackLayout:Landroid/widget/RelativeLayout;

.field private mActionBarCancelGap_1:Landroid/view/View;

.field private mActionBarCancelGap_2:Landroid/view/View;

.field private mActionBarCancelGap_3:Landroid/view/View;

.field private mActionBarCancelLayout:Landroid/widget/RelativeLayout;

.field private mActionBarGap_1:Landroid/view/View;

.field private mActionBarGap_2:Landroid/view/View;

.field private mActionBarGap_3:Landroid/view/View;

.field private mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

.field private mActionBarSaveLayout:Landroid/widget/RelativeLayout;

.field private mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

.field private mBlackLayerLinearLayout:Landroid/widget/LinearLayout;

.field private mButtonTouchListener:Landroid/view/View$OnTouchListener;

.field private mCameraDegress:I

.field private mCancelImageView:Landroid/widget/ImageView;

.field private mCancelTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mCandidateFaceNum:[I

.field private mContentView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mCurEvent:Landroid/view/MotionEvent;

.field private mCurTouchedId:I

.field private mCurrentCandidate:I

.field private mCurrentPerson:I

.field private mCurrentState:I

.field private mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

.field private mFaceRectArray:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mFitInBitmapRect:Landroid/graphics/Rect;

.field private mHandler:Landroid/os/Handler;

.field private mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

.field private mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

.field private mInputNum:I

.field private mIntent:Landroid/content/Intent;

.field private mIsActivityFinished:Z

.field private mIsForQA:Z

.field private mIsLandScape:Z

.field private mIsTouchLocked:Z

.field private mKeepSrcFiles:Z

.field private mNoFaceDialog:Landroid/app/Dialog;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

.field mOnHoverListener:Landroid/view/View$OnHoverListener;

.field mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private volatile mOperateEnable:Z

.field private mPersonChoose:I

.field private mPersonNum:I

.field private mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

.field private mPhotos:[Ljava/lang/String;

.field private mPicBestLayout:Landroid/widget/LinearLayout;

.field private mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

.field private mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

.field private mProcessingFinish:Z

.field private mProgressNum:I

.field private mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

.field private mRectDst:Landroid/graphics/Rect;

.field private mRectSrc:Landroid/graphics/Rect;

.field private mReferenceImageId:I

.field private mReplaceHistoryArray:[I

.field private mResFileName:Ljava/lang/String;

.field private mResHeight:I

.field private mResWidth:I

.field private mSActionBarBackImageView:Landroid/widget/ImageView;

.field private mSaveImageView:Landroid/widget/ImageView;

.field private mSaveLock:Ljava/util/concurrent/Semaphore;

.field private mSaveTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

.field private mSaveThread:Ljava/lang/Thread;

.field private mSaveYuvOpen:Z

.field private mScreenDensity:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

.field private mSelectPhotoNum:I

.field mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

.field private mSrcHeight:I

.field private mSrcMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mSrcWidth:I

.field private mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

.field private mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

.field private mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

.field private mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

.field mbIsSaveBtnOn:Z

.field obj:Ljava/lang/Object;

.field showMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->RANDOM_DATA:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xa
        0x1e
        0x32
        0x46
        0x55
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIntent:Landroid/content/Intent;

    .line 82
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    .line 83
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotos:[Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    .line 86
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    .line 87
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveYuvOpen:Z

    .line 89
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    .line 91
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCameraDegress:I

    .line 92
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenWidth:I

    .line 93
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenHeight:I

    .line 94
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSelectPhotoNum:I

    .line 95
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mInputNum:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenDensity:F

    .line 98
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    .line 100
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    .line 101
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .line 102
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 103
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .line 104
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .line 105
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    .line 107
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 109
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 110
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 112
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcWidth:I

    .line 113
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcHeight:I

    .line 114
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 115
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReferenceImageId:I

    .line 116
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResWidth:I

    .line 117
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResHeight:I

    .line 118
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 120
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    .line 121
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    .line 122
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    .line 123
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 124
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 125
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarGap_1:Landroid/view/View;

    .line 126
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarGap_2:Landroid/view/View;

    .line 127
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarGap_3:Landroid/view/View;

    .line 128
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveImageView:Landroid/widget/ImageView;

    .line 129
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSActionBarBackImageView:Landroid/widget/ImageView;

    .line 131
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    .line 132
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCancelTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 133
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelGap_1:Landroid/view/View;

    .line 134
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelGap_2:Landroid/view/View;

    .line 135
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelGap_3:Landroid/view/View;

    .line 136
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCancelImageView:Landroid/widget/ImageView;

    .line 143
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 144
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 147
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressNum:I

    .line 149
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 212
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->field:Ljava/lang/reflect/Field;

    .line 213
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->obj:Ljava/lang/Object;

    .line 214
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showMethod:Ljava/lang/reflect/Method;

    .line 215
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideMethod:Ljava/lang/reflect/Method;

    .line 363
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 411
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$2;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnHoverListener:Landroid/view/View$OnHoverListener;

    .line 677
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    .line 678
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsActivityFinished:Z

    .line 757
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .line 758
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonNum:I

    .line 759
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    .line 760
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    .line 761
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    .line 866
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    .line 874
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicBestLayout:Landroid/widget/LinearLayout;

    .line 875
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    .line 897
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mBlackLayerLinearLayout:Landroid/widget/LinearLayout;

    .line 993
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    .line 1068
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$7;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHandler:Landroid/os/Handler;

    .line 1091
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOperateEnable:Z

    .line 1139
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mKeepSrcFiles:Z

    .line 1253
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 1255
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonChoose:I

    .line 1256
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentCandidate:I

    .line 1379
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .line 1390
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$8;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$8;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1480
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    .line 1481
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    .line 1482
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    .line 1518
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawWidth:I

    .line 1519
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawHeight:I

    .line 1602
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProcessingFinish:Z

    .line 1610
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsTouchLocked:Z

    .line 1611
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I

    .line 1612
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$9;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    .line 1634
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$10;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$10;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 153
    return-void
.end method

.method private abnormalToastShow(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 553
    invoke-static {}, Lcom/arcsoft/magicshotstudio/utils/ToastController;->closeToast()V

    .line 555
    const-string v0, "ArcSoft_BestFace_ArcPicBestUILogic"

    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    .line 557
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCameraDegress:I

    return v0
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOperateEnable:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsTouchLocked:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsTouchLocked:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I

    return v0
.end method

.method static synthetic access$1602(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I

    return p1
.end method

.method static synthetic access$1702(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->popUpToast(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->dismissAllDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    return v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    return-object v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    .prologue
    .line 67
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mInputNum:I

    return v0
.end method

.method private dismissAllDialog()V
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 748
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 750
    :cond_1
    return-void
.end method

.method private hideBars()V
    .locals 2

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1319
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1320
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1321
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1324
    :cond_0
    return-void
.end method

.method private hideProgressDialog()V
    .locals 4

    .prologue
    .line 648
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 651
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSharedPref:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v1, :cond_1

    .line 652
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "TYPE_BEST_FACE"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 654
    .local v0, "isFirstIn":Z
    if-eqz v0, :cond_2

    .line 655
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "TYPE_BEST_FACE"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 656
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 657
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 658
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->show(I)V

    .line 667
    .end local v0    # "isFirstIn":Z
    :cond_1
    :goto_0
    return-void

    .line 660
    .restart local v0    # "isFirstIn":Z
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v1, :cond_1

    .line 661
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    goto :goto_0
.end method

.method private inflateHorizontalProgressingView()V
    .locals 3

    .prologue
    .line 514
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-nez v0, :cond_0

    .line 515
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    const v2, 0x7f060015

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressNum:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 519
    return-void
.end method

.method private initAnimation()V
    .locals 9

    .prologue
    .line 497
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 500
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 502
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, -0x40800000    # -1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 505
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 506
    return-void
.end method

.method private initDispalyUI()V
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 230
    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    .line 231
    .local v8, "dm":Landroid/util/DisplayMetrics;
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 233
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenDensity:F

    .line 234
    iget v0, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenWidth:I

    .line 235
    iget v0, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenHeight:I

    .line 236
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenWidth:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenHeight:I

    if-le v0, v1, :cond_5

    .line 237
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    .line 246
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    .line 248
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    .line 250
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    .line 252
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;-><init>(Landroid/content/Context;Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener$MySingleDoubleClickListener;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    .line 253
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->setZoomControl(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;)V

    .line 255
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    const v2, 0x7f020018

    const v3, 0x7f020019

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->init(Landroid/content/Context;IILcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    .line 257
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setZoomState(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;)V

    .line 258
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    .line 259
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomController;->setAspectQuotient(Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;)V

    .line 260
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setDrawFaceRect(Z)V

    .line 261
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setEdgeView(Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/EdgeView;)V

    .line 262
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCameraDegress:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFaceRectRotateDegree(I)V

    .line 263
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    .line 264
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 267
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->initAnimation()V

    .line 268
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->inflateHorizontalProgressingView()V

    .line 270
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const v1, 0x7f090026

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    .line 276
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    .line 278
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 279
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 280
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarGap_1:Landroid/view/View;

    .line 281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090014

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarGap_2:Landroid/view/View;

    .line 282
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090016

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarGap_3:Landroid/view/View;

    .line 283
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090013

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveImageView:Landroid/widget/ImageView;

    .line 286
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    .line 288
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 289
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCancelTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    .line 290
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090018

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelGap_1:Landroid/view/View;

    .line 291
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelGap_2:Landroid/view/View;

    .line 292
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09001c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelGap_3:Landroid/view/View;

    .line 293
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCancelImageView:Landroid/widget/ImageView;

    .line 295
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    .line 297
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 298
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-nez v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSActionBarBackImageView:Landroid/widget/ImageView;

    .line 306
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 307
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 312
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 317
    :cond_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 318
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 326
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const v1, 0x7f090028

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicBestLayout:Landroid/widget/LinearLayout;

    .line 329
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const v1, 0x7f090027

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    .line 332
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCameraDegress:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicBestLayout:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSelectPhotoNum:I

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->init(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;Landroid/content/Context;ILandroid/widget/LinearLayout;I)V

    .line 334
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    const v1, 0x7f090025

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/BestFace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mBlackLayerLinearLayout:Landroid/widget/LinearLayout;

    .line 337
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getExitDialog()Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    .line 338
    new-instance v7, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-direct {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V

    .line 339
    .local v7, "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v0, v7}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setBtnOnClick(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 342
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f09000e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 344
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v10

    .line 345
    .local v10, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 346
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSActionBarBackImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 359
    :goto_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setActionBarLongClickable(Z)V

    .line 360
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->updateGapValue(Z)V

    .line 361
    return-void

    .line 239
    .end local v7    # "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    .end local v10    # "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    :cond_5
    iput-boolean v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    goto/16 :goto_0

    .line 348
    .restart local v7    # "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    .restart local v10    # "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    :cond_6
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSActionBarBackImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 351
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarBackLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 352
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 353
    .local v9, "param":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v6, v0, Landroid/util/DisplayMetrics;->density:F

    .line 354
    .local v6, "density":F
    const/high16 v0, 0x41600000    # 14.0f

    mul-float/2addr v0, v6

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 355
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v0, v9}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private popUpToast(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1405
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    .line 1407
    move-object v0, p1

    .line 1408
    .local v0, "pressedView":Landroid/view/View;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v1

    .line 1409
    .local v1, "screenWith":I
    const/4 v2, 0x0

    .line 1411
    .local v2, "textRes":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1427
    :goto_0
    :pswitch_0
    if-eqz v2, :cond_0

    .line 1428
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v3, v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showPopUpToast(Landroid/view/View;II)V

    .line 1430
    :cond_0
    const/4 v3, 0x1

    return v3

    .line 1414
    :pswitch_1
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    if-eqz v3, :cond_1

    const v2, 0x7f06000c

    .line 1415
    :goto_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveImageView:Landroid/widget/ImageView;

    .line 1416
    goto :goto_0

    .line 1414
    :cond_1
    const v2, 0x7f060018

    goto :goto_1

    .line 1418
    :pswitch_2
    const v2, 0x7f06000e

    .line 1419
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCancelImageView:Landroid/widget/ImageView;

    .line 1420
    goto :goto_0

    .line 1422
    :pswitch_3
    const v2, 0x7f060027

    .line 1423
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSActionBarBackImageView:Landroid/widget/ImageView;

    goto :goto_0

    .line 1411
    :pswitch_data_0
    .packed-switch 0x7f09000c
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private setActionBarLongClickable(Z)V
    .locals 2
    .param p1, "isLandScape"    # Z

    .prologue
    .line 402
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarCancelLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 409
    :cond_1
    return-void
.end method

.method private setCurrentPerson(I)V
    .locals 1
    .param p1, "person"    # I

    .prologue
    .line 868
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonNum:I

    if-le p1, v0, :cond_1

    .line 872
    :cond_0
    :goto_0
    return-void

    .line 871
    :cond_1
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    goto :goto_0
.end method

.method private showBars()V
    .locals 2

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1310
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1311
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1312
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1315
    :cond_0
    return-void
.end method

.method private showBarsWithoutAnimation()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 675
    :cond_0
    return-void
.end method

.method private showNoFaceDialog()V
    .locals 3

    .prologue
    .line 690
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsActivityFinished:Z

    if-eqz v1, :cond_1

    .line 742
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 694
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 696
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f06001b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 697
    const v1, 0x7f060029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 698
    const v1, 0x7f060014

    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$3;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 720
    const v1, 0x7f06000e

    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$4;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$4;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 740
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mNoFaceDialog:Landroid/app/Dialog;

    goto :goto_0
.end method

.method private showProgressDialog()V
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    .line 645
    :cond_0
    return-void
.end method

.method private switchActionBar()V
    .locals 1

    .prologue
    .line 1299
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1300
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1301
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideBars()V

    .line 1306
    :cond_0
    :goto_0
    return-void

    .line 1303
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBars()V

    goto :goto_0
.end method

.method private updateGapValue(Z)V
    .locals 1
    .param p1, "isLandScape"    # Z

    .prologue
    .line 423
    const/4 v0, 0x0

    .line 424
    .local v0, "param":Landroid/view/ViewGroup$LayoutParams;
    if-eqz p1, :cond_0

    .line 494
    :cond_0
    return-void
.end method

.method private visibleSwitch(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 900
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mBlackLayerLinearLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    if-eqz v0, :cond_0

    .line 901
    if-eqz p1, :cond_1

    .line 902
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mBlackLayerLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 903
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->setVisibility(I)V

    .line 909
    :cond_0
    :goto_0
    return-void

    .line 905
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mBlackLayerLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 906
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public changeSaveBtnState(Z)V
    .locals 5
    .param p1, "isSaveState"    # Z

    .prologue
    const v3, 0x7f060018

    const v2, 0x7f06000c

    .line 1241
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    if-eqz v1, :cond_0

    .line 1242
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const v1, 0x7f02000f

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1243
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveTextView:Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;

    if-eqz p1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/arcsoft/magicshotstudio/ui/common/OutlineTextView;->setText(I)V

    .line 1245
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 1246
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1248
    .local v0, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1250
    .end local v0    # "description":Ljava/lang/String;
    :cond_1
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    .line 1251
    return-void

    .line 1242
    :cond_2
    const v1, 0x7f020013

    goto :goto_0

    :cond_3
    move v1, v3

    .line 1243
    goto :goto_1

    :cond_4
    move v2, v3

    .line 1246
    goto :goto_2
.end method

.method public closeToast()V
    .locals 4

    .prologue
    .line 1147
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->obj:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 1159
    :cond_0
    :goto_0
    return-void

    .line 1154
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->obj:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1155
    :catch_0
    move-exception v0

    .line 1157
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public dealWithAnotherSelected(I)V
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 616
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProcessingFinish:Z

    .line 618
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 620
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    aput p1, v0, v1

    .line 628
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_2

    .line 629
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentCandidate:I

    invoke-virtual {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->StartMerge(II)V

    .line 631
    :cond_2
    return-void
.end method

.method public dealWithTheSameSelected()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 592
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    invoke-virtual {v0, v1, v3, v4}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 597
    :cond_0
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->visibleSwitch(Z)V

    .line 598
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBars()V

    .line 599
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFocusIndex(I)V

    .line 600
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 603
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    invoke-virtual {v0, v1, v2, v4}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 607
    :cond_1
    return-void
.end method

.method public getCurrentCandidate()I
    .locals 1

    .prologue
    .line 1259
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentCandidate:I

    return v0
.end method

.method public getIsActivityFinished()Z
    .locals 1

    .prologue
    .line 686
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsActivityFinished:Z

    return v0
.end method

.method public getMaxHeight()I
    .locals 3

    .prologue
    .line 1507
    const/4 v0, 0x0

    .line 1508
    .local v0, "temp":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 1509
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40200000    # 2.5f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 1511
    :cond_0
    return v0
.end method

.method public getMaxWidth()I
    .locals 3

    .prologue
    .line 1499
    const/4 v0, 0x0

    .line 1500
    .local v0, "temp":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 1501
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40200000    # 2.5f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 1503
    :cond_0
    return v0
.end method

.method public getOffsetX()I
    .locals 3

    .prologue
    .line 1563
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_0

    .line 1564
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1566
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    neg-float v0, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getOffsetY()I
    .locals 3

    .prologue
    .line 1572
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_0

    .line 1573
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 1575
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    neg-float v0, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getOperateEnable()Z
    .locals 1

    .prologue
    .line 1099
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOperateEnable:Z

    return v0
.end method

.method public getProcessFinish()Z
    .locals 1

    .prologue
    .line 1606
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProcessingFinish:Z

    return v0
.end method

.method public getRectDst()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1515
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getZoomOutPoint()Landroid/graphics/Point;
    .locals 12

    .prologue
    .line 1521
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 1522
    .local v4, "point":Landroid/graphics/Point;
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResWidth:I

    .line 1523
    .local v1, "bitmapWidth":I
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResHeight:I

    .line 1524
    .local v0, "bitmapHeight":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getWidth()I

    move-result v6

    .line 1525
    .local v6, "viewWidth":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getHeight()I

    move-result v5

    .line 1526
    .local v5, "viewHeight":I
    if-le v6, v5, :cond_0

    .line 1527
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getLandscapeFitInBitmapRect()Landroid/graphics/Rect;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    .line 1532
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->updateZoomState()V

    .line 1534
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    mul-int/2addr v9, v1

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    div-int/2addr v9, v10

    iput v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawWidth:I

    .line 1535
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int/2addr v9, v0

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    div-int/2addr v9, v10

    iput v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawHeight:I

    .line 1538
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOffsetX()I

    move-result v9

    neg-int v2, v9

    .line 1539
    .local v2, "offset_x":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOffsetY()I

    move-result v9

    neg-int v3, v9

    .line 1541
    .local v3, "offset_y":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v2

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawWidth:I

    sub-int/2addr v10, v11

    mul-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawWidth:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-int v7, v9

    .line 1545
    .local v7, "x":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v10, v3

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawHeight:I

    sub-int/2addr v10, v11

    mul-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawHeight:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-int v8, v9

    .line 1549
    .local v8, "y":I
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawWidth:I

    if-gt v9, v6, :cond_1

    .line 1550
    div-int/lit8 v9, v6, 0x2

    iput v9, v4, Landroid/graphics/Point;->x:I

    .line 1554
    :goto_1
    iget v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->bitmapDrawHeight:I

    if-gt v9, v5, :cond_2

    .line 1555
    div-int/lit8 v9, v5, 0x2

    iput v9, v4, Landroid/graphics/Point;->y:I

    .line 1559
    :goto_2
    return-object v4

    .line 1529
    .end local v2    # "offset_x":I
    .end local v3    # "offset_y":I
    .end local v7    # "x":I
    .end local v8    # "y":I
    :cond_0
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getPortraitFitInBitmapRect()Landroid/graphics/Rect;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    goto :goto_0

    .line 1552
    .restart local v2    # "offset_x":I
    .restart local v3    # "offset_y":I
    .restart local v7    # "x":I
    .restart local v8    # "y":I
    :cond_1
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v7

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 1557
    :cond_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v8

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public hidePopUpToast()V
    .locals 1

    .prologue
    .line 1434
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    if-eqz v0, :cond_0

    .line 1435
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->hide()V

    .line 1437
    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;JJLjava/lang/String;IIILcom/arcsoft/magicshotstudio/utils/MSize;JJ)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "contentView"    # Landroid/view/View;
    .param p4, "bitmapPtr"    # J
    .param p6, "preData"    # J
    .param p8, "savePath"    # Ljava/lang/String;
    .param p9, "degree"    # I
    .param p10, "selectPhotoNum"    # I
    .param p11, "inputNum"    # I
    .param p12, "size"    # Lcom/arcsoft/magicshotstudio/utils/MSize;
    .param p13, "exifBuff"    # J
    .param p15, "exifBuffSize"    # J

    .prologue
    .line 158
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v3, v4, p4

    if-eqz v3, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v3, v4, p6

    if-eqz v3, :cond_0

    if-eqz p8, :cond_0

    if-lez p10, :cond_0

    if-gtz p11, :cond_2

    .line 161
    :cond_0
    const-string v3, "initUI function MERR_INVALID_PARAM"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->abnormalToastShow(Ljava/lang/String;)V

    .line 210
    :cond_1
    :goto_0
    return-void

    .line 164
    :cond_2
    new-instance v3, Ljava/util/concurrent/Semaphore;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 165
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContentView:Landroid/view/View;

    .line 166
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIntent:Landroid/content/Intent;

    .line 167
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    .line 168
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 170
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    .line 171
    move/from16 v0, p9

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCameraDegress:I

    .line 172
    move/from16 v0, p10

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSelectPhotoNum:I

    .line 173
    move/from16 v0, p11

    move-object/from16 v1, p0

    iput v0, v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mInputNum:I

    .line 175
    const-string v3, "MAGICSHOT_SPF_Key"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSharedPref:Landroid/content/SharedPreferences;

    .line 176
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 178
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 179
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-nez v3, :cond_3

    .line 180
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    move-object/from16 v0, p0

    iget v6, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSelectPhotoNum:I

    invoke-direct {v3, v4, v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;-><init>(Lcom/arcsoft/magicshotstudio/utils/TimeRecord;ZI)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .line 183
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->initDispalyUI()V

    .line 184
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v3, :cond_1

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->setProcessListener(Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;)V

    .line 194
    move/from16 v0, p11

    new-array v12, v0, [I

    .line 195
    .local v12, "bestIndex":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    move/from16 v0, p11

    if-ge v2, v0, :cond_4

    .line 196
    aput v2, v12, v2

    .line 195
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 199
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v4, v4, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSrcMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v5, v5, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScreenWidth:I

    const/16 v7, 0xe6

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    move/from16 v13, p9

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    invoke-virtual/range {v3 .. v17}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->init(IIIIJJ[IIJJ)J

    .line 202
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->StartProcess()V

    goto/16 :goto_0
.end method

.method public isSaveState()Z
    .locals 1

    .prologue
    .line 1236
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    .line 1237
    .local v0, "ret":Z
    return v0
.end method

.method public onBackPressed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1339
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1340
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    .line 1341
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 1364
    :goto_0
    return v0

    .line 1345
    :cond_0
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1346
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 1347
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->visibleSwitch(Z)V

    .line 1348
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFocusIndex(I)V

    .line 1349
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBars()V

    goto :goto_0

    .line 1352
    :cond_1
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 1353
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_2

    .line 1354
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    :cond_2
    :goto_1
    move v0, v1

    .line 1364
    goto :goto_0

    .line 1357
    :cond_3
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    if-ne v2, v0, :cond_2

    .line 1358
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_2

    .line 1359
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x5000

    const/4 v2, 0x0

    .line 1163
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOperateEnable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1233
    :goto_0
    return-void

    .line 1166
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1221
    :pswitch_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hidePopUpToast()V

    .line 1222
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1223
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->isUserEdit()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1224
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 1168
    :pswitch_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hidePopUpToast()V

    .line 1188
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    .line 1189
    .local v0, "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1190
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    if-eqz v1, :cond_1

    .line 1191
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->saveResult_EX()V

    .line 1192
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1193
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->changeSaveBtnState(Z)V

    goto :goto_0

    .line 1195
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    if-eqz v1, :cond_2

    .line 1196
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto :goto_0

    .line 1198
    :cond_2
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 1199
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto :goto_0

    .line 1203
    :cond_3
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 1204
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->saveResult_EX()V

    .line 1205
    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1206
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->exitActivity()V

    .line 1207
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 1211
    .end local v0    # "service":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    :pswitch_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hidePopUpToast()V

    .line 1212
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mbIsSaveBtnOn:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->isUserEdit()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1213
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_0

    .line 1216
    :cond_4
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 1217
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1218
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    goto/16 :goto_0

    .line 1227
    :cond_5
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    goto/16 :goto_0

    .line 1166
    :pswitch_data_0
    .packed-switch 0x7f09000c
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x1

    .line 522
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hidePopUpToast()V

    .line 523
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 524
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    .line 528
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setActionBarLongClickable(Z)V

    .line 529
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->updateGapValue(Z)V

    .line 530
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 531
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2, p1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->updateHelpConfig(ILandroid/content/res/Configuration;)V

    .line 542
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_2

    .line 543
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mActionBarRelativeLayout:Landroid/widget/RelativeLayout;

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurTouchedId:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 544
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurEvent:Landroid/view/MotionEvent;

    if-eqz v1, :cond_2

    .line 545
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurEvent:Landroid/view/MotionEvent;

    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 546
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mButtonTouchListener:Landroid/view/View$OnTouchListener;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurEvent:Landroid/view/MotionEvent;

    invoke-interface {v1, v0, v2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 549
    .end local v0    # "view":Landroid/view/View;
    :cond_2
    return-void

    .line 525
    :cond_3
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_0

    .line 526
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsLandScape:Z

    goto :goto_0
.end method

.method public onDoubleClick(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1592
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOperateEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1596
    :goto_0
    return-void

    .line 1595
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->closeToast()V

    goto :goto_0
.end method

.method public onHomeKeyPressed()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1368
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_1

    .line 1369
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1370
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1372
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onSaveMergeResult()V

    .line 1373
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->finish()V

    .line 1374
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1376
    :cond_1
    return v2
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_0

    .line 1328
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->onPause()V

    .line 1330
    :cond_0
    return-void
.end method

.method public onPicEditFinish()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 913
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBars()V

    .line 918
    const/4 v2, 0x0

    .line 919
    .local v2, "result":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v4, :cond_0

    .line 920
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getResultImage()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 923
    :cond_0
    if-eqz v2, :cond_2

    .line 924
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v4, :cond_1

    .line 925
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v4, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 927
    :cond_1
    invoke-direct {p0, v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->visibleSwitch(Z)V

    .line 928
    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 929
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFocusIndex(I)V

    .line 932
    :cond_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v4, :cond_4

    .line 933
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getRectVector()Ljava/util/Vector;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    .line 934
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v4, :cond_4

    .line 935
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v1

    .line 936
    .local v1, "length":I
    new-array v3, v1, [Landroid/graphics/Rect;

    .line 937
    .local v3, "tempRects":[Landroid/graphics/Rect;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 938
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    aput-object v4, v3, v0

    .line 937
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 942
    :cond_3
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v4, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFaceRect([Landroid/graphics/Rect;)V

    .line 943
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setEdited()V

    .line 948
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v3    # "tempRects":[Landroid/graphics/Rect;
    :cond_4
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProcessingFinish:Z

    .line 950
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    if-eqz v4, :cond_5

    .line 951
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v5, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v7, v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 955
    :cond_5
    return-void
.end method

.method public onPicEditStart()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 879
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonChoose:I

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setCurrentPerson(I)V

    .line 881
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    if-eqz v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    invoke-virtual {v0, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->setCandidateFaceView(ILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;[I)V

    .line 883
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    invoke-virtual {v0, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->setEnableScroll(Z)V

    .line 886
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 887
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->hideGoodposeView(I)V

    .line 888
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentPerson:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->setFocusFaceView(I)V

    .line 891
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideBars()V

    .line 892
    invoke-direct {p0, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->visibleSwitch(Z)V

    .line 893
    const/4 v0, 0x2

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 895
    return-void
.end method

.method public onProcessImgFinish()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v8, 0x1

    .line 765
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsActivityFinished:Z

    if-eqz v6, :cond_1

    .line 840
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    const/4 v4, 0x0

    .line 770
    .local v4, "result":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v6, :cond_4

    .line 771
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v6, :cond_2

    .line 772
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const/16 v7, 0x64

    invoke-virtual {v6, v7}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 775
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getAllPersonFaceInfo()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v6

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .line 776
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getPersonNum()I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonNum:I

    .line 777
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getReferenceImageId()I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReferenceImageId:I

    .line 778
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mInputNum:I

    if-eq v8, v6, :cond_3

    .line 779
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getResultImage()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 781
    :cond_3
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->getRectVector()Ljava/util/Vector;

    move-result-object v6

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    .line 784
    :cond_4
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v6, :cond_5

    if-eqz v4, :cond_5

    .line 785
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v6, v4}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 788
    :cond_5
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v6, :cond_7

    .line 789
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v3

    .line 790
    .local v3, "length":I
    new-array v5, v3, [Landroid/graphics/Rect;

    .line 791
    .local v5, "tempRects":[Landroid/graphics/Rect;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_6

    .line 792
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    invoke-virtual {v6, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    aput-object v6, v5, v2

    .line 791
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 796
    :cond_6
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v6, v5}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFaceRect([Landroid/graphics/Rect;)V

    .line 799
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v5    # "tempRects":[Landroid/graphics/Rect;
    :cond_7
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonNum:I

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    .line 800
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonNum:I

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    .line 802
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    if-eqz v6, :cond_8

    .line 803
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonNum:I

    if-ge v2, v6, :cond_8

    .line 804
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    aget-object v7, v7, v2

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetFaceGroup()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    move-result-object v7

    array-length v7, v7

    aput v7, v6, v2

    .line 805
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    aget-object v7, v7, v2

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetBestID()I

    move-result v7

    aput v7, v6, v2

    .line 803
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 809
    .end local v2    # "i":I
    :cond_8
    iput v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 810
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideProgressDialog()V

    .line 811
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v6, :cond_9

    .line 812
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/BestFace;->hideFirstImage()V

    .line 813
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v6, v10}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setVisibility(I)V

    .line 815
    :cond_9
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBarsWithoutAnimation()V

    .line 817
    iput-boolean v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProcessingFinish:Z

    .line 818
    invoke-virtual {p0, v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 820
    iget v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mInputNum:I

    if-ne v8, v6, :cond_a

    .line 821
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v6, :cond_a

    .line 822
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->invalidate()V

    .line 827
    :cond_a
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    if-eqz v6, :cond_0

    .line 828
    const/4 v0, 0x0

    .line 829
    .local v0, "candidateFaceNum":I
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mFaceRectArray:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v1

    .line 831
    .local v1, "candidatePersonNum":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    array-length v6, v6

    if-ge v2, v6, :cond_b

    .line 832
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCandidateFaceNum:[I

    aget v6, v6, v2

    add-int/2addr v0, v6

    .line 831
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 834
    :cond_b
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v7, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->FACE_NUM:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "person number = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; face number = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v10, v8}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 837
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v7, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->APP_START_PROCESS_DISMISS:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v10, v8}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    goto/16 :goto_0
.end method

.method public onProcessImgNoFace()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 844
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsActivityFinished:Z

    if-eqz v0, :cond_1

    .line 864
    :cond_0
    :goto_0
    return-void

    .line 847
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_2

    .line 848
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 850
    :cond_2
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReferenceImageId:I

    .line 851
    const/4 v0, 0x4

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 852
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hideProgressDialog()V

    .line 853
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBarsWithoutAnimation()V

    .line 854
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showNoFaceDialog()V

    .line 855
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProcessingFinish:Z

    .line 856
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 859
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->FACE_NUM:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const-string v2, "no person has been detected"

    invoke-virtual {v0, v1, v3, v2}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    .line 861
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->APP_START_PROCESS_DISMISS:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J

    goto :goto_0
.end method

.method public onProcessImgStart()V
    .locals 0

    .prologue
    .line 754
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showProgressDialog()V

    .line 755
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 1333
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_0

    .line 1334
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->onResume()V

    .line 1336
    :cond_0
    return-void
.end method

.method public onSaveDirect()V
    .locals 0

    .prologue
    .line 960
    return-void
.end method

.method public onSaveMergeResult()V
    .locals 1

    .prologue
    .line 964
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOperateEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 965
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 966
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->saveResult()V

    .line 991
    :cond_0
    return-void
.end method

.method public onSetProcessBar(I)V
    .locals 3
    .param p1, "process"    # I

    .prologue
    .line 1116
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_1

    .line 1117
    add-int/lit8 p1, p1, 0x19

    .line 1118
    move v0, p1

    .line 1119
    .local v0, "progress":I
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressNum:I

    if-eqz v1, :cond_0

    .line 1120
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressNum:I

    mul-int/lit8 v2, p1, 0x1e

    div-int/lit8 v2, v2, 0x64

    add-int v0, v1, v2

    .line 1122
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 1124
    .end local v0    # "progress":I
    :cond_1
    return-void
.end method

.method public onShowFirstImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1129
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResWidth:I

    .line 1130
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResHeight:I

    .line 1131
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1137
    :goto_0
    return-void

    .line 1135
    :cond_0
    const-string v0, "onShowFirstImage MERR_INVALID_PARAM"

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->abnormalToastShow(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSingleClick(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1582
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->getOperateEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1587
    :goto_0
    return-void

    .line 1585
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->closeToast()V

    .line 1586
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->processSingleTouchEvent(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1103
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->hidePopUpToast()V

    .line 1104
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 1106
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1110
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    .line 1112
    :cond_0
    return-void

    .line 1107
    :catch_0
    move-exception v0

    .line 1108
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public processSingleTouchEvent(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, -0x1

    .line 1267
    if-nez p1, :cond_0

    .line 1296
    :goto_0
    return-void

    .line 1271
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1276
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v0, :cond_1

    .line 1277
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getRectChoosed(FF)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonChoose:I

    .line 1280
    :cond_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonChoose:I

    if-eq v0, v3, :cond_2

    .line 1281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mReplaceHistoryArray:[I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPersonChoose:I

    aget v0, v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentCandidate:I

    .line 1282
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->onPicEditStart()V

    goto :goto_0

    .line 1273
    :pswitch_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->switchActionBar()V

    goto :goto_0

    .line 1284
    :cond_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->switchActionBar()V

    goto :goto_0

    .line 1288
    :pswitch_3
    const/4 v0, 0x1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentState:I

    .line 1289
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v0, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->setFocusIndex(I)V

    .line 1290
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->visibleSwitch(Z)V

    .line 1291
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->showBars()V

    goto :goto_0

    .line 1271
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public saveResult()V
    .locals 2

    .prologue
    .line 996
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 998
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1002
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    .line 1007
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->getSavePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    .line 1008
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$5;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    .line 1029
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1030
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1031
    return-void

    .line 999
    :catch_0
    move-exception v0

    .line 1000
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public saveResult_EX()V
    .locals 2

    .prologue
    .line 1033
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 1035
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1039
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    .line 1045
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/BestFace;->getSavePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResFileName:Ljava/lang/String;

    .line 1046
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$6;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic$6;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    .line 1061
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1062
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1063
    return-void

    .line 1036
    :catch_0
    move-exception v0

    .line 1037
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCurrentCandidate(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 1263
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mCurrentCandidate:I

    .line 1264
    return-void
.end method

.method public setEnablePerformanceShow(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 634
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsForQA:Z

    .line 635
    return-void
.end method

.method public setIfKeepSourceFile(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 638
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mKeepSrcFiles:Z

    .line 639
    return-void
.end method

.method public setIsActivityFinished(Z)V
    .locals 0
    .param p1, "finished"    # Z

    .prologue
    .line 681
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mIsActivityFinished:Z

    .line 682
    return-void
.end method

.method public setOperateEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1093
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mOperateEnable:Z

    .line 1094
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    if-eqz v0, :cond_0

    .line 1095
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcTouchListener;->setOperateEnable(Z)V

    .line 1097
    :cond_0
    return-void
.end method

.method public setProgressDialog(Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;I)V
    .locals 0
    .param p1, "progressDialog"    # Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    .param p2, "progress"    # I

    .prologue
    .line 509
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressingDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 510
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mProgressNum:I

    .line 511
    return-void
.end method

.method public setTimeRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord;)V
    .locals 0
    .param p1, "timeRecord"    # Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .prologue
    .line 1382
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mTimeRecord:Lcom/arcsoft/magicshotstudio/utils/TimeRecord;

    .line 1383
    return-void
.end method

.method public setUserEditFlag()V
    .locals 1

    .prologue
    .line 1599
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/BestFace;->setUserEditFlag()V

    .line 1600
    return-void
.end method

.method public unInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 560
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->dismissAllDialog()V

    .line 562
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 568
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->unInit()V

    .line 569
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPicturesBestEngine:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    .line 570
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 577
    :cond_1
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mAllPersonFace:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .line 578
    return-void
.end method

.method public unInitUI()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 580
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->unInit()V

    .line 582
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    if-eqz v0, :cond_1

    .line 586
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;->unInit()V

    .line 587
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mScrollView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcHorizontalScrollView;

    .line 589
    :cond_1
    return-void
.end method

.method public updateZoomInPointX()I
    .locals 4

    .prologue
    .line 1485
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getWidth()I

    move-result v1

    .line 1486
    .local v1, "viewWidth":I
    const v2, 0x3f2aaaab

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v1, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 1488
    .local v0, "temp":I
    return v0
.end method

.method public updateZoomInPointY()I
    .locals 4

    .prologue
    .line 1492
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getHeight()I

    move-result v1

    .line 1493
    .local v1, "viewHeight":I
    const v2, 0x3f2aaaab

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v1, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 1495
    .local v0, "temp":I
    return v0
.end method

.method public updateZoomState()V
    .locals 14

    .prologue
    .line 1441
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;

    move-result-object v3

    .line 1442
    .local v3, "mAspectQuotient":Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;
    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcAspectQuotient;->get()F

    move-result v0

    .line 1444
    .local v0, "aspectQuotient":F
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getWidth()I

    move-result v7

    .line 1445
    .local v7, "viewWidth":I
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mPhotoView:Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/bestface/widgets/ArcImageView;->getHeight()I

    move-result v6

    .line 1446
    .local v6, "viewHeight":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResWidth:I

    .line 1447
    .local v2, "bitmapWidth":I
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mResHeight:I

    .line 1449
    .local v1, "bitmapHeight":I
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanX()F

    move-result v4

    .line 1450
    .local v4, "panX":F
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getPanY()F

    move-result v5

    .line 1451
    .local v5, "panY":F
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v10, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomX(F)F

    move-result v10

    int-to-float v11, v7

    mul-float/2addr v10, v11

    int-to-float v11, v2

    div-float v8, v10, v11

    .line 1452
    .local v8, "zoomX":F
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mState:Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;

    invoke-virtual {v10, v0}, Lcom/arcsoft/magicshotstudio/ui/bestface/zoom/ArcZoomState;->getZoomY(F)F

    move-result v10

    int-to-float v11, v6

    mul-float/2addr v10, v11

    int-to-float v11, v1

    div-float v9, v10, v11

    .line 1454
    .local v9, "zoomY":F
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v11, v2

    mul-float/2addr v11, v4

    int-to-float v12, v7

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v8

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 1455
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v11, v1

    mul-float/2addr v11, v5

    int-to-float v12, v6

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v13, v9

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->top:I

    .line 1456
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    int-to-float v12, v7

    div-float/2addr v12, v8

    add-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->right:I

    .line 1457
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    int-to-float v12, v6

    div-float/2addr v12, v9

    add-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->bottom:I

    .line 1458
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 1459
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/Rect;->top:I

    .line 1460
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iput v7, v10, Landroid/graphics/Rect;->right:I

    .line 1461
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iput v6, v10, Landroid/graphics/Rect;->bottom:I

    .line 1462
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    if-gez v10, :cond_0

    .line 1463
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v11, v10, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    neg-int v12, v12

    int-to-float v12, v12

    mul-float/2addr v12, v8

    add-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 1464
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/Rect;->left:I

    .line 1466
    :cond_0
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    if-le v10, v2, :cond_1

    .line 1467
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v11, v10, Landroid/graphics/Rect;->right:I

    int-to-float v11, v11

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    sub-int/2addr v12, v2

    int-to-float v12, v12

    mul-float/2addr v12, v8

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->right:I

    .line 1468
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iput v2, v10, Landroid/graphics/Rect;->right:I

    .line 1470
    :cond_1
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    if-gez v10, :cond_2

    .line 1471
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v11, v10, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    neg-int v12, v12

    int-to-float v12, v12

    mul-float/2addr v12, v9

    add-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->top:I

    .line 1472
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/Rect;->top:I

    .line 1474
    :cond_2
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    if-le v10, v1, :cond_3

    .line 1475
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectDst:Landroid/graphics/Rect;

    iget v11, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v11, v11

    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v12, v1

    int-to-float v12, v12

    mul-float/2addr v12, v9

    sub-float/2addr v11, v12

    float-to-int v11, v11

    iput v11, v10, Landroid/graphics/Rect;->bottom:I

    .line 1476
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->mRectSrc:Landroid/graphics/Rect;

    iput v1, v10, Landroid/graphics/Rect;->bottom:I

    .line 1478
    :cond_3
    return-void
.end method
