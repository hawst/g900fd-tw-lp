.class public interface abstract Lcom/arcsoft/magicshotstudio/ui/bestface/ProcessListener;
.super Ljava/lang/Object;
.source "ProcessListener.java"


# virtual methods
.method public abstract onPicEditFinish()V
.end method

.method public abstract onPicEditStart()V
.end method

.method public abstract onProcessImgFinish()V
.end method

.method public abstract onProcessImgNoFace()V
.end method

.method public abstract onProcessImgStart()V
.end method

.method public abstract onSaveDirect()V
.end method

.method public abstract onSaveMergeResult()V
.end method

.method public abstract onSetProcessBar(I)V
.end method

.method public abstract onShowFirstImage(Landroid/graphics/Bitmap;)V
.end method
