.class Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;
.super Ljava/lang/Thread;
.source "PicturesBestEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->StartProcess()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    .line 238
    const/16 v7, 0xa

    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V

    .line 240
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 241
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->process()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v8

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1702(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    .line 244
    :cond_0
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v7

    if-nez v7, :cond_1

    .line 245
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 314
    :goto_0
    return-void

    .line 249
    :cond_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v8

    array-length v8, v8

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I
    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1802(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;I)I

    .line 250
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1800(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v7

    if-nez v7, :cond_2

    .line 251
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 255
    :cond_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    array-length v7, v7

    new-array v5, v7, [I

    .line 256
    .local v5, "personIds":[I
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    array-length v7, v7

    new-array v2, v7, [I

    .line 257
    .local v2, "faceIds":[I
    const/4 v1, 0x0

    .line 258
    .local v1, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    array-length v7, v7

    if-ge v4, v7, :cond_4

    .line 259
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mProcessResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1700(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    aget-object v7, v7, v4

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->GetBestID()I

    move-result v0

    .line 260
    .local v0, "bestFaceId":I
    if-eqz v0, :cond_3

    .line 261
    add-int/lit8 v1, v1, 0x1

    .line 263
    :cond_3
    aput v4, v5, v4

    .line 264
    aput v0, v2, v4

    .line 258
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 267
    .end local v0    # "bestFaceId":I
    :cond_4
    const/4 v7, 0x1

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mSelectPhotoNum:I
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1900(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v8

    if-eq v7, v8, :cond_5

    .line 268
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-result-object v7

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mMergeResult:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2000(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    move-result-object v8

    invoke-virtual {v7, v5, v2, v8}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->applyMultiMerge([I[ILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I

    .line 271
    :cond_5
    const/4 v4, 0x0

    :goto_2
    array-length v7, v5

    if-ge v4, v7, :cond_6

    .line 272
    const-string v7, "ArcSoft_BestFace_PicturesBestEngine"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "change faceIndexs_Changed["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget v9, v5, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v7, "ArcSoft_BestFace_PicturesBestEngine"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "change replaceFaceImageIndexs_Changed["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget v9, v2, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 276
    :cond_6
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v7

    if-nez v7, :cond_7

    .line 277
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7, v8}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2102(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;Ljava/util/Vector;)Ljava/util/Vector;

    .line 280
    :cond_7
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 281
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Vector;->clear()V

    .line 282
    const/4 v3, 0x0

    .line 283
    .local v3, "faceInfo":Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 284
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mNative:Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1600(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;

    move-result-object v7

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->getResultFaceInfo()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v3

    .line 285
    if-eqz v3, :cond_9

    .line 286
    const/4 v4, 0x0

    :goto_3
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mPersonNum:I
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1800(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)I

    move-result v7

    if-ge v4, v7, :cond_9

    .line 287
    if-ltz v4, :cond_8

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v7

    array-length v7, v7

    if-ge v4, v7, :cond_8

    .line 288
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    move-result-object v8

    aget-object v8, v8, v4

    invoke-virtual {v8}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->getResFaceRect()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 286
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 296
    .end local v3    # "faceInfo":Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    :cond_9
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v7

    if-eqz v7, :cond_a

    .line 297
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mFaceRectArray:Ljava/util/Vector;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$2100(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v6

    .line 307
    .local v6, "size":I
    if-nez v6, :cond_a

    .line 308
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 313
    .end local v6    # "size":I
    :cond_a
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;->access$1500(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
