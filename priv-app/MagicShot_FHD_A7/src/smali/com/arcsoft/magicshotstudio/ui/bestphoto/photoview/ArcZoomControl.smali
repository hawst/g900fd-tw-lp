.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
.super Ljava/lang/Object;
.source "ArcZoomControl.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;
    }
.end annotation


# static fields
.field private static final ESPINON:F = 1.0E-6f

.field public static final MAX_DOUBLE_TAP_FACTOR:F = 2.5f

.field private static final MAX_ZOOM:F = 3.0f

.field private static final MIN_DOUBLE_TAP_FACTOR:F = 1.0f

.field private static final MIN_ZOOM:F = 1.0f


# instance fields
.field private mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

.field private mContext:Landroid/content/Context;

.field private mCurrentDirection:I

.field private mDoubleTapZoomInOrOut:Z

.field private final mDoubleTapZoomRunnable:Ljava/lang/Runnable;

.field private mDoubleZoomFactor:F

.field private mDoubleZoomInXPosition:F

.field private mDoubleZoomInYPosition:F

.field private final mHandler:Landroid/os/Handler;

.field private mPanMaxX:F

.field private mPanMaxY:F

.field private mPanMinX:F

.field private mPanMinY:F

.field private mPointF:Landroid/graphics/PointF;

.field private final mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

.field private mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    .line 19
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    .line 20
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mHandler:Landroid/os/Handler;

    .line 29
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mContext:Landroid/content/Context;

    .line 292
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    .line 293
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F

    .line 294
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F

    .line 295
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    .line 301
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    .line 357
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    .line 362
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    .line 32
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    return p1
.end method

.method static synthetic access$116(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$124(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F

    return v0
.end method

.method static synthetic access$302(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F

    return p1
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F

    return v0
.end method

.method static synthetic access$402(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
    .param p1, "x1"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F

    return p1
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private doubleTapZoomInWithAnimation()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 290
    return-void
.end method

.method private getMaxPanDelta(F)F
    .locals 3
    .param p1, "zoom"    # F

    .prologue
    .line 137
    const/4 v0, 0x0

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, p1, v2

    div-float/2addr v2, p1

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private limitPan()V
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v8, 0x0

    .line 172
    const/4 v7, 0x0

    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    .line 174
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 176
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v5

    .line 177
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v6

    .line 179
    .local v6, "zoomY":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    sub-float v3, v9, v7

    .line 180
    .local v3, "panMinX":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    add-float v1, v9, v7

    .line 181
    .local v1, "panMaxX":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    sub-float v4, v9, v7

    .line 182
    .local v4, "panMinY":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    add-float v2, v9, v7

    .line 184
    .local v2, "panMaxY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v7

    cmpg-float v7, v7, v3

    if-gez v7, :cond_0

    .line 185
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 186
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    cmpl-float v7, v7, v8

    if-lez v7, :cond_0

    .line 187
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    or-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    .line 192
    :cond_0
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v7

    cmpl-float v7, v7, v1

    if-lez v7, :cond_1

    .line 193
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 194
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    .line 195
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    or-int/lit8 v7, v7, 0x4

    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    .line 200
    :cond_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v7

    cmpg-float v7, v7, v4

    if-gez v7, :cond_2

    .line 201
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 202
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    cmpl-float v7, v7, v8

    if-lez v7, :cond_2

    .line 203
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    or-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    .line 208
    :cond_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_3

    .line 209
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 210
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    cmpl-float v7, v7, v8

    if-lez v7, :cond_3

    .line 211
    iget v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    or-int/lit8 v7, v7, 0x8

    iput v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    .line 216
    :cond_3
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v7

    cmpl-float v7, v10, v7

    if-gtz v7, :cond_4

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v7

    cmpg-float v7, v10, v7

    if-gez v7, :cond_5

    .line 217
    :cond_4
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    invoke-virtual {v7, v8}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setEffectDirection(I)V

    .line 222
    :cond_5
    return-void
.end method

.method private limitPanWithoutEdgeEffect()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f000000    # 0.5f

    .line 225
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 227
    .local v0, "aspectQuotient":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v5

    .line 228
    .local v5, "zoomX":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v6

    .line 230
    .local v6, "zoomY":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    sub-float v3, v8, v7

    .line 231
    .local v3, "panMinX":F
    invoke-direct {p0, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    add-float v1, v8, v7

    .line 232
    .local v1, "panMaxX":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    sub-float v4, v8, v7

    .line 233
    .local v4, "panMinY":F
    invoke-direct {p0, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v7

    add-float v2, v8, v7

    .line 235
    .local v2, "panMaxY":F
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v7

    cmpg-float v7, v7, v3

    if-gez v7, :cond_0

    .line 236
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 238
    :cond_0
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v7

    cmpl-float v7, v7, v1

    if-lez v7, :cond_1

    .line 239
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 241
    :cond_1
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v7

    cmpg-float v7, v7, v4

    if-gez v7, :cond_2

    .line 242
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 244
    :cond_2
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_3

    .line 245
    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v7, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 248
    :cond_3
    return-void
.end method

.method private limitZoom()V
    .locals 4

    .prologue
    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 141
    const/16 v0, 0xf

    .line 142
    .local v0, "currentDirection":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setEffectDirection(I)V

    .line 144
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    .line 152
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    .line 148
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->stopZoomEdgeEffect()V

    goto :goto_0
.end method

.method private limitZoomWithoutEdgeEffect()V
    .locals 3

    .prologue
    const/high16 v2, 0x40400000    # 3.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 251
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    goto :goto_0
.end method

.method private updatePanLimits()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 160
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 162
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v1

    .line 163
    .local v1, "zoomX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v2

    .line 165
    .local v2, "zoomY":F
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    sub-float v3, v4, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPanMinX:F

    .line 166
    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    add-float/2addr v3, v4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPanMaxX:F

    .line 167
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    sub-float v3, v4, v3

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPanMinY:F

    .line 168
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->getMaxPanDelta(F)F

    move-result v3

    add-float/2addr v3, v4

    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPanMaxY:F

    .line 169
    return-void
.end method


# virtual methods
.method public doubleTapZoom(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 273
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    .line 275
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    .line 276
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F

    .line 277
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F

    .line 278
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->doubleTapZoomInWithAnimation()V

    .line 286
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F

    .line 281
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F

    .line 282
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F

    .line 283
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->doubleTapZoomInWithAnimation()V

    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomInOrOut:Z

    goto :goto_0
.end method

.method public getZoomState()Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    return-object v0
.end method

.method public isLeftEdge()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 431
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRightEdge()Z
    .locals 2

    .prologue
    .line 436
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mCurrentDirection:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isZoomed()Z
    .locals 2

    .prologue
    .line 421
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 422
    const/4 v0, 0x1

    .line 425
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pan(FF)V
    .locals 4
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 103
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 105
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v3

    div-float/2addr p1, v3

    .line 106
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v3

    div-float/2addr p2, v3

    .line 108
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v3

    add-float v1, v3, p1

    .line 109
    .local v1, "newPanX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v3

    add-float v2, v3, p2

    .line 111
    .local v2, "newPanY":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 112
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 113
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setEffectOffset_X(F)V

    .line 114
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, p2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setEffectOffset_Y(F)V

    .line 116
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->limitPan()V

    .line 117
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    .line 118
    return-void
.end method

.method public panWithoutEdgeEffect(FF)V
    .locals 4
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 121
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 123
    .local v0, "aspectQuotient":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v3

    div-float/2addr p1, v3

    .line 124
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v3

    div-float/2addr p2, v3

    .line 126
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v3

    add-float v1, v3, p1

    .line 127
    .local v1, "newPanX":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v3

    add-float v2, v3, p2

    .line 129
    .local v2, "newPanY":F
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 130
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 132
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->limitPanWithoutEdgeEffect()V

    .line 133
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    .line 134
    return-void
.end method

.method public setAspectQuotient(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;)V
    .locals 1
    .param p1, "aspectQuotient"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->deleteObserver(Ljava/util/Observer;)V

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    .line 41
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->addObserver(Ljava/util/Observer;)V

    .line 42
    return-void
.end method

.method public setEdgeEffectEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 262
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setEdgeEffectEnabled(Z)V

    .line 263
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    .line 264
    return-void
.end method

.method public setZoomStateListener(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    .line 360
    return-void
.end method

.method public startZoom()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->isZooming()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->startZooming()V

    .line 52
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->startZoom()V

    .line 54
    :cond_0
    return-void
.end method

.method public stopDoubleTapZoomRunnable()V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 344
    :cond_0
    return-void
.end method

.method public stopZoom()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->isZooming()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->stopZooming()V

    .line 60
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->stopZoom()V

    .line 62
    :cond_0
    return-void
.end method

.method public stopZoomEdgeEffect()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->stopZoomEdgeEffect()V

    .line 157
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->limitZoom()V

    .line 269
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->limitPanWithoutEdgeEffect()V

    .line 270
    return-void
.end method

.method public updatePan(Landroid/view/MotionEvent;II)V
    .locals 15
    .param p1, "e"    # Landroid/view/MotionEvent;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    .line 366
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    if-nez v12, :cond_0

    .line 418
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v12}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v12

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_1

    .line 371
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v12}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->getZoomOutPoint()Landroid/graphics/Point;

    move-result-object v11

    .line 372
    .local v11, "zoomOutPoint":Landroid/graphics/Point;
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v13, v11, Landroid/graphics/Point;->x:I

    int-to-float v13, v13

    move/from16 v0, p2

    int-to-float v14, v0

    div-float/2addr v13, v14

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 373
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v13, v11, Landroid/graphics/Point;->y:I

    int-to-float v13, v13

    move/from16 v0, p3

    int-to-float v14, v0

    div-float/2addr v13, v14

    iput v13, v12, Landroid/graphics/PointF;->y:F

    goto :goto_0

    .line 375
    .end local v11    # "zoomOutPoint":Landroid/graphics/Point;
    :cond_1
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v12}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->updateZoomState()V

    .line 377
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v12}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->getMaxSize()Lcom/arcsoft/magicshotstudio/utils/MSize;

    move-result-object v2

    .line 378
    .local v2, "mSize":Lcom/arcsoft/magicshotstudio/utils/MSize;
    iget v4, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 379
    .local v4, "maxWidth":I
    iget v3, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 381
    .local v3, "maxHeight":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v12}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->getZoomOffsetPoint()Landroid/graphics/Point;

    move-result-object v1

    .line 382
    .local v1, "mOffSetPoint":Landroid/graphics/Point;
    iget v9, v1, Landroid/graphics/Point;->x:I

    .line 383
    .local v9, "zoomOffsetX":I
    iget v10, v1, Landroid/graphics/Point;->y:I

    .line 384
    .local v10, "zoomOffsetY":I
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mZoomStateListener:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;

    invoke-interface {v12}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$ZoomStateListener;->getDestRect()Landroid/graphics/Rect;

    move-result-object v8

    .line 385
    .local v8, "rectDst":Landroid/graphics/Rect;
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v12

    sub-int v12, p2, v12

    div-int/lit8 v5, v12, 0x2

    .line 386
    .local v5, "offsetX":I
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v12

    sub-int v12, p3, v12

    div-int/lit8 v6, v12, 0x2

    .line 388
    .local v6, "offsetY":I
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 389
    .local v7, "pointAfterAdjust":Landroid/graphics/Point;
    move/from16 v0, p2

    if-gt v4, v0, :cond_2

    .line 390
    div-int/lit8 v12, p2, 0x2

    iput v12, v7, Landroid/graphics/Point;->x:I

    .line 402
    :goto_1
    move/from16 v0, p3

    if-gt v3, v0, :cond_5

    .line 403
    div-int/lit8 v12, p3, 0x2

    iput v12, v7, Landroid/graphics/Point;->y:I

    .line 415
    :goto_2
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v13, v7, Landroid/graphics/Point;->x:I

    int-to-float v13, v13

    move/from16 v0, p2

    int-to-float v14, v0

    div-float/2addr v13, v14

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 416
    iget-object v12, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mPointF:Landroid/graphics/PointF;

    iget v13, v7, Landroid/graphics/Point;->y:I

    int-to-float v13, v13

    move/from16 v0, p3

    int-to-float v14, v0

    div-float/2addr v13, v14

    iput v13, v12, Landroid/graphics/PointF;->y:F

    goto/16 :goto_0

    .line 392
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    add-int v13, v5, v9

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_3

    .line 393
    add-int v12, v5, v9

    iput v12, v7, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 395
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    sub-int v13, p2, v5

    sub-int/2addr v13, v9

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_4

    .line 396
    sub-int v12, p2, v5

    sub-int/2addr v12, v9

    iput v12, v7, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 399
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    float-to-int v12, v12

    iput v12, v7, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 405
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    add-int v13, v6, v10

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_6

    .line 406
    add-int v12, v6, v10

    iput v12, v7, Landroid/graphics/Point;->y:I

    goto :goto_2

    .line 408
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    sub-int v13, p3, v6

    sub-int/2addr v13, v10

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_7

    .line 409
    sub-int v12, p3, v6

    sub-int/2addr v12, v10

    iput v12, v7, Landroid/graphics/Point;->y:I

    goto :goto_2

    .line 412
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    float-to-int v12, v12

    iput v12, v7, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public zoom(FFF)V
    .locals 12
    .param p1, "f"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 65
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 67
    .local v0, "aspectQuotient":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v3

    .line 68
    .local v3, "prevZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v4

    .line 70
    .local v4, "prevZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v6

    mul-float/2addr v6, p1

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    .line 71
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->limitZoom()V

    .line 73
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v1

    .line 74
    .local v1, "newZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v2

    .line 77
    .local v2, "newZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v6

    sub-float v7, p2, v11

    div-float v8, v10, v3

    div-float v9, v10, v1

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 78
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v6

    sub-float v7, p3, v11

    div-float v8, v10, v4

    div-float v9, v10, v2

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 80
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    .line 81
    return-void
.end method

.method public zoomWithoutEdgeEffect(FFF)V
    .locals 12
    .param p1, "f"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    .line 84
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mAspectQuotient:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcAspectQuotient;->get()F

    move-result v0

    .line 86
    .local v0, "aspectQuotient":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v3

    .line 87
    .local v3, "prevZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v4

    .line 89
    .local v4, "prevZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setZoom(F)V

    .line 90
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->limitZoomWithoutEdgeEffect()V

    .line 92
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomX(F)F

    move-result v1

    .line 93
    .local v1, "newZoomX":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoomY(F)F

    move-result v2

    .line 96
    .local v2, "newZoomY":F
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanX()F

    move-result v6

    sub-float v7, p2, v11

    div-float v8, v10, v3

    div-float v9, v10, v1

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 97
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getPanY()F

    move-result v6

    sub-float v7, p3, v11

    div-float v8, v10, v4

    div-float v9, v10, v2

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 99
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    .line 100
    return-void
.end method
