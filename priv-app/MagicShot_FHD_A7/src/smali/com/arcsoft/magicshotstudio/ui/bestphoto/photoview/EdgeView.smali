.class public Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;
.super Landroid/view/View;
.source "EdgeView.java"


# static fields
.field public static final BOTTOM:I = 0x4

.field public static final DIR_ALL:I = 0x0

.field public static final DIR_BOTTOM:I = 0x8

.field public static final DIR_LEFT:I = 0x1

.field public static final DIR_RIGHT:I = 0x4

.field public static final DIR_SHOW_ALL:I = 0x10

.field public static final DIR_TOP:I = 0x2

.field public static final LEFT:I = 0x2

.field public static final RIGHT:I = 0x8

.field private static final TAG:Ljava/lang/String; = "ArcSoft_EdgeView"

.field public static final TOP:I = 0x1


# instance fields
.field private mEdgeBottom:I

.field private mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

.field private mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

.field private mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

.field private mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

.field private mEdgeLeft:I

.field private mEdgeRight:I

.field private mEdgeTop:I

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 28
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 29
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 30
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 32
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeLeft:I

    .line 33
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeRight:I

    .line 34
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeTop:I

    .line 35
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeBottom:I

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mRect:Landroid/graphics/Rect;

    .line 41
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 42
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 43
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 44
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 45
    return-void
.end method

.method private drawBottomEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 227
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228
    const/4 v0, 0x0

    .line 243
    :goto_0
    return v0

    .line 231
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 238
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeRight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeBottom:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 239
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 241
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 242
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawLeftEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 179
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 180
    const/4 v0, 0x0

    .line 192
    :goto_0
    return v0

    .line 183
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 187
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeBottom:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 188
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 190
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 191
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawRightEdge(Landroid/graphics/Canvas;)Z
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 196
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 197
    const/4 v0, 0x0

    .line 208
    :goto_0
    return v0

    .line 200
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 201
    .local v1, "restoreCount":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->getPaddingRight()I

    move-result v4

    sub-int v2, v3, v4

    .line 203
    .local v2, "width":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeRight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeTop:I

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 204
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 206
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 207
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawTopEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 212
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    const/4 v0, 0x0

    .line 223
    :goto_0
    return v0

    .line 217
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 219
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeTop:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 221
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 222
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method


# virtual methods
.method public getEdgeBound()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 74
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeLeft:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeRight:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeTop:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeBottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 75
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 150
    const/4 v1, 0x0

    .line 152
    .local v1, "needsInvalidate":Z
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeRight:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeLeft:I

    sub-int v2, v3, v4

    .line 153
    .local v2, "w":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeBottom:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeTop:I

    sub-int v0, v3, v4

    .line 154
    .local v0, "h":I
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 155
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    if-eqz v3, :cond_0

    .line 156
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->drawLeftEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 159
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    if-eqz v3, :cond_1

    .line 160
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->drawRightEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 163
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    if-eqz v3, :cond_2

    .line 164
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->drawTopEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 167
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    if-eqz v3, :cond_3

    .line 168
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->drawBottomEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 171
    :cond_3
    if-eqz v1, :cond_4

    .line 172
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->invalidate()V

    .line 176
    :cond_4
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 51
    :cond_0
    return-void
.end method

.method public onPull(FI)V
    .locals 2
    .param p1, "offset"    # F
    .param p2, "direction"    # I

    .prologue
    .line 81
    const/4 v0, 0x0

    .line 82
    .local v0, "tempEdgeEffect":Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;
    packed-switch p2, :pswitch_data_0

    .line 99
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onPull(F)V

    .line 101
    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->invalidate()V

    .line 106
    :cond_0
    const/4 v0, 0x0

    .line 107
    return-void

    .line 84
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 85
    goto :goto_0

    .line 87
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 88
    goto :goto_0

    .line 90
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 91
    goto :goto_0

    .line 93
    :pswitch_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    .line 94
    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onRelease()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 111
    const/4 v0, 0x0

    .line 116
    .local v0, "more":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 117
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    .line 119
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 120
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    or-int/2addr v0, v1

    .line 122
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 123
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    or-int/2addr v0, v1

    .line 125
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 126
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_3
    or-int/2addr v0, v2

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->invalidate()V

    .line 131
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 117
    goto :goto_0

    :cond_2
    move v1, v3

    .line 120
    goto :goto_1

    :cond_3
    move v1, v3

    .line 123
    goto :goto_2

    :cond_4
    move v2, v3

    .line 126
    goto :goto_3
.end method

.method public onRelease(I)V
    .locals 1
    .param p1, "releaseSide"    # I

    .prologue
    .line 133
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 136
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 139
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 142
    :cond_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_3

    .line 143
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->onRelease()V

    .line 145
    :cond_3
    return-void
.end method

.method public setEdgeBound(IIII)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 54
    sub-int v1, p3, p1

    .line 55
    .local v1, "w":I
    sub-int v0, p4, p2

    .line 57
    .local v0, "h":I
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeLeft:I

    .line 58
    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeRight:I

    .line 59
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeTop:I

    .line 60
    iput p4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeBottom:I

    .line 62
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setSize(II)V

    .line 63
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setSize(II)V

    .line 64
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setSize(II)V

    .line 65
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setSize(II)V

    .line 67
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setMinWidth(I)V

    .line 68
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setMinWidth(I)V

    .line 69
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setMinWidth(I)V

    .line 70
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcEdgeEffect;->setMinWidth(I)V

    .line 71
    return-void
.end method
