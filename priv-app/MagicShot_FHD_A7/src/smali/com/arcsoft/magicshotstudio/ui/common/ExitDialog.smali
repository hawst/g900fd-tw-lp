.class public Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
.super Ljava/lang/Object;
.source "ExitDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog$ExitDialogType;
    }
.end annotation


# static fields
.field public static final ID_COM_CANCEL_BTN:I = -0x3

.field public static final ID_COM_DISCARD_BTN:I = -0x2

.field public static final ID_COM_SAVE_BTN:I = -0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mDialogBuilder:Landroid/app/AlertDialog$Builder;

.field mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

.field mHasCancel:Z

.field mHasDiscard:Z

.field mHasSave:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mContext:Landroid/content/Context;

    .line 51
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    .line 52
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 58
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    .line 55
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mContext:Landroid/content/Context;

    .line 56
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 27
    :cond_0
    return-void
.end method

.method public setBtnOnClick(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    .line 61
    return-void
.end method

.method public setDialogButtons(ZZZ)V
    .locals 3
    .param p1, "hasSave"    # Z
    .param p2, "hasCancel"    # Z
    .param p3, "hasDiscard"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mHasSave:Z

    .line 78
    iput-boolean p2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mHasCancel:Z

    .line 79
    iput-boolean p3, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mHasDiscard:Z

    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_3

    .line 82
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mHasSave:Z

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000c

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 86
    :cond_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mHasDiscard:Z

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000e

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    :cond_1
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mHasCancel:Z

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06002d

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogCallBack:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    .line 95
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 97
    :cond_3
    return-void
.end method

.method public setMessage(I)V
    .locals 1
    .param p1, "messageId"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 73
    :cond_0
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 67
    :cond_0
    return-void
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 30
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 35
    const v2, 0x7f06004f

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setTitle(I)V

    .line 37
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 38
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030015

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 39
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 40
    invoke-virtual {p0, v4, v4, v4}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setDialogButtons(ZZZ)V

    .line 41
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    .line 42
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 44
    :cond_0
    return-void
.end method
