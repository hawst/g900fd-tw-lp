.class public Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;
.super Landroid/view/View;
.source "EdgeView.java"


# static fields
.field public static final BOTTOM:I = 0x4

.field public static final DIR_ALL:I = 0x0

.field public static final DIR_BOTTOM:I = 0x8

.field public static final DIR_LEFT:I = 0x1

.field public static final DIR_RIGHT:I = 0x4

.field public static final DIR_SHOW_ALL:I = 0x10

.field public static final DIR_TOP:I = 0x2

.field public static final LEFT:I = 0x2

.field public static final RIGHT:I = 0x8

.field private static final TAG:Ljava/lang/String; = "ArcSoft_EdgeView"

.field public static final TOP:I = 0x1


# instance fields
.field private mEdgeBottom:I

.field private mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

.field private mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

.field private mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

.field private mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

.field private mEdgeLeft:I

.field private mEdgeRight:I

.field private mEdgeTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 26
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 27
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 28
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 30
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeLeft:I

    .line 31
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeRight:I

    .line 32
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeTop:I

    .line 33
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeBottom:I

    .line 37
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 38
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 39
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 40
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-direct {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 41
    return-void
.end method

.method private drawBottomEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 218
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 234
    :goto_0
    return v0

    .line 222
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 229
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeRight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeBottom:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 230
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 232
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 233
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawLeftEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 170
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 183
    :goto_0
    return v0

    .line 174
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 178
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeBottom:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 179
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 181
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 182
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawRightEdge(Landroid/graphics/Canvas;)Z
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 187
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    const/4 v0, 0x0

    .line 199
    :goto_0
    return v0

    .line 191
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 192
    .local v1, "restoreCount":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->getPaddingRight()I

    move-result v4

    sub-int v2, v3, v4

    .line 194
    .local v2, "width":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeRight:I

    int-to-float v3, v3

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeTop:I

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 195
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 197
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 198
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method private drawTopEdge(Landroid/graphics/Canvas;)Z
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 203
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0

    .line 208
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 210
    .local v1, "restoreCount":I
    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeLeft:I

    int-to-float v2, v2

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeTop:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 212
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 213
    .local v0, "needsInvalidate":Z
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 141
    const/4 v1, 0x0

    .line 143
    .local v1, "needsInvalidate":Z
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeRight:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeLeft:I

    sub-int v2, v3, v4

    .line 144
    .local v2, "w":I
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeBottom:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeTop:I

    sub-int v0, v3, v4

    .line 145
    .local v0, "h":I
    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 146
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_0

    .line 147
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->drawLeftEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 150
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_1

    .line 151
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->drawRightEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 154
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_2

    .line 155
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->drawTopEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 158
    :cond_2
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    if-eqz v3, :cond_3

    .line 159
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->drawBottomEdge(Landroid/graphics/Canvas;)Z

    move-result v3

    or-int/2addr v1, v3

    .line 162
    :cond_3
    if-eqz v1, :cond_4

    .line 163
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->invalidate()V

    .line 167
    :cond_4
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 47
    :cond_0
    return-void
.end method

.method public onPull(FI)V
    .locals 2
    .param p1, "offset"    # F
    .param p2, "direction"    # I

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "tempEdgeEffect":Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;
    packed-switch p2, :pswitch_data_0

    .line 90
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onPull(F)V

    .line 92
    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->invalidate()V

    .line 97
    :cond_0
    const/4 v0, 0x0

    .line 98
    return-void

    .line 75
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 76
    goto :goto_0

    .line 78
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 79
    goto :goto_0

    .line 81
    :pswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 82
    goto :goto_0

    .line 84
    :pswitch_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    .line 85
    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onRelease()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 102
    const/4 v0, 0x0

    .line 107
    .local v0, "more":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 108
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    .line 110
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 111
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    or-int/2addr v0, v1

    .line 113
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 114
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    or-int/2addr v0, v1

    .line 116
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 117
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_3
    or-int/2addr v0, v2

    .line 119
    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->invalidate()V

    .line 122
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 108
    goto :goto_0

    :cond_2
    move v1, v3

    .line 111
    goto :goto_1

    :cond_3
    move v1, v3

    .line 114
    goto :goto_2

    :cond_4
    move v2, v3

    .line 117
    goto :goto_3
.end method

.method public onRelease(I)V
    .locals 1
    .param p1, "releaseSide"    # I

    .prologue
    .line 124
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 127
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 130
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 133
    :cond_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->onRelease()V

    .line 136
    :cond_3
    return-void
.end method

.method public setEdgeBound(IIII)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 50
    sub-int v1, p3, p1

    .line 51
    .local v1, "w":I
    sub-int v0, p4, p2

    .line 53
    .local v0, "h":I
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeLeft:I

    .line 54
    iput p3, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeRight:I

    .line 55
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeTop:I

    .line 56
    iput p4, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeBottom:I

    .line 58
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setSize(II)V

    .line 59
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setSize(II)V

    .line 60
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setSize(II)V

    .line 61
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setSize(II)V

    .line 63
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectLeft:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 64
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectRight:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 65
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectTop:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 66
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;->mEdgeEffectBottom:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcEdgeEffect;->setMinWidth(I)V

    .line 67
    return-void
.end method
