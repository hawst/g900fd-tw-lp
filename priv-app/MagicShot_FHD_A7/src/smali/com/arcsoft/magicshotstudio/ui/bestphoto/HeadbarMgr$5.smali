.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;
.super Ljava/lang/Thread;
.source "HeadbarMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->saveFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 475
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$1200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    move-result-object v5

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$1200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    move-result-object v5

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mIndexes:[I

    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$1200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    move-result-object v5

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mIndexes:[I

    array-length v5, v5

    if-lez v5, :cond_6

    .line 477
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBest()[Z

    move-result-object v4

    .line 479
    .local v4, "best":[Z
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 480
    .local v16, "saveIndexs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    array-length v5, v4

    if-ge v14, v5, :cond_1

    .line 481
    aget-boolean v5, v4, v14

    if-eqz v5, :cond_0

    .line 483
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$1200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    move-result-object v5

    iget-object v5, v5, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mIndexes:[I

    aget v5, v5, v14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    :cond_0
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 491
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getSavePath()Ljava/lang/String;

    move-result-object v17

    .line 492
    .local v17, "savePath":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v7, v5, [Ljava/lang/String;

    .line 494
    .local v7, "saveFiles":[Ljava/lang/String;
    const/4 v14, 0x0

    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v14, v5, :cond_2

    .line 495
    invoke-static/range {v17 .. v17}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->createFile(Ljava/lang/String;)V

    .line 496
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-static {v5, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V

    .line 498
    add-int/lit8 v19, v14, 0x1

    .line 499
    .local v19, "temp":I
    const-string v5, "."

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v14

    .line 494
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 503
    .end local v19    # "temp":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5, v7}, Lcom/arcsoft/magicshotstudio/BestPhoto;->setCurSavePaths([Ljava/lang/String;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getBmpAddress()J

    move-result-wide v8

    .line 506
    .local v8, "pBmp":J
    const/4 v15, -0x1

    .line 507
    .local v15, "res":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getNativeExifJPGBuff()J

    move-result-wide v10

    .line 508
    .local v10, "exifBuff":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getNativeExifJPGBuffSize()J

    move-result-wide v12

    .line 511
    .local v12, "exifBuffSize":J
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 512
    .local v18, "size":I
    move/from16 v0, v18

    new-array v6, v0, [I

    .line 513
    .local v6, "contents":[I
    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v14, v0, :cond_3

    .line 514
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v6, v14

    .line 513
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 517
    :cond_3
    const-wide/16 v20, 0x0

    cmp-long v5, v20, v10

    if-eqz v5, :cond_4

    const-wide/16 v20, 0x1

    cmp-long v5, v12, v20

    if-gez v5, :cond_5

    .line 518
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/arcsoft/magicshotstudio/BestPhoto;->saveBitmaps([I[Ljava/lang/String;J)I

    move-result v15

    .line 523
    :goto_3
    if-nez v15, :cond_6

    .line 524
    const/4 v14, 0x0

    :goto_4
    array-length v5, v7

    if-ge v14, v5, :cond_6

    .line 525
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/content/Context;

    move-result-object v5

    aget-object v20, v7, v14

    const/16 v21, 0x831

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    move-object/from16 v22, v0

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mOrientation:I
    invoke-static/range {v22 .. v22}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$1300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)I

    move-result v22

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v5, v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->refreshDBRecords(Landroid/content/Context;Ljava/lang/String;II)Z

    .line 524
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 520
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr$5;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual/range {v5 .. v13}, Lcom/arcsoft/magicshotstudio/BestPhoto;->saveBitmapsWithExif([I[Ljava/lang/String;JJJ)I

    move-result v15

    goto :goto_3

    .line 530
    .end local v4    # "best":[Z
    .end local v6    # "contents":[I
    .end local v7    # "saveFiles":[Ljava/lang/String;
    .end local v8    # "pBmp":J
    .end local v10    # "exifBuff":J
    .end local v12    # "exifBuffSize":J
    .end local v14    # "i":I
    .end local v15    # "res":I
    .end local v16    # "saveIndexs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v17    # "savePath":Ljava/lang/String;
    .end local v18    # "size":I
    :cond_6
    return-void
.end method
