.class Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;
.super Ljava/lang/Object;
.source "DeleteOriImageDailog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 83
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->access$200(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 84
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->access$200(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/widget/CheckBox;

    move-result-object v2

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 85
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->access$000(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->access$100(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->access$100(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "TYPE_DELETE_DAILOG_NEED_SHOW"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 87
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog$3;->this$0:Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;->access$100(Lcom/arcsoft/magicshotstudio/ui/common/DeleteOriImageDailog;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 89
    :cond_0
    return-void

    .line 84
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
