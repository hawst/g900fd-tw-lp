.class public Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;
.super Landroid/view/View;
.source "BounceView.java"


# static fields
.field private static final ANIM_SPEED:I = 0xa

.field public static final ARROW_HEIGHT:I = 0x50

.field public static final ARROW_WIDTH:I = 0x50

.field private static final BAR_HEIGHT:I = 0x14

.field private static final BAR_WIDTH:I = 0x50

.field public static final BOUNCE_STYLE_ACC:I = 0xa

.field public static final BOUNCE_STYLE_ACCDEC:I = 0xe

.field public static final BOUNCE_STYLE_BOUNCE:I = 0xb

.field public static final BOUNCE_STYLE_DEC:I = 0xd

.field public static final BOUNCE_STYLE_LINEAR:I = 0xc

.field public static final DIR_ALL:I = 0x0

.field public static final DIR_BOTTOM:I = 0x8

.field public static final DIR_LEFT:I = 0x1

.field public static final DIR_RIGHT:I = 0x4

.field public static final DIR_SHOW_ALL:I = 0x10

.field public static final DIR_TOP:I = 0x2

.field private static final EDGE_WIDTH:I = 0xa

.field private static final GLOW_WIDTH:I = 0xb9

.field public static final MOVE_DOWN:I = -0x1

.field public static final MOVE_UP:I = 0x1

.field public static final NO_MOVE:I = 0x0

.field private static final OFFSET_X:I = 0x64

.field private static final OFFSET_Y:I = 0xa

.field public static final RESET:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ArcSoft_BounceView:"


# instance fields
.field public CurrentDirection:I

.field private bBounceBack:Z

.field public bTouching:Z

.field public curX:F

.field public curY:F

.field private dX:F

.field private dY:F

.field private mAnimCount:I

.field private mArrowDownX:I

.field public mArrowDownY:I

.field private mArrowUpX:I

.field public mArrowUpY:I

.field private mBarX:I

.field private mBarY:I

.field private mBitmapEdgeBottom:Landroid/graphics/drawable/Drawable;

.field private mBitmapEdgeLeft:Landroid/graphics/drawable/Drawable;

.field private mBitmapEdgeRight:Landroid/graphics/drawable/Drawable;

.field private mBitmapEdgeTop:Landroid/graphics/drawable/Drawable;

.field private mBitmapGlowBottom:Landroid/graphics/drawable/Drawable;

.field private mBitmapGlowLeft:Landroid/graphics/drawable/Drawable;

.field private mBitmapGlowRight:Landroid/graphics/drawable/Drawable;

.field private mBitmapGlowTop:Landroid/graphics/drawable/Drawable;

.field private mBounceBackHandler:Landroid/os/Handler;

.field mCallback:Landroid/os/Handler$Callback;

.field private mContext:Landroid/content/Context;

.field public mDown:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mPaint:Landroid/graphics/Paint;

.field private mShadowRect:Landroid/graphics/RectF;

.field public mUp:Z

.field private rtImgRect:Landroid/graphics/Rect;

.field private rtScreenRect:Landroid/graphics/Rect;

.field private screenHeight:I

.field private screenWidth:I

.field public startX:F

.field public startY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 84
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 39
    const/4 v0, 0x5

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    .line 40
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 41
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    .line 42
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bTouching:Z

    .line 43
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mContext:Landroid/content/Context;

    .line 44
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    .line 47
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bBounceBack:Z

    .line 48
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowLeft:Landroid/graphics/drawable/Drawable;

    .line 49
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowTop:Landroid/graphics/drawable/Drawable;

    .line 50
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowRight:Landroid/graphics/drawable/Drawable;

    .line 51
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowBottom:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeLeft:Landroid/graphics/drawable/Drawable;

    .line 53
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeTop:Landroid/graphics/drawable/Drawable;

    .line 54
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeRight:Landroid/graphics/drawable/Drawable;

    .line 55
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeBottom:Landroid/graphics/drawable/Drawable;

    .line 56
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 58
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarX:I

    .line 59
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    .line 60
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpX:I

    .line 61
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    .line 62
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownX:I

    .line 63
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    .line 64
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mUp:Z

    .line 65
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mDown:Z

    .line 80
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    .line 81
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mPaint:Landroid/graphics/Paint;

    .line 119
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    .line 120
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtScreenRect:Landroid/graphics/Rect;

    .line 199
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    .line 546
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mCallback:Landroid/os/Handler$Callback;

    .line 568
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    .line 85
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mContext:Landroid/content/Context;

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/4 v0, 0x5

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    .line 40
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 41
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    .line 42
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bTouching:Z

    .line 43
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mContext:Landroid/content/Context;

    .line 44
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    .line 47
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bBounceBack:Z

    .line 48
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowLeft:Landroid/graphics/drawable/Drawable;

    .line 49
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowTop:Landroid/graphics/drawable/Drawable;

    .line 50
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowRight:Landroid/graphics/drawable/Drawable;

    .line 51
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowBottom:Landroid/graphics/drawable/Drawable;

    .line 52
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeLeft:Landroid/graphics/drawable/Drawable;

    .line 53
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeTop:Landroid/graphics/drawable/Drawable;

    .line 54
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeRight:Landroid/graphics/drawable/Drawable;

    .line 55
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeBottom:Landroid/graphics/drawable/Drawable;

    .line 56
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 58
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarX:I

    .line 59
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    .line 60
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpX:I

    .line 61
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    .line 62
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownX:I

    .line 63
    iput v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    .line 64
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mUp:Z

    .line 65
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mDown:Z

    .line 80
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    .line 81
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mPaint:Landroid/graphics/Paint;

    .line 119
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    .line 120
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtScreenRect:Landroid/graphics/Rect;

    .line 199
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    .line 546
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView$1;-><init>(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mCallback:Landroid/os/Handler$Callback;

    .line 568
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    .line 90
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mContext:Landroid/content/Context;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .prologue
    .line 24
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)I
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .prologue
    .line 24
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)Landroid/view/animation/Interpolator;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .prologue
    .line 24
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dX:F

    return v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)F
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .prologue
    .line 24
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dY:F

    return v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private drawScrollMask(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v12, 0x0

    .line 202
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 203
    .local v1, "dX":F
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 204
    .local v2, "dY":F
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    sub-float/2addr v10, v11

    cmpg-float v10, v10, v12

    if-gtz v10, :cond_1

    move v5, v8

    .line 205
    .local v5, "toLeftOfStart":Z
    :goto_0
    iget v10, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iget v11, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    sub-float/2addr v10, v11

    cmpg-float v10, v10, v12

    if-gtz v10, :cond_2

    move v6, v8

    .line 206
    .local v6, "toTopOfStart":Z
    :goto_1
    const/4 v7, 0x0

    .local v7, "top":F
    const/4 v3, 0x0

    .local v3, "left":F
    const/4 v4, 0x0

    .local v4, "right":F
    const/4 v0, 0x0

    .line 207
    .local v0, "bottom":F
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_3

    if-nez v6, :cond_3

    .line 208
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    add-float v7, v8, v2

    .line 211
    :goto_2
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v8, v8, 0x8

    if-eqz v8, :cond_4

    if-eqz v6, :cond_4

    .line 212
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    sub-float v0, v8, v2

    .line 215
    :goto_3
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_5

    if-nez v5, :cond_5

    .line 216
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    add-float v3, v8, v1

    .line 220
    :goto_4
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v8, v8, 0x4

    if-eqz v8, :cond_6

    if-eqz v5, :cond_6

    .line 221
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    sub-float v4, v8, v1

    .line 225
    :goto_5
    iget v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    const/16 v9, 0x10

    if-ne v8, v9, :cond_0

    .line 226
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    add-float v7, v8, v2

    .line 227
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    sub-float v0, v8, v2

    .line 228
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    add-float v3, v8, v1

    .line 229
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    sub-float v4, v8, v1

    .line 232
    :cond_0
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v3, v7, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 233
    return-void

    .end local v0    # "bottom":F
    .end local v3    # "left":F
    .end local v4    # "right":F
    .end local v5    # "toLeftOfStart":Z
    .end local v6    # "toTopOfStart":Z
    .end local v7    # "top":F
    :cond_1
    move v5, v9

    .line 204
    goto :goto_0

    .restart local v5    # "toLeftOfStart":Z
    :cond_2
    move v6, v9

    .line 205
    goto :goto_1

    .line 210
    .restart local v0    # "bottom":F
    .restart local v3    # "left":F
    .restart local v4    # "right":F
    .restart local v6    # "toTopOfStart":Z
    .restart local v7    # "top":F
    :cond_3
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    int-to-float v7, v8

    goto :goto_2

    .line 214
    :cond_4
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v8

    goto :goto_3

    .line 218
    :cond_5
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    int-to-float v3, v8

    goto :goto_4

    .line 223
    :cond_6
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    int-to-float v4, v8

    goto :goto_5
.end method

.method private initPaint()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 195
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mPaint:Landroid/graphics/Paint;

    .line 196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mPaint:Landroid/graphics/Paint;

    const/16 v1, 0x7f

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 197
    return-void
.end method

.method private setDefaultRect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->updateScreenSize()V

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setRect(Landroid/graphics/Rect;)V

    .line 125
    return-void
.end method

.method private startMyAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 537
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bBounceBack:Z

    .line 538
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dX:F

    .line 539
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dY:F

    .line 540
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    .line 541
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v0, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->postInvalidate()V

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 544
    return-void
.end method


# virtual methods
.method public actionDown(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v1, 0x1

    .line 507
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bTouching:Z

    .line 508
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    .line 509
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 511
    :cond_0
    div-int/lit8 v0, p1, 0x3

    int-to-float v0, v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    .line 512
    div-int/lit8 v0, p2, 0x3

    int-to-float v0, v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    .line 513
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    .line 514
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 515
    return-void
.end method

.method public actionMove(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 518
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 519
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    .line 521
    :cond_0
    return-void
.end method

.method public actionUp()V
    .locals 2

    .prologue
    .line 524
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bTouching:Z

    .line 526
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 530
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->isBouncing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 531
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startMyAnimation()V

    goto :goto_0

    .line 533
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    goto :goto_0
.end method

.method public drawArrows(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "up"    # Landroid/graphics/drawable/Drawable;
    .param p3, "down"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 289
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpX:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpX:I

    add-int/lit8 v2, v2, 0x50

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    add-int/lit8 v3, v3, 0x50

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 291
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownX:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownX:I

    add-int/lit8 v2, v2, 0x50

    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    add-int/lit8 v3, v3, 0x50

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 293
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 294
    invoke-virtual {p3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 295
    return-void
.end method

.method public drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "alpha"    # I
    .param p4, "left"    # I
    .param p5, "top"    # I
    .param p6, "right"    # I
    .param p7, "bottom"    # I

    .prologue
    .line 380
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 381
    invoke-virtual {p2, p4, p5, p6, p7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 382
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 383
    return-void
.end method

.method public drawGlows(Landroid/graphics/Canvas;)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 298
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v14

    .line 299
    .local v14, "dX":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v15

    .line 300
    .local v15, "dY":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    const/16 v16, 0x1

    .line 301
    .local v16, "toLeftOfStart":Z
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_2

    const/16 v17, 0x1

    .line 302
    .local v17, "toTopOfStart":Z
    :goto_1
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v15

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    const/16 v2, 0xff

    if-le v1, v2, :cond_3

    const/16 v4, 0xff

    .line 304
    .local v4, "alphaTopBottom":I
    :goto_2
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v14

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    const/16 v2, 0xff

    if-le v1, v2, :cond_4

    const/16 v13, 0xff

    .line 307
    .local v13, "alphaLeftRight":I
    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowTop:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v5, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v6, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v7, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit16 v8, v1, 0xb9

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 312
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeTop:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v12, v1, 0xa

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowBottom:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v5, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit16 v6, v1, -0xb9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v7, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 319
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeBottom:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v1, -0xa

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 324
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowLeft:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit16 v11, v1, 0xb9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move v8, v13

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 327
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeLeft:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v11, v1, 0xa

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 332
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowRight:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit16 v9, v1, -0xb9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move v8, v13

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 335
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeRight:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v1, -0xa

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 376
    :cond_0
    :goto_4
    return-void

    .line 300
    .end local v4    # "alphaTopBottom":I
    .end local v13    # "alphaLeftRight":I
    .end local v16    # "toLeftOfStart":Z
    .end local v17    # "toTopOfStart":Z
    :cond_1
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 301
    .restart local v16    # "toLeftOfStart":Z
    :cond_2
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 302
    .restart local v17    # "toTopOfStart":Z
    :cond_3
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v15

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v4, v1

    goto/16 :goto_2

    .line 304
    .restart local v4    # "alphaTopBottom":I
    :cond_4
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v14

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v13, v1

    goto/16 :goto_3

    .line 342
    .restart local v13    # "alphaLeftRight":I
    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    if-nez v17, :cond_6

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowTop:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v5, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v6, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v7, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit16 v8, v1, 0xb9

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 346
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeTop:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v12, v1, 0xa

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 350
    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    if-eqz v17, :cond_7

    .line 351
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowBottom:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v5, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit16 v6, v1, -0xb9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v7, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v8, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 354
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeBottom:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v1, -0xa

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 359
    :cond_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_8

    if-nez v16, :cond_8

    .line 360
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowLeft:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit16 v11, v1, 0xb9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move v8, v13

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 363
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeLeft:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v9, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v11, v1, 0xa

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 368
    :cond_8
    move-object/from16 v0, p0

    iget v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_0

    if-eqz v16, :cond_0

    .line 369
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowRight:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit16 v9, v1, -0xb9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move v8, v13

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    .line 372
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeRight:Landroid/graphics/drawable/Drawable;

    const/16 v8, 0xff

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v1, -0xa

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v10, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v11, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v12, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlowOneSide(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIIII)V

    goto/16 :goto_4
.end method

.method public endAllBounce(Landroid/graphics/Rect;Z)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "bInvalidate"    # Z

    .prologue
    .line 461
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v0, :cond_1

    .line 462
    if-nez p2, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    .line 463
    :cond_0
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setRect(Landroid/graphics/Rect;)V

    .line 464
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->hideBounce(I)V

    .line 465
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->invalidate()V

    .line 473
    :cond_1
    :goto_0
    return-void

    .line 467
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->isBouncing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 468
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startMyAnimation()V

    goto :goto_0

    .line 470
    :cond_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->reset()V

    goto :goto_0
.end method

.method public endSingleBounce(Landroid/graphics/Rect;I)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "direction"    # I

    .prologue
    .line 476
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 477
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setRect(Landroid/graphics/Rect;)V

    .line 478
    invoke-virtual {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->hideBounce(I)V

    .line 479
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->invalidate()V

    .line 481
    :cond_0
    return-void
.end method

.method public getIsTouching()Z
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bTouching:Z

    return v0
.end method

.method public hideBounce(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 441
    if-nez p1, :cond_0

    .line 442
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    .line 446
    :goto_0
    return-void

    .line 444
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    xor-int/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    goto :goto_0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 95
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0200b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowLeft:Landroid/graphics/drawable/Drawable;

    .line 97
    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowTop:Landroid/graphics/drawable/Drawable;

    .line 99
    const v1, 0x7f0200b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowRight:Landroid/graphics/drawable/Drawable;

    .line 101
    const v1, 0x7f0200b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapGlowBottom:Landroid/graphics/drawable/Drawable;

    .line 104
    const v1, 0x7f0200af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeLeft:Landroid/graphics/drawable/Drawable;

    .line 106
    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeTop:Landroid/graphics/drawable/Drawable;

    .line 108
    const v1, 0x7f0200b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeRight:Landroid/graphics/drawable/Drawable;

    .line 110
    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBitmapEdgeBottom:Landroid/graphics/drawable/Drawable;

    .line 113
    const/16 v1, 0xe

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setBounceStyle(I)V

    .line 114
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->hideBounce(I)V

    .line 115
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setDefaultRect()V

    .line 116
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->initPaint()V

    .line 117
    return-void
.end method

.method public isBouncing()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "res":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-nez v2, :cond_0

    .line 398
    :goto_0
    return v1

    .line 392
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    float-to-int v3, v3

    if-eq v2, v3, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 398
    goto :goto_0

    :cond_2
    move v0, v1

    .line 392
    goto :goto_1
.end method

.method public isBouncingBack()Z
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bTouching:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bBounceBack:Z

    if-eqz v0, :cond_1

    .line 186
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 187
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawScrollMask(Landroid/graphics/Canvas;)V

    .line 188
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->drawGlows(Landroid/graphics/Canvas;)V

    .line 189
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 191
    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 192
    return-void
.end method

.method public reset()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 155
    iget v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v3, :cond_2

    move v0, v1

    .line 156
    .local v0, "bInvalidate":Z
    :goto_0
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->bBounceBack:Z

    .line 157
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setRect(Landroid/graphics/Rect;)V

    .line 158
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->hideBounce(I)V

    .line 159
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBounceBackHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 160
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    .line 161
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 162
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    .line 163
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    .line 164
    iput v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mAnimCount:I

    .line 165
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dX:F

    .line 166
    iput v4, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->dY:F

    .line 167
    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-eqz v1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->postInvalidate()V

    .line 171
    :cond_0
    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->invalidate()V

    .line 174
    :cond_1
    return-void

    .end local v0    # "bInvalidate":Z
    :cond_2
    move v0, v2

    .line 155
    goto :goto_0
.end method

.method public setBounceStyle(I)V
    .locals 1
    .param p1, "style"    # I

    .prologue
    .line 484
    packed-switch p1, :pswitch_data_0

    .line 501
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 504
    :goto_0
    return-void

    .line 486
    :pswitch_0
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    goto :goto_0

    .line 489
    :pswitch_1
    new-instance v0, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v0}, Landroid/view/animation/BounceInterpolator;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    goto :goto_0

    .line 492
    :pswitch_2
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    goto :goto_0

    .line 495
    :pswitch_3
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    goto :goto_0

    .line 498
    :pswitch_4
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mInterpolator:Landroid/view/animation/Interpolator;

    goto :goto_0

    .line 484
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setBounceViewSize(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 139
    iput p1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    .line 140
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    .line 141
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtScreenRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenWidth:I

    iget v2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 142
    return-void
.end method

.method public setRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    .line 145
    if-nez p1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 152
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtScreenRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    .line 149
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 150
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mShadowRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->rtImgRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public showBounce(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    const/high16 v1, 0x43820000    # 260.0f

    .line 407
    sparse-switch p1, :sswitch_data_0

    .line 438
    :goto_0
    return-void

    .line 409
    :sswitch_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    .line 410
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    .line 435
    :goto_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    .line 437
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->invalidate()V

    goto :goto_0

    .line 413
    :sswitch_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    .line 414
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    goto :goto_1

    .line 417
    :sswitch_2
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 418
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    goto :goto_1

    .line 421
    :sswitch_3
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 422
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    goto :goto_1

    .line 425
    :sswitch_4
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    .line 426
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    .line 428
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startX:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curX:F

    .line 429
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->startY:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->curY:F

    goto :goto_1

    .line 407
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x4 -> :sswitch_1
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method

.method public startAllBounce(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 449
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->CurrentDirection:I

    if-nez v0, :cond_0

    .line 450
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setRect(Landroid/graphics/Rect;)V

    .line 451
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->showBounce(I)V

    .line 453
    :cond_0
    return-void
.end method

.method public startBounce(Landroid/graphics/Rect;I)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "direction"    # I

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->setRect(Landroid/graphics/Rect;)V

    .line 457
    invoke-virtual {p0, p2}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->showBounce(I)V

    .line 458
    return-void
.end method

.method public updateArrowPositions(IIIIZZ)V
    .locals 5
    .param p1, "startPointX"    # I
    .param p2, "startPointY"    # I
    .param p3, "offsety"    # I
    .param p4, "flag"    # I
    .param p5, "zoomoutEnabled"    # Z
    .param p6, "zoominEnabled"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 237
    const-string v0, "ArcSoft_BounceView:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateArrowPositions: startPointX="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startPointY="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " offsety="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    add-int/lit8 v0, p1, -0x64

    add-int/lit8 v0, v0, -0x50

    if-gez v0, :cond_4

    add-int/lit8 v0, p1, 0x64

    add-int/lit8 v0, v0, 0x50

    :goto_0
    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarX:I

    .line 242
    add-int/lit8 v0, p2, -0x50

    add-int/lit8 v0, v0, -0xa

    if-gez v0, :cond_5

    .line 243
    const/16 v0, 0x5a

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    .line 249
    :goto_1
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarX:I

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpX:I

    .line 250
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarX:I

    add-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownX:I

    .line 251
    if-eq p4, v4, :cond_0

    if-ne p4, v3, :cond_c

    .line 252
    :cond_0
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, -0x50

    add-int/lit8 v1, v1, -0xa

    if-ge v0, v1, :cond_7

    .line 253
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    .line 265
    :cond_1
    :goto_2
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, -0x50

    add-int/lit8 v1, v1, -0xa

    if-le v0, v1, :cond_a

    .line 266
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v0, v0, -0x50

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    .line 271
    :cond_2
    :goto_3
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v1, v1, 0x14

    if-ge v0, v1, :cond_b

    .line 272
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, 0x14

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    .line 281
    :cond_3
    :goto_4
    iput-boolean p6, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mUp:Z

    .line 282
    iput-boolean p5, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mDown:Z

    .line 284
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->postInvalidate()V

    .line 285
    return-void

    .line 240
    :cond_4
    add-int/lit8 v0, p1, -0x64

    add-int/lit8 v0, v0, -0x50

    goto :goto_0

    .line 244
    :cond_5
    add-int/lit8 v0, p2, 0x50

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, 0x14

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    if-le v0, v1, :cond_6

    .line 245
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    add-int/lit8 v0, v0, -0x50

    add-int/lit8 v0, v0, -0xa

    add-int/lit8 v0, v0, -0x14

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    goto :goto_1

    .line 247
    :cond_6
    iput p2, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    goto :goto_1

    .line 254
    :cond_7
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v1, v1, 0x14

    if-le v0, v1, :cond_8

    .line 255
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    goto :goto_2

    .line 256
    :cond_8
    if-ne p4, v4, :cond_9

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, -0x50

    add-int/lit8 v1, v1, -0xa

    if-ne v0, v1, :cond_9

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v1, v1, 0x14

    if-ne v0, v1, :cond_9

    .line 259
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    goto :goto_2

    .line 260
    :cond_9
    if-ne p4, v3, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, -0x50

    add-int/lit8 v1, v1, -0xa

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v1, v1, 0x14

    if-ne v0, v1, :cond_1

    .line 263
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    goto/16 :goto_2

    .line 267
    :cond_a
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    if-gez v0, :cond_2

    .line 268
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    goto/16 :goto_3

    .line 273
    :cond_b
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    iget v1, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    add-int/lit8 v1, v1, -0x50

    if-le v0, v1, :cond_3

    .line 274
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->screenHeight:I

    add-int/lit8 v0, v0, -0x50

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    goto/16 :goto_4

    .line 276
    :cond_c
    const/4 v0, 0x2

    if-ne p4, v0, :cond_3

    .line 277
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v0, v0, -0x50

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowUpY:I

    .line 278
    iget v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mBarY:I

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, 0x14

    iput v0, p0, Lcom/arcsoft/magicshotstudio/ui/picmotion/BounceView;->mArrowDownY:I

    goto/16 :goto_4
.end method

.method public updateScreenSize()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method
