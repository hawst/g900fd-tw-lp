.class Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;
.super Ljava/lang/Object;
.source "ArcZoomControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v4, 0x3e800000    # 0.25f

    const/4 v6, 0x0

    const/high16 v3, 0x40200000    # 2.5f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 303
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomInOrOut:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$000(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 304
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 305
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_1

    const/4 v0, 0x1

    .line 306
    .local v0, "isAtRest":Z
    :goto_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v2

    mul-float/2addr v2, v4

    # -= operator for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$124(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 307
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_0

    .line 308
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->zoomWithoutEdgeEffect(FFF)V

    .line 312
    if-eqz v0, :cond_2

    .line 313
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    :goto_1
    return-void

    .line 305
    .end local v0    # "isAtRest":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 315
    .restart local v0    # "isAtRest":Z
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 316
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanX(F)V

    .line 317
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->setPanY(F)V

    .line 318
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->notifyObservers()V

    goto :goto_1

    .line 321
    .end local v0    # "isAtRest":Z
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mState:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$200(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ZoomState;->getZoom()F

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 322
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v1

    const v2, 0x3727c5ac    # 1.0E-5f

    add-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    const/4 v0, 0x1

    .line 323
    .restart local v0    # "isAtRest":Z
    :goto_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v2

    mul-float/2addr v2, v4

    # += operator for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$116(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 324
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    .line 325
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 328
    :cond_4
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$100(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$300(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v3

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$400(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->zoomWithoutEdgeEffect(FFF)V

    .line 329
    if-nez v0, :cond_6

    .line 330
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$600(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # getter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleTapZoomRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$500(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 322
    .end local v0    # "isAtRest":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 332
    .restart local v0    # "isAtRest":Z
    :cond_6
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomFactor:F
    invoke-static {v1, v5}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$102(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 333
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInXPosition:F
    invoke-static {v1, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$302(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    .line 334
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl$1;->this$0:Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;

    # setter for: Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->mDoubleZoomInYPosition:F
    invoke-static {v1, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;->access$402(Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcZoomControl;F)F

    goto/16 :goto_1
.end method
