.class public Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;
.super Landroid/widget/ImageView;
.source "ImageViewWithStroke.java"


# instance fields
.field private mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 14
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 17
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 18
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 19
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 24
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 26
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 27
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 31
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 34
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->mPaint:Landroid/graphics/Paint;

    const v2, -0xc0c0c1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 41
    .local v0, "stroke_rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ImageViewWithStroke;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 42
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 48
    return-void
.end method
