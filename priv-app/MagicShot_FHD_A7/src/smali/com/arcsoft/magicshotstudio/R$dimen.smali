.class public final Lcom/arcsoft/magicshotstudio/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final activity_horizontal_margin:I = 0x7f050000

.field public static final activity_vertical_margin:I = 0x7f050001

.field public static final bestface_circle_width:I = 0x7f050091

.field public static final bestphoto_indicate_line_margin_bottom:I = 0x7f050030

.field public static final bestphoto_page_margin:I = 0x7f050031

.field public static final common_headbar_icon_height:I = 0x7f050028

.field public static final common_headbar_icon_width:I = 0x7f050027

.field public static final common_headbar_margin_left_back:I = 0x7f05002e

.field public static final common_headbar_margin_right:I = 0x7f05002b

.field public static final common_headbar_margin_top:I = 0x7f05002c

.field public static final common_headbar_setting_line_margin_right:I = 0x7f05002f

.field public static final common_headbar_text_margin_top:I = 0x7f05002d

.field public static final common_headbar_widget_height:I = 0x7f05002a

.field public static final common_headbar_widget_width:I = 0x7f050029

.field public static final customed_surface_height:I = 0x7f050003

.field public static final customed_surface_width:I = 0x7f050002

.field public static final delete_dialog_message_lay_height:I = 0x7f050092

.field public static final delete_dialog_message_lay_height_l:I = 0x7f050093

.field public static final drama_edit_bar_width:I = 0x7f05003b

.field public static final drama_edit_button_height:I = 0x7f05003a

.field public static final drama_edit_button_width1:I = 0x7f050038

.field public static final drama_edit_button_width2:I = 0x7f050039

.field public static final drama_frame_height:I = 0x7f050096

.field public static final drama_frame_width:I = 0x7f050095

.field public static final drama_thumbnail_height_l:I = 0x7f050033

.field public static final drama_thumbnail_height_p:I = 0x7f050035

.field public static final drama_thumbnail_item_padding:I = 0x7f050037

.field public static final drama_thumbnail_side_padding:I = 0x7f050036

.field public static final drama_thumbnail_width_l:I = 0x7f050032

.field public static final drama_thumbnail_width_p:I = 0x7f050034

.field public static final edit_bar_margin_bottom_landscape:I = 0x7f05000a

.field public static final head_bar_arrow_margin_left:I = 0x7f050007

.field public static final head_bar_icon_height:I = 0x7f050009

.field public static final head_bar_icon_width:I = 0x7f050008

.field public static final head_bar_margin_top:I = 0x7f050006

.field public static final help_bestfaceimg_height_h:I = 0x7f05005f

.field public static final help_bestfaceimg_height_v:I = 0x7f05005e

.field public static final help_bestfaceimg_margin_left:I = 0x7f050062

.field public static final help_bestfaceimg_margin_top_h:I = 0x7f05005d

.field public static final help_bestfaceimg_margin_top_v:I = 0x7f05005c

.field public static final help_bestfaceimg_width_h:I = 0x7f050061

.field public static final help_bestfaceimg_width_v:I = 0x7f050060

.field public static final help_bestfacetxt_margin_top_h:I = 0x7f050064

.field public static final help_bestfacetxt_margin_top_v:I = 0x7f050063

.field public static final help_bestfacetxt_width_h:I = 0x7f050066

.field public static final help_bestfacetxt_width_v:I = 0x7f050065

.field public static final help_bestphotoimg_height_h:I = 0x7f050054

.field public static final help_bestphotoimg_height_v:I = 0x7f050053

.field public static final help_bestphotoimg_margin_left:I = 0x7f050057

.field public static final help_bestphotoimg_margin_top_h:I = 0x7f050052

.field public static final help_bestphotoimg_margin_top_v:I = 0x7f050051

.field public static final help_bestphotoimg_width_h:I = 0x7f050056

.field public static final help_bestphotoimg_width_v:I = 0x7f050055

.field public static final help_bestphototxt_margin_top_h:I = 0x7f050059

.field public static final help_bestphototxt_margin_top_v:I = 0x7f050058

.field public static final help_bestphototxt_width_h:I = 0x7f05005b

.field public static final help_bestphototxt_width_v:I = 0x7f05005a

.field public static final help_dramaimg_height_h:I = 0x7f050068

.field public static final help_dramaimg_height_v:I = 0x7f050067

.field public static final help_dramaimg_width:I = 0x7f050069

.field public static final help_dramatxt_margin_top_h:I = 0x7f05006b

.field public static final help_dramatxt_margin_top_v:I = 0x7f05006a

.field public static final help_dramatxt_width_h:I = 0x7f05006d

.field public static final help_dramatxt_width_v:I = 0x7f05006c

.field public static final help_eraserimg_height_h:I = 0x7f050073

.field public static final help_eraserimg_height_v:I = 0x7f050072

.field public static final help_eraserimg_margin_left:I = 0x7f050074

.field public static final help_eraserimg_margin_top_h:I = 0x7f05006f

.field public static final help_eraserimg_margin_top_v:I = 0x7f05006e

.field public static final help_eraserimg_width_h:I = 0x7f050071

.field public static final help_eraserimg_width_v:I = 0x7f050070

.field public static final help_erasertxt_margin_top_h:I = 0x7f050076

.field public static final help_erasertxt_margin_top_v:I = 0x7f050075

.field public static final help_erasertxt_width_h:I = 0x7f050078

.field public static final help_erasertxt_width_v:I = 0x7f050077

.field public static final help_okbutton_margin_bottom:I = 0x7f05004e

.field public static final help_okbutton_margin_right:I = 0x7f05004f

.field public static final help_okbutton_margin_top:I = 0x7f050050

.field public static final help_picmotionblurimg_height_h:I = 0x7f05007d

.field public static final help_picmotionblurimg_height_v:I = 0x7f05007c

.field public static final help_picmotionblurimg_left_margin:I = 0x7f05007b

.field public static final help_picmotionblurimg_margin_top_h:I = 0x7f05007a

.field public static final help_picmotionblurimg_margin_top_v:I = 0x7f050079

.field public static final help_picmotionblurimg_width_h:I = 0x7f05007f

.field public static final help_picmotionblurimg_width_v:I = 0x7f05007e

.field public static final help_picmotionblurtxt_margin_top_h:I = 0x7f050081

.field public static final help_picmotionblurtxt_margin_top_v:I = 0x7f050080

.field public static final help_picmotionblurtxt_width_h:I = 0x7f050083

.field public static final help_picmotionblurtxt_width_v:I = 0x7f050082

.field public static final help_picmotionobjectimg_height_h:I = 0x7f050085

.field public static final help_picmotionobjectimg_height_v:I = 0x7f050084

.field public static final help_picmotionobjectimg_width:I = 0x7f050086

.field public static final help_picmotionobjecttxt_margin_top_h:I = 0x7f050088

.field public static final help_picmotionobjecttxt_margin_top_v:I = 0x7f050087

.field public static final help_picmotionobjecttxt_width_h:I = 0x7f05008a

.field public static final help_picmotionobjecttxt_width_v:I = 0x7f050089

.field public static final help_text_line_height:I = 0x7f05008b

.field public static final image_bottom_margin:I = 0x7f050017

.field public static final image_side_margin:I = 0x7f050018

.field public static final image_subtext_size:I = 0x7f050015

.field public static final image_text_size:I = 0x7f050014

.field public static final image_top_margin:I = 0x7f050016

.field public static final main_bottom_l_width:I = 0x7f05001b

.field public static final main_bottom_p_width:I = 0x7f05001c

.field public static final main_bottom_padding_bottom:I = 0x7f05001d

.field public static final main_bottom_padding_left:I = 0x7f05001e

.field public static final main_bottom_padding_right:I = 0x7f05001f

.field public static final main_bottom_text_height:I = 0x7f05001a

.field public static final main_drawer_icon_left:I = 0x7f050045

.field public static final main_drawer_icon_size:I = 0x7f050042

.field public static final main_drawer_icon_top_bottom:I = 0x7f050044

.field public static final main_drawer_item_height:I = 0x7f050043

.field public static final main_drawer_text_left:I = 0x7f050046

.field public static final main_drawer_text_size:I = 0x7f050041

.field public static final main_drawer_width:I = 0x7f050040

.field public static final main_front_mode_l_side_margin:I = 0x7f050025

.field public static final main_front_mode_l_width:I = 0x7f050023

.field public static final main_front_mode_p_side_margin:I = 0x7f050026

.field public static final main_front_mode_p_width:I = 0x7f050024

.field public static final main_list_icon_size:I = 0x7f050048

.field public static final main_list_item_height:I = 0x7f050049

.field public static final main_list_left_padding:I = 0x7f05004a

.field public static final main_list_text_height:I = 0x7f05004b

.field public static final main_mode_l_width:I = 0x7f050020

.field public static final main_mode_p_width:I = 0x7f050021

.field public static final main_mode_side_margin:I = 0x7f050022

.field public static final main_more_list_width:I = 0x7f050047

.field public static final main_selection_image_padding:I = 0x7f05004d

.field public static final main_selection_image_size:I = 0x7f05004c

.field public static final main_title_text_size:I = 0x7f050019

.field public static final picmotion_edit_bar_width:I = 0x7f05003d

.field public static final picmotion_edit_button_height:I = 0x7f05003c

.field public static final picmotion_thumbnail_item_padding:I = 0x7f05003f

.field public static final picmotion_thumbnail_side_padding:I = 0x7f05003e

.field public static final progress_text_title_size:I = 0x7f05000c

.field public static final right_bar_capture_margin_top:I = 0x7f050005

.field public static final right_bar_setting_margin_top:I = 0x7f050004

.field public static final setting_item_margin_left:I = 0x7f050012

.field public static final setting_item_margin_right:I = 0x7f050013

.field public static final setting_padding:I = 0x7f05000e

.field public static final setting_title_margin_left:I = 0x7f050011

.field public static final share_application_text_size:I = 0x7f05008d

.field public static final share_item_icon_size:I = 0x7f050090

.field public static final share_item_text_size:I = 0x7f05008f

.field public static final share_tab_text_margin_bottom:I = 0x7f05008c

.field public static final share_via_text_size:I = 0x7f05008e

.field public static final text_nomarl_size:I = 0x7f05000d

.field public static final text_title_size:I = 0x7f05000b

.field public static final toast_edge_padding:I = 0x7f050094

.field public static final waiting_progress_margin:I = 0x7f05000f

.field public static final waiting_progress_title_size:I = 0x7f050010


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
