.class public final Lcom/arcsoft/magicshotstudio/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f060000

.field public static final back_btn:I = 0x7f060027

.field public static final best_face:I = 0x7f06001b

.field public static final best_photo:I = 0x7f06001a

.field public static final bestface_description:I = 0x7f06002b

.field public static final bestgroup_title:I = 0x7f060028

.field public static final common_exit_dialog_message:I = 0x7f06002e

.field public static final common_exit_dialog_title:I = 0x7f06002d

.field public static final common_no_moving_object:I = 0x7f06002c

.field public static final del_mask_fuse:I = 0x7f060004

.field public static final discard_image_popup:I = 0x7f06004c

.field public static final discard_image_popup_header:I = 0x7f06004d

.field public static final do_not_show_again:I = 0x7f060047

.field public static final drama_shot:I = 0x7f06001c

.field public static final edit_object:I = 0x7f060003

.field public static final eraser:I = 0x7f06001d

.field public static final face_detect_dialog_message:I = 0x7f06002a

.field public static final help_best_drama:I = 0x7f060040

.field public static final help_best_face:I = 0x7f06003f

.field public static final help_best_photo:I = 0x7f060043

.field public static final help_eraser:I = 0x7f060044

.field public static final help_guide_text_best_face1:I = 0x7f060031

.field public static final help_guide_text_best_face2:I = 0x7f060032

.field public static final help_guide_text_best_face3:I = 0x7f060033

.field public static final help_guide_text_best_face4:I = 0x7f060034

.field public static final help_guide_text_best_photo1:I = 0x7f060039

.field public static final help_guide_text_best_photo2:I = 0x7f06003a

.field public static final help_guide_text_drama1:I = 0x7f060035

.field public static final help_guide_text_drama2:I = 0x7f060036

.field public static final help_guide_text_eraser1:I = 0x7f060037

.field public static final help_guide_text_eraser2:I = 0x7f060038

.field public static final help_guide_text_picmotion1:I = 0x7f06003b

.field public static final help_guide_text_picmotion2:I = 0x7f06003c

.field public static final help_guide_text_picmotion3:I = 0x7f06003d

.field public static final help_mask_eraser:I = 0x7f060045

.field public static final help_picmotion:I = 0x7f060042

.field public static final help_picmotion2:I = 0x7f060041

.field public static final help_picmotion3:I = 0x7f06004b

.field public static final hide_shot:I = 0x7f060006

.field public static final icon_for_moving_objects:I = 0x7f060025

.field public static final main_not_sef_file:I = 0x7f06002f

.field public static final main_sef_not_support:I = 0x7f060030

.field public static final mask_fuse:I = 0x7f060002

.field public static final modified_pop_up_fail_to_detection:I = 0x7f060023

.field public static final multi_save_toast:I = 0x7f06004a

.field public static final new_bestgroup_title:I = 0x7f060029

.field public static final new_fail_erase:I = 0x7f060024

.field public static final new_mask_remove_guide:I = 0x7f060049

.field public static final new_mask_restore_guide:I = 0x7f060048

.field public static final not_enough_sapce_in_app:I = 0x7f060057

.field public static final not_enough_space:I = 0x7f060056

.field public static final original_image:I = 0x7f060046

.field public static final original_image_popup_header:I = 0x7f060050

.field public static final pic_motion:I = 0x7f06001e

.field public static final pic_motion_motion_blur:I = 0x7f060020

.field public static final pic_motion_object:I = 0x7f06001f

.field public static final private_save_toast:I = 0x7f060053

.field public static final result_image_ok:I = 0x7f060001

.field public static final save_changes_popup:I = 0x7f06004e

.field public static final save_changes_popup_header:I = 0x7f06004f

.field public static final show_shot:I = 0x7f060005

.field public static final stms_appgroup:I = 0x7f060058

.field public static final text_angle:I = 0x7f060017

.field public static final text_applicactions:I = 0x7f060019

.field public static final text_best_off:I = 0x7f060052

.field public static final text_best_on:I = 0x7f060051

.field public static final text_cancel:I = 0x7f06000e

.field public static final text_check:I = 0x7f060054

.field public static final text_decoding:I = 0x7f060016

.field public static final text_disabled:I = 0x7f060012

.field public static final text_done:I = 0x7f06000f

.field public static final text_edit:I = 0x7f06000b

.field public static final text_for_restore_in_preview:I = 0x7f060022

.field public static final text_help:I = 0x7f060013

.field public static final text_not_selected:I = 0x7f060011

.field public static final text_ok:I = 0x7f060014

.field public static final text_progressing:I = 0x7f060015

.field public static final text_redo:I = 0x7f06000a

.field public static final text_remove:I = 0x7f060007

.field public static final text_restore:I = 0x7f060008

.field public static final text_save:I = 0x7f06000c

.field public static final text_selected:I = 0x7f060010

.field public static final text_share:I = 0x7f06000d

.field public static final text_share_via:I = 0x7f060018

.field public static final text_success_to_detection:I = 0x7f060021

.field public static final text_uncheck:I = 0x7f060055

.field public static final text_undo:I = 0x7f060009

.field public static final toast_save_to_studio_folder:I = 0x7f06003e

.field public static final toggle_icon_for_moving_objects:I = 0x7f060026


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
