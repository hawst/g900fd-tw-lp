.class public final Lcom/arcsoft/magicshotstudio/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final CANCEL_LAYOUT:I = 0x7f09007d

.field public static final DONE_LAYOUT:I = 0x7f09007f

.field public static final DRAMA_BRUSH_ERASER_LAYOUT:I = 0x7f090085

.field public static final DRAMA_IB_BRUSH_ERASER:I = 0x7f090086

.field public static final DRAMA_IB_BRUSH_ERASER_SELECTOR:I = 0x7f090087

.field public static final DRAMA_IB_RESTORE:I = 0x7f090082

.field public static final DRAMA_IB_RESTORE_SELECTOR:I = 0x7f090083

.field public static final DRAMA_RESTORE_LAYOUT:I = 0x7f090081

.field public static final EDIT_BAR_DIVIDER_LINE:I = 0x7f09007c

.field public static final IB_REDO_ERASER:I = 0x7f09008d

.field public static final IB_SELECTOR:I = 0x7f090090

.field public static final IB_UNDO_ERASER:I = 0x7f09008a

.field public static final ImageView_back_arrow_view:I = 0x7f090043

.field public static final ImageView_cancel:I = 0x7f09004f

.field public static final ImageView_menu:I = 0x7f09005c

.field public static final ImageView_save:I = 0x7f090055

.field public static final PICMOTION_ANGLE:I = 0x7f0900db

.field public static final PICMOTION_ANGLE_NUMBER:I = 0x7f0900dc

.field public static final PICMOTION_BRUSH_ERASER_LAYOUT:I = 0x7f0900d3

.field public static final PICMOTION_IB_BRUSH_ERASER:I = 0x7f0900d4

.field public static final PICMOTION_IB_BRUSH_ERASER_SELECTOR:I = 0x7f0900d5

.field public static final PICMOTION_IB_RESTORE:I = 0x7f0900d0

.field public static final PICMOTION_IB_RESTORE_SELECTOR:I = 0x7f0900d1

.field public static final PICMOTION_MOTION_BLUR_ANGLE_IMGVIEW:I = 0x7f0900dd

.field public static final PICMOTION_RESTORE_LAYOUT:I = 0x7f0900cf

.field public static final ProgressBar01:I = 0x7f0900e5

.field public static final REDO_LAYOUT:I = 0x7f09008c

.field public static final TV_CANCEL:I = 0x7f09007e

.field public static final TV_DONE:I = 0x7f090080

.field public static final UNDO_LAYOUT:I = 0x7f090089

.field public static final action_bar_back_img:I = 0x7f0900b1

.field public static final action_bar_back_layout:I = 0x7f0900b0

.field public static final action_bar_layout:I = 0x7f0900ae

.field public static final action_drawer:I = 0x7f0900af

.field public static final actionbar:I = 0x7f090026

.field public static final actionbar_Relativelayout:I = 0x7f090067

.field public static final actionbar_brush_eraser_layout:I = 0x7f09007b

.field public static final actionbar_cancel_relativelayout:I = 0x7f09000f

.field public static final actionbar_cancelimage:I = 0x7f090019

.field public static final actionbar_picmotion_blur_layout:I = 0x7f0900ce

.field public static final actionbar_save_relativelayout:I = 0x7f090010

.field public static final actionbar_saveimage:I = 0x7f090013

.field public static final back_Layout:I = 0x7f09000a

.field public static final back_arrow_Layout:I = 0x7f090042

.field public static final back_button:I = 0x7f09000d

.field public static final back_layout_gap_1:I = 0x7f09004e

.field public static final back_layout_gap_2:I = 0x7f090050

.field public static final back_layout_gap_3:I = 0x7f090052

.field public static final back_relativelayout:I = 0x7f09000c

.field public static final best_face_img_layout:I = 0x7f0900a9

.field public static final best_face_imgview:I = 0x7f0900b4

.field public static final best_face_title_textview:I = 0x7f09000e

.field public static final best_face_txt:I = 0x7f0900b6

.field public static final best_photo_besticon_layout:I = 0x7f090046

.field public static final best_photo_img_layout:I = 0x7f0900a8

.field public static final best_photo_imgview:I = 0x7f0900b7

.field public static final best_photo_txt:I = 0x7f0900b9

.field public static final besticon_imageview:I = 0x7f090048

.field public static final besticon_layout_gap_1:I = 0x7f090047

.field public static final besticon_layout_gap_2:I = 0x7f090049

.field public static final besticon_layout_gap_3:I = 0x7f09004b

.field public static final bevel:I = 0x7f090000

.field public static final black_layer:I = 0x7f090025

.field public static final bottom:I = 0x7f090003

.field public static final cancel_gap_1:I = 0x7f090018

.field public static final cancel_gap_2:I = 0x7f09001a

.field public static final cancel_gap_3:I = 0x7f09001c

.field public static final cancel_layout:I = 0x7f09004c

.field public static final check_box:I = 0x7f090063

.field public static final clockwise:I = 0x7f090007

.field public static final common_headbar_layout:I = 0x7f090041

.field public static final common_headbar_title_textview:I = 0x7f090044

.field public static final counterclockwise:I = 0x7f090008

.field public static final del_dailog_textview:I = 0x7f090064

.field public static final description_piclear:I = 0x7f090091

.field public static final description_select_frame:I = 0x7f09006e

.field public static final description_select_frame_gap:I = 0x7f090073

.field public static final dialog_buttons:I = 0x7f0900a0

.field public static final dialog_cancel:I = 0x7f0900a1

.field public static final dialog_check_panel:I = 0x7f09009f

.field public static final dialog_message:I = 0x7f090062

.field public static final dialog_message_panel:I = 0x7f090060

.field public static final dialog_message_scrollview:I = 0x7f090061

.field public static final dialog_no:I = 0x7f0900a2

.field public static final dialog_panel:I = 0x7f09009c

.field public static final dialog_title:I = 0x7f09009e

.field public static final dialog_title_panel:I = 0x7f09009d

.field public static final dialog_yes:I = 0x7f0900a3

.field public static final dim_back_layout:I = 0x7f0900ad

.field public static final down_arrows:I = 0x7f090072

.field public static final down_arrows_layout:I = 0x7f090071

.field public static final drama_edit_bar_divider1:I = 0x7f090084

.field public static final drama_edit_bar_divider2:I = 0x7f090088

.field public static final drama_edit_bar_divider3:I = 0x7f09008b

.field public static final drama_edit_bar_layout:I = 0x7f090070

.field public static final drama_shot_img_layout:I = 0x7f0900aa

.field public static final drama_shot_imgview:I = 0x7f0900ba

.field public static final drama_shot_txt:I = 0x7f0900bc

.field public static final drama_title_textview:I = 0x7f090074

.field public static final edgeview:I = 0x7f090024

.field public static final edit_imageview:I = 0x7f090077

.field public static final edit_layout:I = 0x7f090075

.field public static final edit_layout_gap_1:I = 0x7f090076

.field public static final edit_layout_gap_2:I = 0x7f090078

.field public static final edit_layout_gap_3:I = 0x7f09007a

.field public static final editbar_bottomspace:I = 0x7f090068

.field public static final eraser_img_layout:I = 0x7f0900ab

.field public static final eraser_imgview:I = 0x7f0900bd

.field public static final eraser_txt:I = 0x7f0900bf

.field public static final exit_dialog_cancel_btn:I = 0x7f09003e

.field public static final exit_dialog_discard_btn:I = 0x7f09003f

.field public static final exit_dialog_save_btn:I = 0x7f090040

.field public static final faceImage:I = 0x7f09001e

.field public static final faceImageItem:I = 0x7f09001f

.field public static final faceRelative:I = 0x7f09001d

.field public static final facescroll:I = 0x7f090027

.field public static final first_image:I = 0x7f090023

.field public static final frame_bottom_gap:I = 0x7f09006d

.field public static final frame_image:I = 0x7f09008f

.field public static final frame_item:I = 0x7f09008e

.field public static final frame_lineraLayout:I = 0x7f09006b

.field public static final frameviewP:I = 0x7f09006c

.field public static final goodPose:I = 0x7f090020

.field public static final help_imgview:I = 0x7f090098

.field public static final help_ok_imageview:I = 0x7f09009b

.field public static final help_ok_layout:I = 0x7f09009a

.field public static final help_text:I = 0x7f090099

.field public static final imageview:I = 0x7f09003c

.field public static final img_page_layout:I = 0x7f090039

.field public static final imgview_layout:I = 0x7f0900b3

.field public static final indicate_layout:I = 0x7f09003a

.field public static final indicate_line:I = 0x7f09003b

.field public static final largePhoto:I = 0x7f090022

.field public static final left:I = 0x7f090004

.field public static final main_Layout:I = 0x7f090065

.field public static final main_Relativelayout:I = 0x7f090009

.field public static final main_background:I = 0x7f0900a4

.field public static final main_background_image:I = 0x7f0900a5

.field public static final main_best_face_grap_layout:I = 0x7f0900b5

.field public static final main_best_photo_grap_layout:I = 0x7f0900b8

.field public static final main_drama_grap_layout:I = 0x7f0900bb

.field public static final main_eraser_grap_layout:I = 0x7f0900be

.field public static final main_list_layout:I = 0x7f0900a7

.field public static final main_magicshot_icon:I = 0x7f0900a6

.field public static final main_more_list_text:I = 0x7f0900c3

.field public static final main_pic_motion_grap_layout:I = 0x7f0900c1

.field public static final main_relativelayout:I = 0x7f090021

.field public static final main_title_textview:I = 0x7f0900b2

.field public static final manual_description_operation_mask_layout:I = 0x7f090069

.field public static final manual_description_operation_mask_textview:I = 0x7f09006a

.field public static final manual_fail_text_layout:I = 0x7f090093

.field public static final manual_fail_textview:I = 0x7f090094

.field public static final menu_layout:I = 0x7f090059

.field public static final menu_layout_gap_1:I = 0x7f09005b

.field public static final menu_layout_gap_2:I = 0x7f09005d

.field public static final menu_layout_gap_3:I = 0x7f09005f

.field public static final mergetime:I = 0x7f090037

.field public static final mid_gap:I = 0x7f09000b

.field public static final miter:I = 0x7f090001

.field public static final over_scroll:I = 0x7f090066

.field public static final pic_motion_blur_txt:I = 0x7f0900df

.field public static final pic_motion_img_layout:I = 0x7f0900ac

.field public static final pic_motion_imgview:I = 0x7f0900c0

.field public static final pic_motion_motion_blur_imgview:I = 0x7f0900de

.field public static final pic_motion_object_imgview:I = 0x7f0900e0

.field public static final pic_motion_object_txt:I = 0x7f0900e1

.field public static final pic_motion_txt:I = 0x7f0900c2

.field public static final picbest:I = 0x7f090028

.field public static final picbest_fd:I = 0x7f090032

.field public static final picbest_get_result_images:I = 0x7f090034

.field public static final picbest_get_result_info:I = 0x7f090035

.field public static final picbest_init_memory:I = 0x7f090031

.field public static final picbest_process_frames:I = 0x7f090033

.field public static final picmotion_blur_angle_img_layout:I = 0x7f0900ca

.field public static final picmotion_blur_edit_img_layout:I = 0x7f0900cb

.field public static final picmotion_blur_list_layout:I = 0x7f0900c5

.field public static final picmotion_edit_bar_divider1:I = 0x7f0900d2

.field public static final picmotion_edit_bar_divider2:I = 0x7f0900d6

.field public static final picmotion_edit_bar_divider3:I = 0x7f0900d7

.field public static final picmotion_edit_bar_layout:I = 0x7f0900c4

.field public static final picmotion_edit_motion_blur_layout:I = 0x7f0900d8

.field public static final picmotion_motion_blur_bar_layout:I = 0x7f0900da

.field public static final picmotion_motion_blur_img_layout:I = 0x7f0900cd

.field public static final picmotion_obiect_img_layout:I = 0x7f0900cc

.field public static final picmotion_seek_bar:I = 0x7f0900c7

.field public static final picmotion_seek_bar_layout:I = 0x7f0900c6

.field public static final picmotion_seek_bar_text:I = 0x7f0900c8

.field public static final picmotion_seek_bar_value:I = 0x7f0900c9

.field public static final processtime:I = 0x7f090036

.field public static final progress_bar:I = 0x7f0900d9

.field public static final progressbar:I = 0x7f090097

.field public static final progressing_text:I = 0x7f090096

.field public static final progressingdlg:I = 0x7f090095

.field public static final progresslineralayout:I = 0x7f09003d

.field public static final right:I = 0x7f090005

.field public static final right_layout:I = 0x7f090045

.field public static final round:I = 0x7f090002

.field public static final save_gap_1:I = 0x7f090012

.field public static final save_gap_2:I = 0x7f090014

.field public static final save_gap_3:I = 0x7f090016

.field public static final save_layout:I = 0x7f090053

.field public static final save_layout_gap_1:I = 0x7f090054

.field public static final save_layout_gap_2:I = 0x7f090056

.field public static final save_layout_gap_3:I = 0x7f090058

.field public static final savetime:I = 0x7f090038

.field public static final select:I = 0x7f0900e6

.field public static final select_photo:I = 0x7f09002f

.field public static final select_photo_algorithm_init:I = 0x7f09002c

.field public static final select_photo_algorithm_process:I = 0x7f09002d

.field public static final select_photo_algorithm_uninit:I = 0x7f09002e

.field public static final select_photo_face_detect:I = 0x7f09002b

.field public static final select_photo_init_memory:I = 0x7f09002a

.field public static final setting_3_line:I = 0x7f09004d

.field public static final setting_first_line:I = 0x7f090011

.field public static final setting_second_line:I = 0x7f090017

.field public static final setting_third_line:I = 0x7f09005a

.field public static final showtime:I = 0x7f090029

.field public static final text_view_description_piclear:I = 0x7f090092

.field public static final text_view_description_select_frame:I = 0x7f09006f

.field public static final text_view_face_number:I = 0x7f090030

.field public static final textview_back:I = 0x7f090051

.field public static final textview_besticon:I = 0x7f09004a

.field public static final textview_cancel:I = 0x7f09001b

.field public static final textview_done:I = 0x7f090015

.field public static final textview_edit:I = 0x7f090079

.field public static final textview_menu:I = 0x7f09005e

.field public static final textview_save:I = 0x7f090057

.field public static final toast_textview:I = 0x7f0900e2

.field public static final top:I = 0x7f090006

.field public static final waiting_progress_bar:I = 0x7f0900e3

.field public static final watingview_layout:I = 0x7f0900e4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
