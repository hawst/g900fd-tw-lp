.class public Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;
.super Ljava/lang/Object;
.source "PicBest_NativeJni.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;,
        Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;,
        Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;,
        Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_PicBest_NativeJni"


# instance fields
.field private mEngine:I

.field private mFirstImageHeight:I

.field private mFirstImageWidth:I

.field private mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

.field private mPtrInJni:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 171
    :try_start_0
    const-string v1, "arcsoft_magicshot_bestface_T"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 172
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 173
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mPtrInJni:I

    .line 13
    iput v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    .line 16
    iput v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mFirstImageWidth:I

    .line 17
    iput v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mFirstImageHeight:I

    .line 20
    const-string v0, "ArcSoft_BestFace_PicBest_NativeJni"

    const-string v1, "PicBest_NativeJni Contructor!"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static doImageScore([Ljava/lang/String;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;)I
    .locals 1
    .param p0, "pathArray"    # [Ljava/lang/String;
    .param p1, "outScores"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;

    .prologue
    .line 124
    invoke-static {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_ImageScore([Ljava/lang/String;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;)I

    move-result v0

    return v0
.end method

.method public static doImageScoreWithRawData(JJILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;Z)I
    .locals 2
    .param p0, "bitmapPtr"    # J
    .param p2, "preData"    # J
    .param p4, "length"    # I
    .param p5, "outScores"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;
    .param p6, "selectData"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;
    .param p7, "enableData"    # Z

    .prologue
    .line 129
    invoke-static/range {p0 .. p7}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_ImageScoreWithRawData(JJILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;Z)I

    move-result v0

    return v0
.end method

.method public static native native_ImageScore([Ljava/lang/String;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;)I
.end method

.method public static native native_ImageScoreWithRawData(JJILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$ImagesScore;Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$SFPerformanceData;Z)I
.end method


# virtual methods
.method public SetRawDataAddress(JJ[IJ)I
    .locals 9
    .param p1, "bitmapPtr"    # J
    .param p3, "preData"    # J
    .param p5, "bestIndex"    # [I
    .param p6, "scale"    # J

    .prologue
    .line 47
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-wide v7, p6

    invoke-virtual/range {v0 .. v8}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_SetRawDataAddress(IJJ[IJ)I

    move-result v0

    return v0
.end method

.method public applyMerge(IILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I
    .locals 1
    .param p1, "personId"    # I
    .param p2, "faceIndex"    # I
    .param p3, "out"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    .prologue
    .line 55
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_ApplyMerge(IIILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I

    move-result v0

    return v0
.end method

.method public applyMultiMerge([I[ILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I
    .locals 1
    .param p1, "personIds"    # [I
    .param p2, "faceIds"    # [I
    .param p3, "out"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;

    .prologue
    .line 72
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_ApplyMultiMerge(I[I[ILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I

    move-result v0

    return v0
.end method

.method public getResultFaceInfo()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_GetResultFaceInfo(I)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v0

    return-object v0
.end method

.method public getmFirstImageHeight()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mFirstImageHeight:I

    return v0
.end method

.method public getmFirstImageWidth()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mFirstImageWidth:I

    return v0
.end method

.method public handleMsg(IIILjava/lang/Object;)V
    .locals 3
    .param p1, "msgType"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 84
    const-string v0, "ArcSoft_BestFace_PicBest_NativeJni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMsg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    packed-switch p1, :pswitch_data_0

    .line 96
    .end local p4    # "obj":Ljava/lang/Object;
    :goto_0
    :pswitch_0
    return-void

    .line 87
    .restart local p4    # "obj":Ljava/lang/Object;
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    invoke-interface {v0, p2}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;->onProcessUpdateListener(I)V

    goto :goto_0

    .line 90
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    check-cast p4, Landroid/graphics/Bitmap;

    .end local p4    # "obj":Ljava/lang/Object;
    invoke-interface {v0, p4}, Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;->onShowFirstImageListener(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public init(IIIIIIZLcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;)I
    .locals 14
    .param p1, "ImageWidth"    # I
    .param p2, "ImageHeight"    # I
    .param p3, "DisplayWidth"    # I
    .param p4, "ThumbWidth"    # I
    .param p5, "Count"    # I
    .param p6, "degree"    # I
    .param p7, "enablePerformance"    # Z
    .param p8, "performanceData"    # Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;

    .prologue
    .line 38
    sget-object v7, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->DELETE_EXIF_FIELDS:[I

    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->DELETE_EXIF_FIELDS:[I

    array-length v8, v0

    sget-object v9, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->MODIFY_EXIF_FIELDS:[I

    sget-object v10, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->MODIFIED_EXIF_NUM:[I

    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->MODIFY_EXIF_FIELDS:[I

    array-length v11, v0

    move-object v0, p0

    move v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v12, p7

    move-object/from16 v13, p8

    invoke-virtual/range {v0 .. v13}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_Init(IIIIII[II[I[IIZLcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    .line 43
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    return v0
.end method

.method public loadExifInfo(JJ)I
    .locals 7
    .param p1, "exifBuff"    # J
    .param p3, "exifBuffSize"    # J

    .prologue
    .line 80
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_SetExifInfo(IJJ)I

    move-result v0

    return v0
.end method

.method public native native_ApplyMerge(IIILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I
.end method

.method public native native_ApplyMultiMerge(I[I[ILcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$OutBitmap;)I
.end method

.method public native native_GetResultFaceInfo(I)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
.end method

.method public native native_Init(IIIIII[II[I[IIZLcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni$BFPerformanceData;)I
.end method

.method public native native_Process(I)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
.end method

.method public native native_SaveResultImage(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public native native_SetDebugable(II)V
.end method

.method public native native_SetExifInfo(IJJ)I
.end method

.method public native native_SetRawDataAddress(IJJ[IJ)I
.end method

.method public native native_UnInit(I)V
.end method

.method public process()Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_Process(I)Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;

    move-result-object v0

    return-object v0
.end method

.method public saveResultImage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "extres"    # Ljava/lang/String;

    .prologue
    .line 76
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_SaveResultImage(ILjava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public setDebugable(I)V
    .locals 1
    .param p1, "enable"    # I

    .prologue
    .line 68
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0, p1}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_SetDebugable(II)V

    .line 69
    return-void
.end method

.method public setProcessBarCallBack(Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mProcessUpdataListener:Lcom/arcsoft/magicshotstudio/ui/bestface/PicturesBestEngine$JniCallBackListener;

    .line 33
    return-void
.end method

.method public uninit()V
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->native_UnInit(I)V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_NativeJni;->mEngine:I

    .line 65
    return-void
.end method
