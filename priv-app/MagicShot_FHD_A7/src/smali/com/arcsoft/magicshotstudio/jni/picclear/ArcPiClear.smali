.class public Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;
.super Ljava/lang/Object;
.source "ArcPiClear.java"


# static fields
.field public static final ARCPICLEAR_AUTO_MODE:I = 0x0

.field public static final ARCPICLEAR_MANUAL_MODE:I = 0x1

.field public static final ARCPICLEAR_MANUAL_REDO_MASK:I = 0x1

.field public static final ARCPICLEAR_MANUAL_UNDO_MASK:I = 0x0

.field public static final HANDLER_PROGRESS_INDEX:I = 0x6

.field public static final MOR_BLANK:I = 0x7000

.field public static final MOR_NO_OBJECT:I = 0x7000

.field public static final MOR_NO_OBJECTS:I = 0x8000

.field public static final MOR_OBJECTS_OFFFRAME:I = 0x8001

.field public static final MOR_OBJECTS_REMAIN:I = 0x8003

.field public static final MOR_OBJECTS_REMOVE:I = 0x8002

.field public static final MOR_RECOVER:I = 0x7002

.field public static final MOR_REMOVE:I = 0x7001

.field private static final TAG:Ljava/lang/String; = "ARCJNI_PICLEAR"


# instance fields
.field private mArcPiclearEngine:I

.field private mMarkImageHeight:I

.field private mMarkImageWidth:I

.field private mMarkRects:Ljava/lang/Object;

.field private mMovingObjectBitmaps:Ljava/lang/Object;

.field private mProgressHandler:Landroid/os/Handler;

.field private mProgressIndex:I

.field private mRealImageHeight:I

.field private mRealImageWidth:I

.field private mRefImageIndex:I

.field private mResult:I

.field private mStatus:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    :try_start_0
    const-string v1, "arcsoft_magicshot_eraser_T"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 46
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 47
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mArcPiclearEngine:I

    .line 104
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkRects:Ljava/lang/Object;

    .line 105
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMovingObjectBitmaps:Ljava/lang/Object;

    .line 106
    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mResult:I

    .line 107
    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRefImageIndex:I

    .line 108
    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mProgressIndex:I

    .line 109
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mStatus:[Z

    .line 114
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mProgressHandler:Landroid/os/Handler;

    return-void
.end method

.method private OnMarkRect(Ljava/lang/Object;)V
    .locals 0
    .param p1, "MarkRects"    # Ljava/lang/Object;

    .prologue
    .line 363
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkRects:Ljava/lang/Object;

    .line 371
    return-void
.end method

.method private OnMarkRresult(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 390
    iput p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mResult:I

    .line 391
    return-void
.end method

.method private OnMovingObjectBitmap(Ljava/lang/Object;)V
    .locals 0
    .param p1, "MovingObjectBitmaps"    # Ljava/lang/Object;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMovingObjectBitmaps:Ljava/lang/Object;

    .line 381
    return-void
.end method

.method private OnProgressIndex(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 410
    iput p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mProgressIndex:I

    .line 411
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mProgressHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 413
    return-void
.end method

.method private OnRefImageIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 400
    iput p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRefImageIndex:I

    .line 401
    return-void
.end method

.method private fromRealToMark([Landroid/graphics/Rect;)[Landroid/graphics/Rect;
    .locals 5
    .param p1, "realRects"    # [Landroid/graphics/Rect;

    .prologue
    .line 422
    array-length v2, p1

    new-array v1, v2, [Landroid/graphics/Rect;

    .line 423
    .local v1, "markRects":[Landroid/graphics/Rect;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 425
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v1, v0

    .line 426
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkImageWidth:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRealImageWidth:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 427
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkImageWidth:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRealImageWidth:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 428
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkImageHeight:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRealImageHeight:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 429
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkImageHeight:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRealImageHeight:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 423
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 432
    :cond_0
    return-object v1
.end method

.method private native native_GetCropImageHeight()I
.end method

.method private native native_GetCropImageWidth()I
.end method

.method private native native_GetFirstImage()Ljava/lang/Object;
.end method

.method private native native_GetMarkImage()Ljava/lang/Object;
.end method

.method private native native_GetNoMaskImage()[B
.end method

.method private native native_GetResult()[B
.end method

.method private native native_ImageProcess(Ljava/lang/String;)I
.end method

.method private native native_Init()I
.end method

.method private native native_ProcessManualObjectInMarkImage([BI)Ljava/lang/Object;
.end method

.method private native native_ProcessObjectInMarkImage(II)Ljava/lang/Object;
.end method

.method private native native_ProcessObjectInMarkImage_ByRect(I)Ljava/lang/Object;
.end method

.method private native native_SaveImage(Ljava/lang/String;[II)I
.end method

.method private native native_SetMarkImageSize(II)V
.end method

.method private native native_SetMode(I)V
.end method

.method private native native_SetPhotoCount(I)V
.end method

.method private native native_SetPhotoSize(II)V
.end method

.method private native native_SetUseRawData(Z)I
.end method

.method private native native_Set_NV21(J[II)I
.end method

.method private native native_UnInit()V
.end method


# virtual methods
.method public ArcPiClear_GetCropImageSize()Lcom/arcsoft/magicshotstudio/utils/MSize;
    .locals 3

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_GetCropImageWidth()I

    move-result v2

    .line 351
    .local v2, "width":I
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_GetCropImageHeight()I

    move-result v0

    .line 352
    .local v0, "hight":I
    new-instance v1, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {v1, v2, v0}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    .line 353
    .local v1, "imageSize":Lcom/arcsoft/magicshotstudio/utils/MSize;
    return-object v1
.end method

.method public ArcPiClear_GetFirstImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_GetFirstImage()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public ArcPiClear_GetMarkImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_GetMarkImage()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public ArcPiClear_GetMovingObjectBitmaps()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMovingObjectBitmaps:Ljava/lang/Object;

    return-object v0
.end method

.method public ArcPiClear_GetNoMaskImage()[B
    .locals 1

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_GetNoMaskImage()[B

    move-result-object v0

    return-object v0
.end method

.method public ArcPiClear_GetObjectArea()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkRects:Ljava/lang/Object;

    return-object v0
.end method

.method public ArcPiClear_GetProcessResult()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mResult:I

    return v0
.end method

.method public ArcPiClear_GetProgressIndex()I
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mProgressIndex:I

    return v0
.end method

.method public ArcPiClear_GetRefImageIndex()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRefImageIndex:I

    return v0
.end method

.method public ArcPiClear_GetResult()[B
    .locals 1

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_GetResult()[B

    move-result-object v0

    return-object v0
.end method

.method public ArcPiClear_ImageProcess(Ljava/lang/String;)I
    .locals 1
    .param p1, "FileName"    # Ljava/lang/String;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_ImageProcess(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public ArcPiClear_Init()I
    .locals 2

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "res":I
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_Init()I

    move-result v0

    .line 125
    if-nez v0, :cond_0

    .line 126
    const/4 v1, 0x0

    .line 128
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x5

    goto :goto_0
.end method

.method public ArcPiClear_ProcessManualObjectInMarkImage([BI)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "jpegData"    # [B
    .param p2, "DoFlag"    # I

    .prologue
    .line 321
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_ProcessManualObjectInMarkImage([BI)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public ArcPiClear_ProcessObjectInMarkImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "pointX"    # I
    .param p2, "pointY"    # I

    .prologue
    .line 298
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_ProcessObjectInMarkImage(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public ArcPiClear_ProcessObjectInMarkImage_ByRect(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "rectIndex"    # I

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_ProcessObjectInMarkImage_ByRect(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public ArcPiClear_SaveImage(Ljava/lang/String;[I)I
    .locals 2
    .param p1, "savePath"    # Ljava/lang/String;
    .param p2, "moveObjectIndexArray"    # [I

    .prologue
    .line 285
    if-eqz p2, :cond_0

    array-length v0, p2

    .line 286
    .local v0, "length":I
    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SaveImage(Ljava/lang/String;[II)I

    move-result v1

    return v1

    .line 285
    .end local v0    # "length":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ArcPiClear_SetMarkImageSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkImageWidth:I

    .line 194
    iput p2, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mMarkImageHeight:I

    .line 195
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetMarkImageSize(II)V

    .line 196
    return-void
.end method

.method public ArcPiClear_SetMode(I)V
    .locals 0
    .param p1, "iMode"    # I

    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetMode(I)V

    .line 162
    return-void
.end method

.method public ArcPiClear_SetPhotoCount(I)V
    .locals 0
    .param p1, "photoCount"    # I

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetPhotoCount(I)V

    .line 152
    return-void
.end method

.method public ArcPiClear_SetPhotoSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRealImageWidth:I

    .line 140
    iput p2, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mRealImageHeight:I

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetPhotoSize(II)V

    .line 142
    return-void
.end method

.method public ArcPiClear_SetProgressHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "iHandler"    # Landroid/os/Handler;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->mProgressHandler:Landroid/os/Handler;

    .line 172
    return-void
.end method

.method public ArcPiClear_UnInit()V
    .locals 0

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_UnInit()V

    .line 341
    return-void
.end method

.method public native native_SetExifInfo(JJ)I
.end method

.method public native native_SetFileLog(Ljava/lang/String;)I
.end method

.method public native native_SetOpenLogChecked(Z)I
.end method

.method public setExifInfo(JJ)I
    .locals 1
    .param p1, "nativeExifJPGBuff"    # J
    .param p3, "nativeExifJPGBuffSize"    # J

    .prologue
    .line 436
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetExifInfo(JJ)I

    move-result v0

    return v0
.end method

.method public native setPreprocessorData(JI)I
.end method

.method public set_LogFileName(Ljava/lang/String;)I
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 448
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetFileLog(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public set_LogOpen(Z)I
    .locals 1
    .param p1, "isOpen"    # Z

    .prologue
    .line 452
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetOpenLogChecked(Z)I

    move-result v0

    return v0
.end method

.method public set_NV21(J[II)I
    .locals 1
    .param p1, "inputAds"    # J
    .param p3, "indexArray"    # [I
    .param p4, "length"    # I

    .prologue
    .line 440
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_Set_NV21(J[II)I

    move-result v0

    return v0
.end method

.method public set_UseRawData(Z)I
    .locals 1
    .param p1, "bUseRawData"    # Z

    .prologue
    .line 444
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->native_SetUseRawData(Z)I

    move-result v0

    return v0
.end method
