.class public Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;
.super Ljava/lang/Object;
.source "PicBest_FaceInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_PicBest_FaceInfo"


# instance fields
.field private mPicBestFaceImg:Landroid/graphics/Bitmap;

.field private mPicBestFaceRect:Landroid/graphics/Rect;

.field private mPicBestImgId:I

.field private morient:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Rect;II)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "id"    # I
    .param p4, "orient"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->mPicBestFaceImg:Landroid/graphics/Bitmap;

    .line 16
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->mPicBestFaceRect:Landroid/graphics/Rect;

    .line 17
    iput p3, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->mPicBestImgId:I

    .line 18
    iput p4, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->morient:I

    .line 19
    return-void
.end method


# virtual methods
.method public GetFaceImg()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->mPicBestFaceImg:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public GetFaceRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->mPicBestFaceRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public GetId()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->mPicBestImgId:I

    return v0
.end method

.method public getOrient()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;->morient:I

    return v0
.end method
