.class public Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;
.super Ljava/lang/Object;
.source "ActionEngine_JNI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;,
        Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;,
        Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;,
        Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;,
        Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;,
        Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;
    }
.end annotation


# static fields
.field public static final APICSEQUENCE_ERR_LARGE_OBJECT:I = 0x15

.field public static final MERR_APICSEQUENCE_MULTIMOVING_OBJECT:I = 0x18

.field public static final MERR_APICSEQUENCE_NO_MOVING_OBJECT:I = 0x17

.field public static final MERR_APICSEQUENCE_USER_CANCEL:I = 0x16

.field static final MSG_NOTIFY_ERROR:I = 0x1

.field static final MSG_NOTIFY_PROGRESS:I = 0x2

.field static final MSG_NOTIFY_RESULT:I

.field private static final tag:Ljava/lang/String;


# instance fields
.field private mAsyncLock:Ljava/util/concurrent/Semaphore;

.field private mJNIEngine:I

.field private mListener:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

.field private mMSGCallback:Landroid/os/Handler$Callback;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-class v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    .line 340
    :try_start_0
    const-string v1, "arcsoft_magicshot_drama_T"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 341
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 342
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    .line 20
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    .line 307
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$1;-><init>(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mMSGCallback:Landroid/os/Handler$Callback;

    .line 337
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mMSGCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    .line 23
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

    return-object v0
.end method

.method private native native_applyMask(II[BIIZLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_applySelected(I[BLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_cancelEdit(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_confirmEdit(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_getAutoResult(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_getCandidateThumbs(IIILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;)I
.end method

.method private native native_getMotionDirection(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;)I
.end method

.method private native native_getObjectMask(IILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;)I
.end method

.method private native native_getResultImageByMask(II[BIILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_getSelected(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;)I
.end method

.method private native native_init(I)I
.end method

.method private native native_processFrame(I[BII)I
.end method

.method private native native_processFrames(I[Ljava/lang/String;II)I
.end method

.method private native native_processFramesEX(IJI)I
.end method

.method private native native_requestAutoResult(I)I
.end method

.method private native native_requestCancelAuto(I)I
.end method

.method private native native_saveFile(ILjava/lang/String;)I
.end method

.method private native native_setDumpDataChecked(IZLjava/lang/String;)I
.end method

.method private native native_setExifInfo(IJJ)I
.end method

.method private native native_setFileLog(ILjava/lang/String;)I
.end method

.method private native native_setOpenLogChecked(IZ)I
.end method

.method private native native_setPreprocessorData(IJI)I
.end method

.method private native native_uninit(I)I
.end method

.method private onNotifyAutoResult(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 279
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyAutoResult mUIHandler "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 281
    .local v0, "message":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 282
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyAutoResult bitmap  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyAutoResult message  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 285
    return-void
.end method

.method private onNotifyError(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 288
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    const-string v2, " onNotifyError error"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 290
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 292
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 293
    return-void
.end method

.method private onNotifyProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 296
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->tag:Ljava/lang/String;

    const-string v2, " onNotifyProgress"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 298
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 299
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 300
    return-void
.end method


# virtual methods
.method public applyMask(I[BIIZLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
    .locals 9
    .param p1, "maskIndex"    # I
    .param p2, "pMask"    # [B
    .param p3, "maskWidth"    # I
    .param p4, "maskHeight"    # I
    .param p5, "isAdd"    # Z
    .param p6, "result"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    .prologue
    .line 224
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 225
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_applyMask(II[BIIZLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v8

    .line 227
    .local v8, "res":I
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 228
    return v8
.end method

.method public applySelected([BLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
    .locals 2
    .param p1, "selectFlags"    # [B
    .param p2, "result"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 234
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_applySelected(I[BLcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 235
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 236
    return v0
.end method

.method public cancelEdit(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
    .locals 2
    .param p1, "result"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    .prologue
    .line 249
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 250
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_cancelEdit(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 251
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 252
    return v0
.end method

.method public confirmEdit(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
    .locals 2
    .param p1, "result"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    .prologue
    .line 241
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 242
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_confirmEdit(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 243
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 244
    return v0
.end method

.method public getAutoResult(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
    .locals 2
    .param p1, "result"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    .prologue
    .line 171
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 172
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_getAutoResult(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 173
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 174
    return v0
.end method

.method public getCandidateThumbs(IILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;)I
    .locals 2
    .param p1, "thumbWidth"    # I
    .param p2, "thumbHeight"    # I
    .param p3, "rtThumbs"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;

    .prologue
    .line 187
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 188
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_getCandidateThumbs(IIILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnThumbs;)I

    move-result v0

    .line 189
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 190
    return v0
.end method

.method public getMotionDirection(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;)I
    .locals 2
    .param p1, "direction"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;

    .prologue
    .line 179
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 180
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_getMotionDirection(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$MotionDirection;)I

    move-result v0

    .line 181
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 182
    return v0
.end method

.method public getObjectMask(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;)I
    .locals 2
    .param p1, "objectIndex"    # I
    .param p2, "returnMask"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;

    .prologue
    .line 203
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 204
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_getObjectMask(IILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnMask;)I

    move-result v0

    .line 205
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 206
    return v0
.end method

.method public getResultImageByMask(I[BIILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I
    .locals 8
    .param p1, "maskIndex"    # I
    .param p2, "pMask"    # [B
    .param p3, "maskWidth"    # I
    .param p4, "maskHeight"    # I
    .param p5, "result"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 214
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_getResultImageByMask(II[BIILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnBitmap;)I

    move-result v7

    .line 216
    .local v7, "res":I
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 217
    return v7
.end method

.method public getSelected(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;)I
    .locals 2
    .param p1, "rtSelected"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;

    .prologue
    .line 195
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 196
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_getSelected(ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$ReturnSelect;)I

    move-result v0

    .line 197
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 198
    return v0
.end method

.method public init(I)I
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 36
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_init(I)I

    move-result v0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    .line 37
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public processFrame([BII)I
    .locals 2
    .param p1, "NV21Data"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 57
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 58
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_processFrame(I[BII)I

    move-result v0

    .line 59
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 60
    return v0
.end method

.method public processFrames([Ljava/lang/String;II)I
    .locals 2
    .param p1, "NV21DataPath"    # [Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 105
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 106
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_processFrames(I[Ljava/lang/String;II)I

    move-result v0

    .line 107
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 108
    return v0
.end method

.method public processFramesEX(JI)I
    .locals 3
    .param p1, "address"    # J
    .param p3, "count"    # I

    .prologue
    .line 113
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 114
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_processFramesEX(IJI)I

    move-result v0

    .line 115
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 116
    return v0
.end method

.method public requestAutoResult()I
    .locals 2

    .prologue
    .line 73
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 74
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_requestAutoResult(I)I

    move-result v0

    .line 75
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 76
    return v0
.end method

.method public requestCancelAuto()I
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 66
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_requestCancelAuto(I)I

    move-result v0

    .line 67
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 68
    return v0
.end method

.method public saveFile(Ljava/lang/String;)I
    .locals 2
    .param p1, "savePath"    # Ljava/lang/String;

    .prologue
    .line 265
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 266
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_saveFile(ILjava/lang/String;)I

    move-result v0

    .line 267
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 268
    return v0
.end method

.method public setDumpDataChecked(ZLjava/lang/String;)I
    .locals 2
    .param p1, "isDump"    # Z
    .param p2, "dumpDataPath"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 82
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_setDumpDataChecked(IZLjava/lang/String;)I

    move-result v0

    .line 83
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 84
    return v0
.end method

.method public setExifInfo(JJ)I
    .locals 7
    .param p1, "nativeExifJPGBuff"    # J
    .param p3, "nativeExifJPGBuffSize"    # J

    .prologue
    .line 257
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 258
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_setExifInfo(IJJ)I

    move-result v6

    .line 259
    .local v6, "res":I
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 260
    return v6
.end method

.method public setFileLog(Ljava/lang/String;)I
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 90
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_setFileLog(ILjava/lang/String;)I

    move-result v0

    .line 91
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 92
    return v0
.end method

.method public setNotifyListener(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

    .line 129
    return-void
.end method

.method public setOpenLogChecked(Z)I
    .locals 2
    .param p1, "isOpen"    # Z

    .prologue
    .line 97
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 98
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_setOpenLogChecked(IZ)I

    move-result v0

    .line 99
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 100
    return v0
.end method

.method public setPreprocessorData(JI)I
    .locals 3
    .param p1, "address"    # J
    .param p3, "count"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 122
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1, p1, p2, p3}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_setPreprocessorData(IJI)I

    move-result v0

    .line 123
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 124
    return v0
.end method

.method public uninit()I
    .locals 3

    .prologue
    .line 44
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 45
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 46
    iget v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    invoke-direct {p0, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->native_uninit(I)I

    move-result v0

    .line 47
    .local v0, "ret":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mJNIEngine:I

    .line 48
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 49
    return v0
.end method
