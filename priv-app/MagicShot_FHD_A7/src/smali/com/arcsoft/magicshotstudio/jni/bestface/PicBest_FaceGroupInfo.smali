.class public Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;
.super Ljava/lang/Object;
.source "PicBest_FaceGroupInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_PicBest_FaceGroupInfo"


# instance fields
.field private mPicBestBestId:I

.field private mPicBestFaceInfo:[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

.field private mResFaceRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>([Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;Landroid/graphics/Rect;I)V
    .locals 0
    .param p1, "info"    # [Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;
    .param p2, "resFaceRect"    # Landroid/graphics/Rect;
    .param p3, "id"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->mPicBestFaceInfo:[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    .line 15
    iput p3, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->mPicBestBestId:I

    .line 16
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->mResFaceRect:Landroid/graphics/Rect;

    .line 17
    return-void
.end method


# virtual methods
.method public GetBestID()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->mPicBestBestId:I

    return v0
.end method

.method public GetFaceGroup()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->mPicBestFaceInfo:[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceInfo;

    return-object v0
.end method

.method public getResFaceRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;->mResFaceRect:Landroid/graphics/Rect;

    return-object v0
.end method
