.class public Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;
.super Ljava/lang/Object;
.source "BestPhoto_JNI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;,
        Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    }
.end annotation


# static fields
.field private static mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;


# instance fields
.field private mProcessCallback:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;

.field private mSemaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    .line 119
    const-string v0, "arcsoft_magicshot_bestphoto_T"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    .line 38
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mProcessCallback:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;

    .line 19
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    .line 20
    return-void
.end method

.method private native getCandidates(JILcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;JI)I
.end method

.method public static getEngine()Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;-><init>()V

    sput-object v0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    .line 27
    :cond_0
    sget-object v0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    return-object v0
.end method

.method private native init()I
.end method

.method private native saveBitmaps([I[Ljava/lang/String;J)I
.end method

.method private native saveBitmapsWithExif([I[Ljava/lang/String;JJJ)I
.end method

.method private native setLogFileName(Ljava/lang/String;)I
.end method

.method private native uninit()I
.end method

.method private updateProcess(I)V
    .locals 1
    .param p1, "process"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mProcessCallback:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mProcessCallback:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;

    invoke-interface {v0, p1}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;->update(I)V

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public createCandidates()Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;-><init>(Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;)V

    return-object v0
.end method

.method public nativeGetCandidates(JILcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;JI)I
    .locals 3
    .param p1, "preDataAdd"    # J
    .param p3, "preDataSize"    # I
    .param p4, "candidates"    # Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    .param p5, "mBitmapAddr"    # J
    .param p7, "ori"    # I

    .prologue
    .line 61
    const/4 v0, -0x1

    .line 62
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 63
    invoke-direct/range {p0 .. p7}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->getCandidates(JILcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;JI)I

    move-result v0

    .line 64
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 66
    return v0
.end method

.method public nativeInit()I
    .locals 2

    .prologue
    .line 88
    const/4 v0, -0x1

    .line 89
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 90
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->init()I

    move-result v0

    .line 91
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 93
    return v0
.end method

.method public nativeSaveBitmaps([I[Ljava/lang/String;J)I
    .locals 3
    .param p1, "saveIndexes"    # [I
    .param p2, "pathArray"    # [Ljava/lang/String;
    .param p3, "mBitmapAddr"    # J

    .prologue
    .line 70
    const/4 v0, -0x1

    .line 71
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->saveBitmaps([I[Ljava/lang/String;J)I

    move-result v0

    .line 73
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 75
    return v0
.end method

.method public nativeSaveBitmapsWithExif([I[Ljava/lang/String;JJJ)I
    .locals 3
    .param p1, "saveIndexes"    # [I
    .param p2, "pathArray"    # [Ljava/lang/String;
    .param p3, "mBitmapAddr"    # J
    .param p5, "exifBuff"    # J
    .param p7, "exifBuffSize"    # J

    .prologue
    .line 79
    const/4 v0, -0x1

    .line 80
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 81
    invoke-direct/range {p0 .. p8}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->saveBitmapsWithExif([I[Ljava/lang/String;JJJ)I

    move-result v0

    .line 82
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 84
    return v0
.end method

.method public nativeSetLogFileName(Ljava/lang/String;)I
    .locals 2
    .param p1, "logFile"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v0, -0x1

    .line 52
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 53
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->setLogFileName(Ljava/lang/String;)I

    move-result v0

    .line 54
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 56
    return v0
.end method

.method public nativeUninit()I
    .locals 2

    .prologue
    .line 97
    const/4 v0, -0x1

    .line 98
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 99
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->uninit()I

    move-result v0

    .line 100
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 102
    return v0
.end method

.method public setProcessLis(Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;)V
    .locals 0
    .param p1, "back"    # Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->mProcessCallback:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;

    .line 42
    return-void
.end method
