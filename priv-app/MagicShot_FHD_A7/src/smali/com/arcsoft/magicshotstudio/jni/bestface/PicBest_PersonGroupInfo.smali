.class public Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;
.super Ljava/lang/Object;
.source "PicBest_PersonGroupInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BestFace_PicBest_PersonGroupInfo"


# instance fields
.field private mPicBestFaceGroupInfo:[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

.field private mReferenceImageId:I


# direct methods
.method public constructor <init>([Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;I)V
    .locals 0
    .param p1, "info"    # [Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;
    .param p2, "id"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->mPicBestFaceGroupInfo:[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    .line 11
    iput p2, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->mReferenceImageId:I

    .line 12
    return-void
.end method


# virtual methods
.method public GetFaceGroupInfo()[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->mPicBestFaceGroupInfo:[Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_FaceGroupInfo;

    return-object v0
.end method

.method public getReferenceImageId()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/arcsoft/magicshotstudio/jni/bestface/PicBest_PersonGroupInfo;->mReferenceImageId:I

    return v0
.end method
