.class Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;
.super Ljava/lang/Object;
.source "PicmotionEngine_JNI.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;->this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 360
    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleMessage mListener ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;->this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$100(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleMessage msg.what ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;->this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$100(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    move-result-object v4

    if-nez v4, :cond_0

    .line 363
    const/4 v3, 0x0

    .line 383
    :goto_0
    return v3

    .line 366
    :cond_0
    const/4 v3, 0x1

    .line 367
    .local v3, "res":Z
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 381
    const/4 v3, 0x0

    goto :goto_0

    .line 369
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 370
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;->this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$100(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;->onNotifyAutoResult(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 373
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 374
    .local v1, "error":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;->this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$100(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;->onNotifyError(I)V

    goto :goto_0

    .line 377
    .end local v1    # "error":I
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 378
    .local v2, "progress":I
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;->this$0:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    # getter for: Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->access$100(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;->onNotifyProgress(I)V

    goto :goto_0

    .line 367
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
