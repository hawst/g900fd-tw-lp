.class public Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
.super Ljava/lang/Object;
.source "PicmotionEngine_JNI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;,
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;,
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;,
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;,
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;,
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;,
        Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    }
.end annotation


# static fields
.field public static final APICSEQUENCE_ERR_LARGE_OBJECT:I = 0x15

.field public static final INVALID_NUM:I = -0x1

.field public static final MERR_APICSEQUENCE_MULTIMOVING_OBJECT:I = 0x18

.field public static final MERR_APICSEQUENCE_NO_MOVING_OBJECT:I = 0x17

.field public static final MERR_APICSEQUENCE_USER_CANCEL:I = 0x16

.field static final MSG_NOTIFY_ERROR:I = 0x1

.field static final MSG_NOTIFY_PROGRESS:I = 0x2

.field static final MSG_NOTIFY_RESULT:I

.field private static final tag:Ljava/lang/String;


# instance fields
.field private mAsyncLock:Ljava/util/concurrent/Semaphore;

.field private mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

.field private mMSGCallback:Landroid/os/Handler$Callback;

.field private mPicMotionAddres:J

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    const-class v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    .line 390
    :try_start_0
    const-string v1, "arcsoft_picmotion_T"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 391
    const-string v1, "arcsoft_magicshot_picmotion_T"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 392
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 393
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    .line 21
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    .line 357
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$1;-><init>(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mMSGCallback:Landroid/os/Handler$Callback;

    .line 387
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mMSGCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    return-object v0
.end method

.method private native native_applyMask(JI[BIIZLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_applySelected(J[BLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_cancelEdit(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_confirmEdit(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_getAutoResult(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_getBestImageIndex(J)I
.end method

.method private native native_getBlurInfo(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;)I
.end method

.method private native native_getCandidateThumbs(JIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;)I
.end method

.method private native native_getMotionDirection(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;)I
.end method

.method private native native_getObjectMask(JILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;)I
.end method

.method private native native_getResultImageByMask(JI[BIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private native native_getSelected(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;)I
.end method

.method private native native_init(I)I
.end method

.method private native native_processFrame(J[BII)I
.end method

.method private native native_processFrames(J[Ljava/lang/String;II)I
.end method

.method private native native_processFramesEX(JJI)I
.end method

.method private native native_requestAutoResult(J)I
.end method

.method private native native_requestCancelAuto(J)I
.end method

.method private native native_saveFile(JLjava/lang/String;)I
.end method

.method private native native_setDumpDataChecked(JZLjava/lang/String;)I
.end method

.method private native native_setExifInfo(JJJ)I
.end method

.method private native native_setFileLog(JLjava/lang/String;)I
.end method

.method private native native_setOpenLogChecked(Z)I
.end method

.method private native native_setPreprocessorData(JJI)I
.end method

.method private native native_uninit(J)I
.end method

.method private native native_updateBlurInfo(JIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
.end method

.method private onNotifyAutoResult(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 329
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyAutoResult mUIHandler "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 331
    .local v0, "message":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 332
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyAutoResult bitmap  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyAutoResult message  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 335
    return-void
.end method

.method private onNotifyError(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 338
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    const-string v2, " onNotifyError error"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 340
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 342
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 343
    return-void
.end method

.method private onNotifyProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 346
    sget-object v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    const-string v2, " onNotifyProgress"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 348
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 349
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 350
    return-void
.end method


# virtual methods
.method public applyMask(I[BIIZLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 10
    .param p1, "maskIndex"    # I
    .param p2, "pMask"    # [B
    .param p3, "maskWidth"    # I
    .param p4, "maskHeight"    # I
    .param p5, "isAdd"    # Z
    .param p6, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 275
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_applyMask(JI[BIIZLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 277
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 278
    return v0
.end method

.method public applySelected([BLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 4
    .param p1, "selectFlags"    # [B
    .param p2, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 283
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 284
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_applySelected(J[BLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 285
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 286
    return v0
.end method

.method public cancelEdit(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 4
    .param p1, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 299
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 300
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_cancelEdit(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 301
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 302
    return v0
.end method

.method public confirmEdit(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 4
    .param p1, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 291
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 292
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_confirmEdit(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 293
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 294
    return v0
.end method

.method public getAutoResult(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 4
    .param p1, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 221
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 222
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getAutoResult(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 223
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 224
    return v0
.end method

.method public getBestImageIndex()I
    .locals 4

    .prologue
    .line 145
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 146
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getBestImageIndex(J)I

    move-result v0

    .line 147
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 148
    return v0
.end method

.method public getBlurInfo(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;)I
    .locals 4
    .param p1, "blurInfo"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;

    .prologue
    .line 153
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 154
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getBlurInfo(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBlurInfo;)I

    move-result v0

    .line 155
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 156
    return v0
.end method

.method public getCandidateThumbs(IILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;)I
    .locals 7
    .param p1, "thumbWidth"    # I
    .param p2, "thumbHeight"    # I
    .param p3, "rtThumbs"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;

    .prologue
    .line 237
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 238
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getCandidateThumbs(JIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnThumbs;)I

    move-result v0

    .line 239
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 240
    return v0
.end method

.method public getMotionDirection(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;)I
    .locals 4
    .param p1, "direction"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;

    .prologue
    .line 229
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 230
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getMotionDirection(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$MotionDirection;)I

    move-result v0

    .line 231
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 232
    return v0
.end method

.method public getObjectMask(ILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;)I
    .locals 4
    .param p1, "objectIndex"    # I
    .param p2, "returnMask"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;

    .prologue
    .line 253
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 254
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getObjectMask(JILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnMask;)I

    move-result v0

    .line 255
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 256
    return v0
.end method

.method public getResultImageByMask(I[BIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 9
    .param p1, "maskIndex"    # I
    .param p2, "pMask"    # [B
    .param p3, "maskWidth"    # I
    .param p4, "maskHeight"    # I
    .param p5, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 263
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 264
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getResultImageByMask(JI[BIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 266
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 267
    return v0
.end method

.method public getSelected(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;)I
    .locals 4
    .param p1, "rtSelected"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;

    .prologue
    .line 245
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 246
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_getSelected(JLcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnSelect;)I

    move-result v0

    .line 247
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 248
    return v0
.end method

.method public getmPicMotionAddres()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    return-wide v0
.end method

.method public init(I)I
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const-wide/16 v4, 0x0

    .line 45
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "ret":I
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 49
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_uninit(J)I

    move-result v0

    .line 50
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    .line 52
    :cond_0
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_init(I)I

    move-result v0

    .line 53
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 54
    return v0
.end method

.method public processFrame([BII)I
    .locals 7
    .param p1, "NV21Data"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 73
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 74
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_processFrame(J[BII)I

    move-result v0

    .line 75
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 76
    return v0
.end method

.method public processFrames([Ljava/lang/String;II)I
    .locals 7
    .param p1, "NV21DataPath"    # [Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 122
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_processFrames(J[Ljava/lang/String;II)I

    move-result v0

    .line 123
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 124
    return v0
.end method

.method public processFramesEX(JI)I
    .locals 7
    .param p1, "address"    # J
    .param p3, "count"    # I

    .prologue
    .line 129
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 130
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_processFramesEX(JJI)I

    move-result v0

    .line 131
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 132
    return v0
.end method

.method public requestAutoResult()I
    .locals 4

    .prologue
    .line 89
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 90
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_requestAutoResult(J)I

    move-result v0

    .line 91
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 92
    return v0
.end method

.method public requestCancelAuto()I
    .locals 4

    .prologue
    .line 81
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 82
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_requestCancelAuto(J)I

    move-result v0

    .line 83
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 84
    return v0
.end method

.method public saveFile(Ljava/lang/String;)I
    .locals 4
    .param p1, "savePath"    # Ljava/lang/String;

    .prologue
    .line 315
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 316
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_saveFile(JLjava/lang/String;)I

    move-result v0

    .line 317
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 318
    return v0
.end method

.method public setDumpDataChecked(ZLjava/lang/String;)I
    .locals 4
    .param p1, "isDump"    # Z
    .param p2, "dumpDataPath"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 98
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1, p2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_setDumpDataChecked(JZLjava/lang/String;)I

    move-result v0

    .line 99
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 100
    return v0
.end method

.method public setExifInfo(JJ)I
    .locals 9
    .param p1, "nativeExifJPGBuff"    # J
    .param p3, "nativeExifJPGBuffSize"    # J

    .prologue
    .line 307
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 308
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_setExifInfo(JJJ)I

    move-result v0

    .line 309
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 310
    return v0
.end method

.method public setFileLog(Ljava/lang/String;)I
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 106
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_setFileLog(JLjava/lang/String;)I

    move-result v0

    .line 107
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 108
    return v0
.end method

.method public setNotifyListener(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mListener:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    .line 169
    return-void
.end method

.method public setOpenLogChecked(Z)I
    .locals 2
    .param p1, "isOpen"    # Z

    .prologue
    .line 113
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 114
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_setOpenLogChecked(Z)I

    move-result v0

    .line 115
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 116
    return v0
.end method

.method public setPreprocessorData(JI)I
    .locals 7
    .param p1, "address"    # J
    .param p3, "count"    # I

    .prologue
    .line 137
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 138
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_setPreprocessorData(JJI)I

    move-result v0

    .line 139
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 140
    return v0
.end method

.method public setmPicMotionAddres(J)V
    .locals 5
    .param p1, "mPicMotionAddres"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    .line 29
    sget-object v0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PicmotionEngine_JNI setmPicMotionAddres "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public uninit()I
    .locals 4

    .prologue
    .line 60
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 61
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 62
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    invoke-direct {p0, v2, v3}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_uninit(J)I

    move-result v0

    .line 63
    .local v0, "ret":I
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    .line 64
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 65
    return v0
.end method

.method public updateBlurInfo(IILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I
    .locals 7
    .param p1, "mBlurSize"    # I
    .param p2, "mBlurDegree"    # I
    .param p3, "result"    # Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;

    .prologue
    .line 161
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 162
    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mPicMotionAddres:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->native_updateBlurInfo(JIILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$ReturnBitmap;)I

    move-result v0

    .line 163
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->mAsyncLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 164
    return v0
.end method
