.class Lcom/arcsoft/magicshotstudio/Eraser$3;
.super Ljava/lang/Object;
.source "Eraser.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Eraser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 547
    const/4 v0, 0x0

    .line 549
    .local v0, "consumed":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1200(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mCurTouchedId:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1300(Lcom/arcsoft/magicshotstudio/Eraser;)I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    if-eq v2, v5, :cond_0

    .line 550
    const/4 v0, 0x1

    move v1, v0

    .line 573
    .end local v0    # "consumed":Z
    .local v1, "consumed":I
    :goto_0
    return v1

    .line 554
    .end local v1    # "consumed":I
    .restart local v0    # "consumed":Z
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 555
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z
    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1202(Lcom/arcsoft/magicshotstudio/Eraser;Z)Z

    .line 556
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mCurTouchedId:I
    invoke-static {v2, v5}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1302(Lcom/arcsoft/magicshotstudio/Eraser;I)I

    .line 557
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1200(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 558
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 567
    :cond_1
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v3, v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v2, v5, :cond_3

    .line 569
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z
    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1202(Lcom/arcsoft/magicshotstudio/Eraser;Z)Z

    .line 570
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mCurTouchedId:I
    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1302(Lcom/arcsoft/magicshotstudio/Eraser;I)I

    .line 571
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser$3;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1200(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v5

    if-nez v5, :cond_5

    :goto_2
    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    :cond_3
    move v1, v0

    .line 573
    .restart local v1    # "consumed":I
    goto :goto_0

    .end local v1    # "consumed":I
    :cond_4
    move v2, v4

    .line 557
    goto :goto_1

    :cond_5
    move v3, v4

    .line 571
    goto :goto_2

    .line 558
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f09004c -> :sswitch_0
        0x7f090053 -> :sswitch_0
    .end sparse-switch
.end method
