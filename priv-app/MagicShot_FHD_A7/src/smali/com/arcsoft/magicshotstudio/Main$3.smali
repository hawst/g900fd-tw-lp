.class Lcom/arcsoft/magicshotstudio/Main$3;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Main;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    check-cast p2, Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;->getService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$602(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 346
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getMainModeFlags()Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    move-result-object v1

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mMainModeEnable:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$702(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;)Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    .line 349
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # invokes: Lcom/arcsoft/magicshotstudio/Main;->setModeEnable()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$800(Lcom/arcsoft/magicshotstudio/Main;)V

    .line 350
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Main;->access$600(Lcom/arcsoft/magicshotstudio/Main;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$902(Lcom/arcsoft/magicshotstudio/Main;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 351
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$900(Lcom/arcsoft/magicshotstudio/Main;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBackgroundImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Main;->access$1000(Lcom/arcsoft/magicshotstudio/Main;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$3;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Main;->access$900(Lcom/arcsoft/magicshotstudio/Main;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 356
    :cond_0
    const-string v0, "MagicShotService"

    const-string v1, "Main onServiceConnected"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 360
    return-void
.end method
