.class Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Eraser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Eraser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SdcardStateChanageReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0

    .prologue
    .line 1592
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, 0x1f4

    const/16 v5, 0x800

    const/16 v4, 0xa

    .line 1596
    const/4 v0, 0x0

    .line 1597
    .local v0, "needFinish":Z
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->getOperateEnable()Z

    move-result v1

    .line 1598
    .local v1, "operateEnabled":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1599
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->addFlags(I)V

    .line 1600
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$4900(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->isExSDCard(Ljava/lang/String;)Z

    move-result v0

    .line 1601
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 1602
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 1603
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1100(Lcom/arcsoft/magicshotstudio/Eraser;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1613
    :cond_1
    :goto_0
    return-void

    .line 1606
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1100(Lcom/arcsoft/magicshotstudio/Eraser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1609
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1610
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->addFlags(I)V

    .line 1611
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1100(Lcom/arcsoft/magicshotstudio/Eraser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
