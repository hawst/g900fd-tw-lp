.class Lcom/arcsoft/magicshotstudio/Main$2;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Main;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 219
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    move v0, v1

    .line 239
    :cond_0
    :goto_1
    return v0

    .line 221
    :pswitch_0
    sget-object v2, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v3, "ACTION_DOWN"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z
    invoke-static {v2, v0}, Lcom/arcsoft/magicshotstudio/Main;->access$302(Lcom/arcsoft/magicshotstudio/Main;Z)Z

    .line 223
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Main;->access$400(Lcom/arcsoft/magicshotstudio/Main;)I

    move-result v2

    if-gtz v2, :cond_0

    .line 224
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I
    invoke-static {v0, v2}, Lcom/arcsoft/magicshotstudio/Main;->access$402(Lcom/arcsoft/magicshotstudio/Main;I)I

    goto :goto_0

    .line 230
    :pswitch_1
    sget-object v2, Lcom/arcsoft/magicshotstudio/Main;->tag:Ljava/lang/String;

    const-string v3, "ACTION_UP"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mModePressed:Z
    invoke-static {v2, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$302(Lcom/arcsoft/magicshotstudio/Main;Z)Z

    .line 232
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # getter for: Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/Main;->access$400(Lcom/arcsoft/magicshotstudio/Main;)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 233
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Main$2;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    # setter for: Lcom/arcsoft/magicshotstudio/Main;->mCurrentClickedLayoutId:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Main;->access$402(Lcom/arcsoft/magicshotstudio/Main;I)I

    move v0, v1

    .line 234
    goto :goto_1

    .line 219
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
