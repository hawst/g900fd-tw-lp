.class Lcom/arcsoft/magicshotstudio/Eraser$4;
.super Ljava/lang/Object;
.source "Eraser.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Eraser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0

    .prologue
    .line 577
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x5000

    const/4 v2, 0x0

    .line 582
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onClick in "

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$000(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 647
    :goto_0
    return-void

    .line 586
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 646
    :goto_1
    :sswitch_0
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onClick out "

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 588
    :sswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 589
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "press cancel button"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1500(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1600(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsUserEdit:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1700(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1800(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_1

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 595
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 596
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->setActivityResult()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1900(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 597
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2000(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 598
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    goto :goto_1

    .line 601
    :sswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 602
    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "press save button"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mFromGallery:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2100(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 604
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1500(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 605
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->saveResult()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2200(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 606
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->changeSaveBtnState(Z)V

    .line 607
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 609
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2300(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 610
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2300(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2400(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 612
    :cond_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2302(Lcom/arcsoft/magicshotstudio/Eraser;Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;)Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 613
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2300(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2500(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;->shareFile(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 617
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->saveResult()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2200(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 618
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 619
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 620
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2600(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 621
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->exitActivity()V

    .line 622
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 626
    :sswitch_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 627
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2700(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 628
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1500(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mIsUserEdit:Z
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2800(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 629
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$2900(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto/16 :goto_1

    .line 631
    :cond_5
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$4;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    goto/16 :goto_1

    .line 586
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_3
        0x7f09004c -> :sswitch_1
        0x7f090053 -> :sswitch_2
        0x7f090059 -> :sswitch_0
    .end sparse-switch
.end method
