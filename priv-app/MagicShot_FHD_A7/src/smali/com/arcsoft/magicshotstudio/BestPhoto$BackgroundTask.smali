.class Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;
.super Landroid/os/AsyncTask;
.source "BestPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/BestPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BackgroundTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/BestPhoto;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 11
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 319
    const-string v1, "yyyyMMddhhmm"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 321
    .local v10, "time":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BestPhoto_performance_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "logFile":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$800(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mPreDataAddress:J
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$300(Lcom/arcsoft/magicshotstudio/BestPhoto;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mPreDataSize:I
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$400(Lcom/arcsoft/magicshotstudio/BestPhoto;)I

    move-result v4

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$500(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    move-result-object v5

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mBitmapAddress:J
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$600(Lcom/arcsoft/magicshotstudio/BestPhoto;)J

    move-result-wide v6

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mOrientation:I
    invoke-static {v8}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$700(Lcom/arcsoft/magicshotstudio/BestPhoto;)I

    move-result v8

    invoke-virtual/range {v1 .. v8}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->nativeGetCandidates(JILcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;JI)I

    move-result v9

    .line 327
    .local v9, "ret":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 316
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 337
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$900(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$900(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dimissDelay(J)V

    .line 340
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    :goto_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 346
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestPhoto;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BestPhotoStartTime cost time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # getter for: Lcom/arcsoft/magicshotstudio/BestPhoto;->BestPhotoStartTime:J
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$1100(Lcom/arcsoft/magicshotstudio/BestPhoto;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    return-void

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/BestPhoto;

    # invokes: Lcom/arcsoft/magicshotstudio/BestPhoto;->initUI()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->access$1000(Lcom/arcsoft/magicshotstudio/BestPhoto;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 316
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 332
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 333
    return-void
.end method
