.class Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;
.super Landroid/os/AsyncTask;
.source "Drama.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Drama;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitEditDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Drama;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/Drama;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/Drama;Lcom/arcsoft/magicshotstudio/Drama$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/Drama$1;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;-><init>(Lcom/arcsoft/magicshotstudio/Drama;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Drama;->access$1200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    move-result-object v0

    new-instance v1, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask$1;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask$1;-><init>(Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;)V

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initEditData(Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;)Z

    .line 313
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 299
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    .line 323
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 324
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->hideFirstImage()V

    .line 325
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->updateImageAndFrameListView()V

    .line 326
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # setter for: Lcom/arcsoft/magicshotstudio/Drama;->mbProcessingFinish:Z
    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/Drama;->access$802(Lcom/arcsoft/magicshotstudio/Drama;Z)Z

    .line 327
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1100(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 328
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->showTopActionBar()V

    .line 329
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1300(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1400(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mSharedPref:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1300(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "TYPE_DRAMA"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 332
    .local v0, "isFirstIn":Z
    if-eqz v0, :cond_1

    .line 333
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1400(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "TYPE_DRAMA"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 334
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1400(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 335
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/Drama;->access$1502(Lcom/arcsoft/magicshotstudio/Drama;Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 336
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1500(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->show(I)V

    .line 343
    .end local v0    # "isFirstIn":Z
    :cond_0
    :goto_0
    return-void

    .line 338
    .restart local v0    # "isFirstIn":Z
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1500(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Drama;->access$1500(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 299
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 318
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 319
    return-void
.end method
