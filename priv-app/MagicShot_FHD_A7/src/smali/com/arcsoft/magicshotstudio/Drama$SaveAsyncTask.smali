.class Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "Drama.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Drama;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Drama;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/Drama;)V
    .locals 0

    .prologue
    .line 347
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/Drama;Lcom/arcsoft/magicshotstudio/Drama$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/Drama$1;

    .prologue
    .line 347
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;-><init>(Lcom/arcsoft/magicshotstudio/Drama;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Drama;->access$1200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Drama;->access$1200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResultFile()Z

    .line 354
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 347
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 365
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 366
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Drama;->access$1600(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 367
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    .line 368
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 347
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 360
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Drama;->access$1600(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 361
    return-void
.end method
