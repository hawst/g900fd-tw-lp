.class public Lcom/arcsoft/magicshotstudio/Eraser;
.super Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.source "Eraser.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;
    }
.end annotation


# static fields
.field public static final HANDLER_DELETE:I = 0x1

.field public static final HANDLER_FINISH:I = 0x9

.field public static final HANDLER_IMAGEACTION:I = 0x2

.field public static final HANDLER_INVATER:I = 0x7

.field public static final HANDLER_PROCESS_FINISH:I = 0xe

.field public static final HANDLER_PROGRESS_INDEX:I = 0x6

.field public static final HANDLER_SDCARD_CHANGED:I = 0xa

.field public static final HANDLER_SPEN_FINISH:I = 0xb

.field public static final HIDE_DESCRIPTION:I = 0x3

.field private static final HIDE_DESCRIPTION_DELAY_TIME:I = 0x1388

.field public static final HIDE_MANUALFAILTEXTLAYOUT:I = 0x4

.field public static final HIDE_PROGRESS_BAR:I = 0x5

.field private static final MIN_PHOTO_NUM:I = 0x5

.field private static final tag:Ljava/lang/String;


# instance fields
.field private bitmapDrawHeight:I

.field private bitmapDrawWidth:I

.field private mActionbarLayout:Landroid/widget/RelativeLayout;

.field private mBackArrowImageView:Landroid/widget/ImageView;

.field private mBackArrowLayout:Landroid/widget/RelativeLayout;

.field private mBackImageView:Landroid/widget/ImageView;

.field private mBackLayout:Landroid/widget/RelativeLayout;

.field private mBackLayoutGap3:Landroid/view/View;

.field private mBackTextView:Landroid/widget/TextView;

.field private mContentView:Landroid/view/View;

.field private mCostTimeTotal:J

.field private mCurTouchedId:I

.field private mDefaultActionBar:Landroid/widget/RelativeLayout;

.field private mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

.field private mEditImageView:Landroid/widget/ImageView;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFitInBitmapRect:Landroid/graphics/Rect;

.field private mHandler:Landroid/os/Handler;

.field private mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

.field private mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

.field private mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

.field private mInitEngineThread:Ljava/lang/Thread;

.field private mIsActivityFinished:Z

.field private mIsFirstImageSetDone:Z

.field private mIsLandScape:Z

.field private mIsSavaButtonPressed:Z

.field private mIsTouchLocked:Z

.field private mKeepSrcFiles:Z

.field private mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

.field private mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mMaskBitmap:Landroid/graphics/Bitmap;

.field private mMaskNum:I

.field private mMaskRectArray:[Landroid/graphics/Rect;

.field private mMenuImageView:Landroid/widget/ImageView;

.field private mMenuLayout:Landroid/widget/RelativeLayout;

.field private mMenuLayoutGap3:Landroid/view/View;

.field private mMenuTextView:Landroid/widget/TextView;

.field private mMode:I

.field private mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

.field private mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener$MySingleTapUpCallback;

.field private mNoMaskBitmap:Landroid/graphics/Bitmap;

.field private mNoMaskBitmapNeedUpdate:Z

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field private mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

.field mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private volatile mOperateEnable:Z

.field private mPhotoNum:I

.field private mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

.field private mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;

.field private mProcessFinished:Z

.field private mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

.field private mProgressNum:I

.field private mProgressTime:J

.field private mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field private mRectDst:Landroid/graphics/Rect;

.field private mRectSrc:Landroid/graphics/Rect;

.field mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;

.field private mSaveImageView:Landroid/widget/ImageView;

.field private mSaveLayout:Landroid/widget/RelativeLayout;

.field private mSaveLayoutGap3:Landroid/view/View;

.field private mSaveLock:Ljava/util/concurrent/Semaphore;

.field private mSaveTextView:Landroid/widget/TextView;

.field private mSaveThread:Ljava/lang/Thread;

.field public mScreenDensity:F

.field private mScreenHeightInitAcitivy:I

.field private mScreenWidthInitActivity:I

.field private mSelectIndexes:[I

.field private mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

.field private mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

.field private mTapFaceTextView:Landroid/widget/TextView;

.field private mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

.field private mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mUIinited:Z

.field private mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

.field private m_OffsetX:I

.field private m_OffsetY:I

.field private m_ScaleX:F

.field private m_ScaleY:F

.field private mbIsSaveBtnOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;-><init>()V

    .line 67
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mKeepSrcFiles:Z

    .line 68
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsActivityFinished:Z

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenDensity:F

    .line 71
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskNum:I

    .line 74
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPhotoNum:I

    .line 75
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSelectIndexes:[I

    .line 76
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;

    .line 77
    iput v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMode:I

    .line 79
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    .line 81
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->VALUE_5M_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 82
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->VALUE_VGA_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 85
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsSavaButtonPressed:Z

    .line 86
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProcessFinished:Z

    .line 88
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    .line 90
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 91
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 93
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenWidthInitActivity:I

    .line 94
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenHeightInitAcitivy:I

    .line 95
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    .line 96
    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mCostTimeTotal:J

    .line 98
    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressTime:J

    .line 99
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmapNeedUpdate:Z

    .line 102
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 103
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z

    .line 104
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    .line 110
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 111
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mContentView:Landroid/view/View;

    .line 118
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsFirstImageSetDone:Z

    .line 119
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressNum:I

    .line 120
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    .line 123
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 125
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Eraser$1;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener$MySingleTapUpCallback;

    .line 387
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    .line 423
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    .line 424
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceTextView:Landroid/widget/TextView;

    .line 425
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    .line 436
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 437
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveTextView:Landroid/widget/TextView;

    .line 438
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackTextView:Landroid/widget/TextView;

    .line 439
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuTextView:Landroid/widget/TextView;

    .line 440
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayoutGap3:Landroid/view/View;

    .line 441
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayoutGap3:Landroid/view/View;

    .line 442
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayoutGap3:Landroid/view/View;

    .line 542
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z

    .line 543
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mCurTouchedId:I

    .line 544
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Eraser$3;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 577
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Eraser$4;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 669
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mUIinited:Z

    .line 693
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z

    .line 702
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$5;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Eraser$5;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    .line 742
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveThread:Ljava/lang/Thread;

    .line 1013
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 1014
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 1115
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskRectArray:[Landroid/graphics/Rect;

    .line 1116
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    .line 1291
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$8;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Eraser$8;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 1387
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 1388
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    .line 1389
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_OffsetX:I

    .line 1390
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_OffsetY:I

    .line 1391
    iput v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_ScaleX:F

    .line 1392
    iput v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_ScaleY:F

    .line 1456
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    .line 1457
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    .line 1458
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    .line 1489
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawWidth:I

    .line 1490
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawHeight:I

    .line 1581
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;

    .line 1700
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$9;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Eraser$9;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z

    return v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V

    return-void
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/Eraser;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNativeExifJPGBuffSize:J

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/Eraser;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/arcsoft/magicshotstudio/Eraser;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsTouchLocked:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/Eraser;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mCurTouchedId:I

    return v0
.end method

.method static synthetic access$1302(Lcom/arcsoft/magicshotstudio/Eraser;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mCurTouchedId:I

    return p1
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsUserEdit:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setActivityResult()V

    return-void
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFromGallery:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->saveResult()V

    return-void
.end method

.method static synthetic access$2300(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/arcsoft/magicshotstudio/Eraser;Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;)Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsUserEdit:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/Eraser;I)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Eraser;->maskRectTap(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->saveResultFile()V

    return-void
.end method

.method static synthetic access$3802(Lcom/arcsoft/magicshotstudio/Eraser;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/arcsoft/magicshotstudio/Eraser;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I

    return v0
.end method

.method static synthetic access$4500(Lcom/arcsoft/magicshotstudio/Eraser;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mKeepSrcFiles:Z

    return v0
.end method

.method static synthetic access$4600(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4602(Lcom/arcsoft/magicshotstudio/Eraser;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$4700(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/arcsoft/magicshotstudio/Eraser;Landroid/view/View;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Eraser;->popUpToast(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4900(Lcom/arcsoft/magicshotstudio/Eraser;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/Eraser;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPreDataAddress:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/Eraser;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPreDataSize:I

    return v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/Eraser;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBitmapAddress:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/Eraser;)[I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSelectIndexes:[I

    return-object v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/Eraser;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Eraser;

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNativeExifJPGBuff:J

    return-wide v0
.end method

.method private fromRealToMark([Landroid/graphics/Rect;Lcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)[Landroid/graphics/Rect;
    .locals 5
    .param p1, "realRects"    # [Landroid/graphics/Rect;
    .param p2, "markSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;
    .param p3, "realSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 1644
    const/4 v1, 0x0

    .line 1645
    .local v1, "markRects":[Landroid/graphics/Rect;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 1646
    array-length v2, p1

    new-array v1, v2, [Landroid/graphics/Rect;

    .line 1647
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 1649
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v1, v0

    .line 1650
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    mul-int/2addr v3, v4

    iget v4, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 1651
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    mul-int/2addr v3, v4

    iget v4, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1652
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    mul-int/2addr v3, v4

    iget v4, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 1653
    aget-object v2, v1, v0

    aget-object v3, p1, v0

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    mul-int/2addr v3, v4

    iget v4, p3, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    div-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 1647
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1656
    .end local v0    # "i":I
    :cond_0
    return-object v1
.end method

.method private hideActionBar()V
    .locals 2

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1081
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1083
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1086
    :cond_0
    return-void
.end method

.method private hidePopupToast()V
    .locals 1

    .prologue
    .line 1365
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    if-eqz v0, :cond_0

    .line 1366
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->hide()V

    .line 1368
    :cond_0
    return-void
.end method

.method private hideProgressDialog()V
    .locals 4

    .prologue
    .line 1057
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsActivityFinished:Z

    if-eqz v1, :cond_1

    .line 1077
    :cond_0
    :goto_0
    return-void

    .line 1060
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1061
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 1063
    :cond_2
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSharedPref:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v1, :cond_0

    .line 1064
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "TYPE_ERASER"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1065
    .local v0, "isFirstIn":Z
    if-eqz v0, :cond_3

    .line 1066
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "TYPE_ERASER"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1067
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1068
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 1069
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->show(I)V

    goto :goto_0

    .line 1071
    :cond_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v1, :cond_0

    .line 1072
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    goto :goto_0
.end method

.method private initAnimation()V
    .locals 9

    .prologue
    .line 1018
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 1021
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1023
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, -0x40800000    # -1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    .line 1026
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHideTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1028
    return-void
.end method

.method private initSDCardActionReceiver()V
    .locals 2

    .prologue
    .line 1584
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1585
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1586
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1587
    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1588
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1589
    new-instance v1, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;

    .line 1590
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1591
    return-void
.end method

.method private initUI()V
    .locals 2

    .prologue
    .line 671
    const v1, 0x7f090091

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    .line 672
    const v1, 0x7f090092

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceTextView:Landroid/widget/TextView;

    .line 673
    const v1, 0x7f090093

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    .line 675
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->initDefaultActionbarUI()V

    .line 676
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnExitDialogClickListener:Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;

    invoke-direct {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;-><init>(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;)V

    .line 677
    .local v0, "dialogCallBack":Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->setBtnOnClick(Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack;)V

    .line 679
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mUIinited:Z

    .line 680
    return-void
.end method

.method private initZoomView()V
    .locals 4

    .prologue
    .line 244
    const v0, 0x7f09003c

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    .line 245
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    .line 246
    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    .line 247
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    .line 248
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;

    .line 249
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;->setZoomControl(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;)V

    .line 250
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMySingleTapUpCallback:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener$MySingleTapUpCallback;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;->setMySingleTapUpCallback(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener$MySingleTapUpCallback;)V

    .line 251
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    .line 252
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setZoomState(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;)V

    .line 253
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEdgeView:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setEdgeView(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/EdgeView;)V

    .line 254
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->setAspectQuotient(Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;)V

    .line 255
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->resetZoomState()V

    .line 256
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPinchZoomListener:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 257
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    const v1, 0x7f02009e

    const v2, 0x7f0200a2

    const v3, 0x7f0200a3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->initRect(Landroid/content/Context;III)V

    .line 258
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setDrawFaceRect(Z)V

    .line 259
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setFaceRectRotateDegree(I)V

    .line 261
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 262
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setFirstImage()V

    .line 267
    :goto_0
    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsFirstImageSetDone:Z

    .line 265
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideFirstImage()V

    goto :goto_0
.end method

.method private maskRectTap(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 304
    if-ltz p1, :cond_0

    .line 324
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setCurMovingObjIndex(I)V

    .line 325
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->changeSaveBtnState(Z)V

    .line 326
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setUserEditFlag()V

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->switchActionBar()V

    goto :goto_0
.end method

.method private popUpToast(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1307
    move-object v0, p1

    .line 1308
    .local v0, "pressedView":Landroid/view/View;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v1

    .line 1309
    .local v1, "screenWith":I
    const/4 v2, 0x0

    .line 1311
    .local v2, "textRes":I
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->getInstance(Landroid/content/Context;)Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    .line 1312
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 1330
    :goto_0
    if-eqz v2, :cond_0

    .line 1331
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mToastInstance:Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;

    invoke-virtual {v3, v0, v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/PopupToast;->showPopUpToast(Landroid/view/View;II)V

    .line 1334
    :cond_0
    const/4 v3, 0x1

    return v3

    .line 1314
    :sswitch_0
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z

    if-eqz v3, :cond_1

    const v2, 0x7f06000c

    .line 1315
    :goto_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveImageView:Landroid/widget/ImageView;

    .line 1316
    goto :goto_0

    .line 1314
    :cond_1
    const v2, 0x7f060018

    goto :goto_1

    .line 1318
    :sswitch_1
    const v2, 0x7f060027

    .line 1319
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 1320
    goto :goto_0

    .line 1322
    :sswitch_2
    const v2, 0x7f06000e

    .line 1323
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackImageView:Landroid/widget/ImageView;

    .line 1324
    goto :goto_0

    .line 1326
    :sswitch_3
    const v2, 0x7f06000d

    .line 1327
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayout:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 1312
    :sswitch_data_0
    .sparse-switch
        0x7f090042 -> :sswitch_1
        0x7f09004c -> :sswitch_2
        0x7f090053 -> :sswitch_0
        0x7f090059 -> :sswitch_3
    .end sparse-switch
.end method

.method private resetZoomState()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 297
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanX(F)V

    .line 298
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setPanY(F)V

    .line 299
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->setZoom(F)V

    .line 300
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->notifyObservers()V

    .line 301
    return-void
.end method

.method private saveResult()V
    .locals 3

    .prologue
    .line 744
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "saveResultFile_EX function in"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProcessFinished:Z

    if-nez v1, :cond_0

    .line 747
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "saveResultFile_EX piclear engine process not finished..."

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    :goto_0
    return-void

    .line 751
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    .line 753
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveThread:Ljava/lang/Thread;

    .line 760
    :cond_1
    new-instance v1, Lcom/arcsoft/magicshotstudio/Eraser$6;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/Eraser$6;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveThread:Ljava/lang/Thread;

    .line 788
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 789
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 790
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "saveResultFile_EX function out"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 754
    :catch_0
    move-exception v0

    .line 755
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method private saveResultFile()V
    .locals 4

    .prologue
    .line 733
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "saveResultFile function in"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsSavaButtonPressed:Z

    .line 735
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->saveResult()V

    .line 736
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 737
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->exitActivity()V

    .line 739
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "saveResultFile function out"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    return-void
.end method

.method private setActivityResult()V
    .locals 3

    .prologue
    .line 990
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 991
    .local v0, "res":Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsSavaButtonPressed:Z

    if-eqz v1, :cond_0

    .line 992
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "setResult(RESULT_OK, intent)"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    const-string v1, "path"

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 994
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->setResult(ILandroid/content/Intent;)V

    .line 1000
    :goto_0
    return-void

    .line 997
    :cond_0
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "setResult(RESULT_CANCELED, intent)"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private setFirstImage()V
    .locals 2

    .prologue
    .line 270
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setImage(Landroid/graphics/Bitmap;)V

    .line 280
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsFirstImageSetDone:Z

    .line 281
    return-void

    .line 275
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideFirstImage()V

    goto :goto_0

    .line 278
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideFirstImage()V

    goto :goto_0
.end method

.method private showActionBar()V
    .locals 2

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1090
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1091
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V

    .line 1092
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShowTranslateAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1093
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1096
    :cond_0
    return-void
.end method

.method private showProgressDialog()V
    .locals 1

    .prologue
    .line 1045
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsActivityFinished:Z

    if-eqz v0, :cond_1

    .line 1054
    :cond_0
    :goto_0
    return-void

    .line 1048
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_2

    .line 1050
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 1052
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    goto :goto_0
.end method

.method private startEngineThread(IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 7
    .param p1, "photoNum"    # I
    .param p2, "mode"    # I
    .param p3, "realImageSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;
    .param p4, "markImageSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 390
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 396
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    .line 398
    :cond_0
    new-instance v0, Lcom/arcsoft/magicshotstudio/Eraser$2;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/arcsoft/magicshotstudio/Eraser$2;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    .line 412
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 413
    return-void

    .line 393
    :catch_0
    move-exception v6

    .line 394
    .local v6, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private unInit()V
    .locals 3

    .prologue
    .line 1237
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "unInit() in"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 1239
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 1241
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1245
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mInitEngineThread:Ljava/lang/Thread;

    .line 1248
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    if-eqz v1, :cond_1

    .line 1249
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "unInit() aaaa"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->unInit()V

    .line 1253
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1254
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "unInit() out"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    return-void

    .line 1242
    :catch_0
    move-exception v0

    .line 1243
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateGapValue(Z)V
    .locals 1
    .param p1, "isLandScape"    # Z

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mUIinited:Z

    if-eqz v0, :cond_0

    .line 521
    if-eqz p1, :cond_0

    .line 540
    :cond_0
    return-void
.end method

.method private updateNoMarkImage()V
    .locals 4

    .prologue
    .line 334
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmapNeedUpdate:Z

    if-eqz v2, :cond_0

    .line 335
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getNoMarkImage()[B

    move-result-object v0

    .line 336
    .local v0, "noMarkImageBytes":[B
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    invoke-static {v0, v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->decodeBitmapFromArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 337
    .local v1, "noMaskBitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->updateAutoModeBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 338
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmapNeedUpdate:Z

    .line 340
    .end local v0    # "noMarkImageBytes":[B
    .end local v1    # "noMaskBitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelDelDailog()V
    .locals 1

    .prologue
    .line 653
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->cancelDelDailog()V

    .line 654
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 655
    return-void
.end method

.method public changeSaveBtnState(Z)V
    .locals 5
    .param p1, "isSaveState"    # Z

    .prologue
    const v3, 0x7f060018

    const v2, 0x7f06000c

    .line 658
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 659
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    const v1, 0x7f02000f

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 660
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(I)V

    .line 662
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 663
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 664
    .local v0, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 666
    .end local v0    # "description":Ljava/lang/String;
    :cond_1
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z

    .line 667
    return-void

    .line 659
    :cond_2
    const v1, 0x7f020013

    goto :goto_0

    :cond_3
    move v1, v3

    .line 660
    goto :goto_1

    :cond_4
    move v2, v3

    .line 663
    goto :goto_2
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 1339
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "Eraser finish !!!"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsActivityFinished:Z

    if-eqz v0, :cond_0

    .line 1346
    :goto_0
    return-void

    .line 1343
    :cond_0
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V

    .line 1344
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsActivityFinished:Z

    .line 1345
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    goto :goto_0
.end method

.method public formScreenToImage(II)Landroid/graphics/Point;
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1372
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    .line 1373
    .local v0, "point":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->updateZoomState()V

    .line 1375
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_ScaleX:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_OffsetX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 1376
    iget v1, v0, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_ScaleY:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->m_OffsetY:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 1378
    iget v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-static {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->rotatePoint(Landroid/graphics/Point;ILcom/arcsoft/magicshotstudio/utils/MSize;)Landroid/graphics/Point;

    move-result-object v0

    .line 1380
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    div-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 1381
    iget v1, v0, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v2, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    div-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 1383
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "real pointX = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "real pointY = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    return-object v0
.end method

.method public getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method public getMaskNum()I
    .locals 1

    .prologue
    .line 936
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskNum:I

    return v0
.end method

.method public getMaxHeight()I
    .locals 3

    .prologue
    .line 1477
    const/4 v0, 0x0

    .line 1478
    .local v0, "temp":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 1479
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40200000    # 2.5f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 1481
    :cond_0
    return v0
.end method

.method public getMaxWidth()I
    .locals 3

    .prologue
    .line 1470
    const/4 v0, 0x0

    .line 1471
    .local v0, "temp":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 1472
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40200000    # 2.5f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 1474
    :cond_0
    return v0
.end method

.method public getOffsetX()I
    .locals 3

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_0

    .line 1538
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1540
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    neg-float v0, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getOffsetY()I
    .locals 3

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_0

    .line 1547
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 1549
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    neg-float v0, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getOperateEnable()Z
    .locals 1

    .prologue
    .line 698
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z

    return v0
.end method

.method public getProcessResult()V
    .locals 14

    .prologue
    .line 1120
    sget-object v10, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v11, "getProcessResult() ------  start"

    invoke-static {v10, v11}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    const/4 v8, 0x0

    .line 1123
    .local v8, "maskBitmap":Landroid/graphics/Bitmap;
    const/4 v9, 0x0

    .line 1124
    .local v9, "noMaskBitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1126
    .local v0, "getMarkImageTime":J
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getFirstImage()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1127
    sget-object v10, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cost_time getMarkImage() time = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v0

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1133
    .local v6, "getProcessResultTime":J
    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v10}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getProcessResult()I

    move-result v10

    iput v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskNum:I

    .line 1135
    sget-object v10, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cost_time getProcessResult() time = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v6

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1142
    .local v4, "getProcessObjectInMarkImageTime":J
    sget-object v10, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cost_time getProcessObjectInMarkImage(-1, -1) time = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v4

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1161
    .local v2, "getNoMarkImageTime":J
    invoke-virtual {p0, v8, v9}, Lcom/arcsoft/magicshotstudio/Eraser;->updateAutoModeBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 1162
    sget-object v10, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v11, "getProcessResult() ------  end"

    invoke-static {v10, v11}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    return-void
.end method

.method public getRectDst()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1484
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1693
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 1694
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getEraserSavePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    .line 1697
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method public getZoomOutPoint()Landroid/graphics/Point;
    .locals 12

    .prologue
    .line 1492
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 1493
    .local v4, "point":Landroid/graphics/Point;
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 1494
    .local v1, "bitmapWidth":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 1495
    .local v0, "bitmapHeight":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v6

    .line 1496
    .local v6, "viewWidth":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v5

    .line 1497
    .local v5, "viewHeight":I
    if-le v6, v5, :cond_0

    .line 1498
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getLandscapeFitInBitmapRect()Landroid/graphics/Rect;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    .line 1503
    :goto_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->updateZoomState()V

    .line 1505
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    mul-int/2addr v9, v1

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    div-int/2addr v9, v10

    iput v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawWidth:I

    .line 1506
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    mul-int/2addr v9, v0

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    div-int/2addr v9, v10

    iput v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawHeight:I

    .line 1508
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getOffsetX()I

    move-result v9

    neg-int v2, v9

    .line 1509
    .local v2, "offset_x":I
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getOffsetY()I

    move-result v9

    neg-int v3, v9

    .line 1511
    .local v3, "offset_y":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v2

    iget v11, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawWidth:I

    sub-int/2addr v10, v11

    mul-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    iget v11, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawWidth:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-int v7, v9

    .line 1514
    .local v7, "x":I
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v10, v3

    iget v11, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawHeight:I

    sub-int/2addr v10, v11

    mul-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    iget v11, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawHeight:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    div-float/2addr v9, v10

    float-to-int v8, v9

    .line 1518
    .local v8, "y":I
    iget v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawWidth:I

    if-gt v9, v6, :cond_1

    .line 1519
    div-int/lit8 v9, v6, 0x2

    iput v9, v4, Landroid/graphics/Point;->x:I

    .line 1525
    :goto_1
    iget v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->bitmapDrawHeight:I

    if-gt v9, v5, :cond_2

    .line 1526
    div-int/lit8 v9, v5, 0x2

    iput v9, v4, Landroid/graphics/Point;->y:I

    .line 1532
    :goto_2
    return-object v4

    .line 1500
    .end local v2    # "offset_x":I
    .end local v3    # "offset_y":I
    .end local v7    # "x":I
    .end local v8    # "y":I
    :cond_0
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v9}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getPortraitFitInBitmapRect()Landroid/graphics/Rect;

    move-result-object v9

    iput-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    goto :goto_0

    .line 1523
    .restart local v2    # "offset_x":I
    .restart local v3    # "offset_y":I
    .restart local v7    # "x":I
    .restart local v8    # "y":I
    :cond_1
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v7

    iput v9, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 1530
    :cond_2
    iget-object v9, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFitInBitmapRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v8

    iput v9, v4, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 810
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->handleMessage(Landroid/os/Message;)V

    .line 811
    sget-object v3, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMessage...."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const/4 v2, 0x0

    .line 813
    .local v2, "progress":I
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 907
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 815
    :pswitch_1
    iget-boolean v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mKeepSrcFiles:Z

    if-nez v3, :cond_0

    .line 816
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 817
    .local v0, "ArcPiClear_deleteSourceFile":J
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;

    invoke-static {p0, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteSourceFile(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 818
    sget-object v3, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ArcPiClear_deleteSourceFile is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 826
    .end local v0    # "ArcPiClear_deleteSourceFile":J
    :pswitch_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideProgressDialog()V

    .line 827
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setActivityResult()V

    goto :goto_0

    .line 830
    :pswitch_3
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    .line 832
    :pswitch_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hide_description()V

    goto :goto_0

    .line 835
    :pswitch_5
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hide_ManualFailTextLayout()V

    goto :goto_0

    .line 838
    :pswitch_6
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideProgressDialog()V

    .line 839
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v3

    if-nez v3, :cond_0

    .line 840
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 841
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v3, v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setVisibility(I)V

    goto :goto_0

    .line 845
    :pswitch_7
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->invalidate()V

    goto :goto_0

    .line 848
    :pswitch_8
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v3, :cond_0

    .line 849
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getArcPiClear()Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    move-result-object v3

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_GetProgressIndex()I

    move-result v2

    .line 850
    iget v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressNum:I

    if-eqz v3, :cond_1

    .line 851
    iget v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressNum:I

    mul-int/lit8 v4, v2, 0x1e

    div-int/lit8 v4, v4, 0x64

    add-int v2, v3, v4

    .line 853
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    goto :goto_0

    .line 857
    :pswitch_9
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideProgressDialog()V

    .line 858
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setActivityResult()V

    .line 859
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    goto/16 :goto_0

    .line 862
    :pswitch_a
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x800

    invoke-virtual {v3, v4}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_0

    .line 867
    :pswitch_b
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->onProcessImgFinish()V

    goto/16 :goto_0

    .line 871
    :pswitch_c
    const-string v3, "Get infomation error!"

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 873
    sget-object v3, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v4, "Can not get the correct infomation from intent."

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 877
    :pswitch_d
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 878
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v4, 0x7f060015

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 880
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    goto/16 :goto_0

    .line 884
    :pswitch_e
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v3, :cond_0

    .line 885
    iget v3, p1, Landroid/os/Message;->arg1:I

    mul-int/lit8 v3, v3, 0x46

    div-int/lit8 v2, v3, 0x64

    .line 886
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 887
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressNum:I

    goto/16 :goto_0

    .line 896
    :pswitch_f
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->startMode()I

    goto/16 :goto_0

    .line 900
    :pswitch_10
    const v3, 0x7f06002f

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;II)V

    .line 902
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    goto/16 :goto_0

    .line 813
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public hideFirstImage()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setVisibility(I)V

    .line 294
    :cond_0
    return-void
.end method

.method public hide_ManualFailTextLayout()V
    .locals 2

    .prologue
    .line 916
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->setVisibility(I)V

    .line 919
    :cond_0
    return-void
.end method

.method public hide_description()V
    .locals 2

    .prologue
    .line 910
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->setVisibility(I)V

    .line 913
    :cond_0
    return-void
.end method

.method protected inflateHorizontalProgressingView()V
    .locals 2

    .prologue
    .line 1031
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-nez v0, :cond_0

    .line 1032
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 1034
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v1, 0x7f060015

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 1035
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressNum:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 1036
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    new-instance v1, Lcom/arcsoft/magicshotstudio/Eraser$7;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/Eraser$7;-><init>(Lcom/arcsoft/magicshotstudio/Eraser;)V

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1042
    return-void
.end method

.method public initDefaultActionbarUI()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 445
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mShareDialog:Lcom/arcsoft/magicshotstudio/ui/common/ShareDialog;

    .line 446
    const v2, 0x7f090041

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    .line 448
    const v2, 0x7f090053

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    .line 449
    const v2, 0x7f09004c

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 450
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v2}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 451
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v2}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 452
    const v2, 0x7f090059

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayout:Landroid/widget/RelativeLayout;

    .line 453
    const v2, 0x7f090042

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    .line 454
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f06001d

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/Eraser;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 455
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-static {p0, v2}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 456
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v2

    if-nez v2, :cond_0

    .line 457
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 459
    :cond_0
    const v2, 0x7f090077

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEditImageView:Landroid/widget/ImageView;

    .line 460
    const v2, 0x7f090055

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveImageView:Landroid/widget/ImageView;

    .line 461
    const v2, 0x7f09004f

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackImageView:Landroid/widget/ImageView;

    .line 462
    const v2, 0x7f09005c

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuImageView:Landroid/widget/ImageView;

    .line 464
    const v2, 0x7f090043

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowImageView:Landroid/widget/ImageView;

    .line 466
    const v2, 0x7f090057

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveTextView:Landroid/widget/TextView;

    .line 467
    const v2, 0x7f090051

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackTextView:Landroid/widget/TextView;

    .line 468
    const v2, 0x7f09005e

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuTextView:Landroid/widget/TextView;

    .line 471
    const v2, 0x7f090058

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayoutGap3:Landroid/view/View;

    .line 472
    const v2, 0x7f090052

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayoutGap3:Landroid/view/View;

    .line 473
    const v2, 0x7f09005f

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayoutGap3:Landroid/view/View;

    .line 475
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 476
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 477
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    .line 478
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 479
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 481
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_2

    .line 482
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 484
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 487
    :cond_2
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_3

    .line 488
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 489
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 490
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMenuLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 493
    :cond_3
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_4

    .line 494
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 496
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 500
    :cond_4
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFromGallery:Z

    if-eqz v2, :cond_5

    .line 501
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowImageView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 516
    :goto_0
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->updateGapValue(Z)V

    .line 517
    return-void

    .line 503
    :cond_5
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowImageView:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 504
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_6

    .line 505
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 507
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBackArrowLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 509
    :cond_6
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 510
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 511
    .local v0, "density":F
    const/high16 v2, 0x41600000    # 14.0f

    mul-float/2addr v2, v0

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 512
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected onActivityServiceConnected()V
    .locals 1

    .prologue
    .line 284
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onActivityServiceConnected()V

    .line 285
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsFirstImageSetDone:Z

    if-nez v0, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setFirstImage()V

    .line 288
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 965
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    .line 967
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 987
    :cond_0
    :goto_0
    return-void

    .line 971
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mbIsSaveBtnOn:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsUserEdit:Z

    if-eqz v0, :cond_2

    .line 972
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 976
    :cond_2
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z

    if-eqz v0, :cond_0

    .line 979
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFromGallery:Z

    if-nez v0, :cond_3

    .line 980
    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 981
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 982
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    goto :goto_0

    .line 984
    :cond_3
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 1100
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "onConfigurationChanged in"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V

    .line 1102
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1103
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    .line 1108
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->updateGapValue(Z)V

    .line 1109
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1110
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1, p1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->updateHelpConfig(ILandroid/content/res/Configuration;)V

    .line 1112
    :cond_1
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1113
    return-void

    .line 1105
    :cond_2
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    .line 1106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 153
    const-string v0, "MagicShotApplication"

    const-string v1, "Eraser onCreate1"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 155
    const-string v0, "MagicShotApplication"

    const-string v1, "Eraser onCreate2"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    .line 157
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 161
    return-void
.end method

.method protected onDestroy()V
    .locals 10

    .prologue
    .line 1259
    sget-object v6, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v7, "Eraser onDestroy in"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1260
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    if-eqz v6, :cond_0

    .line 1261
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1262
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->unInit()V

    .line 1267
    :cond_0
    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mKeepSrcFiles:Z

    if-nez v6, :cond_1

    .line 1268
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1269
    .local v0, "ArcPiClear_deleteSourceFile":J
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFileList:Ljava/util/ArrayList;

    invoke-static {p0, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteSourceFile(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 1270
    sget-object v6, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ArcPiClear_deleteSourceFile is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    .end local v0    # "ArcPiClear_deleteSourceFile":J
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->unInit()V

    .line 1274
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mUIinited:Z

    .line 1276
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    if-eqz v6, :cond_2

    .line 1277
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mZoomControl:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcZoomControl;->getZoomState()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->deleteObservers()V

    .line 1279
    :cond_2
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1280
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1282
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    if-eqz v6, :cond_3

    .line 1283
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    .local v2, "arr$":[Landroid/graphics/Bitmap;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_3

    aget-object v3, v2, v4

    .line 1284
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 1283
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1287
    .end local v2    # "arr$":[Landroid/graphics/Bitmap;
    .end local v3    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_3
    sget-object v6, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v7, "Eraser onDestroy out"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onDestroy()V

    .line 1289
    return-void
.end method

.method public onHomeKeyPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1661
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z

    if-nez v0, :cond_0

    .line 1687
    :goto_0
    return-void

    .line 1664
    :cond_0
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "onUserLeaveHint in"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1665
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1666
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsActivityFinished:Z

    if-nez v0, :cond_1

    .line 1667
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 1685
    :cond_1
    :goto_1
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "onUserLeaveHint out"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1686
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onHomeKeyPressed()V

    goto :goto_0

    .line 1670
    :cond_2
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mFromGallery:Z

    if-nez v0, :cond_1

    .line 1671
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_3

    .line 1672
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 1674
    :cond_3
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 1676
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsSavaButtonPressed:Z

    .line 1677
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->saveResult()V

    .line 1678
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideProgressDialog()V

    .line 1679
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->setActivityResult()V

    .line 1680
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    .line 1681
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 685
    invoke-super {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 690
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onPause()V

    .line 691
    return-void
.end method

.method public onProcessImgFinish()V
    .locals 11

    .prologue
    const v10, 0x7f060023

    const/16 v9, 0x64

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1167
    sget-object v4, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v5, "onProcessImgFinish() in"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProcessFinished:Z

    .line 1169
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_7

    .line 1170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1171
    .local v2, "updateImageTime":J
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getRealImageSize()[I

    move-result-object v1

    .line 1172
    .local v1, "realImageSizeFromJni":[I
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    if-eqz v4, :cond_0

    .line 1173
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    aget v5, v1, v8

    iput v5, v4, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 1174
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    aget v5, v1, v6

    iput v5, v4, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 1175
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setOriginalImageSize(Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    .line 1179
    :cond_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getMaskRect()[Landroid/graphics/Rect;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskRectArray:[Landroid/graphics/Rect;

    .line 1180
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskRectArray:[Landroid/graphics/Rect;

    if-eqz v4, :cond_1

    .line 1182
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskRectArray:[Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setFaceRect([Landroid/graphics/Rect;)V

    .line 1186
    :cond_1
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getMovingObjBitmap()[Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    .line 1187
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    .line 1188
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 1189
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v0

    iget v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v4, v0

    .line 1188
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1191
    :cond_2
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMovingObjBitmapArray:[Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setMovingObjBitmaps([Landroid/graphics/Bitmap;)V

    .line 1195
    .end local v0    # "i":I
    :cond_3
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->setImageIndex(I)V

    .line 1197
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v4, :cond_4

    .line 1198
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v4, v9}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 1201
    :cond_4
    iget v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskNum:I

    if-nez v4, :cond_5

    .line 1203
    invoke-virtual {p0, v8, v10}, Lcom/arcsoft/magicshotstudio/Eraser;->showDescriptionMessage(ZI)V

    .line 1209
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-gez v4, :cond_6

    .line 1210
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x5

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1218
    :goto_1
    iget-boolean v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    invoke-direct {p0, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->updateGapValue(Z)V

    .line 1219
    sget-object v4, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cost_time  updateImageTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    sget-object v4, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v5, "onProcessImgFinish() out"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    sget-object v4, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cost_time  total_cost_time = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mCostTimeTotal:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    .end local v1    # "realImageSizeFromJni":[I
    .end local v2    # "updateImageTime":J
    :goto_2
    sget-object v4, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v5, "onProcessImgFinish out"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    return-void

    .line 1213
    .restart local v1    # "realImageSizeFromJni":[I
    .restart local v2    # "updateImageTime":J
    :cond_6
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideProgressDialog()V

    .line 1214
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1215
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v4, v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setVisibility(I)V

    goto :goto_1

    .line 1226
    .end local v1    # "realImageSizeFromJni":[I
    .end local v2    # "updateImageTime":J
    :cond_7
    sget-object v4, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v5, "onProcessImgFinish() mMaskBitmap == null"

    invoke-static {v4, v5}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    invoke-virtual {p0, v8, v10}, Lcom/arcsoft/magicshotstudio/Eraser;->showDescriptionMessage(ZI)V

    .line 1228
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v4, v9}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 1229
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideProgressDialog()V

    .line 1230
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1231
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v4, v8}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 418
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onResume()V

    .line 420
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->initSDCardActionReceiver()V

    .line 240
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStart()V

    .line 241
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 1562
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V

    .line 1564
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSDCardStateReceiver:Lcom/arcsoft/magicshotstudio/Eraser$SdcardStateChanageReceiver;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1565
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStop()V

    .line 1566
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 1557
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onUserLeaveHint()V

    .line 1558
    return-void
.end method

.method public pointTransform(II)Landroid/graphics/PointF;
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1449
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 1450
    .local v1, "pointF":Landroid/graphics/PointF;
    invoke-virtual {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/Eraser;->formScreenToImage(II)Landroid/graphics/Point;

    move-result-object v0

    .line 1451
    .local v0, "point":Landroid/graphics/Point;
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 1452
    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget v3, v3, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 1453
    return-object v1
.end method

.method public removeHandlerMessageAndHideDescriptionMessage()V
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 955
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hide_description()V

    .line 956
    return-void
.end method

.method public removeHandlerMessageAndHideTextView()V
    .locals 2

    .prologue
    .line 959
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 960
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hide_ManualFailTextLayout()V

    .line 961
    return-void
.end method

.method public setImageIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 368
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setImageIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const/4 p1, 0x2

    .line 370
    packed-switch p1, :pswitch_data_0

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 372
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 377
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOperateEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 695
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z

    .line 696
    return-void
.end method

.method public showDescriptionMessage(ZI)V
    .locals 4
    .param p1, "moving_objs_detected"    # Z
    .param p2, "string_id"    # I

    .prologue
    .line 940
    if-eqz p1, :cond_1

    .line 941
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "no moving objects detected"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    :cond_0
    :goto_0
    return-void

    .line 944
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    if-eqz v0, :cond_0

    .line 945
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->removeHandlerMessageAndHideTextView()V

    .line 946
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 947
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTapFaceView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->setVisibility(I)V

    .line 948
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public showMessageIsManualSucceed()V
    .locals 4

    .prologue
    .line 928
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "manual mode: ProcessManualObjectInMarkImage() result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getProcessResult()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    sget-object v0, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v1, "manual mode: ArcPiClear.MOR_BLANK = 28672"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->show_ManualFailTextLayout()V

    .line 931
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 932
    return-void
.end method

.method public show_ManualFailTextLayout()V
    .locals 2

    .prologue
    .line 922
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 923
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mManualFailTextLayout:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ArcRelativeLayout;->setVisibility(I)V

    .line 925
    :cond_0
    return-void
.end method

.method protected startMode()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 165
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMode()I

    move-result v1

    .line 166
    .local v1, "res":I
    if-eqz v1, :cond_0

    .line 167
    sget-object v2, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v3, "Can not get the correct infomation from intent."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    .line 230
    :goto_0
    return v1

    .line 174
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProgressTime:J

    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mCostTimeTotal:J

    .line 177
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSrcImageSize:[I

    aget v3, v3, v4

    iput v3, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 178
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSrcImageSize:[I

    aget v3, v3, v5

    iput v3, v2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 179
    const/4 v2, 0x5

    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPhotoNum:I

    .line 180
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mProcessFinished:Z

    .line 186
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->setProcessListener(Lcom/arcsoft/magicshotstudio/ui/picclear/ProcessListener;)V

    .line 188
    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBitmapCount:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mBitmapCount:I

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->extractArrayList(II)[I

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSelectIndexes:[I

    .line 189
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPicClearEngine:Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/PicClearEngine;->getArcPiClear()Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;

    move-result-object v2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/jni/picclear/ArcPiClear;->ArcPiClear_SetProgressHandler(Landroid/os/Handler;)V

    .line 190
    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mPhotoNum:I

    iget v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMode:I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRealImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMarkImageSize:Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/Eraser;->startEngineThread(IILcom/arcsoft/magicshotstudio/utils/MSize;Lcom/arcsoft/magicshotstudio/utils/MSize;)V

    .line 192
    sget-object v3, Lcom/arcsoft/magicshotstudio/Eraser;->LOCK_OBJ2:Ljava/lang/Object;

    monitor-enter v3

    .line 193
    :try_start_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v4, 0x7f030013

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mContentView:Landroid/view/View;

    .line 194
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mContentView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setContentView(Landroid/view/View;)V

    .line 195
    const v2, 0x7f090067

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mActionbarLayout:Landroid/widget/RelativeLayout;

    .line 196
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mActionbarLayout:Landroid/widget/RelativeLayout;

    const v4, 0x7f090044

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 197
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    const v4, 0x7f06001d

    invoke-virtual {v2, v4}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setText(I)V

    .line 198
    const v2, 0x7f090046

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 199
    const v2, 0x7f09004d

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 200
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 201
    .local v0, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 202
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenDensity:F

    .line 203
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenWidthInitActivity:I

    .line 204
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenHeightInitAcitivy:I

    .line 206
    iget v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenWidthInitActivity:I

    iget v4, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mScreenHeightInitAcitivy:I

    if-le v2, v4, :cond_1

    .line 207
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    .line 213
    :goto_1
    const-string v2, "MAGICSHOT_SPF_Key"

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, Lcom/arcsoft/magicshotstudio/Eraser;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSharedPref:Landroid/content/SharedPreferences;

    .line 214
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 216
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->initUI()V

    .line 217
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->initAnimation()V

    .line 218
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->inflateHorizontalProgressingView()V

    .line 220
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mSavePath:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 221
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->initZoomView()V

    .line 222
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->showProgressDialog()V

    .line 228
    :goto_2
    monitor-exit v3

    goto/16 :goto_0

    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 209
    .restart local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mIsLandScape:Z

    goto :goto_1

    .line 225
    :cond_2
    sget-object v2, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v4, "SrcImage input is not correct"

    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public switchActionBar()V
    .locals 1

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mDefaultActionBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1005
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->hideActionBar()V

    .line 1011
    :cond_0
    :goto_0
    return-void

    .line 1008
    :cond_1
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Eraser;->showActionBar()V

    goto :goto_0
.end method

.method public updateAutoModeBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "maskBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "noMaskBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 343
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "updateAutoModeBitmap() in"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    if-eqz p2, :cond_1

    .line 346
    iget v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 347
    .local v0, "temp":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 350
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 352
    :cond_0
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mNoMaskBitmap:Landroid/graphics/Bitmap;

    .line 355
    .end local v0    # "temp":Landroid/graphics/Bitmap;
    :cond_1
    if-eqz p1, :cond_3

    .line 356
    iget v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mOrientation:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 357
    .restart local v0    # "temp":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 359
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 360
    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 362
    :cond_2
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 364
    .end local v0    # "temp":Landroid/graphics/Bitmap;
    :cond_3
    sget-object v1, Lcom/arcsoft/magicshotstudio/Eraser;->tag:Ljava/lang/String;

    const-string v2, "updateAutoModeBitmap() out"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    return-void
.end method

.method public updateZoomPointX()I
    .locals 4

    .prologue
    .line 1460
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v1

    .line 1461
    .local v1, "viewWidth":I
    const v2, 0x3f2aaaab

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v1, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 1462
    .local v0, "temp":I
    return v0
.end method

.method public updateZoomPointY()I
    .locals 4

    .prologue
    .line 1465
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v1

    .line 1466
    .local v1, "viewHeight":I
    const v2, 0x3f2aaaab

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v1, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 1467
    .local v0, "temp":I
    return v0
.end method

.method public updateZoomState()V
    .locals 17

    .prologue
    .line 1395
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getAspectQuotient()Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;

    move-result-object v4

    .line 1396
    .local v4, "mAspectQuotient":Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;
    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcAspectQuotient;->get()F

    move-result v1

    .line 1398
    .local v1, "aspectQuotient":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getWidth()I

    move-result v10

    .line 1399
    .local v10, "viewWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getHeight()I

    move-result v9

    .line 1400
    .local v9, "viewHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1401
    .local v3, "bitmapWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 1403
    .local v2, "bitmapHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanX()F

    move-result v5

    .line 1404
    .local v5, "panX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v13}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getPanY()F

    move-result v6

    .line 1405
    .local v6, "panY":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v13, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomX(F)F

    move-result v13

    int-to-float v14, v10

    mul-float/2addr v13, v14

    int-to-float v14, v3

    div-float v11, v13, v14

    .line 1406
    .local v11, "zoomX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mState:Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;

    invoke-virtual {v13, v1}, Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ZoomState;->getZoomY(F)F

    move-result v13

    int-to-float v14, v9

    mul-float/2addr v13, v14

    int-to-float v14, v2

    div-float v12, v13, v14

    .line 1408
    .local v12, "zoomY":F
    int-to-float v13, v3

    mul-float/2addr v13, v5

    int-to-float v14, v10

    const/high16 v15, 0x40000000    # 2.0f

    mul-float/2addr v15, v11

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    float-to-int v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->m_OffsetX:I

    .line 1409
    int-to-float v13, v2

    mul-float/2addr v13, v6

    int-to-float v14, v9

    const/high16 v15, 0x40000000    # 2.0f

    mul-float/2addr v15, v12

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    float-to-int v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->m_OffsetY:I

    .line 1411
    int-to-float v13, v10

    div-float/2addr v13, v11

    float-to-int v8, v13

    .line 1412
    .local v8, "srcWidth":I
    int-to-float v13, v9

    div-float/2addr v13, v12

    float-to-int v7, v13

    .line 1415
    .local v7, "srcHeight":I
    int-to-float v13, v8

    int-to-float v14, v10

    div-float/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->m_ScaleX:F

    .line 1416
    int-to-float v13, v7

    int-to-float v14, v9

    div-float/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->m_ScaleY:F

    .line 1419
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v14, v3

    mul-float/2addr v14, v5

    int-to-float v15, v10

    const/high16 v16, 0x40000000    # 2.0f

    mul-float v16, v16, v11

    div-float v15, v15, v16

    sub-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->left:I

    .line 1420
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    int-to-float v14, v2

    mul-float/2addr v14, v6

    int-to-float v15, v9

    const/high16 v16, 0x40000000    # 2.0f

    mul-float v16, v16, v12

    div-float v15, v15, v16

    sub-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->top:I

    .line 1421
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    int-to-float v15, v10

    div-float/2addr v15, v11

    add-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->right:I

    .line 1422
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    int-to-float v15, v9

    div-float/2addr v15, v12

    add-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->bottom:I

    .line 1423
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    const/4 v14, 0x0

    iput v14, v13, Landroid/graphics/Rect;->left:I

    .line 1424
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    const/4 v14, 0x0

    iput v14, v13, Landroid/graphics/Rect;->top:I

    .line 1425
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iput v10, v13, Landroid/graphics/Rect;->right:I

    .line 1426
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iput v9, v13, Landroid/graphics/Rect;->bottom:I

    .line 1427
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    if-gez v13, :cond_0

    .line 1428
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    neg-int v15, v15

    int-to-float v15, v15

    mul-float/2addr v15, v11

    add-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->left:I

    .line 1429
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    const/4 v14, 0x0

    iput v14, v13, Landroid/graphics/Rect;->left:I

    .line 1431
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    if-le v13, v3, :cond_1

    .line 1432
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->right:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->right:I

    sub-int/2addr v15, v3

    int-to-float v15, v15

    mul-float/2addr v15, v11

    sub-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->right:I

    .line 1433
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iput v3, v13, Landroid/graphics/Rect;->right:I

    .line 1435
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    if-gez v13, :cond_2

    .line 1436
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    neg-int v15, v15

    int-to-float v15, v15

    mul-float/2addr v15, v12

    add-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->top:I

    .line 1437
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    const/4 v14, 0x0

    iput v14, v13, Landroid/graphics/Rect;->top:I

    .line 1439
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    if-le v13, v2, :cond_3

    .line 1440
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectDst:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v15, v2

    int-to-float v15, v15

    mul-float/2addr v15, v12

    sub-float/2addr v14, v15

    float-to-int v14, v14

    iput v14, v13, Landroid/graphics/Rect;->bottom:I

    .line 1441
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/arcsoft/magicshotstudio/Eraser;->mRectSrc:Landroid/graphics/Rect;

    iput v2, v13, Landroid/graphics/Rect;->bottom:I

    .line 1445
    :cond_3
    return-void
.end method
