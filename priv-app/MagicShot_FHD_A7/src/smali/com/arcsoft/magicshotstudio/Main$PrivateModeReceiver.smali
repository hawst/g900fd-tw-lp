.class Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrivateModeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Main;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/Main;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/Main;Lcom/arcsoft/magicshotstudio/Main$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/Main;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/Main$1;

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;-><init>(Lcom/arcsoft/magicshotstudio/Main;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 267
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    :cond_0
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    :cond_1
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 277
    :cond_2
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 279
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isFromPrivateFolder()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 280
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Main;->releaseAndExitService()V

    .line 281
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Main$PrivateModeReceiver;->this$0:Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Main;->finish()V

    .line 284
    :cond_3
    return-void
.end method
