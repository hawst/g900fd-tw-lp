.class Lcom/arcsoft/magicshotstudio/PicMotion$4;
.super Ljava/lang/Object;
.source "PicMotion.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/PicMotion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/PicMotion;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyAutoResult(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 186
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v1, "################### PicMotion onNotifyAutoResult #######"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    const/4 v1, 0x1

    # setter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mbProcessingFinish:Z
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$502(Lcom/arcsoft/magicshotstudio/PicMotion;Z)Z

    .line 188
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$600(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 189
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->hideFirstImage()V

    .line 190
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$700(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initEditData()Z

    .line 191
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$800(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$900(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getMainModeFlags()Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    move-result-object v0

    iget-boolean v0, v0, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mMotionEnable:Z

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$600(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 202
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$700(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showTopActionBar()V

    .line 203
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$700(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->showBottomLayout()V

    .line 204
    return-void
.end method

.method public onNotifyError(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    const/4 v3, 0x1

    .line 208
    sget-object v1, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v2, "################### PicMotion onNotifyError #######"

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # setter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mbProcessingFinish:Z
    invoke-static {v1, v3}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$502(Lcom/arcsoft/magicshotstudio/PicMotion;Z)Z

    .line 210
    const-string v0, ""

    .line 211
    .local v0, "errString":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 227
    :pswitch_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$600(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->dismiss()V

    .line 228
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-static {v1, v0, v3}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;Ljava/lang/String;I)V

    .line 229
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    .line 230
    return-void

    .line 211
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onNotifyProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 234
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "progress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressNum:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$1000(Lcom/arcsoft/magicshotstudio/PicMotion;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressNum:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$1000(Lcom/arcsoft/magicshotstudio/PicMotion;)I

    move-result v0

    mul-int/lit8 v1, p1, 0x1e

    div-int/lit8 v1, v1, 0x64

    add-int p1, v0, v1

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$4;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$600(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 239
    return-void
.end method
