.class public Lcom/arcsoft/magicshotstudio/BestPhoto;
.super Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.source "BestPhoto.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;
.implements Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;
.implements Lcom/arcsoft/magicshotstudio/ui/bestphoto/photoview/ArcTouchListener$MyTapUpCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;
    }
.end annotation


# static fields
.field public static final tag:Ljava/lang/String;


# instance fields
.field private BestPhotoStartTime:J

.field private final MAX_PROCESS:I

.field private final MSG_UPDATE_PROCESS:I

.field mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

.field mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

.field private mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

.field private mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mFirstImage:Landroid/widget/ImageView;

.field private mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

.field private mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

.field private mImgBack:Landroid/widget/ImageView;

.field private mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

.field private mImgPageLayout:Landroid/widget/RelativeLayout;

.field private mIndicateLayout:Landroid/widget/RelativeLayout;

.field private mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

.field private mIsFirstImageSetDone:Z

.field private mOnPageChangeListener:Landroid/support/v4/view/MyViewPager$OnPageChangeListener;

.field private mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

.field private mProgressNum:I

.field private mSaveLock:Ljava/util/concurrent/Semaphore;

.field private mSavedPaths:[Ljava/lang/String;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

.field private mbIsAnimDefaultBarIn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/arcsoft/magicshotstudio/BestPhoto;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/BestPhoto;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;-><init>()V

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->BestPhotoStartTime:J

    .line 60
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSavedPaths:[Ljava/lang/String;

    .line 66
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 68
    iput v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressNum:I

    .line 70
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIsFirstImageSetDone:Z

    .line 73
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 150
    new-instance v0, Lcom/arcsoft/magicshotstudio/BestPhoto$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/BestPhoto$1;-><init>(Lcom/arcsoft/magicshotstudio/BestPhoto;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mOnPageChangeListener:Landroid/support/v4/view/MyViewPager$OnPageChangeListener;

    .line 407
    const/16 v0, 0x1000

    iput v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->MSG_UPDATE_PROCESS:I

    .line 408
    const/16 v0, 0x64

    iput v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->MAX_PROCESS:I

    .line 555
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mbIsAnimDefaultBarIn:Z

    .line 556
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 557
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/BestPhoto;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->initUI()V

    return-void
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/BestPhoto;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->BestPhotoStartTime:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    return-object v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/BestPhoto;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mPreDataAddress:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/BestPhoto;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mPreDataSize:I

    return v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/BestPhoto;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBitmapAddress:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/BestPhoto;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mOrientation:I

    return v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    return-object v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/BestPhoto;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/BestPhoto;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    return-object v0
.end method

.method private initImagePageView(I)V
    .locals 5
    .param p1, "curItem"    # I

    .prologue
    .line 497
    const-string v3, "xsj"

    const-string v4, "resetImagePageView"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    .line 499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 501
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 503
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v3, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setAdapterRes(Ljava/util/List;)V

    .line 505
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 506
    .local v2, "pageMargin":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setPageMargin(I)V

    .line 507
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    const v4, 0x7f0200e9

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setPageMarginDrawable(I)V

    .line 508
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mOnPageChangeListener:Landroid/support/v4/view/MyViewPager$OnPageChangeListener;

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setOnPageChangeListener(Landroid/support/v4/view/MyViewPager$OnPageChangeListener;)V

    .line 509
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v3, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setCurrentItem(I)V

    .line 510
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPageLayout:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 511
    return-void
.end method

.method private initUI()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 351
    const-string v3, "MAGICSHOT_SPF_Key"

    invoke-virtual {p0, v3, v5}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSharedPref:Landroid/content/SharedPreferences;

    .line 352
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 354
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSharedPref:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v3, :cond_0

    .line 355
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "TYPE_BEST_PHOTO"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 356
    .local v0, "isFirstIn":Z
    if-eqz v0, :cond_3

    .line 357
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v4, "TYPE_BEST_PHOTO"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 358
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 359
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 360
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->show(I)V

    .line 370
    .end local v0    # "isFirstIn":Z
    :cond_0
    :goto_0
    const v3, 0x7f09003b

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/BestPhoto;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    .line 371
    const v3, 0x7f09003a

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/BestPhoto;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLayout:Landroid/widget/RelativeLayout;

    .line 373
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 374
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v1, v3

    .line 375
    .local v1, "length":I
    const/4 v2, 0x0

    .line 376
    .local v2, "uiBest":I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 377
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v3, v3, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mIndexes:[I

    aget v3, v3, v2

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget v4, v4, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBestIndex:I

    if-ne v3, v4, :cond_4

    .line 382
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v4, v4, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v4, v4

    invoke-virtual {v3, v4, v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->init(II)V

    .line 383
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    invoke-virtual {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->setOnCursorClickedListener(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine$CursorClickListener;)V

    .line 384
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->hideFirstImage()V

    .line 386
    const v3, 0x7f090039

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/BestPhoto;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPageLayout:Landroid/widget/RelativeLayout;

    .line 387
    invoke-direct {p0, v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->initImagePageView(I)V

    .line 391
    .end local v1    # "length":I
    .end local v2    # "uiBest":I
    :cond_2
    new-instance v3, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    .line 392
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget v6, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mOrientation:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setParam(Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;I)V

    .line 393
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v3, v7}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setBestIconSelected(Z)V

    .line 394
    return-void

    .line 362
    .restart local v0    # "isFirstIn":Z
    :cond_3
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v3, :cond_0

    .line 363
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    goto :goto_0

    .line 376
    .end local v0    # "isFirstIn":Z
    .restart local v1    # "length":I
    .restart local v2    # "uiBest":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private saveResult()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 536
    const/4 v1, 0x0

    .line 538
    .local v1, "ret":I
    const/4 v0, 0x0

    .line 539
    .local v0, "processFinished":Z
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v3, :cond_0

    .line 540
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v0, 0x1

    .line 543
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 544
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v3, :cond_1

    .line 545
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v3, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 547
    :cond_1
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->saveFiles()V

    .line 552
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 540
    goto :goto_0

    .line 549
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private setFirstImage()V
    .locals 2

    .prologue
    .line 104
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mFirstImage:Landroid/widget/ImageView;

    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 114
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIsFirstImageSetDone:Z

    .line 115
    return-void

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->hideFirstImage()V

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->hideFirstImage()V

    goto :goto_0
.end method


# virtual methods
.method public animDefaultBarIn()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 559
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-nez v3, :cond_0

    .line 578
    :goto_0
    return-void

    .line 563
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_1

    .line 564
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    .line 565
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->getHeight()I

    move-result v2

    .line 566
    .local v2, "move":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->getLayout()Landroid/widget/RelativeLayout;

    move-result-object v3

    const-string v4, "translationY"

    new-array v5, v10, [F

    neg-int v6, v2

    int-to-float v6, v6

    aput v6, v5, v9

    aput v8, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 568
    .local v0, "deaultbarIn":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 569
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLayout:Landroid/widget/RelativeLayout;

    const-string v4, "translationY"

    new-array v5, v10, [F

    int-to-float v6, v2

    aput v6, v5, v9

    aput v8, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 572
    .local v1, "indicateLineIn":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 575
    .end local v0    # "deaultbarIn":Landroid/animation/Animator;
    .end local v1    # "indicateLineIn":Landroid/animation/Animator;
    .end local v2    # "move":I
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarIn:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 577
    iput-boolean v7, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mbIsAnimDefaultBarIn:Z

    goto :goto_0
.end method

.method public animDefaultBarOut()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 581
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-nez v3, :cond_0

    .line 600
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_1

    .line 586
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    .line 587
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->getHeight()I

    move-result v2

    .line 588
    .local v2, "move":I
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->getLayout()Landroid/widget/RelativeLayout;

    move-result-object v3

    const-string v4, "translationY"

    new-array v5, v10, [F

    aput v8, v5, v7

    neg-int v6, v2

    int-to-float v6, v6

    aput v6, v5, v9

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 590
    .local v0, "deaultbarOut":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    .line 591
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLayout:Landroid/widget/RelativeLayout;

    const-string v4, "translationY"

    new-array v5, v10, [F

    aput v8, v5, v7

    int-to-float v6, v2

    aput v6, v5, v9

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 594
    .local v1, "indicateLineOut":Landroid/animation/Animator;
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 597
    .end local v0    # "deaultbarOut":Landroid/animation/Animator;
    .end local v1    # "indicateLineOut":Landroid/animation/Animator;
    .end local v2    # "move":I
    :cond_1
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mAnimSetDefaultBarOut:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 599
    iput-boolean v7, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mbIsAnimDefaultBarIn:Z

    goto :goto_0
.end method

.method public cancelDelDailog()V
    .locals 2

    .prologue
    .line 618
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->cancelDelDailog()V

    .line 619
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setBtnClickEnable(Z)V

    .line 620
    return-void
.end method

.method public doubleTapUp(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 614
    return-void
.end method

.method protected exitLogic()V
    .locals 0

    .prologue
    .line 212
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->exitLogic()V

    .line 213
    return-void
.end method

.method public finish()V
    .locals 0

    .prologue
    .line 248
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->finish()V

    .line 249
    return-void
.end method

.method public getBmpAddress()J
    .locals 2

    .prologue
    .line 147
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBitmapAddress:J

    return-wide v0
.end method

.method public getCurSavePaths()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSavedPaths:[Ljava/lang/String;

    return-object v0
.end method

.method public getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getBestPhotoSavePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSavePath:Ljava/lang/String;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v4, 0x7f060015

    const/16 v3, 0x64

    .line 412
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->handleMessage(Landroid/os/Message;)V

    .line 413
    const/4 v0, 0x0

    .line 414
    .local v0, "progress":I
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 416
    :sswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 417
    iget v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressNum:I

    if-eqz v1, :cond_1

    .line 418
    iget v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressNum:I

    mul-int/lit8 v2, v0, 0x1e

    div-int/lit8 v2, v2, 0x64

    add-int v0, v1, v2

    .line 420
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_0

    .line 421
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 423
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v3, :cond_0

    .line 424
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1, v3}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    goto :goto_0

    .line 432
    :sswitch_1
    sget-object v1, Lcom/arcsoft/magicshotstudio/BestPhoto;->tag:Ljava/lang/String;

    const-string v2, "Can not get the correct infomation from intent."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->finish()V

    goto :goto_0

    .line 437
    :sswitch_2
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 438
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {p0, v4}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 439
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    goto :goto_0

    .line 443
    :sswitch_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_0

    .line 444
    iget v1, p1, Landroid/os/Message;->arg1:I

    mul-int/lit8 v1, v1, 0x46

    div-int/lit8 v0, v1, 0x64

    .line 445
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 446
    iput v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressNum:I

    goto :goto_0

    .line 456
    :sswitch_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->startMode()I

    goto :goto_0

    .line 460
    :sswitch_5
    const v1, 0x7f06002f

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;II)V

    .line 462
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->finish()V

    goto :goto_0

    .line 414
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
        0x12 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_5
        0x1000 -> :sswitch_0
    .end sparse-switch
.end method

.method public hideFirstImage()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mFirstImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    :cond_0
    return-void
.end method

.method public isScreenLocked()Z
    .locals 2

    .prologue
    .line 492
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 493
    .local v0, "mKeyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    return v1
.end method

.method public lockSaveProcess()V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 638
    return-void
.end method

.method protected onActivityServiceConnected()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onActivityServiceConnected()V

    .line 119
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIsFirstImageSetDone:Z

    if-nez v0, :cond_0

    .line 120
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->setFirstImage()V

    .line 122
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 282
    :goto_0
    return-void

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->isSaveState()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIsUserEdit:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIndicateLine:Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/IndicateLine;->getBestCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 281
    :cond_1
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 476
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, p1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->updateHelpConfig(ILandroid/content/res/Configuration;)V

    .line 479
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-eqz v1, :cond_1

    .line 480
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v1, p1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 482
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    if-eqz v1, :cond_2

    .line 483
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->isScreenLocked()Z

    move-result v1

    if-nez v1, :cond_2

    .line 484
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->getCurrentItem()I

    move-result v0

    .line 485
    .local v0, "cur":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->scrollToItem(IZIZ)V

    .line 488
    .end local v0    # "cur":I
    :cond_2
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 489
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    const v2, 0x7f030006

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->setContentView(I)V

    .line 83
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->containsMainActivity()Z

    move-result v2

    if-nez v2, :cond_0

    .line 84
    const v2, 0x7f090043

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgBack:Landroid/widget/ImageView;

    .line 85
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgBack:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    const v2, 0x7f090044

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    .line 87
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 88
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 89
    .local v0, "density":F
    const/high16 v2, 0x41600000    # 14.0f

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 90
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mTitleTextView:Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;

    invoke-virtual {v2, v1}, Lcom/arcsoft/magicshotstudio/ui/common/TitleOutlineTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    .end local v0    # "density":F
    .end local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    const v2, 0x7f090023

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/BestPhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mFirstImage:Landroid/widget/ImageView;

    .line 94
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 95
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->setFirstImage()V

    .line 100
    :goto_0
    new-instance v2, Ljava/util/concurrent/Semaphore;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    .line 101
    return-void

    .line 97
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mIsFirstImageSetDone:Z

    .line 98
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->hideFirstImage()V

    goto :goto_0
.end method

.method public onCursorClicked(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 253
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mImgPage:Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/ImgPage;->setCurrentItem(IZ)V

    .line 254
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 217
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 228
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 229
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->nativeUninit()I

    .line 230
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->exitLogic()V

    .line 231
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 234
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->releaseUIBitmaps()V

    .line 235
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v0, :cond_0

    .line 236
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->hidePopupToast()V

    .line 242
    :cond_1
    sget-object v0, Lcom/arcsoft/magicshotstudio/BestPhoto;->tag:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onDestroy()V

    .line 244
    return-void
.end method

.method public onHomeKeyPressed()V
    .locals 3

    .prologue
    .line 515
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-nez v1, :cond_0

    .line 533
    :goto_0
    return-void

    .line 519
    :cond_0
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mFromGallery:Z

    if-nez v1, :cond_3

    .line 520
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->saveResult()I

    .line 521
    const/4 v0, 0x0

    .line 522
    .local v0, "processFinished":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_1

    .line 523
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x1

    .line 526
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 527
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->finish()V

    .line 529
    :cond_2
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x5000

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 532
    .end local v0    # "processFinished":Z
    :cond_3
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onHomeKeyPressed()V

    goto :goto_0

    .line 523
    .restart local v0    # "processFinished":Z
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onNoItemSelected(Z)V
    .locals 4
    .param p1, "bAllSelected"    # Z

    .prologue
    const/4 v1, 0x1

    .line 470
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    const v3, 0x7f090055

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->setState(IZ)V

    .line 471
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->changeSaveBtnState(Z)V

    .line 472
    return-void

    .line 470
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 286
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onResume()V

    .line 288
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mHeadbarMgr:Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/bestphoto/HeadbarMgr;->hidePopupToast()V

    .line 199
    :cond_0
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStop()V

    .line 200
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 263
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onUserLeaveHint()V

    .line 264
    return-void
.end method

.method public releaseUIBitmaps()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 180
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v2, v2, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 181
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v2, v2, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    array-length v1, v2

    .line 182
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 184
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iget-object v2, v2, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iput-object v4, v2, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBitmaps:[Landroid/graphics/Bitmap;

    .line 187
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    const/4 v3, 0x0

    iput v3, v2, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mBestIndex:I

    .line 188
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    iput-object v4, v2, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;->mIndexes:[I

    .line 189
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    .line 192
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_1
    return-void
.end method

.method public saveBitmaps([I[Ljava/lang/String;J)I
    .locals 3
    .param p1, "saveIndexes"    # [I
    .param p2, "pathArray"    # [Ljava/lang/String;
    .param p3, "mBitmapAddr"    # J

    .prologue
    .line 623
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->nativeSaveBitmaps([I[Ljava/lang/String;J)I

    move-result v0

    .line 624
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 625
    return v0
.end method

.method public saveBitmapsWithExif([I[Ljava/lang/String;JJJ)I
    .locals 11
    .param p1, "saveIndexes"    # [I
    .param p2, "pathArray"    # [Ljava/lang/String;
    .param p3, "mBitmapAddr"    # J
    .param p5, "exifBuff"    # J
    .param p7, "exifBuffSize"    # J

    .prologue
    .line 630
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v9}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->nativeSaveBitmapsWithExif([I[Ljava/lang/String;JJJ)I

    move-result v0

    .line 632
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSaveLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 633
    return v0
.end method

.method public setCurSavePaths([Ljava/lang/String;)V
    .locals 0
    .param p1, "paths"    # [Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSavedPaths:[Ljava/lang/String;

    .line 140
    return-void
.end method

.method public singleTapUp(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mbIsAnimDefaultBarIn:Z

    if-eqz v0, :cond_0

    .line 605
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->animDefaultBarOut()V

    .line 609
    :goto_0
    return-void

    .line 607
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->animDefaultBarIn()V

    goto :goto_0
.end method

.method protected startMode()I
    .locals 4

    .prologue
    .line 291
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->BestPhotoStartTime:J

    .line 292
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMode()I

    move-result v1

    .line 293
    .local v1, "ret":I
    if-eqz v1, :cond_0

    .line 296
    sget-object v2, Lcom/arcsoft/magicshotstudio/BestPhoto;->tag:Ljava/lang/String;

    const-string v3, "Can not get the correct infomation from intent."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->finish()V

    .line 313
    :goto_0
    return v1

    .line 301
    :cond_0
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mSubModeStartFromMain:Z

    if-eqz v2, :cond_1

    .line 302
    new-instance v2, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 303
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v3, 0x7f060015

    invoke-virtual {p0, v3}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 304
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    .line 306
    :cond_1
    invoke-static {}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->getEngine()Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    .line 307
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->nativeInit()I

    .line 308
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->createCandidates()Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    move-result-object v2

    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mCandidates:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$Candidates;

    .line 309
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mBestPhotoEngine:Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;

    invoke-virtual {v2, p0}, Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI;->setProcessLis(Lcom/arcsoft/magicshotstudio/jni/bestphoto/BestPhoto_JNI$ProcessCallback;)V

    .line 310
    new-instance v0, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;-><init>(Lcom/arcsoft/magicshotstudio/BestPhoto;)V

    .line 311
    .local v0, "mTask":Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/arcsoft/magicshotstudio/BestPhoto$BackgroundTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public update(I)V
    .locals 3
    .param p1, "process"    # I

    .prologue
    .line 398
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestPhoto;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v2, :cond_0

    .line 399
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/BestPhoto;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 400
    .local v0, "mUIHandler":Landroid/os/Handler;
    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v1

    .line 401
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x1000

    iput v2, v1, Landroid/os/Message;->what:I

    .line 402
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 403
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 405
    .end local v0    # "mUIHandler":Landroid/os/Handler;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
