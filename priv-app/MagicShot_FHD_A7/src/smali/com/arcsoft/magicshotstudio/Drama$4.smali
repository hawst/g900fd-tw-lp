.class Lcom/arcsoft/magicshotstudio/Drama$4;
.super Ljava/lang/Object;
.source "Drama.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Drama;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Drama;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Drama;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyAutoResult(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 213
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Drama;->access$500(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getMainModeFlags()Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    move-result-object v1

    .line 214
    .local v1, "modeEnable":Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;
    iget-boolean v0, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mDramaEnable:Z

    .line 216
    .local v0, "hasMovingObject":Z
    if-nez v0, :cond_0

    .line 217
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # invokes: Lcom/arcsoft/magicshotstudio/Drama;->showNoMovingObjectDialog()V
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Drama;->access$600(Lcom/arcsoft/magicshotstudio/Drama;)V

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/Drama;->access$700(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x17

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onNotifyError(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    const/4 v2, 0x1

    .line 226
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # setter for: Lcom/arcsoft/magicshotstudio/Drama;->mbProcessingFinish:Z
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/Drama;->access$802(Lcom/arcsoft/magicshotstudio/Drama;Z)Z

    .line 227
    const-string v0, ""

    .line 228
    .local v0, "errString":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 242
    :pswitch_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-static {v1, v0, v2}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;Ljava/lang/String;I)V

    .line 243
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    .line 244
    return-void

    .line 228
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onNotifyProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 248
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama$4;->this$0:Lcom/arcsoft/magicshotstudio/Drama;

    # getter for: Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Drama;->access$900(Lcom/arcsoft/magicshotstudio/Drama;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "progress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void
.end method
