.class Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;
.super Ljava/lang/Object;
.source "BestFace.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/BestFace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyFileFilter"
.end annotation


# instance fields
.field private suffixFormat:Ljava/lang/String;

.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/BestFace;


# direct methods
.method public constructor <init>(Lcom/arcsoft/magicshotstudio/BestFace;Ljava/lang/String;)V
    .locals 1
    .param p2, "suffixFormat"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;->suffixFormat:Ljava/lang/String;

    .line 94
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;->suffixFormat:Ljava/lang/String;

    .line 95
    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 4
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 99
    const-string v3, "."

    invoke-virtual {p2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 100
    .local v0, "index":I
    if-gez v0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v2

    .line 103
    :cond_1
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "suffixName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 107
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$MyFileFilter;->suffixFormat:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    goto :goto_0
.end method
