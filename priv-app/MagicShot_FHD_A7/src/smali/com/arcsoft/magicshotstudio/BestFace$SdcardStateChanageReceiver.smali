.class Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BestFace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/BestFace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SdcardStateChanageReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/BestFace;


# direct methods
.method private constructor <init>(Lcom/arcsoft/magicshotstudio/BestFace;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/arcsoft/magicshotstudio/BestFace;Lcom/arcsoft/magicshotstudio/BestFace$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/arcsoft/magicshotstudio/BestFace;
    .param p2, "x1"    # Lcom/arcsoft/magicshotstudio/BestFace$1;

    .prologue
    .line 331
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;-><init>(Lcom/arcsoft/magicshotstudio/BestFace;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, 0x1f4

    const/16 v5, 0x800

    const/16 v4, 0x101

    .line 334
    # getter for: Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/BestFace;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive in"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/BestFace;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 341
    .local v0, "mHandler":Landroid/os/Handler;
    const/4 v1, 0x0

    .line 342
    .local v1, "needFinish":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 344
    :cond_0
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/BestFace;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->addFlags(I)V

    .line 345
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    # getter for: Lcom/arcsoft/magicshotstudio/BestFace;->mSavePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/BestFace;->access$200(Lcom/arcsoft/magicshotstudio/BestFace;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->isExSDCard(Ljava/lang/String;)Z

    move-result v1

    .line 346
    if-eqz v1, :cond_3

    .line 347
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    # getter for: Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/BestFace;->access$300(Lcom/arcsoft/magicshotstudio/BestFace;)Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 348
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    # getter for: Lcom/arcsoft/magicshotstudio/BestFace;->mArcPicBestUILogic:Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/BestFace;->access$300(Lcom/arcsoft/magicshotstudio/BestFace;)Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/ui/bestface/ArcPicBestUILogic;->setOperateEnable(Z)V

    .line 350
    :cond_1
    const/16 v2, 0x100

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 358
    :cond_2
    :goto_0
    # getter for: Lcom/arcsoft/magicshotstudio/BestFace;->tag:Ljava/lang/String;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/BestFace;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onReceive out"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void

    .line 352
    :cond_3
    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 354
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 355
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/BestFace$SdcardStateChanageReceiver;->this$0:Lcom/arcsoft/magicshotstudio/BestFace;

    invoke-virtual {v2}, Lcom/arcsoft/magicshotstudio/BestFace;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Window;->addFlags(I)V

    .line 356
    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
