.class public Lcom/arcsoft/magicshotstudio/utils/UserStack;
.super Ljava/lang/Object;
.source "UserStack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private MAX_STACK_COUNT:I

.field private final TAG:Ljava/lang/String;

.field private USE_FILE:Z

.field private mHandler:Landroid/os/Handler;

.field private mStack:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<TE;>;"
        }
    .end annotation
.end field

.field private mThread:Landroid/os/HandlerThread;

.field private mTotalSize:I

.field private mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-string v0, "UserStack"

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->TAG:Ljava/lang/String;

    .line 12
    const v0, 0x7fffffff

    iput v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->MAX_STACK_COUNT:I

    .line 13
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->USE_FILE:Z

    .line 15
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    .line 16
    iput v2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    .line 18
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    .line 19
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    .line 21
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    .line 30
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/utils/UserStack;)Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/utils/UserStack;

    .prologue
    .line 8
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/utils/UserStack;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/utils/UserStack;

    .prologue
    .line 8
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    return-object v0
.end method

.method private loadData()V
    .locals 2

    .prologue
    .line 119
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/UserStack$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/utils/UserStack$2;-><init>(Lcom/arcsoft/magicshotstudio/utils/UserStack;)V

    .line 135
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 136
    return-void
.end method

.method private saveData(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    .local p1, "data":Ljava/lang/Object;, "TE;"
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/UserStack$1;

    invoke-direct {v0, p0, p1}, Lcom/arcsoft/magicshotstudio/utils/UserStack$1;-><init>(Lcom/arcsoft/magicshotstudio/utils/UserStack;Ljava/lang/Object;)V

    .line 115
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 116
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 68
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 69
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    invoke-interface {v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;->clearDataFile()V

    .line 72
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    .line 73
    return-void
.end method

.method public closeFileStore()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    const/4 v1, 0x0

    .line 44
    const v0, 0x7fffffff

    iput v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->MAX_STACK_COUNT:I

    .line 45
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->USE_FILE:Z

    .line 48
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 50
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 55
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    .line 57
    :cond_1
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 60
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    iget v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openFileStore(ILcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;)V
    .locals 4
    .param p1, "stackMaxCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    .local p2, "listener":Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;, "Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener<TE;>;"
    iput p1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->MAX_STACK_COUNT:I

    .line 35
    iput-object p2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mUserStackListener:Lcom/arcsoft/magicshotstudio/utils/UserStack$UserStackListener;

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->USE_FILE:Z

    .line 38
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UserStack_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 40
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mHandler:Landroid/os/Handler;

    .line 41
    return-void
.end method

.method public declared-synchronized popData()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    monitor-enter p0

    const/4 v0, 0x0

    .line 91
    .local v0, "result":Ljava/lang/Object;, "TE;"
    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 93
    iget v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    .line 96
    .end local v0    # "result":Ljava/lang/Object;, "TE;"
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->MAX_STACK_COUNT:I

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-le v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->USE_FILE:Z

    if-eqz v1, :cond_1

    .line 97
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->loadData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :cond_1
    monitor-exit p0

    return-object v0

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized pushData(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    .local p1, "data":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    iget v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->MAX_STACK_COUNT:I

    if-le v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->USE_FILE:Z

    if-eqz v1, :cond_1

    .line 81
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mStack:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 82
    .local v0, "firstData":Ljava/lang/Object;, "TE;"
    if-eqz v0, :cond_1

    .line 83
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/UserStack;->saveData(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    .end local v0    # "firstData":Ljava/lang/Object;, "TE;"
    :cond_1
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 64
    .local p0, "this":Lcom/arcsoft/magicshotstudio/utils/UserStack;, "Lcom/arcsoft/magicshotstudio/utils/UserStack<TE;>;"
    iget v0, p0, Lcom/arcsoft/magicshotstudio/utils/UserStack;->mTotalSize:I

    return v0
.end method
