.class public Lcom/arcsoft/magicshotstudio/utils/ArcUtils;
.super Ljava/lang/Object;
.source "ArcUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_MagicShot_ArcUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static arrayToArrayList([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "mArray"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    if-nez p0, :cond_1

    .line 91
    const/4 v3, 0x0

    .line 97
    :cond_0
    return-object v3

    .line 93
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v3, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 95
    .local v4, "tempItem":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 7
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 606
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 607
    .local v0, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 608
    .local v4, "width":I
    const/4 v1, 0x1

    .line 610
    .local v1, "inSampleSize":I
    if-gt v0, p2, :cond_0

    if-le v4, p1, :cond_2

    .line 611
    :cond_0
    if-le v4, v0, :cond_1

    .line 612
    int-to-float v5, v0

    int-to-float v6, p2

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 624
    :goto_0
    mul-int v5, v4, v0

    int-to-float v2, v5

    .line 628
    .local v2, "totalPixels":F
    mul-int v5, p1, p2

    mul-int/lit8 v5, v5, 0x2

    int-to-float v3, v5

    .line 630
    .local v3, "totalReqPixelsCap":F
    :goto_1
    mul-int v5, v1, v1

    int-to-float v5, v5

    div-float v5, v2, v5

    cmpl-float v5, v5, v3

    if-lez v5, :cond_2

    .line 631
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 614
    .end local v2    # "totalPixels":F
    .end local v3    # "totalReqPixelsCap":F
    :cond_1
    int-to-float v5, v4

    int-to-float v6, p1

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0

    .line 634
    :cond_2
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->prevPowerOf2(I)I

    move-result v5

    return v5
.end method

.method public static compareTwoString(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0, "a"    # Ljava/lang/String;
    .param p1, "b"    # Ljava/lang/String;

    .prologue
    .line 304
    const/4 v0, 0x0

    .line 305
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 306
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    .line 307
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 308
    const/4 v0, -0x1

    goto :goto_0

    .line 310
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static createDir(Ljava/lang/String;)Z
    .locals 2
    .param p0, "dirPath"    # Ljava/lang/String;

    .prologue
    .line 190
    if-eqz p0, :cond_1

    .line 191
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 192
    .local v0, "dirFiles":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    const/4 v1, 0x1

    .line 198
    .end local v0    # "dirFiles":Ljava/io/File;
    :goto_0
    return v1

    .line 195
    .restart local v0    # "dirFiles":Ljava/io/File;
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    goto :goto_0

    .line 198
    .end local v0    # "dirFiles":Ljava/io/File;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static createFile(Ljava/lang/String;)V
    .locals 4
    .param p0, "fullPath"    # Ljava/lang/String;

    .prologue
    .line 465
    if-nez p0, :cond_1

    .line 476
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 470
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    .line 472
    .local v2, "filePathStr":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 473
    .local v1, "filePath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 474
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    goto :goto_0
.end method

.method public static decodeBitmapFromArray([BII)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "data"    # [B
    .param p1, "minWidth"    # I
    .param p2, "minHeight"    # I

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 823
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 824
    .local v1, "opts":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 825
    array-length v4, p0

    invoke-static {p0, v6, v4, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 826
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v4, v4

    int-to-float v5, p1

    div-float v2, v4, v5

    .line 827
    .local v2, "x":F
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v4, v4

    int-to-float v5, p2

    div-float v3, v4, v5

    .line 828
    .local v3, "y":F
    cmpg-float v4, v2, v7

    if-lez v4, :cond_0

    cmpg-float v4, v3, v7

    if-gtz v4, :cond_1

    .line 829
    :cond_0
    iput v8, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 832
    :goto_0
    iput-boolean v6, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 833
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 834
    array-length v4, p0

    invoke-static {p0, v6, v4, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 835
    .local v0, "bmp":Landroid/graphics/Bitmap;
    return-object v0

    .line 831
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    cmpl-float v4, v2, v3

    if-lez v4, :cond_2

    float-to-int v4, v3

    :goto_1
    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_0

    :cond_2
    float-to-int v4, v2

    goto :goto_1
.end method

.method public static declared-synchronized decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 570
    const-class v2, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;

    monitor-enter v2

    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 571
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 572
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 575
    invoke-static {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 578
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 581
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 582
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 570
    .end local v0    # "options":Landroid/graphics/BitmapFactory$Options;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized decodeSampledBitmapFromFile([BII)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "dataArray"    # [B
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 588
    const-class v2, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;

    monitor-enter v2

    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 589
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 590
    const/4 v1, 0x0

    array-length v3, p0

    invoke-static {p0, v1, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 593
    invoke-static {v0, p1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 596
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 599
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 600
    const/4 v1, 0x0

    array-length v3, p0

    invoke-static {p0, v1, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 588
    .end local v0    # "options":Landroid/graphics/BitmapFactory$Options;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static deleteDir(Ljava/lang/String;)Z
    .locals 6
    .param p0, "dirPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 226
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "dirFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v4

    .line 230
    :cond_1
    const/4 v2, 0x1

    .line 231
    .local v2, "flag":Z
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 232
    .local v1, "files":[Ljava/io/File;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v5, v1

    if-ge v3, v5, :cond_2

    .line 233
    aget-object v5, v1, v3

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 234
    aget-object v5, v1, v3

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteFile(Ljava/lang/String;)Z

    move-result v2

    .line 235
    if-nez v2, :cond_4

    .line 243
    :cond_2
    if-eqz v2, :cond_0

    .line 245
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 246
    const/4 v4, 0x1

    goto :goto_0

    .line 238
    :cond_3
    aget-object v5, v1, v3

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->deleteDir(Ljava/lang/String;)Z

    move-result v2

    .line 239
    if-eqz v2, :cond_2

    .line 232
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static deleteFile(Ljava/lang/String;)Z
    .locals 3
    .param p0, "fPath"    # Ljava/lang/String;

    .prologue
    .line 208
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 210
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 211
    .local v0, "bSucc":Z
    if-eqz v0, :cond_0

    .line 212
    const/4 v2, 0x1

    .line 216
    .end local v0    # "bSucc":Z
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static final deleteFileIfAlreadyExist(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "mExifJPG"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 480
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    const/4 v6, 0x0

    .line 485
    .local v6, "cursor":Landroid/database/Cursor;
    const-string v5, "datetaken DESC, _id DESC"

    .line 486
    .local v5, "strOrderClause":Ljava/lang/String;
    const/4 v8, 0x0

    .line 487
    .local v8, "id":I
    const/4 v7, 0x0

    .line 489
    .local v7, "fileName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    aput-object v4, v2, v3

    const-string v3, "_data=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p1, v4, v11

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 492
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 494
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 495
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 496
    invoke-virtual {v7, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_5

    .line 502
    :cond_3
    :goto_1
    if-eqz v6, :cond_4

    .line 503
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 504
    const/4 v6, 0x0

    .line 508
    :cond_4
    const/4 v10, 0x0

    .line 509
    .local v10, "uri":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 510
    .local v9, "result":I
    if-eqz v8, :cond_0

    .line 511
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 514
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v10, v12, v12}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    goto :goto_0

    .line 499
    .end local v9    # "result":I
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_5
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 502
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 503
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 504
    const/4 v6, 0x0

    :cond_6
    throw v0
.end method

.method public static deleteSourceFile(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 13
    .param p0, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "mFilePathArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 37
    if-nez p1, :cond_1

    .line 87
    :cond_0
    return-void

    .line 41
    :cond_1
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 42
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 41
    :cond_2
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 45
    :cond_3
    new-instance v7, Ljava/io/File;

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 46
    .local v7, "file":Ljava/io/File;
    if-eqz v7, :cond_5

    .line 47
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 48
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 50
    :cond_4
    const/4 v7, 0x0

    .line 53
    :cond_5
    const/4 v10, 0x0

    .line 54
    .local v10, "imagesor":Landroid/database/Cursor;
    const/4 v9, -0x1

    .line 56
    .local v9, "imageid":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 62
    if-eqz v10, :cond_6

    .line 63
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v12, :cond_6

    .line 64
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 65
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 70
    :cond_6
    if-eqz v10, :cond_7

    .line 72
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 73
    const/4 v10, 0x0

    .line 80
    :cond_7
    :goto_2
    const/4 v0, -0x1

    if-eq v9, v0, :cond_2

    .line 81
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 74
    :catch_0
    move-exception v6

    .line 75
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 70
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_8

    .line 72
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 73
    const/4 v10, 0x0

    .line 76
    :cond_8
    :goto_3
    throw v0

    .line 74
    :catch_1
    move-exception v6

    .line 75
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public static deleteSourceFile(Z[Ljava/lang/String;Landroid/content/Context;)V
    .locals 13
    .param p0, "mKeepSrcFiles"    # Z
    .param p1, "mPhotos"    # [Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 413
    if-eqz p0, :cond_1

    .line 462
    :cond_0
    return-void

    .line 417
    :cond_1
    if-eqz p1, :cond_0

    .line 421
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, p1

    if-ge v8, v0, :cond_0

    .line 422
    aget-object v0, p1, v8

    if-nez v0, :cond_3

    .line 421
    :cond_2
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 425
    :cond_3
    new-instance v7, Ljava/io/File;

    aget-object v0, p1, v8

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 427
    .local v7, "file":Ljava/io/File;
    if-eqz v7, :cond_5

    .line 428
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 429
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 430
    const-string v0, "ArcSoft_MagicShot_ArcUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delete deleteSourceFile(),delete file : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_4
    const/4 v7, 0x0

    .line 435
    :cond_5
    const/4 v10, 0x0

    .line 436
    .local v10, "imagesor":Landroid/database/Cursor;
    const/4 v9, -0x1

    .line 438
    .local v9, "imageid":I
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 441
    if-eqz v10, :cond_6

    .line 442
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v12, :cond_6

    .line 443
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 444
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 448
    :cond_6
    if-eqz v10, :cond_7

    .line 450
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 451
    const/4 v10, 0x0

    .line 458
    :cond_7
    :goto_2
    const/4 v0, -0x1

    if-eq v9, v0, :cond_2

    .line 459
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 452
    :catch_0
    move-exception v6

    .line 453
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "ArcSoft_MagicShot_ArcUtils"

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 448
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_8

    .line 450
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 451
    const/4 v10, 0x0

    .line 454
    :cond_8
    :goto_3
    throw v0

    .line 452
    :catch_1
    move-exception v6

    .line 453
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v1, "ArcSoft_MagicShot_ArcUtils"

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static divideArray([II)[I
    .locals 12
    .param p0, "array"    # [I
    .param p1, "group_num"    # I

    .prologue
    const/4 v10, 0x0

    .line 687
    const/4 v0, 0x0

    .line 688
    .local v0, "arrayLength":I
    const/4 v3, 0x0

    .line 689
    .local v3, "groupLength":I
    const/4 v8, 0x0

    .local v8, "mode":I
    move-object v2, v10

    .line 690
    check-cast v2, [[I

    .line 691
    .local v2, "groupArray":[[I
    const/4 v1, 0x0

    .line 692
    .local v1, "backArray":[I
    const/4 v5, 0x0

    .line 693
    .local v5, "index":I
    if-eqz p0, :cond_0

    .line 694
    array-length v0, p0

    .line 697
    :cond_0
    if-eqz v0, :cond_1

    if-gtz p1, :cond_2

    .line 736
    :cond_1
    :goto_0
    return-object v10

    .line 701
    :cond_2
    if-ge v0, p1, :cond_3

    .line 702
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->doSort([I)[I

    move-result-object v10

    goto :goto_0

    .line 705
    :cond_3
    new-array v2, p1, [[I

    .line 706
    new-array v1, p1, [I

    .line 707
    div-int v3, v0, p1

    .line 708
    rem-int v8, v0, p1

    .line 710
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v10, v2

    if-ge v4, v10, :cond_7

    .line 711
    array-length v10, v2

    add-int/lit8 v10, v10, -0x1

    if-ge v4, v10, :cond_5

    .line 712
    if-ge v4, v8, :cond_4

    .line 713
    add-int/lit8 v10, v3, 0x1

    new-array v10, v10, [I

    aput-object v10, v2, v4

    .line 717
    :goto_2
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    aget-object v10, v2, v4

    array-length v10, v10

    if-ge v7, v10, :cond_6

    .line 718
    aget-object v10, v2, v4

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "index":I
    .local v6, "index":I
    aget v11, p0, v5

    aput v11, v10, v7

    .line 717
    add-int/lit8 v7, v7, 0x1

    move v5, v6

    .end local v6    # "index":I
    .restart local v5    # "index":I
    goto :goto_3

    .line 715
    .end local v7    # "j":I
    :cond_4
    new-array v10, v3, [I

    aput-object v10, v2, v4

    goto :goto_2

    .line 721
    :cond_5
    sub-int v10, v0, v5

    new-array v10, v10, [I

    aput-object v10, v2, v4

    .line 722
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_4
    aget-object v10, v2, v4

    array-length v10, v10

    if-ge v7, v10, :cond_6

    .line 723
    aget-object v10, v2, v4

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "index":I
    .restart local v6    # "index":I
    aget v11, p0, v5

    aput v11, v10, v7

    .line 722
    add-int/lit8 v7, v7, 0x1

    move v5, v6

    .end local v6    # "index":I
    .restart local v5    # "index":I
    goto :goto_4

    .line 710
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 728
    .end local v7    # "j":I
    :cond_7
    const/4 v9, 0x0

    .line 729
    .local v9, "temp":I
    const/4 v4, 0x0

    :goto_5
    if-ge v4, p1, :cond_9

    .line 730
    if-lez v4, :cond_8

    .line 731
    add-int/lit8 v10, v4, -0x1

    aget-object v10, v2, v10

    array-length v10, v10

    add-int/2addr v9, v10

    .line 733
    :cond_8
    aget-object v10, v2, v4

    invoke-static {v10}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->doSort([I)[I

    move-result-object v10

    const/4 v11, 0x0

    aget v10, v10, v11

    add-int/2addr v10, v9

    aput v10, v1, v4

    .line 729
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_9
    move-object v10, v1

    .line 736
    goto :goto_0
.end method

.method public static doSort([I)[I
    .locals 8
    .param p0, "src"    # [I

    .prologue
    .line 663
    array-length v2, p0

    .line 664
    .local v2, "len":I
    new-array v3, v2, [I

    .line 665
    .local v3, "res":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 666
    aput v0, v3, v0

    .line 665
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 668
    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    .line 669
    add-int/lit8 v1, v0, 0x1

    .local v1, "j":I
    :goto_2
    if-ge v1, v2, :cond_2

    .line 672
    aget v6, p0, v0

    aget v7, p0, v1

    if-ge v6, v7, :cond_1

    .line 673
    aget v4, p0, v1

    .line 674
    .local v4, "temp":I
    aget v6, p0, v0

    aput v6, p0, v1

    .line 675
    aput v4, p0, v0

    .line 677
    aget v5, v3, v1

    .line 678
    .local v5, "temp2":I
    aget v6, v3, v0

    aput v6, v3, v1

    .line 679
    aput v5, v3, v0

    .line 669
    .end local v4    # "temp":I
    .end local v5    # "temp2":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 668
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 683
    .end local v1    # "j":I
    :cond_3
    return-object v3
.end method

.method public static extractArrayList(II)[I
    .locals 10
    .param p0, "inputNum"    # I
    .param p1, "extractNum"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 891
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 892
    :cond_0
    const/4 v5, 0x0

    .line 937
    :cond_1
    :goto_0
    return-object v5

    .line 895
    :cond_2
    new-array v3, p0, [I

    .line 896
    .local v3, "input":[I
    new-array v5, p1, [I

    .line 898
    .local v5, "output":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p0, :cond_3

    .line 899
    aput v1, v3, v1

    .line 898
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 902
    :cond_3
    if-lt p1, p0, :cond_4

    move-object v5, v3

    .line 903
    goto :goto_0

    .line 906
    :cond_4
    if-ne v8, p1, :cond_5

    .line 907
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->randomIntDataFromArray(I)I

    move-result v6

    .line 908
    .local v6, "temp":I
    if-ltz v6, :cond_1

    array-length v7, v3

    if-ge v6, v7, :cond_1

    .line 909
    aget v7, v3, v6

    aput v7, v5, v9

    goto :goto_0

    .line 914
    .end local v6    # "temp":I
    :cond_5
    const/4 v7, 0x2

    if-ne v7, p1, :cond_6

    .line 915
    aget v7, v3, v9

    aput v7, v5, v9

    .line 916
    array-length v7, v3

    add-int/lit8 v7, v7, -0x1

    aget v7, v3, v7

    aput v7, v5, v8

    goto :goto_0

    .line 920
    :cond_6
    add-int/lit8 v7, p1, -0x1

    div-int v0, p0, v7

    .line 921
    .local v0, "gapLength":I
    const/4 v2, 0x0

    .line 923
    .local v2, "indexChoosed":I
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_2
    if-ge v4, p1, :cond_1

    .line 924
    if-nez v4, :cond_7

    .line 925
    aget v7, v3, v9

    aput v7, v5, v9

    .line 923
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 926
    :cond_7
    add-int/lit8 v7, p1, -0x1

    if-ne v7, v4, :cond_8

    .line 927
    add-int/lit8 v7, p1, -0x1

    add-int/lit8 v8, p0, -0x1

    aget v8, v3, v8

    aput v8, v5, v7

    goto :goto_3

    .line 929
    :cond_8
    add-int/2addr v2, v0

    .line 930
    add-int/lit8 v7, p1, -0x1

    rem-int v7, p0, v7

    if-ge v4, v7, :cond_9

    .line 931
    add-int/lit8 v2, v2, 0x1

    .line 933
    :cond_9
    aget v7, v3, v2

    aput v7, v5, v4

    goto :goto_3
.end method

.method public static getBitmapFromID(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resID"    # I

    .prologue
    .line 317
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 318
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 319
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "pathandname"    # Ljava/lang/String;

    .prologue
    .line 740
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 741
    .local v0, "start":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 742
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 744
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getFileNameWithoutPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "srcPath"    # Ljava/lang/String;

    .prologue
    .line 168
    if-nez p0, :cond_0

    .line 169
    const-string v1, ""

    .line 172
    :goto_0
    return-object v1

    .line 171
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    .local v0, "srcFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "srcPath"    # Ljava/lang/String;

    .prologue
    .line 160
    if-nez p0, :cond_0

    .line 161
    const-string v1, ""

    .line 164
    :goto_0
    return-object v1

    .line 163
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "srcFile":Ljava/io/File;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFilePostfix(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "srcPath"    # Ljava/lang/String;

    .prologue
    .line 176
    if-nez p0, :cond_1

    .line 177
    const-string v0, ""

    .line 185
    :cond_0
    :goto_0
    return-object v0

    .line 179
    :cond_1
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getFileNameWithoutPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\."

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "pathSplitArray":[Ljava/lang/String;
    array-length v1, v2

    .line 181
    .local v1, "length":I
    const-string v0, "."

    .line 182
    .local v0, "lastName":Ljava/lang/String;
    const/4 v3, 0x2

    if-lt v1, v3, :cond_0

    .line 183
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getMiddle([Ljava/lang/String;II)I
    .locals 2
    .param p0, "list"    # [Ljava/lang/String;
    .param p1, "low"    # I
    .param p2, "high"    # I

    .prologue
    .line 288
    aget-object v0, p0, p1

    .line 289
    .local v0, "tmp":Ljava/lang/String;
    :goto_0
    if-ge p1, p2, :cond_2

    .line 290
    :goto_1
    if-ge p1, p2, :cond_0

    aget-object v1, p0, p2

    invoke-static {v1, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->compareTwoString(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 291
    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    .line 293
    :cond_0
    aget-object v1, p0, p2

    aput-object v1, p0, p1

    .line 294
    :goto_2
    if-ge p1, p2, :cond_1

    aget-object v1, p0, p1

    invoke-static {v1, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->compareTwoString(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    .line 295
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    .line 297
    :cond_1
    aget-object v1, p0, p1

    aput-object v1, p0, p2

    goto :goto_0

    .line 299
    :cond_2
    aput-object v0, p0, p1

    .line 300
    return p1
.end method

.method public static getPhotoSize(Ljava/lang/String;)Lcom/arcsoft/magicshotstudio/utils/MSize;
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 324
    if-eqz p0, :cond_0

    .line 325
    new-instance v1, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>()V

    .line 326
    .local v1, "tempSize":Lcom/arcsoft/magicshotstudio/utils/MSize;
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 327
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 328
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 329
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/lit8 v2, v2, 0x2

    mul-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 330
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/lit8 v2, v2, 0x2

    mul-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 334
    .end local v0    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v1    # "tempSize":Lcom/arcsoft/magicshotstudio/utils/MSize;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isExSDCard(Ljava/lang/String;)Z
    .locals 5
    .param p0, "savePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 807
    const/4 v2, 0x0

    .line 808
    .local v2, "result":Z
    if-nez p0, :cond_0

    .line 820
    :goto_0
    return v3

    .line 813
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 814
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 815
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_2

    move v2, v3

    .end local v1    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    move v3, v2

    .line 820
    goto :goto_0

    .line 815
    .restart local v1    # "file":Ljava/io/File;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 817
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 818
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static prevPowerOf2(I)I
    .locals 1
    .param p0, "n"    # I

    .prologue
    .line 638
    if-gtz p0, :cond_0

    .line 639
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 640
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    return v0
.end method

.method public static quickSort([Ljava/lang/String;)V
    .locals 2
    .param p0, "str"    # [Ljava/lang/String;

    .prologue
    .line 271
    if-nez p0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    array-length v0, p0

    if-lez v0, :cond_0

    .line 275
    const/4 v0, 0x0

    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->quickSortEx([Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public static quickSortArrayList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p0, :cond_1

    .line 268
    :cond_0
    return-void

    .line 257
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 258
    .local v2, "length":I
    new-array v3, v2, [Ljava/lang/String;

    .line 259
    .local v3, "temStrings":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 260
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, v3, v0

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_2
    invoke-static {v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->quickSort([Ljava/lang/String;)V

    .line 265
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 266
    aget-object v4, v3, v1

    invoke-virtual {p0, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 265
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static quickSortEx([Ljava/lang/String;II)V
    .locals 2
    .param p0, "list"    # [Ljava/lang/String;
    .param p1, "low"    # I
    .param p2, "high"    # I

    .prologue
    .line 280
    if-ge p1, p2, :cond_0

    .line 281
    invoke-static {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->getMiddle([Ljava/lang/String;II)I

    move-result v0

    .line 282
    .local v0, "middle":I
    add-int/lit8 v1, v0, -0x1

    invoke-static {p0, p1, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->quickSortEx([Ljava/lang/String;II)V

    .line 283
    add-int/lit8 v1, v0, 0x1

    invoke-static {p0, v1, p2}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->quickSortEx([Ljava/lang/String;II)V

    .line 285
    .end local v0    # "middle":I
    :cond_0
    return-void
.end method

.method public static randomIntDataFromArray(I)I
    .locals 6
    .param p0, "length"    # I

    .prologue
    .line 941
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    int-to-double v4, p0

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 942
    .local v0, "index":I
    return v0
.end method

.method public static randomIntDataFromArray([I)I
    .locals 6
    .param p0, "arr"    # [I

    .prologue
    .line 347
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    array-length v1, p0

    int-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 348
    .local v0, "index":I
    aget v1, p0, v0

    return v1
.end method

.method public static recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 406
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 409
    :cond_0
    const/4 p0, 0x0

    .line 410
    return-void
.end method

.method public static final refreshDBRecords(Landroid/content/Context;Ljava/lang/String;II)Z
    .locals 12
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "mExifJPG"    # Ljava/lang/String;
    .param p2, "sef_file_type"    # I
    .param p3, "orientation"    # I

    .prologue
    const/4 v8, 0x0

    .line 520
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 564
    :cond_0
    :goto_0
    return v8

    .line 524
    :cond_1
    const/4 v7, 0x0

    .line 526
    .local v7, "uri":Landroid/net/Uri;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 527
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 531
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 532
    .local v3, "fileNameWithoutName":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 533
    .local v4, "filePath":Ljava/lang/String;
    const-string v9, "image/jpeg"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v9, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 535
    .local v5, "mimeType":Ljava/lang/String;
    new-instance v6, Landroid/content/ContentValues;

    const/4 v8, 0x6

    invoke-direct {v6, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 536
    .local v6, "newValues":Landroid/content/ContentValues;
    const-string v8, "title"

    invoke-virtual {v6, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v8, "_display_name"

    invoke-virtual {v6, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const-string v8, "_data"

    invoke-virtual {v6, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const-string v8, "datetaken"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 540
    const-string v8, "_size"

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 541
    const-string v8, "mime_type"

    invoke-virtual {v6, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-string v8, "orientation"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 544
    packed-switch p2, :pswitch_data_0

    .line 557
    :goto_1
    sget-object v7, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 558
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 559
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v0, v7, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v7

    .line 561
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 562
    .local v1, "data":Landroid/net/Uri;
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v8, v9, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 564
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 551
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "data":Landroid/net/Uri;
    :pswitch_0
    const-string v8, "sef_file_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 544
    :pswitch_data_0
    .packed-switch 0x831
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static renameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 101
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v10

    .line 105
    :cond_1
    const/4 v4, 0x0

    .line 106
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 107
    .local v6, "fos":Ljava/io/FileOutputStream;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v9, "srcFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v1, "destFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 117
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 118
    .local v2, "destFileDir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_2

    .line 119
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 122
    :cond_2
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 129
    :goto_1
    :try_start_1
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6

    .line 130
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_2
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7

    .line 131
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    const/16 v12, 0x400

    :try_start_3
    new-array v0, v12, [B
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_4

    .line 132
    .local v0, "buffer":[B
    const/4 v8, 0x0

    .line 134
    .local v8, "n":I
    :goto_2
    const/4 v12, -0x1

    :try_start_4
    invoke-virtual {v5, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    if-eq v12, v8, :cond_5

    .line 135
    const/4 v12, 0x0

    invoke-virtual {v7, v0, v12, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 137
    :catch_0
    move-exception v3

    .line 138
    .local v3, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 141
    if-eqz v7, :cond_3

    .line 142
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 144
    :cond_3
    if-eqz v5, :cond_4

    .line 145
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_4
    move v10, v11

    .line 147
    goto :goto_0

    .line 123
    .end local v0    # "buffer":[B
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "n":I
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v3

    .line 124
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_1

    .line 141
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "n":I
    :cond_5
    if-eqz v7, :cond_6

    .line 142
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 144
    :cond_6
    if-eqz v5, :cond_7

    .line 145
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_7
    move v10, v11

    .line 147
    goto :goto_0

    .line 148
    :catch_2
    move-exception v3

    .line 149
    .local v3, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    :goto_3
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 154
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 148
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v3, "e":Ljava/io/IOException;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v3

    .line 149
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_3

    .line 152
    .end local v0    # "buffer":[B
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v8    # "n":I
    :catch_4
    move-exception v3

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 153
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :goto_4
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 140
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "n":I
    :catchall_0
    move-exception v12

    .line 141
    if-eqz v7, :cond_8

    .line 142
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 144
    :cond_8
    if-eqz v5, :cond_9

    .line 145
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_4

    :cond_9
    move v10, v11

    .line 147
    goto/16 :goto_0

    .line 148
    :catch_5
    move-exception v3

    .line 149
    .local v3, "e":Ljava/lang/Exception;
    :try_start_a
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 150
    throw v12
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_4

    .line 152
    .end local v0    # "buffer":[B
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "n":I
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v3

    goto :goto_4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v3

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_4
.end method

.method public static rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # I
    .param p2, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 352
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 353
    if-eqz p1, :cond_0

    .line 354
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 355
    .local v2, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 356
    const/16 v8, 0x8

    new-array v5, v8, [F

    .line 357
    .local v5, "srctmp":[F
    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 358
    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 359
    const/4 v8, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 360
    const/4 v8, 0x3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 361
    const/4 v8, 0x4

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 362
    const/4 v8, 0x5

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 363
    const/4 v8, 0x6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 364
    const/4 v8, 0x7

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 366
    int-to-float v8, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    invoke-virtual {v2, v8, v9, v10}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 368
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 369
    const/4 v8, 0x0

    aget v8, v5, v8

    const/4 v9, 0x2

    aget v9, v5, v9

    const/4 v10, 0x4

    aget v10, v5, v10

    const/4 v11, 0x6

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 371
    .local v3, "max":F
    const/4 v8, 0x0

    aget v8, v5, v8

    const/4 v9, 0x2

    aget v9, v5, v9

    const/4 v10, 0x4

    aget v10, v5, v10

    const/4 v11, 0x6

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 374
    .local v4, "min":F
    sub-float/2addr v3, v4

    .line 375
    float-to-int v7, v3

    .line 377
    .local v7, "width":I
    const/4 v8, 0x1

    aget v8, v5, v8

    const/4 v9, 0x3

    aget v9, v5, v9

    const/4 v10, 0x5

    aget v10, v5, v10

    const/4 v11, 0x7

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 379
    const/4 v8, 0x1

    aget v8, v5, v8

    const/4 v9, 0x3

    aget v9, v5, v9

    const/4 v10, 0x5

    aget v10, v5, v10

    const/4 v11, 0x7

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 382
    sub-float/2addr v3, v4

    .line 383
    float-to-int v1, v3

    .line 385
    .local v1, "height":I
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 386
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int/2addr v8, v7

    neg-int v8, v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v9, v1

    neg-int v9, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {v2, v8, v9}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 389
    int-to-float v8, p1

    div-int/lit8 v9, v7, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v1, 0x2

    int-to-float v10, v10

    invoke-virtual {v2, v8, v9, v10}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 391
    invoke-static {v7, v1, p2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 392
    .local v6, "temp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 393
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, p0, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 395
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 402
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "height":I
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v3    # "max":F
    .end local v4    # "min":F
    .end local v5    # "srctmp":[F
    .end local v6    # "temp":Landroid/graphics/Bitmap;
    .end local v7    # "width":I
    :goto_0
    return-object v6

    :cond_0
    move-object v6, p0

    .line 399
    goto :goto_0

    .line 402
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static rotatePoint(Landroid/graphics/Point;ILcom/arcsoft/magicshotstudio/utils/MSize;)Landroid/graphics/Point;
    .locals 3
    .param p0, "p"    # Landroid/graphics/Point;
    .param p1, "rotateDegree"    # I
    .param p2, "imageSize"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 644
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 645
    .local v0, "point":Landroid/graphics/Point;
    const/16 v1, 0x5a

    if-ne v1, p1, :cond_0

    .line 646
    iget v1, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    iget v2, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 647
    iget v1, p0, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 658
    :goto_0
    return-object v0

    .line 648
    :cond_0
    const/16 v1, 0xb4

    if-ne v1, p1, :cond_1

    .line 649
    iget v1, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iget v2, p0, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 650
    iget v1, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    iget v2, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 651
    :cond_1
    const/16 v1, 0x10e

    if-ne v1, p1, :cond_2

    .line 652
    iget v1, p2, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iget v2, p0, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 653
    iget v1, p0, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 655
    :cond_2
    iget v1, p0, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 656
    iget v1, p0, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public static saveBinary([BLjava/lang/String;)Z
    .locals 8
    .param p0, "bin"    # [B
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 946
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 947
    .local v5, "saveFile":Ljava/io/File;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 976
    :cond_0
    :goto_0
    return v6

    .line 951
    :cond_1
    const-string v7, "/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 952
    .local v4, "lastIndex":I
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 954
    .local v3, "folder":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 958
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 959
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 961
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3

    .line 963
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 964
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 966
    .local v2, "fileOutputStream":Ljava/io/FileOutputStream;
    if-eqz v2, :cond_2

    .line 967
    invoke-virtual {v2, p0}, Ljava/io/FileOutputStream;->write([B)V

    .line 969
    :cond_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 970
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 976
    .end local v2    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_3
    const/4 v6, 0x1

    goto :goto_0

    .line 971
    :catch_0
    move-exception v1

    .line 972
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static saveFileFromPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "destPath"    # Ljava/lang/String;

    .prologue
    .line 749
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 750
    :cond_0
    const-string v3, ""

    .line 803
    :goto_0
    return-object v3

    .line 753
    :cond_1
    move-object v3, p1

    .line 754
    .local v3, "destFullPath":Ljava/lang/String;
    const/4 v5, 0x0

    .line 755
    .local v5, "fis":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 756
    .local v7, "fos":Ljava/io/FileOutputStream;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 757
    .local v10, "srcFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 759
    .local v1, "destFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_2

    .line 760
    const-string v3, ""

    goto :goto_0

    .line 762
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 763
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 777
    :goto_1
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_6

    .line 778
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v6, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7

    .line 779
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .local v8, "fos":Ljava/io/FileOutputStream;
    const/16 v11, 0x400

    :try_start_2
    new-array v0, v11, [B
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    .line 780
    .local v0, "buffer":[B
    const/4 v9, 0x0

    .line 782
    .local v9, "n":I
    :goto_2
    const/4 v11, -0x1

    :try_start_3
    invoke-virtual {v6, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    if-eq v11, v9, :cond_7

    .line 783
    const/4 v11, 0x0

    invoke-virtual {v8, v0, v11, v9}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 785
    :catch_0
    move-exception v4

    .line 786
    .local v4, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 789
    if-eqz v8, :cond_3

    .line 790
    :try_start_5
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 792
    :cond_3
    if-eqz v6, :cond_4

    .line 793
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_3

    .end local v4    # "e":Ljava/io/IOException;
    :cond_4
    :goto_3
    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 801
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 765
    .end local v0    # "buffer":[B
    .end local v9    # "n":I
    :cond_5
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    .local v2, "destFileDir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_6

    .line 767
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 770
    :cond_6
    :try_start_6
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 771
    :catch_1
    move-exception v4

    .line 772
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_1

    .line 789
    .end local v2    # "destFileDir":Ljava/io/File;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "n":I
    :cond_7
    if-eqz v8, :cond_8

    .line 790
    :try_start_7
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 792
    :cond_8
    if-eqz v6, :cond_4

    .line 793
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_3

    .line 795
    :catch_2
    move-exception v4

    .line 796
    .local v4, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    .line 799
    .end local v0    # "buffer":[B
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v9    # "n":I
    :catch_3
    move-exception v4

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 800
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v4, "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_4
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 795
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .local v4, "e":Ljava/io/IOException;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "n":I
    :catch_4
    move-exception v4

    .line 796
    .local v4, "e":Ljava/lang/Exception;
    :try_start_9
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_3

    .line 788
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    .line 789
    if-eqz v8, :cond_9

    .line 790
    :try_start_a
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 792
    :cond_9
    if-eqz v6, :cond_a

    .line 793
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_3

    .line 797
    :cond_a
    :goto_5
    :try_start_b
    throw v11

    .line 795
    :catch_5
    move-exception v4

    .line 796
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_3

    goto :goto_5

    .line 799
    .end local v0    # "buffer":[B
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .end local v9    # "n":I
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v4

    goto :goto_4

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v4

    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_4
.end method

.method public static saveImage(Ljava/lang/String;[B)V
    .locals 9
    .param p0, "fullpath"    # Ljava/lang/String;
    .param p1, "jpegData"    # [B

    .prologue
    .line 839
    if-nez p0, :cond_1

    .line 840
    const-string v6, "ArcSoft_MagicShot_ArcUtils"

    const-string v7, "saveImage fullpath is null"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 844
    :cond_1
    if-nez p1, :cond_2

    .line 845
    const-string v6, "ArcSoft_MagicShot_ArcUtils"

    const-string v7, "saveImage jpegData is null"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 849
    :cond_2
    const/4 v5, 0x0

    .line 850
    .local v5, "resultDir":Ljava/lang/String;
    if-eqz p0, :cond_3

    .line 851
    const-string v6, "/"

    invoke-virtual {p0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 852
    .local v4, "lastIndex":I
    const/4 v6, 0x0

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 855
    .end local v4    # "lastIndex":I
    :cond_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 856
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_5

    .line 857
    const-string v6, "ArcSoft_MagicShot_ArcUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resultDir = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "is not exist"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 859
    const-string v6, "ArcSoft_MagicShot_ArcUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resultDir = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mkdirs the path, now it is ok"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    :goto_1
    const/4 v0, 0x0

    .line 866
    .local v0, "bos":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 867
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .local v1, "bos":Ljava/io/BufferedOutputStream;
    if-eqz v1, :cond_4

    .line 868
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 870
    :cond_4
    if-eqz v1, :cond_7

    .line 871
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 872
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 873
    const/4 v0, 0x0

    .line 879
    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    :goto_2
    if-eqz v0, :cond_0

    .line 881
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 884
    :goto_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 861
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    :cond_5
    const-string v6, "ArcSoft_MagicShot_ArcUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resultDir = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "is exist"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 875
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    :catch_0
    move-exception v2

    .line 876
    .local v2, "e":Ljava/io/IOException;
    :goto_4
    :try_start_3
    const-string v6, "ArcSoft_MagicShot_ArcUtils"

    const-string v7, "BufferedOutputStream have something wrong."

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 879
    if-eqz v0, :cond_0

    .line 881
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 884
    :goto_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 879
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_6
    if-eqz v0, :cond_6

    .line 881
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 884
    :goto_7
    const/4 v0, 0x0

    :cond_6
    throw v6

    .line 882
    :catch_1
    move-exception v6

    goto :goto_3

    .restart local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    goto :goto_5

    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    goto :goto_7

    .line 879
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_6

    .line 875
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_4

    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    :cond_7
    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_2
.end method

.method public static waitWithoutInterrupt(Ljava/lang/Object;)V
    .locals 4
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 339
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :goto_0
    return-void

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "ArcSoft_MagicShot_ArcUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected interrupt: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
