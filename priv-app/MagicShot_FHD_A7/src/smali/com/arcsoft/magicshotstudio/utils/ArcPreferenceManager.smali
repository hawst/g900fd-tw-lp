.class public Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;
.super Ljava/lang/Object;
.source "ArcPreferenceManager.java"


# static fields
.field public static final SETTING_KEY_DUMP_DATA:Ljava/lang/String; = "KEY_DUMP_DATA"

.field public static final SETTING_KEY_IS_FIRST_START:Ljava/lang/String; = "KEY_IS_FIRST_START"

.field public static final SETTING_KEY_MANUAL_MODE:Ljava/lang/String; = "KEY_MANUAL_MODE"

.field public static final SETTING_KEY_OPEN_LOG:Ljava/lang/String; = "KEY_OPEN_LOG"

.field public static final VALUE_DUMP_DATA_DEFAULT:Z = false

.field public static final VALUE_IS_FIRST_START_DEFAULT:Z = true

.field public static final VALUE_MANUAL_MODE_DEFAULT:Z

.field public static final VALUE_OPEN_LOG_DEFAULT:Z


# instance fields
.field private listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 25
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->initSharedPreferences(Landroid/content/Context;)V

    .line 26
    return-void
.end method


# virtual methods
.method public getFirstStart()Z
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "KEY_IS_FIRST_START"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public initSharedPreferences(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const-string v0, "PicAction_Setting"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    .line 30
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 31
    return-void
.end method

.method public isDumpDataChecked()Z
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "KEY_DUMP_DATA"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isManualModeChecked()Z
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "KEY_MANUAL_MODE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isOpenLogChecked()Z
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "KEY_OPEN_LOG"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public restoreDefault()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->setDumpData(Z)V

    .line 77
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->setManualMode(Z)V

    .line 78
    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->setOpenLog(Z)V

    .line 79
    return-void
.end method

.method public setDumpData(Z)V
    .locals 2
    .param p1, "mBoolean"    # Z

    .prologue
    .line 60
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "KEY_DUMP_DATA"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 61
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 62
    return-void
.end method

.method public setFirstStart(Z)V
    .locals 2
    .param p1, "isFirst"    # Z

    .prologue
    .line 38
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "KEY_IS_FIRST_START"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 39
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 40
    return-void
.end method

.method public setManualMode(Z)V
    .locals 2
    .param p1, "mBoolean"    # Z

    .prologue
    .line 49
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "KEY_MANUAL_MODE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 50
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 51
    return-void
.end method

.method public setOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 83
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 84
    return-void
.end method

.method public setOpenLog(Z)V
    .locals 2
    .param p1, "mBoolean"    # Z

    .prologue
    .line 70
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "KEY_OPEN_LOG"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 71
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 72
    return-void
.end method

.method public unregisterOnSharedPreferenceChangeListener()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->mPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/ArcPreferenceManager;->listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0
.end method
