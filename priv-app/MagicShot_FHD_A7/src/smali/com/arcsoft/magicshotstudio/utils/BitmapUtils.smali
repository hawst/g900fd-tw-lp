.class public Lcom/arcsoft/magicshotstudio/utils/BitmapUtils;
.super Ljava/lang/Object;
.source "BitmapUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ArcSoft_BitmapUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 91
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 94
    :cond_0
    const/4 p0, 0x0

    .line 95
    return-void
.end method

.method public static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "bmp"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 16
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 17
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 18
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 21
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 23
    .local v8, "temp":Landroid/graphics/Bitmap;
    if-eq p0, v8, :cond_0

    .line 24
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    move-object p0, v8

    .line 31
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v8    # "temp":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p0

    .line 27
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v7

    .line 28
    .local v7, "er":Ljava/lang/Error;
    invoke-virtual {v7}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method

.method public static rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # I
    .param p2, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 38
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 39
    if-eqz p1, :cond_0

    .line 40
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 41
    .local v2, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 42
    const/16 v8, 0x8

    new-array v5, v8, [F

    .line 43
    .local v5, "srctmp":[F
    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 44
    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 45
    const/4 v8, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 46
    const/4 v8, 0x3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 47
    const/4 v8, 0x4

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 48
    const/4 v8, 0x5

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 49
    const/4 v8, 0x6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 50
    const/4 v8, 0x7

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 52
    int-to-float v8, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    invoke-virtual {v2, v8, v9, v10}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 54
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 55
    const/4 v8, 0x0

    aget v8, v5, v8

    const/4 v9, 0x2

    aget v9, v5, v9

    const/4 v10, 0x4

    aget v10, v5, v10

    const/4 v11, 0x6

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 57
    .local v3, "max":F
    const/4 v8, 0x0

    aget v8, v5, v8

    const/4 v9, 0x2

    aget v9, v5, v9

    const/4 v10, 0x4

    aget v10, v5, v10

    const/4 v11, 0x6

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 60
    .local v4, "min":F
    sub-float/2addr v3, v4

    .line 61
    float-to-int v7, v3

    .line 63
    .local v7, "width":I
    const/4 v8, 0x1

    aget v8, v5, v8

    const/4 v9, 0x3

    aget v9, v5, v9

    const/4 v10, 0x5

    aget v10, v5, v10

    const/4 v11, 0x7

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 65
    const/4 v8, 0x1

    aget v8, v5, v8

    const/4 v9, 0x3

    aget v9, v5, v9

    const/4 v10, 0x5

    aget v10, v5, v10

    const/4 v11, 0x7

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 68
    sub-float/2addr v3, v4

    .line 69
    float-to-int v1, v3

    .line 71
    .local v1, "height":I
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 72
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int/2addr v8, v7

    neg-int v8, v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v9, v1

    neg-int v9, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {v2, v8, v9}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 75
    int-to-float v8, p1

    div-int/lit8 v9, v7, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v1, 0x2

    int-to-float v10, v10

    invoke-virtual {v2, v8, v9, v10}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 77
    invoke-static {v7, v1, p2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 78
    .local v6, "temp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, p0, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 81
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/BitmapUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 87
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "height":I
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v3    # "max":F
    .end local v4    # "min":F
    .end local v5    # "srctmp":[F
    .end local v6    # "temp":Landroid/graphics/Bitmap;
    .end local v7    # "width":I
    :goto_0
    return-object v6

    :cond_0
    move-object v6, p0

    .line 84
    goto :goto_0

    .line 87
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method
