.class public final enum Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;
.super Ljava/lang/Enum;
.source "TimeRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TimeRecordEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

.field public static final enum APP_START_PROCESS_DISMISS:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

.field public static final enum FACE_NUM:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

.field public static final enum MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

.field public static final enum SAVE_FILE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

.field public static final enum SELECT_TIME:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const-string v1, "APP_START_PROCESS_DISMISS"

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->APP_START_PROCESS_DISMISS:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const-string v1, "MERGE_FACE"

    invoke-direct {v0, v1, v3}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const-string v1, "SAVE_FILE"

    invoke-direct {v0, v1, v4}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SAVE_FILE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const-string v1, "FACE_NUM"

    invoke-direct {v0, v1, v5}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->FACE_NUM:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    const-string v1, "SELECT_TIME"

    invoke-direct {v0, v1, v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SELECT_TIME:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    .line 30
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->APP_START_PROCESS_DISMISS:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->MERGE_FACE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SAVE_FILE:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->FACE_NUM:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->SELECT_TIME:Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    aput-object v1, v0, v6

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->$VALUES:[Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    return-object v0
.end method

.method public static values()[Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->$VALUES:[Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    invoke-virtual {v0}, [Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;

    return-object v0
.end method
