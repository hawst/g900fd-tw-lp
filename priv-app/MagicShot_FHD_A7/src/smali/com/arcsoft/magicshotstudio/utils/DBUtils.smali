.class public Lcom/arcsoft/magicshotstudio/utils/DBUtils;
.super Ljava/lang/Object;
.source "DBUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getLatestThumbnalPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 9
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 280
    const/4 v2, 0x0

    .line 281
    .local v2, "thumbPath":Ljava/lang/String;
    sget-object v4, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 282
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_data"

    aput-object v8, v6, v7

    invoke-static {v5, v4, v6}, Landroid/provider/MediaStore$Images$Thumbnails;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 284
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    move-object v3, v2

    .line 299
    .end local v2    # "thumbPath":Ljava/lang/String;
    .local v3, "thumbPath":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 289
    .end local v3    # "thumbPath":Ljava/lang/String;
    .restart local v2    # "thumbPath":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 290
    const-string v5, "_data"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 296
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 297
    const/4 v0, 0x0

    :goto_1
    move-object v3, v2

    .line 299
    .end local v2    # "thumbPath":Ljava/lang/String;
    .restart local v3    # "thumbPath":Ljava/lang/String;
    goto :goto_0

    .line 293
    .end local v3    # "thumbPath":Ljava/lang/String;
    .restart local v2    # "thumbPath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 297
    const/4 v0, 0x0

    .line 298
    goto :goto_1

    .line 296
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 297
    const/4 v0, 0x0

    throw v5
.end method

.method public static final getUriFromImages(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 12
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 230
    sget-object v10, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 231
    .local v10, "mUri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 232
    .local v8, "mImageUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "bucket_display_name"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 235
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    move-object v9, v8

    .line 255
    .end local v8    # "mImageUri":Landroid/net/Uri;
    .local v9, "mImageUri":Landroid/net/Uri;
    :goto_0
    return-object v9

    .line 239
    .end local v9    # "mImageUri":Landroid/net/Uri;
    .restart local v8    # "mImageUri":Landroid/net/Uri;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 240
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 241
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 243
    .local v7, "data":Ljava/lang/String;
    invoke-static {p1, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 246
    .local v11, "ringtoneID":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 252
    .end local v7    # "data":Ljava/lang/String;
    .end local v11    # "ringtoneID":I
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 253
    const/4 v6, 0x0

    move-object v9, v8

    .line 255
    .end local v8    # "mImageUri":Landroid/net/Uri;
    .restart local v9    # "mImageUri":Landroid/net/Uri;
    goto :goto_0

    .line 249
    .end local v9    # "mImageUri":Landroid/net/Uri;
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v8    # "mImageUri":Landroid/net/Uri;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1
.end method

.method public static final getUriFromThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 9
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "thumbPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 260
    const/4 v0, 0x0

    .line 261
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 263
    .local v2, "image_id":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "image_id"

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/provider/MediaStore$Images$Thumbnails;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 267
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 268
    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 276
    :cond_0
    :goto_0
    return-object v3

    .line 273
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static refreshDatabase(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imagePath"    # Ljava/lang/String;
    .param p2, "imageThumbPath"    # Ljava/lang/String;
    .param p3, "thumbWidth"    # I
    .param p4, "thumbHeight"    # I

    .prologue
    .line 26
    const/4 v2, 0x0

    .line 27
    .local v2, "success":Z
    invoke-static {p1}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->decodeBitmapFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 29
    .local v1, "SrcBitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 30
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    .line 66
    .end local v2    # "success":Z
    .local v3, "success":I
    :goto_0
    return v3

    .line 33
    .end local v3    # "success":I
    .restart local v2    # "success":Z
    :cond_1
    const/4 v0, 0x0

    .line 34
    .local v0, "DesBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, p1}, Lcom/arcsoft/magicshotstudio/utils/DBUtils;->refreshImagesRecords(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 35
    if-nez v2, :cond_3

    .line 36
    if-eqz v1, :cond_2

    .line 37
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    move v3, v2

    .line 39
    .restart local v3    # "success":I
    goto :goto_0

    .line 42
    .end local v3    # "success":I
    :cond_3
    const/4 v4, 0x1

    invoke-static {v1, p3, p4, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 43
    invoke-static {v0, p2}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->convertBitmapToJPG(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v2

    .line 44
    if-nez v2, :cond_6

    .line 45
    if-eqz v1, :cond_4

    .line 46
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 49
    :cond_4
    if-eqz v0, :cond_5

    .line 50
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    move v3, v2

    .line 52
    .restart local v3    # "success":I
    goto :goto_0

    .line 55
    .end local v3    # "success":I
    :cond_6
    invoke-static {p0, p1, p2}, Lcom/arcsoft/magicshotstudio/utils/DBUtils;->refreshThumbnailsRecords(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 58
    if-eqz v1, :cond_7

    .line 59
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 62
    :cond_7
    if-eqz v0, :cond_8

    .line 63
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_8
    move v3, v2

    .line 66
    .restart local v3    # "success":I
    goto :goto_0
.end method

.method public static final refreshImagesRecords(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 21
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 71
    const/4 v10, 0x0

    .line 72
    .local v10, "cursor":Landroid/database/Cursor;
    const-string v7, "datetaken DESC, _id DESC"

    .line 74
    .local v7, "strOrderClause":Ljava/lang/String;
    const/4 v15, 0x0

    .line 75
    .local v15, "id":I
    const/4 v12, 0x0

    .line 76
    .local v12, "fileName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 78
    .local v8, "bExist":Z
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_data"

    aput-object v6, v4, v5

    const-string v5, "_data=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/16 v20, 0x0

    aput-object p1, v6, v20

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 83
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 85
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 86
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 87
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    .line 89
    const/4 v8, 0x1

    .line 96
    :cond_1
    :goto_0
    if-eqz v10, :cond_2

    .line 97
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 98
    const/4 v10, 0x0

    .line 102
    :cond_2
    const/16 v19, 0x0

    .line 103
    .local v19, "uri":Landroid/net/Uri;
    if-eqz v15, :cond_3

    if-eqz v8, :cond_3

    .line 104
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 106
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 109
    :cond_3
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    .local v11, "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_6

    .line 111
    const/4 v2, 0x0

    .line 135
    :goto_1
    return v2

    .line 93
    .end local v11    # "file":Ljava/io/File;
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_4
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_5

    .line 97
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 98
    const/4 v10, 0x0

    :cond_5
    throw v2

    .line 114
    .restart local v11    # "file":Ljava/io/File;
    .restart local v19    # "uri":Landroid/net/Uri;
    :cond_6
    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    .line 115
    .local v13, "fileNameWithoutName":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    .line 116
    .local v14, "filePath":Ljava/lang/String;
    const-string v2, "image/jpeg"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 117
    .local v16, "mimeType":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->getExifOrientation(Ljava/lang/String;)I

    move-result v18

    .line 119
    .local v18, "orientation":I
    new-instance v17, Landroid/content/ContentValues;

    const/4 v2, 0x6

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 120
    .local v17, "newValues":Landroid/content/ContentValues;
    const-string v2, "title"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "_display_name"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "_data"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v2, "date_modified"

    invoke-virtual {v11}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 126
    const-string v2, "_size"

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 127
    const-string v2, "mime_type"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v2, "orientation"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 130
    sget-object v19, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 132
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 133
    .local v9, "cr":Landroid/content/ContentResolver;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v19

    .line 135
    const/4 v2, 0x1

    goto/16 :goto_1
.end method

.method public static final refreshThumbnailsRecords(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 19
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "mSrcPath"    # Ljava/lang/String;
    .param p2, "mThumbPath"    # Ljava/lang/String;

    .prologue
    .line 141
    const-string v6, "datetaken DESC, _id DESC"

    .line 143
    .local v6, "strOrderClause":Ljava/lang/String;
    const/4 v10, 0x0

    .line 144
    .local v10, "fileName":Ljava/lang/String;
    const/16 v17, 0x0

    .line 146
    .local v17, "uri":Landroid/net/Uri;
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 147
    .local v13, "srcfile":Ljava/io/File;
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .local v15, "thumbfile":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 149
    :cond_0
    const/4 v1, 0x0

    .line 224
    :goto_0
    return v1

    .line 151
    :cond_1
    const/4 v9, 0x0

    .line 152
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 153
    .local v11, "id":I
    const/4 v7, 0x0

    .line 155
    .local v7, "bHasSame":Z
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "_data"

    aput-object v5, v3, v4

    const-string v4, "_data=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/16 v18, 0x0

    aput-object p1, v5, v18

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 160
    if-eqz v9, :cond_3

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 162
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 163
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 164
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_9

    .line 165
    const/4 v7, 0x1

    .line 171
    :cond_3
    :goto_1
    if-eqz v9, :cond_4

    .line 172
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 173
    const/4 v9, 0x0

    .line 177
    :cond_4
    if-eqz v7, :cond_d

    .line 178
    const/4 v7, 0x0

    .line 179
    const/4 v14, 0x0

    .line 181
    .local v14, "thumbId":I
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "_data"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/provider/MediaStore$Images$Thumbnails;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 185
    if-eqz v9, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 187
    :cond_5
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 188
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 189
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v1, :cond_b

    .line 190
    const/4 v7, 0x1

    .line 196
    :cond_6
    :goto_2
    if-eqz v9, :cond_7

    .line 197
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 198
    const/4 v9, 0x0

    .line 202
    :cond_7
    invoke-static/range {p2 .. p2}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->decodeBitmapFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 203
    .local v16, "thumbnailBitmap":Landroid/graphics/Bitmap;
    new-instance v12, Landroid/content/ContentValues;

    const/4 v1, 0x6

    invoke-direct {v12, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 204
    .local v12, "newValues":Landroid/content/ContentValues;
    const-string v1, "_data"

    move-object/from16 v0, p2

    invoke-virtual {v12, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v1, "image_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 206
    const-string v1, "kind"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 207
    const-string v1, "height"

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    const-string v1, "width"

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 212
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 213
    .local v8, "cr":Landroid/content/ContentResolver;
    if-eqz v7, :cond_8

    .line 214
    sget-object v1, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 217
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v8, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 219
    :cond_8
    sget-object v17, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 220
    move-object/from16 v0, v17

    invoke-virtual {v8, v0, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v17

    .line 222
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 168
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v12    # "newValues":Landroid/content/ContentValues;
    .end local v14    # "thumbId":I
    .end local v16    # "thumbnailBitmap":Landroid/graphics/Bitmap;
    :cond_9
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_1

    .line 171
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_a

    .line 172
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 173
    const/4 v9, 0x0

    :cond_a
    throw v1

    .line 193
    .restart local v14    # "thumbId":I
    :cond_b
    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v1

    if-nez v1, :cond_5

    goto :goto_2

    .line 196
    :catchall_1
    move-exception v1

    if-eqz v9, :cond_c

    .line 197
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 198
    const/4 v9, 0x0

    :cond_c
    throw v1

    .line 224
    .end local v14    # "thumbId":I
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
