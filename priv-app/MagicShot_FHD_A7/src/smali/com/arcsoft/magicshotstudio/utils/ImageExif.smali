.class public Lcom/arcsoft/magicshotstudio/utils/ImageExif;
.super Ljava/lang/Object;
.source "ImageExif.java"


# static fields
.field public static final AME_TAGID_APERTUREVALUE:I = 0x9202

.field public static final AME_TAGID_BRIGHTNESSVALUE:I = 0x9203

.field public static final AME_TAGID_EXPOSUREBIASVALUE:I = 0x9204

.field public static final AME_TAGID_EXPOSURETIME:I = 0x829a

.field public static final AME_TAGID_FLASH:I = 0x9209

.field public static final AME_TAGID_FNUMBER:I = 0x829d

.field public static final AME_TAGID_FOCALLENGTH:I = 0x920a

.field public static final AME_TAGID_ISOSPEEDRATINGS:I = 0x8827

.field public static final AME_TAGID_MAXAPERTUREVALUE:I = 0x9205

.field public static final AME_TAGID_SCENECAPTURETYPE:I = 0xa406

.field public static final AME_TAGID_SHUTTERSPEEDVALUE:I = 0x9201

.field public static final DELETE_EXIF_FIELDS:[I

.field public static final MODIFIED_EXIF_NUM:[I

.field public static final MODIFIED_SCENECAPTURETYPE:I = 0x2

.field public static final MODIFY_EXIF_FIELDS:[I

.field private static final TAG:Ljava/lang/String; = "ArcSoft_ImageExif"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->DELETE_EXIF_FIELDS:[I

    .line 36
    new-array v0, v3, [I

    const v1, 0xa406

    aput v1, v0, v2

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->MODIFY_EXIF_FIELDS:[I

    .line 41
    new-array v0, v3, [I

    const/4 v1, 0x2

    aput v1, v0, v2

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->MODIFIED_EXIF_NUM:[I

    return-void

    .line 28
    :array_0
    .array-data 4
        0x829a
        0x8827
        0x9201
        0x9202
        0x9203
        0x9204
        0x9209
        0x920a
        0x829d
        0x9205
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getExifOrientation(Ljava/lang/String;)I
    .locals 8
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 49
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 53
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 54
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v7}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 56
    .local v4, "orientation":I
    if-eq v4, v7, :cond_0

    .line 58
    packed-switch v4, :pswitch_data_0

    .line 71
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "ex":Ljava/io/IOException;
    const-string v5, "ArcSoft_ImageExif"

    const-string v6, "cannot read exif"

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 61
    goto :goto_1

    .line 63
    :pswitch_2
    const/16 v0, 0xb4

    .line 64
    goto :goto_1

    .line 66
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
