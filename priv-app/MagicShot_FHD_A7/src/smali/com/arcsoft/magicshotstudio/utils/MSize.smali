.class public Lcom/arcsoft/magicshotstudio/utils/MSize;
.super Ljava/lang/Object;
.source "MSize.java"


# instance fields
.field public height:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "cx"    # I
    .param p2, "cy"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 43
    iput p2, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 44
    return-void
.end method


# virtual methods
.method public copy(Lcom/arcsoft/magicshotstudio/utils/MSize;)V
    .locals 1
    .param p1, "s"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 55
    if-eqz p1, :cond_0

    .line 56
    iget v0, p1, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 57
    iget v0, p1, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    iput v0, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 59
    :cond_0
    return-void
.end method

.method public equals(II)Z
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 47
    iget v0, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Lcom/arcsoft/magicshotstudio/utils/MSize;)Z
    .locals 2
    .param p1, "s"    # Lcom/arcsoft/magicshotstudio/utils/MSize;

    .prologue
    .line 51
    if-eqz p1, :cond_0

    iget v0, p1, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    iget v1, p1, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    invoke-virtual {p0, v0, v1}, Lcom/arcsoft/magicshotstudio/utils/MSize;->equals(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MSize [width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
