.class public Lcom/arcsoft/magicshotstudio/utils/TimeRecord;
.super Ljava/lang/Object;
.source "TimeRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/utils/TimeRecord$1;,
        Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;
    }
.end annotation


# static fields
.field public static final BEST_FACE_PERFORMANCE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ArcSoft_TimeRecord"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMergeFace:J

.field private mMergetimeTextView:Landroid/widget/TextView;

.field private mOther:J

.field private mPicBestFD:Landroid/widget/TextView;

.field private mPicBestGetResultImages:Landroid/widget/TextView;

.field private mPicBestGetResultInfo:Landroid/widget/TextView;

.field private mPicBestInitMemory:Landroid/widget/TextView;

.field private mPicBestProcess:Landroid/widget/TextView;

.field private mProcessingTextView:Landroid/widget/TextView;

.field private mSaveFile:J

.field private mSavetimeTextView:Landroid/widget/TextView;

.field private mSelectFrameAlgorithmInit:Landroid/widget/TextView;

.field private mSelectFrameAlgorithmProcess:Landroid/widget/TextView;

.field private mSelectFrameAlgorithmUninit:Landroid/widget/TextView;

.field private mSelectFrameFaceDetect:Landroid/widget/TextView;

.field private mSelectFrameInitMemory:Landroid/widget/TextView;

.field private mSelectTime:J

.field private mSelectTimeTextView:Landroid/widget/TextView;

.field private mShowFaceNumberTextView:Landroid/widget/TextView;

.field private mStartDismiss:J

.field private m_Factory:Landroid/view/LayoutInflater;

.field private m_MainView:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->MAGICSHOT_PERFORMANCE_LOGPATH:Ljava/lang/String;

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->BEST_FACE_PERFORMANCE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mStartDismiss:J

    .line 25
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergeFace:J

    .line 26
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSaveFile:J

    .line 27
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mOther:J

    .line 28
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTime:J

    .line 34
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mContext:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_Factory:Landroid/view/LayoutInflater;

    .line 36
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    .line 37
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTimeTextView:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mShowFaceNumberTextView:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mProcessingTextView:Landroid/widget/TextView;

    .line 40
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergetimeTextView:Landroid/widget/TextView;

    .line 41
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSavetimeTextView:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameInitMemory:Landroid/widget/TextView;

    .line 45
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameFaceDetect:Landroid/widget/TextView;

    .line 46
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmInit:Landroid/widget/TextView;

    .line 47
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmProcess:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmUninit:Landroid/widget/TextView;

    .line 182
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestInitMemory:Landroid/widget/TextView;

    .line 183
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestFD:Landroid/widget/TextView;

    .line 184
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestProcess:Landroid/widget/TextView;

    .line 185
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultImages:Landroid/widget/TextView;

    .line 186
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultInfo:Landroid/widget/TextView;

    .line 51
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method

.method private calculateTime(JJZ)J
    .locals 3
    .param p1, "time"    # J
    .param p3, "record"    # J
    .param p5, "isStart"    # Z

    .prologue
    .line 155
    if-eqz p5, :cond_0

    .line 156
    move-wide p3, p1

    move-wide v0, p3

    .line 161
    .end local p3    # "record":J
    .local v0, "record":J
    :goto_0
    return-wide v0

    .line 158
    .end local v0    # "record":J
    .restart local p3    # "record":J
    :cond_0
    sub-long p3, p1, p3

    move-wide v0, p3

    .line 159
    .end local p3    # "record":J
    .restart local v0    # "record":J
    goto :goto_0
.end method

.method private createFile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "fullPath"    # Ljava/lang/String;

    .prologue
    .line 210
    if-nez p1, :cond_1

    .line 211
    const/4 v3, 0x0

    .line 223
    :cond_0
    :goto_0
    return v3

    .line 214
    :cond_1
    const/4 v3, 0x0

    .line 215
    .local v3, "success":Z
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    .line 218
    .local v2, "filePathStr":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .local v1, "filePath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 220
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v3

    goto :goto_0
.end method

.method private initBestFace()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f090031

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestInitMemory:Landroid/widget/TextView;

    .line 190
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestFD:Landroid/widget/TextView;

    .line 191
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f090033

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestProcess:Landroid/widget/TextView;

    .line 192
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultImages:Landroid/widget/TextView;

    .line 193
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultInfo:Landroid/widget/TextView;

    .line 194
    return-void
.end method

.method private initSelectFrame()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameInitMemory:Landroid/widget/TextView;

    .line 166
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f09002b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameFaceDetect:Landroid/widget/TextView;

    .line 167
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f09002c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmInit:Landroid/widget/TextView;

    .line 168
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmProcess:Landroid/widget/TextView;

    .line 169
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v1, 0x7f09002e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmUninit:Landroid/widget/TextView;

    .line 170
    return-void
.end method

.method private updateTimeView(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "tag"    # Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;
    .param p2, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 72
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$1;->$SwitchMap$com$arcsoft$magicshotstudio$utils$TimeRecord$TimeRecordEnum:[I

    invoke-virtual {p1}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 111
    :goto_0
    return-void

    .line 74
    :pswitch_0
    if-eqz p2, :cond_0

    .line 75
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mShowFaceNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mShowFaceNumberTextView:Landroid/widget/TextView;

    const-string v1, "person and face number:"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 81
    :pswitch_1
    if-eqz p2, :cond_1

    .line 82
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mProcessingTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mProcessingTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Total process time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mStartDismiss:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 88
    :pswitch_2
    if-eqz p2, :cond_2

    .line 89
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergetimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergetimeTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "merge time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergeFace:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 95
    :pswitch_3
    if-eqz p2, :cond_3

    .line 96
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSavetimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 98
    :cond_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSavetimeTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "save time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSaveFile:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 102
    :pswitch_4
    if-eqz p2, :cond_4

    .line 103
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 105
    :cond_4
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTimeTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select photo total cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private writeData()V
    .locals 12

    .prologue
    .line 227
    const-string v8, "yyyyMMddkkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v8, v10, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 228
    .local v3, "dataFormat":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->BEST_FACE_PERFORMANCE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "BestFace_Performance_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".txt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "bestFace":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 231
    .local v5, "f":Ljava/io/File;
    invoke-direct {p0, v0}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->createFile(Ljava/lang/String;)Z

    .line 234
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_0
    const/4 v6, 0x0

    .line 240
    .local v6, "fw":Ljava/io/FileWriter;
    const/4 v1, 0x0

    .line 243
    .local v1, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v7, Ljava/io/FileWriter;

    invoke-direct {v7, v0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    .end local v6    # "fw":Ljava/io/FileWriter;
    .local v7, "fw":Ljava/io/FileWriter;
    :try_start_2
    new-instance v2, Ljava/io/BufferedWriter;

    invoke-direct {v2, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 246
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .local v2, "bw":Ljava/io/BufferedWriter;
    :try_start_3
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameInitMemory:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 247
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 248
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameFaceDetect:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 249
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 250
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmInit:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 252
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmProcess:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 253
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 254
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmUninit:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 257
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestInitMemory:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 258
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 259
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestFD:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 260
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 261
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestProcess:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 262
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 263
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultImages:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 264
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 265
    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultInfo:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 266
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->newLine()V

    .line 268
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 273
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileWriter;->close()V

    .line 274
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 279
    :goto_1
    const/4 v6, 0x0

    .line 280
    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    const/4 v1, 0x0

    .line 282
    .end local v2    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :goto_2
    return-void

    .line 235
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .end local v6    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v4

    .line 236
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 275
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v2    # "bw":Ljava/io/BufferedWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catch_1
    move-exception v4

    .line 276
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 269
    .end local v2    # "bw":Ljava/io/BufferedWriter;
    .end local v4    # "e":Ljava/io/IOException;
    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    :catch_2
    move-exception v4

    .line 270
    .restart local v4    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 273
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileWriter;->close()V

    .line 274
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 279
    :goto_4
    const/4 v6, 0x0

    .line 280
    const/4 v1, 0x0

    .line 281
    goto :goto_2

    .line 275
    :catch_3
    move-exception v4

    .line 276
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 272
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 273
    :goto_5
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileWriter;->close()V

    .line 274
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 279
    :goto_6
    const/4 v6, 0x0

    .line 280
    const/4 v1, 0x0

    throw v8

    .line 275
    :catch_4
    move-exception v4

    .line 276
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 272
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "fw":Ljava/io/FileWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    goto :goto_5

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .end local v6    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "bw":Ljava/io/BufferedWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catchall_2
    move-exception v8

    move-object v1, v2

    .end local v2    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    move-object v6, v7

    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    goto :goto_5

    .line 269
    .end local v6    # "fw":Ljava/io/FileWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catch_5
    move-exception v4

    move-object v6, v7

    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    goto :goto_3

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .end local v6    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "bw":Ljava/io/BufferedWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catch_6
    move-exception v4

    move-object v1, v2

    .end local v2    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    move-object v6, v7

    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    goto :goto_3
.end method


# virtual methods
.method public getMergeFaceTime()J
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergeFace:J

    return-wide v0
.end method

.method public getOtherTime()J
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mOther:J

    return-wide v0
.end method

.method public getSaveFileTime()J
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSaveFile:J

    return-wide v0
.end method

.method public getStartDismissTime()J
    .locals 2

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mStartDismiss:J

    return-wide v0
.end method

.method public initShowTimeView(Landroid/widget/RelativeLayout;)V
    .locals 5
    .param p1, "mainLayout"    # Landroid/widget/RelativeLayout;

    .prologue
    const/4 v4, -0x2

    .line 55
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_Factory:Landroid/view/LayoutInflater;

    .line 56
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_Factory:Landroid/view/LayoutInflater;

    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    .line 57
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v2, 0x7f09002f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTimeTextView:Landroid/widget/TextView;

    .line 58
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v2, 0x7f090030

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mShowFaceNumberTextView:Landroid/widget/TextView;

    .line 59
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v2, 0x7f090036

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mProcessingTextView:Landroid/widget/TextView;

    .line 60
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v2, 0x7f090037

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergetimeTextView:Landroid/widget/TextView;

    .line 61
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    const v2, 0x7f090038

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSavetimeTextView:Landroid/widget/TextView;

    .line 63
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->initSelectFrame()V

    .line 64
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->initBestFace()V

    .line 65
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 66
    .local v0, "pm":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 67
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 68
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->m_MainView:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    return-void
.end method

.method public performanceRecord(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;ZLjava/lang/CharSequence;)J
    .locals 7
    .param p1, "tag"    # Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;
    .param p2, "isStart"    # Z
    .param p3, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 130
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 131
    .local v2, "time":J
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$1;->$SwitchMap$com$arcsoft$magicshotstudio$utils$TimeRecord$TimeRecordEnum:[I

    invoke-virtual {p1}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 145
    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mOther:J

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->calculateTime(JJZ)J

    move-result-wide v2

    .end local v2    # "time":J
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mOther:J

    .line 148
    .restart local v2    # "time":J
    :goto_0
    if-nez p2, :cond_0

    .line 149
    invoke-direct {p0, p1, p3}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->updateTimeView(Lcom/arcsoft/magicshotstudio/utils/TimeRecord$TimeRecordEnum;Ljava/lang/CharSequence;)V

    .line 151
    :cond_0
    return-wide v2

    .line 133
    :pswitch_0
    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mStartDismiss:J

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->calculateTime(JJZ)J

    move-result-wide v2

    .end local v2    # "time":J
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mStartDismiss:J

    .line 134
    .restart local v2    # "time":J
    goto :goto_0

    .line 136
    :pswitch_1
    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergeFace:J

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->calculateTime(JJZ)J

    move-result-wide v2

    .end local v2    # "time":J
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mMergeFace:J

    .line 137
    .restart local v2    # "time":J
    goto :goto_0

    .line 139
    :pswitch_2
    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSaveFile:J

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->calculateTime(JJZ)J

    move-result-wide v2

    .end local v2    # "time":J
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSaveFile:J

    .line 140
    .restart local v2    # "time":J
    goto :goto_0

    .line 142
    :pswitch_3
    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTime:J

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->calculateTime(JJZ)J

    move-result-wide v2

    .end local v2    # "time":J
    iput-wide v2, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectTime:J

    .line 143
    .restart local v2    # "time":J
    goto :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public showBestFacePerformance([I)V
    .locals 3
    .param p1, "bestFaceData"    # [I

    .prologue
    .line 197
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestInitMemory:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PicBest init memory cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestFD:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PicBest face detect cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestProcess:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PicBest process frames cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultImages:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PicBest get result images cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mPicBestGetResultInfo:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PicBest get result info cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x4

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    return-void
.end method

.method public showSelectPerformance([I)V
    .locals 3
    .param p1, "selectData"    # [I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameInitMemory:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select photo init memory cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameFaceDetect:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select photo face detect cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmInit:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select photo algorithm init cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmProcess:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select photo algorithm process cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/utils/TimeRecord;->mSelectFrameAlgorithmUninit:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select photo algorithm uninit cost time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x4

    aget v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    return-void
.end method
