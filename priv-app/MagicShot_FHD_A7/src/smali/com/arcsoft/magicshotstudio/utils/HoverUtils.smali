.class public Lcom/arcsoft/magicshotstudio/utils/HoverUtils;
.super Ljava/lang/Object;
.source "HoverUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static removeHovering(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 11
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    invoke-virtual {p1, v1}, Landroid/view/View;->setHovered(Z)V

    .line 13
    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 15
    :cond_0
    return-void
.end method

.method public static setHovering(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 18
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/HoverUtils;->isHoveringUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {p1, v1}, Landroid/view/View;->setHovered(Z)V

    .line 20
    invoke-virtual {p1, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 22
    :cond_0
    return-void
.end method
