.class public Lcom/arcsoft/magicshotstudio/utils/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final convertBitmapToJPG(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 6
    .param p0, "mbitmap"    # Landroid/graphics/Bitmap;
    .param p1, "DstPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 173
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v3

    .line 177
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 178
    .local v2, "mThumbnailPath":Ljava/io/File;
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 180
    .local v0, "bos":Ljava/io/BufferedOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p0, v4, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 181
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    .line 182
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    const/4 v3, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v2    # "mThumbnailPath":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 185
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static final decodeBitmapFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "srcPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 160
    if-eqz p0, :cond_0

    const-string v2, ""

    if-ne v2, p0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-object v1

    .line 163
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 168
    .local v1, "tempBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public static getImageSize(Ljava/lang/String;)Lcom/arcsoft/magicshotstudio/utils/MSize;
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 20
    new-instance v1, Lcom/arcsoft/magicshotstudio/utils/MSize;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>()V

    .line 21
    .local v1, "size":Lcom/arcsoft/magicshotstudio/utils/MSize;
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 22
    .local v0, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 23
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 24
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v2, v1, Lcom/arcsoft/magicshotstudio/utils/MSize;->width:I

    .line 25
    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v2, v1, Lcom/arcsoft/magicshotstudio/utils/MSize;->height:I

    .line 26
    return-object v1
.end method

.method public static final grey(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v8, 0x0

    .line 30
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 31
    .local v6, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 33
    .local v4, "height":I
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 36
    .local v3, "faceIconGreyBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 37
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 38
    .local v5, "paint":Landroid/graphics/Paint;
    new-instance v1, Landroid/graphics/ColorMatrix;

    invoke-direct {v1}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 39
    .local v1, "colorMatrix":Landroid/graphics/ColorMatrix;
    invoke-virtual {v1, v8}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 40
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v2, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 42
    .local v2, "colorMatrixFilter":Landroid/graphics/ColorMatrixColorFilter;
    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 43
    invoke-virtual {v0, p0, v8, v8, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 44
    return-object v3
.end method

.method public static recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 153
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 156
    :cond_0
    const/4 p0, 0x0

    .line 157
    return-void
.end method

.method public static rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # I

    .prologue
    const/4 v1, 0x0

    .line 83
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    if-eqz p1, :cond_0

    .line 85
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 86
    .local v5, "matrix":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 87
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 89
    .local v7, "tempBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "tempBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v7

    :cond_0
    move-object v7, p0

    .line 92
    goto :goto_0

    .line 95
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public static rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "src"    # Landroid/graphics/Bitmap;
    .param p1, "degree"    # I
    .param p2, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 99
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_1

    .line 100
    if-eqz p1, :cond_0

    .line 101
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 102
    .local v2, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 103
    const/16 v8, 0x8

    new-array v5, v8, [F

    .line 104
    .local v5, "srctmp":[F
    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 105
    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 106
    const/4 v8, 0x2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 107
    const/4 v8, 0x3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 108
    const/4 v8, 0x4

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 109
    const/4 v8, 0x5

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 110
    const/4 v8, 0x6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    aput v9, v5, v8

    .line 111
    const/4 v8, 0x7

    const/4 v9, 0x0

    aput v9, v5, v8

    .line 113
    int-to-float v8, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    invoke-virtual {v2, v8, v9, v10}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 115
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 116
    const/4 v8, 0x0

    aget v8, v5, v8

    const/4 v9, 0x2

    aget v9, v5, v9

    const/4 v10, 0x4

    aget v10, v5, v10

    const/4 v11, 0x6

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 118
    .local v3, "max":F
    const/4 v8, 0x0

    aget v8, v5, v8

    const/4 v9, 0x2

    aget v9, v5, v9

    const/4 v10, 0x4

    aget v10, v5, v10

    const/4 v11, 0x6

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 121
    .local v4, "min":F
    sub-float/2addr v3, v4

    .line 122
    float-to-int v7, v3

    .line 124
    .local v7, "width":I
    const/4 v8, 0x1

    aget v8, v5, v8

    const/4 v9, 0x3

    aget v9, v5, v9

    const/4 v10, 0x5

    aget v10, v5, v10

    const/4 v11, 0x7

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 126
    const/4 v8, 0x1

    aget v8, v5, v8

    const/4 v9, 0x3

    aget v9, v5, v9

    const/4 v10, 0x5

    aget v10, v5, v10

    const/4 v11, 0x7

    aget v11, v5, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 129
    sub-float/2addr v3, v4

    .line 130
    float-to-int v1, v3

    .line 132
    .local v1, "height":I
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 133
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int/2addr v8, v7

    neg-int v8, v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v9, v1

    neg-int v9, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {v2, v8, v9}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 136
    int-to-float v8, p1

    div-int/lit8 v9, v7, 0x2

    int-to-float v9, v9

    div-int/lit8 v10, v1, 0x2

    int-to-float v10, v10

    invoke-virtual {v2, v8, v9, v10}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 138
    invoke-static {v7, v1, p2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 139
    .local v6, "temp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 140
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0, p0, v2, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 142
    invoke-static {p0}, Lcom/arcsoft/magicshotstudio/utils/ImageUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 149
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "height":I
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v3    # "max":F
    .end local v4    # "min":F
    .end local v5    # "srctmp":[F
    .end local v6    # "temp":Landroid/graphics/Bitmap;
    .end local v7    # "width":I
    :goto_0
    return-object v6

    :cond_0
    move-object v6, p0

    .line 146
    goto :goto_0

    .line 149
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static saveBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;I)Z
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "quality"    # I

    .prologue
    const/4 v6, 0x0

    .line 48
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .local v5, "saveFile":Ljava/io/File;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v6

    .line 53
    :cond_1
    const-string v7, "/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 54
    .local v4, "lastIndex":I
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 56
    .local v3, "folder":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 60
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 63
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3

    .line 65
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 66
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 68
    .local v2, "fileOutputStream":Ljava/io/FileOutputStream;
    if-eqz v2, :cond_2

    .line 69
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p0, v7, p2, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 72
    :cond_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 73
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .end local v2    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_3
    const/4 v6, 0x1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
