.class public interface abstract Lcom/arcsoft/magicshotstudio/utils/APP_Constant;
.super Ljava/lang/Object;
.source "APP_Constant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/utils/APP_Constant$MAGICSHOT_FILE_TYPE;
    }
.end annotation


# static fields
.field public static final ACTION_PRIVATE_MODE_OFF:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field public static final ACTION_PRIVATE_MODE_ON:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"

.field public static final ARCPICLEAR_AUTO_MODE:I = 0x0

.field public static final ARCPICLEAR_DUG_NO:Z = true

.field public static final ARCPICLEAR_MUAUL_MODE:I = 0x1

.field public static final BESTFACE:Ljava/lang/String; = "BestFace_"

.field public static final BESTFACE_MODE_NAME:Ljava/lang/String; = "com.arcsoft.magicshotstudio.BestFace"

.field public static final BESTPHOTO:Ljava/lang/String; = "BestPhoto_"

.field public static final BESTPHOTO_MODE_NAME:Ljava/lang/String; = "com.arcsoft.magicshotstudio.BestPhoto"

.field public static final BITMAP_ADDRESS:Ljava/lang/String; = "bitmap_address"

.field public static final BITMAP_COUNT:Ljava/lang/String; = "bitmap_count"

.field public static final BITMAP_HEIGTH:Ljava/lang/String; = "bitmap_heigth"

.field public static final BITMAP_WIDTH:Ljava/lang/String; = "bitmap_width"

.field public static final CAMERA_DEGRESS:Ljava/lang/String; = "camera_degress"

.field public static final CPU_CORE_NUM:I = 0x4

.field public static final DATE_FORMAT:Ljava/lang/String; = "yyyyMMdd_kkmmss"

.field public static final DRAMA:Ljava/lang/String; = "Drama_"

.field public static final DRAMA_MODE_NAME:Ljava/lang/String; = "com.arcsoft.magicshotstudio.Drama"

.field public static final ERASER:Ljava/lang/String; = "Eraser_"

.field public static final ERASER_MODE_NAME:Ljava/lang/String; = "com.arcsoft.magicshotstudio.Eraser"

.field public static final ERROR:I = -0x1

.field public static final FRONT_CAMERA_PIC_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field public static final HELP_TYPE_BEST_FACE:I = 0x11

.field public static final HELP_TYPE_BEST_PHOTO:I = 0x10

.field public static final HELP_TYPE_DRAMA_SHOT:I = 0x12

.field public static final HELP_TYPE_ERASER:I = 0x13

.field public static final HELP_TYPE_PICMOTION_BLUR:I = 0x14

.field public static final HELP_TYPE_PICMOTION_OBJECT:I = 0x15

.field public static final IMAGE_HEIGHT:Ljava/lang/String; = "image_height"

.field public static final IMAGE_WIDTH:Ljava/lang/String; = "image_width"

.field public static final INPUT_PHOTOS_NUM:I = 0x14

.field public static final INTENT_NV21_IMAGE_SIZE:Ljava/lang/String; = "image_size"

.field public static final INTENT_PATHS_NAME:Ljava/lang/String; = "selectedItems"

.field public static final INTENT_RESULT_PATH_NAME_BEST_FACE:Ljava/lang/String; = "bestface"

.field public static final INTENT_RESULT_PATH_NAME_BEST_PHOTO:Ljava/lang/String; = "bestphoto"

.field public static final INTENT_RESULT_PATH_NAME_DRAMA:Ljava/lang/String; = "drama"

.field public static final INTENT_RESULT_PATH_NAME_ERASER:Ljava/lang/String; = "eraser"

.field public static final INTENT_RESULT_PATH_NAME_PICMOTION:Ljava/lang/String; = "picmotion"

.field public static final INTENT_SEF_PATH_NAME:Ljava/lang/String; = "sef_file_name"

.field public static final KEY_TYPE_BEST_FACE:Ljava/lang/String; = "TYPE_BEST_FACE"

.field public static final KEY_TYPE_BEST_PHOTO:Ljava/lang/String; = "TYPE_BEST_PHOTO"

.field public static final KEY_TYPE_DELETE_DAILOG_NEED_SHOW:Ljava/lang/String; = "TYPE_DELETE_DAILOG_NEED_SHOW"

.field public static final KEY_TYPE_DRAMA:Ljava/lang/String; = "TYPE_DRAMA"

.field public static final KEY_TYPE_ERASER:Ljava/lang/String; = "TYPE_ERASER"

.field public static final KEY_TYPE_PICMOTION_BlUR:Ljava/lang/String; = "TYPE_PICMOTION_BlUR"

.field public static final KEY_TYPE_PICMOTION_OBJECT:Ljava/lang/String; = "TYPE_PICMOTION_OBJECT"

.field public static final KEY_TYPE_SRC_IMAGE_NEED_DELETE:Ljava/lang/String; = "TYPE_SRC_IMAGE_NEED_DELETE"

.field public static final LAUNCHFROMGALLERY:Ljava/lang/String; = "lauch_mode"

.field public static final LOCK_OBJ1:Ljava/lang/Object;

.field public static final LOCK_OBJ2:Ljava/lang/Object;

.field public static final MAGICSHOT_PERFORMANCE_LOGPATH:Ljava/lang/String;

.field public static final MAGICSHOT_REQUESTCODE:I = 0x999999

.field public static final MAGICSHOT_SPF_KEY:Ljava/lang/String; = "MAGICSHOT_SPF_Key"

.field public static final MAIN_SELECT_SEF_REQUIRED_CODE:I = 0x1000

.field public static final MESSAGE_DECODE_FIRSTPICTURE_OK:I = 0x13

.field public static final MESSAGE_DECODE_PROGRESS:I = 0x12

.field public static final MESSAGE_DO_PROCESS:I = 0x16

.field public static final MESSAGE_FINISH_DECODE:I = 0x14

.field public static final MESSAGE_GET_INFO_ERROR:I = 0x10

.field public static final MESSAGE_INIT:I = 0x17

.field public static final MESSAGE_NO_SEF_FILE:I = 0x15

.field public static final MESSAGE_SEF_NOT_SUPPORT:I = 0x18

.field public static final MESSAGE_START_DECODE:I = 0x11

.field public static final MIN_STORAGE_SIZE:J = 0x1400000L

.field public static final MODE_NAME:Ljava/lang/String; = "mode_name"

.field public static final MPAF_OTHERS_NV21:I = 0x70000002

.field public static final OK:I = 0x0

.field public static final PACKAGE_NAME:Ljava/lang/String; = "PackageName"

.field public static final PICMOTION:Ljava/lang/String; = "Panningshot_"

.field public static final PICMOTION_MODE_NAME:Ljava/lang/String; = "com.arcsoft.magicshotstudio.PicMotion"

.field public static final PICTURE_NUM:I = 0x5

.field public static final PREDATA_ADDRESS:Ljava/lang/String; = "predata_address"

.field public static final PREDATA_SIZE:Ljava/lang/String; = "predata_size"

.field public static final PROGRESS_MAX_FOR_DECODING:I = 0x46

.field public static final PROGRESS_MAX_FOR_PROCESSING:I = 0x1e

.field public static final PROJECT_PACKAGE_NAME:Ljava/lang/String; = "com.arcsoft.magicshotstudio"

.field public static final RET_SEF_NOT_SUPPORT:I = 0x10008

.field public static final ROTATE_0:I = 0x0

.field public static final ROTATE_180:I = 0xb4

.field public static final ROTATE_270:I = 0x10e

.field public static final ROTATE_90:I = 0x5a

.field public static final SAMSUNG_GALLERY_CLASS_NAME:Ljava/lang/String; = "com.sec.android.gallery3d.app.Gallery"

.field public static final SAMSUNG_GALLERY_PACKAGE:Ljava/lang/String; = "com.sec.android.gallery3d"

.field public static final SAMSUNG_PRIVATE_PATH:Ljava/lang/String; = "/storage/Private/"

.field public static final SDCARD:Ljava/lang/String;

.field public static final SHOW_AUTO_NO_MASK_BITMAP:I = 0x1

.field public static final SHOW_AUTO_WITH_MASK_BITMAP:I = 0x2

.field public static final SIZE_SCALE:I = 0x2

.field public static final SSTUDIO:Ljava/lang/String;

.field public static final SUBMODE_FROM_MAIN:Ljava/lang/String; = "sub_mode_from_main"

.field public static final VALUE_5M_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

.field public static final VALUE_VGA_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->SDCARD:Ljava/lang/String;

    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->SDCARD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Studio/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->SSTUDIO:Ljava/lang/String;

    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/MagicShot_performance/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->MAGICSHOT_PERFORMANCE_LOGPATH:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->LOCK_OBJ1:Ljava/lang/Object;

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->LOCK_OBJ2:Ljava/lang/Object;

    .line 80
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    const/16 v1, 0x780

    const/16 v2, 0x438

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->FRONT_CAMERA_PIC_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 83
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    const/16 v1, 0xa20

    const/16 v2, 0x798

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->VALUE_5M_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    .line 84
    new-instance v0, Lcom/arcsoft/magicshotstudio/utils/MSize;

    const/16 v1, 0x500

    const/16 v2, 0x3c0

    invoke-direct {v0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MSize;-><init>(II)V

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/APP_Constant;->VALUE_VGA_SIZE:Lcom/arcsoft/magicshotstudio/utils/MSize;

    return-void
.end method
