.class public Lcom/arcsoft/magicshotstudio/utils/ToastController;
.super Ljava/lang/Object;
.source "ToastController.java"


# static fields
.field private static mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static closeToast()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 25
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    .line 28
    :cond_0
    return-void
.end method

.method public static makeQuickToast(Landroid/content/Context;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 31
    if-eqz p0, :cond_0

    .line 32
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 33
    :cond_0
    return-void
.end method

.method public static makeQuickToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 36
    if-eqz p0, :cond_0

    .line 37
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 38
    :cond_0
    return-void
.end method

.method public static makeToast(Landroid/content/Context;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 10
    if-eqz p0, :cond_0

    .line 11
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    .line 12
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 14
    :cond_0
    return-void
.end method

.method public static makeToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 17
    if-eqz p0, :cond_0

    .line 18
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    .line 19
    sget-object v0, Lcom/arcsoft/magicshotstudio/utils/ToastController;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 21
    :cond_0
    return-void
.end method
