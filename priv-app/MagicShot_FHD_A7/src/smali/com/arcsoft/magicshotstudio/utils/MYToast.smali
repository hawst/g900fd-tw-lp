.class public Lcom/arcsoft/magicshotstudio/utils/MYToast;
.super Ljava/lang/Object;
.source "MYToast.java"


# static fields
.field public static final DURATION_LONG:I = 0x1

.field public static final DURATION_SHORT:I

.field static mMessage:Ljava/lang/String;

.field static mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, ""

    sput-object v0, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bottomShow(Landroid/content/Context;II)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msgId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "msg":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 47
    .local v0, "empty":Z
    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 51
    :cond_0
    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 52
    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    if-eqz v2, :cond_1

    .line 53
    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 58
    :cond_1
    sput-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    .line 59
    invoke-static {p0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    sput-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    .line 60
    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static make(Landroid/content/Context;II)Landroid/widget/Toast;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "msg":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 66
    .local v0, "empty":Z
    if-eqz v0, :cond_0

    .line 67
    const/4 v2, 0x0

    .line 75
    :goto_0
    return-object v2

    .line 70
    :cond_0
    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    sput-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    .line 72
    invoke-static {p0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    sput-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    .line 75
    :cond_1
    sget-object v2, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;II)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 19
    if-nez p0, :cond_0

    .line 23
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "msg":Ljava/lang/String;
    invoke-static {p0, v0, p2}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    const/4 v3, 0x0

    .line 26
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 27
    .local v0, "empty":Z
    if-eqz v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 31
    :cond_0
    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 32
    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    .line 33
    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 38
    :cond_1
    sput-object p1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mMessage:Ljava/lang/String;

    .line 39
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    .line 40
    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    const/16 v2, 0x11

    invoke-virtual {v1, v2, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 41
    sget-object v1, Lcom/arcsoft/magicshotstudio/utils/MYToast;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
