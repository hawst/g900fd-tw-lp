.class public final Lcom/arcsoft/magicshotstudio/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CircleProgressBar:[I

.field public static final CircleProgressBar_background:I = 0x2

.field public static final CircleProgressBar_cursor_bar_normal:I = 0x0

.field public static final CircleProgressBar_cursor_bar_pressed:I = 0x1

.field public static final CircleProgressBar_cursor_offset:I = 0x3

.field public static final CircleProgressBar_direction:I = 0x5

.field public static final CircleProgressBar_zero_angle_position:I = 0x4

.field public static final FrameListView:[I

.field public static final FrameListView_android_choiceMode:I = 0xd

.field public static final FrameListView_android_drawSelectorOnTop:I = 0xb

.field public static final FrameListView_android_listSelector:I = 0xa

.field public static final FrameListView_android_orientation:I = 0x8

.field public static final FrameListView_android_scrollbarAlwaysDrawHorizontalTrack:I = 0x5

.field public static final FrameListView_android_scrollbarAlwaysDrawVerticalTrack:I = 0x6

.field public static final FrameListView_android_scrollbarDefaultDelayBeforeFade:I = 0xf

.field public static final FrameListView_android_scrollbarFadeDuration:I = 0xe

.field public static final FrameListView_android_scrollbarSize:I = 0x0

.field public static final FrameListView_android_scrollbarStyle:I = 0x7

.field public static final FrameListView_android_scrollbarThumbHorizontal:I = 0x1

.field public static final FrameListView_android_scrollbarThumbVertical:I = 0x2

.field public static final FrameListView_android_scrollbarTrackHorizontal:I = 0x3

.field public static final FrameListView_android_scrollbarTrackVertical:I = 0x4

.field public static final FrameListView_android_scrollbars:I = 0x9

.field public static final FrameListView_android_spacing:I = 0xc

.field public static final StrokeTextView:[I

.field public static final StrokeTextView_strokeColor:I = 0x2

.field public static final StrokeTextView_strokeJoinStyle:I = 0x3

.field public static final StrokeTextView_strokeMiter:I = 0x1

.field public static final StrokeTextView_strokeWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6610
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/arcsoft/magicshotstudio/R$styleable;->CircleProgressBar:[I

    .line 6736
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/arcsoft/magicshotstudio/R$styleable;->FrameListView:[I

    .line 6854
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/arcsoft/magicshotstudio/R$styleable;->StrokeTextView:[I

    return-void

    .line 6610
    :array_0
    .array-data 4
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
    .end array-data

    .line 6736
    :array_1
    .array-data 4
        0x1010063
        0x1010064
        0x1010065
        0x1010066
        0x1010067
        0x1010068
        0x1010069
        0x101007f
        0x10100c4
        0x10100de
        0x10100fb
        0x10100fc
        0x1010113
        0x101012b
        0x10102a8
        0x10102a9
    .end array-data

    .line 6854
    :array_2
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
