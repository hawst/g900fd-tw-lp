.class Lcom/arcsoft/magicshotstudio/Eraser$1;
.super Ljava/lang/Object;
.source "Eraser.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/picclear/zoom/ArcTouchListener$MySingleTapUpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Eraser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doubleTapUp(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 147
    return-void
.end method

.method public singleTapUp(II)V
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 128
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mOperateEnable:Z
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$000(Lcom/arcsoft/magicshotstudio/Eraser;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->hidePopupToast()V
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$100(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 133
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 135
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mImageView:Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/Eraser;->access$200(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;

    move-result-object v1

    int-to-float v2, p1

    int-to-float v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/arcsoft/magicshotstudio/ui/picclear/widgets/ImageZoomView;->getRectChoosed(FF)I

    move-result v0

    .line 136
    .local v0, "rectChoose":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->maskRectTap(I)V
    invoke-static {v1, v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$300(Lcom/arcsoft/magicshotstudio/Eraser;I)V

    .line 140
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Eraser$1;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    goto :goto_0
.end method
