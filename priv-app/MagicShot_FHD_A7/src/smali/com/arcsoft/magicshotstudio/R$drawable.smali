.class public final Lcom/arcsoft/magicshotstudio/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_bg:I = 0x7f020000

.field public static final action_bar_divider:I = 0x7f020001

.field public static final action_bar_icon_back:I = 0x7f020002

.field public static final action_bar_icon_back_dim:I = 0x7f020003

.field public static final action_bar_icon_back_focus:I = 0x7f020004

.field public static final action_bar_icon_back_press:I = 0x7f020005

.field public static final action_bar_icon_best:I = 0x7f020006

.field public static final action_bar_icon_best_dim:I = 0x7f020007

.field public static final action_bar_icon_best_focus:I = 0x7f020008

.field public static final action_bar_icon_best_on:I = 0x7f020009

.field public static final action_bar_icon_best_press:I = 0x7f02000a

.field public static final action_bar_icon_cancel:I = 0x7f02000b

.field public static final action_bar_icon_cancel_dim:I = 0x7f02000c

.field public static final action_bar_icon_cancel_focus:I = 0x7f02000d

.field public static final action_bar_icon_cancel_press:I = 0x7f02000e

.field public static final action_bar_icon_save:I = 0x7f02000f

.field public static final action_bar_icon_save_dim:I = 0x7f020010

.field public static final action_bar_icon_save_focus:I = 0x7f020011

.field public static final action_bar_icon_save_press:I = 0x7f020012

.field public static final action_bar_icon_share:I = 0x7f020013

.field public static final action_bar_icon_share_dim:I = 0x7f020014

.field public static final action_bar_icon_share_focus:I = 0x7f020015

.field public static final action_bar_icon_share_press:I = 0x7f020016

.field public static final app_icon:I = 0x7f020017

.field public static final best_face_normal:I = 0x7f020018

.field public static final best_face_selected:I = 0x7f020019

.field public static final best_photo_besticon:I = 0x7f02001a

.field public static final bestface_back_button:I = 0x7f02001b

.field public static final bestface_cancel_button:I = 0x7f02001c

.field public static final bestface_face_background:I = 0x7f02001d

.field public static final bestface_horizontal_progress:I = 0x7f02001e

.field public static final bestface_save_button:I = 0x7f02001f

.field public static final black:I = 0x7f0200e9

.field public static final bottom_button_bg:I = 0x7f020020

.field public static final btn_focus:I = 0x7f020021

.field public static final btn_press:I = 0x7f020022

.field public static final camera_beauty_face_af:I = 0x7f020023

.field public static final camera_best_icon:I = 0x7f020024

.field public static final camera_best_photo_frame:I = 0x7f020025

.field public static final camera_best_photo_frame_focus:I = 0x7f020026

.field public static final camera_btn_close:I = 0x7f020027

.field public static final camera_btn_close_press:I = 0x7f020028

.field public static final camera_btn_open:I = 0x7f020029

.field public static final camera_btn_open_press:I = 0x7f02002a

.field public static final camera_capturing_bg:I = 0x7f02002b

.field public static final camera_close:I = 0x7f02002c

.field public static final camera_close_press:I = 0x7f02002d

.field public static final camera_drama_shot_edit:I = 0x7f02002e

.field public static final camera_drama_shot_edit_dim:I = 0x7f02002f

.field public static final camera_drama_shot_edit_focus:I = 0x7f020030

.field public static final camera_drama_shot_edit_press:I = 0x7f020031

.field public static final camera_drama_shot_redo:I = 0x7f020032

.field public static final camera_drama_shot_redo_dim:I = 0x7f020033

.field public static final camera_drama_shot_redo_focus:I = 0x7f020034

.field public static final camera_drama_shot_redo_press:I = 0x7f020035

.field public static final camera_drama_shot_redo_selected:I = 0x7f020036

.field public static final camera_drama_shot_remove:I = 0x7f020037

.field public static final camera_drama_shot_remove_dim:I = 0x7f020038

.field public static final camera_drama_shot_remove_focus:I = 0x7f020039

.field public static final camera_drama_shot_remove_press:I = 0x7f02003a

.field public static final camera_drama_shot_remove_selected:I = 0x7f02003b

.field public static final camera_drama_shot_restore:I = 0x7f02003c

.field public static final camera_drama_shot_restore_dim:I = 0x7f02003d

.field public static final camera_drama_shot_restore_focus:I = 0x7f02003e

.field public static final camera_drama_shot_restore_press:I = 0x7f02003f

.field public static final camera_drama_shot_restore_selected:I = 0x7f020040

.field public static final camera_drama_shot_undo:I = 0x7f020041

.field public static final camera_drama_shot_undo_dim:I = 0x7f020042

.field public static final camera_drama_shot_undo_focus:I = 0x7f020043

.field public static final camera_drama_shot_undo_press:I = 0x7f020044

.field public static final camera_drama_shot_undo_selected:I = 0x7f020045

.field public static final camera_moving_object:I = 0x7f020046

.field public static final camera_moving_object_dim:I = 0x7f020047

.field public static final camera_moving_object_press:I = 0x7f020048

.field public static final camera_quick_settings_line:I = 0x7f020049

.field public static final common_checkbox_style:I = 0x7f02004a

.field public static final common_headbar_back_button:I = 0x7f02004b

.field public static final common_headbar_background:I = 0x7f02004c

.field public static final common_headbar_cancel_button:I = 0x7f02004d

.field public static final common_headbar_save_button:I = 0x7f02004e

.field public static final drama_back_button:I = 0x7f02004f

.field public static final drama_bg_dialog:I = 0x7f020050

.field public static final drama_brush_button:I = 0x7f020051

.field public static final drama_checkbox_checked_drawable_l:I = 0x7f020052

.field public static final drama_checkbox_checked_drawable_p:I = 0x7f020053

.field public static final drama_checkbox_selector_l:I = 0x7f020054

.field public static final drama_checkbox_selector_p:I = 0x7f020055

.field public static final drama_close_style:I = 0x7f020056

.field public static final drama_done_button:I = 0x7f020057

.field public static final drama_edit_button:I = 0x7f020058

.field public static final drama_list_focus_select_bg:I = 0x7f020059

.field public static final drama_list_selector:I = 0x7f02005a

.field public static final drama_list_selector_background_transition:I = 0x7f02005b

.field public static final drama_open_style:I = 0x7f02005c

.field public static final drama_redo_button:I = 0x7f02005d

.field public static final drama_thumb_stroke:I = 0x7f02005e

.field public static final drama_undo_button:I = 0x7f02005f

.field public static final edit_bar_background:I = 0x7f020060

.field public static final edit_bar_divider:I = 0x7f020061

.field public static final edit_popup_bg:I = 0x7f020062

.field public static final edit_popup_focus:I = 0x7f020063

.field public static final edit_popup_press:I = 0x7f020064

.field public static final edit_popup_select_check:I = 0x7f020065

.field public static final edit_popup_selected:I = 0x7f020066

.field public static final eraser_button:I = 0x7f020067

.field public static final eraser_horizontal_progress:I = 0x7f020068

.field public static final eraser_showmovingicon:I = 0x7f020069

.field public static final exitdialog_button_bg:I = 0x7f02006a

.field public static final help_button_bg:I = 0x7f02006b

.field public static final help_cancel_btn_focus:I = 0x7f02006c

.field public static final help_cancel_btn_nor:I = 0x7f02006d

.field public static final help_cancel_btn_press:I = 0x7f02006e

.field public static final help_guide_best_face:I = 0x7f02006f

.field public static final help_guide_best_face_l:I = 0x7f020070

.field public static final help_guide_best_photo:I = 0x7f020071

.field public static final help_guide_best_photo_l:I = 0x7f020072

.field public static final help_guide_drama:I = 0x7f020073

.field public static final help_guide_drama_l:I = 0x7f020074

.field public static final help_guide_eraser:I = 0x7f020075

.field public static final help_guide_eraser_l:I = 0x7f020076

.field public static final help_guide_picmotion:I = 0x7f020077

.field public static final help_guide_picmotion_l:I = 0x7f020078

.field public static final icon_best_face:I = 0x7f020079

.field public static final icon_best_face_dim:I = 0x7f02007a

.field public static final icon_best_face_focus:I = 0x7f02007b

.field public static final icon_best_face_press:I = 0x7f02007c

.field public static final icon_best_face_selected:I = 0x7f02007d

.field public static final icon_best_photo:I = 0x7f02007e

.field public static final icon_best_photo_dim:I = 0x7f02007f

.field public static final icon_best_photo_focus:I = 0x7f020080

.field public static final icon_best_photo_press:I = 0x7f020081

.field public static final icon_best_photo_selected:I = 0x7f020082

.field public static final icon_drama_shot:I = 0x7f020083

.field public static final icon_drama_shot_dim:I = 0x7f020084

.field public static final icon_drama_shot_focus:I = 0x7f020085

.field public static final icon_drama_shot_press:I = 0x7f020086

.field public static final icon_drama_shot_selected:I = 0x7f020087

.field public static final icon_eraser:I = 0x7f020088

.field public static final icon_eraser_dim:I = 0x7f020089

.field public static final icon_eraser_focus:I = 0x7f02008a

.field public static final icon_eraser_press:I = 0x7f02008b

.field public static final icon_eraser_selected:I = 0x7f02008c

.field public static final icon_object:I = 0x7f02008d

.field public static final icon_object_dim:I = 0x7f02008e

.field public static final icon_object_focus:I = 0x7f02008f

.field public static final icon_object_press:I = 0x7f020090

.field public static final icon_object_selected:I = 0x7f020091

.field public static final icon_pic_motion:I = 0x7f020092

.field public static final icon_pic_motion_dim:I = 0x7f020093

.field public static final icon_pic_motion_focus:I = 0x7f020094

.field public static final icon_pic_motion_press:I = 0x7f020095

.field public static final icon_pic_motion_selected:I = 0x7f020096

.field public static final image_focus_bg_focus:I = 0x7f020097

.field public static final list_disabled:I = 0x7f020098

.field public static final list_focused:I = 0x7f020099

.field public static final list_longpressed:I = 0x7f02009a

.field public static final list_pressed:I = 0x7f02009b

.field public static final magic_drama_shot_thumb_bg:I = 0x7f02009c

.field public static final magic_photo_focus_01:I = 0x7f02009d

.field public static final magic_photo_focus_02:I = 0x7f02009e

.field public static final magic_shot_best_face:I = 0x7f02009f

.field public static final magic_shot_best_face_focus:I = 0x7f0200a0

.field public static final magic_shot_bg:I = 0x7f0200a1

.field public static final magic_shot_eraser_minus:I = 0x7f0200a2

.field public static final magic_shot_eraser_plus:I = 0x7f0200a3

.field public static final magicshot_icon:I = 0x7f0200a4

.field public static final main_best_face_selector:I = 0x7f0200a5

.field public static final main_best_photo_selector:I = 0x7f0200a6

.field public static final main_drama_selector:I = 0x7f0200a7

.field public static final main_eraser_selector:I = 0x7f0200a8

.field public static final main_picmotion_selector:I = 0x7f0200a9

.field public static final overlay_help_button:I = 0x7f0200aa

.field public static final overlay_help_button_focus:I = 0x7f0200ab

.field public static final overlay_help_button_press:I = 0x7f0200ac

.field public static final overscroll_edge:I = 0x7f0200ad

.field public static final overscroll_edge_bottom:I = 0x7f0200ae

.field public static final overscroll_edge_left:I = 0x7f0200af

.field public static final overscroll_edge_right:I = 0x7f0200b0

.field public static final overscroll_edge_top:I = 0x7f0200b1

.field public static final overscroll_glow:I = 0x7f0200b2

.field public static final overscroll_glow_bottom:I = 0x7f0200b3

.field public static final overscroll_glow_left:I = 0x7f0200b4

.field public static final overscroll_glow_right:I = 0x7f0200b5

.field public static final overscroll_glow_top:I = 0x7f0200b6

.field public static final photo_btn_divider:I = 0x7f0200b7

.field public static final photo_gauge_bg:I = 0x7f0200b8

.field public static final photo_gauge_masking:I = 0x7f0200b9

.field public static final photo_gauge_progress:I = 0x7f0200ba

.field public static final photo_page_best_photo_focus_ic:I = 0x7f0200bb

.field public static final photo_page_best_photo_ic:I = 0x7f0200bc

.field public static final photo_page_navicator_focus:I = 0x7f0200bd

.field public static final photo_page_navicator_nor:I = 0x7f0200be

.field public static final pic_motion_angle:I = 0x7f0200bf

.field public static final pic_motion_blur:I = 0x7f0200c0

.field public static final pic_motion_blur_handler:I = 0x7f0200c1

.field public static final pic_motion_blur_handler_press:I = 0x7f0200c2

.field public static final picmotion_motion_blur:I = 0x7f0200c3

.field public static final picmotion_motion_blur_angle:I = 0x7f0200c4

.field public static final picmotion_object:I = 0x7f0200c5

.field public static final picmotion_progress_bar:I = 0x7f0200c6

.field public static final ripple:I = 0x7f0200c7

.field public static final share_via:I = 0x7f0200c8

.field public static final tw_btn_check_off_disabled_focused_holo_light:I = 0x7f0200c9

.field public static final tw_btn_check_off_disabled_holo_light:I = 0x7f0200ca

.field public static final tw_btn_check_off_focused_holo_light:I = 0x7f0200cb

.field public static final tw_btn_check_off_holo_light:I = 0x7f0200cc

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f0200cd

.field public static final tw_btn_check_on_disabled_focused_holo_light:I = 0x7f0200ce

.field public static final tw_btn_check_on_disabled_holo_light:I = 0x7f0200cf

.field public static final tw_btn_check_on_focused_holo_light:I = 0x7f0200d0

.field public static final tw_btn_check_on_holo_light:I = 0x7f0200d1

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f0200d2

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f0200d3

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f0200d4

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f0200d5

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f0200d6

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f0200d7

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f0200d8

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f0200d9

.field public static final tw_dialog_middle_holo_light:I = 0x7f0200da

.field public static final tw_dialog_top_medium_holo_light:I = 0x7f0200db

.field public static final tw_divider_popup_vertical_holo_dark:I = 0x7f0200dc

.field public static final tw_divider_popup_vertical_holo_light:I = 0x7f0200dd

.field public static final tw_ic_cab_done_holo_dark:I = 0x7f0200de

.field public static final tw_ic_cab_done_holo_dark_dim:I = 0x7f0200df

.field public static final tw_ic_cab_done_holo_dark_focus:I = 0x7f0200e0

.field public static final tw_ic_cab_done_holo_dark_press:I = 0x7f0200e1

.field public static final tw_progress_bg_holo_dark:I = 0x7f0200e2

.field public static final tw_progress_primary_holo_dark:I = 0x7f0200e3

.field public static final tw_progress_secondary_holo_dark:I = 0x7f0200e4

.field public static final tw_scrollbar_holo_dark:I = 0x7f0200e5

.field public static final tw_scrollbar_holo_light:I = 0x7f0200e6

.field public static final tw_toast_frame_holo_light:I = 0x7f0200e7

.field public static final white:I = 0x7f0200e8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
