.class Lcom/arcsoft/magicshotstudio/Eraser$5;
.super Ljava/lang/Object;
.source "Eraser.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/ui/common/DialogCallBack$OnCallBackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/Eraser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/Eraser;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/Eraser;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    const/4 v1, 0x0

    .line 705
    packed-switch p1, :pswitch_data_0

    .line 729
    :goto_0
    return-void

    .line 707
    :pswitch_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3000(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3100(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 710
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3200(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    goto :goto_0

    .line 713
    :pswitch_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3300(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 714
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3400(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 716
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 717
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->setActivityResult()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$1900(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 718
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->finish()V

    goto :goto_0

    .line 721
    :pswitch_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3500(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 722
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # getter for: Lcom/arcsoft/magicshotstudio/Eraser;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3600(Lcom/arcsoft/magicshotstudio/Eraser;)Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->dismiss()V

    .line 724
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/Eraser;->setOperateEnable(Z)V

    .line 725
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    # invokes: Lcom/arcsoft/magicshotstudio/Eraser;->saveResultFile()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->access$3700(Lcom/arcsoft/magicshotstudio/Eraser;)V

    .line 726
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Eraser$5;->this$0:Lcom/arcsoft/magicshotstudio/Eraser;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/Eraser;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 705
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
