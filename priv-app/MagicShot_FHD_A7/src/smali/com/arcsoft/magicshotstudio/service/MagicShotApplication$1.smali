.class Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$1;
.super Ljava/lang/Object;
.source "MagicShotApplication.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$1;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 44
    const-string v0, "MagicShotApplication"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->stopMySelf()V

    .line 46
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 47
    return-void
.end method
