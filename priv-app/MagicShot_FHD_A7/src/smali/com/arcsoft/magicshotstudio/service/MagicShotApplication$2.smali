.class Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;
.super Ljava/lang/Object;
.source "MagicShotApplication.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # setter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$102(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;Landroid/app/Activity;)Landroid/app/Activity;

    .line 77
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$200(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)I

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    # setter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$202(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;I)I

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # invokes: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->addActivity(Landroid/app/Activity;)V
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$400(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;Landroid/app/Activity;)V

    .line 89
    const-string v0, "MagicShotApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MagicShotApplication--->onActivityCreated-->Name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$200(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)I

    move-result v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # invokes: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->closeActivities()V
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    # setter for: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I
    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$202(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;I)I

    goto :goto_0
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    # invokes: Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->removeActivity(Landroid/app/Activity;)V
    invoke-static {v0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->access$000(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;Landroid/app/Activity;)V

    .line 70
    const-string v0, "MagicShotApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MagicShotApplication--->onActivityDestroyed-->Name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 65
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 62
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 56
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 53
    return-void
.end method
