.class public Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
.super Landroid/app/Application;
.source "MagicShotApplication.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MagicShotApplication"

.field private static mIsTalkBackEnabled:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

.field private mActivityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private mActivityList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrent:Landroid/app/Activity;

.field private mTaskId:I

.field private mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-boolean v0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mIsTalkBackEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 22
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 41
    new-instance v0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$1;-><init>(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 50
    new-instance v0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$2;-><init>(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 207
    new-instance v0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$3;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication$3;-><init>(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->removeActivity(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$102(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;Landroid/app/Activity;)Landroid/app/Activity;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;

    return-object p1
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    .prologue
    .line 19
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I

    return v0
.end method

.method static synthetic access$202(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;I)I
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mTaskId:I

    return p1
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->closeActivities()V

    return-void
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->addActivity(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$502(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 19
    sput-boolean p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mIsTalkBackEnabled:Z

    return p0
.end method

.method private addActivity(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v0, "MagicShotApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MagicShotApplication--->addActivity Name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method private closeActivities()V
    .locals 6

    .prologue
    .line 136
    const-class v4, Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    .line 137
    .local v3, "mainActivity":Ljava/lang/String;
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 138
    .local v0, "activity":Landroid/app/Activity;
    const-string v4, "MagicShotApplication"

    const-string v5, "closeActivities (A)"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "activityName":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    const-string v4, "MagicShotApplication"

    const-string v5, "closeActivities (B)"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v0

    .line 142
    check-cast v4, Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/Main;->releaseAndExitService()V

    .line 143
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 145
    :cond_0
    const-string v4, "MagicShotApplication"

    const-string v5, "closeActivities (C)"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v0

    .line 146
    check-cast v4, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->releaseAll()V

    move-object v4, v0

    .line 147
    check-cast v4, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    invoke-virtual {v4}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->exitService()V

    .line 148
    check-cast v0, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->superFinish()V

    goto :goto_0

    .line 152
    .end local v1    # "activityName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private containsActivity(Landroid/app/Activity;)Z
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 107
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "activityName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 109
    .local v2, "isContained":Z
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 110
    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 111
    .local v3, "itemActivity":Landroid/app/Activity;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 112
    .local v4, "itemActivityName":Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 113
    const/4 v2, 0x1

    .line 118
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "itemActivity":Landroid/app/Activity;
    .end local v4    # "itemActivityName":Ljava/lang/String;
    :cond_1
    return v2
.end method

.method private finishPreviousActivities()V
    .locals 14

    .prologue
    .line 155
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 156
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (A)"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v6

    .line 161
    .local v6, "instance":Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    if-nez v6, :cond_1

    .line 162
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (B)"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 166
    :cond_1
    const-class v11, Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v11}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    .line 167
    .local v9, "mainActivity":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v1

    .line 168
    .local v1, "bStartFromGallery":Z
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mCurrent:Landroid/app/Activity;

    invoke-virtual {v11}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 169
    .local v2, "curr":Landroid/content/Intent;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery(Landroid/content/Intent;)Z

    move-result v4

    .line 171
    .local v4, "currFromGallery":Z
    if-eqz v1, :cond_3

    .line 172
    const-string v11, "MagicShotApplication"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "finishPreviousActivities currFromGallery = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v10

    .line 174
    .local v10, "nSize":I
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    add-int/lit8 v12, v10, -0x1

    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    .line 176
    .local v8, "lastActivity":Landroid/app/Activity;
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    .line 177
    .local v7, "lastAct":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 178
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (C)  Activity"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 181
    :cond_2
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->closeActivities()V

    .line 182
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (D)"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 186
    .end local v7    # "lastAct":Ljava/lang/String;
    .end local v8    # "lastActivity":Landroid/app/Activity;
    .end local v10    # "nSize":I
    :cond_3
    iget-object v11, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 187
    .local v0, "activity":Landroid/app/Activity;
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (E) "

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    .line 189
    .restart local v7    # "lastAct":Ljava/lang/String;
    if-nez v4, :cond_4

    const/4 v3, 0x1

    .line 190
    .local v3, "currFromCamera":Z
    :goto_2
    if-eqz v3, :cond_5

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 191
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (F) "

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 189
    .end local v3    # "currFromCamera":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 193
    .restart local v3    # "currFromCamera":Z
    :cond_5
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 194
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (G)"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v11, v0

    .line 195
    check-cast v11, Lcom/arcsoft/magicshotstudio/Main;

    invoke-virtual {v11}, Lcom/arcsoft/magicshotstudio/Main;->releaseAndExitService()V

    .line 196
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 198
    :cond_6
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (H)"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 202
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v3    # "currFromCamera":Z
    .end local v7    # "lastAct":Ljava/lang/String;
    :cond_7
    const-string v11, "MagicShotApplication"

    const-string v12, "finishPreviousActivities (J)"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static isTalkBackOn()Z
    .locals 1

    .prologue
    .line 216
    sget-boolean v0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mIsTalkBackEnabled:Z

    return v0
.end method

.method private removeActivity(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->containsActivity(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public containsMainActivity()Z
    .locals 5

    .prologue
    .line 122
    const/4 v1, 0x0

    .line 123
    .local v1, "isContained":Z
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 124
    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 125
    .local v2, "itemActivity":Landroid/app/Activity;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "itemActivityName":Ljava/lang/String;
    const-string v4, "Main"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 127
    const/4 v1, 0x1

    .line 132
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "itemActivity":Landroid/app/Activity;
    .end local v3    # "itemActivityName":Ljava/lang/String;
    :cond_1
    return v1
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 30
    const-string v0, "MagicShotApplication"

    const-string v1, "MagicShotApplication--->onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mActivityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mUncaughtExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 33
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 34
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    sput-boolean v0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mIsTalkBackEnabled:Z

    .line 36
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotApplication;->mAccessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 38
    :cond_0
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 39
    return-void
.end method
