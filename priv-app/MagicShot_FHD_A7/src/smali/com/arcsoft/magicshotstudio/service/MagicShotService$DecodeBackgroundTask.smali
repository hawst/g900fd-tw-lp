.class Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;
.super Landroid/os/AsyncTask;
.source "MagicShotService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/service/MagicShotService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DecodeBackgroundTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 21
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 264
    const/16 v18, 0x0

    .line 265
    .local v18, "ret":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$000(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/arcsoft/sefmanager/SEFManager;->nativeInit(Ljava/lang/String;)I

    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # invokes: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSefData()Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$200(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z

    move-result v20

    .line 267
    .local v20, "success":Z
    if-eqz v20, :cond_6

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    move-result-object v2

    iget-object v2, v2, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;->mBuffs:[Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    array-length v0, v2

    move/from16 v17, v0

    .line 269
    .local v17, "nSEFCount":I
    add-int/lit8 v16, v17, -0x1

    .line 276
    .local v16, "nPreDataPosition":I
    new-instance v19, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;

    invoke-direct/range {v19 .. v19}, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;-><init>()V

    .line 278
    .local v19, "selectInfo":Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$400(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-object/from16 v0, v19

    # invokes: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)V
    invoke-static {v2, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$500(Lcom/arcsoft/magicshotstudio/service/MagicShotService;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)V

    .line 283
    :cond_0
    move/from16 v0, v16

    new-array v11, v0, [Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;

    .line 285
    .local v11, "buffs":[Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$400(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$600(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v14, 0x1

    .line 286
    .local v14, "isCameraEntryMode":Z
    :goto_0
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_3

    .line 287
    new-instance v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;

    invoke-direct {v2}, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;-><init>()V

    aput-object v2, v11, v12

    .line 288
    aget-object v2, v11, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    move-result-object v4

    iget-object v4, v4, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;->mBuffs:[Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    aget-object v4, v4, v12

    iget v4, v4, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nativeBuff:I

    iput v4, v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;->nativeBuff:I

    .line 289
    aget-object v2, v11, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    move-result-object v4

    iget-object v4, v4, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;->mBuffs:[Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    aget-object v4, v4, v12

    iget v4, v4, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nLength:I

    iput v4, v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;->nLength:I

    .line 290
    aget-object v2, v11, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    move-result-object v4

    iget-object v4, v4, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;->mBuffs:[Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    aget-object v4, v4, v12

    iget v4, v4, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nSEFType:I

    iput v4, v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;->nSEFType:I

    .line 291
    if-eqz v14, :cond_2

    .line 293
    aget-object v2, v11, v12

    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;->bNeedDecode:Z

    .line 286
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 285
    .end local v12    # "i":I
    .end local v14    # "isCameraEntryMode":Z
    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    .line 297
    .restart local v12    # "i":I
    .restart local v14    # "isCameraEntryMode":Z
    :cond_2
    aget-object v2, v11, v12

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;->bNeedDecode:Z

    goto :goto_2

    .line 301
    :cond_3
    if-eqz v14, :cond_5

    .line 303
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_3
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mSelectIndex:[I

    array-length v2, v2

    if-ge v15, v2, :cond_5

    .line 305
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mSelectIndex:[I

    aget v13, v2, v15

    .line 306
    .local v13, "index":I
    array-length v2, v11

    if-ge v13, v2, :cond_4

    .line 307
    aget-object v2, v11, v13

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;->bNeedDecode:Z

    .line 303
    :cond_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 312
    .end local v13    # "index":I
    .end local v15    # "j":I
    :cond_5
    new-instance v3, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;

    invoke-direct {v3, v11}, Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;-><init>([Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;)V

    .line 314
    .local v3, "mSEFArray":Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1000(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/common/readengine/ReadEngine;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbNeedDecode:Z
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$700(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z

    move-result v4

    const/4 v5, 0x4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageWidth:I
    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$800(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageHeight:I
    invoke-static {v7}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$900(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I

    move-result v7

    const-wide/32 v8, 0x70000002

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Lcom/arcsoft/common/readengine/ReadEngine;->initEngineFromSEF(Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;ZIIIJZ)I

    move-result v18

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;
    invoke-static {v4}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1000(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/common/readengine/ReadEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/arcsoft/common/readengine/ReadEngine;->readToMemory()J

    move-result-wide v4

    # setter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J
    invoke-static {v2, v4, v5}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1102(Lcom/arcsoft/magicshotstudio/service/MagicShotService;J)J

    .line 319
    const-string v2, "MagicShotService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mBitmapAddress: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J
    invoke-static {v5}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1100(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    .end local v3    # "mSEFArray":Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;
    .end local v11    # "buffs":[Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;
    .end local v12    # "i":I
    .end local v14    # "isCameraEntryMode":Z
    .end local v16    # "nPreDataPosition":I
    .end local v17    # "nSEFCount":I
    .end local v19    # "selectInfo":Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/arcsoft/sefmanager/SEFManager;->nativeUninit()I

    .line 326
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2

    .line 321
    :cond_6
    const-string v2, "MagicShotService"

    const-string v4, "getSefData failed"

    invoke-static {v2, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const/16 v18, -0x1

    goto :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 261
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 341
    const-string v1, "MagicShotService"

    const-string v2, "MagicShotService onPostExecute"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # invokes: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->checkEnable()I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I

    move-result v0

    .line 344
    .local v0, "res":I
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 345
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 346
    const v1, 0x10008

    if-ne v0, v1, :cond_0

    .line 347
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 351
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 352
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 261
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 331
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onPreExecute"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 333
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 334
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1000(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/common/readengine/ReadEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    iget-object v1, v1, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/common/readengine/ReadEngine;->setNotifyListener(Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;)V

    .line 337
    return-void
.end method
