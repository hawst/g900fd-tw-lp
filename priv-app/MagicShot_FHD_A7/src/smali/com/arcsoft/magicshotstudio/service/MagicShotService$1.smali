.class Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;
.super Ljava/lang/Object;
.source "MagicShotService.java"

# interfaces
.implements Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/magicshotstudio/service/MagicShotService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFirstPicture(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "firstBitmap"    # Landroid/graphics/Bitmap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 413
    const-string v1, "MagicShotService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFirstPicture: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1400(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 415
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;->this$0:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mExifOrientation:I
    invoke-static {v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1500(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->rotateBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    # setter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1402(Lcom/arcsoft/magicshotstudio/service/MagicShotService;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 417
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 418
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x13

    iput v1, v0, Landroid/os/Message;->what:I

    .line 419
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 420
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 422
    :cond_0
    return-void
.end method

.method public onNotifyProgress(I)V
    .locals 4
    .param p1, "progress"    # I

    .prologue
    .line 401
    const-string v1, "MagicShotService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNotifyProgress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 403
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x12

    iput v1, v0, Landroid/os/Message;->what:I

    .line 404
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 405
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 406
    # getter for: Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->access$1200()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 408
    :cond_0
    return-void
.end method
