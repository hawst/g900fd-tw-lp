.class public Lcom/arcsoft/magicshotstudio/service/MagicShotService;
.super Landroid/app/Service;
.source "MagicShotService.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;,
        Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;
    }
.end annotation


# static fields
.field private static PRIVATE_FOLDER:Ljava/lang/String; = null

.field private static SAVE_DIR:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "MagicShotService"

.field private static mHandler:Landroid/os/Handler;

.field private static mIsFromPrivate:Z

.field private static mSingleInstance:Lcom/arcsoft/magicshotstudio/service/MagicShotService;


# instance fields
.field private mBestFaceSavePath:Ljava/lang/String;

.field private mBestPhotoSavePath:Ljava/lang/String;

.field private mBinder:Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;

.field private mBitmapAddress:J

.field private mBitmapCount:I

.field private mDramaSavePath:Ljava/lang/String;

.field private mEraserSavePath:Ljava/lang/String;

.field private mExifOrientation:I

.field private mFirstBitmap:Landroid/graphics/Bitmap;

.field private mFromGallery:Z

.field private mHasBind:Z

.field private mImageHeight:I

.field private mImageWidth:I

.field private mIsSEF:Z

.field mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

.field private mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

.field private mModeName:Ljava/lang/String;

.field private mNativeExifJPGBuff:J

.field private mNativeExifJPGBuffSize:J

.field private mNeedStartMain:Z

.field private mPicMotionSavePath:Ljava/lang/String;

.field private mPreDataAddress:J

.field private mPreDataSize:I

.field private mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

.field private mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;

.field private mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

.field private mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

.field private mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

.field private mSefFilePath:Ljava/lang/String;

.field private mStartId:I

.field private mbIsSupportMode:Z

.field private mbNeedDecode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39
    sput-object v3, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;

    .line 71
    const/4 v2, 0x0

    sput-boolean v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsFromPrivate:Z

    .line 72
    sput-object v3, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSingleInstance:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 74
    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SSTUDIO:Ljava/lang/String;

    sput-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    .line 75
    const-string v2, "/storage/Private/"

    sput-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->PRIVATE_FOLDER:Ljava/lang/String;

    .line 568
    :try_start_0
    const-string v2, "arcsoft_magicshot_common_T"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 569
    const-string v2, "arcsoft_readengine_T"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 570
    const-string v2, "SEF"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 571
    const-string v2, "SEFManager_jni_T"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 573
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SSTUDIO:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 574
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 575
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 585
    :catch_0
    move-exception v0

    .line 586
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 36
    new-instance v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;-><init>(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBinder:Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;

    .line 37
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z

    .line 38
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNeedStartMain:Z

    .line 44
    iput v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapCount:I

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbNeedDecode:Z

    .line 48
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mDramaSavePath:Ljava/lang/String;

    .line 49
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPicMotionSavePath:Ljava/lang/String;

    .line 50
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mEraserSavePath:Ljava/lang/String;

    .line 51
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBestPhotoSavePath:Ljava/lang/String;

    .line 52
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBestFaceSavePath:Ljava/lang/String;

    .line 54
    new-instance v0, Lcom/arcsoft/common/readengine/ReadEngine;

    invoke-direct {v0}, Lcom/arcsoft/common/readengine/ReadEngine;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;

    .line 55
    new-instance v0, Lcom/arcsoft/sefmanager/SEFManager;

    invoke-direct {v0}, Lcom/arcsoft/sefmanager/SEFManager;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

    .line 56
    new-instance v0, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    invoke-direct {v0}, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    .line 57
    new-instance v0, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-direct {v0, v1, v1, v1}, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;-><init>(III)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    .line 58
    new-instance v0, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-direct {v0, v1, v1, v1}, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;-><init>(III)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    .line 59
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsSEF:Z

    .line 60
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreDataAddress:J

    .line 61
    iput v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreDataSize:I

    .line 62
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J

    .line 63
    iput v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mExifOrientation:I

    .line 65
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNativeExifJPGBuff:J

    .line 66
    iput-wide v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNativeExifJPGBuffSize:J

    .line 68
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHasBind:Z

    .line 70
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 73
    iput v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mStartId:I

    .line 377
    new-instance v0, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    invoke-direct {v0}, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    .line 397
    iput-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;

    .line 398
    new-instance v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$1;-><init>(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    .line 552
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/common/readengine/ReadEngine;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J

    return-wide v0
.end method

.method static synthetic access$1102(Lcom/arcsoft/magicshotstudio/service/MagicShotService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .param p1, "x1"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J

    return-wide p1
.end method

.method static synthetic access$1200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->checkEnable()I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/arcsoft/magicshotstudio/service/MagicShotService;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mExifOrientation:I

    return v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSefData()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z

    return v0
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/service/MagicShotService;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .param p1, "x1"    # Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)V

    return-void
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    return v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbNeedDecode:Z

    return v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageWidth:I

    return v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .prologue
    .line 33
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageHeight:I

    return v0
.end method

.method private checkEnable()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 380
    const/4 v0, 0x0

    .line 381
    .local v0, "enableResult":I
    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsSEF:Z

    if-eqz v1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    invoke-static {v1, v2}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetEnableInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;)I

    move-result v0

    .line 390
    :goto_0
    return v0

    .line 384
    :cond_0
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v2, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestPhotoEnable:Z

    .line 385
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v2, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mBestFaceEnable:Z

    .line 386
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v2, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mEraserEnable:Z

    .line 387
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v2, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mDramaEnable:Z

    .line 388
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    iput-boolean v2, v1, Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;->mMotionEnable:Z

    goto :goto_0
.end method

.method private getIntentInfo(Landroid/content/Intent;)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 170
    const-string v6, "sef_file_name"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    .line 171
    const-string v6, "mode_name"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeName:Ljava/lang/String;

    .line 172
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 173
    .local v1, "index":I
    if-eq v1, v4, :cond_0

    .line 174
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->PRIVATE_FOLDER:Ljava/lang/String;

    .line 176
    :cond_0
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    const-string v7, "/storage/Private/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 177
    sput-boolean v3, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsFromPrivate:Z

    .line 178
    sget-object v6, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->PRIVATE_FOLDER:Ljava/lang/String;

    sput-object v6, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    .line 183
    :goto_0
    const/4 v0, 0x0

    .line 184
    .local v0, "NoFile":Z
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    if-nez v6, :cond_3

    .line 185
    const-string v5, "MagicShotService"

    const-string v6, "SefFilePath not be deliveried by intent"

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_1
    :goto_1
    return v4

    .line 180
    .end local v0    # "NoFile":Z
    :cond_2
    sput-boolean v5, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsFromPrivate:Z

    .line 181
    sget-object v6, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SSTUDIO:Ljava/lang/String;

    sput-object v6, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    goto :goto_0

    .line 188
    .restart local v0    # "NoFile":Z
    :cond_3
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_4

    move v0, v3

    .line 189
    :goto_2
    if-eqz v0, :cond_5

    .line 190
    const-string v5, "MagicShotService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No such file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move v0, v5

    .line 188
    goto :goto_2

    .line 193
    :cond_5
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/arcsoft/sefmanager/SEFManager;->nativeIsSEF(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsSEF:Z

    .line 196
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/arcsoft/magicshotstudio/utils/ImageExif;->getExifOrientation(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mExifOrientation:I

    .line 198
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 199
    .local v2, "opts":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 200
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    invoke-static {v6, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 201
    iget v6, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageWidth:I

    .line 202
    iget v6, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageHeight:I

    .line 203
    const-string v6, "MagicShotService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mImageWidth: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  mImageHeight: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageWidth:I

    iget v7, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageHeight:I

    mul-int/2addr v6, v7

    if-gtz v6, :cond_6

    .line 208
    .local v3, "sizeError":Z
    :goto_3
    if-nez v0, :cond_1

    if-nez v3, :cond_1

    iget-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsSEF:Z

    if-eqz v6, :cond_1

    move v4, v5

    .line 212
    goto/16 :goto_1

    .end local v3    # "sizeError":Z
    :cond_6
    move v3, v5

    .line 206
    goto :goto_3
.end method

.method private getSefData()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 356
    const-string v6, "MagicShotService"

    const-string v7, "getSefData----in----"

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

    invoke-virtual {v6}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSEFDataCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapCount:I

    .line 359
    const-string v6, "MagicShotService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSefData mBitmapCount:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 361
    .local v4, "start":J
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFDatas:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    invoke-virtual {v6, v7}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSEFBuffs(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;)I

    move-result v2

    .line 362
    .local v2, "ret":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 364
    .local v0, "end":J
    const-string v6, "MagicShotService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSefData----out---- cost = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-long v8, v0, v4

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    iget v7, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapCount:I

    iget-object v8, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v6, v7, v8}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSEFBuff(Ljava/lang/String;ILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I

    move-result v6

    or-int/2addr v2, v6

    .line 366
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    iget v6, v6, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nativeBuff:I

    int-to-long v6, v6

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreDataAddress:J

    .line 367
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    iget v6, v6, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nLength:I

    iput v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreDataSize:I

    .line 369
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    iget-object v7, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v6, v3, v7}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSEFBuff(Ljava/lang/String;ILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I

    move-result v6

    or-int/2addr v2, v6

    .line 371
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    iget v6, v6, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nativeBuff:I

    int-to-long v6, v6

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNativeExifJPGBuff:J

    .line 372
    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    iget v6, v6, Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;->nLength:I

    int-to-long v6, v6

    iput-wide v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNativeExifJPGBuffSize:J

    .line 374
    if-nez v2, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method private getSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)V
    .locals 7
    .param p1, "selectInfo"    # Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;

    .prologue
    const/4 v6, 0x1

    .line 216
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 217
    const-string v3, "com.arcsoft.magicshotstudio.BestFace"

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 218
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 219
    const/4 v3, 0x2

    iput v3, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mMode:I

    .line 220
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v3, p1}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)I

    .line 221
    iget-object v3, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mSelectIndex:[I

    array-length v1, v3

    .line 222
    .local v1, "length":I
    const-string v3, "MagicShotService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSelectIndex.length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 224
    const-string v3, "MagicShotService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSelectIndex["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mSelectIndex:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    iget v2, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mTotalSelectNumber:I

    .line 227
    .local v2, "totalSelectNum":I
    const-string v3, "MagicShotService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTotalSelectNumber: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const-string v3, "MagicShotService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTotalSelectIndex.length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mTotalSelectIndex:[I

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mTotalSelectIndex:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 230
    const-string v3, "MagicShotService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTotalSelectIndex["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mTotalSelectIndex:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 232
    :cond_1
    const-string v3, "MagicShotService"

    const-string v4, "Is BESTFACE_MODE"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "totalSelectNum":I
    :cond_2
    const-string v3, "com.arcsoft.magicshotstudio.BestPhoto"

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 235
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 236
    iput v6, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mMode:I

    .line 237
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v3, p1}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)I

    .line 238
    const-string v3, "MagicShotService"

    const-string v4, "Is BESTPHOTO_MODE"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_3
    const-string v3, "com.arcsoft.magicshotstudio.Drama"

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 241
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 242
    const/4 v3, 0x4

    iput v3, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mMode:I

    .line 243
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v3, p1}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)I

    .line 244
    const-string v3, "MagicShotService"

    const-string v4, "Is DRAMA_MODE"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_4
    const-string v3, "com.arcsoft.magicshotstudio.Eraser"

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 248
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 249
    const/4 v3, 0x3

    iput v3, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mMode:I

    .line 250
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v3, p1}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)I

    .line 251
    const-string v3, "MagicShotService"

    const-string v4, "Is ERASER_MODE"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_5
    const-string v3, "com.arcsoft.magicshotstudio.PicMotion"

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 255
    iput-boolean v6, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mbIsSupportMode:Z

    .line 256
    const/4 v3, 0x5

    iput v3, p1, Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;->mMode:I

    .line 257
    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v3, p1}, Lcom/arcsoft/sefmanager/SEFManager;->nativeGetSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)I

    .line 258
    const-string v3, "MagicShotService"

    const-string v4, "Is MOTION_MODE"

    invoke-static {v3, v4}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_6
    return-void
.end method

.method public static getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1

    .prologue
    .line 563
    sget-object v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSingleInstance:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method public static isFromPrivateFolder()Z
    .locals 1

    .prologue
    .line 599
    sget-boolean v0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsFromPrivate:Z

    return v0
.end method

.method public static isStartFromGallery(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 131
    const-string v1, "PackageName"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "com.arcsoft.magicshotstudio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const/4 v1, 0x1

    .line 135
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private judgeStartMethod(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 126
    invoke-static {p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery(Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z

    .line 127
    return-void
.end method

.method private printVersionlog()V
    .locals 7

    .prologue
    .line 86
    const/4 v3, 0x0

    .line 88
    .local v3, "versionName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 89
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.arcsoft.magicshotstudio"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 90
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    const-string v4, "MagicShotService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MagicShot_Version_Num: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, ""

    goto :goto_0
.end method

.method public static setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 559
    sput-object p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;

    .line 560
    return-void
.end method


# virtual methods
.method public getBestFaceSavePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 469
    const-string v1, "yyyyMMdd_kkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 471
    .local v0, "dataFormat":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BestFace_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBestFaceSavePath:Ljava/lang/String;

    .line 472
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBestFaceSavePath:Ljava/lang/String;

    return-object v1
.end method

.method public getBestPhotoSavePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 462
    const-string v1, "yyyyMMdd_kkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "dataFormat":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BestPhoto_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBestPhotoSavePath:Ljava/lang/String;

    .line 465
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBestPhotoSavePath:Ljava/lang/String;

    return-object v1
.end method

.method public getBitmapAddress()J
    .locals 2

    .prologue
    .line 438
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J

    return-wide v0
.end method

.method public getBitmapCount()I
    .locals 1

    .prologue
    .line 442
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapCount:I

    return v0
.end method

.method public getDramaSavePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 476
    const-string v1, "yyyyMMdd_kkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 478
    .local v0, "dataFormat":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Drama_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mDramaSavePath:Ljava/lang/String;

    .line 479
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mDramaSavePath:Ljava/lang/String;

    return-object v1
.end method

.method public getEraserSavePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 490
    const-string v1, "yyyyMMdd_kkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    .local v0, "dataFormat":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Eraser_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mEraserSavePath:Ljava/lang/String;

    .line 493
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mEraserSavePath:Ljava/lang/String;

    return-object v1
.end method

.method public getExitMode()Z
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNeedStartMain:Z

    return v0
.end method

.method public getFirstBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getImageSize()[I
    .locals 3

    .prologue
    .line 497
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageWidth:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mImageHeight:I

    aput v2, v0, v1

    return-object v0
.end method

.method public getMainModeFlags()Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mModeFlags:Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;

    return-object v0
.end method

.method public getNativeExifJPG()J
    .locals 2

    .prologue
    .line 454
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNativeExifJPGBuff:J

    return-wide v0
.end method

.method public getNativeExifJPGSize()J
    .locals 2

    .prologue
    .line 458
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNativeExifJPGBuffSize:J

    return-wide v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mExifOrientation:I

    return v0
.end method

.method public getPicMotionSavePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 483
    const-string v1, "yyyyMMdd_kkmmss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 485
    .local v0, "dataFormat":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Panningshot_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPicMotionSavePath:Ljava/lang/String;

    .line 486
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPicMotionSavePath:Ljava/lang/String;

    return-object v1
.end method

.method public getPreDataAddress()J
    .locals 2

    .prologue
    .line 446
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreDataAddress:J

    return-wide v0
.end method

.method public getPreDataSize()I
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreDataSize:I

    return v0
.end method

.method public getSEFFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSefFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public hasBind()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHasBind:Z

    return v0
.end method

.method public isStartFromGallery()Z
    .locals 1

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 108
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onBind~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->judgeStartMethod(Landroid/content/Intent;)V

    .line 111
    invoke-virtual {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->startDecode(Landroid/content/Intent;)V

    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setBind(Z)V

    .line 113
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBinder:Lcom/arcsoft/magicshotstudio/service/MagicShotService$MagicShotBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onCreate~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->printVersionlog()V

    .line 102
    sput-object p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSingleInstance:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    .line 103
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 104
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 518
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onDestroy~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFromGallery:Z

    .line 520
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 521
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 118
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onRebind~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->judgeStartMethod(Landroid/content/Intent;)V

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setBind(Z)V

    .line 121
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 122
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 511
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onStartCommand~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iput p3, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mStartId:I

    .line 513
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 141
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService onUnbind~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setBind(Z)V

    .line 143
    const/4 v0, 0x1

    return v0
.end method

.method public releaseAll()V
    .locals 2

    .prologue
    .line 524
    const-string v0, "MagicShotService"

    const-string v1, "MagicShotService releaseResource"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mFirstBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/utils/ArcUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 526
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->releaseDecodeData()V

    .line 527
    return-void
.end method

.method public declared-synchronized releaseDecodeData()V
    .locals 2

    .prologue
    .line 530
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mReadEngine:Lcom/arcsoft/common/readengine/ReadEngine;

    invoke-virtual {v0}, Lcom/arcsoft/common/readengine/ReadEngine;->unInitEngine()I

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

    if-eqz v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFMgr:Lcom/arcsoft/sefmanager/SEFManager;

    invoke-virtual {v0}, Lcom/arcsoft/sefmanager/SEFManager;->nativeUninit()I

    .line 538
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mBitmapAddress:J

    .line 539
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    if-eqz v0, :cond_2

    .line 540
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mPreprocessData:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v0}, Lcom/arcsoft/sefmanager/SEFManager;->nativeReleaseSEFBuff(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I

    .line 543
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    if-eqz v0, :cond_3

    .line 544
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mSEFBuffWithExif:Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    invoke-static {v0}, Lcom/arcsoft/sefmanager/SEFManager;->nativeReleaseSEFBuff(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 546
    :cond_3
    monitor-exit p0

    return-void

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBind(Z)V
    .locals 0
    .param p1, "has"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHasBind:Z

    .line 83
    return-void
.end method

.method public setExitMode(Z)V
    .locals 3
    .param p1, "needStartMain"    # Z

    .prologue
    .line 505
    const-string v0, "MagicShotService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setExitMode needStartMain = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mNeedStartMain:Z

    .line 507
    return-void
.end method

.method public startDecode(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getIntentInfo(Landroid/content/Intent;)I

    move-result v1

    .line 149
    .local v1, "result":I
    if-nez v1, :cond_1

    .line 150
    new-instance v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;

    invoke-direct {v2, p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;-><init>(Lcom/arcsoft/magicshotstudio/service/MagicShotService;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/arcsoft/magicshotstudio/service/MagicShotService$DecodeBackgroundTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 153
    const/16 v0, 0x10

    .line 154
    .local v0, "MSG_WHAT":I
    iget-boolean v2, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mIsSEF:Z

    if-nez v2, :cond_2

    .line 155
    const/16 v0, 0x15

    .line 156
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->checkEnable()I

    .line 163
    :cond_2
    sget-object v2, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x32

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 164
    const-string v2, "MagicShotService"

    const-string v3, "Can not get the correct infomation from intent."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopMySelf()V
    .locals 1

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->releaseDecodeData()V

    .line 592
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mStartId:I

    if-eqz v0, :cond_0

    .line 593
    iget v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mStartId:I

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->stopSelf(I)V

    .line 594
    const/4 v0, 0x0

    iput v0, p0, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->mStartId:I

    .line 596
    :cond_0
    return-void
.end method
