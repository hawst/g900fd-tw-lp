.class public Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HomeKeyEventReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;
    }
.end annotation


# static fields
.field public static final SYSTEM_HOME_KEY:Ljava/lang/String; = "homekey"

.field public static final SYSTEM_REASON:Ljava/lang/String; = "reason"

.field public static final SYSTEM_RECENT_APPS:Ljava/lang/String; = "recentapps"

.field public static final tag:Ljava/lang/String;


# instance fields
.field mHomeListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->mHomeListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->mHomeListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    if-nez v2, :cond_1

    .line 18
    sget-object v2, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->tag:Ljava/lang/String;

    const-string v3, "mHomeListener is null"

    invoke-static {v2, v3}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    const-string v2, "reason"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 24
    .local v1, "reason":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 25
    const-string v2, "homekey"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 26
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->mHomeListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;->onHomePressed()V

    goto :goto_0

    .line 27
    :cond_2
    const-string v2, "recentapps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->mHomeListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    invoke-interface {v2}, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;->onHomeLongPressed()V

    goto :goto_0
.end method

.method public setHomeKeyListener(Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver;->mHomeListener:Lcom/arcsoft/magicshotstudio/service/HomeKeyEventReceiver$HomeKeyListener;

    .line 42
    return-void
.end method
