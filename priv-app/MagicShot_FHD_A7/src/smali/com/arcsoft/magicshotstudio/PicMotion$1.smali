.class Lcom/arcsoft/magicshotstudio/PicMotion$1;
.super Ljava/lang/Object;
.source "PicMotion.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/arcsoft/magicshotstudio/PicMotion;->startMode()I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/magicshotstudio/PicMotion;


# direct methods
.method constructor <init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$1;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 90
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v1, "################### PicMotion processFramesEX #######"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion$1;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    invoke-static {v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$200(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$1;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mBitmapAddress:J
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$000(Lcom/arcsoft/magicshotstudio/PicMotion;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion$1;->this$0:Lcom/arcsoft/magicshotstudio/PicMotion;

    # getter for: Lcom/arcsoft/magicshotstudio/PicMotion;->mBitmapCount:I
    invoke-static {v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->access$100(Lcom/arcsoft/magicshotstudio/PicMotion;)I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->processFramesEX(JI)I

    .line 92
    return-void
.end method
