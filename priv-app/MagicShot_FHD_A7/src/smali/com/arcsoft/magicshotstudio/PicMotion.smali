.class public Lcom/arcsoft/magicshotstudio/PicMotion;
.super Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.source "PicMotion.java"

# interfaces
.implements Lcom/arcsoft/magicshotstudio/utils/APP_Constant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;
    }
.end annotation


# static fields
.field public static final tag:Ljava/lang/String;


# instance fields
.field private mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

.field private mFileLog:Ljava/lang/String;

.field private mFirstImage:Landroid/widget/ImageView;

.field private mIsFirstImageSetDone:Z

.field private mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

.field private mNotifyCallback:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

.field private mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

.field private mProgressNum:I

.field private mWaitDialog:Landroid/app/ProgressDialog;

.field private mbProcessingFinish:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/arcsoft/magicshotstudio/PicMotion;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .line 34
    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFileLog:Ljava/lang/String;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mbProcessingFinish:Z

    .line 37
    iput v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressNum:I

    .line 39
    iput-boolean v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mIsFirstImageSetDone:Z

    .line 183
    new-instance v0, Lcom/arcsoft/magicshotstudio/PicMotion$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/PicMotion$4;-><init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mNotifyCallback:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    .line 279
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/PicMotion;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mBitmapAddress:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/PicMotion;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mBitmapCount:I

    return v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/PicMotion;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressNum:I

    return v0
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/PicMotion;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/PicMotion;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->saveResultFile()V

    return-void
.end method

.method static synthetic access$502(Lcom/arcsoft/magicshotstudio/PicMotion;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mbProcessingFinish:Z

    return p1
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    return-object v0
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    return-object v0
.end method

.method static synthetic access$800(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/PicMotion;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/PicMotion;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method private initJNIEngine()I
    .locals 6

    .prologue
    .line 131
    new-instance v1, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    invoke-direct {v1}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;-><init>()V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    .line 132
    const/4 v0, 0x0

    .line 135
    .local v0, "res":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget v2, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mOrientation:I

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->init(I)I

    move-result v0

    .line 136
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mPreDataAddress:J

    iget v4, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mPreDataSize:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->setPreprocessorData(JI)I

    move-result v1

    or-int/2addr v0, v1

    .line 137
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mNativeExifJPGBuff:J

    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mNativeExifJPGBuffSize:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->setExifInfo(JJ)I

    move-result v1

    or-int/2addr v0, v1

    .line 138
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mNotifyCallback:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;->setNotifyListener(Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI$NotifyListener;)V

    .line 140
    return v0
.end method

.method private saveResultFile()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;

    .line 145
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f060015

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/PicMotion;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 147
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mWaitDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 149
    new-instance v0, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;-><init>(Lcom/arcsoft/magicshotstudio/PicMotion;Lcom/arcsoft/magicshotstudio/PicMotion$1;)V

    .line 150
    .local v0, "task":Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/PicMotion$SaveAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 151
    return-void
.end method

.method private setFirstImage()V
    .locals 2

    .prologue
    .line 103
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFirstImage:Landroid/widget/ImageView;

    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 113
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mIsFirstImageSetDone:Z

    .line 114
    return-void

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->hideFirstImage()V

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->hideFirstImage()V

    goto :goto_0
.end method

.method private showNoMovingObjectDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 155
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 157
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f06001e

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 158
    const v2, 0x7f06002c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 159
    const v2, 0x7f060014

    new-instance v3, Lcom/arcsoft/magicshotstudio/PicMotion$2;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/PicMotion$2;-><init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 168
    const v2, 0x7f06000e

    new-instance v3, Lcom/arcsoft/magicshotstudio/PicMotion$3;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/PicMotion$3;-><init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 177
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 178
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 179
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 180
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 181
    return-void
.end method

.method private uninit()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInit()V

    .line 246
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelDelDailog()V
    .locals 2

    .prologue
    .line 410
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->cancelDelDailog()V

    .line 411
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->setBtnClickEnable(Z)V

    .line 414
    :cond_0
    return-void
.end method

.method public getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mSavePath:Ljava/lang/String;

    .line 401
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getPicMotionSavePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mSavePath:Ljava/lang/String;

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 340
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->handleMessage(Landroid/os/Message;)V

    .line 341
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 379
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 345
    :pswitch_1
    sget-object v1, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v2, "Can not get the correct infomation from intent."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    goto :goto_0

    .line 350
    :pswitch_2
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 351
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v2, 0x7f060015

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/PicMotion;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    goto :goto_0

    .line 357
    :pswitch_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_0

    .line 358
    iget v1, p1, Landroid/os/Message;->arg1:I

    mul-int/lit8 v1, v1, 0x46

    div-int/lit8 v0, v1, 0x64

    .line 359
    .local v0, "progress":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 360
    iput v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressNum:I

    goto :goto_0

    .line 370
    .end local v0    # "progress":I
    :pswitch_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->startMode()I

    goto :goto_0

    .line 374
    :pswitch_5
    const v1, 0x7f06002f

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;II)V

    .line 376
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public hideFirstImage()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFirstImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    :cond_0
    return-void
.end method

.method protected onActivityServiceConnected()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onActivityServiceConnected()V

    .line 118
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mIsFirstImageSetDone:Z

    if-nez v0, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->setFirstImage()V

    .line 121
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 304
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v1, :cond_1

    .line 305
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isHelpViewShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 306
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hideHelpView()V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isSaveState()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->isSelectFrameView()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mIsUserEdit:Z

    if-eqz v1, :cond_2

    .line 313
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 317
    :cond_2
    const/4 v0, 0x0

    .line 318
    .local v0, "bHandled":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v1, :cond_3

    .line 319
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->onBackPressed()Z

    move-result v0

    .line 321
    :cond_3
    if-nez v0, :cond_0

    .line 322
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->setResult(I)V

    .line 323
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 276
    :cond_0
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 277
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 262
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v1, "################### PicMotion onDestroy #######"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->uninit()V

    .line 264
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 265
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->unInit()V

    .line 268
    :cond_0
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onDestroy()V

    .line 269
    return-void
.end method

.method public onHomeKeyPressed()V
    .locals 2

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFromGallery:Z

    if-nez v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mbProcessingFinish:Z

    if-eqz v0, :cond_1

    .line 385
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v1, "onHomeKeyPressed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->saveResult()V

    .line 390
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    .line 391
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 395
    :cond_1
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onHomeKeyPressed()V

    .line 396
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onPause()V

    .line 335
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->hidePopupToast()V

    .line 257
    :cond_0
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStop()V

    .line 258
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 329
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onUserLeaveHint()V

    .line 330
    return-void
.end method

.method protected startMode()I
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 50
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMode()I

    move-result v8

    .line 51
    .local v8, "ret":I
    if-eqz v8, :cond_0

    .line 54
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v1, "Can not get the correct infomation from intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    .line 99
    .end local v8    # "ret":I
    :goto_0
    return v8

    .line 58
    .restart local v8    # "ret":I
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030021

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 59
    .local v2, "contentView":Landroid/view/View;
    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/PicMotion;->setContentView(Landroid/view/View;)V

    .line 60
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/PicMotion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFirstImage:Landroid/widget/ImageView;

    .line 61
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 62
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->setFirstImage()V

    .line 67
    :goto_1
    iput-object v4, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mFileLog:Ljava/lang/String;

    .line 68
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->initJNIEngine()I

    move-result v7

    .line 71
    .local v7, "res":I
    if-eqz v7, :cond_2

    .line 72
    sget-object v0, Lcom/arcsoft/magicshotstudio/PicMotion;->tag:Ljava/lang/String;

    const-string v1, "init failed..."

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->finish()V

    move v8, v7

    .line 74
    goto :goto_0

    .line 64
    .end local v7    # "res":I
    :cond_1
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mIsFirstImageSetDone:Z

    .line 65
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/PicMotion;->hideFirstImage()V

    goto :goto_1

    .line 77
    .restart local v7    # "res":I
    :cond_2
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-nez v0, :cond_3

    .line 78
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v1, 0x7f060015

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/PicMotion;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 82
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    .line 85
    :cond_4
    iput-boolean v3, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mbProcessingFinish:Z

    .line 86
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/arcsoft/magicshotstudio/PicMotion$1;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/PicMotion$1;-><init>(Lcom/arcsoft/magicshotstudio/PicMotion;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 96
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    .line 97
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mSrcImageSize:[I

    iget-object v4, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mSavePath:Ljava/lang/String;

    iget v5, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mOrientation:I

    iget-object v6, p0, Lcom/arcsoft/magicshotstudio/PicMotion;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcom/arcsoft/magicshotstudio/ui/picmotion/ManualAction;->initManualModeUI(Landroid/app/Activity;Landroid/view/View;[ILjava/lang/String;ILcom/arcsoft/magicshotstudio/jni/picmotion/PicmotionEngine_JNI;)V

    goto/16 :goto_0
.end method
