.class public Lcom/arcsoft/magicshotstudio/Drama;
.super Lcom/arcsoft/magicshotstudio/activity/BaseActivity;
.source "Drama.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;,
        Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;,
        Lcom/arcsoft/magicshotstudio/Drama$InitEditDataCallback;
    }
.end annotation


# instance fields
.field private mContentView:Landroid/view/View;

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

.field private mFileLog:Ljava/lang/String;

.field private mFirstImage:Landroid/widget/ImageView;

.field private mHandler:Landroid/os/Handler;

.field private mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

.field private mIsFirstImageSetDone:Z

.field private mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

.field private mNotifyCallback:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

.field private mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

.field private mProgressNum:I

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mWaitDialog:Landroid/app/ProgressDialog;

.field private mbProcessingFinish:Z

.field private tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;-><init>()V

    .line 33
    const-class v0, Lcom/arcsoft/magicshotstudio/Drama;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .line 39
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFileLog:Ljava/lang/String;

    .line 40
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Drama;->mbProcessingFinish:Z

    .line 41
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHandler:Landroid/os/Handler;

    .line 46
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 47
    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mContentView:Landroid/view/View;

    .line 49
    iput v2, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressNum:I

    .line 51
    iput-boolean v2, p0, Lcom/arcsoft/magicshotstudio/Drama;->mIsFirstImageSetDone:Z

    .line 210
    new-instance v0, Lcom/arcsoft/magicshotstudio/Drama$4;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/Drama$4;-><init>(Lcom/arcsoft/magicshotstudio/Drama;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mNotifyCallback:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

    .line 347
    return-void
.end method

.method static synthetic access$000(Lcom/arcsoft/magicshotstudio/Drama;)J
    .locals 2
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mBitmapAddress:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/arcsoft/magicshotstudio/Drama;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mBitmapCount:I

    return v0
.end method

.method static synthetic access$1000(Lcom/arcsoft/magicshotstudio/Drama;)I
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressNum:I

    return v0
.end method

.method static synthetic access$1100(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSharedPref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEditor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/arcsoft/magicshotstudio/Drama;Lcom/arcsoft/magicshotstudio/ui/common/HelpView;)Lcom/arcsoft/magicshotstudio/ui/common/HelpView;
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;
    .param p1, "x1"    # Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    return-object v0
.end method

.method static synthetic access$400(Lcom/arcsoft/magicshotstudio/Drama;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->saveResultFile()V

    return-void
.end method

.method static synthetic access$500(Lcom/arcsoft/magicshotstudio/Drama;)Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method static synthetic access$600(Lcom/arcsoft/magicshotstudio/Drama;)V
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->showNoMovingObjectDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/arcsoft/magicshotstudio/Drama;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$802(Lcom/arcsoft/magicshotstudio/Drama;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mbProcessingFinish:Z

    return p1
.end method

.method static synthetic access$900(Lcom/arcsoft/magicshotstudio/Drama;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/arcsoft/magicshotstudio/Drama;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    return-object v0
.end method

.method private doProcessFrames()V
    .locals 2

    .prologue
    .line 148
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/arcsoft/magicshotstudio/Drama$1;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/Drama$1;-><init>(Lcom/arcsoft/magicshotstudio/Drama;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 156
    return-void
.end method

.method private initJNIEngine()V
    .locals 6

    .prologue
    .line 160
    new-instance v0, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    .line 163
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mOrientation:I

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->init(I)I

    .line 164
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/Drama;->mPreDataAddress:J

    iget v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mPreDataSize:I

    invoke-virtual {v0, v2, v3, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->setPreprocessorData(JI)I

    .line 165
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-wide v2, p0, Lcom/arcsoft/magicshotstudio/Drama;->mNativeExifJPGBuff:J

    iget-wide v4, p0, Lcom/arcsoft/magicshotstudio/Drama;->mNativeExifJPGBuffSize:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->setExifInfo(JJ)I

    .line 166
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mNotifyCallback:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;->setNotifyListener(Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI$NotifyListener;)V

    .line 167
    return-void
.end method

.method private saveResultFile()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 170
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;

    .line 171
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f060015

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Drama;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 173
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mWaitDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 175
    new-instance v0, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;-><init>(Lcom/arcsoft/magicshotstudio/Drama;Lcom/arcsoft/magicshotstudio/Drama$1;)V

    .line 176
    .local v0, "task":Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/Drama$SaveAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 177
    return-void
.end method

.method private setFirstImage()V
    .locals 2

    .prologue
    .line 121
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->isStartFromGallery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFirstImage:Landroid/widget/ImageView;

    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 131
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mIsFirstImageSetDone:Z

    .line 132
    return-void

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->hideFirstImage()V

    goto :goto_0

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->hideFirstImage()V

    goto :goto_0
.end method

.method private showNoMovingObjectDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 181
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 183
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f06001c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 184
    const v2, 0x7f06002c

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 185
    const v2, 0x7f060014

    new-instance v3, Lcom/arcsoft/magicshotstudio/Drama$2;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/Drama$2;-><init>(Lcom/arcsoft/magicshotstudio/Drama;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 194
    const v2, 0x7f06000e

    new-instance v3, Lcom/arcsoft/magicshotstudio/Drama$3;

    invoke-direct {v3, p0}, Lcom/arcsoft/magicshotstudio/Drama$3;-><init>(Lcom/arcsoft/magicshotstudio/Drama;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 203
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 204
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 205
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 206
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 207
    return-void
.end method

.method private uninit()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInit()V

    .line 259
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelDelDailog()V
    .locals 2

    .prologue
    .line 487
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->cancelDelDailog()V

    .line 488
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->setBtnClickEnable(Z)V

    .line 491
    :cond_0
    return-void
.end method

.method public getFirstImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getFirstBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getMagicShotService()Lcom/arcsoft/magicshotstudio/service/MagicShotService;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    return-object v0
.end method

.method public getSavePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getDramaSavePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSavePath:Ljava/lang/String;

    .line 481
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSavePath:Ljava/lang/String;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 413
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->handleMessage(Landroid/os/Message;)V

    .line 414
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 460
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 418
    :pswitch_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    const-string v2, "Can not get the correct infomation from intent."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    goto :goto_0

    .line 423
    :pswitch_2
    new-instance v1, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v1, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 424
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v2, 0x7f060015

    invoke-virtual {p0, v2}, Lcom/arcsoft/magicshotstudio/Drama;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 426
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    goto :goto_0

    .line 430
    :pswitch_3
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-eqz v1, :cond_0

    .line 431
    iget v1, p1, Landroid/os/Message;->arg1:I

    mul-int/lit8 v1, v1, 0x46

    div-int/lit8 v0, v1, 0x64

    .line 432
    .local v0, "progress":I
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v1, v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->updateProgress(I)V

    .line 433
    iput v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressNum:I

    goto :goto_0

    .line 443
    .end local v0    # "progress":I
    :pswitch_4
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->startMode()I

    goto :goto_0

    .line 447
    :pswitch_5
    const v1, 0x7f06002f

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/arcsoft/magicshotstudio/utils/MYToast;->show(Landroid/content/Context;II)V

    .line 449
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    goto :goto_0

    .line 452
    :pswitch_6
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->doProcessFrames()V

    goto :goto_0

    .line 455
    :pswitch_7
    new-instance v1, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;-><init>(Lcom/arcsoft/magicshotstudio/Drama;Lcom/arcsoft/magicshotstudio/Drama$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/arcsoft/magicshotstudio/Drama$InitEditDataAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public hideFirstImage()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFirstImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFirstImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 145
    :cond_0
    return-void
.end method

.method protected onActivityServiceConnected()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onActivityServiceConnected()V

    .line 136
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mIsFirstImageSetDone:Z

    if-nez v0, :cond_0

    .line 137
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->setFirstImage()V

    .line 139
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 373
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 374
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->hide()V

    .line 375
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isSaveState()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->isSelectFrameView()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mIsUserEdit:Z

    if-eqz v1, :cond_2

    .line 381
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mExitDialog:Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/common/ExitDialog;->show()V

    goto :goto_0

    .line 385
    :cond_2
    const/4 v0, 0x0

    .line 386
    .local v0, "bHandled":Z
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v1, :cond_3

    .line 387
    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->onBackPressed()Z

    move-result v0

    .line 389
    :cond_3
    if-nez v0, :cond_0

    .line 390
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Drama;->setResult(I)V

    .line 391
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0, p1}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHelpView:Lcom/arcsoft/magicshotstudio/ui/common/HelpView;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1}, Lcom/arcsoft/magicshotstudio/ui/common/HelpView;->updateHelpConfig(ILandroid/content/res/Configuration;)V

    .line 292
    :cond_1
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 293
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    const-string v0, "MagicShotApplication"

    const-string v1, "Drama onCreate1"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-super {p0, p1}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const-string v0, "MagicShotApplication"

    const-string v1, "Drama onCreate2"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHandler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    const-string v1, "onDestroy function"

    invoke-static {v0, v1}, Lcom/arcsoft/magicshotstudio/utils/ArcLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->uninit()V

    .line 277
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 278
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->unInitUI()V

    .line 281
    :cond_0
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onDestroy()V

    .line 282
    return-void
.end method

.method public onHomeKeyPressed()V
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mbProcessingFinish:Z

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    const-string v1, "onHomeKeyPressed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-boolean v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFromGallery:Z

    if-nez v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mMagicShotService:Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->setExitMode(Z)V

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->saveResult()Z

    .line 471
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    .line 472
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x5000

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 476
    :cond_1
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onHomeKeyPressed()V

    .line 477
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 408
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onPause()V

    .line 409
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 403
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onResume()V

    .line 404
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->hidePopupToast()V

    .line 270
    :cond_0
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onStop()V

    .line 271
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 397
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->onUserLeaveHint()V

    .line 398
    return-void
.end method

.method protected startMode()I
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 65
    invoke-super {p0}, Lcom/arcsoft/magicshotstudio/activity/BaseActivity;->startMode()I

    move-result v6

    .line 66
    .local v6, "res":I
    if-eqz v6, :cond_0

    .line 69
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    const-string v1, "Can not get the correct infomation from intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->finish()V

    .line 117
    :goto_0
    return v6

    .line 73
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mContentView:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mContentView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Drama;->setContentView(Landroid/view/View;)V

    .line 75
    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/arcsoft/magicshotstudio/Drama;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFirstImage:Landroid/widget/ImageView;

    .line 76
    invoke-static {}, Lcom/arcsoft/magicshotstudio/service/MagicShotService;->getSingleInstance()Lcom/arcsoft/magicshotstudio/service/MagicShotService;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 77
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->setFirstImage()V

    .line 83
    :goto_1
    const-string v0, "MAGICSHOT_SPF_Key"

    invoke-virtual {p0, v0, v4}, Lcom/arcsoft/magicshotstudio/Drama;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSharedPref:Landroid/content/SharedPreferences;

    .line 84
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEditor:Landroid/content/SharedPreferences$Editor;

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/arcsoft/magicshotstudio/Drama;->MAGICSHOT_PERFORMANCE_LOGPATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/arcsoft/magicshotstudio/Drama;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Performance_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".log"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mFileLog:Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    if-nez v0, :cond_1

    .line 89
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-direct {v0, p0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    const v1, 0x7f060015

    invoke-virtual {p0, v1}, Lcom/arcsoft/magicshotstudio/Drama;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->reset()V

    .line 94
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mProgressDialog:Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;

    invoke-virtual {v0}, Lcom/arcsoft/magicshotstudio/ui/common/WaitingProgressDialog;->show()V

    .line 97
    :cond_2
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/Drama;->mbProcessingFinish:Z

    .line 99
    invoke-direct {p0}, Lcom/arcsoft/magicshotstudio/Drama;->initJNIEngine()V

    .line 113
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 114
    new-instance v0, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    invoke-direct {v0}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    .line 115
    iget-object v0, p0, Lcom/arcsoft/magicshotstudio/Drama;->mManualAction:Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;

    iget-object v2, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSrcImageSize:[I

    iget-object v3, p0, Lcom/arcsoft/magicshotstudio/Drama;->mSavePath:Ljava/lang/String;

    iget v4, p0, Lcom/arcsoft/magicshotstudio/Drama;->mOrientation:I

    iget-object v5, p0, Lcom/arcsoft/magicshotstudio/Drama;->mEngineJNI:Lcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/arcsoft/magicshotstudio/ui/drama/ManualAction;->initManualModeUI(Landroid/app/Activity;[ILjava/lang/String;ILcom/arcsoft/magicshotstudio/jni/drama/ActionEngine_JNI;)V

    goto/16 :goto_0

    .line 79
    :cond_3
    iput-boolean v4, p0, Lcom/arcsoft/magicshotstudio/Drama;->mIsFirstImageSetDone:Z

    .line 80
    invoke-virtual {p0}, Lcom/arcsoft/magicshotstudio/Drama;->hideFirstImage()V

    goto/16 :goto_1
.end method
