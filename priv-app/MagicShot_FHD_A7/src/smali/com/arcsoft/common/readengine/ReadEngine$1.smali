.class Lcom/arcsoft/common/readengine/ReadEngine$1;
.super Ljava/lang/Object;
.source "ReadEngine.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/arcsoft/common/readengine/ReadEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/arcsoft/common/readengine/ReadEngine;


# direct methods
.method constructor <init>(Lcom/arcsoft/common/readengine/ReadEngine;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/arcsoft/common/readengine/ReadEngine$1;->this$0:Lcom/arcsoft/common/readengine/ReadEngine;

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 104
    iget-object v3, p0, Lcom/arcsoft/common/readengine/ReadEngine$1;->this$0:Lcom/arcsoft/common/readengine/ReadEngine;

    # getter for: Lcom/arcsoft/common/readengine/ReadEngine;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;
    invoke-static {v3}, Lcom/arcsoft/common/readengine/ReadEngine;->access$0(Lcom/arcsoft/common/readengine/ReadEngine;)Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    move-result-object v3

    if-nez v3, :cond_0

    .line 105
    const/4 v2, 0x0

    .line 120
    :goto_0
    return v2

    .line 107
    :cond_0
    const/4 v2, 0x1

    .line 108
    .local v2, "res":Z
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 118
    const/4 v2, 0x0

    goto :goto_0

    .line 110
    :pswitch_0
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 111
    .local v1, "progress":I
    iget-object v3, p0, Lcom/arcsoft/common/readengine/ReadEngine$1;->this$0:Lcom/arcsoft/common/readengine/ReadEngine;

    # getter for: Lcom/arcsoft/common/readengine/ReadEngine;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;
    invoke-static {v3}, Lcom/arcsoft/common/readengine/ReadEngine;->access$0(Lcom/arcsoft/common/readengine/ReadEngine;)Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;->onNotifyProgress(I)V

    goto :goto_0

    .line 114
    .end local v1    # "progress":I
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 115
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/arcsoft/common/readengine/ReadEngine$1;->this$0:Lcom/arcsoft/common/readengine/ReadEngine;

    # getter for: Lcom/arcsoft/common/readengine/ReadEngine;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;
    invoke-static {v3}, Lcom/arcsoft/common/readengine/ReadEngine;->access$0(Lcom/arcsoft/common/readengine/ReadEngine;)Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;->onFirstPicture(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
