.class public Lcom/arcsoft/common/readengine/ReadEngine;
.super Ljava/lang/Object;
.source "ReadEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;,
        Lcom/arcsoft/common/readengine/ReadEngine$ReturnBitmap;,
        Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFFR;,
        Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;
    }
.end annotation


# static fields
.field public static final MPAF_16BITS:J = 0x5000000L

.field public static final MPAF_32BITS:J = 0x7000000L

.field public static final MPAF_BGR:J = 0x1000L

.field public static final MPAF_I420:J

.field public static final MPAF_I422H:J

.field public static final MPAF_I422V:J

.field public static final MPAF_I444:J

.field public static final MPAF_OTHERS:J = 0x70000000L

.field public static final MPAF_OTHERS_NV12:J = 0x70000003L

.field public static final MPAF_OTHERS_NV21:J = 0x70000002L

.field public static final MPAF_RGB16_R5G6B5:J

.field public static final MPAF_RGB32_A8R8G8B8:J

.field public static final MPAF_RGB32_B8G8R8A8:J

.field public static final MPAF_RGBA_BASE:J = 0x30000000L

.field public static final MPAF_RGB_BASE:J = 0x10000000L

.field public static final MPAF_YUV_BASE:J = 0x50000000L

.field public static final MPAF_YUV_PLANAR:J = 0x800L

.field private static final MSG_NOTIFY_FIRSTBITMAP:I = 0x2

.field private static final MSG_NOTIFY_PROGRESS:I = 0x1


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

.field private mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

.field private mMSGCallback:Landroid/os/Handler$Callback;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/16 v8, 0x8

    const-wide/32 v6, 0x50000800

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 30
    invoke-static {v5}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_H(I)J

    move-result-wide v0

    or-long/2addr v0, v6

    invoke-static {v5}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_V(I)J

    move-result-wide v2

    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_I420:J

    .line 32
    invoke-static {v4}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_H(I)J

    move-result-wide v0

    or-long/2addr v0, v6

    invoke-static {v5}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_V(I)J

    move-result-wide v2

    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_I422V:J

    .line 34
    invoke-static {v5}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_H(I)J

    move-result-wide v0

    or-long/2addr v0, v6

    invoke-static {v4}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_V(I)J

    move-result-wide v2

    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_I422H:J

    .line 36
    invoke-static {v4}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_H(I)J

    move-result-wide v0

    or-long/2addr v0, v6

    invoke-static {v4}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_V(I)J

    move-result-wide v2

    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_I444:J

    .line 43
    const-wide/32 v0, 0x15000000

    .line 44
    invoke-static {v9}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_R(I)J

    move-result-wide v2

    .line 43
    or-long/2addr v0, v2

    .line 44
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_G(I)J

    move-result-wide v2

    .line 43
    or-long/2addr v0, v2

    .line 44
    invoke-static {v9}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_B(I)J

    move-result-wide v2

    .line 43
    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_RGB16_R5G6B5:J

    .line 45
    const-wide/32 v0, 0x37000000

    .line 46
    invoke-static {v8}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_R(I)J

    move-result-wide v2

    .line 45
    or-long/2addr v0, v2

    .line 46
    invoke-static {v8}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_G(I)J

    move-result-wide v2

    .line 45
    or-long/2addr v0, v2

    .line 46
    invoke-static {v8}, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_MAKE_B(I)J

    move-result-wide v2

    .line 45
    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_RGB32_A8R8G8B8:J

    .line 47
    const-wide/16 v0, 0x1000

    sget-wide v2, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_RGB32_A8R8G8B8:J

    or-long/2addr v0, v2

    sput-wide v0, Lcom/arcsoft/common/readengine/ReadEngine;->MPAF_RGB32_B8G8R8A8:J

    .line 15
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, "ReadEngine"

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    .line 101
    new-instance v0, Lcom/arcsoft/common/readengine/ReadEngine$1;

    invoke-direct {v0, p0}, Lcom/arcsoft/common/readengine/ReadEngine$1;-><init>(Lcom/arcsoft/common/readengine/ReadEngine;)V

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mMSGCallback:Landroid/os/Handler$Callback;

    .line 123
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mMSGCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mUIHandler:Landroid/os/Handler;

    .line 71
    return-void
.end method

.method public static MPAF_MAKE_B(I)J
    .locals 2
    .param p0, "b"    # I

    .prologue
    .line 66
    add-int/lit8 v0, p0, -0x1

    shl-int/lit8 v0, v0, 0x0

    int-to-long v0, v0

    return-wide v0
.end method

.method public static MPAF_MAKE_G(I)J
    .locals 2
    .param p0, "g"    # I

    .prologue
    .line 62
    add-int/lit8 v0, p0, -0x1

    shl-int/lit8 v0, v0, 0x4

    int-to-long v0, v0

    return-wide v0
.end method

.method public static MPAF_MAKE_H(I)J
    .locals 2
    .param p0, "n"    # I

    .prologue
    .line 50
    add-int/lit8 v0, p0, -0x1

    shl-int/lit8 v0, v0, 0x4

    int-to-long v0, v0

    return-wide v0
.end method

.method public static MPAF_MAKE_R(I)J
    .locals 2
    .param p0, "r"    # I

    .prologue
    .line 58
    add-int/lit8 v0, p0, -0x1

    shl-int/lit8 v0, v0, 0x8

    int-to-long v0, v0

    return-wide v0
.end method

.method public static MPAF_MAKE_V(I)J
    .locals 2
    .param p0, "n"    # I

    .prologue
    .line 54
    add-int/lit8 v0, p0, -0x1

    shl-int/lit8 v0, v0, 0x0

    int-to-long v0, v0

    return-wide v0
.end method

.method static synthetic access$0(Lcom/arcsoft/common/readengine/ReadEngine;)Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    return-object v0
.end method

.method private native getBitmapFromMemoryByIndex(Lcom/arcsoft/common/readengine/ReadHandler;IIIJLcom/arcsoft/common/readengine/ReadEngine$ReturnBitmap;)I
.end method

.method private native initEngine(Lcom/arcsoft/common/readengine/ReadHandler;[Ljava/lang/String;ZIIIJ)I
.end method

.method private native initEngineFromData(Lcom/arcsoft/common/readengine/ReadHandler;[[BZIIIJZ)I
.end method

.method private native initEngineFromSEF(Lcom/arcsoft/common/readengine/ReadHandler;Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;ZIIIJZ)I
.end method

.method private native initHandler(Lcom/arcsoft/common/readengine/ReadHandler;)I
.end method

.method private onFirstBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "firstBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 132
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 133
    .local v0, "message":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 134
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 135
    return-void
.end method

.method private onNotifyProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 127
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 128
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 129
    return-void
.end method

.method private native readToMemory(Lcom/arcsoft/common/readengine/ReadHandler;)J
.end method

.method private native unInitEngine(Lcom/arcsoft/common/readengine/ReadHandler;)I
.end method

.method private native unInitHandler(Lcom/arcsoft/common/readengine/ReadHandler;)I
.end method

.method private native writeLogToFilePath(Lcom/arcsoft/common/readengine/ReadHandler;Ljava/lang/String;)I
.end method


# virtual methods
.method public native copyMemoryFromData([B)J
.end method

.method public native functionToBlur(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;I)I
.end method

.method public getBitmapFromMemoryByIndex(IIIJLcom/arcsoft/common/readengine/ReadEngine$ReturnBitmap;)I
    .locals 10
    .param p1, "index"    # I
    .param p2, "dstRawWidth"    # I
    .param p3, "dstRawHeight"    # I
    .param p4, "dstRawPixelFormat"    # J
    .param p6, "result"    # Lcom/arcsoft/common/readengine/ReadEngine$ReturnBitmap;

    .prologue
    .line 253
    iget-object v2, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    move-object v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/arcsoft/common/readengine/ReadEngine;->getBitmapFromMemoryByIndex(Lcom/arcsoft/common/readengine/ReadHandler;IIIJLcom/arcsoft/common/readengine/ReadEngine$ReturnBitmap;)I

    move-result v0

    return v0
.end method

.method public initEngine([Ljava/lang/String;ZIIIJ)I
    .locals 10
    .param p1, "srcFilePath"    # [Ljava/lang/String;
    .param p2, "needDecode"    # Z
    .param p3, "coreCnt"    # I
    .param p4, "rawWidth"    # I
    .param p5, "rawHeight"    # I
    .param p6, "rawPixelFormat"    # J

    .prologue
    .line 165
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->unInitEngine(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 169
    :cond_0
    new-instance v0, Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {v0}, Lcom/arcsoft/common/readengine/ReadHandler;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    .line 170
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->initHandler(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 171
    iget-object v2, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/arcsoft/common/readengine/ReadEngine;->initEngine(Lcom/arcsoft/common/readengine/ReadHandler;[Ljava/lang/String;ZIIIJ)I

    move-result v0

    return v0
.end method

.method public initEngineFromData([[BZIIIJZ)I
    .locals 12
    .param p1, "srcData"    # [[B
    .param p2, "needDecode"    # Z
    .param p3, "coreCnt"    # I
    .param p4, "rawWidth"    # I
    .param p5, "rawHeight"    # I
    .param p6, "rawPixelFormat"    # J
    .param p8, "bMemCpy"    # Z

    .prologue
    .line 190
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->unInitEngine(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 194
    :cond_0
    new-instance v0, Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {v0}, Lcom/arcsoft/common/readengine/ReadHandler;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    .line 195
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->initHandler(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 197
    iget-object v2, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/arcsoft/common/readengine/ReadEngine;->initEngineFromData(Lcom/arcsoft/common/readengine/ReadHandler;[[BZIIIJZ)I

    move-result v0

    return v0
.end method

.method public initEngineFromSEF(Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;ZIIIJZ)I
    .locals 12
    .param p1, "sefData"    # Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;
    .param p2, "needDecode"    # Z
    .param p3, "coreCnt"    # I
    .param p4, "rawWidth"    # I
    .param p5, "rawHeight"    # I
    .param p6, "rawPixelFormat"    # J
    .param p8, "bMemCpy"    # Z

    .prologue
    .line 218
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->unInitEngine(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 222
    :cond_0
    new-instance v0, Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {v0}, Lcom/arcsoft/common/readengine/ReadHandler;-><init>()V

    iput-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    .line 223
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->initHandler(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 225
    iget-object v2, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/arcsoft/common/readengine/ReadEngine;->initEngineFromSEF(Lcom/arcsoft/common/readengine/ReadHandler;Lcom/arcsoft/common/readengine/ReadEngine$SEFBUFF_ARRAY;ZIIIJZ)I

    move-result v0

    return v0
.end method

.method public readToMemory()J
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0}, Lcom/arcsoft/common/readengine/ReadEngine;->readToMemory(Lcom/arcsoft/common/readengine/ReadHandler;)J

    move-result-wide v0

    return-wide v0
.end method

.method public native releaseCopyMemory(J)I
.end method

.method public setNotifyListener(Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mListener:Lcom/arcsoft/common/readengine/ReadEngine$NotifyListener;

    .line 144
    return-void
.end method

.method public native setOpenLog(Z)I
.end method

.method public unInitEngine()I
    .locals 4

    .prologue
    .line 267
    const/4 v0, -0x1

    .line 268
    .local v0, "ret":I
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v1}, Lcom/arcsoft/common/readengine/ReadEngine;->unInitEngine(Lcom/arcsoft/common/readengine/ReadHandler;)I

    move-result v0

    .line 271
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v1}, Lcom/arcsoft/common/readengine/ReadEngine;->unInitHandler(Lcom/arcsoft/common/readengine/ReadHandler;)I

    .line 272
    iget-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/arcsoft/common/readengine/ReadHandler;->setmHandlerAddress(J)V

    .line 274
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    .line 275
    return v0
.end method

.method public writeLogToFilePath(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/arcsoft/common/readengine/ReadEngine;->mHandler:Lcom/arcsoft/common/readengine/ReadHandler;

    invoke-direct {p0, v0, p1}, Lcom/arcsoft/common/readengine/ReadEngine;->writeLogToFilePath(Lcom/arcsoft/common/readengine/ReadHandler;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
