.class public Lcom/arcsoft/sefmanager/SEFManager;
.super Ljava/lang/Object;
.source "SEFManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;,
        Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;,
        Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;,
        Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;
    }
.end annotation


# static fields
.field public static final BESTFACE_MODE:I = 0x2

.field public static final BESTPHOTO_MODE:I = 0x1

.field public static final DRAMA_MODE:I = 0x4

.field public static final ERASER_MODE:I = 0x3

.field public static final MOTION_MODE:I = 0x5

.field public static final RET_BAD_STATE:I = 0x10008

.field public static final RET_BASE:I = 0x10000

.field public static final RET_INVALID_PARAMS:I = 0x10001

.field public static final RET_NO_MEMORY:I = 0x10002

.field public static final RET_OK:I = 0x0

.field public static final RET_OPEN_FILE_FAIL:I = 0x10003

.field public static final RET_SEFDATA_NOT_EXIST:I = 0x10007

.field public static final RET_SEF_EXTRACT_FAIL:I = 0x10004

.field public static final RET_SEF_NOT_EXIST:I = 0x10006

.field public static final RET_SEF_NOT_INIT:I = 0x10005

.field public static final SEF_JPEG:I = 0x1

.field public static final SEF_MAGICSHOT_INFO:I = 0x830

.field public static final SEF_USER_DATA:I


# instance fields
.field private mSEFNative:I

.field private mSyncLock:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    .line 26
    iput v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    .line 3
    return-void
.end method

.method public static native nativeGetEnableInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$MSENABLE;)I
.end method

.method public static native nativeGetSEFBuff(Ljava/lang/String;ILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
.end method

.method public static native nativeGetSelectInfo(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;Lcom/arcsoft/sefmanager/SEFManager$SELECTINFO;)I
.end method

.method public static native nativeIsSEF(Ljava/lang/String;)Z
.end method

.method public static native nativeReleaseSEFBuff(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
.end method


# virtual methods
.method public nativeExtractSEF(Ljava/lang/String;)I
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 202
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 203
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 204
    monitor-exit v2

    const v0, 0x10008

    .line 207
    :goto_0
    return v0

    .line 206
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1}, Lcom/arcsoft/sefmanager/SEFManager;->native_ExtractSEF(ILjava/lang/String;)I

    move-result v0

    .line 207
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 202
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeGetSEFBuffByIndex(ILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
    .locals 3
    .param p1, "nIndex"    # I
    .param p2, "buff"    # Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    .prologue
    .line 182
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 183
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 184
    monitor-exit v2

    const v0, 0x10008

    .line 187
    :goto_0
    return v0

    .line 186
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1, p2}, Lcom/arcsoft/sefmanager/SEFManager;->native_GetSEFBuffByIndex(IILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I

    move-result v0

    .line 187
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 182
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeGetSEFBuffByName(Ljava/lang/String;Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "buff"    # Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    .prologue
    .line 172
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 173
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 174
    monitor-exit v2

    const v0, 0x10008

    .line 177
    :goto_0
    return v0

    .line 176
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1, p2}, Lcom/arcsoft/sefmanager/SEFManager;->native_GetSEFBuffByName(ILjava/lang/String;Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I

    move-result v0

    .line 177
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 172
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeGetSEFBuffs(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;)I
    .locals 3
    .param p1, "buffArray"    # Lcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;

    .prologue
    .line 192
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 193
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 194
    monitor-exit v2

    const v0, 0x10008

    .line 197
    :goto_0
    return v0

    .line 196
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1}, Lcom/arcsoft/sefmanager/SEFManager;->native_GetSEFBuffs(ILcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;)I

    move-result v0

    .line 197
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 192
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeGetSEFDataCount()I
    .locals 3

    .prologue
    .line 162
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 163
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 164
    monitor-exit v2

    const v0, 0x10008

    .line 167
    :goto_0
    return v0

    .line 166
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1}, Lcom/arcsoft/sefmanager/SEFManager;->native_GetSEFDataCount(I)I

    move-result v0

    .line 167
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 162
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeInit(Ljava/lang/String;)I
    .locals 3
    .param p1, "dstSEFName"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 79
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/arcsoft/sefmanager/SEFManager;->native_Init(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    .line 80
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 81
    .local v0, "ret":I
    :goto_0
    monitor-exit v2

    return v0

    .line 80
    .end local v0    # "ret":I
    :cond_0
    const v0, 0x10001

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeInsertSEFData(Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
    .locals 3
    .param p1, "sefbuff"    # Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;

    .prologue
    .line 100
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 101
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 102
    monitor-exit v2

    const v0, 0x10008

    .line 106
    :goto_0
    return v0

    .line 105
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1}, Lcom/arcsoft/sefmanager/SEFManager;->native_InsertSEFData(ILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I

    move-result v0

    .line 106
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 100
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeInsertSEFData(Ljava/lang/String;I)I
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "sef_type"    # I

    .prologue
    .line 122
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 123
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 124
    monitor-exit v2

    const v0, 0x10008

    .line 127
    :goto_0
    return v0

    .line 126
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1, p2}, Lcom/arcsoft/sefmanager/SEFManager;->native_InsertSEFData(ILjava/lang/String;I)I

    move-result v0

    .line 127
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 122
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeInsertSEFData([BI)I
    .locals 3
    .param p1, "buff"    # [B
    .param p2, "sef_type"    # I

    .prologue
    .line 111
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 112
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 113
    monitor-exit v2

    const v0, 0x10008

    .line 117
    :goto_0
    return v0

    .line 116
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1, p2}, Lcom/arcsoft/sefmanager/SEFManager;->native_InsertSEFData(I[BI)I

    move-result v0

    .line 117
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 111
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeInsertSEFDatas([Ljava/lang/String;I)I
    .locals 3
    .param p1, "filePath"    # [Ljava/lang/String;
    .param p2, "sef_type"    # I

    .prologue
    .line 132
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 133
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 134
    monitor-exit v2

    const v0, 0x10008

    .line 137
    :goto_0
    return v0

    .line 136
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1, p2}, Lcom/arcsoft/sefmanager/SEFManager;->native_InsertSEFDatas(I[Ljava/lang/String;I)I

    move-result v0

    .line 137
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 132
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeRemoveAllSEFDatas()I
    .locals 3

    .prologue
    .line 152
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 153
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 154
    monitor-exit v2

    const v0, 0x10008

    .line 157
    :goto_0
    return v0

    .line 156
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1}, Lcom/arcsoft/sefmanager/SEFManager;->native_RemoveAllSEFDatas(I)I

    move-result v0

    .line 157
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 152
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeRemoveSEFData(Ljava/lang/String;)I
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 142
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 143
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 144
    monitor-exit v2

    const v0, 0x10008

    .line 147
    :goto_0
    return v0

    .line 146
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1, p1}, Lcom/arcsoft/sefmanager/SEFManager;->native_RemoveSEFData(ILjava/lang/String;)I

    move-result v0

    .line 147
    .local v0, "ret":I
    monitor-exit v2

    goto :goto_0

    .line 142
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public nativeUninit()I
    .locals 3

    .prologue
    .line 86
    iget-object v2, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSyncLock:[B

    monitor-enter v2

    .line 87
    :try_start_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    if-nez v1, :cond_0

    .line 88
    monitor-exit v2

    const v0, 0x10008

    .line 95
    :goto_0
    return v0

    .line 91
    :cond_0
    iget v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    invoke-virtual {p0, v1}, Lcom/arcsoft/sefmanager/SEFManager;->native_Uninit(I)I

    move-result v0

    .line 92
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 93
    const/4 v1, 0x0

    iput v1, p0, Lcom/arcsoft/sefmanager/SEFManager;->mSEFNative:I

    .line 95
    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 86
    .end local v0    # "ret":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public native native_ExtractSEF(ILjava/lang/String;)I
.end method

.method public native native_GetSEFBuffByIndex(IILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
.end method

.method public native native_GetSEFBuffByName(ILjava/lang/String;Lcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
.end method

.method public native native_GetSEFBuffs(ILcom/arcsoft/sefmanager/SEFManager$SEFBUFF_ARRAY;)I
.end method

.method public native native_GetSEFDataCount(I)I
.end method

.method public native native_Init(Ljava/lang/String;)I
.end method

.method public native native_InsertSEFData(ILcom/arcsoft/sefmanager/SEFManager$SEFBUFFR;)I
.end method

.method public native native_InsertSEFData(ILjava/lang/String;I)I
.end method

.method public native native_InsertSEFData(I[BI)I
.end method

.method public native native_InsertSEFDatas(I[Ljava/lang/String;I)I
.end method

.method public native native_RemoveAllSEFDatas(I)I
.end method

.method public native native_RemoveSEFData(ILjava/lang/String;)I
.end method

.method public native native_Uninit(I)I
.end method
