.class public Lcom/maximintegrated/bio/uv/MaximUVSensor;
.super Ljava/lang/Object;
.source "MaximUVSensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/location/LocationListener;


# static fields
.field private static final IN_ACC_X:I = 0x4

.field private static final IN_ACC_Y:I = 0x5

.field private static final IN_ACC_Z:I = 0x6

.field private static final IN_ALTITUDE:I = 0x3

.field private static final IN_LATITUDE:I = 0x1

.field private static final IN_LONGITUDE:I = 0x2

.field private static final IN_MAG_X:I = 0x8

.field private static final IN_MAG_Y:I = 0x9

.field private static final IN_MAG_Z:I = 0xa

.field private static final IN_PRESS:I = 0x7

.field private static final IN_UVRAW:I = 0x0

.field private static final LOGTAG:Ljava/lang/String; = "MaximUV"

.field private static final MIN_DISTANCE_CHANGE_FOR_UPDATES:J = 0xaL

.field private static final MIN_TIME_BW_UPDATES:J = 0xea60L

.field private static final OUT_UVA:I = 0x2

.field private static final OUT_UVB:I = 0x3

.field private static final OUT_UVINDEX:I = 0x1

.field private static final OUT_UVINTENSITY:I = 0x4

.field private static final OUT_UVRAW:I = 0x0

.field private static final VERSION:Ljava/lang/String; = "1.0"

.field private static isJniLoaded:Z


# instance fields
.field private accSensor:Landroid/hardware/Sensor;

.field private algorithm_data_in:[F

.field private algorithm_data_out:[F

.field private final context:Landroid/content/Context;

.field private isLocationAvailable:Z

.field private location:Landroid/location/Location;

.field private locationManager:Landroid/location/LocationManager;

.field private magSensor:Landroid/hardware/Sensor;

.field private pressSensor:Landroid/hardware/Sensor;

.field private sensor_type:I

.field private sm:Landroid/hardware/SensorManager;

.field private uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

.field private uvListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

.field private uvSensor:Landroid/hardware/Sensor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 303
    :try_start_0
    const-string v1, "MxmUVSensor-jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 304
    const/4 v1, 0x1

    sput-boolean v1, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isJniLoaded:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 305
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 306
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isJniLoaded:Z

    .line 307
    const-string v1, "MaximUV"

    const-string v2, "JNI File Not Found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensor_type"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sensor_type:I

    .line 30
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 31
    iput-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isLocationAvailable:Z

    .line 59
    const/16 v0, 0xb

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    .line 60
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    .line 62
    const/4 v0, 0x5

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    .line 64
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .line 68
    const-string v0, "MaximUV"

    const-string v1, "Version:1.0"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->context:Landroid/content/Context;

    .line 70
    iput p2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sensor_type:I

    .line 73
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 72
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    .line 74
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvSensor:Landroid/hardware/Sensor;

    .line 75
    const-string v0, "MaximUV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MaximUVSensor() - uvSensor : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->accSensor:Landroid/hardware/Sensor;

    .line 77
    const-string v0, "MaximUV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MaximUVSensor() - accSensor : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->accSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->pressSensor:Landroid/hardware/Sensor;

    .line 79
    const-string v0, "MaximUV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MaximUVSensor() - pressSensor : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->pressSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->magSensor:Landroid/hardware/Sensor;

    .line 81
    const-string v0, "MaximUV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MaximUVSensor() - magSensor : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->magSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 84
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 87
    return-void

    .line 59
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 62
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private native runAlgorithm([F[F)V
.end method

.method private native startAlgorithm()V
.end method

.method private native stopAlgorithm()V
.end method


# virtual methods
.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public isLocationAvailable()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 94
    const/4 v0, 0x0

    .line 95
    .local v0, "isGPSEnabled":Z
    const/4 v1, 0x0

    .line 96
    .local v1, "isNetworkEnabled":Z
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    if-eqz v2, :cond_1

    .line 97
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 98
    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 101
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 102
    const-string v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 104
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 105
    const-string v3, "passive"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    .line 104
    iput-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 107
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-nez v2, :cond_0

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 108
    iput-boolean v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isLocationAvailable:Z

    .line 109
    const-string v2, "MaximUV"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Location information is unavailable : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :goto_0
    iget-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isLocationAvailable:Z

    return v2

    .line 111
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isLocationAvailable:Z

    .line 112
    const-string v2, "MaximUV"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Location information is available : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    :cond_1
    iput-boolean v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isLocationAvailable:Z

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 271
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 275
    const-string v0, "MaximUV"

    const-string v1, "Location Updated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 277
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 281
    const-string v0, "MaximUV"

    const-string v1, "onProviderDisabled called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 286
    const-string v0, "MaximUV"

    const-string v1, "onProviderEnabled called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 214
    monitor-enter p0

    .line 215
    :try_start_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 233
    :goto_0
    :pswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    iget v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sensor_type:I

    if-ne v0, v1, :cond_0

    .line 235
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    .line 237
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-nez v0, :cond_1

    .line 238
    const-string v0, "MaximUV"

    const-string v1, "location null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x1

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    .line 240
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x2

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    .line 241
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x3

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    .line 252
    :goto_1
    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isJniLoaded:Z

    if-eqz v0, :cond_2

    .line 253
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    invoke-direct {p0, v0, v1}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->runAlgorithm([F[F)V

    .line 257
    :goto_2
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v1, v1

    iput v1, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->RAW:I

    .line 258
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    float-to-double v2, v1

    iput-wide v2, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVA:D

    .line 259
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    float-to-double v2, v1

    iput-wide v2, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVB:D

    .line 260
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    float-to-double v2, v1

    iput-wide v2, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVIndex:D

    .line 261
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    float-to-double v2, v1

    iput-wide v2, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVIntensity:D

    .line 262
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-interface {v0, v1}, Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;->onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V

    .line 214
    :cond_0
    monitor-exit p0

    .line 267
    return-void

    .line 217
    :pswitch_1
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x4

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    .line 218
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x5

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 219
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x6

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    aput v2, v0, v1

    goto/16 :goto_0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 223
    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x7

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    goto/16 :goto_0

    .line 227
    :pswitch_3
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v1, 0x8

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    .line 228
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v1, 0x9

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 229
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v1, 0xa

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    aput v2, v0, v1

    goto/16 :goto_0

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 246
    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    .line 245
    aput v2, v0, v1

    .line 247
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 248
    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    .line 247
    aput v2, v0, v1

    .line 249
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 250
    invoke-virtual {v2}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    double-to-float v2, v2

    .line 249
    aput v2, v0, v1

    goto/16 :goto_1

    .line 255
    :cond_2
    const-string v0, "MaximUV"

    .line 256
    const-string v1, "JNI not loaded calling runAlgorithm failed"

    .line 255
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 291
    const-string v0, "MaximUV"

    const-string v1, "onStatusChanged called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    return-void
.end method

.method public registerListener(Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;)V
    .locals 9
    .param p1, "uvListener"    # Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .prologue
    const-wide/32 v2, 0xea60

    const/4 v8, 0x3

    const/4 v5, 0x0

    const/high16 v4, 0x41200000    # 10.0f

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 125
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .line 126
    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-direct {v0}, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;-><init>()V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    .line 128
    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isJniLoaded:Z

    if-eqz v0, :cond_5

    .line 129
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->startAlgorithm()V

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 134
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->accSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 135
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->pressSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 137
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->magSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 140
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 141
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v6

    .line 144
    .local v6, "isGPSEnabled":Z
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 145
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v7

    .line 147
    .local v7, "isNetworkEnabled":Z
    const-string v0, "MaximUV"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "MaximUVSensor registerListener() - isGPSEnabled : "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", isNetworkEnabled : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 151
    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 150
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 152
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 153
    const-string v0, "MaximUV"

    const-string v1, "PASSIVE_PROVIDER provide location information"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-nez v0, :cond_3

    if-nez v6, :cond_1

    if-eqz v7, :cond_3

    .line 157
    :cond_1
    const-string v0, "MaximUV"

    .line 158
    const-string v1, "PASSIVE_PROVIDER DO NOT provide location information"

    .line 157
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    if-eqz v7, :cond_2

    .line 160
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 161
    const-string v1, "network"

    move-object v5, p0

    .line 160
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 163
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 165
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 164
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 166
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-eqz v0, :cond_2

    .line 167
    const-string v0, "MaximUV"

    .line 168
    const-string v1, "NETWORK_PROVIDER provide location information"

    .line 167
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_2
    if-eqz v6, :cond_3

    .line 173
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-nez v0, :cond_3

    .line 174
    const-string v0, "MaximUV"

    .line 175
    const-string v1, "NETWORK_PROVIDER DO NOT provide location information"

    .line 174
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 177
    const-string v1, "gps"

    move-object v5, p0

    .line 176
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 179
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    .line 181
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 180
    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 182
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-eqz v0, :cond_3

    .line 183
    const-string v0, "MaximUV"

    .line 184
    const-string v1, "GPS_PROVIDER provide location information"

    .line 183
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    if-nez v0, :cond_4

    .line 190
    if-nez v7, :cond_6

    if-nez v6, :cond_6

    .line 191
    const-string v0, "MaximUV"

    const-string v1, "No Location Service : algorithm will not run"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_4
    :goto_1
    return-void

    .line 131
    .end local v6    # "isGPSEnabled":Z
    .end local v7    # "isNetworkEnabled":Z
    :cond_5
    const-string v0, "MaximUV"

    const-string v1, "JNI not loaded calling startAlgorithm failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 193
    .restart local v6    # "isGPSEnabled":Z
    .restart local v7    # "isNetworkEnabled":Z
    :cond_6
    const-string v0, "MaximUV"

    .line 194
    const-string v1, "No LastKnowLocation, waiting for LocationListner"

    .line 193
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public unregisterListener(Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;)V
    .locals 3
    .param p1, "uvListener"    # Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .prologue
    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->sm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 201
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->locationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 203
    :cond_0
    iput-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->location:Landroid/location/Location;

    .line 204
    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isJniLoaded:Z

    if-eqz v0, :cond_1

    .line 205
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->stopAlgorithm()V

    .line 209
    :goto_0
    iput-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .line 210
    iput-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->uvEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    .line 211
    return-void

    .line 207
    :cond_1
    const-string v0, "MaximUV"

    const-string v1, "JNI not loaded calling stopAlgorithm failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
