.class public Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;
.super Ljava/lang/Object;
.source "MaximUVSensorEvent.java"


# instance fields
.field public RAW:I

.field public UVA:D

.field public UVB:D

.field public UVIndex:D

.field public UVIntensity:D


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->RAW:I

    .line 5
    iput-wide v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVA:D

    .line 6
    iput-wide v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVB:D

    .line 7
    iput-wide v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVIndex:D

    .line 8
    iput-wide v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->UVIntensity:D

    .line 3
    return-void
.end method
