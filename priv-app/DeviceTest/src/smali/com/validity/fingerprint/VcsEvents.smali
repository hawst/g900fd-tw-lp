.class public Lcom/validity/fingerprint/VcsEvents;
.super Ljava/lang/Object;
.source "VcsEvents.java"


# static fields
.field public static final FINGER_INDEX_ALL:I = 0x15

.field public static final FINGER_INDEX_LEFT_INDEX:I = 0x2

.field public static final FINGER_INDEX_LEFT_INDEX_SECOND:I = 0xc

.field public static final FINGER_INDEX_LEFT_LITTLE:I = 0x5

.field public static final FINGER_INDEX_LEFT_LITTLE_SECOND:I = 0xf

.field public static final FINGER_INDEX_LEFT_MIDDLE:I = 0x3

.field public static final FINGER_INDEX_LEFT_MIDDLE_SECOND:I = 0xd

.field public static final FINGER_INDEX_LEFT_RING:I = 0x4

.field public static final FINGER_INDEX_LEFT_RING_SECOND:I = 0xe

.field public static final FINGER_INDEX_LEFT_THUMB:I = 0x1

.field public static final FINGER_INDEX_LEFT_THUMB_SECOND:I = 0xb

.field public static final FINGER_INDEX_RIGHT_INDEX:I = 0x7

.field public static final FINGER_INDEX_RIGHT_INDEX_SECOND:I = 0x11

.field public static final FINGER_INDEX_RIGHT_LITTLE:I = 0xa

.field public static final FINGER_INDEX_RIGHT_LITTLE_SECOND:I = 0x14

.field public static final FINGER_INDEX_RIGHT_MIDDLE:I = 0x8

.field public static final FINGER_INDEX_RIGHT_MIDDLE_SECOND:I = 0x12

.field public static final FINGER_INDEX_RIGHT_RING:I = 0x9

.field public static final FINGER_INDEX_RIGHT_RING_SECOND:I = 0x13

.field public static final FINGER_INDEX_RIGHT_THUMB:I = 0x6

.field public static final FINGER_INDEX_RIGHT_THUMB_SECOND:I = 0x10

.field public static final VCS_EVT_ALL_SENSORS_INITIALIZED:I = 0x9

.field public static final VCS_EVT_EIV_FINGERPRINT_CAPTURED:I = 0x11

.field public static final VCS_EVT_EIV_FINGERPRINT_CAPTURED_BAD:I = 0x2d

.field public static final VCS_EVT_EIV_FINGERPRINT_CAPTURE_REDUNDANT:I = 0x31

.field public static final VCS_EVT_ENROLL_CAPTURE_STATUS:I = 0x20

.field public static final VCS_EVT_ENROLL_COMPLETED:I = 0xd

.field public static final VCS_EVT_ENROLL_FAILED:I = 0x1a8

.field public static final VCS_EVT_ENROLL_NEXT_CAPTURE_START:I = 0x10

.field public static final VCS_EVT_ENROLL_SUCCESS:I = 0x1a5

.field public static final VCS_EVT_ENVIRONMENT_NOISE_DETECTED:I = 0x2f

.field public static final VCS_EVT_FINGER_DETECTED:I = 0xb

.field public static final VCS_EVT_FINGER_REMOVED:I = 0x14

.field public static final VCS_EVT_FINGER_SETTLED:I = 0x21

.field public static final VCS_EVT_GESTURE:I = 0x1ab

.field public static final VCS_EVT_IDENTIFY_COMPLETED:I = 0xf

.field public static final VCS_EVT_IDENTIFY_FAILED:I = 0x1aa

.field public static final VCS_EVT_IDENTIFY_SUCCESS:I = 0x1a7

.field public static final VCS_EVT_SENSOR_DETECTED:I = 0x2

.field public static final VCS_EVT_SENSOR_FAILED_INITIALIZATION:I = 0x4

.field public static final VCS_EVT_SENSOR_FINGERPRINT_CAPTURE_COMPLETE:I = 0x5

.field public static final VCS_EVT_SENSOR_FINGERPRINT_CAPTURE_FAILED:I = 0x7

.field public static final VCS_EVT_SENSOR_FINGERPRINT_CAPTURE_START:I = 0x8

.field public static final VCS_EVT_SENSOR_FINGERPRINT_FAILED_SWIPE_RETRY:I = 0xa

.field public static final VCS_EVT_SENSOR_INFO:I = 0x1ac

.field public static final VCS_EVT_SENSOR_NON_RECOVERABLE:I = 0x2e

.field public static final VCS_EVT_SENSOR_RAW_FINGERPRINT_CAPTURE_COMPLETE:I = 0x6

.field public static final VCS_EVT_SENSOR_READY_FOR_USE:I = 0x3

.field public static final VCS_EVT_SENSOR_REMOVED:I = 0x1

.field public static final VCS_EVT_SET_IR_FLAGS:I = 0x25

.field public static final VCS_EVT_SWIPE_DIRECTION:I = 0x29

.field public static final VCS_EVT_SWIPE_SPEED_UPDATE:I = 0x2a

.field public static final VCS_EVT_VERIFY_COMPLETED:I = 0xe

.field public static final VCS_EVT_VERIFY_FAILED:I = 0x1a9

.field public static final VCS_EVT_VERIFY_SUCCESS:I = 0x1a6

.field public static final VCS_IMAGE_QUALITY_ASP_DATA_INVALID:I = 0x100000

.field public static final VCS_IMAGE_QUALITY_BAD_SWIPE:I = 0x2000

.field public static final VCS_IMAGE_QUALITY_BASELINE_DATA_INVALID:I = 0x400000

.field public static final VCS_IMAGE_QUALITY_EMPTY_TOUCH:I = 0x10000000

.field public static final VCS_IMAGE_QUALITY_FINGER_OFFSET:I = 0x1000

.field public static final VCS_IMAGE_QUALITY_FINGER_OFFSET_TOO_FAR_LEFT:I = 0x20000

.field public static final VCS_IMAGE_QUALITY_FINGER_OFFSET_TOO_FAR_RIGHT:I = 0x40000

.field public static final VCS_IMAGE_QUALITY_FINGER_TOO_THIN:I = 0x2000000

.field public static final VCS_IMAGE_QUALITY_GESTURE:I = 0x20000000

.field public static final VCS_IMAGE_QUALITY_GOOD:I = 0x0

.field public static final VCS_IMAGE_QUALITY_NOT_A_FINGER_SWIPE:I = 0x200000

.field public static final VCS_IMAGE_QUALITY_ONE_HAND_SWIPE:I = 0x400

.field public static final VCS_IMAGE_QUALITY_PARTIAL_TOUCH:I = 0x8000000

.field public static final VCS_IMAGE_QUALITY_PRESSURE_TOO_HARD:I = 0x80000

.field public static final VCS_IMAGE_QUALITY_PRESSURE_TOO_LIGHT:I = 0x10000

.field public static final VCS_IMAGE_QUALITY_PROCESS_FAILED:I = 0x40000000

.field public static final VCS_IMAGE_QUALITY_PROCESS_FAILED_FATAL:I = -0x80000000

.field public static final VCS_IMAGE_QUALITY_REVERSE_MOTION:I = 0x8

.field public static final VCS_IMAGE_QUALITY_SKEW_TOO_LARGE:I = 0x8000

.field public static final VCS_IMAGE_QUALITY_SOMETHING_ON_THE_SENSOR:I = 0x200

.field public static final VCS_IMAGE_QUALITY_STICTION:I = 0x1

.field public static final VCS_IMAGE_QUALITY_TOO_FAST:I = 0x2

.field public static final VCS_IMAGE_QUALITY_TOO_SHORT:I = 0x4

.field public static final VCS_IMAGE_QUALITY_TOO_SLOW:I = 0x10

.field public static final VCS_IMAGE_QUALITY_WET_FINGER:I = 0x1000000

.field public static final VCS_NOTIFY_AUTH_SESSION_BEGIN:I = 0x5

.field public static final VCS_NOTIFY_AUTH_SESSION_END:I = 0x6

.field public static final VCS_POLICY_AUTHENTICATE_ALWAYS:I = 0x1

.field public static final VCS_POLICY_AUTHENTICATE_ON_PREVIOUS_IDENTIFY_TIMEOUT:I = 0x3

.field public static final VCS_POLICY_AUTHENTICATE_ON_SCREEN_UNLOCK_TIMEOUT:I = 0x2

.field public static final VCS_PROTECT_ALG_SIMPLE_APP_DATA:I = 0x2

.field public static final VCS_PROTECT_ALG_SIMPLE_USER_DATA:I = 0x1

.field public static final VCS_RESULT_ALREADY_INPROGRESS:I = 0x3ec

.field public static final VCS_RESULT_BAD_DEVICE:I = 0x201

.field public static final VCS_RESULT_BAD_PARAM:I = 0x11

.field public static final VCS_RESULT_BAD_QUALITY_IMAGE:I = 0x259

.field public static final VCS_RESULT_CANCEL:I = 0x1

.field public static final VCS_RESULT_DATA_REMOVE_FAILED:I = 0x3eb

.field public static final VCS_RESULT_DATA_RETRIEVE_FAILED:I = 0x3ea

.field public static final VCS_RESULT_DATA_STORE_FAILED:I = 0x3e9

.field public static final VCS_RESULT_ERROR:I = 0x36

.field public static final VCS_RESULT_FAILED:I = -0x1

.field public static final VCS_RESULT_GET_PRINT_STOPPED:I = 0x1f5

.field public static final VCS_RESULT_INVALID_ARGUMENT:I = 0x3ee

.field public static final VCS_RESULT_INVALID_USER_HANDLE:I = 0x46

.field public static final VCS_RESULT_KEYDATA_NOT_FOUND:I = 0x309

.field public static final VCS_RESULT_MATCHER_ADD_IMAGE_FAILED:I = 0x132

.field public static final VCS_RESULT_MATCHER_CHECK_QUALITY_FAILED:I = 0x134

.field public static final VCS_RESULT_MATCHER_CLOSE_FAILED:I = 0x12d

.field public static final VCS_RESULT_MATCHER_ENROLL_FAILED:I = 0x130

.field public static final VCS_RESULT_MATCHER_EXTRACT_FAILED:I = 0x12f

.field public static final VCS_RESULT_MATCHER_MATCH_FAILED:I = 0x12e

.field public static final VCS_RESULT_MATCHER_MATCH_IMAGE_FAILED:I = 0x133

.field public static final VCS_RESULT_MATCHER_OPEN_FAILED:I = 0x12c

.field public static final VCS_RESULT_MATCHER_VERIFY_FAILED:I = 0x131

.field public static final VCS_RESULT_MATCH_FAILED:I = 0x16f

.field public static final VCS_RESULT_NOT_IMPLEMENTED:I = 0x3ed

.field public static final VCS_RESULT_NO_MORE_ENTRIES:I = 0x3

.field public static final VCS_RESULT_NULL_POINTER:I = 0x28

.field public static final VCS_RESULT_OK:I = 0x0

.field public static final VCS_RESULT_OPERATION_CANCELED:I = 0x169

.field public static final VCS_RESULT_OPERATION_DENIED:I = 0x1fe

.field public static final VCS_RESULT_OUT_OF_MEMORY:I = 0x2

.field public static final VCS_RESULT_PIPE_ERROR:I = 0x1ff

.field public static final VCS_RESULT_SENSOR_BUSY:I = 0x2e

.field public static final VCS_RESULT_SENSOR_IS_REMOVED:I = 0x168

.field public static final VCS_RESULT_SENSOR_KEYS_NOT_READY:I = 0x200

.field public static final VCS_RESULT_SENSOR_NOT_FOUND:I = 0x13

.field public static final VCS_RESULT_SENSOR_NOT_READY_FOR_USE:I = 0x166

.field public static final VCS_RESULT_SENSOR_OPEN_FAILED:I = 0x16

.field public static final VCS_RESULT_SERVICE_NOT_RUNNING:I = 0x141

.field public static final VCS_RESULT_SERVICE_STOPPED:I = 0x1f4

.field public static final VCS_RESULT_TEMPLATE_CORRUPTED:I = 0x57

.field public static final VCS_RESULT_TEMPLATE_DOESNT_EXIST:I = 0x42

.field public static final VCS_RESULT_TEMPLATE_FILE_CREATION_FAILED:I = 0x40

.field public static final VCS_RESULT_TIMEOUT:I = 0x22

.field public static final VCS_RESULT_TOO_MANY_BAD_SWIPES:I = 0x203

.field public static final VCS_RESULT_USER_DOESNT_EXIST:I = 0x41

.field public static final VCS_RESULT_USER_FINGER_ALREADY_ENROLLED:I = 0x58

.field public static final VCS_RESULT_USER_IDENTIFICATION_FAILED:I = 0x16a

.field public static final VCS_RESULT_USER_VERIFICATION_FAILED:I = 0x16b

.field public static final VCS_SECURITY_LEVEL_HIGH:I = 0x2

.field public static final VCS_SECURITY_LEVEL_LOW:I = 0x0

.field public static final VCS_SECURITY_LEVEL_REGULAR:I = 0x1

.field public static final VCS_SECURITY_LEVEL_VERY_HIGH:I = 0x3

.field public static final VCS_SWIPE_DIRECTION_DOWN:I = 0x1

.field public static final VCS_SWIPE_DIRECTION_LEFT:I = 0x3

.field public static final VCS_SWIPE_DIRECTION_REST:I = 0x5

.field public static final VCS_SWIPE_DIRECTION_RIGHT:I = 0x4

.field public static final VCS_SWIPE_DIRECTION_UP:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
