.class public Lcom/validity/fingerprint/SecurityPolicy;
.super Ljava/lang/Object;
.source "SecurityPolicy.java"


# instance fields
.field public policy:I

.field public time:I

.field public version:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "0.1"

    iput-object v0, p0, Lcom/validity/fingerprint/SecurityPolicy;->version:Ljava/lang/String;

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/validity/fingerprint/SecurityPolicy;->policy:I

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/validity/fingerprint/SecurityPolicy;->time:I

    .line 23
    return-void
.end method

.method constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "policy"    # I
    .param p3, "time"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/validity/fingerprint/SecurityPolicy;->version:Ljava/lang/String;

    .line 28
    iput p2, p0, Lcom/validity/fingerprint/SecurityPolicy;->policy:I

    .line 29
    iput p3, p0, Lcom/validity/fingerprint/SecurityPolicy;->time:I

    .line 30
    return-void
.end method
