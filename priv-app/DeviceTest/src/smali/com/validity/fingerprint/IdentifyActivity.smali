.class public Lcom/validity/fingerprint/IdentifyActivity;
.super Landroid/app/Activity;
.source "IdentifyActivity.java"

# interfaces
.implements Lcom/validity/fingerprint/FingerprintCore$EventListener;


# static fields
.field private static final CONFIRM_EXISTING_REQUEST:I = 0x64

.field private static final DBG:Z = true

.field public static FINGER_ACTION_LABEL:Ljava/lang/String; = null

.field public static final REQUEST_CODE:I = 0x65

.field private static final TAG:Ljava/lang/String; = "IdentifyActivity"

.field public static final USER_ID:Ljava/lang/String; = "userId"

.field public static final VCS_RESULT:Ljava/lang/String; = "result"

.field private static mTotalFailedPatternAttempts:I


# instance fields
.field private mActivityView:Landroid/view/View;

.field private mAlternateButton:Landroid/widget/Button;

.field private mCancelButton:Landroid/widget/Button;

.field private mCountdownTimer:Landroid/os/CountDownTimer;

.field private mFingerprint:Lcom/validity/fingerprint/Fingerprint;

.field private mHandler:Landroid/os/Handler;

.field private mLock:Ljava/util/concurrent/Semaphore;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mPowerMgr:Landroid/os/PowerManager;

.field private mSensorView:Lcom/validity/fingerprint/SensorView;

.field private mStatusView:Landroid/widget/TextView;

.field private mThread:Ljava/lang/Thread;

.field private mTimeoutText:Landroid/widget/TextView;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "Please swipe"

    sput-object v0, Lcom/validity/fingerprint/IdentifyActivity;->FINGER_ACTION_LABEL:Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    sput v0, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mHandler:Landroid/os/Handler;

    .line 55
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mLock:Ljava/util/concurrent/Semaphore;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mUserId:Ljava/lang/String;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mCountdownTimer:Landroid/os/CountDownTimer;

    return-void
.end method

.method static synthetic access$000(Lcom/validity/fingerprint/IdentifyActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/IdentifyActivity;->setResult(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/validity/fingerprint/IdentifyActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->handleNext()V

    return-void
.end method

.method static synthetic access$1000(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/os/PowerManager;
    .locals 1
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mPowerMgr:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 35
    sput p0, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    return p0
.end method

.method static synthetic access$200(Lcom/validity/fingerprint/IdentifyActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/validity/fingerprint/IdentifyActivity;)Lcom/validity/fingerprint/Fingerprint;
    .locals 1
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    return-object v0
.end method

.method static synthetic access$302(Lcom/validity/fingerprint/IdentifyActivity;Lcom/validity/fingerprint/Fingerprint;)Lcom/validity/fingerprint/Fingerprint;
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;
    .param p1, "x1"    # Lcom/validity/fingerprint/Fingerprint;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    return-object p1
.end method

.method static synthetic access$400(Lcom/validity/fingerprint/IdentifyActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/validity/fingerprint/IdentifyActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/IdentifyActivity;->setUiString(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/validity/fingerprint/IdentifyActivity;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$700(Lcom/validity/fingerprint/IdentifyActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->startIdentify()V

    return-void
.end method

.method static synthetic access$800(Lcom/validity/fingerprint/IdentifyActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->stopCountdownTimer()V

    return-void
.end method

.method static synthetic access$902(Lcom/validity/fingerprint/IdentifyActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;
    .locals 0
    .param p0, "x0"    # Lcom/validity/fingerprint/IdentifyActivity;
    .param p1, "x1"    # Landroid/os/CountDownTimer;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mCountdownTimer:Landroid/os/CountDownTimer;

    return-object p1
.end method

.method private cancel()V
    .locals 3

    .prologue
    .line 316
    const-string v1, "IdentifyActivity"

    const-string v2, "cancel()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 318
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->stopCountdownTimer()V

    .line 320
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/validity/fingerprint/IdentifyActivity$6;

    invoke-direct {v2, p0}, Lcom/validity/fingerprint/IdentifyActivity$6;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mThread:Ljava/lang/Thread;

    .line 333
    :try_start_0
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :goto_0
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 339
    return-void

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private getPrepareView()Landroid/view/View;
    .locals 26

    .prologue
    .line 140
    const/16 v20, 0x64

    .line 143
    .local v20, "vId":I
    new-instance v15, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 144
    .local v15, "root":Landroid/widget/RelativeLayout;
    invoke-virtual/range {p0 .. p0}, Lcom/validity/fingerprint/IdentifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x106000e

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getColor(I)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 147
    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x1

    const/16 v23, -0x2

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 150
    .local v18, "trlp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0x0

    const/16 v23, 0x5

    const/16 v24, 0x0

    const/16 v25, 0x5

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 151
    new-instance v9, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 152
    .local v9, "btnLayout":Landroid/widget/RelativeLayout;
    add-int/lit8 v21, v20, 0x1

    .end local v20    # "vId":I
    .local v21, "vId":I
    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 153
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    invoke-virtual {v15, v9}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 157
    new-instance v10, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, 0xc8

    const/16 v23, -0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 159
    .local v10, "cblp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0x9

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 160
    new-instance v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/validity/fingerprint/IdentifyActivity;->mCancelButton:Landroid/widget/Button;

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mCancelButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Cancel"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mCancelButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mCancelButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    add-int/lit8 v20, v21, 0x1

    .end local v21    # "vId":I
    .restart local v20    # "vId":I
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mCancelButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    new-instance v23, Lcom/validity/fingerprint/IdentifyActivity$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/validity/fingerprint/IdentifyActivity$1;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mCancelButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 172
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, 0xc8

    const/16 v23, -0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 174
    .local v5, "ablp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0xb

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 175
    new-instance v22, Landroid/widget/Button;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/validity/fingerprint/IdentifyActivity;->mAlternateButton:Landroid/widget/Button;

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mAlternateButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    const-string v23, "Alternate"

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mAlternateButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mAlternateButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "vId":I
    .restart local v21    # "vId":I
    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mAlternateButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    new-instance v23, Lcom/validity/fingerprint/IdentifyActivity$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/validity/fingerprint/IdentifyActivity$2;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mAlternateButton:Landroid/widget/Button;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 187
    new-instance v17, Landroid/view/View;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 188
    .local v17, "topSpacer":Landroid/view/View;
    new-instance v19, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x1

    const/16 v23, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 190
    .local v19, "tslp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0x3

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getId()I

    move-result v23

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 191
    const/16 v22, 0x0

    const/16 v23, 0x5

    const/16 v24, 0x0

    const/16 v25, 0x5

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 192
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    add-int/lit8 v20, v21, 0x1

    .end local v21    # "vId":I
    .restart local v20    # "vId":I
    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 194
    const v22, 0x1080014

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 195
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 198
    new-instance v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x2

    const/16 v23, -0x2

    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 203
    .local v16, "tolp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0xe

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 204
    const/16 v22, 0x3

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getId()I

    move-result v23

    move-object/from16 v0, v16

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "vId":I
    .restart local v21    # "vId":I
    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 210
    new-instance v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    new-instance v13, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x2

    const/16 v23, -0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 215
    .local v13, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0xe

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 216
    const/16 v22, 0xc

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 217
    const/16 v22, 0x0

    const/16 v23, 0x5

    const/16 v24, 0x0

    const/16 v25, 0x5

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, 0x1030044

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    add-int/lit8 v20, v21, 0x1

    .end local v21    # "vId":I
    .restart local v20    # "vId":I
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 224
    new-instance v7, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 225
    .local v7, "bottomSpacer":Landroid/view/View;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x1

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 227
    .local v8, "bslp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getId()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v8, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 228
    const/16 v22, 0x0

    const/16 v23, 0x5

    const/16 v24, 0x0

    const/16 v25, 0x5

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 229
    invoke-virtual {v7, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    add-int/lit8 v21, v20, 0x1

    .end local v20    # "vId":I
    .restart local v21    # "vId":I
    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setId(I)V

    .line 231
    const v22, 0x1080014

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 232
    invoke-virtual {v15, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 235
    const-class v22, Lcom/validity/fingerprint/IdentifyActivity;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v22

    const-string v23, "res/drawable/fingerprint.png"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 237
    .local v12, "is":Ljava/io/InputStream;
    if-eqz v12, :cond_0

    .line 238
    invoke-static {v12}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 240
    .local v6, "bmp":Landroid/graphics/Bitmap;
    new-instance v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 241
    .local v14, "mImageFP":Landroid/widget/ImageView;
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v22, -0x2

    const/16 v23, -0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 244
    .local v11, "ilp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v22, 0xe

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 245
    const/16 v22, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mTimeoutText:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getId()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 246
    const/16 v22, 0x2

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 247
    const/16 v22, 0xa

    const/16 v23, 0xa

    const/16 v24, 0xa

    const/16 v25, 0xa

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 248
    invoke-virtual {v14, v11}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    add-int/lit8 v20, v21, 0x1

    .end local v21    # "vId":I
    .restart local v20    # "vId":I
    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setId(I)V

    .line 250
    sget-object v22, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 251
    invoke-virtual {v14, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 252
    invoke-virtual {v15, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 256
    .end local v6    # "bmp":Landroid/graphics/Bitmap;
    .end local v11    # "ilp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v14    # "mImageFP":Landroid/widget/ImageView;
    :goto_0
    new-instance v22, Lcom/validity/fingerprint/SensorView;

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/validity/fingerprint/SensorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/validity/fingerprint/IdentifyActivity;->mSensorView:Lcom/validity/fingerprint/SensorView;

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/validity/fingerprint/IdentifyActivity;->mSensorView:Lcom/validity/fingerprint/SensorView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 259
    return-object v15

    .end local v20    # "vId":I
    .restart local v21    # "vId":I
    :cond_0
    move/from16 v20, v21

    .end local v21    # "vId":I
    .restart local v20    # "vId":I
    goto :goto_0
.end method

.method private handleAttemptLockout(J)V
    .locals 2
    .param p1, "elapsedRealtimeDeadline"    # J

    .prologue
    .line 503
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/validity/fingerprint/IdentifyActivity$8;

    invoke-direct {v1, p0}, Lcom/validity/fingerprint/IdentifyActivity$8;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 525
    return-void
.end method

.method private handleNext()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 444
    const-string v7, "IdentifyActivity"

    const-string v8, "handleNext()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    const/4 v1, 0x0

    .line 449
    .local v1, "altUnlockMethod":I
    :try_start_0
    const-class v7, Lcom/android/internal/widget/LockPatternUtils;

    const-string v8, "getAltUnlockMethod"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 451
    .local v4, "methodAltUnlock":Ljava/lang/reflect/Method;
    if-eqz v4, :cond_0

    .line 452
    iget-object v7, p0, Lcom/validity/fingerprint/IdentifyActivity;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v4, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 453
    .local v6, "value":Ljava/lang/Object;
    check-cast v6, Ljava/lang/Integer;

    .end local v6    # "value":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 460
    :cond_0
    if-nez v1, :cond_2

    .line 461
    const-string v7, "Alternate unlock method was not set!"

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 500
    .end local v4    # "methodAltUnlock":Ljava/lang/reflect/Method;
    :cond_1
    :goto_0
    return-void

    .line 455
    :catch_0
    move-exception v2

    .line 456
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 466
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v4    # "methodAltUnlock":Ljava/lang/reflect/Method;
    :cond_2
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->cancel()V

    .line 469
    const-string v0, ""

    .line 470
    .local v0, "activityClassName":Ljava/lang/String;
    sparse-switch v1, :sswitch_data_0

    .line 483
    :goto_1
    const-string v7, ""

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 484
    invoke-virtual {p0}, Lcom/validity/fingerprint/IdentifyActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.android.settings"

    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 485
    .local v3, "intent":Landroid/content/Intent;
    const-string v7, "com.android.settings"

    invoke-virtual {v3, v7, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 486
    const/high16 v7, 0x20000000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 489
    const/16 v7, 0x64

    :try_start_1
    invoke-virtual {p0, v3, v7}, Lcom/validity/fingerprint/IdentifyActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 490
    :catch_1
    move-exception v5

    .line 491
    .local v5, "se":Ljava/lang/SecurityException;
    const-string v7, "IdentifyActivity"

    invoke-virtual {v5}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    const-string v7, "Alternate unlock screen doesn\'t have permission to launch from external app."

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 473
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "se":Ljava/lang/SecurityException;
    :sswitch_0
    const-string v0, "com.android.settings.ConfirmLockPattern"

    .line 474
    goto :goto_1

    .line 478
    :sswitch_1
    const-string v0, "com.android.settings.ConfirmLockPassword"

    goto :goto_1

    .line 496
    .restart local v3    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v2

    .line 497
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v7, "IdentifyActivity"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 470
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x40000 -> :sswitch_1
        0x50000 -> :sswitch_1
    .end sparse-switch
.end method

.method private setResult(II)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "identifyResult"    # I

    .prologue
    .line 437
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 438
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "result"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 439
    invoke-virtual {p0, p1, v0}, Lcom/validity/fingerprint/IdentifyActivity;->setResult(ILandroid/content/Intent;)V

    .line 440
    invoke-virtual {p0}, Lcom/validity/fingerprint/IdentifyActivity;->finish()V

    .line 441
    return-void
.end method

.method private setUiString(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 263
    move-object v0, p1

    .line 264
    .local v0, "s":Ljava/lang/String;
    const-string v1, "IdentifyActivity"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    new-instance v1, Lcom/validity/fingerprint/IdentifyActivity$3;

    invoke-direct {v1, p0, v0}, Lcom/validity/fingerprint/IdentifyActivity$3;-><init>(Lcom/validity/fingerprint/IdentifyActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/validity/fingerprint/IdentifyActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 270
    return-void
.end method

.method private startIdentify()V
    .locals 3

    .prologue
    .line 273
    const-string v1, "IdentifyActivity"

    const-string v2, "startIdentify()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/validity/fingerprint/IdentifyActivity$4;

    invoke-direct {v2, p0}, Lcom/validity/fingerprint/IdentifyActivity$4;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mThread:Ljava/lang/Thread;

    .line 297
    :try_start_0
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :goto_0
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 303
    return-void

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private startIdentifyWithDelay()V
    .locals 4

    .prologue
    .line 306
    const-string v0, "IdentifyActivity"

    const-string v1, "startIdentifyWithDelay()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 308
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/validity/fingerprint/IdentifyActivity$5;

    invoke-direct {v1, p0}, Lcom/validity/fingerprint/IdentifyActivity$5;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 313
    return-void
.end method

.method private stopCountdownTimer()V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/validity/fingerprint/IdentifyActivity;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 531
    :cond_0
    const/4 v0, 0x0

    sput v0, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    .line 532
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, -0x1

    .line 130
    const-string v0, "IdentifyActivity"

    const-string v1, "onActivityResult()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 132
    if-ne p2, v2, :cond_0

    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/validity/fingerprint/IdentifyActivity;->setResult(II)V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    const/16 v0, 0x169

    invoke-direct {p0, p2, v0}, Lcom/validity/fingerprint/IdentifyActivity;->setResult(II)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "IdentifyActivity"

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/4 v0, 0x0

    const/16 v1, 0x169

    invoke-direct {p0, v0, v1}, Lcom/validity/fingerprint/IdentifyActivity;->setResult(II)V

    .line 126
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    const-string v2, "IdentifyActivity"

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->getPrepareView()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/validity/fingerprint/IdentifyActivity;->mActivityView:Landroid/view/View;

    .line 70
    iget-object v2, p0, Lcom/validity/fingerprint/IdentifyActivity;->mActivityView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/validity/fingerprint/IdentifyActivity;->setContentView(Landroid/view/View;)V

    .line 71
    const-string v2, "Identify"

    invoke-virtual {p0, v2}, Lcom/validity/fingerprint/IdentifyActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 73
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/validity/fingerprint/IdentifyActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/validity/fingerprint/IdentifyActivity;->mPowerMgr:Landroid/os/PowerManager;

    .line 74
    new-instance v2, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v2, p0}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/validity/fingerprint/IdentifyActivity;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 76
    invoke-virtual {p0}, Lcom/validity/fingerprint/IdentifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 77
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 78
    const-string v2, "userId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "userId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 80
    iput-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mUserId:Ljava/lang/String;

    .line 83
    .end local v1    # "userId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 118
    const-string v0, "IdentifyActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 120
    return-void
.end method

.method public onEvent(Lcom/validity/fingerprint/FingerprintEvent;)V
    .locals 8
    .param p1, "fpEvent"    # Lcom/validity/fingerprint/FingerprintEvent;

    .prologue
    .line 353
    if-nez p1, :cond_1

    .line 354
    const-string v3, "IdentifyActivity"

    const-string v4, "Invalid event data."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    const-string v3, "IdentifyActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onEventsCB(): id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string v2, ""

    .line 362
    .local v2, "statusMessage":Ljava/lang/String;
    iget v3, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    sparse-switch v3, :sswitch_data_0

    .line 430
    :cond_2
    :goto_1
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 431
    invoke-direct {p0, v2}, Lcom/validity/fingerprint/IdentifyActivity;->setUiString(Ljava/lang/String;)V

    goto :goto_0

    .line 364
    :sswitch_0
    sget-object v2, Lcom/validity/fingerprint/IdentifyActivity;->FINGER_ACTION_LABEL:Ljava/lang/String;

    .line 365
    goto :goto_1

    .line 368
    :sswitch_1
    const-string v2, "Captured fingerprint"

    .line 369
    goto :goto_1

    .line 372
    :sswitch_2
    const-string v3, "IdentifyActivity"

    const-string v4, "VCS_EVT_VERIFY_SUCCESS"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :sswitch_3
    const-string v3, "IdentifyActivity"

    const-string v4, "VCS_EVT_IDENTIFY_SUCCESS"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    const-string v2, "Identify success, User found."

    .line 377
    const/4 v3, 0x0

    sput v3, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    .line 378
    iget-object v3, p0, Lcom/validity/fingerprint/IdentifyActivity;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/validity/fingerprint/IdentifyActivity$7;

    invoke-direct {v4, p0}, Lcom/validity/fingerprint/IdentifyActivity$7;-><init>(Lcom/validity/fingerprint/IdentifyActivity;)V

    const-wide/16 v6, 0x1f4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 386
    :sswitch_4
    const-string v3, "IdentifyActivity"

    const-string v4, "VCS_EVT_VERIFY_FAILED"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :sswitch_5
    const-string v3, "IdentifyActivity"

    const-string v4, "VCS_EVT_IDENTIFY_FAILED"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    const/4 v1, 0x0

    .line 393
    .local v1, "reverify":Z
    const/4 v0, -0x1

    .line 394
    .local v0, "opResult":I
    iget-object v3, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    instance-of v3, v3, Ljava/lang/Integer;

    if-eqz v3, :cond_3

    .line 395
    iget-object v3, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 398
    :cond_3
    sparse-switch v0, :sswitch_data_1

    .line 412
    const-string v2, "Identify failed, User not found."

    .line 413
    sget v3, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    .line 414
    sget v3, Lcom/validity/fingerprint/IdentifyActivity;->mTotalFailedPatternAttempts:I

    const/4 v4, 0x5

    if-lt v3, v4, :cond_4

    .line 415
    const-wide/16 v4, 0x7530

    invoke-direct {p0, v4, v5}, Lcom/validity/fingerprint/IdentifyActivity;->handleAttemptLockout(J)V

    .line 419
    :goto_2
    const-string v3, "IdentifyActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fingerprint verify failed, Result: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :goto_3
    if-eqz v1, :cond_2

    .line 424
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->startIdentifyWithDelay()V

    goto/16 :goto_1

    .line 400
    :sswitch_6
    const-string v2, "Operation has been cancelled"

    .line 401
    goto :goto_3

    .line 403
    :sswitch_7
    const-string v2, "Sensor has been removed"

    .line 404
    const/4 v1, 0x1

    .line 405
    goto :goto_3

    .line 407
    :sswitch_8
    const-string v2, "Sensor not found"

    .line 408
    const/4 v1, 0x1

    .line 409
    goto :goto_3

    .line 417
    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    .line 362
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x11 -> :sswitch_1
        0x1a6 -> :sswitch_2
        0x1a7 -> :sswitch_3
        0x1a9 -> :sswitch_4
        0x1aa -> :sswitch_5
    .end sparse-switch

    .line 398
    :sswitch_data_1
    .sparse-switch
        0x13 -> :sswitch_8
        0x168 -> :sswitch_7
        0x169 -> :sswitch_6
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 111
    const-string v0, "IdentifyActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 113
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->cancel()V

    .line 114
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 87
    const-string v1, "IdentifyActivity"

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 90
    invoke-static {}, Lcom/validity/fingerprint/ConfigReader;->getData()Lcom/validity/fingerprint/ConfigReader$ConfigData;

    move-result-object v0

    .line 91
    .local v0, "configData":Lcom/validity/fingerprint/ConfigReader$ConfigData;
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity;->mSensorView:Lcom/validity/fingerprint/SensorView;

    invoke-virtual {v1, v0}, Lcom/validity/fingerprint/SensorView;->setLocation(Lcom/validity/fingerprint/ConfigReader$ConfigData;)Z

    .line 93
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerActionGenericLabel:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerActionGenericLabel:Ljava/lang/String;

    sput-object v1, Lcom/validity/fingerprint/IdentifyActivity;->FINGER_ACTION_LABEL:Ljava/lang/String;

    .line 106
    :cond_0
    invoke-direct {p0}, Lcom/validity/fingerprint/IdentifyActivity;->startIdentify()V

    .line 107
    return-void
.end method
