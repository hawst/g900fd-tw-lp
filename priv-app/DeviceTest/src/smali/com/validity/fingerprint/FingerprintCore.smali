.class public Lcom/validity/fingerprint/FingerprintCore;
.super Lcom/validity/fingerprint/VcsEvents;
.source "FingerprintCore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/validity/fingerprint/FingerprintCore$EventListener;
    }
.end annotation


# static fields
.field public static final API_VERSION:Ljava/lang/String; = "0.7"

.field protected static final DBG:Z = false

.field protected static final ENROLL:I = 0x97

.field protected static final GETDATAFROMUSER:I = 0x9a

.field protected static final GETPRINT:I = 0x9c

.field protected static final IDENTIFY:I = 0x98

.field protected static final IDLE:I = 0x96

.field protected static final REMOVEDATAFROMUSER:I = 0x9b

.field protected static final STOREDATATOUSER:I = 0x99

.field protected static final TAG:Ljava/lang/String; = "Fingerprint"

.field protected static final USER_ID:Ljava/lang/String; = "android"

.field public static final VCS_POLICY_VERSION:Ljava/lang/String; = "0.1"


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mAppKey:Ljava/lang/String;

.field private mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

.field private mKeyData:Ljava/lang/String;

.field protected mOperation:I

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 420
    :try_start_0
    const-string v1, "vcsfp"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    .local v0, "e":Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 421
    .end local v0    # "e":Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 422
    .restart local v0    # "e":Ljava/lang/Throwable;
    const-string v1, "Fingerprint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error loading library libvcsfp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/validity/fingerprint/VcsEvents;-><init>()V

    .line 51
    const/16 v0, 0x96

    iput v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mAppId:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mUserId:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mAppKey:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mKeyData:Ljava/lang/String;

    .line 62
    invoke-direct {p0, p0}, Lcom/validity/fingerprint/FingerprintCore;->jniInitVcs(Lcom/validity/fingerprint/FingerprintCore;)I

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/validity/fingerprint/FingerprintCore$EventListener;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/validity/fingerprint/VcsEvents;-><init>()V

    .line 51
    const/16 v0, 0x96

    iput v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mAppId:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mUserId:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mAppKey:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mKeyData:Ljava/lang/String;

    .line 66
    invoke-direct {p0, p0}, Lcom/validity/fingerprint/FingerprintCore;->jniInitVcs(Lcom/validity/fingerprint/FingerprintCore;)I

    .line 67
    iput-object p2, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .line 68
    return-void
.end method

.method private isOperationComplete(I)Z
    .locals 1
    .param p1, "eventId"    # I

    .prologue
    .line 392
    const/16 v0, 0x1a5

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a7

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a6

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a8

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1aa

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a9

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1ab

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native jniCancelOp()I
.end method

.method private native jniCleanupVcs()I
.end method

.method private native jniGetEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I
.end method

.method private native jniGetFingerprintImage()I
.end method

.method private native jniGetFingerprintImageEx(Ljava/lang/Object;)I
.end method

.method private native jniGetUserList(Lcom/validity/fingerprint/VcsInt;)[Ljava/lang/String;
.end method

.method private native jniGetVersion()Ljava/lang/String;
.end method

.method private native jniIdentify(Ljava/lang/String;)I
.end method

.method private native jniInitVcs()I
.end method

.method private native jniInitVcs(Lcom/validity/fingerprint/FingerprintCore;)I
.end method

.method private native jniProtect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I
.end method

.method private native jniRegisterEventsCB(Lcom/validity/fingerprint/FingerprintCore;)I
.end method

.method private native jniSetDetectFinger(I)I
.end method

.method private native jniSetSecurityLevel(I)I
.end method

.method private native jniUnprotect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I
.end method


# virtual methods
.method public declared-synchronized FingerprintEventCallback(Lcom/validity/fingerprint/FingerprintEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/validity/fingerprint/FingerprintEvent;

    .prologue
    .line 360
    monitor-enter p0

    if-nez p1, :cond_1

    .line 361
    :try_start_0
    const-string v0, "Fingerprint"

    const-string v1, "FP - EventsCB()::Invalid event data!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 366
    :cond_1
    :try_start_1
    iget v0, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    invoke-direct {p0, v0}, Lcom/validity/fingerprint/FingerprintCore;->isOperationComplete(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    const-string v0, "Fingerprint"

    const-string v1, "Operation complete, setting to IDLE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/16 v0, 0x96

    iput v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    .line 372
    :cond_2
    iget v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v1, 0x9c

    if-ne v0, v1, :cond_4

    iget v0, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    iget v0, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_4

    .line 376
    :cond_3
    const/16 v0, 0x96

    iput v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    .line 379
    :cond_4
    iget-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    invoke-interface {v0, p1}, Lcom/validity/fingerprint/FingerprintCore$EventListener;->onEvent(Lcom/validity/fingerprint/FingerprintEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cancel()I
    .locals 4

    .prologue
    const/16 v3, 0x96

    .line 287
    const/4 v0, -0x1

    .line 289
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    if-ne v2, v3, :cond_0

    move v1, v0

    .line 295
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 291
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    invoke-direct {p0}, Lcom/validity/fingerprint/FingerprintCore;->jniCancelOp()I

    move-result v0

    .line 293
    iput v3, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    move v1, v0

    .line 295
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public cleanUp()I
    .locals 2

    .prologue
    .line 306
    const/4 v0, -0x1

    .line 309
    .local v0, "ret":I
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .line 311
    invoke-direct {p0}, Lcom/validity/fingerprint/FingerprintCore;->jniCleanupVcs()I

    move-result v0

    .line 313
    return v0
.end method

.method public getEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "fingermask"    # Lcom/validity/fingerprint/VcsInt;

    .prologue
    .line 272
    if-nez p1, :cond_0

    const-string p1, ""

    .line 274
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/FingerprintCore;->jniGetEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I

    move-result v0

    return v0
.end method

.method public getFingerprintImage()I
    .locals 4

    .prologue
    .line 199
    const/4 v0, -0x1

    .line 201
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 209
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 203
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    invoke-direct {p0}, Lcom/validity/fingerprint/FingerprintCore;->jniGetFingerprintImage()I

    move-result v0

    .line 205
    if-nez v0, :cond_1

    .line 206
    const/16 v2, 0x9c

    iput v2, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    :cond_1
    move v1, v0

    .line 209
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public getFingerprintImage(Lcom/validity/fingerprint/ConsumerInfo;)I
    .locals 4
    .param p1, "info"    # Lcom/validity/fingerprint/ConsumerInfo;

    .prologue
    .line 226
    const/4 v0, -0x1

    .line 228
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 238
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 230
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    if-nez p1, :cond_1

    const-string v2, "Fingerprint"

    const-string v3, "ConsumerInfo is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_1
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/FingerprintCore;->jniGetFingerprintImageEx(Ljava/lang/Object;)I

    move-result v0

    .line 234
    if-nez v0, :cond_2

    .line 235
    const/16 v2, 0x9c

    iput v2, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    :cond_2
    move v1, v0

    .line 238
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public getUserList(Lcom/validity/fingerprint/VcsStringArray;)I
    .locals 2
    .param p1, "userList"    # Lcom/validity/fingerprint/VcsStringArray;

    .prologue
    .line 250
    new-instance v0, Lcom/validity/fingerprint/VcsInt;

    invoke-direct {v0}, Lcom/validity/fingerprint/VcsInt;-><init>()V

    .line 251
    .local v0, "retCode":Lcom/validity/fingerprint/VcsInt;
    invoke-direct {p0, v0}, Lcom/validity/fingerprint/FingerprintCore;->jniGetUserList(Lcom/validity/fingerprint/VcsInt;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    .line 252
    iget-object v1, p1, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    if-nez v1, :cond_0

    .line 253
    iget v1, v0, Lcom/validity/fingerprint/VcsInt;->num:I

    .line 255
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    iget v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 81
    const-string v0, "Fingerprint"

    const-string v1, "Other operation is in progress, cancelling request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/validity/fingerprint/FingerprintCore;->jniGetVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public identify(Ljava/lang/String;)I
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x98

    .line 153
    const/4 v0, -0x1

    .line 155
    .local v0, "ret":I
    iget v1, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    if-ne v1, v3, :cond_0

    const/16 v1, 0x3ec

    .line 167
    :goto_0
    return v1

    .line 157
    :cond_0
    iget v1, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v2, 0x96

    if-eq v1, v2, :cond_1

    move v1, v0

    goto :goto_0

    .line 159
    :cond_1
    if-nez p1, :cond_2

    const-string p1, ""

    .line 161
    :cond_2
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/FingerprintCore;->jniIdentify(Ljava/lang/String;)I

    move-result v0

    .line 163
    if-nez v0, :cond_3

    .line 164
    iput v3, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    :cond_3
    move v1, v0

    .line 167
    goto :goto_0
.end method

.method public protect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I
    .locals 1
    .param p1, "toProtect"    # [B
    .param p2, "addInfo"    # Lcom/validity/fingerprint/VcsAddInfo;
    .param p3, "protectedData"    # Lcom/validity/fingerprint/VcsByteArray;

    .prologue
    .line 328
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 329
    :cond_0
    const/16 v0, 0x3ee

    .line 331
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/validity/fingerprint/FingerprintCore;->jniProtect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I

    move-result v0

    goto :goto_0
.end method

.method public registerListener(Lcom/validity/fingerprint/FingerprintCore$EventListener;)I
    .locals 2
    .param p1, "listener"    # Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .prologue
    .line 96
    iget v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    const/4 v0, -0x1

    .line 100
    :goto_0
    return v0

    .line 98
    :cond_0
    iput-object p1, p0, Lcom/validity/fingerprint/FingerprintCore;->mEventListener:Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .line 100
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDetectFinger(I)I
    .locals 1
    .param p1, "timeout"    # I

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/FingerprintCore;->jniSetDetectFinger(I)I

    move-result v0

    return v0
.end method

.method public setSecurityLevel(I)I
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 117
    iget v0, p0, Lcom/validity/fingerprint/FingerprintCore;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 118
    const/4 v0, -0x1

    .line 127
    :goto_0
    return v0

    .line 121
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 125
    :cond_1
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/FingerprintCore;->jniSetSecurityLevel(I)I

    move-result v0

    goto :goto_0

    .line 127
    :cond_2
    const/16 v0, 0x3ee

    goto :goto_0
.end method

.method public unProtect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I
    .locals 1
    .param p1, "protectedData"    # [B
    .param p2, "addInfo"    # Lcom/validity/fingerprint/VcsAddInfo;
    .param p3, "unProtectedData"    # Lcom/validity/fingerprint/VcsByteArray;

    .prologue
    .line 342
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 343
    :cond_0
    const/16 v0, 0x3ee

    .line 350
    :goto_0
    return v0

    .line 346
    :cond_1
    if-nez p2, :cond_2

    .line 347
    new-instance p2, Lcom/validity/fingerprint/VcsAddInfo;

    .end local p2    # "addInfo":Lcom/validity/fingerprint/VcsAddInfo;
    invoke-direct {p2}, Lcom/validity/fingerprint/VcsAddInfo;-><init>()V

    .line 350
    .restart local p2    # "addInfo":Lcom/validity/fingerprint/VcsAddInfo;
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/validity/fingerprint/FingerprintCore;->jniUnprotect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I

    move-result v0

    goto :goto_0
.end method
