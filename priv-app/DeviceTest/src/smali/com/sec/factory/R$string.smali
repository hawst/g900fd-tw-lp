.class public final Lcom/sec/factory/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ATM:I = 0x7f06019a

.field public static final BIST:I = 0x7f060081

.field public static final Bin_Firm_Version:I = 0x7f060138

.field public static final CALIB:I = 0x7f060071

.field public static final DMB_Detach_Test:I = 0x7f06013a

.field public static final Dockchargecheck_title:I = 0x7f060198

.field public static final ELECTRIC_SHOCK:I = 0x7f0601cb

.field public static final EXIT:I = 0x7f060051

.field public static final FAIL:I = 0x7f06008e

.field public static final FIFO_Test:I = 0x7f06006b

.field public static final FirmwareCheck:I = 0x7f06012d

.field public static final GYROSCOPE_Sensor:I = 0x7f06006f

.field public static final INT_Check:I = 0x7f060135

.field public static final Initialized:I = 0x7f060072

.field public static final MAX:I = 0x7f06012c

.field public static final MCU:I = 0x7f060139

.field public static final MHL_Version_Check:I = 0x7f060126

.field public static final MIN:I = 0x7f06012b

.field public static final Mcu_Firm_Version:I = 0x7f060137

.field public static final PASS:I = 0x7f06008d

.field public static final Panel_Firm_Version:I = 0x7f06012f

.field public static final Phone_Firm_Version:I = 0x7f06012e

.field public static final RATE:I = 0x7f060082

.field public static final RESULT:I = 0x7f060083

.field public static final RETRY:I = 0x7f06008f

.field public static final Reference_Test_Fail:I = 0x7f06012a

.field public static final Reset:I = 0x7f06019d

.field public static final SPDIF_Audio_Test:I = 0x7f0600fb

.field public static final SPDIF_result_text:I = 0x7f0600fc

.field public static final SensorHubFirmwareCheck:I = 0x7f060136

.field public static final SensorHub_Test:I = 0x7f060134

.field public static final TEST_RESULT:I = 0x7f0601cc

.field public static final TSP:I = 0x7f060130

.field public static final TSP_SPEC:I = 0x7f060131

.field public static final Temperature:I = 0x7f060073

.field public static final VALUE:I = 0x7f060080

.field public static final Wireless_Battery_Test:I = 0x7f060124

.field public static final X:I = 0x7f06008a

.field public static final XYZ:I = 0x7f060077

.field public static final XYZ_DIFF:I = 0x7f060079

.field public static final XYZ_PRIME:I = 0x7f060078

.field public static final XYZ_ZERO_RATE:I = 0x7f060070

.field public static final X_PRIME:I = 0x7f060074

.field public static final Y:I = 0x7f06008b

.field public static final Y_PRIME:I = 0x7f060075

.field public static final Z:I = 0x7f06008c

.field public static final Z_PRIME:I = 0x7f060076

.field public static final acc_cover_path:I = 0x7f0601d3

.field public static final acc_cover_short:I = 0x7f0601d4

.field public static final acc_cover_title:I = 0x7f0601d2

.field public static final acc_cover_wait:I = 0x7f0601d5

.field public static final acc_value:I = 0x7f060153

.field public static final adc:I = 0x7f0600f3

.field public static final afchargecheck_title:I = 0x7f060196

.field public static final ant_close:I = 0x7f06013c

.field public static final ant_open:I = 0x7f06013b

.field public static final app_name:I = 0x7f060000

.field public static final avg:I = 0x7f060197

.field public static final barometer_altitude:I = 0x7f06001d

.field public static final barometer_pressure:I = 0x7f06001c

.field public static final barometer_result:I = 0x7f06001e

.field public static final barometer_temp:I = 0x7f06001b

.field public static final barometer_title:I = 0x7f06001a

.field public static final barometer_waterproof_title:I = 0x7f060199

.field public static final btn_start:I = 0x7f060013

.field public static final btn_stop:I = 0x7f060014

.field public static final button_blue_text:I = 0x7f060141

.field public static final button_exit_text:I = 0x7f060143

.field public static final button_green_text:I = 0x7f060140

.field public static final button_red_text:I = 0x7f06013f

.field public static final button_reset_text:I = 0x7f060142

.field public static final buttons_accuracy_pressure:I = 0x7f06010e

.field public static final buttons_linearity:I = 0x7f06010f

.field public static final buttons_linetest:I = 0x7f060110

.field public static final buttons_pba:I = 0x7f060122

.field public static final buttons_smd:I = 0x7f060121

.field public static final channel_up_down:I = 0x7f060114

.field public static final check_svc_led:I = 0x7f06013d

.field public static final check_svc_led_random:I = 0x7f06013e

.field public static final comp_value:I = 0x7f060155

.field public static final cp_unknown:I = 0x7f060005

.field public static final default_string:I = 0x7f060004

.field public static final diff_p1:I = 0x7f0601a0

.field public static final diff_p2:I = 0x7f0601a1

.field public static final down:I = 0x7f060116

.field public static final dump:I = 0x7f060194

.field public static final earphone:I = 0x7f060190

.field public static final earphone_key:I = 0x7f060192

.field public static final ecg_fw_version:I = 0x7f06016e

.field public static final ecg_title:I = 0x7f06016d

.field public static final echo_test_meas:I = 0x7f0601b6

.field public static final echo_test_mic1:I = 0x7f0601b2

.field public static final echo_test_mic2:I = 0x7f0601b3

.field public static final echo_test_mic3:I = 0x7f0601b4

.field public static final echo_test_spec:I = 0x7f0601b5

.field public static final epen_main_title:I = 0x7f06010d

.field public static final ese_test:I = 0x7f0600e2

.field public static final exit:I = 0x7f06018a

.field public static final factory_format:I = 0x7f06000d

.field public static final felica_detecting_tag:I = 0x7f0600e6

.field public static final felica_reader_mode_test:I = 0x7f0600e5

.field public static final felica_test:I = 0x7f0600e4

.field public static final file_copy_text:I = 0x7f0600fa

.field public static final fingerprint_P:I = 0x7f06015e

.field public static final fingerprint_S:I = 0x7f06015f

.field public static final fingerprint_communication_msg:I = 0x7f060165

.field public static final fingerprint_data_max:I = 0x7f060161

.field public static final fingerprint_data_min:I = 0x7f060160

.field public static final fingerprint_stdev_avg:I = 0x7f060164

.field public static final fingerprint_stdev_max:I = 0x7f060163

.field public static final fingerprint_stdev_min:I = 0x7f060162

.field public static final fingerprint_test:I = 0x7f06015d

.field public static final format_caution:I = 0x7f06000f

.field public static final format_caution2:I = 0x7f060010

.field public static final format_info:I = 0x7f06000e

.field public static final gesture_title:I = 0x7f060011

.field public static final glucose:I = 0x7f0601d9

.field public static final glucose_detect:I = 0x7f0601d8

.field public static final glucose_sensor:I = 0x7f0601d6

.field public static final glucose_serial_number:I = 0x7f0601d7

.field public static final grip1:I = 0x7f06002d

.field public static final grip2:I = 0x7f06002e

.field public static final grip_init_working:I = 0x7f06002b

.field public static final grip_normal_working:I = 0x7f06002c

.field public static final grip_release:I = 0x7f060029

.field public static final grip_sens:I = 0x7f060028

.field public static final grip_sens1:I = 0x7f060025

.field public static final grip_sens2:I = 0x7f060026

.field public static final grip_sens3:I = 0x7f060027

.field public static final grip_title:I = 0x7f060024

.field public static final grip_working:I = 0x7f06002a

.field public static final gripsensor_Diff_str:I = 0x7f060052

.field public static final gripsensor_Offset_str:I = 0x7f060053

.field public static final gripsensor_RAW_Count_str:I = 0x7f060032

.field public static final gripsensor_SPEC_str:I = 0x7f060033

.field public static final gripsensor_cal:I = 0x7f060037

.field public static final gripsensor_cal_erase:I = 0x7f060038

.field public static final gripsensor_cal_menu:I = 0x7f060047

.field public static final gripsensor_cal_menu_1:I = 0x7f060048

.field public static final gripsensor_cal_menu_2:I = 0x7f060049

.field public static final gripsensor_cal_result:I = 0x7f060040

.field public static final gripsensor_cal_skip:I = 0x7f060039

.field public static final gripsensor_caldata_str:I = 0x7f060036

.field public static final gripsensor_cap_main:I = 0x7f06004c

.field public static final gripsensor_click_next:I = 0x7f060056

.field public static final gripsensor_completed:I = 0x7f060055

.field public static final gripsensor_cspercent_str:I = 0x7f060034

.field public static final gripsensor_initial_test:I = 0x7f06004a

.field public static final gripsensor_initial_thd:I = 0x7f06003c

.field public static final gripsensor_initial_thd_1:I = 0x7f06003d

.field public static final gripsensor_initial_thd_2:I = 0x7f06003e

.field public static final gripsensor_max:I = 0x7f060046

.field public static final gripsensor_min:I = 0x7f060045

.field public static final gripsensor_normal_thd:I = 0x7f06003f

.field public static final gripsensor_on:I = 0x7f06003a

.field public static final gripsensor_prox_offset:I = 0x7f06004e

.field public static final gripsensor_prox_useful:I = 0x7f06004d

.field public static final gripsensor_proxpercent_str:I = 0x7f060035

.field public static final gripsensor_raw_data:I = 0x7f060041

.field public static final gripsensor_raw_data_1:I = 0x7f060042

.field public static final gripsensor_raw_data_2:I = 0x7f060043

.field public static final gripsensor_real:I = 0x7f060044

.field public static final gripsensor_reset:I = 0x7f060050

.field public static final gripsensor_result:I = 0x7f06003b

.field public static final gripsensor_test_mode:I = 0x7f06004b

.field public static final gyro_disp_title_sub:I = 0x7f06006e

.field public static final gyro_display:I = 0x7f06007e

.field public static final gyro_main_title:I = 0x7f06006d

.field public static final gyro_self:I = 0x7f06006c

.field public static final gyroscope_bias_dps:I = 0x7f060066

.field public static final gyroscope_diff:I = 0x7f060060

.field public static final gyroscope_fifo:I = 0x7f060058

.field public static final gyroscope_hwself:I = 0x7f060063

.field public static final gyroscope_initialized:I = 0x7f060059

.field public static final gyroscope_intial_bias:I = 0x7f06005f

.field public static final gyroscope_no_selftest:I = 0x7f060064

.field public static final gyroscope_noise_bias:I = 0x7f06005e

.field public static final gyroscope_noise_power:I = 0x7f060061

.field public static final gyroscope_packet_cnt:I = 0x7f060067

.field public static final gyroscope_result:I = 0x7f060068

.field public static final gyroscope_rms:I = 0x7f060062

.field public static final gyroscope_selftest:I = 0x7f060065

.field public static final gyroscope_temperature:I = 0x7f06005a

.field public static final gyroscope_title:I = 0x7f060057

.field public static final gyroscope_value:I = 0x7f06007d

.field public static final gyroscope_x:I = 0x7f06005b

.field public static final gyroscope_y:I = 0x7f06005c

.field public static final gyroscope_z:I = 0x7f06005d

.field public static final hallic_1st:I = 0x7f0600db

.field public static final hallic_2nd:I = 0x7f0600dc

.field public static final hallic_folder_close:I = 0x7f0600d6

.field public static final hallic_folder_open:I = 0x7f0600d5

.field public static final hallic_test_text:I = 0x7f0600d9

.field public static final health_cover_path:I = 0x7f060188

.field public static final health_cover_short:I = 0x7f060189

.field public static final health_cover_title:I = 0x7f060186

.field public static final health_cover_wait:I = 0x7f060187

.field public static final hrm_btn_exit:I = 0x7f06017e

.field public static final hrm_btn_start:I = 0x7f06017d

.field public static final hrm_common_ir:I = 0x7f060172

.field public static final hrm_common_item:I = 0x7f060171

.field public static final hrm_common_red:I = 0x7f060173

.field public static final hrm_common_result:I = 0x7f060174

.field public static final hrm_common_title:I = 0x7f06016f

.field public static final hrm_eoltest_title:I = 0x7f060170

.field public static final hrm_frequency_check:I = 0x7f060179

.field public static final hrm_frequency_dc_level:I = 0x7f06017b

.field public static final hrm_frequency_noise:I = 0x7f06017c

.field public static final hrm_frequency_sample_no:I = 0x7f06017a

.field public static final hrm_peak_to_peak_check_title:I = 0x7f060175

.field public static final hrm_peak_to_peak_dc_level:I = 0x7f060177

.field public static final hrm_peak_to_peak_peak:I = 0x7f060176

.field public static final hrm_peak_to_peak_ratio:I = 0x7f060178

.field public static final humitemp_title:I = 0x7f060152

.field public static final hw_self_dps:I = 0x7f060086

.field public static final id_check_title:I = 0x7f06011f

.field public static final id_detect_check_title:I = 0x7f06011d

.field public static final id_detect_title:I = 0x7f06011e

.field public static final idcheck_title:I = 0x7f06011c

.field public static final ir_thermo_celius:I = 0x7f0601c4

.field public static final ir_thermo_exit:I = 0x7f0601c7

.field public static final ir_thermo_pgm:I = 0x7f0601c8

.field public static final ir_thermo_pgm_mode_text1:I = 0x7f0601c9

.field public static final ir_thermo_pgm_mode_text2:I = 0x7f0601ca

.field public static final ir_thermo_plus_minus:I = 0x7f0601c5

.field public static final ir_thermo_spec:I = 0x7f0601c3

.field public static final ir_thermo_test:I = 0x7f0601c6

.field public static final ir_thermo_title:I = 0x7f0601c2

.field public static final ired_off:I = 0x7f060119

.field public static final ired_on:I = 0x7f060118

.field public static final ired_repeat:I = 0x7f06011a

.field public static final ired_title:I = 0x7f060111

.field public static final ired_vendor:I = 0x7f060112

.field public static final irled_clr_button:I = 0x7f06011b

.field public static final key_0:I = 0x7f06009a

.field public static final key_1:I = 0x7f06009b

.field public static final key_2:I = 0x7f06009c

.field public static final key_3:I = 0x7f06009d

.field public static final key_3g:I = 0x7f0600b8

.field public static final key_4:I = 0x7f06009e

.field public static final key_5:I = 0x7f06009f

.field public static final key_6:I = 0x7f0600a0

.field public static final key_7:I = 0x7f0600a1

.field public static final key_8:I = 0x7f0600a2

.field public static final key_9:I = 0x7f0600a3

.field public static final key_active:I = 0x7f0600b4

.field public static final key_back:I = 0x7f060096

.field public static final key_back_outer:I = 0x7f0600ba

.field public static final key_beam:I = 0x7f0600b7

.field public static final key_call:I = 0x7f060098

.field public static final key_camera:I = 0x7f0600af

.field public static final key_del:I = 0x7f060099

.field public static final key_dpad_center:I = 0x7f0600aa

.field public static final key_dpad_down:I = 0x7f0600a7

.field public static final key_dpad_left:I = 0x7f0600a8

.field public static final key_dpad_right:I = 0x7f0600a9

.field public static final key_dpad_up:I = 0x7f0600a6

.field public static final key_end:I = 0x7f060092

.field public static final key_exit:I = 0x7f0600b0

.field public static final key_f1:I = 0x7f0600b2

.field public static final key_focus:I = 0x7f0600ac

.field public static final key_func1:I = 0x7f0600ad

.field public static final key_func2:I = 0x7f0600ae

.field public static final key_hidden_1:I = 0x7f0600bb

.field public static final key_hidden_2:I = 0x7f0600bc

.field public static final key_hidden_3:I = 0x7f0600bd

.field public static final key_hidden_4:I = 0x7f0600be

.field public static final key_home:I = 0x7f060095

.field public static final key_menu:I = 0x7f060094

.field public static final key_network_sel:I = 0x7f060097

.field public static final key_pound:I = 0x7f0600a5

.field public static final key_power:I = 0x7f060093

.field public static final key_recent:I = 0x7f0600b6

.field public static final key_recent_outer:I = 0x7f0600b9

.field public static final key_reset:I = 0x7f0600b1

.field public static final key_search:I = 0x7f0600ab

.field public static final key_star:I = 0x7f0600a4

.field public static final key_switch:I = 0x7f0600b5

.field public static final key_user:I = 0x7f0600b3

.field public static final key_volume_down:I = 0x7f060091

.field public static final key_volume_up:I = 0x7f060090

.field public static final lcd_alpm_test:I = 0x7f0601da

.field public static final light_title:I = 0x7f0600f1

.field public static final loading:I = 0x7f060006

.field public static final log:I = 0x7f0601ab

.field public static final loopback:I = 0x7f060191

.field public static final lux:I = 0x7f0600f4

.field public static final magnetic_adc:I = 0x7f0600cd

.field public static final magnetic_bmz:I = 0x7f0600cb

.field public static final magnetic_dac:I = 0x7f0600cc

.field public static final magnetic_delta:I = 0x7f0600d3

.field public static final magnetic_hx:I = 0x7f0600c5

.field public static final magnetic_hy:I = 0x7f0600c6

.field public static final magnetic_hz:I = 0x7f0600c7

.field public static final magnetic_initialized:I = 0x7f0600c0

.field public static final magnetic_max:I = 0x7f0600d2

.field public static final magnetic_min:I = 0x7f0600d1

.field public static final magnetic_moebius:I = 0x7f0600d4

.field public static final magnetic_offset_h:I = 0x7f0600ce

.field public static final magnetic_result:I = 0x7f0600cf

.field public static final magnetic_status:I = 0x7f0600d0

.field public static final magnetic_sx:I = 0x7f0600c2

.field public static final magnetic_sy:I = 0x7f0600c3

.field public static final magnetic_sz:I = 0x7f0600c4

.field public static final magnetic_temp:I = 0x7f0600c1

.field public static final magnetic_title:I = 0x7f0600bf

.field public static final magnetic_x:I = 0x7f0600c8

.field public static final magnetic_y:I = 0x7f0600c9

.field public static final magnetic_z:I = 0x7f0600ca

.field public static final max:I = 0x7f0601a4

.field public static final mhl_selftest_status:I = 0x7f060127

.field public static final mic_button_exit:I = 0x7f060168

.field public static final mic_button_start:I = 0x7f060167

.field public static final mic_echo:I = 0x7f06018b

.field public static final mic_only:I = 0x7f06018d

.field public static final mic_title:I = 0x7f060166

.field public static final motor_on:I = 0x7f06018f

.field public static final motor_spec:I = 0x7f0601d1

.field public static final motor_x:I = 0x7f0601ce

.field public static final motor_y:I = 0x7f0601cf

.field public static final motor_z:I = 0x7f0601d0

.field public static final motortest_title:I = 0x7f0601cd

.field public static final need_cal_msg:I = 0x7f06000c

.field public static final next:I = 0x7f060054

.field public static final nfc_card_mode_test:I = 0x7f0600e0

.field public static final nfc_detecting_tag:I = 0x7f0600df

.field public static final nfc_test:I = 0x7f0600dd

.field public static final nfc_test2:I = 0x7f0600de

.field public static final no:I = 0x7f060003

.field public static final not_set:I = 0x7f06004f

.field public static final nv:I = 0x7f060123

.field public static final nvhistory_id:I = 0x7f0600e8

.field public static final nvhistory_item:I = 0x7f0600e9

.field public static final nvhistory_result:I = 0x7f0600ea

.field public static final nvhistory_title:I = 0x7f0600e7

.field public static final ois_gyro_self_test:I = 0x7f060084

.field public static final ois_selftest_title:I = 0x7f060085

.field public static final ok:I = 0x7f060001

.field public static final on_off:I = 0x7f060117

.field public static final otg_message:I = 0x7f0600ec

.field public static final otg_result:I = 0x7f0600ed

.field public static final otg_title:I = 0x7f0600eb

.field public static final pending:I = 0x7f0600f9

.field public static final pressure:I = 0x7f06019f

.field public static final prox_light_Button_Offset:I = 0x7f0600ef

.field public static final prox_light_title:I = 0x7f0600ee

.field public static final proximity_title:I = 0x7f0600f0

.field public static final quickchargecheck_title:I = 0x7f060195

.field public static final raw_data:I = 0x7f0601a5

.field public static final raw_value:I = 0x7f060154

.field public static final reader_mode_test:I = 0x7f0600e3

.field public static final ref:I = 0x7f0601a6

.field public static final release:I = 0x7f0600d8

.field public static final res_angle:I = 0x7f060015

.field public static final res_angle_spec:I = 0x7f060016

.field public static final res_count_delta:I = 0x7f060017

.field public static final res_direction:I = 0x7f060012

.field public static final res_peaktopeak:I = 0x7f060019

.field public static final res_result:I = 0x7f060018

.field public static final result:I = 0x7f06006a

.field public static final retest:I = 0x7f06018c

.field public static final rgb_cct:I = 0x7f0600f6

.field public static final rgb_result:I = 0x7f0600f7

.field public static final rgb_spec:I = 0x7f0600f8

.field public static final rgb_title:I = 0x7f0600f5

.field public static final s_end1:I = 0x7f0601a9

.field public static final s_end2:I = 0x7f0601aa

.field public static final s_start1:I = 0x7f0601a7

.field public static final s_start2:I = 0x7f0601a8

.field public static final semi_agc:I = 0x7f060183

.field public static final semi_band:I = 0x7f060181

.field public static final semi_exit:I = 0x7f060185

.field public static final semi_final:I = 0x7f06017f

.field public static final semi_freq:I = 0x7f060182

.field public static final semi_retest:I = 0x7f060184

.field public static final semi_spec:I = 0x7f060180

.field public static final sensor1:I = 0x7f060030

.field public static final sensor2:I = 0x7f060031

.field public static final sensor_test:I = 0x7f060069

.field public static final sim1_checks:I = 0x7f06014b

.field public static final sim1_switching:I = 0x7f06014d

.field public static final sim2_checks:I = 0x7f06014c

.field public static final sim2_switching:I = 0x7f06014e

.field public static final sim_slot1_switch:I = 0x7f06014f

.field public static final sim_slot2_switch:I = 0x7f060150

.field public static final sim_switch_result:I = 0x7f060151

.field public static final sim_switch_title:I = 0x7f06014a

.field public static final slew_rate_1:I = 0x7f0601a2

.field public static final slew_rate_2:I = 0x7f0601a3

.field public static final spec:I = 0x7f0600f2

.field public static final spen_back:I = 0x7f06010a

.field public static final spen_buttontest_eraser_text:I = 0x7f0600ff

.field public static final spen_buttontest_text:I = 0x7f0600fe

.field public static final spen_buttontest_title:I = 0x7f0600fd

.field public static final spen_detection_title:I = 0x7f060104

.field public static final spen_fail:I = 0x7f060108

.field public static final spen_hovertest_result_hovering:I = 0x7f060102

.field public static final spen_hovertest_result_released:I = 0x7f060103

.field public static final spen_hovertest_text:I = 0x7f060101

.field public static final spen_hovertest_title:I = 0x7f060100

.field public static final spen_keytest_title:I = 0x7f06010c

.field public static final spen_menu:I = 0x7f060109

.field public static final spen_pass:I = 0x7f060107

.field public static final spen_press:I = 0x7f06010b

.field public static final spen_release:I = 0x7f060105

.field public static final spen_working:I = 0x7f060106

.field public static final spk_cal:I = 0x7f0601b7

.field public static final spk_cal_cal:I = 0x7f0601b8

.field public static final spk_cal_cal_title:I = 0x7f0601b9

.field public static final spk_cal_data_title:I = 0x7f0601bb

.field public static final spk_cal_spec_title:I = 0x7f0601ba

.field public static final spk_mic:I = 0x7f06018e

.field public static final start_button:I = 0x7f06019b

.field public static final stop_button:I = 0x7f06019c

.field public static final str_bt_focus:I = 0x7f060169

.field public static final str_bt_result:I = 0x7f06016b

.field public static final str_bt_rotate:I = 0x7f06016a

.field public static final str_humid:I = 0x7f060157

.field public static final str_keytest_1:I = 0x7f06015b

.field public static final str_keytest_2:I = 0x7f06015c

.field public static final str_temp:I = 0x7f060156

.field public static final str_wait_msg:I = 0x7f06016c

.field public static final str_warning:I = 0x7f060159

.field public static final str_warning2:I = 0x7f06015a

.field public static final str_warning_title:I = 0x7f060158

.field public static final string_btle_message:I = 0x7f060022

.field public static final string_btle_retry:I = 0x7f06001f

.field public static final string_btle_searching:I = 0x7f060020

.field public static final string_btle_timecount:I = 0x7f060023

.field public static final string_btle_title:I = 0x7f060021

.field public static final string_ghost_message:I = 0x7f060133

.field public static final string_ghost_touch:I = 0x7f060132

.field public static final swp_test:I = 0x7f0600e1

.field public static final systeminfo_noti_content:I = 0x7f060008

.field public static final systeminfo_noti_hide:I = 0x7f06000a

.field public static final systeminfo_noti_show:I = 0x7f060009

.field public static final systeminfo_noti_title:I = 0x7f060007

.field public static final systeminfowidget_loading:I = 0x7f06000b

.field public static final testTitle:I = 0x7f0600da

.field public static final testnv_name:I = 0x7f060120

.field public static final time:I = 0x7f06019e

.field public static final try_count:I = 0x7f06007f

.field public static final tsp_self_fw_check:I = 0x7f060128

.field public static final tsp_self_name:I = 0x7f060129

.field public static final txt_dps_2000:I = 0x7f060089

.field public static final txt_dps_250:I = 0x7f060087

.field public static final txt_dps_500:I = 0x7f060088

.field public static final up:I = 0x7f060115

.field public static final usb_afc:I = 0x7f060146

.field public static final usb_connect:I = 0x7f060148

.field public static final usb_disconnect:I = 0x7f060149

.field public static final usb_ta:I = 0x7f060145

.field public static final usb_title:I = 0x7f060144

.field public static final usb_usb:I = 0x7f060147

.field public static final uv_test_adc:I = 0x7f0601bd

.field public static final uv_test_result:I = 0x7f0601c1

.field public static final uv_test_spec:I = 0x7f0601be

.field public static final uv_test_title:I = 0x7f0601bc

.field public static final uv_test_uva:I = 0x7f0601bf

.field public static final uv_test_uvb:I = 0x7f0601c0

.field public static final volume_up_down:I = 0x7f060113

.field public static final wait:I = 0x7f06002f

.field public static final waiting:I = 0x7f060193

.field public static final waterproof_fail:I = 0x7f0601ad

.field public static final waterproof_lleak:I = 0x7f0601b0

.field public static final waterproof_pass:I = 0x7f0601ac

.field public static final waterproof_pend:I = 0x7f0601ae

.field public static final waterproof_sleak:I = 0x7f0601b1

.field public static final waterproof_test:I = 0x7f0601af

.field public static final wireless_battery_status:I = 0x7f060125

.field public static final working:I = 0x7f0600d7

.field public static final x_value:I = 0x7f06007a

.field public static final y_value:I = 0x7f06007b

.field public static final yes:I = 0x7f060002

.field public static final z_value:I = 0x7f06007c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
