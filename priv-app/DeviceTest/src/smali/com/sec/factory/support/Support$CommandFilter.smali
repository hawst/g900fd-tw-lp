.class public Lcom/sec/factory/support/Support$CommandFilter;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommandFilter"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "AtCommand"

.field static filteredCommands:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1774
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getFilteredCommands()[Ljava/lang/String;
    .locals 20

    .prologue
    .line 1777
    const-class v16, Lcom/sec/factory/support/Support$CommandFilter;

    monitor-enter v16

    const/4 v12, 0x0

    .line 1778
    .local v12, "para":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1779
    .local v5, "count":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1780
    .local v13, "parent_Enable":Z
    const/4 v14, 0x0

    .line 1782
    .local v14, "parent_name":Ljava/lang/String;
    :try_start_0
    sget-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    if-nez v15, :cond_1

    .line 1783
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    sput-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1793
    :try_start_1
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v15

    const-string v17, "CommandFilter"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/sec/factory/support/XMLDataStorage;->getChildElementSet(Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v10

    .line 1795
    .local v10, "items":[Lorg/w3c/dom/Element;
    move-object v2, v10

    .local v2, "arr$":[Lorg/w3c/dom/Element;
    array-length v11, v2

    .local v11, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v11, :cond_4

    aget-object v9, v2, v8

    .line 1796
    .local v9, "item":Lorg/w3c/dom/Element;
    const-string v15, "enable"

    invoke-interface {v9, v15}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v13

    .line 1797
    const-string v15, "id"

    invoke-interface {v9, v15}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1799
    if-eqz v13, :cond_3

    .line 1800
    invoke-interface {v9}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v15

    const-string v17, "item-group"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1801
    invoke-interface {v9}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 1803
    .local v4, "childNodes":Lorg/w3c/dom/NodeList;
    if-eqz v10, :cond_3

    .line 1804
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v15

    if-ge v7, v15, :cond_3

    .line 1805
    invoke-interface {v4, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 1807
    .local v3, "child":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v15

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v15, v0, :cond_0

    .line 1808
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v15

    const-string v17, "para"

    move-object/from16 v0, v17

    invoke-interface {v15, v0}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v12

    .line 1810
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v15

    const-string v17, "count"

    move-object/from16 v0, v17

    invoke-interface {v15, v0}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    .line 1812
    sget-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1814
    const-string v15, "Support"

    const-string v17, "CommandFilter"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "filtername-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v15, v0, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1804
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 1785
    .end local v2    # "arr$":[Lorg/w3c/dom/Element;
    .end local v3    # "child":Lorg/w3c/dom/Node;
    .end local v4    # "childNodes":Lorg/w3c/dom/NodeList;
    .end local v7    # "i":I
    .end local v8    # "i$":I
    .end local v9    # "item":Lorg/w3c/dom/Element;
    .end local v10    # "items":[Lorg/w3c/dom/Element;
    .end local v11    # "len$":I
    :cond_1
    :try_start_2
    sget-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_2

    .line 1786
    sget-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1832
    :goto_2
    monitor-exit v16

    return-object v15

    .line 1788
    :cond_2
    const/4 v15, 0x0

    goto :goto_2

    .line 1795
    .restart local v2    # "arr$":[Lorg/w3c/dom/Element;
    .restart local v8    # "i$":I
    .restart local v9    # "item":Lorg/w3c/dom/Element;
    .restart local v10    # "items":[Lorg/w3c/dom/Element;
    .restart local v11    # "len$":I
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 1822
    .end local v2    # "arr$":[Lorg/w3c/dom/Element;
    .end local v8    # "i$":I
    .end local v9    # "item":Lorg/w3c/dom/Element;
    .end local v10    # "items":[Lorg/w3c/dom/Element;
    .end local v11    # "len$":I
    :catch_0
    move-exception v6

    .line 1823
    .local v6, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 1824
    const/4 v15, 0x0

    sput-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    .line 1825
    const/4 v15, 0x0

    goto :goto_2

    .line 1828
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v2    # "arr$":[Lorg/w3c/dom/Element;
    .restart local v8    # "i$":I
    .restart local v10    # "items":[Lorg/w3c/dom/Element;
    .restart local v11    # "len$":I
    :cond_4
    sget-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1829
    const/4 v15, 0x0

    goto :goto_2

    .line 1832
    :cond_5
    sget-object v15, Lcom/sec/factory/support/Support$CommandFilter;->filteredCommands:Ljava/util/ArrayList;

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1777
    .end local v2    # "arr$":[Lorg/w3c/dom/Element;
    .end local v8    # "i$":I
    .end local v10    # "items":[Lorg/w3c/dom/Element;
    .end local v11    # "len$":I
    :catchall_0
    move-exception v15

    monitor-exit v16

    throw v15
.end method
