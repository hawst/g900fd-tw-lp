.class public Lcom/sec/factory/support/Support$TestCase;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TestCase"
.end annotation


# static fields
.field public static final AT_BATTEST_RESET_WHEN_READ:Ljava/lang/String; = "AT_BATTEST_RESET_WHEN_READ"

.field public static final AT_CALIDATE:Ljava/lang/String; = "AT_CALIDATE"

.field public static final AT_GPSSTEST:Ljava/lang/String; = "AT_GPSSTEST"

.field public static final AT_ISDBTEST:Ljava/lang/String; = "AT_ISDBTEST"

.field public static final CAMERA_TORCH_MODE_RESPONSE_MARGINE:Ljava/lang/String; = "CAMERA_TORCH_MODE_RESPONSE_MARGINE"

.field public static final GEOMAGNETIC_ALPS_CAL_DELTA_SPEC:Ljava/lang/String; = "GEOMAGNETIC_ALPS_CAL_DELTA_SPEC"

.field public static final GEOMAGNETIC_IC_POINT:Ljava/lang/String; = "GEOMAGNETIC_IC_POINT"

.field public static final GESTURE_SENSOR_IR_CURRENT_CHANGE:Ljava/lang/String; = "GESTURE_SENSOR_IR_CURRENT_CHANGE"

.field public static final GESTURE_SENSOR_PEEK_TO_PEEK:Ljava/lang/String; = "GESTURE_SENSOR_PEEK_TO_PEEK"

.field public static final GRIPSENSOR_CAL_SUPPORT:Ljava/lang/String; = "GRIPSENSOR_CAL_SUPPORT"

.field public static final GRIPSENSOR_DUAL:Ljava/lang/String; = "GRIPSENSOR_DUAL"

.field public static final G_VECTOR_SUM_ACC_BIT:Ljava/lang/String; = "G_VECTOR_SUM_ACC_BIT"

.field public static final HALLIC_TEST_MILLIS_SEC:Ljava/lang/String; = "HALLIC_TEST_MILLIS_SEC"

.field public static final HALL_IC_TEST:Ljava/lang/String; = "HALL_IC_TEST"

.field public static final HALL_IC_TEST_2ND:Ljava/lang/String; = "HALL_IC_TEST_2ND"

.field public static final HUMITEMP_TEST:Ljava/lang/String; = "HUMITEMP_TEST"

.field public static final HV_CHARGE:Ljava/lang/String; = "HV_CHARGE"

.field public static final IS_ACC_FULL_SCALE_4G:Ljava/lang/String; = "IS_ACC_FULL_SCALE_4G"

.field public static final IS_ATMEL_TSP_IC_TOUCHKEY:Ljava/lang/String; = "IS_ATMEL_TSP_IC_TOUCHKEY"

.field public static final IS_DUMMYKEY_SAME_KEYCODE:Ljava/lang/String; = "IS_DUMMYKEY_SAME_KEYCODE"

.field public static final IS_IRLED_TEST_SPLIT_COMMAND:Ljava/lang/String; = "IS_IRLED_TEST_SPLIT_COMMAND"

.field public static final IS_KEY_TEST_THRESHOLD:Ljava/lang/String; = "IS_KEY_TEST_THRESHOLD"

.field public static final IS_NEW_TSP_SELFTEST_SYNAPTICS:Ljava/lang/String; = "IS_NEW_TSP_SELFTEST_SYNAPTICS"

.field public static final IS_NEW_TSP_SELFTEST_ZINITIX:Ljava/lang/String; = "IS_NEW_TSP_SELFTEST_ZINITIX"

.field public static final IS_PROXIMITY_TEST_MOTOR_FEEDBACK:Ljava/lang/String; = "IS_PROXIMITY_TEST_MOTOR_FEEDBACK"

.field public static final IS_SECOND_SELFTEST:Ljava/lang/String; = "IS_SECOND_SELFTEST"

.field public static final IS_SENSOR_TEST_ACC_REVERSE:Ljava/lang/String; = "IS_SENSOR_TEST_ACC_REVERSE"

.field public static final IS_SIMPLE_TEST_ACC_SYSFS:Ljava/lang/String; = "IS_SIMPLE_TEST_ACC_SYSFS"

.field public static final IS_TSP_HOVERING_TEST_SET_EDGE_DEADLOCK:Ljava/lang/String; = "IS_TSP_HOVERING_TEST_SET_EDGE_DEADLOCK"

.field public static final IS_TSP_SECOND_LCD_TEST:Ljava/lang/String; = "IS_TSP_SECOND_LCD_TEST"

.field public static final IS_TSP_SELFTEST_HOVER_ENABLE_FOR_STM:Ljava/lang/String; = "IS_TSP_SELFTEST_HOVER_ENABLE_FOR_STM"

.field public static final IS_TSP_STANDARD_CHANNEL:Ljava/lang/String; = "IS_TSP_STANDARD_CHANNEL"

.field public static final IS_VIBETONZ_UNSUPPORTED:Ljava/lang/String; = "IS_VIBETONZ_UNSUPPORTED"

.field public static final KEYCODE_END:I = 0x6

.field public static final KEY_TEST_0:Ljava/lang/String; = "KEY_TEST_0"

.field public static final KEY_TEST_1:Ljava/lang/String; = "KEY_TEST_1"

.field public static final KEY_TEST_2:Ljava/lang/String; = "KEY_TEST_2"

.field public static final KEY_TEST_3:Ljava/lang/String; = "KEY_TEST_3"

.field public static final KEY_TEST_3G:Ljava/lang/String; = "KEY_TEST_3G"

.field public static final KEY_TEST_4:Ljava/lang/String; = "KEY_TEST_4"

.field public static final KEY_TEST_5:Ljava/lang/String; = "KEY_TEST_5"

.field public static final KEY_TEST_6:Ljava/lang/String; = "KEY_TEST_6"

.field public static final KEY_TEST_7:Ljava/lang/String; = "KEY_TEST_7"

.field public static final KEY_TEST_8:Ljava/lang/String; = "KEY_TEST_8"

.field public static final KEY_TEST_9:Ljava/lang/String; = "KEY_TEST_9"

.field public static final KEY_TEST_ACTIVE:Ljava/lang/String; = "KEY_TEST_ACTIVE"

.field public static final KEY_TEST_APP_RECENT:Ljava/lang/String; = "KEY_TEST_APP_RECENT"

.field public static final KEY_TEST_APP_SWITCH:Ljava/lang/String; = "KEY_TEST_APP_SWITCH"

.field public static final KEY_TEST_BACK:Ljava/lang/String; = "KEY_TEST_BACK"

.field public static final KEY_TEST_BACK_OUT:Ljava/lang/String; = "KEY_TEST_BACK_OUT"

.field public static final KEY_TEST_BEAM:Ljava/lang/String; = "KEY_TEST_BEAM"

.field public static final KEY_TEST_CALL:Ljava/lang/String; = "KEY_TEST_CALL"

.field public static final KEY_TEST_CAMERA:Ljava/lang/String; = "KEY_TEST_CAMERA"

.field public static final KEY_TEST_DEL:Ljava/lang/String; = "KEY_TEST_DEL"

.field public static final KEY_TEST_DPAD_CENTER:Ljava/lang/String; = "KEY_TEST_DPAD_CENTER"

.field public static final KEY_TEST_DPAD_DOWN:Ljava/lang/String; = "KEY_TEST_DPAD_DOWN"

.field public static final KEY_TEST_DPAD_LEFT:Ljava/lang/String; = "KEY_TEST_DPAD_LEFT"

.field public static final KEY_TEST_DPAD_RIGHT:Ljava/lang/String; = "KEY_TEST_DPAD_RIGHT"

.field public static final KEY_TEST_DPAD_UP:Ljava/lang/String; = "KEY_TEST_DPAD_UP"

.field public static final KEY_TEST_END:Ljava/lang/String; = "KEY_TEST_END"

.field public static final KEY_TEST_F1:Ljava/lang/String; = "KEY_TEST_F1"

.field public static final KEY_TEST_FOCUS:Ljava/lang/String; = "KEY_TEST_FOCUS"

.field public static final KEY_TEST_FUNC1:Ljava/lang/String; = "KEY_TEST_FUNC1"

.field public static final KEY_TEST_FUNC2:Ljava/lang/String; = "KEY_TEST_FUNC2"

.field public static final KEY_TEST_HIDDEN_1:Ljava/lang/String; = "KEY_TEST_HIDDEN_1"

.field public static final KEY_TEST_HIDDEN_2:Ljava/lang/String; = "KEY_TEST_HIDDEN_2"

.field public static final KEY_TEST_HIDDEN_3:Ljava/lang/String; = "KEY_TEST_HIDDEN_3"

.field public static final KEY_TEST_HIDDEN_4:Ljava/lang/String; = "KEY_TEST_HIDDEN_4"

.field public static final KEY_TEST_HOME:Ljava/lang/String; = "KEY_TEST_HOME"

.field public static final KEY_TEST_MENU:Ljava/lang/String; = "KEY_TEST_MENU"

.field public static final KEY_TEST_NETWOR_SEL:Ljava/lang/String; = "KEY_TEST_NETWOR_SEL"

.field public static final KEY_TEST_POUND:Ljava/lang/String; = "KEY_TEST_POUND"

.field public static final KEY_TEST_POWER:Ljava/lang/String; = "KEY_TEST_POWER"

.field public static final KEY_TEST_RECENT_OUT:Ljava/lang/String; = "KEY_TEST_RECENT_OUT"

.field public static final KEY_TEST_SEARCH:Ljava/lang/String; = "KEY_TEST_SEARCH"

.field public static final KEY_TEST_STAR:Ljava/lang/String; = "KEY_TEST_STAR"

.field public static final KEY_TEST_TEXT_SIZE:Ljava/lang/String; = "KEY_TEST_TEXT_SIZE"

.field public static final KEY_TEST_TOUCH_KEY_ODER:Ljava/lang/String; = "KEY_TEST_TOUCH_KEY_ODER"

.field public static final KEY_TEST_TOUCH_KEY_ODER_HIDDEN:Ljava/lang/String; = "KEY_TEST_TOUCH_KEY_ODER_HIDDEN"

.field public static final KEY_TEST_USER:Ljava/lang/String; = "KEY_TEST_USER"

.field public static final KEY_TEST_VIEW_TABLE:Ljava/lang/String; = "KEY_TEST_VIEW_TABLE"

.field public static final KEY_TEST_VOLUME_DOWN:Ljava/lang/String; = "KEY_TEST_VOLUME_DOWN"

.field public static final KEY_TEST_VOLUME_UP:Ljava/lang/String; = "KEY_TEST_VOLUME_UP"

.field public static final MOBILE_TV_TYPE:Ljava/lang/String; = "MOBILE_TV_TYPE"

.field public static final NEED_ACK_FOR_CAMERA_STOP:Ljava/lang/String; = "NEED_ACK_FOR_CAMERA_STOP"

.field public static final NFC_ESE_TEST:Ljava/lang/String; = "NFC_ESE_TEST"

.field public static final NFC_SWP_TEST:Ljava/lang/String; = "NFC_SWP_TEST"

.field public static final PA0_TEMP_ADC:Ljava/lang/String; = "PA0_TEMP_ADC"

.field public static final PA1_TEMP_ADC:Ljava/lang/String; = "PA1_TEMP_ADC"

.field public static final PA_TEMP_ADC_VAL_TYPE:Ljava/lang/String; = "PA_TEMP_ADC_VAL_TYPE"

.field public static final QC_QUICKCHARGE:Ljava/lang/String; = "QC_QUICKCHARGE"

.field public static final QWERTY_EXIT_BUTTON_SIZE:Ljava/lang/String; = "QWERTY_EXIT_BUTTON_SIZE"

.field public static final QWERTY_KEY_TEST_ROW_1:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_1"

.field public static final QWERTY_KEY_TEST_ROW_10:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_10"

.field public static final QWERTY_KEY_TEST_ROW_11:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_11"

.field public static final QWERTY_KEY_TEST_ROW_2:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_2"

.field public static final QWERTY_KEY_TEST_ROW_3:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_3"

.field public static final QWERTY_KEY_TEST_ROW_4:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_4"

.field public static final QWERTY_KEY_TEST_ROW_5:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_5"

.field public static final QWERTY_KEY_TEST_ROW_6:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_6"

.field public static final QWERTY_KEY_TEST_ROW_7:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_7"

.field public static final QWERTY_KEY_TEST_ROW_8:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_8"

.field public static final QWERTY_KEY_TEST_ROW_9:Ljava/lang/String; = "QWERTY_KEY_TEST_ROW_9"

.field public static final QWERTY_KEY_TEXT_SIZE:Ljava/lang/String; = "QWERTY_KEY_TEXT_SIZE"

.field public static final QWERTY_KEY_TEXT_VIEW_HEIGHT:Ljava/lang/String; = "QWERTY_KEY_TEXT_VIEW_HEIGHT"

.field public static final READ_TARGET_GEOMAGNETIC:Ljava/lang/String; = "READ_TARGET_GEOMAGNETIC"

.field public static final SEMI_KEY_TEST_HOME:Ljava/lang/String; = "SEMI_KEY_TEST_HOME"

.field public static final SEMI_KEY_TEST_POWER:Ljava/lang/String; = "SEMI_KEY_TEST_POWER"

.field public static final SEMI_KEY_TEST_USER:Ljava/lang/String; = "SEMI_KEY_TEST_USER"

.field public static final SEMI_KEY_TEST_VOLUME_DOWN:Ljava/lang/String; = "SEMI_KEY_TEST_VOLUME_DOWN"

.field public static final SEMI_KEY_TEST_VOLUME_UP:Ljava/lang/String; = "SEMI_KEY_TEST_VOLUME_UP"

.field public static final SENSOR_TEST_ACC_BIT:Ljava/lang/String; = "SENSOR_TEST_ACC_BIT"

.field public static final SIMPLE_TEST_MEGACAM_SCALE_VALUE:Ljava/lang/String; = "SIMPLE_TEST_MEGACAM_SCALE_VALUE"

.field public static final SUPPORT_CHARGE_COUNT:Ljava/lang/String; = "SUPPORT_CHARGE_COUNT"

.field public static final SUPPORT_GEOMAGNETIC_ALPS_CAL:Ljava/lang/String; = "SUPPORT_GEOMAGNETIC_ALPS_CAL"

.field public static final SUPPORT_MARVELL_RIL:Ljava/lang/String; = "SUPPORT_MARVELL_RIL"

.field public static final SUPPORT_SEMI_MAXCLOCK_VALUE:Ljava/lang/String; = "SUPPORT_SEMI_MAXCLOCK_VALUE"

.field public static final TAG:Ljava/lang/String; = "TestCase"

.field public static final UV_SENSOR_ACTIVITY_AUTO_FINISH:Ljava/lang/String; = "UV_SENSOR_ACTIVITY_AUTO_FINISH"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "attr"    # Ljava/lang/String;
    .param p2, "deafult"    # Ljava/lang/String;

    .prologue
    .line 656
    const/4 v1, 0x1

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p0, p1, v1}, Lcom/sec/factory/support/Support$Values;->access$700(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 657
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "Unknown"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p2

    .line 660
    .end local v0    # "ret":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public static getCount(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 648
    const-string v0, "count"

    # invokes: Lcom/sec/factory/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$400(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 640
    const-string v0, "enable"

    # invokes: Lcom/sec/factory/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getKeyTextSize(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 636
    const-string v0, "size"

    # invokes: Lcom/sec/factory/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$500(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getSize(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 652
    const-string v0, "size"

    # invokes: Lcom/sec/factory/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$500(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 644
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getViewPoint(Ljava/lang/String;)Landroid/graphics/Point;
    .locals 4
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 610
    const-string v3, "point"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 611
    .local v2, "position":Ljava/lang/String;
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 613
    .local v0, "point":Landroid/graphics/Point;
    if-eqz v2, :cond_0

    .line 614
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 615
    .local v1, "pointStr":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->x:I

    .line 616
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->y:I

    .line 619
    .end local v1    # "pointStr":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getViewPointF(Ljava/lang/String;)Landroid/graphics/PointF;
    .locals 4
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 623
    const-string v3, "point"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 624
    .local v2, "position":Ljava/lang/String;
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 626
    .local v0, "point":Landroid/graphics/PointF;
    if-eqz v2, :cond_0

    .line 627
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 628
    .local v1, "pointStr":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 629
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 632
    .end local v1    # "pointStr":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static isTouchkey(Ljava/lang/String;)Z
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 606
    const-string v0, "keytype"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "touch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
