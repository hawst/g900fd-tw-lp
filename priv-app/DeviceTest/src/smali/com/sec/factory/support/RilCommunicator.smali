.class public Lcom/sec/factory/support/RilCommunicator;
.super Ljava/lang/Object;
.source "RilCommunicator.java"


# static fields
.field public static final INTENT_ACTION_RESPONSE_NV:Ljava/lang/String; = "com.android.samsungtest.RilOmissionCommand"

.field public static final INTENT_ACTION_RESPONSE_NV_HISTORY:Ljava/lang/String; = "com.android.samsungtest.RilOmissionCommand"

.field private static REQUEST:I

.field public static final REQUEST_NV:I

.field public static final REQUEST_NV_HISTORY:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    const/4 v0, 0x0

    sput v0, Lcom/sec/factory/support/RilCommunicator;->REQUEST:I

    .line 15
    sget v0, Lcom/sec/factory/support/RilCommunicator;->REQUEST:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/support/RilCommunicator;->REQUEST:I

    sput v0, Lcom/sec/factory/support/RilCommunicator;->REQUEST_NV:I

    .line 16
    sget v0, Lcom/sec/factory/support/RilCommunicator;->REQUEST:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/support/RilCommunicator;->REQUEST:I

    sput v0, Lcom/sec/factory/support/RilCommunicator;->REQUEST_NV_HISTORY:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static requestGetNVorNVHistory(I)V
    .locals 4
    .param p0, "request"    # I

    .prologue
    .line 20
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 21
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 24
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x12

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 25
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 26
    const/4 v3, 0x7

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 27
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 28
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 30
    sget v3, Lcom/sec/factory/support/RilCommunicator;->REQUEST_NV:I

    if-ne p0, v3, :cond_1

    .line 31
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    sget v3, Lcom/sec/factory/support/RilCommunicator;->REQUEST_NV_HISTORY:I

    if-ne p0, v3, :cond_0

    .line 33
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v2

    .line 36
    .local v2, "e":Ljava/io/IOException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static requestSetNV(BB)V
    .locals 4
    .param p0, "itemID"    # B
    .param p1, "result"    # B

    .prologue
    .line 45
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 46
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 49
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x12

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 50
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 51
    const/16 v3, 0x9

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 52
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 53
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 54
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 55
    invoke-virtual {v1, p0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 56
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v2

    .line 58
    .local v2, "e":Ljava/io/IOException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method
