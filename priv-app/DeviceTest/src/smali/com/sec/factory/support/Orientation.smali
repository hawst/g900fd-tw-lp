.class public final enum Lcom/sec/factory/support/Orientation;
.super Ljava/lang/Enum;
.source "Orientation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/factory/support/Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/factory/support/Orientation;

.field private static final CLASS_NAME:Ljava/lang/String; = "Orientation"

.field public static final enum LANDSCAPE:Lcom/sec/factory/support/Orientation;

.field public static final enum PORTRAIT:Lcom/sec/factory/support/Orientation;

.field public static final enum REVERSE_LANDSCAPE:Lcom/sec/factory/support/Orientation;

.field public static final enum REVERSE_PORTRAIT:Lcom/sec/factory/support/Orientation;


# instance fields
.field private mId:Ljava/lang/String;

.field private mOrientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 8
    new-instance v0, Lcom/sec/factory/support/Orientation;

    const-string v1, "REVERSE_PORTRAIT"

    const-string v2, "reversePortrait"

    const/16 v3, 0x9

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/factory/support/Orientation;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/support/Orientation;->REVERSE_PORTRAIT:Lcom/sec/factory/support/Orientation;

    .line 9
    new-instance v0, Lcom/sec/factory/support/Orientation;

    const-string v1, "PORTRAIT"

    const-string v2, "portrait"

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/sec/factory/support/Orientation;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/support/Orientation;->PORTRAIT:Lcom/sec/factory/support/Orientation;

    .line 10
    new-instance v0, Lcom/sec/factory/support/Orientation;

    const-string v1, "LANDSCAPE"

    const-string v2, "landscape"

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/sec/factory/support/Orientation;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/support/Orientation;->LANDSCAPE:Lcom/sec/factory/support/Orientation;

    .line 11
    new-instance v0, Lcom/sec/factory/support/Orientation;

    const-string v1, "REVERSE_LANDSCAPE"

    const-string v2, "reverseLandscape"

    const/16 v3, 0x8

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/factory/support/Orientation;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/support/Orientation;->REVERSE_LANDSCAPE:Lcom/sec/factory/support/Orientation;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/factory/support/Orientation;

    sget-object v1, Lcom/sec/factory/support/Orientation;->REVERSE_PORTRAIT:Lcom/sec/factory/support/Orientation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/factory/support/Orientation;->PORTRAIT:Lcom/sec/factory/support/Orientation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/factory/support/Orientation;->LANDSCAPE:Lcom/sec/factory/support/Orientation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/factory/support/Orientation;->REVERSE_LANDSCAPE:Lcom/sec/factory/support/Orientation;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/factory/support/Orientation;->$VALUES:[Lcom/sec/factory/support/Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "orientation"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput-object p3, p0, Lcom/sec/factory/support/Orientation;->mId:Ljava/lang/String;

    .line 19
    iput p4, p0, Lcom/sec/factory/support/Orientation;->mOrientation:I

    .line 20
    return-void
.end method

.method private equals(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/factory/support/Orientation;->mId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getOrientation(Ljava/lang/String;)I
    .locals 8
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {}, Lcom/sec/factory/support/Orientation;->values()[Lcom/sec/factory/support/Orientation;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/factory/support/Orientation;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 32
    .local v3, "o":Lcom/sec/factory/support/Orientation;
    invoke-direct {v3, p0}, Lcom/sec/factory/support/Orientation;->equals(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 33
    const-string v4, "Orientation"

    const-string v5, "getOrientation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", orientation: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/sec/factory/support/Orientation;->mOrientation:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {v3}, Lcom/sec/factory/support/Orientation;->getOrientation()I

    move-result v4

    .line 39
    .end local v3    # "o":Lcom/sec/factory/support/Orientation;
    :goto_1
    return v4

    .line 31
    .restart local v3    # "o":Lcom/sec/factory/support/Orientation;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    .end local v3    # "o":Lcom/sec/factory/support/Orientation;
    :cond_1
    const-string v4, "Orientation"

    const-string v5, "getOrientation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Not found id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public static getOrientation(Ljava/lang/String;I)I
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "rotation"    # I

    .prologue
    .line 43
    invoke-static {p0}, Lcom/sec/factory/support/Orientation;->getOrientation(Ljava/lang/String;)I

    move-result v1

    .line 44
    .local v1, "requestedOrientation":I
    move v0, v1

    .line 46
    .local v0, "convertedOrientation":I
    packed-switch p1, :pswitch_data_0

    .line 68
    :goto_0
    const-string v2, "Orientation"

    const-string v3, "getOrientation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestedOrientation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", convertOrientation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return v0

    .line 48
    :pswitch_0
    const-string v2, "Orientation"

    const-string v3, "getOrientation"

    const-string v4, "ROTATION_0"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :pswitch_1
    const-string v2, "Orientation"

    const-string v3, "getOrientation"

    const-string v4, "ROTATION_180"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :pswitch_2
    const-string v2, "Orientation"

    const-string v3, "getOrientation"

    const-string v4, "ROTATION_90"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :pswitch_3
    const-string v2, "Orientation"

    const-string v3, "getOrientation"

    const-string v4, "ROTATION_270"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/factory/support/Orientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/sec/factory/support/Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/factory/support/Orientation;

    return-object v0
.end method

.method public static values()[Lcom/sec/factory/support/Orientation;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/sec/factory/support/Orientation;->$VALUES:[Lcom/sec/factory/support/Orientation;

    invoke-virtual {v0}, [Lcom/sec/factory/support/Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/factory/support/Orientation;

    return-object v0
.end method


# virtual methods
.method public getOrientation()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/factory/support/Orientation;->mOrientation:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/factory/support/Orientation;->mId:Ljava/lang/String;

    return-object v0
.end method
