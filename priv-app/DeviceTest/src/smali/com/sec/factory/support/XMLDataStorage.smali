.class public Lcom/sec/factory/support/XMLDataStorage;
.super Ljava/lang/Object;
.source "XMLDataStorage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/support/XMLDataStorage$ElementIdMismatchException;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "XMLDataStorage"

.field private static mInstance:Lcom/sec/factory/support/XMLDataStorage;


# instance fields
.field private mAttrCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

.field private mDocument:Lorg/w3c/dom/Document;

.field private mWasCompletedParsing:Z

.field private mXPath:Ljavax/xml/xpath/XPath;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/support/XMLDataStorage;->mInstance:Lcom/sec/factory/support/XMLDataStorage;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/factory/support/XMLDataStorage;->mWasCompletedParsing:Z

    .line 235
    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/support/XMLDataStorage;->mAttrCache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 57
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 58
    .local v1, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->setIgnoringComments(Z)V

    .line 59
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->setIgnoringElementContentWhitespace(Z)V

    .line 61
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/support/XMLDataStorage;->mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

    .line 62
    invoke-static {}, Ljavax/xml/xpath/XPathFactory;->newInstance()Ljavax/xml/xpath/XPathFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/xpath/XPathFactory;->newXPath()Ljavax/xml/xpath/XPath;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/support/XMLDataStorage;->mXPath:Ljavax/xml/xpath/XPath;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    .end local v1    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private cloneNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)Lorg/w3c/dom/Node;
    .locals 10
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .param p2, "cloneTarget"    # Lorg/w3c/dom/Element;

    .prologue
    .line 373
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v8}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v7

    .line 375
    .local v7, "newNode":Lorg/w3c/dom/Element;
    invoke-interface {p2}, Lorg/w3c/dom/Element;->hasAttributes()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 376
    invoke-virtual {p0, p2}, Lcom/sec/factory/support/XMLDataStorage;->getAttributeNameSet(Lorg/w3c/dom/Element;)[Ljava/lang/String;

    move-result-object v2

    .line 378
    .local v2, "attributes":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 379
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 380
    .local v1, "attr":Ljava/lang/String;
    invoke-interface {p2, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v1, v8}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 385
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "attr":Ljava/lang/String;
    .end local v2    # "attributes":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 387
    .local v6, "list":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 388
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 389
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    check-cast v8, Lorg/w3c/dom/Element;

    invoke-direct {p0, p1, v8}, Lcom/sec/factory/support/XMLDataStorage;->cloneNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 387
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 393
    :cond_2
    return-object v7
.end method

.method private convertBytesToIS([B)Ljava/io/InputStream;
    .locals 1
    .param p1, "bytes"    # [B

    .prologue
    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "byteis":Ljava/io/ByteArrayInputStream;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .end local v0    # "byteis":Ljava/io/ByteArrayInputStream;
    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 205
    .restart local v0    # "byteis":Ljava/io/ByteArrayInputStream;
    return-object v0
.end method

.method private convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "xmlFile"    # Ljava/lang/String;

    .prologue
    .line 162
    const-string v0, "/sdcard/"

    .line 163
    .local v0, "SdCardPath":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 165
    .local v7, "mModelFileInSdcard":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .local v8, "stringbuilder":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 167
    const/4 v4, 0x0

    .line 170
    .local v4, "inputstream":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 171
    .local v3, "file":Ljava/io/File;
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 172
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    .end local v4    # "inputstream":Ljava/io/InputStream;
    .local v5, "inputstream":Ljava/io/InputStream;
    :try_start_1
    const-string v9, "Read model file from sdcard"

    const/4 v10, 0x1

    invoke-static {p1, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v4, v5

    .line 179
    .end local v5    # "inputstream":Ljava/io/InputStream;
    .restart local v4    # "inputstream":Ljava/io/InputStream;
    :goto_0
    :try_start_2
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 181
    .local v1, "buffreader":Ljava/io/BufferedReader;
    const-string v6, ""

    .line 182
    .local v6, "line":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 183
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 186
    .end local v1    # "buffreader":Ljava/io/BufferedReader;
    .end local v3    # "file":Ljava/io/File;
    .end local v6    # "line":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 187
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 191
    if-eqz v4, :cond_0

    .line 192
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 199
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9

    .line 175
    .restart local v3    # "file":Ljava/io/File;
    :cond_1
    :try_start_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {v9, p2, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v4

    goto :goto_0

    .line 191
    .restart local v1    # "buffreader":Ljava/io/BufferedReader;
    .restart local v6    # "line":Ljava/lang/String;
    :cond_2
    if-eqz v4, :cond_0

    .line 192
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 194
    :catch_1
    move-exception v2

    .line 195
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 194
    .end local v1    # "buffreader":Ljava/io/BufferedReader;
    .end local v3    # "file":Ljava/io/File;
    .end local v6    # "line":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 195
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 190
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 191
    :goto_4
    if-eqz v4, :cond_3

    .line 192
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 196
    :cond_3
    :goto_5
    throw v9

    .line 194
    :catch_3
    move-exception v2

    .line 195
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_5

    .line 190
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "inputstream":Ljava/io/InputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v5    # "inputstream":Ljava/io/InputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "inputstream":Ljava/io/InputStream;
    .restart local v4    # "inputstream":Ljava/io/InputStream;
    goto :goto_4

    .line 186
    .end local v4    # "inputstream":Ljava/io/InputStream;
    .restart local v5    # "inputstream":Ljava/io/InputStream;
    :catch_4
    move-exception v2

    move-object v4, v5

    .end local v5    # "inputstream":Ljava/io/InputStream;
    .restart local v4    # "inputstream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private getBaseDocument(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 6
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 286
    const-string v3, "/Factory/BaseDocument"

    sget-object v4, Ljavax/xml/xpath/XPathConstants;->NODE:Ljavax/xml/namespace/QName;

    invoke-direct {p0, p1, v3, v4}, Lcom/sec/factory/support/XMLDataStorage;->xpath(Lorg/w3c/dom/Document;Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 287
    .local v1, "element":Lorg/w3c/dom/Element;
    const-string v0, ""

    .line 288
    .local v0, "datDocument":Ljava/lang/String;
    const-string v2, ""

    .line 289
    .local v2, "result":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "document"

    invoke-interface {v1, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 292
    :goto_0
    if-eqz v0, :cond_1

    .line 293
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 298
    :goto_1
    return-object v2

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_1
    move-object v2, v0

    goto :goto_1
.end method

.method public static declared-synchronized instance()Lcom/sec/factory/support/XMLDataStorage;
    .locals 2

    .prologue
    .line 48
    const-class v1, Lcom/sec/factory/support/XMLDataStorage;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/factory/support/XMLDataStorage;->mInstance:Lcom/sec/factory/support/XMLDataStorage;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/factory/support/XMLDataStorage;

    invoke-direct {v0}, Lcom/sec/factory/support/XMLDataStorage;-><init>()V

    sput-object v0, Lcom/sec/factory/support/XMLDataStorage;->mInstance:Lcom/sec/factory/support/XMLDataStorage;

    .line 52
    :cond_0
    sget-object v0, Lcom/sec/factory/support/XMLDataStorage;->mInstance:Lcom/sec/factory/support/XMLDataStorage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private makeElementArray(Lorg/w3c/dom/NodeList;)[Lorg/w3c/dom/Element;
    .locals 3
    .param p1, "nodeList"    # Lorg/w3c/dom/NodeList;

    .prologue
    .line 318
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    new-array v0, v2, [Lorg/w3c/dom/Element;

    .line 320
    .local v0, "elementSet":[Lorg/w3c/dom/Element;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 321
    invoke-interface {p1, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    aput-object v2, v0, v1

    .line 320
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 324
    :cond_0
    return-object v0
.end method

.method private parseAsset(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "xml"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 113
    :try_start_0
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    const-string v8, "parseAsset Is Started"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/support/XMLDataStorage;->convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 118
    .local v2, "dataBytes":[B
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Convert dat file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v6, p0, Lcom/sec/factory/support/XMLDataStorage;->mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

    invoke-direct {p0, v2}, Lcom/sec/factory/support/XMLDataStorage;->convertBytesToIS([B)Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v3

    .line 120
    .local v3, "document":Lorg/w3c/dom/Document;
    const/4 v1, 0x0

    .line 122
    .local v1, "baseXml":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/factory/support/XMLDataStorage;->getBaseDocument(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 123
    const/4 v2, 0x0

    .line 124
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Decode base file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0, p1, v1}, Lcom/sec/factory/support/XMLDataStorage;->convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 126
    iget-object v6, p0, Lcom/sec/factory/support/XMLDataStorage;->mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

    invoke-direct {p0, v2}, Lcom/sec/factory/support/XMLDataStorage;->convertBytesToIS([B)Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 127
    .local v0, "baseDocument":Lorg/w3c/dom/Document;
    move-object v5, v3

    .line 128
    .local v5, "subDocument":Lorg/w3c/dom/Document;
    invoke-direct {p0, v0, v5}, Lcom/sec/factory/support/XMLDataStorage;->redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Document;)V

    .line 129
    const-string v6, "FactoryTestMenu"

    invoke-interface {v0, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "FactoryTestMenu"

    invoke-interface {v5, v7}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v0, v6, v7}, Lcom/sec/factory/support/XMLDataStorage;->swapNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    .line 133
    const-string v6, "FactoryTestMenuSub"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 134
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    const-string v8, "FactoryTestMenuSub"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v6, "FactoryTestMenuSub"

    invoke-interface {v0, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "FactoryTestMenuSub"

    invoke-interface {v5, v7}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v0, v6, v7}, Lcom/sec/factory/support/XMLDataStorage;->swapNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    .line 140
    :cond_0
    const-string v6, "SemiFunctionMenu"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 141
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    const-string v8, "SemiFunctionMenu"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v6, "SemiFunctionMenu"

    invoke-interface {v0, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    const-string v7, "SemiFunctionMenu"

    invoke-interface {v5, v7}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    invoke-direct {p0, v0, v6, v7}, Lcom/sec/factory/support/XMLDataStorage;->swapNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    .line 147
    :cond_1
    iput-object v0, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    .line 151
    .end local v0    # "baseDocument":Lorg/w3c/dom/Document;
    .end local v5    # "subDocument":Lorg/w3c/dom/Document;
    :goto_0
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    const-string v8, "parseAsset Is Completed"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/factory/support/XMLDataStorage;->mWasCompletedParsing:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    .end local v1    # "baseXml":Ljava/lang/String;
    .end local v2    # "dataBytes":[B
    .end local v3    # "document":Lorg/w3c/dom/Document;
    :goto_1
    iget-boolean v6, p0, Lcom/sec/factory/support/XMLDataStorage;->mWasCompletedParsing:Z

    return v6

    .line 149
    .restart local v1    # "baseXml":Ljava/lang/String;
    .restart local v2    # "dataBytes":[B
    .restart local v3    # "document":Lorg/w3c/dom/Document;
    :cond_2
    :try_start_1
    iput-object v3, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 153
    .end local v1    # "baseXml":Ljava/lang/String;
    .end local v2    # "dataBytes":[B
    .end local v3    # "document":Lorg/w3c/dom/Document;
    :catch_0
    move-exception v4

    .line 154
    .local v4, "e":Ljava/lang/Exception;
    iput-boolean v10, p0, Lcom/sec/factory/support/XMLDataStorage;->mWasCompletedParsing:Z

    .line 155
    invoke-static {v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Document;)V
    .locals 1
    .param p1, "baseDocument"    # Lorg/w3c/dom/Document;
    .param p2, "subDocument"    # Lorg/w3c/dom/Document;

    .prologue
    .line 328
    invoke-interface {p2}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/support/XMLDataStorage;->redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 329
    return-void
.end method

.method private redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 13
    .param p1, "baseDocument"    # Lorg/w3c/dom/Document;
    .param p2, "redefNode"    # Lorg/w3c/dom/Element;

    .prologue
    .line 332
    invoke-interface {p2}, Lorg/w3c/dom/Element;->hasAttributes()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 333
    const-string v9, "id"

    invoke-interface {p2, v9}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 335
    .local v7, "id":Ljava/lang/String;
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 336
    const-string v9, "XMLDataStorage"

    const-string v10, "redefinitionById"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "id="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-virtual {p0, p2}, Lcom/sec/factory/support/XMLDataStorage;->getAttributeNameSet(Lorg/w3c/dom/Element;)[Ljava/lang/String;

    move-result-object v2

    .line 339
    .local v2, "attributes":[Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 340
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v1, v0, v6

    .line 341
    .local v1, "attr":Ljava/lang/String;
    const-string v9, "id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 342
    invoke-interface {p1, v7}, Lorg/w3c/dom/Document;->getElementById(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    .line 344
    .local v4, "element":Lorg/w3c/dom/Element;
    if-eqz v4, :cond_1

    .line 345
    invoke-interface {p2, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v1, v9}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    .end local v4    # "element":Lorg/w3c/dom/Element;
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 347
    .restart local v4    # "element":Lorg/w3c/dom/Element;
    :cond_1
    new-instance v9, Lcom/sec/factory/support/XMLDataStorage$ElementIdMismatchException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Element \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\" does not exist in base document."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/factory/support/XMLDataStorage$ElementIdMismatchException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 356
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "attr":Ljava/lang/String;
    .end local v2    # "attributes":[Ljava/lang/String;
    .end local v4    # "element":Lorg/w3c/dom/Element;
    .end local v6    # "i$":I
    .end local v7    # "id":Ljava/lang/String;
    .end local v8    # "len$":I
    :cond_2
    invoke-interface {p2}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 357
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 359
    .local v3, "childNodes":Lorg/w3c/dom/NodeList;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v5, v9, :cond_4

    .line 360
    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 361
    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    check-cast v9, Lorg/w3c/dom/Element;

    invoke-direct {p0, p1, v9}, Lcom/sec/factory/support/XMLDataStorage;->redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 359
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 365
    .end local v3    # "childNodes":Lorg/w3c/dom/NodeList;
    .end local v5    # "i":I
    :cond_4
    return-void
.end method

.method private swapNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V
    .locals 2
    .param p1, "targetDocument"    # Lorg/w3c/dom/Document;
    .param p2, "baseNode"    # Lorg/w3c/dom/Node;
    .param p3, "subNode"    # Lorg/w3c/dom/Node;

    .prologue
    .line 368
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast p3, Lorg/w3c/dom/Element;

    .end local p3    # "subNode":Lorg/w3c/dom/Node;
    invoke-direct {p0, p1, p3}, Lcom/sec/factory/support/XMLDataStorage;->cloneNode(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lorg/w3c/dom/Node;->replaceChild(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 370
    return-void
.end method

.method private xpath(Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;
    .locals 1
    .param p1, "expression"    # Ljava/lang/String;
    .param p2, "returnType"    # Ljavax/xml/namespace/QName;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/factory/support/XMLDataStorage;->xpath(Lorg/w3c/dom/Document;Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private xpath(Lorg/w3c/dom/Document;Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;
    .locals 3
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .param p2, "expression"    # Ljava/lang/String;
    .param p3, "returnType"    # Ljavax/xml/namespace/QName;

    .prologue
    .line 306
    const/4 v1, 0x0

    .line 309
    .local v1, "result":Ljava/lang/Object;
    :try_start_0
    iget-object v2, p0, Lcom/sec/factory/support/XMLDataStorage;->mXPath:Ljavax/xml/xpath/XPath;

    invoke-interface {v2, p2}, Ljavax/xml/xpath/XPath;->compile(Ljava/lang/String;)Ljavax/xml/xpath/XPathExpression;

    move-result-object v2

    invoke-interface {v2, p1, p3}, Ljavax/xml/xpath/XPathExpression;->evaluate(Ljava/lang/Object;Ljavax/xml/namespace/QName;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 314
    .end local v1    # "result":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 310
    .restart local v1    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getAttributeNameSet(Lorg/w3c/dom/Element;)[Ljava/lang/String;
    .locals 4
    .param p1, "element"    # Lorg/w3c/dom/Element;

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 269
    .local v0, "attributeSet":[Ljava/lang/String;
    invoke-interface {p1}, Lorg/w3c/dom/Element;->hasAttributes()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 270
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 271
    .local v2, "map":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    .line 273
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 274
    invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 278
    .end local v1    # "i":I
    .end local v2    # "map":Lorg/w3c/dom/NamedNodeMap;
    :cond_0
    return-object v0
.end method

.method public getAttributeValueByAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "finaAttr"    # Ljava/lang/String;
    .param p2, "findAttrValue"    # Ljava/lang/String;
    .param p3, "resultAttr"    # Ljava/lang/String;

    .prologue
    .line 257
    invoke-virtual {p0, p1, p2}, Lcom/sec/factory/support/XMLDataStorage;->getElementByAttribute(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    .line 259
    .local v0, "element":Lorg/w3c/dom/Element;
    if-eqz v0, :cond_0

    .line 260
    invoke-interface {v0, p3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 263
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "resultAttr"    # Ljava/lang/String;

    .prologue
    .line 240
    const-string v0, "$"

    .line 241
    .local v0, "DELIMITER":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "$"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "cacheId":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/support/XMLDataStorage;->mAttrCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 243
    .local v1, "attr":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 244
    iget-object v4, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v4, p1}, Lorg/w3c/dom/Document;->getElementById(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    invoke-interface {v4, p2}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "value":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/support/XMLDataStorage;->mAttrCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    move-object v1, v3

    .line 248
    .end local v3    # "value":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public getAttributeValueByTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "resultAttr"    # Ljava/lang/String;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0, p2}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChildElementSet(Ljava/lang/String;)[Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "parentNodeName"    # Ljava/lang/String;

    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljavax/xml/xpath/XPathConstants;->NODESET:Ljavax/xml/namespace/QName;

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/support/XMLDataStorage;->xpath(Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/NodeList;

    invoke-direct {p0, v0}, Lcom/sec/factory/support/XMLDataStorage;->makeElementArray(Lorg/w3c/dom/NodeList;)[Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method public getElementByAttribute(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "attribute"    # Ljava/lang/String;
    .param p2, "attributeValue"    # Ljava/lang/String;

    .prologue
    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "//*[@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljavax/xml/xpath/XPathConstants;->NODE:Ljavax/xml/namespace/QName;

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/support/XMLDataStorage;->xpath(Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    return-object v0
.end method

.method public getElementById(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/factory/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->getElementById(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public getElementByName(Ljava/lang/String;)Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljavax/xml/xpath/XPathConstants;->NODE:Ljavax/xml/namespace/QName;

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/support/XMLDataStorage;->xpath(Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    return-object v0
.end method

.method public getElementSetByName(Ljava/lang/String;)[Lorg/w3c/dom/Element;
    .locals 2
    .param p1, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljavax/xml/xpath/XPathConstants;->NODESET:Ljavax/xml/namespace/QName;

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/support/XMLDataStorage;->xpath(Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/NodeList;

    invoke-direct {p0, v0}, Lcom/sec/factory/support/XMLDataStorage;->makeElementArray(Lorg/w3c/dom/NodeList;)[Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized parseXML(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/factory/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v5

    if-nez v5, :cond_1

    .line 70
    const-string v0, "factory"

    .line 71
    .local v0, "DEFAULT_XML":Ljava/lang/String;
    const-string v5, "PAP"

    const-string v6, "ro.csc.sales_code"

    const-string v7, "OXX"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 72
    .local v2, "isLiveDemo":Z
    const-string v5, "ro.product.model"

    const-string v6, "factory"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 73
    .local v3, "modelXML":Ljava/lang/String;
    const-string v5, "ro.product.device"

    const-string v6, "factory"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "deviceXML":Ljava/lang/String;
    const-string v5, "ro.product.name"

    const-string v6, "factory"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 75
    .local v4, "nameXML":Ljava/lang/String;
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "is Live Demo : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".dat"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 78
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".dat"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".dat"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 81
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "modelXML="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deviceXML="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "nameXML="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v5

    invoke-direct {v5, p1, v3}, Lcom/sec/factory/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 86
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ro.product.model file not found : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v5

    invoke-direct {v5, p1, v1}, Lcom/sec/factory/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 89
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ro.product.device file not found : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v5

    invoke-direct {v5, p1, v4}, Lcom/sec/factory/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 93
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ro.product.name file not found : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v5

    const-string v6, "factory.dat"

    invoke-direct {v5, p1, v6}, Lcom/sec/factory/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    .line 96
    const-string v5, "XMLDataStorage"

    const-string v6, "parseXML"

    const-string v7, "Default file loaded => factory.dat"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_0
    const/4 v5, 0x1

    .line 106
    .end local v0    # "DEFAULT_XML":Ljava/lang/String;
    .end local v1    # "deviceXML":Ljava/lang/String;
    .end local v2    # "isLiveDemo":Z
    .end local v3    # "modelXML":Ljava/lang/String;
    .end local v4    # "nameXML":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return v5

    .line 103
    :cond_1
    :try_start_1
    const-string v5, "XMLDataStorage"

    const-string v6, "parsingXML"

    const-string v7, "FtClient => data parsing was completed."

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    const/4 v5, 0x0

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public wasCompletedParsing()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/sec/factory/support/XMLDataStorage;->mWasCompletedParsing:Z

    return v0
.end method
