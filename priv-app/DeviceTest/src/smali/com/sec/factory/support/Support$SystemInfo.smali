.class public Lcom/sec/factory/support/Support$SystemInfo;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SystemInfo"
.end annotation


# static fields
.field public static final SYS_INFO_BAND_CHANNEL:Ljava/lang/String; = "SYS_INFO_BAND_CHANNEL"

.field public static final SYS_INFO_BATTERY_LEVEL:Ljava/lang/String; = "SYS_INFO_BATTERY_LEVEL"

.field public static final SYS_INFO_BG_COLOR:Ljava/lang/String; = "SYS_INFO_BG_COLOR"

.field public static final SYS_INFO_CAMERA_FIRM_VERSION:Ljava/lang/String; = "SYS_INFO_CAMERA_FIRM_VERSION"

.field public static final SYS_INFO_CSC_VERSION:Ljava/lang/String; = "SYS_INFO_CSC_VERSION"

.field public static final SYS_INFO_CURRENT_CP:Ljava/lang/String; = "SYS_INFO_CURRENT_CP"

.field public static final SYS_INFO_FONT_COLOR:Ljava/lang/String; = "SYS_INFO_FONT_COLOR"

.field public static final SYS_INFO_FONT_SIZE:Ljava/lang/String; = "SYS_INFO_FONT_SIZE"

.field public static final SYS_INFO_HW_VERSION:Ljava/lang/String; = "SYS_INFO_HW_VERSION"

.field public static final SYS_INFO_LCD_VERSION:Ljava/lang/String; = "SYS_INFO_LCD_VERSION"

.field public static final SYS_INFO_LTE_VERSION:Ljava/lang/String; = "SYS_INFO_LTE_VERSION"

.field public static final SYS_INFO_MEMORY_SIZE:Ljava/lang/String; = "SYS_INFO_MEMORY_SIZE"

.field public static final SYS_INFO_MODEL_NAME:Ljava/lang/String; = "SYS_INFO_MODEL_NAME"

.field public static final SYS_INFO_OIS_CAMERA_FIRM_VERSION:Ljava/lang/String; = "SYS_INFO_OIS_CAMERA_FIRM_VERSION"

.field public static final SYS_INFO_OUT_POSITION:Ljava/lang/String; = "SYS_INFO_OUT_POSITION"

.field public static final SYS_INFO_PDA_VERSION:Ljava/lang/String; = "SYS_INFO_PDA_VERSION"

.field public static final SYS_INFO_PHONE2_VERSION:Ljava/lang/String; = "SYS_INFO_PHONE2_VERSION"

.field public static final SYS_INFO_PHONE_VERSION:Ljava/lang/String; = "SYS_INFO_PHONE_VERSION"

.field public static final SYS_INFO_RF_CAL_DATA:Ljava/lang/String; = "SYS_INFO_RF_CAL_DATA"

.field public static final SYS_INFO_SENSOR_HUB_FIRM_VERSION:Ljava/lang/String; = "SYS_INFO_SENSOR_HUB_FIRM_VERSION"

.field public static final SYS_INFO_SIO_MODE:Ljava/lang/String; = "SYS_INFO_SIO_MODE"

.field public static final SYS_INFO_SMD_PDA:Ljava/lang/String; = "SYS_INFO_SMD_PDA"

.field public static final SYS_INFO_SPEN_FIRM_VERSION:Ljava/lang/String; = "SYS_INFO_SPEN_FIRM_VERSION"

.field public static final SYS_INFO_TSK_FIRM_VERSION:Ljava/lang/String; = "SYS_INFO_TSK_FIRM_VERSION"

.field public static final SYS_INFO_TSP_FIRM_VERSION:Ljava/lang/String; = "SYS_INFO_TSP_FIRM_VERSION"

.field public static final SYS_INFO_UNIQUE_NUMBER:Ljava/lang/String; = "SYS_INFO_UNIQUE_NUMBER"

.field public static final SYS_INFO_WIDGET_FONT_SIZE:Ljava/lang/String; = "SYS_INFO_WIDGET_FONT_SIZE"

.field public static final TAG:Ljava/lang/String; = "SystemInfo"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBGColor()I
    .locals 2

    .prologue
    .line 709
    const-string v0, "SYS_INFO_BG_COLOR"

    const-string v1, "color"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getFontColor()I
    .locals 2

    .prologue
    .line 705
    const-string v0, "SYS_INFO_FONT_COLOR"

    const-string v1, "color"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getFontSize()I
    .locals 2

    .prologue
    .line 697
    const-string v0, "SYS_INFO_FONT_SIZE"

    const-string v1, "size"

    # invokes: Lcom/sec/factory/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Values;->access$400(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getPosition()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 713
    const-string v0, "SYS_INFO_OUT_POSITION"

    const-string v1, "point"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVisibility(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 717
    const-string v0, "visibility"

    # invokes: Lcom/sec/factory/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getWidgetFontSize()I
    .locals 2

    .prologue
    .line 701
    const-string v0, "SYS_INFO_WIDGET_FONT_SIZE"

    const-string v1, "size"

    # invokes: Lcom/sec/factory/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Values;->access$400(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
