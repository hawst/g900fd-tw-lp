.class public Lcom/sec/factory/support/Support$Feature;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Feature"
.end annotation


# static fields
.field public static final ANYWAY_JIG_CABLE_TYPE:Ljava/lang/String; = "ANYWAY_JIG_CABLE_TYPE"

.field public static final BATT_TYPE:Ljava/lang/String; = "BATT_TYPE"

.field public static final BOOT_CHECK_TOUCHKEY_WO_SVCLED:Ljava/lang/String; = "BOOT_CHECK_TOUCHKEY_WO_SVCLED"

.field public static final CAL_AP_TEMP:Ljava/lang/String; = "CAL_AP_TEMP"

.field public static final CHIPSET_CP2_MANUFACTURE:Ljava/lang/String; = "CHIPSET_CP2_MANUFACTURE"

.field public static final CHIPSET_CP_MANUFACTURE:Ljava/lang/String; = "CHIPSET_CP_MANUFACTURE"

.field public static final CHIPSET_MANUFACTURE:Ljava/lang/String; = "CHIPSET_MANUFACTURE"

.field public static final CHIPSET_NAME:Ljava/lang/String; = "CHIPSET_NAME"

.field public static final CHIPSET_NAME_CP:Ljava/lang/String; = "CHIPSET_NAME_CP"

.field public static final CHIPSET_NAME_CP2:Ljava/lang/String; = "CHIPSET_NAME_CP2"

.field public static final CONTACT_DIALTACTS_CLASS:Ljava/lang/String; = "CONTACT_DIALTACTS_CLASS"

.field public static final CONTACT_DIALTACTS_PACKAGE:Ljava/lang/String; = "CONTACT_DIALTACTS_PACKAGE"

.field public static final DATA_ONLY_MODEL:Ljava/lang/String; = "DATA_ONLY_MODEL"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "DEVICE_TYPE"

.field public static final DISABLE_TOUCH_SOUND:Ljava/lang/String; = "DISABLE_TOUCH_SOUND"

.field public static final EPEN_SAVINGMODE_ENBLE:Ljava/lang/String; = "EPEN_SAVINGMODE_ENBLE"

.field public static final EXIST_PRE_LCIA:Ljava/lang/String; = "EXIST_PRE_LCIA"

.field public static final FACTOLOG_SYSTEM_INFO_UPDATE:Ljava/lang/String; = "FACTOLOG_SYSTEM_INFO_UPDATE"

.field public static final FACTORY_TEST_APO:Ljava/lang/String; = "FACTORY_TEST_APO"

.field public static final FACTORY_TEST_PROTOCOL:Ljava/lang/String; = "FACTORY_TEST_PROTOCOL"

.field public static final FACTORY_TEST_UNDER_USER_BINARY:Ljava/lang/String; = "FACTORY_TEST_UNDER_USER_BINARY"

.field public static final FEATURE_ENABLE_DYNAMIC_MULTI_SIM:Ljava/lang/String; = "FEATURE_ENABLE_DYNAMIC_MULTI_SIM"

.field public static final FEATURE_ENABLE_GRIP_CAL_BUTTON_DECTECTED_HALLIC:Ljava/lang/String; = "FEATURE_ENABLE_GRIP_CAL_BUTTON_DECTECTED_HALLIC"

.field public static final FLS_TYPE_EFS:Ljava/lang/String; = "FLS_TYPE_EFS"

.field public static final FM_RADIO_SW_VENDOR:Ljava/lang/String; = "FM_RADIO_SW_VENDOR"

.field public static final FONT_SIZE:Ljava/lang/String; = "FONT_SIZE"

.field public static final FRONT_CAMERA_TYPE:Ljava/lang/String; = "FRONT_CAMERA_TYPE"

.field public static final FRS_TYPE_EFS:Ljava/lang/String; = "FRS_TYPE_EFS"

.field public static final FTA_HW_VER:Ljava/lang/String; = "FTA_HW_VER"

.field public static final FTA_SW_VER:Ljava/lang/String; = "FTA_SW_VER"

.field public static final GRIPSENSOR_TYPE:Ljava/lang/String; = "GRIPSENSOR_TYPE"

.field public static final HOVER_XY_REVERSE:Ljava/lang/String; = "HOVER_XY_REVERSE"

.field public static final HW_VER_EFS:Ljava/lang/String; = "HW_VER_EFS"

.field public static final INBATT_SAVE_SOC:Ljava/lang/String; = "INBATT_SAVE_SOC"

.field public static final ISP_FLASH_TEST_SMD:Ljava/lang/String; = "ISP_FLASH_TEST_SMD"

.field public static final IS_INNER_OUTER_TOUCHKEY:Ljava/lang/String; = "IS_INNER_OUTER_TOUCHKEY"

.field public static final IS_MULTI_SIM_FRAMEWORK:Ljava/lang/String; = "IS_MULTI_SIM_FRAMEWORK"

.field public static final IS_SUPPORT_SENSOR_HUB_CHG:Ljava/lang/String; = "IS_SUPPORT_SENSOR_HUB_CHG"

.field public static final JIG_CHECK:Ljava/lang/String; = "JIG_CHECK"

.field public static final LIGHT_SENSOR_LUX_ONLY:Ljava/lang/String; = "LIGHT_SENSOR_LUX_ONLY"

.field public static final LIVE_DEMO_MODEL:Ljava/lang/String; = "LIVE_DEMO_MODEL"

.field public static final LOOPBACK_BUFFER_MARGIN:Ljava/lang/String; = "LOOPBACK_BUFFER_MARGIN"

.field public static final MIC_COUNT:Ljava/lang/String; = "MIC_COUNT"

.field public static final MODEL_COMMUNICATION_MODE:Ljava/lang/String; = "MODEL_COMMUNICATION_MODE"

.field public static final MODEL_COMMUNICATION_TYPE:Ljava/lang/String; = "MODEL_COMMUNICATION_TYPE"

.field public static final MODEL_HARDWARE_REVISION:Ljava/lang/String; = "MODEL_HARDWARE_REVISION"

.field public static final MODEL_NAME:Ljava/lang/String; = "MODEL_NAME"

.field public static final MODEL_NUMBER:Ljava/lang/String; = "MODEL_NUMBER"

.field public static final MODEL_WITH_DI:Ljava/lang/String; = "MODEL_WITH_DI"

.field public static final MULTI_TSK_THD:Ljava/lang/String; = "MULTI_TSK_THD"

.field public static final NEED_CAMDRIVER_OPEN:Ljava/lang/String; = "NEED_CAMDRIVER_OPEN"

.field public static final NEED_IMEM_CHECK_DEVICESIZE:Ljava/lang/String; = "NEED_IMEM_CHECK_DEVICESIZE"

.field public static final NEED_LPA_MODE_SET:Ljava/lang/String; = "NEED_LPA_MODE_SET"

.field public static final NEED_LPM_MODE_SET:Ljava/lang/String; = "NEED_LPM_MODE_SET"

.field public static final NEED_NOTI_AUDIO_MANAGER:Ljava/lang/String; = "NEED_NOTI_AUDIO_MANAGER"

.field public static final OTG_CHECK_ATTACH_ONLY:Ljava/lang/String; = "OTG_CHECK_ATTACH_ONLY"

.field public static final PANEL_TYPE:Ljava/lang/String; = "PANEL_TYPE"

.field public static final RAM_SIZE_IF:Ljava/lang/String; = "RAM_SIZE_IF"

.field public static final REAR_CAMERA_TYPE:Ljava/lang/String; = "REAR_CAMERA_TYPE"

.field public static final RECEIVER_FOREGROUND_FLAG:Ljava/lang/String; = "RECEIVER_FOREGROUND_FLAG"

.field public static final SEMI_FUNCTION_FONT_SIZE:Ljava/lang/String; = "SEMI_FUNCTION_FONT_SIZE"

.field public static final SEMI_FUNCTION_TEST_UI_ORIENTATION:Ljava/lang/String; = "SEMI_FUNCTION_TEST_UI_ORIENTATION"

.field public static final SENSOR_NAME_ACCELEROMETER:Ljava/lang/String; = "SENSOR_NAME_ACCELEROMETER"

.field public static final SENSOR_NAME_GESTURE:Ljava/lang/String; = "SENSOR_NAME_GESTURE"

.field public static final SENSOR_NAME_GYROSCOPE:Ljava/lang/String; = "SENSOR_NAME_GYROSCOPE"

.field public static final SENSOR_NAME_LIGHT:Ljava/lang/String; = "SENSOR_NAME_LIGHT"

.field public static final SENSOR_NAME_MAGNETIC:Ljava/lang/String; = "SENSOR_NAME_MAGNETIC"

.field public static final SENSOR_NAME_PROXIMITY:Ljava/lang/String; = "SENSOR_NAME_PROXIMITY"

.field public static final SENSOR_NAME_UV:Ljava/lang/String; = "SENSOR_NAME_UV"

.field public static final SENSOR_VENDOR_ECG:Ljava/lang/String; = "SENSOR_VENDOR_ECG"

.field public static final SETUPWIZARD_INDEX_LIST:Ljava/lang/String; = "SETUPWIZARD_INDEX_LIST"

.field public static final SPEAKER_COUNT:Ljava/lang/String; = "SPEAKER_COUNT"

.field public static final SPEN_HOVERING_DEL_LEFTTOP_RIGHTDOWN:Ljava/lang/String; = "SPEN_HOVERING_DEL_LEFTTOP_RIGHTDOWN"

.field public static final SPEN_SUB_KEY_SUPPORT:Ljava/lang/String; = "SPEN_SUB_KEY_SUPPORT"

.field public static final STANDARD_JB_SYSFS:Ljava/lang/String; = "STANDARD_JB_SYSFS"

.field public static final SUBKEY_DIFFERENT_VALUE:Ljava/lang/String; = "SUBKEY_DIFFERENT_VALUE"

.field public static final SUBKEY_FIVE_KEY:Ljava/lang/String; = "SUBKEY_FIVE_KEY"

.field public static final SUBKEY_FOUR_KEY:Ljava/lang/String; = "SUBKEY_FOUR_KEY"

.field public static final SUBKEY_SIX_KEY:Ljava/lang/String; = "SUBKEY_SIX_KEY"

.field public static final SUPPORT_2ND_CP:Ljava/lang/String; = "SUPPORT_2ND_CP"

.field public static final SUPPORT_2ND_RIL:Ljava/lang/String; = "SUPPORT_2ND_RIL"

.field public static final SUPPORT_3X4_KEY:Ljava/lang/String; = "SUPPORT_3X4_KEY"

.field public static final SUPPORT_ACC_SENSOR_BMC150:Ljava/lang/String; = "SUPPORT_ACC_SENSOR_BMC150"

.field public static final SUPPORT_ADC_ERROR_SYSFS:Ljava/lang/String; = "SUPPORT_ADC_ERROR_SYSFS"

.field public static final SUPPORT_ADSP_SENSOR_TEMPORARY:Ljava/lang/String; = "SUPPORT_ADSP_SENSOR_TEMPORARY"

.field public static final SUPPORT_APP_SWITCH_KEY:Ljava/lang/String; = "SUPPORT_APP_SWITCH_KEY"

.field public static final SUPPORT_AP_EX_THERM_ADC:Ljava/lang/String; = "SUPPORT_AP_EX_THERM_ADC"

.field public static final SUPPORT_AT_COMMAND:Ljava/lang/String; = "SUPPORT_AT_COMMAND"

.field public static final SUPPORT_AUTO_WAKELOCK:Ljava/lang/String; = "SUPPORT_AUTO_WAKELOCK"

.field public static final SUPPORT_BARCODE_EMULATOR:Ljava/lang/String; = "SUPPORT_BARCODE_EMULATOR"

.field public static final SUPPORT_BATTERY_ADC:Ljava/lang/String; = "SUPPORT_BATTERY_ADC"

.field public static final SUPPORT_BATTERY_TYPE:Ljava/lang/String; = "SUPPORT_BATTERY_TYPE"

.field public static final SUPPORT_BOOK_COVER:Ljava/lang/String; = "SUPPORT_BOOK_COVER"

.field public static final SUPPORT_BOOST_MEDIASCAN:Ljava/lang/String; = "SUPPORT_BOOST_MEDIASCAN"

.field public static final SUPPORT_CAM_FRONT_NOT_ISP:Ljava/lang/String; = "SUPPORT_CAM_FRONT_NOT_ISP"

.field public static final SUPPORT_CAM_ISP:Ljava/lang/String; = "SUPPORT_CAM_ISP"

.field public static final SUPPORT_CP_ACCELEROMETER:Ljava/lang/String; = "SUPPORT_CP_ACCELEROMETER"

.field public static final SUPPORT_CX_DATA_ALL_DISPLAY:Ljava/lang/String; = "SUPPORT_CX_DATA_ALL_DISPLAY"

.field public static final SUPPORT_CX_DATA_GAP_DISPLAY:Ljava/lang/String; = "SUPPORT_CX_DATA_GAP_DISPLAY"

.field public static final SUPPORT_DISABLE_SCROLLING_CACHE:Ljava/lang/String; = "SUPPORT_DISABLE_SCROLLING_CACHE"

.field public static final SUPPORT_DSDS_MSIMM_CHECK:Ljava/lang/String; = "SUPPORT_DSDS_MSIMM_CHECK"

.field public static final SUPPORT_DSTOP_LPA_DISABLE:Ljava/lang/String; = "SUPPORT_DSTOP_LPA_DISABLE"

.field public static final SUPPORT_DUALMODE:Ljava/lang/String; = "SUPPORT_DUALMODE"

.field public static final SUPPORT_DUALMODE_1CP2RIL:Ljava/lang/String; = "SUPPORT_DUALMODE_1CP2RIL"

.field public static final SUPPORT_DUALMODE_2CP2RIL:Ljava/lang/String; = "SUPPORT_DUALMODE_2CP2RIL"

.field public static final SUPPORT_DUALMODE_MARVELL:Ljava/lang/String; = "SUPPORT_DUALMODE_MARVELL"

.field public static final SUPPORT_DUAL_LCD_FOLDER:Ljava/lang/String; = "SUPPORT_DUAL_LCD_FOLDER"

.field public static final SUPPORT_DUAL_MOTOR:Ljava/lang/String; = "SUPPORT_DUAL_MOTOR"

.field public static final SUPPORT_DUAL_STANBY:Ljava/lang/String; = "SUPPORT_DUAL_STANBY"

.field public static final SUPPORT_DUAL_STANBY_ONE_CP:Ljava/lang/String; = "SUPPORT_DUAL_STANBY_ONE_CP"

.field public static final SUPPORT_DUMMY_KEY_FOR_RECENT:Ljava/lang/String; = "SUPPORT_DUMMY_KEY_FOR_RECENT"

.field public static final SUPPORT_EAR_VOLUME_CONTROL:Ljava/lang/String; = "SUPPORT_EAR_VOLUME_CONTROL"

.field public static final SUPPORT_EPEN:Ljava/lang/String; = "SUPPORT_EPEN"

.field public static final SUPPORT_FINAL_LTE:Ljava/lang/String; = "SUPPORT_FINAL_LTE"

.field public static final SUPPORT_FINAL_TD_LTE:Ljava/lang/String; = "SUPPORT_FINAL_TD_LTE"

.field public static final SUPPORT_FINAL_TD_MASTER_SLAVE:Ljava/lang/String; = "SUPPORT_FINAL_TD_MASTER_SLAVE"

.field public static final SUPPORT_FLASH_LED_THERM:Ljava/lang/String; = "SUPPORT_FLASH_LED_THERM"

.field public static final SUPPORT_FM_RADIO:Ljava/lang/String; = "SUPPORT_FM_RADIO"

.field public static final SUPPORT_FRONT_CAMERA:Ljava/lang/String; = "SUPPORT_FRONT_CAMERA"

.field public static final SUPPORT_GLOVE_MODE:Ljava/lang/String; = "SUPPORT_GLOVE_MODE"

.field public static final SUPPORT_GRAPHIC_ACCLETOR:Ljava/lang/String; = "SUPPORT_GRAPHIC_ACCLETOR"

.field public static final SUPPORT_GRIPSENS_CAL_BUTTON:Ljava/lang/String; = "SUPPORT_GRIPSENS_CAL_BUTTON"

.field public static final SUPPORT_HOVER_HIGHTLIGHT:Ljava/lang/String; = "SUPPORT_HOVER_HIGHTLIGHT"

.field public static final SUPPORT_HRM_SPO2:Ljava/lang/String; = "SUPPORT_HRM_SPO2"

.field public static final SUPPORT_IRLED_CONCEPT:Ljava/lang/String; = "SUPPORT_IRLED_CONCEPT"

.field public static final SUPPORT_IRLED_FEEDBACK_IC:Ljava/lang/String; = "SUPPORT_IRLED_FEEDBACK_IC"

.field public static final SUPPORT_IRLED_REPEAT_MODE:Ljava/lang/String; = "SUPPORT_IRLED_REPEAT_MODE"

.field public static final SUPPORT_JIG_ON:Ljava/lang/String; = "SUPPORT_JIG_ON"

.field public static final SUPPORT_KEYTEST_HOME_HARDKEY_SEPARATION:Ljava/lang/String; = "SUPPORT_KEYTEST_HOME_HARDKEY_SEPARATION"

.field public static final SUPPORT_LED:Ljava/lang/String; = "SUPPORT_LED"

.field public static final SUPPORT_LISTEN_KEY:Ljava/lang/String; = "SUPPORT_LISTEN_KEY"

.field public static final SUPPORT_LTE:Ljava/lang/String; = "SUPPORT_LTE"

.field public static final SUPPORT_MIC1_MAIN_MIC:Ljava/lang/String; = "SUPPORT_MIC1_MAIN_MIC"

.field public static final SUPPORT_MIC2_ECHO_TEST:Ljava/lang/String; = "SUPPORT_MIC2_ECHO_TEST"

.field public static final SUPPORT_MOBILE_TV:Ljava/lang/String; = "SUPPORT_MOBILE_TV"

.field public static final SUPPORT_NCR:Ljava/lang/String; = "SUPPORT_NCR"

.field public static final SUPPORT_NVBACKUP_CMD:Ljava/lang/String; = "SUPPORT_NVBACKUP_CMD"

.field public static final SUPPORT_PACKET_LOOPBACK:Ljava/lang/String; = "SUPPORT_PACKET_LOOPBACK"

.field public static final SUPPORT_PBA:Ljava/lang/String; = "SUPPORT_PBA"

.field public static final SUPPORT_PIEZO_RCV:Ljava/lang/String; = "SUPPORT_PIEZO_RCV"

.field public static final SUPPORT_QWERTY_KEY:Ljava/lang/String; = "SUPPORT_QWERTY_KEY"

.field public static final SUPPORT_RCV:Ljava/lang/String; = "SUPPORT_RCV"

.field public static final SUPPORT_REAR_CAM_OIS:Ljava/lang/String; = "SUPPORT_REAR_CAM_OIS"

.field public static final SUPPORT_REF_VALUE:Ljava/lang/String; = "SUPPORT_REF_VALUE"

.field public static final SUPPORT_RING_BUFFER_MODE:Ljava/lang/String; = "SUPPORT_RING_BUFFER_MODE"

.field public static final SUPPORT_RUN_REF:Ljava/lang/String; = "SUPPORT_RUN_REF"

.field public static final SUPPORT_SECOND_MIC_TEST:Ljava/lang/String; = "SUPPORT_SECOND_MIC_TEST"

.field public static final SUPPORT_SELFTEST_DISPLAY_DELAY:Ljava/lang/String; = "SUPPORT_SELFTEST_DISPLAY_DELAY"

.field public static final SUPPORT_SELFTEST_PWC_DISPLAY:Ljava/lang/String; = "SUPPORT_SELFTEST_PWC_DISPLAY"

.field public static final SUPPORT_SEMI_FUNCTION_TEST:Ljava/lang/String; = "SUPPORT_SEMI_FUNCTION_TEST"

.field public static final SUPPORT_SEMI_MAXCLOCK_LOCK:Ljava/lang/String; = "SUPPORT_SEMI_MAXCLOCK_LOCK"

.field public static final SUPPORT_SEMI_NV_RESET:Ljava/lang/String; = "SUPPORT_SEMI_NV_RESET"

.field public static final SUPPORT_SENSORHUB:Ljava/lang/String; = "SUPPORT_SENSORHUB"

.field public static final SUPPORT_SWITCH_KERNEL_FTM:Ljava/lang/String; = "SUPPORT_SWITCH_KERNEL_FTM"

.field public static final SUPPORT_SYSFS_GEO_GYRO:Ljava/lang/String; = "SUPPORT_SYSFS_GEO_GYRO"

.field public static final SUPPORT_SYSTEM_INFO:Ljava/lang/String; = "SUPPORT_SYSTEM_INFO"

.field public static final SUPPORT_UFS:Ljava/lang/String; = "SUPPORT_UFS"

.field public static final SUPPORT_VIBRATOR:Ljava/lang/String; = "SUPPORT_VIBRATOR"

.field public static final SUPPORT_VOICE_CALL_FORCIBLY:Ljava/lang/String; = "SUPPORT_VOICE_CALL_FORCIBLY"

.field public static final SUPPORT_WATERPROOF_TEST:Ljava/lang/String; = "SUPPORT_WATERPROOF_TEST"

.field public static final SUPPORT_WIFI_STRESS_TEST:Ljava/lang/String; = "SUPPORT_WIFI_STRESS_TEST"

.field public static final SVC_LED_TEST_BRIGHTNESS:Ljava/lang/String; = "SVC_LED_TEST_BRIGHTNESS"

.field public static final SYS_INFO_LTE_FINAL:Ljava/lang/String; = "SYS_INFO_LTE_FINAL"

.field public static final SYS_INFO_UPDATE:Ljava/lang/String; = "SYS_INFO_UPDATE"

.field public static final TABLET_DEFAULT_ORIENTATION:Ljava/lang/String; = "TABLET_DEFAULT_ORIENTATION"

.field public static final TAG:Ljava/lang/String; = "Feature"

.field public static final TORCH_MODE_FLASH_ON_CURRENT:Ljava/lang/String; = "TORCH_MODE_FLASH_ON_CURRENT"

.field public static final TSK_LEFT_MAPPING_APP_SWITCH:Ljava/lang/String; = "TSK_LEFT_MAPPING_APP_SWITCH"

.field public static final TSK_MANUFACTURE:Ljava/lang/String; = "TSK_MANUFACTURE"

.field public static final UNSUPPORT_HOME_FINGER:Ljava/lang/String; = "UNSUPPORT_HOME_FINGER"

.field public static final USE_DIFFERENT_THD:Ljava/lang/String; = "USE_DIFFERENT_THD"

.field public static final USE_MODEL_NUMBER:Ljava/lang/String; = "USE_MODEL_NUMBER"

.field public static final USE_SPECIAL_CONTACT_DIALTACTS_PACKAGE:Ljava/lang/String; = "USE_SPECIAL_CONTACT_DIALTACTS_PACKAGE"

.field public static final USE_USERDATA_VERSION:Ljava/lang/String; = "USE_USERDATA_VERSION"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 256
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 260
    const-string v0, "value"

    const/4 v1, 0x1

    # invokes: Lcom/sec/factory/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z
    invoke-static {p0, v0, v1}, Lcom/sec/factory/support/Support$Values;->access$200(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getByte(Ljava/lang/String;)B
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 264
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getByte(Ljava/lang/String;Ljava/lang/String;)B
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$300(Ljava/lang/String;Ljava/lang/String;)B

    move-result v0

    return v0
.end method

.method public static getDouble(Ljava/lang/String;)D
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 280
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getDouble(Ljava/lang/String;Ljava/lang/String;)D
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$600(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFactoryTextSize()F
    .locals 5

    .prologue
    .line 284
    const-string v1, "FONT_SIZE"

    const-string v2, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 285
    .local v0, "fontsize":Ljava/lang/String;
    const-string v1, "Support"

    const-string v2, "getFactoryTextSize"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FONT_SIZE : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Unknown"

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 276
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$500(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 272
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$400(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 268
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
