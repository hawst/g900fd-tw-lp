.class public Lcom/sec/factory/support/Support$Kernel;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Kernel"
.end annotation


# static fields
.field public static final ABCD_RAW:Ljava/lang/String; = "ABCD_RAW"

.field public static final ACCELEROMETER_SENSOR_TYPE:Ljava/lang/String; = "ACCELEROMETER_SENSOR_TYPE"

.field public static final ACCEL_SENSOR_CAL:Ljava/lang/String; = "ACCEL_SENSOR_CAL"

.field public static final ACCEL_SENSOR_INTPIN:Ljava/lang/String; = "ACCEL_SENSOR_INTPIN"

.field public static final ACCEL_SENSOR_NAME:Ljava/lang/String; = "ACCEL_SENSOR_NAME"

.field public static final ACCEL_SENSOR_RAW:Ljava/lang/String; = "ACCEL_SENSOR_RAW"

.field public static final ACCEL_SENSOR_SELFTEST:Ljava/lang/String; = "ACCEL_SENSOR_SELFTEST"

.field public static final ACC_COVER_PWR_LSI:Ljava/lang/String; = "ACC_COVER_PWR_LSI"

.field public static final ACC_COVER_PWR_QCOM:Ljava/lang/String; = "ACC_COVER_PWR_QCOM"

.field public static final ACC_COVER_UART_LSI:Ljava/lang/String; = "ACC_COVER_UART_LSI"

.field public static final ACC_COVER_UART_QCOM:Ljava/lang/String; = "ACC_COVER_UART_QCOM"

.field public static final ADC_ERR_CONTROL:Ljava/lang/String; = "ADC_ERR_CONTROL"

.field public static final ANYWAY_JIG:Ljava/lang/String; = "ANYWAY_JIG"

.field public static final ANYWAY_JIG_30PIN:Ljava/lang/String; = "ANYWAY_JIG_30PIN"

.field public static final APCHIP_TEMP_ADC:Ljava/lang/String; = "APCHIP_TEMP_ADC"

.field public static final BARCODE_EMUL_FIRMWARE_VERSION:Ljava/lang/String; = "BARCODE_EMUL_FIRMWARE_VERSION"

.field public static final BAROMETER_SENSOR_NAME:Ljava/lang/String; = "BAROMETER_SENSOR_NAME"

.field public static final BAROMETE_CALIBRATION:Ljava/lang/String; = "BAROMETE_CALIBRATION"

.field public static final BAROMETE_DELTA:Ljava/lang/String; = "BAROMETE_DELTA"

.field public static final BAROMETE_VENDOR:Ljava/lang/String; = "BAROMETE_VENDOR"

.field public static final BAROME_EEPROM:Ljava/lang/String; = "BAROME_EEPROM"

.field public static final BATTERY_ADC_NOTSUPPORT:Ljava/lang/String; = "BATTERY_ADC_NOTSUPPORT"

.field public static final BATTERY_AUTH:Ljava/lang/String; = "BATTERY_AUTH"

.field public static final BATTERY_CAPACITY:Ljava/lang/String; = "BATTERY_CAPACITY"

.field public static final BATTERY_DECISION:Ljava/lang/String; = "BATTERY_DECISION"

.field public static final BATTERY_INBAT_VOLTAGE:Ljava/lang/String; = "BATTERY_INBAT_VOLTAGE"

.field public static final BATTERY_TEMP:Ljava/lang/String; = "BATTERY_TEMP"

.field public static final BATTERY_TEMP_ADC:Ljava/lang/String; = "BATTERY_TEMP_ADC"

.field public static final BATTERY_TEMP_ADC_AVER:Ljava/lang/String; = "BATTERY_TEMP_ADC_AVER"

.field public static final BATTERY_TEMP_ADC_CAL:Ljava/lang/String; = "BATTERY_TEMP_ADC_CAL"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BATTERY_TEMP_AVER:Ljava/lang/String; = "BATTERY_TEMP_AVER"

.field public static final BATTERY_TEST_MODE:Ljava/lang/String; = "BATTERY_TEST_MODE"

.field public static final BATTERY_TYPE:Ljava/lang/String; = "BATTERY_TYPE"

.field public static final BATTERY_UPDATE_BEFORE_READ:Ljava/lang/String; = "BATTERY_UPDATE_BEFORE_READ"

.field public static final BATTERY_VF_ADC:Ljava/lang/String; = "BATTERY_VF_ADC"

.field public static final BATTERY_VF_OCV:Ljava/lang/String; = "BATTERY_VF_OCV"

.field public static final BATTERY_VOLT:Ljava/lang/String; = "BATTERY_VOLT"

.field public static final BATTERY_VOLT_ADC:Ljava/lang/String; = "BATTERY_VOLT_ADC"

.field public static final BATTERY_VOLT_ADC_AVER:Ljava/lang/String; = "BATTERY_VOLT_ADC_AVER"

.field public static final BATTERY_VOLT_ADC_CAL:Ljava/lang/String; = "BATTERY_VOLT_ADC_CAL"

.field public static final BATTERY_VOLT_AVER:Ljava/lang/String; = "BATTERY_VOLT_AVER"

.field public static final BATTRY_FACTORYMODE:Ljava/lang/String; = "BATTRY_FACTORYMODE"

.field public static final BLUETOOTH_ID:Ljava/lang/String; = "BLUETOOTH_ID"

.field public static final CABC_MODE:Ljava/lang/String; = "CABC_MODE"

.field public static final CAMERA_FRONT_FW_VER:Ljava/lang/String; = "CAMERA_FRONT_FW_VER"

.field public static final CAMERA_FRONT_TYPE:Ljava/lang/String; = "CAMERA_FRONT_TYPE"

.field public static final CAMERA_REAR_FW_VER:Ljava/lang/String; = "CAMERA_REAR_FW_VER"

.field public static final CAMERA_REAR_OIS_FW_VER:Ljava/lang/String; = "CAMERA_REAR_OIS_FW_VER"

.field public static final CAMERA_REAR_TYPE:Ljava/lang/String; = "CAMERA_REAR_TYPE"

.field public static final CHECK_KEY_PRESSED:Ljava/lang/String; = "CHECK_KEY_PRESSED"

.field public static final CHG_TEMP:Ljava/lang/String; = "CHG_TEMP"

.field public static final CHG_TEMP_ADC:Ljava/lang/String; = "CHG_TEMP_ADC"

.field public static final CPU_BIG_LITTLE_STATUS:Ljava/lang/String; = "CPU_BIG_LITTLE_STATUS"

.field public static final CPU_CPU0_ONLINE:Ljava/lang/String; = "CPU_CPU0_ONLINE"

.field public static final CPU_CPU1_ONLINE:Ljava/lang/String; = "CPU_CPU1_ONLINE"

.field public static final CPU_CPU2_ONLINE:Ljava/lang/String; = "CPU_CPU2_ONLINE"

.field public static final CPU_CPU3_ONLINE:Ljava/lang/String; = "CPU_CPU3_ONLINE"

.field public static final CPU_HOTPLUG_DISABLE:Ljava/lang/String; = "CPU_HOTPLUG_DISABLE"

.field public static final CPU_MAX_FREQ_SET:Ljava/lang/String; = "CPU_MAX_FREQ_SET"

.field public static final CPU_MIN_FREQ_SET:Ljava/lang/String; = "CPU_MIN_FREQ_SET"

.field public static final CPU_ONLINE:Ljava/lang/String; = "CPU_ONLINE"

.field public static final DEVICE_RAM_SIZE:Ljava/lang/String; = "DEVICE_RAM_SIZE"

.field public static final DEVICE_RAM_SIZE_FILE:Ljava/lang/String; = "DEVICE_RAM_SIZE_FILE"

.field public static final DM_LOG:Ljava/lang/String; = "DM_LOG"

.field public static final DM_PORT:Ljava/lang/String; = "DM_PORT"

.field public static final DVFS_PATH:Ljava/lang/String; = "DVFS_PATH"

.field public static final DYNAMIC_HOTPLUG:Ljava/lang/String; = "DYNAMIC_HOTPLUG"

.field public static final EARJACK_PLUGGED:Ljava/lang/String; = "EARJACK_PLUGGED"

.field public static final EARJACK_SELECT:Ljava/lang/String; = "EARJACK_SELECT"

.field public static final EARJACK_SWITCH_STATE:Ljava/lang/String; = "EARJACK_SWITCH_STATE"

.field public static final ECG_SENSOR_FW_VERSION:Ljava/lang/String; = "ECG_SENSOR_FW_VERSION"

.field public static final EFS_CALDATE_PATH:Ljava/lang/String; = "EFS_CALDATE_PATH"

.field public static final EFS_CARRIER_ROOT_PATH:Ljava/lang/String; = "EFS_CARRIER_ROOT_PATH"

.field public static final EFS_FACTORYAPP_ROOT_PATH:Ljava/lang/String; = "EFS_FACTORYAPP_ROOT_PATH"

.field public static final EFS_HW_PATH:Ljava/lang/String; = "EFS_HW_PATH"

.field public static final EPEN_CHECKSUM_CHECK:Ljava/lang/String; = "EPEN_CHECKSUM_CHECK"

.field public static final EPEN_CHECKSUM_RESULT:Ljava/lang/String; = "EPEN_CHECKSUM_RESULT"

.field public static final EPEN_DIGITIZER_CHECK:Ljava/lang/String; = "EPEN_DIGITIZER_CHECK"

.field public static final EPEN_FIRMWARE_VERSION:Ljava/lang/String; = "EPEN_FIRMWARE_VERSION"

.field public static final ETHERNET_CONNECTION:Ljava/lang/String; = "ETHERNET_CONNECTION"

.field public static final ETHERNET_MAC_ADDRESS:Ljava/lang/String; = "ETHERNET_MAC_ADDRESS"

.field public static final ETHERNET_MAC_ADDRESS_EFS:Ljava/lang/String; = "ETHERNET_MAC_ADDRESS_EFS"

.field public static final EXTERNAL_MEMORY_INSERTED:Ljava/lang/String; = "EXTERNAL_MEMORY_INSERTED"

.field public static final FACTORY_FAILHIST:Ljava/lang/String; = "FACTORY_FAILHIST"

.field public static final FACTORY_FAILHIST_FLS:Ljava/lang/String; = "FACTORY_FAILHIST_FLS"

.field public static final FACTORY_FAILHIST_FRS:Ljava/lang/String; = "FACTORY_FAILHIST_FRS"

.field public static final FACTORY_MODE:Ljava/lang/String; = "FACTORY_MODE"

.field public static final FIVE_BUTTON:Ljava/lang/String; = "FIVE_BUTTON"

.field public static final FLASH_LED_THERM:Ljava/lang/String; = "FLASH_LED_THERM"

.field public static final FUEL_GAUGE_ADJ:Ljava/lang/String; = "FUEL_GAUGE_ADJ"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FUEL_GAUGE_RAW:Ljava/lang/String; = "FUEL_GAUGE_RAW"

.field public static final FUEL_GAUGE_RESET:Ljava/lang/String; = "FUEL_GAUGE_RESET"

.field public static final GEOMAGNETIC_SENSOR_ADC:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_ADC"

.field public static final GEOMAGNETIC_SENSOR_DAC:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_DAC"

.field public static final GEOMAGNETIC_SENSOR_NAME:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_NAME"

.field public static final GEOMAGNETIC_SENSOR_OFFSET_H:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_OFFSET_H"

.field public static final GEOMAGNETIC_SENSOR_POWER:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_POWER"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GEOMAGNETIC_SENSOR_SELFTEST:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_SELFTEST"

.field public static final GEOMAGNETIC_SENSOR_STATUS:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_STATUS"

.field public static final GEOMAGNETIC_SENSOR_TEMP:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_TEMP"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GPS_CNO_INFO:Ljava/lang/String; = "GPS_CNO_INFO"

.field public static final GPS_STARTED:Ljava/lang/String; = "GPS_STARTED"

.field public static final GRIP_SENSOR_CALIBRATION:Ljava/lang/String; = "GRIP_SENSOR_CALIBRATION"

.field public static final GRIP_SENSOR_CALIBRATION_2:Ljava/lang/String; = "GRIP_SENSOR_CALIBRATION"

.field public static final GRIP_SENSOR_ERASECAL:Ljava/lang/String; = "GRIP_SENSOR_ERASECAL"

.field public static final GRIP_SENSOR_MODE:Ljava/lang/String; = "GRIP_SENSOR_MODE"

.field public static final GRIP_SENSOR_NAME:Ljava/lang/String; = "GRIP_SENSOR_NAME"

.field public static final GRIP_SENSOR_OFFSET:Ljava/lang/String; = "GRIP_SENSOR_OFFSET"

.field public static final GRIP_SENSOR_ONOFF:Ljava/lang/String; = "GRIP_SENSOR_ONOFF"

.field public static final GRIP_SENSOR_ONOFF_2:Ljava/lang/String; = "GRIP_SENSOR_ONOFF_2"

.field public static final GRIP_SENSOR_RAWDATA:Ljava/lang/String; = "GRIP_SENSOR_RAWDATA"

.field public static final GRIP_SENSOR_RAWDATA_2:Ljava/lang/String; = "GRIP_SENSOR_RAWDATA"

.field public static final GRIP_SENSOR_RESET:Ljava/lang/String; = "GRIP_SENSOR_RESET"

.field public static final GRIP_SENSOR_THRESHOLD:Ljava/lang/String; = "GRIP_SENSOR_THRESHOLD"

.field public static final GRIP_SENSOR_THRESHOLD_2:Ljava/lang/String; = "GRIP_SENSOR_THRESHOLD_2"

.field public static final GYRO_OIS_DIFF:Ljava/lang/String; = "GYRO_OIS_DIFF"

.field public static final GYRO_OIS_POWER:Ljava/lang/String; = "GYRO_OIS_POWER"

.field public static final GYRO_OIS_RAWDATA:Ljava/lang/String; = "GYRO_OIS_RAWDATA"

.field public static final GYRO_OIS_SELFTEST:Ljava/lang/String; = "GYRO_OIS_SELFTEST"

.field public static final GYRO_SENSOR_NAME:Ljava/lang/String; = "GYRO_SENSOR_NAME"

.field public static final GYRO_SENSOR_POWER_OFF:Ljava/lang/String; = "GYRO_SENSOR_POWER_OFF"

.field public static final GYRO_SENSOR_POWER_ON:Ljava/lang/String; = "GYRO_SENSOR_POWER_ON"

.field public static final GYRO_SENSOR_SELFTEST:Ljava/lang/String; = "GYRO_SENSOR_SELFTEST"

.field public static final GYRO_SENSOR_TEMP:Ljava/lang/String; = "GYRO_SENSOR_TEMP"

.field public static final HDCP_CHECK_1_3_KEYBOX:Ljava/lang/String; = "HDCP_CHECK_1_3_KEYBOX"

.field public static final HDCP_CHECK_2_0:Ljava/lang/String; = "HDCP_CHECK_2_0"

.field public static final HDCP_CHECK_WIDEVINE_KEYBOX:Ljava/lang/String; = "HDCP_CHECK_WIDEVINE_KEYBOX"

.field public static final HDMI_FHD_MODE:Ljava/lang/String; = "HDMI_FHD_MODE"

.field public static final HDMI_PATTERN_SWITCH:Ljava/lang/String; = "HDMI_PATTERN_SWITCH"

.field public static final HDMI_TEST_RESULT:Ljava/lang/String; = "HDMI_TEST_RESULT"

.field public static final HEALTH_COVER_POWER:Ljava/lang/String; = "HEALTH_COVER_POWER"

.field public static final HIDDEN_MENU_BLOCK:Ljava/lang/String; = "HIDDEN_MENU_BLOCK"

.field public static final HMAC_RESULT:Ljava/lang/String; = "HMAC_RESULT"

.field public static final HOT_PLUG_LOCK:Ljava/lang/String; = "HOT_PLUG_LOCK"

.field public static final HRM_EOL_TEST:Ljava/lang/String; = "HRM_EOL_TEST"

.field public static final HRM_EOL_TEST_RESULT:Ljava/lang/String; = "HRM_EOL_TEST_RESULT"

.field public static final HRM_EOL_TEST_STATUS:Ljava/lang/String; = "HRM_EOL_TEST_STATUS"

.field public static final HRM_HR_RANGE2:Ljava/lang/String; = "HRM_HR_RANGE2"

.field public static final HRM_INT_PIN_CHECK:Ljava/lang/String; = "HRM_INT_PIN_CHECK"

.field public static final HRM_LED_CURRENT:Ljava/lang/String; = "HRM_LED_CURRENT"

.field public static final HRM_LED_CURRENT2:Ljava/lang/String; = "HRM_LED_CURRENT2"

.field public static final HRM_MODE_IR:Ljava/lang/String; = "HRM_MODE_IR"

.field public static final HRM_MODE_R:Ljava/lang/String; = "HRM_MODE_R"

.field public static final HRM_SENSOR_NAME:Ljava/lang/String; = "HRM_SENSOR_NAME"

.field public static final HRM_VENDOR:Ljava/lang/String; = "HRM_VENDOR"

.field public static final HUMITEMP_THERMISTER_BATT:Ljava/lang/String; = "HUMITEMP_THERMISTER_BATT"

.field public static final HUMITEMP_THERMISTER_BATT_CELCIUS:Ljava/lang/String; = "HUMITEMP_THERMISTER_BATT_CELCIUS"

.field public static final HUMITEMP_THERMISTER_PAM:Ljava/lang/String; = "HUMITEMP_THERMISTER_PAM"

.field public static final HUMITEMP_THERMISTER_S_HUB_BATT:Ljava/lang/String; = "HUMITEMP_THERMISTER_S_HUB_BATT"

.field public static final HUMITEMP_THERMISTER_S_HUB_BATT_CELCIUS:Ljava/lang/String; = "HUMITEMP_THERMISTER_S_HUB_BATT_CELCIUS"

.field public static final HUMI_TEMP_SENSOR_CAL:Ljava/lang/String; = "HUMI_TEMP_SENSOR_CAL"

.field public static final HUMI_TEMP_SENSOR_RESET:Ljava/lang/String; = "HUMI_TEMP_SENSOR_RESET"

.field public static final HV_CHARGE_CHIP:Ljava/lang/String; = "HV_CHARGE_CHIP"

.field public static final HV_CHARGE_CURRENT:Ljava/lang/String; = "HV_CHARGE_CURRENT"

.field public static final HV_CHARGE_STATUS:Ljava/lang/String; = "HV_CHARGE_STATUS"

.field public static final HV_CHARGE_TYPE:Ljava/lang/String; = "HV_CHARGE_TYPE"

.field public static final HV_POGO_INPUT:Ljava/lang/String; = "HV_POGO_INPUT"

.field public static final ID_CHIP_VERIFY:Ljava/lang/String; = "ID_CHIP_VERIFY"

.field public static final ID_DETECT_TEST:Ljava/lang/String; = "ID_DETECT_TEST"

.field public static final IMEM_DEVICE_SIZE:Ljava/lang/String; = "IMEM_DEVICE_SIZE"

.field public static final IR_CURRENT:Ljava/lang/String; = "IR_CURRENT"

.field public static final IR_LED_RESULT_TX:Ljava/lang/String; = "IR_LED_RESULT_TX"

.field public static final IR_LED_SEND:Ljava/lang/String; = "IR_LED_SEND"

.field public static final IR_LED_SEND_TEST:Ljava/lang/String; = "IR_LED_SEND_TEST"

.field public static final IR_THERMOMETER_SENSOR_VENDOR:Ljava/lang/String; = "IR_THERMOMETER_SENSOR_VENDOR"

.field public static final JIG_CONNECTION_CHECK:Ljava/lang/String; = "JIG_CONNECTION_CHECK"

.field public static final KEYSTRING_BLOCK:Ljava/lang/String; = "KEYSTRING_BLOCK"

.field public static final KEY_PRESSED:Ljava/lang/String; = "KEY_PRESSED"

.field public static final KEY_PRESSED_3X4:Ljava/lang/String; = "KEY_PRESSED_3X4"

.field public static final KEY_PRESSED_POWER:Ljava/lang/String; = "KEY_PRESSED_POWER"

.field public static final LCD_ALPM:Ljava/lang/String; = "LCD_ALPM"

.field public static final LCD_ID:Ljava/lang/String; = "LCD_ID"

.field public static final LCD_POWER:Ljava/lang/String; = "LCD_POWER"

.field public static final LCD_TYPE:Ljava/lang/String; = "LCD_TYPE"

.field public static final LED_BLINK:Ljava/lang/String; = "LED_BLINK"

.field public static final LED_BLUE:Ljava/lang/String; = "LED_BLUE"

.field public static final LED_GREEN:Ljava/lang/String; = "LED_GREEN"

.field public static final LED_RED:Ljava/lang/String; = "LED_RED"

.field public static final LIGHT_SENSOR_ENABLED:Ljava/lang/String; = "LIGHT_SENSOR_ENABLED"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LIGHT_SENSOR_NAME:Ljava/lang/String; = "LIGHT_SENSOR_NAME"

.field public static final LIGHT_SENSOR_RAW:Ljava/lang/String; = "LIGHT_SENSOR_RAW"

.field public static final LPA_MODE_SET:Ljava/lang/String; = "LPA_MODE_SET"

.field public static final LPM_MODE_SET:Ljava/lang/String; = "LPM_MODE_SET"

.field public static final MCD_STATUS_NODE:Ljava/lang/String; = "MCD_STATUS_NODE"

.field public static final MMCBLK_DEVICE_CSD:Ljava/lang/String; = "MMCBLK_DEVICE_CSD"

.field public static final MOTOR_LPF:Ljava/lang/String; = "MOTOR_LPF"

.field public static final MOVINAND_CHECKSUM_DONE:Ljava/lang/String; = "MOVINAND_CHECKSUM_DONE"

.field public static final MOVINAND_CHECKSUM_PASS:Ljava/lang/String; = "MOVINAND_CHECKSUM_PASS"

.field public static final OCTA_CONNECTION_CHECK:Ljava/lang/String; = "OCTA_CONNECTION_CHECK"

.field public static final OTG_MUIC_SET:Ljava/lang/String; = "OTG_MUIC_SET"

.field public static final OTG_TEST_MODE:Ljava/lang/String; = "OTG_TEST_MODE"

.field public static final PA0_THERMISTER_ADC:Ljava/lang/String; = "PA0_THERMISTER_ADC"

.field public static final PA0_THERMISTER_CELCIUS:Ljava/lang/String; = "PA0_THERMISTER_CELCIUS"

.field public static final PA1_THERMISTER_ADC:Ljava/lang/String; = "PA1_THERMISTER_ADC"

.field public static final PA1_THERMISTER_CELCIUS:Ljava/lang/String; = "PA1_THERMISTER_CELCIUS"

.field public static final PATH_BATTERY_CHARGE_COUNT:Ljava/lang/String; = "PATH_BATTERY_CHARGE_COUNT"

.field public static final PATH_HALLIC_STATE:Ljava/lang/String; = "PATH_HALLIC_STATE"

.field public static final PATH_HALLIC_STATE2:Ljava/lang/String; = "PATH_HALLIC_STATE2"

.field public static final PRE_PAY:Ljava/lang/String; = "PRE_PAY"

.field public static final PROXI_SENSOR_ADC:Ljava/lang/String; = "PROXI_SENSOR_ADC"

.field public static final PROXI_SENSOR_ADC_AVG:Ljava/lang/String; = "PROXI_SENSOR_ADC_AVG"

.field public static final PROXI_SENSOR_NAME:Ljava/lang/String; = "PROXI_SENSOR_NAME"

.field public static final PROXI_SENSOR_OFFSET:Ljava/lang/String; = "PROXI_SENSOR_OFFSET"

.field public static final PROXI_SENSOR_OFFSET_PASS:Ljava/lang/String; = "PROXI_SENSOR_OFFSET_PASS"

.field public static final RAM_VERSION:Ljava/lang/String; = "RAM_VERSION"

.field public static final SEC_EXT_THERMISTER_TEMP:Ljava/lang/String; = "SEC_EXT_THERMISTER_TEMP"

.field public static final SENSORHUB_FIRMWARE_VERSION:Ljava/lang/String; = "SENSORHUB_FIRMWARE_VERSION"

.field public static final SENSORHUB_MCU:Ljava/lang/String; = "SENSORHUB_MCU"

.field public static final SERIAL_NO:Ljava/lang/String; = "SERIAL_NO"

.field public static final SLOT1_SWITCH_CTRL:Ljava/lang/String; = "SLOT1_SWITCH_CTRL"

.field public static final SSP_SENSOR:Ljava/lang/String; = "SSP_SENSOR"

.field public static final SWITCH_FACTORY:Ljava/lang/String; = "SWITCH_FACTORY"

.field public static final SWITCH_KERNEL_FTM:Ljava/lang/String; = "SWITCH_KERNEL_FTM"

.field public static final SYSFS_IFWI_VERSION:Ljava/lang/String; = "SYSFS_IFWI_VERSION"

.field public static final SYS_POWER_FTM_SLEEP:Ljava/lang/String; = "SYS_POWER_FTM_SLEEP"

.field public static final TAG:Ljava/lang/String; = "Sysfs"

.field public static final TEMP_HUMID_ENGINE_VER:Ljava/lang/String; = "TEMP_HUMID_ENGINE_VER"

.field public static final TESTSIM_NOTI:Ljava/lang/String; = "TESTSIM_NOTI"

.field public static final TORCH_MODE_FLASH:Ljava/lang/String; = "TORCH_MODE_FLASH"

.field public static final TORCH_MODE_FLASH_OLD:Ljava/lang/String; = "TORCH_MODE_FLASH_OLD"

.field public static final TOUCH_FIRMWARE_UPDATE:Ljava/lang/String; = "TOUCH_FIRMWARE_UPDATE"

.field public static final TOUCH_FIRMWARE_UPDATE_STATUS:Ljava/lang/String; = "TOUCH_FIRMWARE_UPDATE_STATUS"

.field public static final TOUCH_FIRMWARE_VERSION:Ljava/lang/String; = "TOUCH_FIRMWARE_VERSION"

.field public static final TOUCH_KEY_BRIGHTNESS:Ljava/lang/String; = "TOUCH_KEY_BRIGHTNESS"

.field public static final TOUCH_KEY_RECOMMEND_VERSION:Ljava/lang/String; = "TOUCH_KEY_RECOMMEND_VERSION"

.field public static final TOUCH_KEY_SENSITIVITY_APP_SWITCH:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_APP_SWITCH"

.field public static final TOUCH_KEY_SENSITIVITY_BACK:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_BACK"

.field public static final TOUCH_KEY_SENSITIVITY_BACK_INNER:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_BACK_INNER"

.field public static final TOUCH_KEY_SENSITIVITY_BACK_OUTER:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_BACK_OUTER"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN1:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN1"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN2:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN2"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN3:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN3"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN4:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN4"

.field public static final TOUCH_KEY_SENSITIVITY_HOME:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HOME"

.field public static final TOUCH_KEY_SENSITIVITY_MENU:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_MENU"

.field public static final TOUCH_KEY_SENSITIVITY_OK:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_OK"

.field public static final TOUCH_KEY_SENSITIVITY_POWER:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_POWER"

.field public static final TOUCH_KEY_SENSITIVITY_RECENT:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_RECENT"

.field public static final TOUCH_KEY_SENSITIVITY_RECENT_INNER:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_RECENT_INNER"

.field public static final TOUCH_KEY_SENSITIVITY_RECENT_OUTER:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_RECENT_OUTER"

.field public static final TOUCH_KEY_SENSITIVITY_REF1:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_REF1"

.field public static final TOUCH_KEY_SENSITIVITY_REF2:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_REF2"

.field public static final TOUCH_KEY_SENSITIVITY_SEARCH:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_SEARCH"

.field public static final TOUCH_KEY_THRESHOLD:Ljava/lang/String; = "TOUCH_KEY_THRESHOLD"

.field public static final TOUCH_KEY_THRESHOLD_BACK_INNER:Ljava/lang/String; = "TOUCH_KEY_THRESHOLD_BACK_INNER"

.field public static final TOUCH_KEY_THRESHOLD_BACK_OUTER:Ljava/lang/String; = "TOUCH_KEY_THRESHOLD_BACK_OUTER"

.field public static final TOUCH_KEY_THRESHOLD_RECENT_INNER:Ljava/lang/String; = "TOUCH_KEY_THRESHOLD_RECENT_INNER"

.field public static final TOUCH_KEY_THRESHOLD_RECENT_OUTER:Ljava/lang/String; = "TOUCH_KEY_THRESHOLD_RECENT_OUTER"

.field public static final TOUCH_KEY_UPDATED_VERSION:Ljava/lang/String; = "TOUCH_KEY_UPDATED_VERSION"

.field public static final TOUCH_NOISE_DELTA_0:Ljava/lang/String; = "TOUCH_NOISE_DELTA_0"

.field public static final TOUCH_NOISE_DELTA_1:Ljava/lang/String; = "TOUCH_NOISE_DELTA_1"

.field public static final TOUCH_NOISE_DELTA_2:Ljava/lang/String; = "TOUCH_NOISE_DELTA_2"

.field public static final TOUCH_NOISE_DELTA_3:Ljava/lang/String; = "TOUCH_NOISE_DELTA_3"

.field public static final TOUCH_NOISE_DELTA_4:Ljava/lang/String; = "TOUCH_NOISE_DELTA_4"

.field public static final TOUCH_NOISE_DELTA_5:Ljava/lang/String; = "TOUCH_NOISE_DELTA_5"

.field public static final TOUCH_NOISE_DELTA_6:Ljava/lang/String; = "TOUCH_NOISE_DELTA_6"

.field public static final TOUCH_NOISE_REFER_0:Ljava/lang/String; = "TOUCH_NOISE_REFER_0"

.field public static final TOUCH_NOISE_REFER_1:Ljava/lang/String; = "TOUCH_NOISE_REFER_1"

.field public static final TOUCH_NOISE_REFER_2:Ljava/lang/String; = "TOUCH_NOISE_REFER_2"

.field public static final TOUCH_NOISE_REFER_3:Ljava/lang/String; = "TOUCH_NOISE_REFER_3"

.field public static final TOUCH_NOISE_REFER_4:Ljava/lang/String; = "TOUCH_NOISE_REFER_4"

.field public static final TOUCH_NOISE_REFER_5:Ljava/lang/String; = "TOUCH_NOISE_REFER_5"

.field public static final TOUCH_NOISE_REFER_6:Ljava/lang/String; = "TOUCH_NOISE_REFER_6"

.field public static final TOUCH_NOISE_THRESHOLD:Ljava/lang/String; = "TOUCH_NOISE_THRESHOLD"

.field public static final TSK_FACTORY_MODE:Ljava/lang/String; = "TSK_FACTORY_MODE"

.field public static final TSK_FIRMWARE_UPDATE:Ljava/lang/String; = "TSK_FIRMWARE_UPDATE"

.field public static final TSK_FIRMWARE_UPDATE_STATUS:Ljava/lang/String; = "TSK_FIRMWARE_UPDATE_STATUS"

.field public static final TSK_FIRMWARE_VERSION_PANEL:Ljava/lang/String; = "TSK_FIRMWARE_VERSION_PANEL"

.field public static final TSK_FIRMWARE_VERSION_PHONE:Ljava/lang/String; = "TSK_FIRMWARE_VERSION_PHONE"

.field public static final TSP_COMMAND_CMD:Ljava/lang/String; = "TSP_COMMAND_CMD"

.field public static final TSP_COMMAND_CONNECTION:Ljava/lang/String; = "TSP_COMMAND_CONNECTION"

.field public static final TSP_COMMAND_DELTA_DATA:Ljava/lang/String; = "TSP_COMMAND_DELTA_DATA"

.field public static final TSP_COMMAND_DELTA_SET:Ljava/lang/String; = "TSP_COMMAND_DELTA_SET"

.field public static final TSP_COMMAND_NAME:Ljava/lang/String; = "TSP_COMMAND_NAME"

.field public static final TSP_COMMAND_OFF:Ljava/lang/String; = "TSP_COMMAND_OFF"

.field public static final TSP_COMMAND_ON:Ljava/lang/String; = "TSP_COMMAND_ON"

.field public static final TSP_COMMAND_REFER_DATA:Ljava/lang/String; = "TSP_COMMAND_REFER_DATA"

.field public static final TSP_COMMAND_REFER_SET:Ljava/lang/String; = "TSP_COMMAND_REFER_SET"

.field public static final TSP_COMMAND_RESULT:Ljava/lang/String; = "TSP_COMMAND_RESULT"

.field public static final TSP_COMMAND_STATUS:Ljava/lang/String; = "TSP_COMMAND_STATUS"

.field public static final UART_SELECT:Ljava/lang/String; = "UART_SELECT"

.field public static final UFS_DEVICE_SIZE:Ljava/lang/String; = "UFS_DEVICE_SIZE"

.field public static final ULTRASONIC_VER_CHECK:Ljava/lang/String; = "ULTRASONIC_VER_CHECK"

.field public static final USB3_TYPE_CHECK:Ljava/lang/String; = "USB3_TYPE_CHECK"

.field public static final USB_1ST_PORT_CHECK:Ljava/lang/String; = "USB_1ST_PORT_CHECK"

.field public static final USB_1ST_PORT_SUPER_CHECK:Ljava/lang/String; = "USB_1ST_PORT_SUPER_CHECK"

.field public static final USB_2ST_PORT_CHECK:Ljava/lang/String; = "USB_2ST_PORT_CHECK"

.field public static final USB_2ST_PORT_SUPER_CHECK:Ljava/lang/String; = "USB_2ST_PORT_SUPER_CHECK"

.field public static final USB_MENU_SEL:Ljava/lang/String; = "USB_MENU_SEL"

.field public static final USB_SELECT:Ljava/lang/String; = "USB_SELECT"

.field public static final UV_SENSOR_EOL_TEST:Ljava/lang/String; = "UV_SENSOR_EOL_TEST"

.field public static final UV_SENSOR_NAME:Ljava/lang/String; = "UV_SENSOR_NAME"

.field public static final WIFI_ID:Ljava/lang/String; = "WIFI_ID"

.field public static final WIRELESS_BATTERY:Ljava/lang/String; = "WIRELESS_BATTERY"

.field public static final WIRELESS_CHARGE_ONLINE:Ljava/lang/String; = "WIRELESS_CHARGE_ONLINE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getByte(Ljava/lang/String;)B
    .locals 9
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 1382
    const-string v5, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1383
    .local v1, "path":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1384
    .local v4, "value":B
    const/4 v2, 0x0

    .line 1387
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1389
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    if-eqz v3, :cond_0

    .line 1390
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->read()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    int-to-byte v4, v5

    .line 1395
    :cond_0
    if-eqz v3, :cond_3

    .line 1397
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 1404
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    const-string v5, "Support"

    const-string v6, "Kernel.read"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1405
    return v4

    .line 1398
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 1399
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v2, v3

    .line 1400
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1392
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1393
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1395
    if-eqz v2, :cond_1

    .line 1397
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1398
    :catch_2
    move-exception v0

    .line 1399
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1395
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_2

    .line 1397
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1400
    :cond_2
    :goto_3
    throw v5

    .line 1398
    :catch_3
    move-exception v0

    .line 1399
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 1395
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1392
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static getFileLength(Ljava/lang/String;)J
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1731
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 1378
    const-string v0, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1721
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1722
    .local v0, "f":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isExistFileID(Ljava/lang/String;)Z
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 1726
    new-instance v0, Ljava/io/File;

    const-string v1, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1727
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public static mkDir(Ljava/lang/String;)Z
    .locals 8
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1735
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 1736
    const-string v4, "Support"

    const-string v5, "Kernel.mkDir"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1737
    const-string v4, "/"

    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 1738
    .local v2, "index":I
    const-string v4, "Support"

    const-string v5, "Kernel.mkDir"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "index="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1739
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1740
    .local v0, "dir":Ljava/lang/String;
    const-string v3, "Support"

    const-string v4, "Kernel.mkDir"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dir="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1741
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1743
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1744
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v3

    .line 1750
    .end local v0    # "dir":Ljava/lang/String;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "index":I
    :cond_0
    :goto_0
    return v3

    .line 1746
    .restart local v0    # "dir":Ljava/lang/String;
    .restart local v1    # "f":Ljava/io/File;
    .restart local v2    # "index":I
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static read(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 1560
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static read(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "isLog"    # Z

    .prologue
    .line 1470
    if-eqz p1, :cond_1

    .line 1471
    const-string v8, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v8}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1476
    .local v3, "path":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x0

    .line 1477
    .local v6, "value":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1478
    .local v4, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 1480
    .local v1, "freader":Ljava/io/FileReader;
    if-eqz v3, :cond_0

    const-string v8, "Unknown"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    :cond_0
    move-object v7, v6

    .line 1514
    .end local v6    # "value":Ljava/lang/String;
    .local v7, "value":Ljava/lang/String;
    :goto_1
    return-object v7

    .line 1473
    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .end local v7    # "value":Ljava/lang/String;
    :cond_1
    const-string v8, "path"

    const/4 v9, 0x0

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p0, v8, v9}, Lcom/sec/factory/support/Support$Values;->access$700(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_0

    .line 1485
    .restart local v1    # "freader":Ljava/io/FileReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "value":Ljava/lang/String;
    :cond_2
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1487
    .end local v1    # "freader":Ljava/io/FileReader;
    .local v2, "freader":Ljava/io/FileReader;
    if-eqz v2, :cond_3

    .line 1488
    :try_start_1
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v5, "reader":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 1491
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_3
    if-eqz v4, :cond_4

    .line 1492
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 1494
    if-eqz v6, :cond_4

    .line 1495
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 1501
    :cond_4
    if-eqz v4, :cond_8

    .line 1503
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1510
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    :cond_5
    :goto_2
    if-eqz p1, :cond_6

    .line 1511
    const-string v8, "Support"

    const-string v9, "Kernel.read"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "path="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", value="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v7, v6

    .line 1514
    .end local v6    # "value":Ljava/lang/String;
    .restart local v7    # "value":Ljava/lang/String;
    goto :goto_1

    .line 1504
    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v7    # "value":Ljava/lang/String;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v6    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1505
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v1, v2

    .line 1506
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_2

    .line 1498
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1499
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1501
    if-eqz v4, :cond_5

    .line 1503
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 1504
    :catch_2
    move-exception v0

    .line 1505
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2

    .line 1501
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_4
    if-eqz v4, :cond_7

    .line 1503
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1506
    :cond_7
    :goto_5
    throw v8

    .line 1504
    :catch_3
    move-exception v0

    .line 1505
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_5

    .line 1501
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_4

    .line 1498
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_3

    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :cond_8
    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_2
.end method

.method public static readAllLine(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 13
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "isLog"    # Z

    .prologue
    .line 1411
    if-eqz p1, :cond_1

    .line 1412
    const-string v9, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v9}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1417
    .local v3, "path":Ljava/lang/String;
    :goto_0
    const-string v7, ""

    .line 1418
    .local v7, "value":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1419
    .local v6, "temp_value":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1420
    .local v4, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 1422
    .local v1, "freader":Ljava/io/FileReader;
    if-eqz v3, :cond_0

    const-string v9, "Unknown"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_0
    move-object v8, v7

    .line 1464
    .end local v7    # "value":Ljava/lang/String;
    .local v8, "value":Ljava/lang/String;
    :goto_1
    return-object v8

    .line 1414
    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .end local v6    # "temp_value":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_1
    const-string v9, "path"

    const/4 v10, 0x0

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p0, v9, v10}, Lcom/sec/factory/support/Support$Values;->access$700(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_0

    .line 1427
    .restart local v1    # "freader":Ljava/io/FileReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "temp_value":Ljava/lang/String;
    .restart local v7    # "value":Ljava/lang/String;
    :cond_2
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1429
    .end local v1    # "freader":Ljava/io/FileReader;
    .local v2, "freader":Ljava/io/FileReader;
    if-eqz v2, :cond_3

    .line 1430
    :try_start_1
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v5, "reader":Ljava/io/BufferedReader;
    move-object v4, v5

    .line 1433
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_3
    if-eqz v4, :cond_4

    .line 1435
    :goto_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 1437
    if-nez v6, :cond_7

    .line 1444
    if-eqz v7, :cond_4

    .line 1445
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 1451
    :cond_4
    if-eqz v4, :cond_9

    .line 1453
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1460
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    :cond_5
    :goto_3
    if-eqz p1, :cond_6

    .line 1461
    const-string v9, "Support"

    const-string v10, "Kernel.readAllLine"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "path="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", value="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v8, v7

    .line 1464
    .end local v7    # "value":Ljava/lang/String;
    .restart local v8    # "value":Ljava/lang/String;
    goto :goto_1

    .line 1441
    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v8    # "value":Ljava/lang/String;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v7    # "value":Ljava/lang/String;
    :cond_7
    :try_start_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v7

    goto :goto_2

    .line 1454
    :catch_0
    move-exception v0

    .line 1455
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v1, v2

    .line 1456
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_3

    .line 1448
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1449
    .local v0, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_4
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1451
    if-eqz v4, :cond_5

    .line 1453
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    .line 1454
    :catch_2
    move-exception v0

    .line 1455
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 1451
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_5
    if-eqz v4, :cond_8

    .line 1453
    :try_start_6
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1456
    :cond_8
    :goto_6
    throw v9

    .line 1454
    :catch_3
    move-exception v0

    .line 1455
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_6

    .line 1451
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catchall_1
    move-exception v9

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_5

    .line 1448
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_4

    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :cond_9
    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_3
.end method

.method public static readFromPath(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 11
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "isLog"    # Z

    .prologue
    .line 1518
    const/4 v5, 0x0

    .line 1519
    .local v5, "value":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1520
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 1522
    .local v1, "freader":Ljava/io/FileReader;
    if-eqz p0, :cond_0

    const-string v7, "Unknown"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    move-object v6, v5

    .line 1556
    .end local v5    # "value":Ljava/lang/String;
    .local v6, "value":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 1527
    .end local v6    # "value":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1529
    .end local v1    # "freader":Ljava/io/FileReader;
    .local v2, "freader":Ljava/io/FileReader;
    if-eqz v2, :cond_2

    .line 1530
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 1533
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_2
    if-eqz v3, :cond_3

    .line 1534
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 1536
    if-eqz v5, :cond_3

    .line 1537
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 1543
    :cond_3
    if-eqz v3, :cond_7

    .line 1545
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1552
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    :cond_4
    :goto_1
    if-eqz p1, :cond_5

    .line 1553
    const-string v7, "Support"

    const-string v8, "Kernel.read"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "path="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", value="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v6, v5

    .line 1556
    .end local v5    # "value":Ljava/lang/String;
    .restart local v6    # "value":Ljava/lang/String;
    goto :goto_0

    .line 1546
    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v6    # "value":Ljava/lang/String;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v5    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1547
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v1, v2

    .line 1548
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_1

    .line 1540
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1541
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1543
    if-eqz v3, :cond_4

    .line 1545
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 1546
    :catch_2
    move-exception v0

    .line 1547
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1543
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v3, :cond_6

    .line 1545
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1548
    :cond_6
    :goto_4
    throw v7

    .line 1546
    :catch_3
    move-exception v0

    .line 1547
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_4

    .line 1543
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_3

    .line 1540
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_2

    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :cond_7
    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_1
.end method

.method public static setPermission(Ljava/lang/String;ZZZZZZ)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "excutable"    # Z
    .param p2, "owneronlyE"    # Z
    .param p3, "writable"    # Z
    .param p4, "owneronlyW"    # Z
    .param p5, "readable"    # Z
    .param p6, "owneronlyR"    # Z

    .prologue
    .line 1756
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1758
    .local v0, "f":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1759
    invoke-virtual {v0, p1, p2}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 1760
    invoke-virtual {v0, p3, p4}, Ljava/io/File;->setWritable(ZZ)Z

    .line 1761
    invoke-virtual {v0, p5, p6}, Ljava/io/File;->setReadable(ZZ)Z

    .line 1767
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1763
    :cond_0
    const-string v1, "Support"

    const-string v2, "setPermission"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File not found : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1764
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static write(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1564
    const-string v5, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1565
    .local v1, "path":Ljava/lang/String;
    const/4 v2, 0x1

    .line 1566
    .local v2, "res":Z
    const/4 v3, 0x0

    .line 1568
    .local v3, "writer":Ljava/io/FileWriter;
    const-string v5, "Support"

    const-string v6, "Kernel.write start"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    :try_start_0
    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1572
    .end local v3    # "writer":Ljava/io/FileWriter;
    .local v4, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v4, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1579
    if-eqz v4, :cond_0

    .line 1580
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v3, v4

    .line 1588
    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    const-string v5, "Support"

    const-string v6, "Kernel.write end"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1589
    return v2

    .line 1582
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 1583
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1584
    const/4 v2, 0x0

    move-object v3, v4

    .line 1586
    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_0

    .line 1574
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1575
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1576
    const/4 v2, 0x0

    .line 1579
    if-eqz v3, :cond_1

    .line 1580
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1582
    :catch_2
    move-exception v0

    .line 1583
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1584
    const/4 v2, 0x0

    .line 1586
    goto :goto_0

    .line 1578
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 1579
    :goto_2
    if-eqz v3, :cond_2

    .line 1580
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1585
    :cond_2
    :goto_3
    throw v5

    .line 1582
    :catch_3
    move-exception v0

    .line 1583
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1584
    const/4 v2, 0x0

    goto :goto_3

    .line 1578
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 1574
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public static write(Ljava/lang/String;[B)Z
    .locals 9
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "value"    # [B

    .prologue
    .line 1593
    const-string v5, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1594
    .local v3, "path":Ljava/lang/String;
    const/4 v4, 0x1

    .line 1595
    .local v4, "res":Z
    const/4 v1, 0x0

    .line 1598
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1599
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 1600
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1607
    if-eqz v2, :cond_0

    .line 1608
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 1616
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v6, "Support"

    const-string v7, "Kernel.write"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", value="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p1, :cond_3

    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1617
    return v4

    .line 1610
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 1611
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1612
    const/4 v4, 0x0

    move-object v1, v2

    .line 1614
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 1602
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1603
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1604
    const/4 v4, 0x0

    .line 1607
    if-eqz v1, :cond_1

    .line 1608
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1610
    :catch_2
    move-exception v0

    .line 1611
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1612
    const/4 v4, 0x0

    .line 1614
    goto :goto_0

    .line 1606
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 1607
    :goto_3
    if-eqz v1, :cond_2

    .line 1608
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1613
    :cond_2
    :goto_4
    throw v5

    .line 1610
    :catch_3
    move-exception v0

    .line 1611
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1612
    const/4 v4, 0x0

    goto :goto_4

    .line 1616
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    const-string v5, "null"

    goto :goto_1

    .line 1606
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1602
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static writeNsync(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1676
    if-nez p0, :cond_0

    .line 1677
    const-string v7, "Support"

    const-string v8, "Kernel.writeNsync"

    const-string v9, "id=null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 1717
    :goto_0
    return v5

    .line 1681
    :cond_0
    if-nez p1, :cond_1

    .line 1682
    const-string v7, "Support"

    const-string v8, "Kernel.writeNsync"

    const-string v9, "value=null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 1683
    goto :goto_0

    .line 1686
    :cond_1
    const-string v7, "path"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v7}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1687
    .local v4, "path":Ljava/lang/String;
    const/4 v5, 0x1

    .line 1688
    .local v5, "res":Z
    const/4 v2, 0x0

    .line 1689
    .local v2, "out":Ljava/io/FileOutputStream;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 1691
    .local v0, "bValue":[B
    if-nez v0, :cond_2

    .line 1692
    const-string v7, "Support"

    const-string v8, "Kernel.writeNsync"

    const-string v9, "bValue=null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 1693
    goto :goto_0

    .line 1697
    :cond_2
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 1699
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1700
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1706
    if-eqz v3, :cond_3

    .line 1707
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    move-object v2, v3

    .line 1715
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :cond_4
    :goto_1
    const-string v6, "Support"

    const-string v7, "Kernel.writeNsync"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "path="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", value="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "res="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1709
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 1710
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1711
    const/4 v5, 0x0

    move-object v2, v3

    .line 1713
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 1701
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1702
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1703
    const/4 v5, 0x0

    .line 1706
    if-eqz v2, :cond_4

    .line 1707
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 1709
    :catch_2
    move-exception v1

    .line 1710
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1711
    const/4 v5, 0x0

    .line 1713
    goto :goto_1

    .line 1705
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 1706
    :goto_3
    if-eqz v2, :cond_5

    .line 1707
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1712
    :cond_5
    :goto_4
    throw v6

    .line 1709
    :catch_3
    move-exception v1

    .line 1710
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1711
    const/4 v5, 0x0

    goto :goto_4

    .line 1705
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1701
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static writeToPath(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1648
    const/4 v1, 0x1

    .line 1649
    .local v1, "res":Z
    const/4 v2, 0x0

    .line 1651
    .local v2, "writer":Ljava/io/FileWriter;
    const-string v4, "Support"

    const-string v5, "Kernel.writeToPath start"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1655
    .end local v2    # "writer":Ljava/io/FileWriter;
    .local v3, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1662
    if-eqz v3, :cond_0

    .line 1663
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 1671
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    const-string v4, "Support"

    const-string v5, "Kernel.writeToPath end"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    return v1

    .line 1665
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 1666
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1667
    const/4 v1, 0x0

    move-object v2, v3

    .line 1669
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_0

    .line 1657
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1658
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1659
    const/4 v1, 0x0

    .line 1662
    if-eqz v2, :cond_1

    .line 1663
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1665
    :catch_2
    move-exception v0

    .line 1666
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1667
    const/4 v1, 0x0

    .line 1669
    goto :goto_0

    .line 1661
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 1662
    :goto_2
    if-eqz v2, :cond_2

    .line 1663
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1668
    :cond_2
    :goto_3
    throw v4

    .line 1665
    :catch_3
    move-exception v0

    .line 1666
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1667
    const/4 v1, 0x0

    goto :goto_3

    .line 1661
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 1657
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public static writeToPath(Ljava/lang/String;[B)Z
    .locals 8
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # [B

    .prologue
    .line 1621
    const/4 v3, 0x1

    .line 1622
    .local v3, "res":Z
    const/4 v1, 0x0

    .line 1625
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1626
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 1627
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1634
    if-eqz v2, :cond_0

    .line 1635
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 1643
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v5, "Support"

    const-string v6, "Kernel.write"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", value="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz p1, :cond_3

    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1644
    return v3

    .line 1637
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 1638
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1639
    const/4 v3, 0x0

    move-object v1, v2

    .line 1641
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 1629
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1630
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1631
    const/4 v3, 0x0

    .line 1634
    if-eqz v1, :cond_1

    .line 1635
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1637
    :catch_2
    move-exception v0

    .line 1638
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1639
    const/4 v3, 0x0

    .line 1641
    goto :goto_0

    .line 1633
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 1634
    :goto_3
    if-eqz v1, :cond_2

    .line 1635
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1640
    :cond_2
    :goto_4
    throw v4

    .line 1637
    :catch_3
    move-exception v0

    .line 1638
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1639
    const/4 v3, 0x0

    goto :goto_4

    .line 1643
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    const-string v4, "null"

    goto :goto_1

    .line 1633
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1629
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method
