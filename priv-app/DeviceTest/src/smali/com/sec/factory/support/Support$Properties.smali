.class public Lcom/sec/factory/support/Support$Properties;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Properties"
.end annotation


# static fields
.field public static final BINARY_TYPE:Ljava/lang/String; = "BINARY_TYPE"

.field public static final BOOTLOADER_VERSION:Ljava/lang/String; = "BOOTLOADER_VERSION"

.field public static final BOOT_COMPLETED:Ljava/lang/String; = "BOOT_COMPLETED"

.field public static final BOOT_VERSION:Ljava/lang/String; = "BOOT_VERSION"

.field public static final CONTENTS_VERSION:Ljava/lang/String; = "CONTENTS_VERSION"

.field public static final CPNV_BACKUP_CMD:Ljava/lang/String; = "CPNV_BACKUP_CMD"

.field public static final CSC_VERSION:Ljava/lang/String; = "CSC_VERSION"

.field public static final DUALMODE_STATE:Ljava/lang/String; = "DUALMODE_STATE"

.field public static final EMMC_CHECKSUM:Ljava/lang/String; = "EMMC_CHECKSUM"

.field public static final FACTORYAPP_FAILHIST:Ljava/lang/String; = "FACTORYAPP_FAILHIST"

.field public static final FACTORYAPP_FAILHIST_FLS:Ljava/lang/String; = "FACTORYAPP_FAILHIST_FLS"

.field public static final FACTORYAPP_FAILHIST_TIME:Ljava/lang/String; = "FACTORYAPP_FAILHIST_TIME"

.field public static final FAILDUMP_CP:Ljava/lang/String; = "FAILDUMP_CP"

.field public static final FAILDUMP_TIME_CP_CRASH:Ljava/lang/String; = "FAILDUMP_TIME_CP_CRASH"

.field public static final HDCP_SN:Ljava/lang/String; = "HDCP_SN"

.field public static final HW_REVISION:Ljava/lang/String; = "HW_REVISION"

.field public static final HW_VERSION:Ljava/lang/String; = "HW_VERSION"

.field public static final IMEI_ID:Ljava/lang/String; = "IMEI_ID"

.field public static final IMSI:Ljava/lang/String; = "IMSI"

.field public static final ISERIAL_NO:Ljava/lang/String; = "ISERIAL_NO"

.field public static final LTE_HIDDEN_VERSION:Ljava/lang/String; = "LTE_HIDDEN_VERSION"

.field public static final LTE_VERSION:Ljava/lang/String; = "LTE_VERSION"

.field public static final MAIN_CHIP_NAME_AP:Ljava/lang/String; = "MAIN_CHIP_NAME_AP"

.field public static final MAIN_CHIP_NAME_AP_REAL:Ljava/lang/String; = "MAIN_CHIP_NAME_AP_REAL"

.field public static final MAIN_CHIP_NAME_CP:Ljava/lang/String; = "MAIN_CHIP_NAME_CP"

.field public static final MAIN_CHIP_NAME_CP2:Ljava/lang/String; = "MAIN_CHIP_NAME_CP2"

.field public static final PDA_VERSION:Ljava/lang/String; = "PDA_VERSION"

.field public static final PGM_CCOMMAND:Ljava/lang/String; = "PGM_CCOMMAND"

.field public static final PGM_STAGE:Ljava/lang/String; = "PGM_STAGE"

.field public static final PHONE_VERSION:Ljava/lang/String; = "PHONE_VERSION"

.field public static final PRODUCT_NAME:Ljava/lang/String; = "PRODUCT_NAME"

.field public static final PROPERTIES_DEFAULT_STRING:Ljava/lang/String; = "Unknown"

.field public static final STANDARD_VERSION:Ljava/lang/String; = "STANDARD_VERSION"

.field public static final SW_HIDDEN_VERSION:Ljava/lang/String; = "SW_HIDDEN_VERSION"

.field public static final SW_VERSION:Ljava/lang/String; = "SW_VERSION"

.field public static final TAG:Ljava/lang/String; = "Properties"

.field public static final WARRANTY_BIT_CHECK:Ljava/lang/String; = "WARRANTY_BIT_CHECK"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 932
    const-string v1, "key"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 933
    .local v0, "property":Ljava/lang/String;
    const-string v1, "Support"

    const-string v2, "Properties.get"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "property="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 938
    const-string v1, "key"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 939
    .local v0, "property":Ljava/lang/String;
    const-string v1, "Support"

    const-string v2, "Properties.set"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "property="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", value="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    return-void
.end method
