.class public Lcom/sec/factory/support/Support$Version;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Version"
.end annotation


# static fields
.field public static final FACTORY_TEST_APP:Ljava/lang/String; = "FACTORY_TEST_APP"

.field public static final FACTORY_TEST_COMMAND:Ljava/lang/String; = "FACTORY_TEST_COMMAND"

.field public static final FAILHIST_VERSION:Ljava/lang/String; = "FAILHIST_VERSION"

.field public static final TAG:Ljava/lang/String; = "Version"

.field public static final XML_DOCUMENT:Ljava/lang/String; = "XML_DOCUMENT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 33
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
