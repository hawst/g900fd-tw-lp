.class public Lcom/sec/factory/support/NVAccessor;
.super Ljava/lang/Object;
.source "NVAccessor.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "NVAccessor"

.field public static final NV_VALUE_ENTER:B = 0x45t

.field public static final NV_VALUE_FAIL:B = 0x46t

.field public static final NV_VALUE_NOTEST:B = 0x4et

.field public static final NV_VALUE_PASS:B = 0x50t

.field private static mNVAccessor:Lcom/sec/factory/support/NVAccessor;

.field private static mSuccessLibLoad:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    .line 8
    new-instance v1, Lcom/sec/factory/support/NVAccessor;

    invoke-direct {v1}, Lcom/sec/factory/support/NVAccessor;-><init>()V

    sput-object v1, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    .line 23
    :try_start_0
    const-string v1, "nvaccessor_fb"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    .local v0, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 24
    .end local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 25
    .restart local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "NVAccessor"

    const-string v2, "WARNING: Could not load libnvaccessor"

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static getFullTestNV()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v2

    if-nez v2, :cond_1

    .line 53
    const-string v2, "NVAccessor"

    const-string v3, "getFullTestNV"

    const-string v4, "N/A"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    :goto_0
    return-object v1

    .line 57
    :cond_1
    sget-boolean v2, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 59
    :try_start_0
    sget-object v2, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    invoke-virtual {v2}, Lcom/sec/factory/support/NVAccessor;->nativeGetFullTestNV()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 60
    .local v1, "value":Ljava/lang/String;
    goto :goto_0

    .line 61
    .end local v1    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "NVAccessor"

    const-string v3, "Can not access NV, please check the libomission_avoidance.so"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static getNV(I)B
    .locals 4
    .param p0, "key"    # I

    .prologue
    .line 34
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v2

    if-eqz v2, :cond_1

    if-lez p0, :cond_1

    .line 35
    sget-boolean v2, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 37
    :try_start_0
    sget-object v2, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    invoke-virtual {v2, p0}, Lcom/sec/factory/support/NVAccessor;->nativeGetNV(I)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 39
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    .line 48
    .end local v1    # "value":Ljava/lang/String;
    :goto_0
    return v2

    .line 41
    .restart local v1    # "value":Ljava/lang/String;
    :cond_0
    const-string v2, "NVAccessor"

    const-string v3, "Can not access NV, please check the libomission_avoidance.so"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v1    # "value":Ljava/lang/String;
    :cond_1
    :goto_1
    const/16 v2, 0x4e

    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public static getNVHistory()Ljava/lang/String;
    .locals 3

    .prologue
    .line 107
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    sget-boolean v1, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 109
    sget-object v1, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    invoke-virtual {v1}, Lcom/sec/factory/support/NVAccessor;->nativeGetNVHistory()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 111
    const-string v1, "NVAccessor"

    const-string v2, "Can not access NV, please check the libomission_avoidance.so"

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    :goto_0
    return-object v0

    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setFullTestNV(Ljava/lang/String;)I
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 87
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v2

    if-nez v2, :cond_1

    .line 88
    const-string v2, "NVAccessor"

    const-string v3, "setFullTestNV"

    const-string v4, "N/A"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    sget-boolean v2, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 94
    sget-object v2, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    invoke-virtual {v2, p0}, Lcom/sec/factory/support/NVAccessor;->nativeSetFullTestNV(Ljava/lang/String;)I

    move-result v0

    .line 95
    .local v0, "retValue":I
    if-gez v0, :cond_2

    .line 96
    const-string v2, "NVAccessor"

    const-string v3, "Can not access NV, please check the libomission_avoidance.so"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setLocalTestNV(Ljava/lang/String;IB)Ljava/lang/String;
    .locals 7
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "key"    # I
    .param p2, "value"    # B

    .prologue
    const/4 v2, 0x0

    .line 134
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x3fc

    if-eq v3, v4, :cond_1

    .line 135
    :cond_0
    const-string v3, "NVAccessor"

    const-string v4, "setLocalTestNV"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid source = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :goto_0
    return-object v2

    .line 139
    :cond_1
    if-lez p1, :cond_2

    const/16 v3, 0xff

    if-lt p1, v3, :cond_3

    .line 140
    :cond_2
    const-string v3, "NVAccessor"

    const-string v4, "setLocalTestNV"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid key = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_3
    const/16 v3, 0x4e

    if-eq p2, v3, :cond_4

    const/16 v3, 0x50

    if-eq p2, v3, :cond_4

    const/16 v3, 0x45

    if-eq p2, v3, :cond_4

    const/16 v3, 0x46

    if-eq p2, v3, :cond_4

    .line 146
    const-string v3, "NVAccessor"

    const-string v4, "setLocalTestNV"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid value = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 150
    :cond_4
    const/4 v2, 0x0

    add-int/lit8 v3, p1, -0x1

    mul-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "front":Ljava/lang/String;
    mul-int/lit8 v2, p1, 0x4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "rear":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-char v3, p2

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static setNV(IB)I
    .locals 4
    .param p0, "key"    # I
    .param p1, "value"    # B

    .prologue
    const/4 v1, -0x1

    .line 71
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    sget-boolean v2, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 73
    sget-object v2, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    int-to-char v3, p1

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lcom/sec/factory/support/NVAccessor;->nativeSetNV(ILjava/lang/String;)I

    move-result v0

    .line 74
    .local v0, "retValue":I
    if-gez v0, :cond_1

    .line 75
    const-string v2, "NVAccessor"

    const-string v3, "Can not access NV, please check the libomission_avoidance.so"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .end local v0    # "retValue":I
    :cond_0
    :goto_0
    return v1

    .line 78
    .restart local v0    # "retValue":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static supportExpandedNV()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 122
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    sget-boolean v1, Lcom/sec/factory/support/NVAccessor;->mSuccessLibLoad:Z

    if-ne v1, v0, :cond_0

    .line 124
    sget-object v1, Lcom/sec/factory/support/NVAccessor;->mNVAccessor:Lcom/sec/factory/support/NVAccessor;

    invoke-virtual {v1}, Lcom/sec/factory/support/NVAccessor;->nativeSupportExpandedNV()Z

    move-result v0

    .line 125
    .local v0, "result":Z
    const-string v1, "NVAccessor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SupportExpandedNV = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    return v0
.end method


# virtual methods
.method native nativeGetFullTestNV()Ljava/lang/String;
.end method

.method native nativeGetNV(I)Ljava/lang/String;
.end method

.method native nativeGetNVHistory()Ljava/lang/String;
.end method

.method native nativeSetFullTestNV(Ljava/lang/String;)I
.end method

.method native nativeSetNV(ILjava/lang/String;)I
.end method

.method native nativeSupportExpandedNV()Z
.end method
