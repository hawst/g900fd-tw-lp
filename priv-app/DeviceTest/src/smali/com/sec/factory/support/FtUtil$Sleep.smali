.class public Lcom/sec/factory/support/FtUtil$Sleep;
.super Ljava/lang/Object;
.source "FtUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/FtUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sleep"
.end annotation


# instance fields
.field final _lock:Ljava/lang/Object;

.field private _timeout:J


# direct methods
.method private constructor <init>(J)V
    .locals 3
    .param p1, "timeout"    # J

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/support/FtUtil$Sleep;->_lock:Ljava/lang/Object;

    .line 398
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/factory/support/FtUtil$Sleep;->_timeout:J

    .line 401
    iput-wide p1, p0, Lcom/sec/factory/support/FtUtil$Sleep;->_timeout:J

    .line 402
    return-void
.end method

.method public static sleep(J)V
    .locals 6
    .param p0, "timeout"    # J

    .prologue
    .line 416
    const-string v2, "FtUtil"

    const-string v3, "sleep start"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "timeout: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/factory/support/FtUtil$Sleep;

    invoke-direct {v2, p0, p1}, Lcom/sec/factory/support/FtUtil$Sleep;-><init>(J)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 420
    .local v1, "thread":Ljava/lang/Thread;
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 421
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 426
    :goto_0
    const-string v2, "FtUtil"

    const-string v3, "sleep end"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "timeout: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    return-void

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 406
    iget-object v2, p0, Lcom/sec/factory/support/FtUtil$Sleep;->_lock:Ljava/lang/Object;

    monitor-enter v2

    .line 408
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/support/FtUtil$Sleep;->_lock:Ljava/lang/Object;

    iget-wide v4, p0, Lcom/sec/factory/support/FtUtil$Sleep;->_timeout:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 413
    return-void

    .line 409
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 412
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
