.class public Lcom/sec/factory/support/Support$Spec;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Spec"
.end annotation


# static fields
.field public static final AFC_CURRRENT_SPEC_MAX:Ljava/lang/String; = "AFC_CURRRENT_SPEC_MAX"

.field public static final AFC_CURRRENT_SPEC_MIN:Ljava/lang/String; = "AFC_CURRRENT_SPEC_MIN"

.field public static final ECG_DYNAMIC_RANGE_MAX:Ljava/lang/String; = "ECG_DYNAMIC_RANGE_MAX"

.field public static final ECG_DYNAMIC_RANGE_MIN:Ljava/lang/String; = "ECG_DYNAMIC_RANGE_MIN"

.field public static final ECG_FREQUENCY_MAX:Ljava/lang/String; = "ECG_FREQUENCY_MAX"

.field public static final ECG_FREQUENCY_MIN:Ljava/lang/String; = "ECG_FREQUENCY_MIN"

.field public static final ECG_HR_100_MAX:Ljava/lang/String; = "ECG_HR_100_MAX"

.field public static final ECG_HR_100_MIN:Ljava/lang/String; = "ECG_HR_100_MIN"

.field public static final ECG_HR_200_MAX:Ljava/lang/String; = "ECG_HR_200_MAX"

.field public static final ECG_HR_200_MIN:Ljava/lang/String; = "ECG_HR_200_MIN"

.field public static final ECG_HR_50_MAX:Ljava/lang/String; = "ECG_HR_50_MAX"

.field public static final ECG_HR_50_MIN:Ljava/lang/String; = "ECG_HR_50_MIN"

.field public static final ECG_HR_70_MAX:Ljava/lang/String; = "ECG_HR_70_MAX"

.field public static final ECG_HR_70_MIN:Ljava/lang/String; = "ECG_HR_70_MIN"

.field public static final ECG_SELFTEST_SPEC_MAX:Ljava/lang/String; = "ECG_SELFTEST_SPEC_MAX"

.field public static final ECG_SELFTEST_SPEC_MIN:Ljava/lang/String; = "ECG_SELFTEST_SPEC_MIN"

.field public static final FINGERPRINT_MRM_PIXEL_END:Ljava/lang/String; = "FINGERPRINT_MRM_PIXEL_END"

.field public static final FINGERPRINT_MRM_PIXEL_START:Ljava/lang/String; = "FINGERPRINT_MRM_PIXEL_START"

.field public static final FINGERPRINT_MRM_SPEC_AVG_MIN:Ljava/lang/String; = "FINGERPRINT_MRM_SPEC_AVG_MIN"

.field public static final FINGERPRINT_MRM_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_MRM_SPEC_MAX"

.field public static final FINGERPRINT_MRM_SPEC_MIN:Ljava/lang/String; = "FINGERPRINT_MRM_SPEC_MIN"

.field public static final FINGERPRINT_PRIMARY_PIXEL_END:Ljava/lang/String; = "FINGERPRINT_PRIMARY_PIXEL_END"

.field public static final FINGERPRINT_PRIMARY_PIXEL_START:Ljava/lang/String; = "FINGERPRINT_PRIMARY_PIXEL_START"

.field public static final FINGERPRINT_PRIMARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_PRIMARY_SPEC_AVG"

.field public static final FINGERPRINT_PRIMARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_PRIMARY_SPEC_MAX"

.field public static final FINGERPRINT_PRIMARY_SPEC_MIN:Ljava/lang/String; = "FINGERPRINT_PRIMARY_SPEC_MIN"

.field public static final FINGERPRINT_SECONDARY_PIXEL_END:Ljava/lang/String; = "FINGERPRINT_SECONDARY_PIXEL_END"

.field public static final FINGERPRINT_SECONDARY_PIXEL_START:Ljava/lang/String; = "FINGERPRINT_SECONDARY_PIXEL_START"

.field public static final FINGERPRINT_SECONDARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_SECONDARY_SPEC_AVG"

.field public static final FINGERPRINT_SECONDARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_SECONDARY_SPEC_MAX"

.field public static final FINGERPRINT_SECONDARY_SPEC_MIN:Ljava/lang/String; = "FINGERPRINT_SECONDARY_SPEC_MIN"

.field public static final FINGERPRINT_STDEV_PRIMARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_PRIMARY_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_PRIMARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_PRIMARY_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SECONDARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SECONDARY_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SECONDARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SECONDARY_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SECTION1_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION1_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SECTION1_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION1_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SECTION2_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION2_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SECTION2_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION2_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SPEC_MAX"

.field public static final GESTURESENSOR_IR_CURRENT:Ljava/lang/String; = "GESTURESENSOR_IR_CURRENT"

.field public static final GESTURESENSOR_PEEK_TO_PEEK_SPEC_MAX:Ljava/lang/String; = "GESTURESENSOR_PEEK_TO_PEEK_SPEC_MAX"

.field public static final GESTURESENSOR_PEEK_TO_PEEK_SPEC_MIN:Ljava/lang/String; = "GESTURESENSOR_PEEK_TO_PEEK_SPEC_MIN"

.field public static final GYROSCOPE_HW_SELFTEST:Ljava/lang/String; = "GYROSCOPE_HW_SELFTEST"

.field public static final GYROSCOPE_SELFTEST_MAX:Ljava/lang/String; = "GYROSCOPE_SELFTEST_MAX"

.field public static final GYROSCOPE_SELFTEST_MIN:Ljava/lang/String; = "GYROSCOPE_SELFTEST_MIN"

.field public static final HRM_CLOUD_UV1_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV1_DC_LEVEL"

.field public static final HRM_CLOUD_UV2_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV2_DC_LEVEL"

.field public static final HRM_CLOUD_UV3_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV3_DC_LEVEL"

.field public static final HRM_CLOUD_UV4_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV4_DC_LEVEL"

.field public static final HRM_CLOUD_UV_RATIO:Ljava/lang/String; = "HRM_CLOUD_UV_RATIO"

.field public static final HRM_CLOUD_UV_SUM_RATIO:Ljava/lang/String; = "HRM_CLOUD_UV_SUM_RATIO"

.field public static final HRM_FREQUENCY_DC_IR:Ljava/lang/String; = "HRM_FREQUENCY_DC_IR"

.field public static final HRM_FREQUENCY_DC_RED:Ljava/lang/String; = "HRM_FREQUENCY_DC_RED"

.field public static final HRM_FREQUENCY_NOISE_IR:Ljava/lang/String; = "HRM_FREQUENCY_NOISE_IR"

.field public static final HRM_FREQUENCY_NOISE_RED:Ljava/lang/String; = "HRM_FREQUENCY_NOISE_RED"

.field public static final HRM_FREQUENCY_SAMPLE_NO:Ljava/lang/String; = "HRM_FREQUENCY_SAMPLE_NO"

.field public static final HRM_HR_SPO2:Ljava/lang/String; = "HRM_HR_SPO2"

.field public static final HRM_IR_RED_R_RATIO:Ljava/lang/String; = "HRM_IR_RED_R_RATIO"

.field public static final HRM_PEAK_TO_PEAK_DC_IR:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_DC_IR"

.field public static final HRM_PEAK_TO_PEAK_DC_RED:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_DC_RED"

.field public static final HRM_PEAK_TO_PEAK_PEAK_IR:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_PEAK_IR"

.field public static final HRM_PEAK_TO_PEAK_PEAK_RED:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_PEAK_RED"

.field public static final HRM_PEAK_TO_PEAK_RATIO_IR:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_RATIO_IR"

.field public static final HRM_PEAK_TO_PEAK_RATIO_RED:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_RATIO_RED"

.field public static final OIS_INITIAL_SPEC_MAX:Ljava/lang/String; = "OIS_INITIAL_SPEC_MAX"

.field public static final OIS_INITIAL_SPEC_MIN:Ljava/lang/String; = "OIS_INITIAL_SPEC_MIN"

.field public static final RANGE_LIGHT_BRIGHT:Ljava/lang/String; = "RANGE_LIGHT_BRIGHT"

.field public static final SIDE_TOUCH_ABS_CAP_MAX:Ljava/lang/String; = "SIDE_TOUCH_ABS_CAP_MAX"

.field public static final SIDE_TOUCH_ABS_CAP_MIN:Ljava/lang/String; = "SIDE_TOUCH_ABS_CAP_MIN"

.field public static final SIDE_TOUCH_DELTA_MAX:Ljava/lang/String; = "SIDE_TOUCH_DELTA_MAX"

.field public static final SIDE_TOUCH_DELTA_MIN:Ljava/lang/String; = "SIDE_TOUCH_DELTA_MIN"

.field public static final SIMPLE_TEST_G_MAX:Ljava/lang/String; = "SIMPLE_TEST_G_MAX"

.field public static final SIMPLE_TEST_G_MIN:Ljava/lang/String; = "SIMPLE_TEST_G_MIN"

.field public static final SPEN_NODE_COUNT_HEIGHT:Ljava/lang/String; = "SPEN_NODE_COUNT_HEIGHT"

.field public static final SPEN_NODE_COUNT_WIDTH:Ljava/lang/String; = "SPEN_NODE_COUNT_WIDTH"

.field public static final TAG:Ljava/lang/String; = "HardwareSpec"

.field public static final TOUCH_BACK_KEY_SPEC_MAX:Ljava/lang/String; = "TOUCH_BACK_KEY_SPEC_MAX"

.field public static final TOUCH_BACK_KEY_SPEC_MIN:Ljava/lang/String; = "TOUCH_BACK_KEY_SPEC_MIN"

.field public static final TOUCH_HIDDEN_KEY1_SPEC_MAX:Ljava/lang/String; = "TOUCH_HIDDEN_KEY1_SPEC_MAX"

.field public static final TOUCH_HIDDEN_KEY1_SPEC_MIN:Ljava/lang/String; = "TOUCH_HIDDEN_KEY1_SPEC_MIN"

.field public static final TOUCH_HIDDEN_KEY2_SPEC_MAX:Ljava/lang/String; = "TOUCH_HIDDEN_KEY2_SPEC_MAX"

.field public static final TOUCH_HIDDEN_KEY2_SPEC_MIN:Ljava/lang/String; = "TOUCH_HIDDEN_KEY2_SPEC_MIN"

.field public static final TOUCH_HIDDEN_KEY3_SPEC_MAX:Ljava/lang/String; = "TOUCH_HIDDEN_KEY3_SPEC_MAX"

.field public static final TOUCH_HIDDEN_KEY3_SPEC_MIN:Ljava/lang/String; = "TOUCH_HIDDEN_KEY3_SPEC_MIN"

.field public static final TOUCH_HIDDEN_KEY4_SPEC_MAX:Ljava/lang/String; = "TOUCH_HIDDEN_KEY4_SPEC_MAX"

.field public static final TOUCH_HIDDEN_KEY4_SPEC_MIN:Ljava/lang/String; = "TOUCH_HIDDEN_KEY4_SPEC_MIN"

.field public static final TOUCH_KEY_OUTER_SPEC_MAX:Ljava/lang/String; = "TOUCH_KEY_OUTER_SPEC_MAX"

.field public static final TOUCH_KEY_OUTER_SPEC_MIN:Ljava/lang/String; = "TOUCH_KEY_OUTER_SPEC_MIN"

.field public static final TOUCH_KEY_SPEC_MAX:Ljava/lang/String; = "TOUCH_KEY_SPEC_MAX"

.field public static final TOUCH_KEY_SPEC_MIN:Ljava/lang/String; = "TOUCH_KEY_SPEC_MIN"

.field public static final TOUCH_MENU_KEY_SPEC_MAX:Ljava/lang/String; = "TOUCH_MENU_KEY_SPEC_MAX"

.field public static final TOUCH_MENU_KEY_SPEC_MIN:Ljava/lang/String; = "TOUCH_MENU_KEY_SPEC_MIN"

.field public static final TOUCH_REF_KEY1_SPEC_MAX:Ljava/lang/String; = "TOUCH_REF_KEY1_SPEC_MAX"

.field public static final TOUCH_REF_KEY1_SPEC_MIN:Ljava/lang/String; = "TOUCH_REF_KEY1_SPEC_MIN"

.field public static final TOUCH_REF_KEY2_SPEC_MAX:Ljava/lang/String; = "TOUCH_REF_KEY2_SPEC_MAX"

.field public static final TOUCH_REF_KEY2_SPEC_MIN:Ljava/lang/String; = "TOUCH_REF_KEY2_SPEC_MIN"

.field public static final TOUCH_SWITCH_KEY_SPEC_MAX:Ljava/lang/String; = "TOUCH_SWITCH_KEY_SPEC_MAX"

.field public static final TOUCH_SWITCH_KEY_SPEC_MIN:Ljava/lang/String; = "TOUCH_SWITCH_KEY_SPEC_MIN"

.field public static final TSP_CONNECTION_MIN:Ljava/lang/String; = "TSP_CONNECTION_MIN"

.field public static final TSP_DEADZONE_HEIGHT:Ljava/lang/String; = "TSP_DEADZONE_HEIGHT"

.field public static final TSP_DEADZONE_WIDTH:Ljava/lang/String; = "TSP_DEADZONE_WIDTH"

.field public static final TSP_NODE_COUNT_HEIGHT:Ljava/lang/String; = "TSP_NODE_COUNT_HEIGHT"

.field public static final TSP_NODE_COUNT_WIDTH:Ljava/lang/String; = "TSP_NODE_COUNT_WIDTH"

.field public static final TSP_SELFTEST_MAX2:Ljava/lang/String; = "TSP_SELFTEST_MAX2"

.field public static final TSP_SELFTEST_MIN2:Ljava/lang/String; = "TSP_SELFTEST_MIN2"

.field public static final TSP_SELFTEST_SPEC_MAX_MINUS_MIN:Ljava/lang/String; = "TSP_SELFTEST_SPEC_MAX_MINUS_MIN"

.field public static final TSP_SPEN_NODE_COUNT_HEIGHT:Ljava/lang/String; = "TSP_SPEN_NODE_COUNT_HEIGHT"

.field public static final TSP_SPEN_NODE_COUNT_WIDTH:Ljava/lang/String; = "TSP_SPEN_NODE_COUNT_WIDTH"

.field public static final UV_SENSOR_ADC_MAX:Ljava/lang/String; = "UV_SENSOR_ADC_MAX"

.field public static final UV_SENSOR_ADC_MIN:Ljava/lang/String; = "UV_SENSOR_ADC_MIN"

.field public static final UV_SENSOR_UVA_MAX:Ljava/lang/String; = "UV_SENSOR_UVA_MAX"

.field public static final UV_SENSOR_UVA_MIN:Ljava/lang/String; = "UV_SENSOR_UVA_MIN"

.field public static final UV_SENSOR_UVB_MAX:Ljava/lang/String; = "UV_SENSOR_UVB_MAX"

.field public static final UV_SENSOR_UVB_MIN:Ljava/lang/String; = "UV_SENSOR_UVB_MIN"

.field public static final UV_SENSOR_UVINDEX_MAX:Ljava/lang/String; = "UV_SENSOR_UVINDEX_MAX"

.field public static final UV_SENSOR_UVINDEX_MIN:Ljava/lang/String; = "UV_SENSOR_UVINDEX_MIN"

.field public static final WATERPROOF_L_LEAK_MAIN:Ljava/lang/String; = "WATERPROOF_L_LEAK_MAIN"

.field public static final WATERPROOF_L_LEAK_SUB:Ljava/lang/String; = "WATERPROOF_L_LEAK_SUB"

.field public static final WATERPROOF_S_LEAK_MAIN:Ljava/lang/String; = "WATERPROOF_S_LEAK_MAIN"

.field public static final WATERPROOF_S_LEAK_SUB:Ljava/lang/String; = "WATERPROOF_S_LEAK_SUB"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 862
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getByte(Ljava/lang/String;)B
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 866
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getByte(Ljava/lang/String;Ljava/lang/String;)B
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$300(Ljava/lang/String;Ljava/lang/String;)B

    move-result v0

    return v0
.end method

.method public static getDouble(Ljava/lang/String;)D
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 886
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getDouble(Ljava/lang/String;Ljava/lang/String;)D
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$600(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 882
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$500(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 878
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$400(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 870
    const-string v0, "value"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getString_semi(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 874
    const-string v0, "value_semi"

    # invokes: Lcom/sec/factory/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/factory/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
