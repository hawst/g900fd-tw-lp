.class public Lcom/sec/factory/support/XMLDataStorage$ElementIdMismatchException;
.super Ljava/lang/RuntimeException;
.source "XMLDataStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/support/XMLDataStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ElementIdMismatchException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x481b5e841df8cf50L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 415
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 416
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 425
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 426
    return-void
.end method
