.class Lcom/sec/factory/modules/ModuleTouchScreen$1;
.super Landroid/os/Handler;
.source "ModuleTouchScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleTouchScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleTouchScreen;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleTouchScreen;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 567
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, -0x1

    .line 570
    :try_start_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "msg.arg1 (TSP ID Main) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-static {v5}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "msg.obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 579
    :goto_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne v2, v3, :cond_4

    .line 580
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "Connection"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 584
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne v2, v3, :cond_2

    .line 585
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_OK"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # invokes: Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPConnectionSpec(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$100(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 592
    const-string v2, "NG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 593
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 766
    .end local v1    # "result":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 574
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Ljava/lang/NullPointerException;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "msg.obj is NULL "

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 597
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "result":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 602
    .end local v1    # "result":Ljava/lang/String;
    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne v2, v3, :cond_3

    .line 603
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NG"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 608
    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne v2, v3, :cond_0

    .line 609
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NA"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 616
    :cond_4
    iget v2, p1, Landroid/os/Message;->arg1:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    if-ne v2, v3, :cond_8

    .line 617
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "Connection"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 621
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne v2, v3, :cond_6

    .line 622
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_OK"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 625
    .restart local v1    # "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # invokes: Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPConnectionHoveringSpec(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$200(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 629
    const-string v2, "NG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 630
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 637
    :cond_5
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 644
    .end local v1    # "result":Ljava/lang/String;
    :cond_6
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne v2, v3, :cond_7

    .line 645
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NG"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 651
    :cond_7
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne v2, v3, :cond_0

    .line 652
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NA"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 660
    :cond_8
    iget v2, p1, Landroid/os/Message;->arg1:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    if-ne v2, v3, :cond_c

    .line 661
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "RAWCAP"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 664
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne v2, v3, :cond_a

    .line 665
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_OK"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 668
    .restart local v1    # "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # invokes: Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPReferenceGapSpec(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$300(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 672
    const-string v2, "NG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 673
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 677
    :cond_9
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 681
    .end local v1    # "result":Ljava/lang/String;
    :cond_a
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne v2, v3, :cond_b

    .line 682
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NG"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 687
    :cond_b
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne v2, v3, :cond_0

    .line 688
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NA"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 695
    :cond_c
    iget v2, p1, Landroid/os/Message;->arg1:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    if-ne v2, v3, :cond_10

    .line 696
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "CXDATA"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 699
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne v2, v3, :cond_e

    .line 700
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_OK"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 703
    .restart local v1    # "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # invokes: Lcom/sec/factory/modules/ModuleTouchScreen;->checkCxDataGapSpec(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$400(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 707
    const-string v2, "NG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 708
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 712
    :cond_d
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 716
    .end local v1    # "result":Ljava/lang/String;
    :cond_e
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne v2, v3, :cond_f

    .line 717
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NG"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 722
    :cond_f
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne v2, v3, :cond_0

    .line 723
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NA"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 730
    :cond_10
    iget v2, p1, Landroid/os/Message;->arg1:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    if-ne v2, v3, :cond_0

    .line 731
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "CXDATA ALL"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 734
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne v2, v3, :cond_12

    .line 735
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_OK"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 738
    .restart local v1    # "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # invokes: Lcom/sec/factory/modules/ModuleTouchScreen;->checkCxDataAllSpec(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$500(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 742
    const-string v2, "NG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 743
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 747
    :cond_11
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 751
    .end local v1    # "result":Ljava/lang/String;
    :cond_12
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne v2, v3, :cond_13

    .line 752
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NG"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 757
    :cond_13
    iget v2, p1, Landroid/os/Message;->what:I

    sget v3, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne v2, v3, :cond_0

    .line 758
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "mTSPHandler.handleMessage"

    const-string v4, "TSP_WHAT_STATUS_NA"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen$1;->this$0:Lcom/sec/factory/modules/ModuleTouchScreen;

    # getter for: Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleTouchScreen;->access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    sget v5, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1
.end method
