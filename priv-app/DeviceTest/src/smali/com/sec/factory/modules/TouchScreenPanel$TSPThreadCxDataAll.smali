.class Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;
.super Ljava/lang/Thread;
.source "TouchScreenPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/TouchScreenPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TSPThreadCxDataAll"
.end annotation


# instance fields
.field mNotiHandler:Landroid/os/Handler;

.field subCommand:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/factory/modules/TouchScreenPanel;

.field tspID:I


# direct methods
.method private constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;)V
    .locals 0

    .prologue
    .line 1045
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p2, "x1"    # Lcom/sec/factory/modules/TouchScreenPanel$1;

    .prologue
    .line 1045
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;Landroid/os/Handler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 1045
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 1045
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->setParameter(ILjava/lang/String;)V

    return-void
.end method

.method private setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    .line 1052
    return-void
.end method

.method private setParameter(ILjava/lang/String;)V
    .locals 0
    .param p1, "ID"    # I
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 1055
    iput p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    .line 1056
    iput-object p2, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->subCommand:Ljava/lang/String;

    .line 1057
    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 1060
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "TSP ID (Main) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    move/from16 v19, v0

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->mapping_IDnCommand(I)Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$000(Lcom/sec/factory/modules/TouchScreenPanel;I)Ljava/lang/String;

    move-result-object v4

    .line 1063
    .local v4, "command":Ljava/lang/String;
    const-string v10, "get_cx_data"

    .line 1064
    .local v10, "second_command":Ljava/lang/String;
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Main Command : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sub Command : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->subCommand:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const/4 v11, 0x0

    .line 1067
    .local v11, "status":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1068
    .local v8, "result":Ljava/lang/String;
    const/4 v9, 0x0

    check-cast v9, [[I

    .line 1069
    .local v9, "ret":[[I
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v5, v0, [I

    .line 1071
    .local v5, "data":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountX()Ljava/lang/String;

    move-result-object v12

    .line 1072
    .local v12, "tmpX":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountY()Ljava/lang/String;

    move-result-object v13

    .line 1073
    .local v13, "tmpY":Ljava/lang/String;
    const-string v18, "TSP_NODE_COUNT_WIDTH"

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 1074
    .local v15, "xChannel":I
    const-string v18, "TSP_NODE_COUNT_HEIGHT"

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 1077
    .local v17, "yChannel":I
    :try_start_0
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 1078
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    .line 1083
    :goto_0
    const-string v18, "NA"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    .line 1086
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v4, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v4, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$400(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1090
    const-string v18, "NG"

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 1091
    move/from16 v0, v17

    filled-new-array {v0, v15}, [I

    move-result-object v18

    sget-object v19, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "ret":[[I
    check-cast v9, [[I

    .line 1093
    .restart local v9    # "ret":[[I
    const/16 v16, 0x0

    .local v16, "y":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    .line 1094
    const/4 v14, 0x0

    .local v14, "x":I
    :goto_2
    if-ge v14, v15, :cond_1

    .line 1095
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1097
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ","

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v10, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1099
    aget-object v18, v9, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v10, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$400(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    aput v19, v18, v14

    .line 1100
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1094
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 1079
    .end local v14    # "x":I
    .end local v16    # "y":I
    :catch_0
    move-exception v7

    .line 1080
    .local v7, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1100
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    .restart local v14    # "x":I
    .restart local v16    # "y":I
    :catchall_0
    move-exception v18

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v18
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 1110
    .end local v14    # "x":I
    :catch_1
    move-exception v6

    .line 1112
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 1114
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_3
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "status : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "result : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    if-eqz v11, :cond_3

    const-string v18, "OK"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1118
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_OK"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_OK:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v8}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1140
    .end local v16    # "y":I
    :cond_0
    :goto_4
    return-void

    .line 1093
    .restart local v14    # "x":I
    .restart local v16    # "y":I
    :cond_1
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 1104
    .end local v14    # "x":I
    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/factory/modules/check/CheckCxDataAll;

    invoke-direct/range {v19 .. v19}, Lcom/sec/factory/modules/check/CheckCxDataAll;-><init>()V

    # setter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataAll:Lcom/sec/factory/modules/check/CheckCxDataAll;
    invoke-static/range {v18 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$702(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/check/CheckCxDataAll;)Lcom/sec/factory/modules/check/CheckCxDataAll;

    .line 1105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataAll:Lcom/sec/factory/modules/check/CheckCxDataAll;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$700(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckCxDataAll;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/sec/factory/modules/check/CheckCxDataAll;->getMinMaxCxData([[I)[I

    move-result-object v5

    .line 1106
    const-string v18, "TouchScreenPanel"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "data Min : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x0

    aget v20, v5, v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Max : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x1

    aget v21, v5, v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "%d"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    aget v22, v5, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%d"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x1

    aget v22, v5, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v8

    goto/16 :goto_3

    .line 1122
    :cond_3
    if-eqz v11, :cond_4

    const-string v18, "NOT_APPLICABLE"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1123
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 1128
    :cond_4
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_NG"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NG:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 1135
    .end local v16    # "y":I
    :cond_5
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "command==null"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4
.end method
