.class public Lcom/sec/factory/modules/SensorSpec$Accelerometer;
.super Ljava/lang/Object;
.source "SensorSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Accelerometer"
.end annotation


# instance fields
.field public mBitCount:I

.field public mFlatness_Max:I

.field public mFlatness_Min:I

.field public mIsSupport_INT_Pin:Z

.field public mRange:Lcom/sec/factory/modules/SensorSpec$Range;


# direct methods
.method public constructor <init>(IIIIIIIIIZ)V
    .locals 8
    .param p1, "bitCount"    # I
    .param p2, "xMin"    # I
    .param p3, "xMax"    # I
    .param p4, "yMin"    # I
    .param p5, "yMax"    # I
    .param p6, "zMin"    # I
    .param p7, "zMax"    # I
    .param p8, "flatnessMin"    # I
    .param p9, "flatnessMax"    # I
    .param p10, "isSupport_INT_Pin"    # Z

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 19
    iput p1, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mBitCount:I

    .line 20
    new-instance v1, Lcom/sec/factory/modules/SensorSpec$Range;

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 21
    move/from16 v0, p8

    iput v0, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mFlatness_Min:I

    .line 22
    move/from16 v0, p9

    iput v0, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mFlatness_Max:I

    .line 23
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mIsSupport_INT_Pin:Z

    .line 24
    return-void
.end method
