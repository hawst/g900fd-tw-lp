.class Lcom/sec/factory/modules/SensorReadFile$Info;
.super Ljava/lang/Object;
.source "SensorReadFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorReadFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Info"
.end annotation


# instance fields
.field public mBuffer_SensorValue:[Ljava/lang/String;

.field public mFilePath:Ljava/lang/String;

.field public mIsExistFile:Z

.field public mIsLoopTest:Z

.field private mIsON:Z

.field public mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v0, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsON:Z

    .line 22
    iput-boolean v0, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsLoopTest:Z

    .line 25
    iput-object p1, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mFilePath:Ljava/lang/String;

    .line 27
    invoke-static {p2}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsExistFile:Z

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getOn()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsON:Z

    return v0
.end method

.method public setOn(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsON:Z

    .line 34
    return-void
.end method
