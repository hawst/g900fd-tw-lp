.class Lcom/sec/factory/modules/ModuleCommunicationService$3;
.super Ljava/lang/Thread;
.source "ModuleCommunicationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/modules/ModuleCommunicationService;->nfcDisable()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleCommunicationService;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$3;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 268
    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$900()Landroid/nfc/NfcAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->disable()Z

    move-result v0

    .line 270
    .local v0, "off":Z
    if-eqz v0, :cond_0

    .line 271
    const-string v1, "ModuleCommunicationService"

    const-string v2, "nfcDisable"

    const-string v3, "NFC off success!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    const-string v1, "ModuleCommunicationService"

    const-string v2, "nfcDisable"

    const-string v3, "NFC off fail!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
