.class public Lcom/sec/factory/modules/ModuleTouchScreen;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleTouchScreen.java"


# static fields
.field public static final TSP_DEVICE_TYPE__PHONE:Ljava/lang/String; = "phone"

.field public static final TSP_DEVICE_TYPE__TABLET:Ljava/lang/String; = "tablet"

.field public static final TSP_DUAL_SPEC_1ST:I = 0x0

.field public static final TSP_DUAL_SPEC_2ND:I = 0x1

.field public static final TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:I

.field public static final TSP_HOVER_ID__ABS_CAP:I

.field public static final TSP_HOVER_ID__ABS_DELTA:I

.field private static TSP_ID_DUMMY:I = 0x0

.field public static final TSP_ID_HOVER_DISABLE:I

.field public static final TSP_ID_HOVER_ENABLE:I

.field public static final TSP_ID_SCOPE_MAX:I

.field public static final TSP_ID_SCOPE_MIN:I

.field public static final TSP_ID__CHIP_NAME:I

.field public static final TSP_ID__CM_ABS__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__CM_ABS__NODE:I

.field public static final TSP_ID__CONFIG_VERSION:I

.field public static final TSP_ID__CX_DATA_ALL_NODE:I

.field public static final TSP_ID__CX_DATA_GAP_NODE:I

.field public static final TSP_ID__CX_DATA__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__CX_DATA__NODE:I

.field public static final TSP_ID__DELTA__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__DELTA__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__DELTA__NODE:I

.field public static final TSP_ID__DIFFERENCE__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__DIFFERENCE__NODE:I

.field public static final TSP_ID__DND__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__DND__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__DND__DIFF:I

.field public static final TSP_ID__DND__NODE:I

.field public static final TSP_ID__EXPANSION__CONNECTION:I

.field public static final TSP_ID__EXPANSION__CONNECTION_HOVERING:I

.field public static final TSP_ID__FW_UPDATE:I

.field public static final TSP_ID__FW_VERSION_BINARY:I

.field public static final TSP_ID__FW_VERSION_IC:I

.field public static final TSP_ID__GLOBAL_IDAC__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__GLOBAL_IDAC__NODE:I

.field public static final TSP_ID__INSPECTION__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__INSPECTION__NODE:I

.field public static final TSP_ID__INTENSITY__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__INTENSITY__NODE:I

.field public static final TSP_ID__KEY_CM_DATA:I

.field public static final TSP_ID__KEY_CX_DATA:I

.field public static final TSP_ID__LOCAL_IDAC__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__LOCAL_IDAC__NODE:I

.field public static final TSP_ID__MODULE_VENDOR:I

.field public static final TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__MUTUALCAP__NODE:I

.field public static final TSP_ID__NUMBER_X_CHANNEL:I

.field public static final TSP_ID__NUMBER_Y_CHANNEL:I

.field public static final TSP_ID__POWER_OFF:I

.field public static final TSP_ID__POWER_OFF_SECOND_CHIP:I

.field public static final TSP_ID__POWER_ON:I

.field public static final TSP_ID__POWER_ON_SECOND_CHIP:I

.field public static final TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__RAWCAP__NODE:I

.field public static final TSP_ID__RAW_COUNT__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__RAW_COUNT__NODE:I

.field public static final TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

.field public static final TSP_ID__REFERENCE__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__REFERENCE__NODE:I

.field public static final TSP_ID__REGSITER__RESULT:I

.field public static final TSP_ID__RX_TO_RX__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__RX_TO_RX__NODE:I

.field public static final TSP_ID__SCANTIME__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__SCANTIME__NODE:I

.field public static final TSP_ID__THRESHOLD:I

.field public static final TSP_ID__TRX_SHORT_TEST:I

.field public static final TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__TX_TO_GND__NODE:I

.field public static final TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE:I

.field public static final TSP_ID__TX_TO_TX__NODE:I

.field public static final TSP_ID__VENDOR_NAME:I

.field public static final TSP_PANEL_TYPE__OCTA:Ljava/lang/String; = "OCTA"

.field public static final TSP_PANEL_TYPE__TFT:Ljava/lang/String; = "TFT"

.field public static final TSP_RESULT_VALUE_NG:Ljava/lang/String; = "NG"

.field public static final TSP_RESULT_VALUE_NOT_APPLICABLE:Ljava/lang/String; = "NA"

.field public static final TSP_RESULT_VALUE_OK:Ljava/lang/String; = "OK"

.field public static final TSP_SIDETOUCH_ID__ABS_CAP:I

.field public static final TSP_SIDETOUCH_ID__DELTA:I

.field public static final TSP_VENDOR_NAME__ATMEL:Ljava/lang/String; = "ATMEL"

.field public static final TSP_VENDOR_NAME__CYPRESS:Ljava/lang/String; = "CYPRESS"

.field public static final TSP_VENDOR_NAME__G2:Ljava/lang/String; = "G2"

.field public static final TSP_VENDOR_NAME__IMAGIS:Ljava/lang/String; = "IMAGIS"

.field public static final TSP_VENDOR_NAME__MELFAS:Ljava/lang/String; = "MELFAS"

.field public static final TSP_VENDOR_NAME__SILAB:Ljava/lang/String; = "SILAB"

.field public static final TSP_VENDOR_NAME__STM:Ljava/lang/String; = "STM"

.field public static final TSP_VENDOR_NAME__SYNAPTICS:Ljava/lang/String; = "SYNAPTICS"

.field public static final TSP_VENDOR_NAME__ZI:Ljava/lang/String; = "ZI"

.field public static final TSP_VENDOR_NAME__ZINITIX:Ljava/lang/String; = "ZINITIX"

.field public static final TSP_WHAT_SCOPE_MAX:I

.field public static final TSP_WHAT_SCOPE_MIN:I

.field public static final TSP_WHAT_STATUS_NA:I

.field public static final TSP_WHAT_STATUS_NG:I

.field public static final TSP_WHAT_STATUS_OK:I

.field private static mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;


# instance fields
.field private mIsStandardChannel:Z

.field private mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

.field private mTSPChannelCountX:Ljava/lang/String;

.field private mTSPChannelCountY:Ljava/lang/String;

.field private mTSPConnectionHoveringSpec_Max_X:I

.field private mTSPConnectionHoveringSpec_Max_Y:I

.field private mTSPConnectionHoveringSpec_Min_X:I

.field private mTSPConnectionHoveringSpec_Min_Y:I

.field private mTSPConnectionSpec_Max:I

.field private mTSPConnectionSpec_Min:I

.field private mTSPCxDataGapSpec_X:I

.field private mTSPCxDataGapSpec_Y:I

.field private mTSPCxDataSpec_Max:I

.field private mTSPCxDataSpec_Min:I

.field private mTSPDeviceType:Ljava/lang/String;

.field mTSPHandler:Landroid/os/Handler;

.field private mTSPModuleVendor:Ljava/lang/String;

.field private mTSPNotiHandler:Landroid/os/Handler;

.field private mTSPPanelType:Ljava/lang/String;

.field private mTSPReferenceGapSpec_X:I

.field private mTSPReferenceGapSpec_Y:I

.field private mTSPVendorName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleTouchScreen;->mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;

    .line 136
    const/4 v0, 0x1

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    .line 137
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_SCOPE_MIN:I

    .line 139
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_UPDATE:I

    .line 140
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_BINARY:I

    .line 141
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_IC:I

    .line 142
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CONFIG_VERSION:I

    .line 143
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__THRESHOLD:I

    .line 144
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_OFF:I

    .line 145
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_ON:I

    .line 146
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_OFF_SECOND_CHIP:I

    .line 147
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_ON_SECOND_CHIP:I

    .line 148
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__VENDOR_NAME:I

    .line 149
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MODULE_VENDOR:I

    .line 150
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CHIP_NAME:I

    .line 151
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_X_CHANNEL:I

    .line 152
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_Y_CHANNEL:I

    .line 154
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

    .line 155
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_NONE:I

    .line 156
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__NODE:I

    .line 157
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_MIN_MAX:I

    .line 158
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_NONE:I

    .line 159
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__NODE:I

    .line 160
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__ALL_NODE__RETURN_MIN_MAX:I

    .line 161
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__NODE:I

    .line 162
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INSPECTION__ALL_NODE__RETURN_MIN_MAX:I

    .line 163
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INSPECTION__NODE:I

    .line 164
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__ALL_NODE__RETURN_MIN_MAX:I

    .line 165
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__NODE:I

    .line 166
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__ALL_NODE__RETURN_NONE:I

    .line 167
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__NODE:I

    .line 168
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX:I

    .line 169
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    .line 170
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__ALL_NODE__RETURN_NONE:I

    .line 171
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__NODE:I

    .line 172
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE:I

    .line 173
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__NODE:I

    .line 174
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE:I

    .line 175
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__NODE:I

    .line 176
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__ALL_NODE__RETURN_MIN_MAX:I

    .line 177
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__NODE:I

    .line 178
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__ALL_NODE__RETURN_NONE:I

    .line 179
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__NODE:I

    .line 180
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__ALL_NODE__RETURN_NONE:I

    .line 181
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__NODE:I

    .line 182
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__ALL_NODE__RETURN_NONE:I

    .line 183
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__NODE:I

    .line 184
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_CAP:I

    .line 185
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_DELTA:I

    .line 186
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX:I

    .line 187
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__NODE:I

    .line 188
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REGSITER__RESULT:I

    .line 189
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:I

    .line 190
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__ABS_CAP:I

    .line 191
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__DELTA:I

    .line 192
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TRX_SHORT_TEST:I

    .line 193
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__ALL_NODE__RETURN_MIN_MAX:I

    .line 194
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__ALL_NODE__RETURN_NONE:I

    .line 195
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__NODE:I

    .line 196
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__DIFF:I

    .line 197
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA__ALL_NODE__RETURN_NONE:I

    .line 198
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA__NODE:I

    .line 199
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    .line 200
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    .line 201
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__KEY_CM_DATA:I

    .line 202
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__KEY_CX_DATA:I

    .line 204
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    .line 205
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    .line 206
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_HOVER_ENABLE:I

    .line 207
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_HOVER_DISABLE:I

    .line 208
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_DUMMY:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_SCOPE_MAX:I

    .line 216
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_SCOPE_MIN:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_SCOPE_MIN:I

    .line 217
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_OK:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    .line 218
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NG:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    .line 219
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    .line 220
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_SCOPE_MAX:I

    sput v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_SCOPE_MAX:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 61
    const-string v0, "ModuleTouchScreen"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 55
    iput-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountX:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountY:Ljava/lang/String;

    .line 567
    new-instance v0, Lcom/sec/factory/modules/ModuleTouchScreen$1;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/factory/modules/ModuleTouchScreen$1;-><init>(Lcom/sec/factory/modules/ModuleTouchScreen;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPHandler:Landroid/os/Handler;

    .line 62
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleTouchScreen"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v0, "PANEL_TYPE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPPanelType:Ljava/lang/String;

    .line 64
    const-string v0, "DEVICE_TYPE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPDeviceType:Ljava/lang/String;

    .line 65
    new-instance v0, Lcom/sec/factory/modules/TouchScreenPanel;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPPanelType:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPDeviceType:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/factory/modules/TouchScreenPanel;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    .line 66
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->setTSPInfo()V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/modules/ModuleTouchScreen;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleTouchScreen;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleTouchScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPConnectionSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleTouchScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPConnectionHoveringSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleTouchScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPReferenceGapSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleTouchScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkCxDataGapSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/factory/modules/ModuleTouchScreen;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleTouchScreen;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkCxDataAllSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkCxDataAllSpec(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 961
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 963
    .local v0, "data":[Ljava/lang/String;
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 964
    aget-object v4, v0, v8

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 965
    .local v2, "dataMin":F
    aget-object v4, v0, v9

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 966
    .local v1, "dataMax":F
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataAllSpec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dataMin : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , dataMax : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    iget v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataSpec_Min:I

    int-to-float v4, v4

    cmpg-float v4, v4, v2

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataSpec_Max:I

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gtz v4, :cond_0

    .line 971
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataAllSpec"

    const-string v6, "Spec In"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    const/4 v3, 0x1

    .line 977
    .local v3, "temp":Z
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_1

    const-string v4, "OK"

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 982
    .end local v1    # "dataMax":F
    .end local v2    # "dataMin":F
    .end local v3    # "temp":Z
    :goto_2
    return-object v4

    .line 974
    .restart local v1    # "dataMax":F
    .restart local v2    # "dataMin":F
    :cond_0
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataAllSpec"

    const-string v6, "Spec Out"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    const/4 v3, 0x0

    .restart local v3    # "temp":Z
    goto :goto_0

    .line 977
    :cond_1
    const-string v4, "NG"

    goto :goto_1

    .line 980
    .end local v1    # "dataMax":F
    .end local v2    # "dataMin":F
    .end local v3    # "temp":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataAllSpec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data.length!=2 => length:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    const-string v4, "NG"

    goto :goto_2
.end method

.method private checkCxDataGapSpec(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 935
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 937
    .local v0, "data":[Ljava/lang/String;
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 938
    aget-object v4, v0, v8

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 939
    .local v1, "dataGapX":F
    aget-object v4, v0, v9

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 940
    .local v2, "dataGapY":F
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataGapSpec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dataGapX : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , dataGapY : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    iget v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataGapSpec_X:I

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataGapSpec_Y:I

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gtz v4, :cond_0

    .line 945
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataGapSpec"

    const-string v6, "Spec In"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    const/4 v3, 0x1

    .line 951
    .local v3, "temp":Z
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_1

    const-string v4, "OK"

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 956
    .end local v1    # "dataGapX":F
    .end local v2    # "dataGapY":F
    .end local v3    # "temp":Z
    :goto_2
    return-object v4

    .line 948
    .restart local v1    # "dataGapX":F
    .restart local v2    # "dataGapY":F
    :cond_0
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataGapSpec"

    const-string v6, "Spec Out"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    const/4 v3, 0x0

    .restart local v3    # "temp":Z
    goto :goto_0

    .line 951
    :cond_1
    const-string v4, "NG"

    goto :goto_1

    .line 954
    .end local v1    # "dataGapX":F
    .end local v2    # "dataGapY":F
    .end local v3    # "temp":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkCxDataGapSpec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data.length!=2 => length:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    const-string v4, "NG"

    goto :goto_2
.end method

.method private checkNodeHeight(I)Ljava/lang/String;
    .locals 2
    .param p1, "number"    # I

    .prologue
    .line 1423
    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mIsStandardChannel:Z

    if-eqz v1, :cond_0

    .line 1424
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountX()Ljava/lang/String;

    move-result-object v0

    .line 1429
    .local v0, "height":Ljava/lang/String;
    :goto_0
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1430
    const-string v1, "NG"

    .line 1438
    :goto_1
    return-object v1

    .line 1426
    .end local v0    # "height":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountY()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "height":Ljava/lang/String;
    goto :goto_0

    .line 1431
    :cond_1
    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1432
    const-string v1, "NA"

    goto :goto_1

    .line 1435
    :cond_2
    if-ltz p1, :cond_3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge p1, v1, :cond_3

    .line 1436
    const-string v1, "OK"

    goto :goto_1

    .line 1438
    :cond_3
    const-string v1, "NA"

    goto :goto_1
.end method

.method private checkTSPConnectionHoveringSpec(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 826
    const-string v10, ","

    invoke-virtual {p1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 828
    .local v0, "data":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountY()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 829
    .local v9, "ynode":I
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountX()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 830
    .local v7, "xnode":I
    const v4, 0x3b9ac9ff

    .line 831
    .local v4, "min_y":I
    const v2, -0x3b9ac9ff

    .line 832
    .local v2, "max_y":I
    const v3, 0x3b9ac9ff

    .line 833
    .local v3, "min_x":I
    const v1, -0x3b9ac9ff

    .line 835
    .local v1, "max_x":I
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Y node : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " X node : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/4 v8, 0x0

    .local v8, "y":I
    :goto_0
    if-ge v8, v9, :cond_2

    .line 839
    aget-object v10, v0, v8

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-ge v10, v4, :cond_1

    .line 840
    aget-object v10, v0, v8

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 838
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 841
    :cond_1
    aget-object v10, v0, v8

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-le v10, v2, :cond_0

    .line 842
    aget-object v10, v0, v8

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1

    .line 845
    :cond_2
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "min_y : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " max_y : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    move v6, v9

    .local v6, "x":I
    :goto_2
    add-int v10, v9, v7

    if-ge v6, v10, :cond_5

    .line 849
    aget-object v10, v0, v6

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-ge v10, v3, :cond_4

    .line 850
    aget-object v10, v0, v6

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 848
    :cond_3
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 851
    :cond_4
    aget-object v10, v0, v6

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    if-le v10, v1, :cond_3

    .line 852
    aget-object v10, v0, v6

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_3

    .line 855
    :cond_5
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "min_x : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " max_x : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    array-length v10, v0

    add-int v11, v9, v7

    if-ne v10, v11, :cond_b

    .line 865
    const/4 v5, 0x1

    .line 867
    .local v5, "temp":Z
    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_Y:I

    if-gt v10, v4, :cond_7

    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_Y:I

    if-gt v4, v10, :cond_7

    .line 869
    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_Y:I

    if-gt v10, v2, :cond_6

    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_Y:I

    if-gt v2, v10, :cond_6

    .line 883
    :goto_4
    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_X:I

    if-gt v10, v3, :cond_9

    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_X:I

    if-gt v3, v10, :cond_9

    .line 885
    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_X:I

    if-gt v10, v1, :cond_8

    iget v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_X:I

    if-gt v1, v10, :cond_8

    .line 899
    :goto_5
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v5, :cond_a

    const-string v10, "OK"

    :goto_6
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 904
    .end local v5    # "temp":Z
    :goto_7
    return-object v10

    .line 873
    .restart local v5    # "temp":Z
    :cond_6
    const/4 v5, 0x0

    .line 874
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    const-string v12, "Spec Out => Y_MAX"

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 878
    :cond_7
    const/4 v5, 0x0

    .line 879
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    const-string v12, "Spec Out => Y_MIN"

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 889
    :cond_8
    const/4 v5, 0x0

    .line 890
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    const-string v12, "Spec Out => Y_MAX"

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 894
    :cond_9
    const/4 v5, 0x0

    .line 895
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    const-string v12, "Spec Out => Y_MIN"

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 899
    :cond_a
    const-string v10, "NG"

    goto :goto_6

    .line 902
    .end local v5    # "temp":Z
    :cond_b
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "checkTSPConnectionHoveringSpec"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "data.length!=4 => length:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    array-length v13, v0

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    const-string v10, "NG"

    goto :goto_7
.end method

.method private checkTSPConnectionSpec(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 770
    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 772
    .local v0, "data":[Ljava/lang/String;
    array-length v7, v0

    const/4 v8, 0x2

    if-ne v7, v8, :cond_6

    .line 773
    aget-object v7, v0, v11

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 774
    .local v4, "min":I
    aget-object v7, v0, v12

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 775
    .local v3, "max":I
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "checkTSPConnectionSpec"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "min : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " , max : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    const/4 v1, 0x0

    .line 779
    .local v1, "isCheckMaxMinusMin":Z
    const-string v7, "10"

    invoke-static {v7}, Lcom/sec/factory/support/Support$FactoryTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 783
    .local v2, "mTestCase":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v7, "CHECK_SELF_MAX_MINUS_MIN"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 784
    const/4 v1, 0x1

    .line 787
    :cond_0
    iget v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    if-gt v7, v4, :cond_4

    iget v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    if-gt v4, v7, :cond_4

    .line 788
    iget v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    if-gt v7, v3, :cond_3

    iget v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    if-gt v3, v7, :cond_3

    .line 790
    if-eqz v1, :cond_2

    .line 791
    const-string v7, "TSP_SELFTEST_SPEC_MAX_MINUS_MIN"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 793
    .local v5, "spec_MaxMinusMin":I
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "checkTSPConnectionSpec"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "max - min : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-int v10, v3, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " , "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "spec_MaxMinusMin : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    sub-int v7, v3, v4

    if-gt v7, v5, :cond_1

    .line 798
    const/4 v6, 0x1

    .line 816
    .end local v5    # "spec_MaxMinusMin":I
    .local v6, "temp":Z
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v6, :cond_5

    const-string v7, "OK"

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 821
    .end local v1    # "isCheckMaxMinusMin":Z
    .end local v2    # "mTestCase":Ljava/lang/String;
    .end local v3    # "max":I
    .end local v4    # "min":I
    .end local v6    # "temp":Z
    :goto_2
    return-object v7

    .line 800
    .restart local v1    # "isCheckMaxMinusMin":Z
    .restart local v2    # "mTestCase":Ljava/lang/String;
    .restart local v3    # "max":I
    .restart local v4    # "min":I
    .restart local v5    # "spec_MaxMinusMin":I
    :cond_1
    const/4 v6, 0x0

    .restart local v6    # "temp":Z
    goto :goto_0

    .line 803
    .end local v5    # "spec_MaxMinusMin":I
    .end local v6    # "temp":Z
    :cond_2
    const/4 v6, 0x1

    .restart local v6    # "temp":Z
    goto :goto_0

    .line 806
    .end local v6    # "temp":Z
    :cond_3
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "checkTSPConnectionSpec"

    const-string v9, "Spec Out => MAX"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const/4 v6, 0x0

    .restart local v6    # "temp":Z
    goto :goto_0

    .line 811
    .end local v6    # "temp":Z
    :cond_4
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "checkTSPConnectionSpec"

    const-string v9, "Spec Out => MIN"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const/4 v6, 0x0

    .restart local v6    # "temp":Z
    goto :goto_0

    .line 816
    :cond_5
    const-string v7, "NG"

    goto :goto_1

    .line 819
    .end local v1    # "isCheckMaxMinusMin":Z
    .end local v2    # "mTestCase":Ljava/lang/String;
    .end local v3    # "max":I
    .end local v4    # "min":I
    .end local v6    # "temp":Z
    :cond_6
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getTSPResult_Connection"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "data.length!=2 => length:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    const-string v7, "NG"

    goto :goto_2
.end method

.method private checkTSPReferenceGapSpec(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 909
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 911
    .local v0, "data":[Ljava/lang/String;
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 912
    aget-object v4, v0, v8

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 913
    .local v1, "dataGapX":F
    aget-object v4, v0, v9

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 914
    .local v2, "dataGapY":F
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkTSPReferenceGapSpec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dataGapX : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , dataGapY : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    iget v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPReferenceGapSpec_X:I

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPReferenceGapSpec_Y:I

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gtz v4, :cond_0

    .line 919
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkTSPReferenceGapSpec"

    const-string v6, "Spec In"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    const/4 v3, 0x1

    .line 925
    .local v3, "temp":Z
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_1

    const-string v4, "OK"

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 930
    .end local v1    # "dataGapX":F
    .end local v2    # "dataGapY":F
    .end local v3    # "temp":Z
    :goto_2
    return-object v4

    .line 922
    .restart local v1    # "dataGapX":F
    .restart local v2    # "dataGapY":F
    :cond_0
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkTSPReferenceGapSpec"

    const-string v6, "Spec Out"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    const/4 v3, 0x0

    .restart local v3    # "temp":Z
    goto :goto_0

    .line 925
    :cond_1
    const-string v4, "NG"

    goto :goto_1

    .line 928
    .end local v1    # "dataGapX":F
    .end local v2    # "dataGapY":F
    .end local v3    # "temp":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkTSPReferenceGapSpec"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data.length!=2 => length:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    const-string v4, "NG"

    goto :goto_2
.end method

.method public static convertorTSPID(I)Ljava/lang/String;
    .locals 1
    .param p0, "tspID"    # I

    .prologue
    .line 1528
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_UPDATE:I

    if-ne p0, v0, :cond_0

    .line 1529
    const-string v0, "TSP_ID__FW_UPDATE"

    .line 1647
    :goto_0
    return-object v0

    .line 1530
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_BINARY:I

    if-ne p0, v0, :cond_1

    .line 1531
    const-string v0, "TSP_ID__FW_VERSION_BINARY"

    goto :goto_0

    .line 1532
    :cond_1
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_IC:I

    if-ne p0, v0, :cond_2

    .line 1533
    const-string v0, "TSP_ID__FW_VERSION_IC"

    goto :goto_0

    .line 1534
    :cond_2
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CONFIG_VERSION:I

    if-ne p0, v0, :cond_3

    .line 1535
    const-string v0, "TSP_ID__CONFIG_VERSION"

    goto :goto_0

    .line 1536
    :cond_3
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__THRESHOLD:I

    if-ne p0, v0, :cond_4

    .line 1537
    const-string v0, "TSP_ID__THRESHOLD"

    goto :goto_0

    .line 1538
    :cond_4
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_OFF:I

    if-ne p0, v0, :cond_5

    .line 1539
    const-string v0, "TSP_ID__POWER_OFF"

    goto :goto_0

    .line 1540
    :cond_5
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_ON:I

    if-ne p0, v0, :cond_6

    .line 1541
    const-string v0, "TSP_ID__POWER_ON"

    goto :goto_0

    .line 1542
    :cond_6
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_OFF_SECOND_CHIP:I

    if-ne p0, v0, :cond_7

    .line 1543
    const-string v0, "TSP_ID__POWER_OFF_SECOND_CHIP"

    goto :goto_0

    .line 1544
    :cond_7
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_ON_SECOND_CHIP:I

    if-ne p0, v0, :cond_8

    .line 1545
    const-string v0, "TSP_ID__POWER_ON_SECOND_CHIP"

    goto :goto_0

    .line 1546
    :cond_8
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__VENDOR_NAME:I

    if-ne p0, v0, :cond_9

    .line 1547
    const-string v0, "TSP_ID__VENDOR_NAME"

    goto :goto_0

    .line 1548
    :cond_9
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MODULE_VENDOR:I

    if-ne p0, v0, :cond_a

    .line 1549
    const-string v0, "TSP_ID__MODULE_VENDOR"

    goto :goto_0

    .line 1550
    :cond_a
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CHIP_NAME:I

    if-ne p0, v0, :cond_b

    .line 1551
    const-string v0, "TSP_ID__CHIP_NAME"

    goto :goto_0

    .line 1552
    :cond_b
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_X_CHANNEL:I

    if-ne p0, v0, :cond_c

    .line 1553
    const-string v0, "TSP_ID__NUMBER_X_CHANNEL"

    goto :goto_0

    .line 1554
    :cond_c
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_Y_CHANNEL:I

    if-ne p0, v0, :cond_d

    .line 1555
    const-string v0, "TSP_ID__NUMBER_Y_CHANNEL"

    goto :goto_0

    .line 1558
    :cond_d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_e

    .line 1559
    const-string v0, "TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX"

    goto :goto_0

    .line 1560
    :cond_e
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_f

    .line 1561
    const-string v0, "TSP_ID__REFERENCE__ALL_NODE__RETURN_NONE"

    goto :goto_0

    .line 1562
    :cond_f
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__NODE:I

    if-ne p0, v0, :cond_10

    .line 1563
    const-string v0, "TSP_ID__REFERENCE__NODE"

    goto :goto_0

    .line 1564
    :cond_10
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_11

    .line 1565
    const-string v0, "TSP_ID__DELTA__ALL_NODE__RETURN_MIN_MAX"

    goto :goto_0

    .line 1566
    :cond_11
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_12

    .line 1567
    const-string v0, "TSP_ID__DELTA__ALL_NODE__RETURN_NONE"

    goto :goto_0

    .line 1568
    :cond_12
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__NODE:I

    if-ne p0, v0, :cond_13

    .line 1569
    const-string v0, "TSP_ID__DELTA__NODE"

    goto/16 :goto_0

    .line 1570
    :cond_13
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_14

    .line 1571
    const-string v0, "TSP_ID__CM_ABS__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1572
    :cond_14
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__NODE:I

    if-ne p0, v0, :cond_15

    .line 1573
    const-string v0, "TSP_ID__CM_ABS__NODE"

    goto/16 :goto_0

    .line 1574
    :cond_15
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INSPECTION__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_16

    .line 1575
    const-string v0, "TSP_ID__INSPECTION__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1576
    :cond_16
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INSPECTION__NODE:I

    if-ne p0, v0, :cond_17

    .line 1577
    const-string v0, "TSP_ID__INSPECTION__NODE"

    goto/16 :goto_0

    .line 1578
    :cond_17
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_18

    .line 1579
    const-string v0, "TSP_ID__INTENSITY__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1580
    :cond_18
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__NODE:I

    if-ne p0, v0, :cond_19

    .line 1581
    const-string v0, "TSP_ID__INTENSITY__NODE"

    goto/16 :goto_0

    .line 1582
    :cond_19
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_1a

    .line 1583
    const-string v0, "TSP_ID__SCANTIME__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1584
    :cond_1a
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__NODE:I

    if-ne p0, v0, :cond_1b

    .line 1585
    const-string v0, "TSP_ID__SCANTIME__NODE"

    goto/16 :goto_0

    .line 1586
    :cond_1b
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_1c

    .line 1587
    const-string v0, "TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1588
    :cond_1c
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    if-ne p0, v0, :cond_1d

    .line 1589
    const-string v0, "TSP_ID__RAWCAP__NODE"

    goto/16 :goto_0

    .line 1590
    :cond_1d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_1e

    .line 1591
    const-string v0, "TSP_ID__RX_TO_RX__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1592
    :cond_1e
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__NODE:I

    if-ne p0, v0, :cond_1f

    .line 1593
    const-string v0, "TSP_ID__RX_TO_RX__NODE"

    goto/16 :goto_0

    .line 1594
    :cond_1f
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_20

    .line 1595
    const-string v0, "TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1596
    :cond_20
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__NODE:I

    if-ne p0, v0, :cond_21

    .line 1597
    const-string v0, "TSP_ID__TX_TO_TX__NODE"

    goto/16 :goto_0

    .line 1598
    :cond_21
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_22

    .line 1599
    const-string v0, "TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1600
    :cond_22
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__NODE:I

    if-ne p0, v0, :cond_23

    .line 1601
    const-string v0, "TSP_ID__TX_TO_GND__NODE"

    goto/16 :goto_0

    .line 1602
    :cond_23
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_24

    .line 1603
    const-string v0, "TSP_ID__RAW_COUNT__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1604
    :cond_24
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__NODE:I

    if-ne p0, v0, :cond_25

    .line 1605
    const-string v0, "TSP_ID__RAW_COUNT__NODE"

    goto/16 :goto_0

    .line 1606
    :cond_25
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_26

    .line 1607
    const-string v0, "TSP_ID__DIFFERENCE__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1608
    :cond_26
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__NODE:I

    if-ne p0, v0, :cond_27

    .line 1609
    const-string v0, "TSP_ID__DIFFERENCE__NODE"

    goto/16 :goto_0

    .line 1610
    :cond_27
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_28

    .line 1611
    const-string v0, "TSP_ID__LOCAL_IDAC__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1612
    :cond_28
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__NODE:I

    if-ne p0, v0, :cond_29

    .line 1613
    const-string v0, "TSP_ID__LOCAL_IDAC__NODE"

    goto/16 :goto_0

    .line 1614
    :cond_29
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_2a

    .line 1615
    const-string v0, "TSP_ID__GLOBAL_IDAC__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1616
    :cond_2a
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__NODE:I

    if-ne p0, v0, :cond_2b

    .line 1617
    const-string v0, "TSP_ID__GLOBAL_IDAC__NODE"

    goto/16 :goto_0

    .line 1618
    :cond_2b
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_2c

    .line 1619
    const-string v0, "TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1620
    :cond_2c
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__NODE:I

    if-ne p0, v0, :cond_2d

    .line 1621
    const-string v0, "TSP_ID__MUTUALCAP__NODE"

    goto/16 :goto_0

    .line 1622
    :cond_2d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REGSITER__RESULT:I

    if-ne p0, v0, :cond_2e

    .line 1623
    const-string v0, "TSP_ID__REGSITER__RESULT"

    goto/16 :goto_0

    .line 1624
    :cond_2e
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:I

    if-ne p0, v0, :cond_2f

    .line 1625
    const-string v0, "TSP_HOVER_CMD__HOVER_SET_EDGE"

    goto/16 :goto_0

    .line 1626
    :cond_2f
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__ABS_CAP:I

    if-ne p0, v0, :cond_30

    .line 1627
    const-string v0, "TSP_SIDETOUCH_ID__ABS_CAP"

    goto/16 :goto_0

    .line 1628
    :cond_30
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__DELTA:I

    if-ne p0, v0, :cond_31

    .line 1629
    const-string v0, "TSP_SIDETOUCH_ID__ABS_DELTA"

    goto/16 :goto_0

    .line 1630
    :cond_31
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TRX_SHORT_TEST:I

    if-ne p0, v0, :cond_32

    .line 1631
    const-string v0, "TSP_ID__SHORT_TEST"

    goto/16 :goto_0

    .line 1632
    :cond_32
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p0, v0, :cond_33

    .line 1633
    const-string v0, "TSP_ID__DND__ALL_NODE__RETURN_MIN_MAX"

    goto/16 :goto_0

    .line 1634
    :cond_33
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__ALL_NODE__RETURN_NONE:I

    if-ne p0, v0, :cond_34

    .line 1635
    const-string v0, "TSP_ID__DND__ALL_NODE__RETURN_NONE"

    goto/16 :goto_0

    .line 1636
    :cond_34
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__NODE:I

    if-ne p0, v0, :cond_35

    .line 1637
    const-string v0, "TSP_ID__DND__NODE"

    goto/16 :goto_0

    .line 1640
    :cond_35
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne p0, v0, :cond_36

    .line 1641
    const-string v0, "TSP_ID__EXPANSION__CONNECTION"

    goto/16 :goto_0

    .line 1642
    :cond_36
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_HOVER_ENABLE:I

    if-ne p0, v0, :cond_37

    .line 1643
    const-string v0, "TSP_ID_HOVER_ENABLE"

    goto/16 :goto_0

    .line 1644
    :cond_37
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_HOVER_DISABLE:I

    if-ne p0, v0, :cond_38

    .line 1645
    const-string v0, "TSP_ID_HOVER_DISABLE"

    goto/16 :goto_0

    .line 1647
    :cond_38
    const-string v0, "Unknown"

    goto/16 :goto_0
.end method

.method public static convertorTSPWhat(I)Ljava/lang/String;
    .locals 1
    .param p0, "what"    # I

    .prologue
    .line 1652
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne p0, v0, :cond_0

    .line 1653
    const-string v0, "TSP_WHAT_STATUS_OK"

    .line 1659
    :goto_0
    return-object v0

    .line 1654
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne p0, v0, :cond_1

    .line 1655
    const-string v0, "TSP_WHAT_STATUS_NG"

    goto :goto_0

    .line 1656
    :cond_1
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne p0, v0, :cond_2

    .line 1657
    const-string v0, "TSP_WHAT_STATUS_NA"

    goto :goto_0

    .line 1659
    :cond_2
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method private getTSPNodeAllWidth(II)Ljava/lang/String;
    .locals 9
    .param p1, "tspID"    # I
    .param p2, "number"    # I

    .prologue
    .line 1443
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getTSPNodeAllWidth"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TSP ID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1445
    const-string v3, ""

    .line 1448
    .local v3, "temp":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mIsStandardChannel:Z

    if-eqz v5, :cond_0

    .line 1449
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountY()Ljava/lang/String;

    move-result-object v4

    .line 1454
    .local v4, "width":Ljava/lang/String;
    :goto_0
    const-string v5, "NG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1455
    const-string v5, "NG"

    .line 1484
    :goto_1
    return-object v5

    .line 1451
    .end local v4    # "width":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountX()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "width":Ljava/lang/String;
    goto :goto_0

    .line 1456
    :cond_1
    const-string v5, "NA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1457
    const-string v5, "NA"

    goto :goto_1

    .line 1460
    :cond_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1463
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v0, :cond_7

    .line 1464
    iget-boolean v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mIsStandardChannel:Z

    if-eqz v5, :cond_3

    .line 1465
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, p1, v6}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1470
    .local v2, "result":Ljava/lang/String;
    :goto_3
    const-string v5, "NG"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1471
    const-string v5, "NG"

    goto :goto_1

    .line 1467
    .end local v2    # "result":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, p1, v6}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "result":Ljava/lang/String;
    goto :goto_3

    .line 1472
    :cond_4
    const-string v5, "NA"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1473
    const-string v5, "NA"

    goto :goto_1

    .line 1476
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1478
    add-int/lit8 v5, v0, -0x1

    if-ge v1, v5, :cond_6

    .line 1479
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1463
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 1483
    .end local v2    # "result":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getTSPNodeAllWidth"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v3

    .line 1484
    goto/16 :goto_1
.end method

.method private getTSPResult_Connection(Landroid/os/Handler;)Ljava/lang/String;
    .locals 5
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 521
    if-nez p1, :cond_2

    .line 522
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    invoke-virtual {v1, v2, v0}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 523
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Connection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 542
    .end local v0    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 530
    .restart local v0    # "result":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkTSPConnectionSpec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 532
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 533
    const-string v0, "NG"

    goto :goto_0

    .line 541
    .end local v0    # "result":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method private getTSPResult_Connection_Hovering(Landroid/os/Handler;)V
    .locals 4
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    .line 549
    return-void
.end method

.method private getTSPResult_CxData_All_Result(Landroid/os/Handler;)Ljava/lang/String;
    .locals 4
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v3, 0x0

    .line 563
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    .line 564
    return-object v3
.end method

.method private getTSPResult_CxData_Gap_Result(Landroid/os/Handler;)Ljava/lang/String;
    .locals 4
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v3, 0x0

    .line 558
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    .line 559
    return-object v3
.end method

.method private getTSPResult_Rawcap_Gap_Result(Landroid/os/Handler;)Ljava/lang/String;
    .locals 4
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v3, 0x0

    .line 552
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    .line 553
    return-object v3
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    sget-object v0, Lcom/sec/factory/modules/ModuleTouchScreen;->mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleTouchScreen;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleTouchScreen;->mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;

    .line 74
    :cond_0
    const-string v0, "NA"

    sget-object v1, Lcom/sec/factory/modules/ModuleTouchScreen;->mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    sget-object v0, Lcom/sec/factory/modules/ModuleTouchScreen;->mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-direct {v0}, Lcom/sec/factory/modules/ModuleTouchScreen;->setTSPInfo()V

    .line 78
    :cond_1
    sget-object v0, Lcom/sec/factory/modules/ModuleTouchScreen;->mInstance:Lcom/sec/factory/modules/ModuleTouchScreen;

    return-object v0
.end method

.method private setTSPInfo()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setTSPInfo"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;

    .line 225
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__VENDOR_NAME:I

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    .line 226
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MODULE_VENDOR:I

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPModuleVendor:Ljava/lang/String;

    .line 227
    const-string v0, "PANEL_TYPE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPPanelType:Ljava/lang/String;

    .line 228
    const-string v0, "DEVICE_TYPE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPDeviceType:Ljava/lang/String;

    .line 229
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->setVendor(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setTSPInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTSPVendorName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setTSPInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTSPPanelType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPPanelType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setTSPInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTSPDeviceType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPDeviceType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->setTSPConnectionSpec()V

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TSP_SELFTEST_HOVERING_MIN_Y_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_Y:I

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TSP_SELFTEST_HOVERING_MAX_Y_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_Y:I

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TSP_SELFTEST_HOVERING_MIN_X_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_X:I

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TSP_SELFTEST_HOVERING_MAX_X_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_X:I

    .line 253
    const-string v0, "TSP_SELFTEST_REFRENCE_GAP_X_SYNAPTICS"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPReferenceGapSpec_X:I

    .line 255
    const-string v0, "TSP_SELFTEST_REFRENCE_GAP_Y_SYNAPTICS"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPReferenceGapSpec_Y:I

    .line 257
    const-string v0, "TSP_SELFTEST_CX_DATA_X_STM"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataGapSpec_X:I

    .line 259
    const-string v0, "TSP_SELFTEST_CX_DATA_Y_STM"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataGapSpec_Y:I

    .line 262
    const-string v0, "TSP_SELFTEST_CX_DATA_MIN_STM"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataSpec_Min:I

    .line 264
    const-string v0, "TSP_SELFTEST_CX_DATA_MAX_STM"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPCxDataSpec_Max:I

    .line 266
    const-string v0, "IS_TSP_STANDARD_CHANNEL"

    invoke-static {v0}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mIsStandardChannel:Z

    .line 268
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setTSPInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsStandardChannel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mIsStandardChannel:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    return-void
.end method


# virtual methods
.method public TSPHoveringEdge(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "argu"    # Ljava/lang/String;

    .prologue
    .line 488
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:I

    invoke-virtual {v1, v2, p1}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 490
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "TSPHoveringEdge"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    return-object v0
.end method

.method public getTSPChannelCountX()Ljava/lang/String;
    .locals 5

    .prologue
    .line 443
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountX:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 447
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_X_CHANNEL:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 449
    .local v0, "result":Ljava/lang/String;
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 461
    .end local v0    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 453
    .restart local v0    # "result":Ljava/lang/String;
    :cond_1
    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountX:Ljava/lang/String;

    .line 454
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPChannelCountX"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set X : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    .end local v0    # "result":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPChannelCountX"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "X : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountX:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTSPChannelCountY()Ljava/lang/String;
    .locals 5

    .prologue
    .line 465
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountY:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 469
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_Y_CHANNEL:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 471
    .local v0, "result":Ljava/lang/String;
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 483
    .end local v0    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 475
    .restart local v0    # "result":Ljava/lang/String;
    :cond_1
    iput-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountY:Ljava/lang/String;

    .line 476
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPChannelCountY"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set Y : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    .end local v0    # "result":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPChannelCountY"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Y : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountY:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPChannelCountY:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTSPChipName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 422
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CHIP_NAME:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 424
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPChipName"

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    return-object v0
.end method

.method public getTSPConnectionHoveringSpecMax_X()I
    .locals 1

    .prologue
    .line 410
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_X:I

    return v0
.end method

.method public getTSPConnectionHoveringSpecMax_Y()I
    .locals 1

    .prologue
    .line 402
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Max_Y:I

    return v0
.end method

.method public getTSPConnectionHoveringSpecMin_X()I
    .locals 1

    .prologue
    .line 406
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_X:I

    return v0
.end method

.method public getTSPConnectionHoveringSpecMin_Y()I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionHoveringSpec_Min_Y:I

    return v0
.end method

.method public getTSPConnectionSpecMax()I
    .locals 1

    .prologue
    .line 394
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    return v0
.end method

.method public getTSPConnectionSpecMin()I
    .locals 1

    .prologue
    .line 390
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    return v0
.end method

.method public getTSPDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPDeviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getTSPFirmwareVersionBinary()Ljava/lang/String;
    .locals 4

    .prologue
    .line 429
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_BINARY:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPFirmwareVersionBinary"

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    return-object v0
.end method

.method public getTSPFirmwareVersionIC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 436
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_IC:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 438
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPFirmwareVersionIC"

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    return-object v0
.end method

.method public getTSPModuleVendor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPModuleVendor:Ljava/lang/String;

    return-object v0
.end method

.method public getTSPPanelType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPPanelType:Ljava/lang/String;

    return-object v0
.end method

.method public getTSPReferenceGapSpec_X()I
    .locals 1

    .prologue
    .line 414
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPReferenceGapSpec_X:I

    return v0
.end method

.method public getTSPReferenceGapSpec_Y()I
    .locals 1

    .prologue
    .line 418
    iget v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPReferenceGapSpec_Y:I

    return v0
.end method

.method public getTSPResult(I)Ljava/lang/String;
    .locals 2
    .param p1, "tspID"    # I

    .prologue
    const/4 v1, 0x0

    .line 322
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne p1, v0, :cond_0

    .line 323
    invoke-direct {p0, v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_Connection(Landroid/os/Handler;)Ljava/lang/String;

    move-result-object v0

    .line 325
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-virtual {v0, p1, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTSPResult(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "tspID"    # I
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    .line 314
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne p1, v0, :cond_0

    .line 315
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_Connection(Landroid/os/Handler;)Ljava/lang/String;

    move-result-object v0

    .line 317
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-virtual {v0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTSPResult(ILandroid/os/Handler;)Z
    .locals 2
    .param p1, "tspID"    # I
    .param p2, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 289
    iput-object p2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;

    .line 291
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne p1, v0, :cond_0

    .line 292
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_Connection(Landroid/os/Handler;)Ljava/lang/String;

    .line 309
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 293
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    if-ne p1, v0, :cond_1

    .line 294
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_Connection_Hovering(Landroid/os/Handler;)V

    goto :goto_0

    .line 295
    :cond_1
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    if-ne p1, v0, :cond_2

    .line 296
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_Rawcap_Gap_Result(Landroid/os/Handler;)Ljava/lang/String;

    goto :goto_0

    .line 297
    :cond_2
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_HOVER_ENABLE:I

    if-ne p1, v0, :cond_3

    .line 298
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const-string v1, "1"

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    goto :goto_0

    .line 299
    :cond_3
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID_HOVER_DISABLE:I

    if-ne p1, v0, :cond_4

    .line 300
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const-string v1, "0"

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    goto :goto_0

    .line 301
    :cond_4
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    if-ne p1, v0, :cond_5

    .line 302
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_CxData_Gap_Result(Landroid/os/Handler;)Ljava/lang/String;

    goto :goto_0

    .line 303
    :cond_5
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    if-ne p1, v0, :cond_6

    .line 304
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_CxData_All_Result(Landroid/os/Handler;)Ljava/lang/String;

    goto :goto_0

    .line 306
    :cond_6
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public getTSPResult(ILjava/lang/String;Landroid/os/Handler;)Z
    .locals 1
    .param p1, "tspID"    # I
    .param p2, "subCommand"    # Ljava/lang/String;
    .param p3, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 276
    iput-object p3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPNotiHandler:Landroid/os/Handler;

    .line 278
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne p1, v0, :cond_0

    .line 279
    invoke-direct {p0, p3}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult_Connection(Landroid/os/Handler;)Ljava/lang/String;

    .line 284
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public getTSPResult_Read(II)Ljava/lang/String;
    .locals 10
    .param p1, "readNumber"    # I
    .param p2, "lineNumber"    # I

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1001
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readNumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , lineNumber : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    add-int/lit8 p2, p2, -0x1

    .line 1012
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    const-string v3, "checkNodeHeight"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->checkNodeHeight(I)Ljava/lang/String;

    move-result-object v0

    .line 1015
    .local v0, "result":Ljava/lang/String;
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v1, v0

    .line 1399
    :goto_0
    return-object v1

    .line 1018
    :cond_0
    const-string v1, "NA"

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1019
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleTouchScreen;->setTSPInfo()V

    .line 1026
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "ATMEL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1027
    if-ne p1, v6, :cond_4

    .line 1028
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1030
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    move-object v1, v0

    .line 1035
    goto :goto_0

    .line 1037
    :cond_3
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1040
    :cond_4
    if-ne p1, v7, :cond_54

    .line 1041
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1043
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    move-object v1, v0

    .line 1048
    goto/16 :goto_0

    .line 1050
    :cond_6
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1059
    :cond_7
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "CYPRESS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1060
    if-ne p1, v6, :cond_a

    .line 1061
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1063
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    move-object v1, v0

    .line 1068
    goto/16 :goto_0

    .line 1070
    :cond_9
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1073
    :cond_a
    if-ne p1, v7, :cond_d

    .line 1074
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1076
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_b
    move-object v1, v0

    .line 1081
    goto/16 :goto_0

    .line 1083
    :cond_c
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1086
    :cond_d
    if-ne p1, v8, :cond_10

    .line 1087
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1089
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read3 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_e
    move-object v1, v0

    .line 1094
    goto/16 :goto_0

    .line 1096
    :cond_f
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1099
    :cond_10
    if-ne p1, v9, :cond_54

    .line 1100
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1102
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read4 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    :cond_11
    move-object v1, v0

    .line 1107
    goto/16 :goto_0

    .line 1109
    :cond_12
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1118
    :cond_13
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "MELFAS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1119
    if-ne p1, v6, :cond_16

    .line 1120
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1122
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    :cond_14
    move-object v1, v0

    .line 1127
    goto/16 :goto_0

    .line 1129
    :cond_15
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1131
    :cond_16
    if-ne p1, v7, :cond_19

    .line 1132
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1134
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_17
    move-object v1, v0

    .line 1139
    goto/16 :goto_0

    .line 1141
    :cond_18
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1143
    :cond_19
    if-ne p1, v8, :cond_54

    .line 1144
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1146
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read3 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    :cond_1a
    move-object v1, v0

    .line 1151
    goto/16 :goto_0

    .line 1153
    :cond_1b
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1162
    :cond_1c
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "SILAB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 1163
    if-ne p1, v6, :cond_1d

    .line 1164
    const-string v1, "NG"

    goto/16 :goto_0

    .line 1165
    :cond_1d
    if-ne p1, v7, :cond_1e

    .line 1166
    const-string v1, "NG"

    goto/16 :goto_0

    .line 1167
    :cond_1e
    if-ne p1, v8, :cond_54

    .line 1168
    const-string v1, "NG"

    goto/16 :goto_0

    .line 1177
    :cond_1f
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "SYNAPTICS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 1178
    if-ne p1, v6, :cond_22

    .line 1179
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1181
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_20

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    :cond_20
    move-object v1, v0

    .line 1186
    goto/16 :goto_0

    .line 1188
    :cond_21
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1190
    :cond_22
    if-ne p1, v7, :cond_25

    .line 1191
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1193
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    :cond_23
    move-object v1, v0

    .line 1198
    goto/16 :goto_0

    .line 1200
    :cond_24
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1203
    :cond_25
    if-ne p1, v8, :cond_28

    .line 1204
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1206
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    :cond_26
    move-object v1, v0

    .line 1211
    goto/16 :goto_0

    .line 1213
    :cond_27
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1218
    :cond_28
    if-ne p1, v9, :cond_2b

    .line 1219
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1221
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_29

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    :cond_29
    move-object v1, v0

    .line 1226
    goto/16 :goto_0

    .line 1228
    :cond_2a
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1233
    :cond_2b
    const/4 v1, 0x5

    if-ne p1, v1, :cond_2e

    .line 1234
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1236
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read5 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2c

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    :cond_2c
    move-object v1, v0

    .line 1241
    goto/16 :goto_0

    .line 1243
    :cond_2d
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1246
    :cond_2e
    const/4 v1, 0x6

    if-ne p1, v1, :cond_2f

    .line 1247
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_CAP:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1248
    :cond_2f
    const/4 v1, 0x7

    if-ne p1, v1, :cond_30

    .line 1249
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_DELTA:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1250
    :cond_30
    const/16 v1, 0x8

    if-ne p1, v1, :cond_31

    .line 1251
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__ABS_CAP:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1252
    :cond_31
    const/16 v1, 0x9

    if-ne p1, v1, :cond_54

    .line 1253
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__DELTA:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1260
    :cond_32
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "ZINITIX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_33

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "ZI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 1262
    :cond_33
    if-ne p1, v6, :cond_36

    .line 1263
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1265
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    const-string v1, "NG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_34

    const-string v1, "NA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_35

    :cond_34
    move-object v1, v0

    .line 1269
    goto/16 :goto_0

    .line 1271
    :cond_35
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1274
    :cond_36
    if-ne p1, v7, :cond_39

    .line 1275
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1277
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    const-string v1, "NG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_37

    const-string v1, "NA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    :cond_37
    move-object v1, v0

    .line 1281
    goto/16 :goto_0

    .line 1283
    :cond_38
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1286
    :cond_39
    if-ne p1, v8, :cond_3c

    .line 1287
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1289
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read3 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    const-string v1, "NG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3a

    const-string v1, "NA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3b

    :cond_3a
    move-object v1, v0

    .line 1293
    goto/16 :goto_0

    .line 1295
    :cond_3b
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1297
    :cond_3c
    if-ne p1, v9, :cond_54

    .line 1298
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1299
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read4 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    const-string v1, "NG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    const-string v1, "NA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3e

    :cond_3d
    move-object v1, v0

    .line 1302
    goto/16 :goto_0

    .line 1304
    :cond_3e
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1310
    :cond_3f
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "IMAGIS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_42

    .line 1311
    if-ne p1, v6, :cond_54

    .line 1312
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1314
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1316
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_40

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_41

    :cond_40
    move-object v1, v0

    .line 1318
    goto/16 :goto_0

    .line 1320
    :cond_41
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1325
    :cond_42
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v2, "STM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_53

    .line 1326
    if-ne p1, v6, :cond_45

    .line 1327
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1328
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read1 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_43

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_44

    :cond_43
    move-object v1, v0

    .line 1331
    goto/16 :goto_0

    .line 1333
    :cond_44
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1335
    :cond_45
    if-ne p1, v7, :cond_48

    .line 1336
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1337
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_46

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_47

    :cond_46
    move-object v1, v0

    .line 1340
    goto/16 :goto_0

    .line 1342
    :cond_47
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1344
    :cond_48
    if-ne p1, v8, :cond_4b

    .line 1345
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1346
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read3 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_49

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4a

    :cond_49
    move-object v1, v0

    .line 1349
    goto/16 :goto_0

    .line 1351
    :cond_4a
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1355
    :cond_4b
    if-ne p1, v9, :cond_4e

    .line 1356
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1358
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read2 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1359
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4c

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4d

    :cond_4c
    move-object v1, v0

    .line 1361
    goto/16 :goto_0

    .line 1363
    :cond_4d
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1367
    :cond_4e
    const/4 v1, 0x5

    if-ne p1, v1, :cond_51

    .line 1368
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1369
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read5 == result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4f

    const-string v1, "NA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_50

    :cond_4f
    move-object v1, v0

    .line 1372
    goto/16 :goto_0

    .line 1374
    :cond_50
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__NODE:I

    invoke-direct {p0, v1, p2}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPNodeAllWidth(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1376
    :cond_51
    const/4 v1, 0x6

    if-ne p1, v1, :cond_52

    .line 1377
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const-string v2, "hover_enable"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->setHoverEnable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 1378
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_CAP:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1379
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const-string v2, "hover_enable"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->setHoverEnable(Ljava/lang/String;Ljava/lang/String;)Z

    move-object v1, v0

    .line 1380
    goto/16 :goto_0

    .line 1382
    :cond_52
    const/4 v1, 0x7

    if-ne p1, v1, :cond_54

    .line 1383
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const-string v2, "hover_enable"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->setHoverEnable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 1384
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    sget v2, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_DELTA:I

    invoke-virtual {v1, v2, v5}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPResult(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1385
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSP:Lcom/sec/factory/modules/TouchScreenPanel;

    const-string v2, "hover_enable"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/TouchScreenPanel;->setHoverEnable(Ljava/lang/String;Ljava/lang/String;)Z

    move-object v1, v0

    .line 1386
    goto/16 :goto_0

    .line 1392
    :cond_53
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    const-string v3, "Vendor Name : Unknown"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    const-string v1, "NG"

    goto/16 :goto_0

    .line 1397
    :cond_54
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getTSPResult_Read"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read Number : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (N/A)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    const-string v1, "NA"

    goto/16 :goto_0
.end method

.method public getTSPVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    return-object v0
.end method

.method public isTSPSupport()Z
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "ATMEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "SILAB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "G2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "IMAGIS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 505
    :cond_0
    const/4 v0, 0x1

    .line 507
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTSPConnectionSpec()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 331
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mTSPModuleVendor="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPModuleVendor:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPModuleVendor:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "parsedVendor":[Ljava/lang/String;
    const-string v2, "OK"

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 338
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [ModuleVendorID]: parsedVendor[1]="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    aget-object v2, v1, v7

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 341
    .local v0, "moduleID":I
    if-nez v0, :cond_1

    .line 342
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moduleID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MIN_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    .line 344
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MAX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    .line 368
    .end local v0    # "moduleID":I
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TSP Connection Spec : min="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " max="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    return-void

    .line 345
    .restart local v0    # "moduleID":I
    :cond_1
    if-ne v0, v7, :cond_3

    .line 346
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moduleID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MIN_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_2ND"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    .line 348
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MAX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_2ND"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    .line 349
    iget v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    if-eq v2, v8, :cond_2

    iget v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    if-ne v2, v8, :cond_0

    .line 350
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MIN_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    .line 351
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MAX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    goto/16 :goto_0

    .line 354
    :cond_3
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "moduleID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MIN_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    .line 356
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MAX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    goto/16 :goto_0

    .line 359
    .end local v0    # "moduleID":I
    :cond_4
    const-string v2, "NA"

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "NG"

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 360
    :cond_5
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", applied default TSP Selftest Spec"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MIN_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Min:I

    .line 362
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP_SELFTEST_MAX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPVendorName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->mTSPConnectionSpec_Max:I

    goto/16 :goto_0

    .line 365
    :cond_6
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleTouchScreen;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setTSPConnectionSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", TSP Command ERROR"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
