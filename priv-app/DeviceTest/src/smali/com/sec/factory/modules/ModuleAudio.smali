.class public Lcom/sec/factory/modules/ModuleAudio;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleAudio.java"


# static fields
.field public static final AUDIO_FORMAT:I = 0x2

.field public static final AUDIO_PATH_ALWAYS_MIC_ON:I = 0x5

.field public static final AUDIO_PATH_EAR:I = 0x2

.field public static final AUDIO_PATH_HDMI:I = 0x3

.field public static final AUDIO_PATH_OFF:I = 0x4

.field public static final AUDIO_PATH_RCV:I = 0x1

.field public static final AUDIO_PATH_SPK:I = 0x0

.field public static final AUDIO_SOURCE:I = 0x5

.field public static final CHANNEL_CONFIG:I = 0x3

.field public static final EARKEY_VOLUME_DOWN:I = 0x2

.field public static final EARKEY_VOLUME_DOWN_UP:I = 0x4

.field public static final EARKEY_VOLUME_NONE:I = 0x0

.field public static final EARKEY_VOLUME_UP:I = 0x1

.field public static final EARKEY_VOLUME_UP_DOWN:I = 0x3

.field public static final LOOPBACK_PATH_EAR_EAR:I = 0x5

.field public static final LOOPBACK_PATH_MIC1_SPK:I = 0x9

.field public static final LOOPBACK_PATH_MIC2_EAR:I = 0x7

.field public static final LOOPBACK_PATH_MIC2_RCV:I = 0x6

.field public static final LOOPBACK_PATH_MIC2_SPK:I = 0x4

.field public static final LOOPBACK_PATH_MIC3_EAR:I = 0x8

.field public static final LOOPBACK_PATH_MIC3_SPK:I = 0x2

.field public static final LOOPBACK_PATH_MIC_EAR:I = 0x3

.field public static final LOOPBACK_PATH_MIC_RCV:I = 0x0

.field public static final LOOPBACK_PATH_MIC_SPK:I = 0x1

.field public static final LOOPBACK_PATH_NONE:I = -0x1

.field public static final LOOPBACK_TYPE_CODEC:I = 0x3

.field public static final LOOPBACK_TYPE_PACKET:I = 0x0

.field public static final LOOPBACK_TYPE_PACKET_NODELAY:I = 0x4

.field public static final LOOPBACK_TYPE_PCM:I = 0x1

.field public static final LOOPBACK_TYPE_REALTIME:I = 0x2

.field public static final MIC_1:I = 0x0

.field public static final MIC_2:I = 0x1

.field public static final MIC_3:I = 0x2

.field public static final SAMPLE_RATE:I = 0x3e80

.field public static final SPEAKER_DIRECTION_BOTH:I = 0x2

.field public static final SPEAKER_DIRECTION_LEFT:I = 0x0

.field public static final SPEAKER_DIRECTION_RIGHT:I = 0x1

.field public static final VOLUME_MAX:I = 0x64

.field public static final VOLUME_MAX_INDEX:I = 0xf

.field public static final VOLUME_MIN:I = 0x1e

.field public static final VOLUME_MIN_INDEX:I = 0x4

.field public static final VOLUME_ZERO:I

.field private static mInstance:Lcom/sec/factory/modules/ModuleAudio;


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field private final LOOPBACK_PATH:[Ljava/lang/String;

.field private final LOOPBACK_TYPE:[Ljava/lang/String;

.field buffer:[B

.field private earKeyState:I

.field private mAudioManager:Landroid/media/AudioManager;

.field public mBufferSize:I

.field private mConnectionMode:Ljava/lang/String;

.field private mEarphonePlugged:Z

.field private mIsDoingLoopback:Z

.field public mIsRecording:Z

.field private mLoopbackPreviousPath:I

.field private mLoopbackTestPath:I

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mPacketLoop:Z

.field private mReadBytes:I

.field public mRecorder:Landroid/media/AudioRecord;

.field private mThread:Ljava/lang/Thread;

.field moduleCommon:Lcom/sec/factory/modules/ModuleCommon;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleAudio;->mInstance:Lcom/sec/factory/modules/ModuleAudio;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    const-string v0, "ModuleAudio"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/sec/factory/modules/ModuleAudio;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    .line 74
    iput v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->buffer:[B

    .line 76
    iput v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mReadBytes:I

    .line 77
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsRecording:Z

    .line 228
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "spk"

    aput-object v1, v0, v3

    const-string v1, "rcv"

    aput-object v1, v0, v4

    const-string v1, "ear"

    aput-object v1, v0, v5

    const-string v1, "hdmi"

    aput-object v1, v0, v6

    const-string v1, "off"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "always_mic_on"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->AUDIO_PATH:[Ljava/lang/String;

    .line 466
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mic_rcv;"

    aput-object v1, v0, v3

    const-string v1, "mic_spk;"

    aput-object v1, v0, v4

    const-string v1, "mic3_spk;"

    aput-object v1, v0, v5

    const-string v1, "mic_ear;"

    aput-object v1, v0, v6

    const-string v1, "mic2_spk;"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ear_ear;"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "dualmic_rcv;"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mic2_ear;"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mic3_ear;"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mic1_spk;"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_PATH:[Ljava/lang/String;

    .line 475
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "packet;"

    aput-object v1, v0, v3

    const-string v1, "pcm;"

    aput-object v1, v0, v4

    const-string v1, "realtime"

    aput-object v1, v0, v5

    const-string v1, "codec"

    aput-object v1, v0, v6

    const-string v1, "packet_nodelay;"

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_TYPE:[Ljava/lang/String;

    .line 479
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    .line 480
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    .line 481
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackPreviousPath:I

    .line 482
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mEarphonePlugged:Z

    .line 484
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mPacketLoop:Z

    .line 691
    iput v3, p0, Lcom/sec/factory/modules/ModuleAudio;->earKeyState:I

    .line 27
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleAudio"

    const-string v2, "Create ModuleAudio"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    .line 30
    const-string v0, "MODEL_COMMUNICATION_MODE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mConnectionMode:Ljava/lang/String;

    .line 31
    const-string v0, "SUPPORT_PACKET_LOOPBACK"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mPacketLoop:Z

    .line 32
    return-void
.end method

.method static synthetic access$002(Lcom/sec/factory/modules/ModuleAudio;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleAudio;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/sec/factory/modules/ModuleAudio;->mReadBytes:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/ModuleAudio;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleAudio;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    return-object v0
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleAudio;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    sget-object v0, Lcom/sec/factory/modules/ModuleAudio;->mInstance:Lcom/sec/factory/modules/ModuleAudio;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/sec/factory/modules/ModuleAudio;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleAudio;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleAudio;->mInstance:Lcom/sec/factory/modules/ModuleAudio;

    .line 39
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModuleAudio;->mInstance:Lcom/sec/factory/modules/ModuleAudio;

    return-object v0
.end method

.method public static isSupportSecondMicTest()Z
    .locals 3

    .prologue
    .line 683
    const/4 v0, 0x0

    .line 684
    .local v0, "result":Z
    const-string v1, "SUPPORT_SECOND_MIC_TEST"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 685
    return v0
.end method

.method private release()V
    .locals 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "release"

    const-string v2, "release ModuleAudio"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 335
    :cond_0
    return-void
.end method

.method private stopRecording()Z
    .locals 4

    .prologue
    .line 193
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 195
    :goto_0
    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsRecording:Z

    if-eqz v1, :cond_0

    .line 197
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 203
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    .line 204
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMicCheck()Z

    .line 205
    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 708
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->release()V

    .line 709
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 710
    return-void
.end method

.method public getCurrentLoopbackPath()I
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    return v0
.end method

.method public getEarKeyState()I
    .locals 1

    .prologue
    .line 703
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudio;->earKeyState:I

    return v0
.end method

.method public getMicCount()I
    .locals 2

    .prologue
    .line 677
    const/4 v0, 0x2

    .line 678
    .local v0, "nMic":I
    const-string v1, "MIC_COUNT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 679
    return v0
.end method

.method public getPreviousLoopbackPath()I
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackPreviousPath:I

    return v0
.end method

.method public getRMSValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "rms_value"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStreamMusicVolume()I
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public getStreamMusicVolumeIndex()I
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public initAudioRecord()Z
    .locals 6

    .prologue
    const/16 v2, 0x3e80

    const/4 v3, 0x3

    const/4 v4, 0x2

    .line 114
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "initInputSetting"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->startMicCheck()Z

    .line 117
    invoke-static {v2, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    .line 118
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    .line 119
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->buffer:[B

    .line 122
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v1, 0x5

    iget v5, p0, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mReadBytes:I

    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public isConnectionModeNone()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 665
    const/4 v0, 0x0

    .line 666
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "isConnectionModeNone"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mConnectionMode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleAudio;->mConnectionMode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const-string v2, "none"

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleAudio;->mConnectionMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 669
    :goto_0
    if-ne v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mPacketLoop:Z

    if-eqz v1, :cond_0

    .line 670
    const/4 v0, 0x0

    .line 673
    :cond_0
    return v0

    .line 667
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDoingLoopback()Z
    .locals 1

    .prologue
    .line 643
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    return v0
.end method

.method public isEarphonePlugged()Z
    .locals 2

    .prologue
    .line 655
    const-string v1, "EARJACK_PLUGGED"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 657
    .local v0, "state":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 658
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mEarphonePlugged:Z

    .line 661
    :cond_0
    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mEarphonePlugged:Z

    return v1

    .line 658
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPlayingSound()Z
    .locals 3

    .prologue
    .line 440
    const/4 v1, 0x0

    .line 443
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 448
    :goto_0
    return v1

    .line 443
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public playMedia(IZ)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z

    .prologue
    .line 235
    const/4 v0, 0x2

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZI)V

    .line 236
    return-void
.end method

.method public playMedia(IZI)V
    .locals 4
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z
    .param p3, "direction"    # I

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "playMedia"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLoop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->release()V

    .line 262
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 263
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 264
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 265
    return-void
.end method

.method public playMedia(IZII)V
    .locals 4
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z
    .param p3, "direction"    # I
    .param p4, "StreamType"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "playMedia"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLoop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->release()V

    .line 251
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 252
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 253
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 255
    return-void
.end method

.method public playMedia(IZILandroid/os/Handler;)V
    .locals 4
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z
    .param p3, "direction"    # I
    .param p4, "mhandler"    # Landroid/os/Handler;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "playMedia"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLoop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->release()V

    .line 270
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 271
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 272
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 273
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/sec/factory/modules/ModuleAudio$2;

    invoke-direct {v1, p0, p1, p4}, Lcom/sec/factory/modules/ModuleAudio$2;-><init>(Lcom/sec/factory/modules/ModuleAudio;ILandroid/os/Handler;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 279
    return-void
.end method

.method public playMedia(IZLandroid/os/Handler;)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z
    .param p3, "mhandler"    # Landroid/os/Handler;

    .prologue
    .line 239
    const/4 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZILandroid/os/Handler;)V

    .line 240
    return-void
.end method

.method public playMedia(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "dataSource"    # Ljava/lang/String;
    .param p2, "isLoop"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x2

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(Ljava/lang/String;ZI)V

    .line 244
    return-void
.end method

.method public playMedia(Ljava/lang/String;ZI)V
    .locals 5
    .param p1, "dataSource"    # Ljava/lang/String;
    .param p2, "isLoop"    # Z
    .param p3, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 282
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "playMedia"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dataSource="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLoop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->release()V

    .line 285
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 287
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 288
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 289
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 290
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 292
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 293
    return-void
.end method

.method public sendToAudioManagerFTAOnOff(Z)V
    .locals 6
    .param p1, "on"    # Z

    .prologue
    .line 43
    const-string v1, "com.sec.factory.app.factorytest.FTA_ON"

    .line 44
    .local v1, "ON":Ljava/lang/String;
    const-string v0, "com.sec.factory.app.factorytest.FTA_OFF"

    .line 46
    .local v0, "OFF":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 47
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendToAudioManagerFTAOnOff"

    const-string v5, " com.sec.factory.app.factorytest.FTA_OFF"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.factory.app.factorytest.FTA_OFF"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 49
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleAudio;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 50
    :cond_0
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 51
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendToAudioManagerFTAOnOff"

    const-string v5, "com.sec.factory.app.factorytest.FTA_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.factory.app.factorytest.FTA_ON"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 53
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleAudio;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 55
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendToAudioManagerFTAOnOff"

    const-string v5, "Invalid parameter"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAudioPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 430
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioPath : factory_test_route="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleAudio;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_route="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 432
    return-void
.end method

.method public setDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 313
    packed-switch p1, :pswitch_data_0

    .line 326
    :pswitch_0
    return-void

    .line 313
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setEarKeyState(I)V
    .locals 0
    .param p1, "earKeyState"    # I

    .prologue
    .line 699
    iput p1, p0, Lcom/sec/factory/modules/ModuleAudio;->earKeyState:I

    .line 700
    return-void
.end method

.method public setLoopbackPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setLoopbackPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Change Loopback path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 636
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 637
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 638
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackPreviousPath:I

    .line 639
    iput p1, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    .line 640
    return-void
.end method

.method public setLoopbackPath(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setLoopbackPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Change Loopback path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 625
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 627
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 628
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 629
    return-void
.end method

.method public setMicPath(I)Z
    .locals 4
    .param p1, "micPath"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMicPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    packed-switch p1, :pswitch_data_0

    .line 110
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 99
    :pswitch_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=main"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :pswitch_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=sub"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :pswitch_2
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=third"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMode(I)V

    .line 427
    return-void
.end method

.method public setParam(Ljava/lang/String;)Z
    .locals 4
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    .line 716
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setParam"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "param = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 723
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setParam"

    const-string v2, "mAudioManager is null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPhoneType(Ljava/lang/String;)V
    .locals 4
    .param p1, "phoneType"    # Ljava/lang/String;

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setPhoneType"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "phone type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "phone_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method public setStreamMusicVolume(I)V
    .locals 6
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v5, 0x3

    const/4 v0, 0x0

    .line 371
    if-le p1, v1, :cond_0

    move p1, v1

    .line 372
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setStreamMusicVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volme="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 376
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 379
    :cond_2
    if-nez p1, :cond_3

    .line 380
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v5, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 385
    :goto_0
    return-void

    .line 382
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v5, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public setStreamMusicVolumeIndex(I)V
    .locals 6
    .param p1, "volumeIndex"    # I

    .prologue
    const/16 v1, 0xf

    const/4 v5, 0x3

    const/4 v0, 0x0

    .line 405
    if-le p1, v1, :cond_0

    move p1, v1

    .line 406
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setStreamMusicVolumeIndex"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volme Index ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 410
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 413
    :cond_2
    if-nez p1, :cond_3

    .line 414
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v5, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 418
    :goto_0
    return-void

    .line 416
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v5, p1, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public setStreamMusicVolumeMax()V
    .locals 1

    .prologue
    .line 342
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 343
    return-void
.end method

.method public setStreamMusicVolumeMaxIndex()V
    .locals 1

    .prologue
    .line 354
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeIndex(I)V

    .line 355
    return-void
.end method

.method public setStreamMusicVolumeMin()V
    .locals 1

    .prologue
    .line 346
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 347
    return-void
.end method

.method public setStreamMusicVolumeZero()V
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 351
    return-void
.end method

.method public setStreamVoiceVolume(I)V
    .locals 5
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 358
    if-le p1, v1, :cond_0

    move p1, v1

    .line 359
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 360
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setStreamVoiceVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volme="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    if-nez p1, :cond_2

    .line 363
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 368
    :goto_0
    return-void

    .line 365
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v0, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public setStreamVoiceVolumeMax()V
    .locals 1

    .prologue
    .line 338
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamVoiceVolume(I)V

    .line 339
    return-void
.end method

.method public setStreamVolume(II)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 388
    if-le p2, v1, :cond_0

    move p2, v1

    .line 389
    :cond_0
    if-gez p2, :cond_1

    move p2, v0

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setStreamVoiceVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volme="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    if-nez p2, :cond_2

    .line 393
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p1, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 398
    :goto_0
    return-void

    .line 395
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p2

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, p1, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public startLoopback(II)V
    .locals 4
    .param p1, "path"    # I
    .param p2, "type"    # I

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loopback start path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 520
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 522
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 525
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    .line 526
    iput p1, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    .line 528
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 529
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback"

    const-string v2, "none"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleAudio$4;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleAudio$4;-><init>(Lcom/sec/factory/modules/ModuleAudio;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 538
    :cond_1
    return-void
.end method

.method public startLoopback(Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 488
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loopback start path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 492
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 493
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 496
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    .line 498
    sget-object v0, Lcom/sec/factory/modules/ModuleAudio;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback"

    const-string v2, "none"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleAudio$3;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleAudio$3;-><init>(Lcom/sec/factory/modules/ModuleAudio;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 508
    :cond_1
    return-void
.end method

.method public startLoopback2(II)V
    .locals 5
    .param p1, "path"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v4, 0x1

    .line 567
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loopback start path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 575
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 577
    const-string v0, "SUPPORT_PIEZO_RCV"

    invoke-static {v0, v4}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 578
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "piezoHandset=true"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 580
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 583
    iput-boolean v4, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    .line 584
    iput p1, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    .line 586
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 587
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback"

    const-string v2, "none"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleAudio$6;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleAudio$6;-><init>(Lcom/sec/factory/modules/ModuleAudio;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 596
    :cond_2
    return-void
.end method

.method public startLoopback2(Ljava/lang/String;I)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    const/4 v4, 0x1

    .line 541
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loopback start path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 545
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 546
    const-string v0, "SUPPORT_PIEZO_RCV"

    invoke-static {v0, v4}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 547
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "piezoHandset=true"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 549
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 552
    iput-boolean v4, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    .line 554
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startLoopback"

    const-string v2, "none"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleAudio$5;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleAudio$5;-><init>(Lcom/sec/factory/modules/ModuleAudio;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 564
    :cond_2
    return-void
.end method

.method public startMicCheck()Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startMicCheck"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public startRecording(I)Z
    .locals 4
    .param p1, "duration"    # I

    .prologue
    .line 131
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/factory/modules/ModuleAudio$1;

    invoke-direct {v2, p0}, Lcom/sec/factory/modules/ModuleAudio$1;-><init>(Lcom/sec/factory/modules/ModuleAudio;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    .line 179
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopRecording()Z

    .line 189
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 182
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopRecording()Z

    goto :goto_0

    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->stopRecording()Z

    throw v1
.end method

.method public stopLoopback()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 599
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startLoopback"

    const-string v3, "Loopback stop"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    invoke-static {}, Lcom/sec/factory/modules/ModuleAudio;->isSupportSecondMicTest()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 603
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "dualmic_enabled=false"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 606
    :cond_0
    const-string v1, "SUPPORT_PIEZO_RCV"

    invoke-static {v1, v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v5, :cond_1

    .line 607
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "piezoHandset=false"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 609
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_loopback=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 610
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mIsDoingLoopback:Z

    .line 611
    iput v4, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackTestPath:I

    .line 612
    iput v4, p0, Lcom/sec/factory/modules/ModuleAudio;->mLoopbackPreviousPath:I

    .line 614
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 615
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "stopLoopback"

    const-string v3, "none"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 617
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudio;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/factory/modules/ModuleAudioService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 618
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudio;->stopService(Landroid/content/Intent;)V

    .line 620
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    return-void
.end method

.method public stopMedia()V
    .locals 4

    .prologue
    .line 296
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "stopMedia"

    const-string v3, "Stop media"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 302
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "stopMedia : delay"

    const-string v3, " : 50ms"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :goto_0
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudio;->release()V

    .line 310
    :cond_0
    return-void

    .line 304
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopMicCheck()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "stopMicCheck"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 88
    const/4 v0, 0x1

    return v0
.end method
