.class public Lcom/sec/factory/modules/SensorRead;
.super Ljava/lang/Object;
.source "SensorRead.java"


# static fields
.field public static final RETURN_DATA_ARRAY_INDEX_1_NG:Ljava/lang/String; = "NG"

.field public static final RETURN_DATA_ARRAY_INDEX_1_NONE:Ljava/lang/String; = "None"

.field public static final RETURN_DATA_ARRAY_INDEX_1_OK:Ljava/lang/String; = "OK"

.field private static final STANDARD_GRAVITY:F = 9.80665f

.field public static final TARGET_FILE:I = 0x2

.field public static final TARGET_INTENT:I = 0x3

.field public static final TARGET_MANAGER:I = 0x1


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final RETURN_DATA_ARRAY_SIZE_MAX:I

.field private final mAccelBitCntXML:Ljava/lang/String;

.field private final mAccelSensorName:Ljava/lang/String;

.field private final mFeature_Gyroscope:Ljava/lang/String;

.field private final mFeature_Magnetic:Ljava/lang/String;

.field private final mIsAccelReverse:Z

.field private mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

.field private mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

.field private mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

.field private mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, "SensorRead"

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->CLASS_NAME:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    .line 20
    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    .line 21
    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    .line 29
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/factory/modules/SensorRead;->RETURN_DATA_ARRAY_SIZE_MAX:I

    .line 37
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GYRO:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Gyroscope:Ljava/lang/String;

    .line 39
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    .line 46
    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 47
    const-string v0, "SENSOR_TEST_ACC_BIT"

    invoke-static {v0}, Lcom/sec/factory/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mAccelBitCntXML:Ljava/lang/String;

    .line 48
    const-string v0, "SENSOR_NAME_ACCELEROMETER"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mAccelSensorName:Ljava/lang/String;

    .line 49
    const-string v0, "IS_SENSOR_TEST_ACC_REVERSE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/SensorRead;->mIsAccelReverse:Z

    .line 55
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/sec/factory/modules/SensorReadFile;

    invoke-direct {v0, v2}, Lcom/sec/factory/modules/SensorReadFile;-><init>([I)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    .line 58
    :cond_0
    return-void
.end method

.method private dataCheck([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 2517
    const-string v2, ""

    .line 2518
    .local v2, "result":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2520
    .local v1, "length":I
    if-eqz p1, :cond_2

    .line 2521
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2522
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    .line 2523
    const-string v3, "SensorRead"

    const-string v4, "dataCheck"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "length : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2526
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v3, v1, 0x1

    if-ge v0, v3, :cond_2

    .line 2527
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2529
    if-ge v0, v1, :cond_1

    .line 2530
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2526
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2535
    .end local v0    # "i":I
    :cond_2
    return-object v2
.end method

.method private replaceData([Ljava/lang/String;)V
    .locals 6
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 2539
    const/4 v1, 0x0

    .line 2541
    .local v1, "length":I
    if-eqz p1, :cond_0

    .line 2542
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2543
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v2, v1, 0x1

    if-ge v0, v2, :cond_0

    .line 2544
    aget-object v2, p1, v0

    const-string v3, ","

    const-string v4, "."

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v0

    .line 2543
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2548
    .end local v0    # "i":I
    :cond_0
    const-string v2, "SensorRead"

    const-string v3, "replaceData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2549
    return-void
.end method


# virtual methods
.method public SensorOff([I[ILandroid/hardware/SensorManager;)V
    .locals 4
    .param p1, "idManager"    # [I
    .param p2, "idFile"    # [I
    .param p3, "sm"    # Landroid/hardware/SensorManager;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v0, :cond_0

    .line 88
    const-string v1, "SensorRead"

    const-string v2, "SensorOff"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File, ID="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p2, :cond_2

    invoke-static {p2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    invoke-virtual {v0, p2}, Lcom/sec/factory/modules/SensorReadFile;->sensorOff([I)V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v0, :cond_1

    .line 94
    const-string v1, "SensorRead"

    const-string v2, "SensorOff"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Manager, ID="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_3

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v0, p1, p3}, Lcom/sec/factory/modules/SensorReadManager;->sensorOff([ILandroid/hardware/SensorManager;)V

    .line 98
    :cond_1
    return-void

    .line 88
    :cond_2
    const-string v0, "null"

    goto :goto_0

    .line 94
    :cond_3
    const-string v0, "null"

    goto :goto_1
.end method

.method public declared-synchronized SensorOn(Landroid/content/Context;Landroid/hardware/SensorManager;[I[I[I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sm"    # Landroid/hardware/SensorManager;
    .param p3, "idManager"    # [I
    .param p4, "idFile"    # [I
    .param p5, "idIntent"    # [I

    .prologue
    .line 62
    monitor-enter p0

    if-eqz p3, :cond_0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-nez v0, :cond_3

    .line 64
    new-instance v0, Lcom/sec/factory/modules/SensorReadManager;

    invoke-direct {v0, p3, p2}, Lcom/sec/factory/modules/SensorReadManager;-><init>([ILandroid/hardware/SensorManager;)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    .line 70
    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    .line 71
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-nez v0, :cond_4

    .line 72
    new-instance v0, Lcom/sec/factory/modules/SensorReadFile;

    invoke-direct {v0, p4}, Lcom/sec/factory/modules/SensorReadFile;-><init>([I)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    .line 78
    :cond_1
    :goto_1
    if-eqz p5, :cond_2

    .line 79
    new-instance v0, Lcom/sec/factory/modules/SensorReadIntent;

    invoke-direct {v0, p1, p5}, Lcom/sec/factory/modules/SensorReadIntent;-><init>(Landroid/content/Context;[I)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    .line 82
    :cond_2
    const-string v0, "SensorRead"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mAccelBitCntXML : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mAccelBitCntXML:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "SensorRead"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsAccelReverse : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/factory/modules/SensorRead;->mIsAccelReverse:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    .line 66
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, p2, v1}, Lcom/sec/factory/modules/SensorReadManager;->turnOnOffSensor([ILandroid/hardware/SensorManager;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 74
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    invoke-virtual {v0, p4}, Lcom/sec/factory/modules/SensorReadFile;->sensorOn([I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public getAccelermeterCal(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 484
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 485
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 519
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 487
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v7, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_6

    .line 488
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 490
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 491
    array-length v2, v1

    if-gt v2, v6, :cond_2

    .line 492
    aget-object v2, v1, v8

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 495
    :cond_2
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_3

    .line 496
    const-string v2, "SensorRead"

    const-string v3, "getAccelermeterCal"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :cond_3
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 500
    const-string v2, "None"

    aput-object v2, v0, v6

    .line 501
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 502
    const/4 v2, 0x3

    aget-object v3, v1, v6

    aput-object v3, v0, v2

    .line 503
    const/4 v2, 0x4

    aget-object v3, v1, v7

    aput-object v3, v0, v2

    .line 515
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 516
    const-string v2, "SensorRead"

    const-string v3, "getAccelermeterCal"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 505
    :cond_4
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_5

    .line 506
    const-string v3, "SensorRead"

    const-string v4, "getAccelermeterCal"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v0, v2

    .line 509
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_6
    move-object v0, v2

    .line 512
    goto :goto_0
.end method

.method public getAccelermeterIntpin(I)[Ljava/lang/String;
    .locals 12
    .param p1, "target"    # I

    .prologue
    .line 526
    const/16 v8, 0x11

    new-array v4, v8, [Ljava/lang/String;

    .line 527
    .local v4, "returnData":[Ljava/lang/String;
    const-string v1, "2"

    .line 528
    .local v1, "INTPIN_ENABLE":Ljava/lang/String;
    const-string v0, "0"

    .line 530
    .local v0, "INTPIN_DISABLE":Ljava/lang/String;
    const/4 v8, 0x1

    if-ne p1, v8, :cond_1

    iget-object v8, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v8, :cond_1

    .line 531
    const/4 v4, 0x0

    .line 593
    .end local v4    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 532
    .restart local v4    # "returnData":[Ljava/lang/String;
    :cond_1
    const/4 v8, 0x2

    if-ne p1, v8, :cond_a

    iget-object v8, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v8, :cond_a

    .line 533
    const-string v8, "ACCEL_SENSOR_INTPIN"

    invoke-static {v8, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 535
    :try_start_0
    const-string v8, "SUPPORT_ACC_SENSOR_BMC150"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 536
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :goto_1
    const/4 v5, 0x0

    .line 546
    .local v5, "tempString":[Ljava/lang/String;
    const-string v8, "SUPPORT_ADSP_SENSOR_TEMPORARY"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    const-string v8, "ADSP"

    const-string v9, "ACCELEROMETER_SENSOR_TYPE"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 548
    const-string v8, "SensorRead"

    const-string v9, "getAccelermeterIntpin"

    const-string v10, "Sleep 2500"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-wide/16 v6, 0x64

    .line 550
    .local v6, "time":J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    const/4 v8, 0x7

    if-ge v3, v8, :cond_2

    .line 551
    const-string v8, "SensorRead"

    const-string v9, "getAccelermeterIntpin"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Sleep : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ms"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 557
    :goto_3
    const-wide/16 v6, 0x190

    .line 558
    iget-object v8, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v9, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v5

    .line 559
    if-eqz v5, :cond_6

    const-string v8, "1"

    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 560
    const-string v8, "SensorRead"

    const-string v9, "getAccelermeterIntpin"

    const-string v10, "Success CAL"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    .end local v3    # "i":I
    .end local v6    # "time":J
    :cond_2
    :goto_4
    if-eqz v5, :cond_8

    .line 569
    sget v8, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v9, 0x1

    if-gt v8, v9, :cond_3

    .line 570
    const-string v8, "SensorRead"

    const-string v9, "getAccelermeterIntpin"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Count : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, v5

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    :cond_3
    const/4 v8, 0x0

    const-string v9, "2"

    aput-object v9, v4, v8

    .line 574
    const/4 v8, 0x1

    const-string v9, "None"

    aput-object v9, v4, v8

    .line 575
    const/4 v8, 0x2

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v4, v8

    .line 576
    const-string v8, "ACCEL_SENSOR_INTPIN"

    invoke-static {v8, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 589
    .end local v5    # "tempString":[Ljava/lang/String;
    :cond_4
    sget v8, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v9, 0x2

    if-gt v8, v9, :cond_0

    .line 590
    const-string v8, "SensorRead"

    const-string v9, "getAccelermeterIntpin"

    invoke-direct {p0, v4}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 538
    :cond_5
    const-wide/16 v8, 0x12c

    :try_start_2
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 540
    :catch_0
    move-exception v2

    .line 541
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1

    .line 554
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .restart local v3    # "i":I
    .restart local v5    # "tempString":[Ljava/lang/String;
    .restart local v6    # "time":J
    :catch_1
    move-exception v2

    .line 555
    .restart local v2    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 550
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 565
    .end local v3    # "i":I
    .end local v6    # "time":J
    :cond_7
    iget-object v8, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v9, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 578
    :cond_8
    sget v8, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v9, 0x2

    if-gt v8, v9, :cond_9

    .line 579
    const-string v8, "SensorRead"

    const-string v9, "getAccelermeterIntpin"

    const-string v10, "null"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 586
    .end local v5    # "tempString":[Ljava/lang/String;
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public getAccelermeterSelf(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x2

    .line 420
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 421
    .local v0, "returnData":[Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v2

    .line 422
    .local v2, "tempString":[Ljava/lang/String;
    new-array v1, v7, [I

    .line 424
    .local v1, "tempInt":[I
    if-eqz v2, :cond_1

    .line 425
    aget-object v3, v2, v6

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v4

    .line 426
    aget-object v3, v2, v7

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v5

    .line 427
    const/4 v3, 0x4

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v6

    .line 428
    const-string v3, "2"

    aput-object v3, v0, v4

    .line 429
    const-string v3, "None"

    aput-object v3, v0, v5

    .line 430
    aget v3, v1, v4

    aget v4, v1, v5

    aget v5, v1, v6

    invoke-static {v3, v4, v5}, Lcom/sec/factory/modules/SensorCalculator;->getResultAccelerometerSelf(III)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    .line 440
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_0

    .line 441
    const-string v3, "SensorRead"

    const-string v4, "getAccelermeterSelf"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 433
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_2

    .line 434
    const-string v3, "SensorRead"

    const-string v4, "getAccelermeterSelf"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAccelermeterSelfTest(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 451
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 452
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v8, :cond_3

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_3

    .line 453
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELFTEST:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 455
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 456
    array-length v2, v1

    if-gt v2, v6, :cond_0

    .line 457
    aget-object v2, v1, v7

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 460
    :cond_0
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 461
    const-string v2, "SensorRead"

    const-string v3, "getAccelermeterSelfTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_1
    const-string v2, "4"

    aput-object v2, v0, v7

    .line 465
    aget-object v2, v1, v7

    aput-object v2, v0, v6

    .line 466
    aget-object v2, v1, v6

    aput-object v2, v0, v8

    .line 467
    aget-object v2, v1, v8

    aput-object v2, v0, v9

    .line 468
    const/4 v2, 0x4

    aget-object v3, v1, v9

    aput-object v3, v0, v2

    .line 477
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempString":[Ljava/lang/String;
    :goto_0
    return-object v0

    .line 470
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempString":[Ljava/lang/String;
    :cond_2
    const-string v3, "SensorRead"

    const-string v4, "getAccelermeterSelfTest"

    const-string v5, "No SensorData"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 471
    goto :goto_0

    .line 474
    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_3
    const-string v3, "SensorRead"

    const-string v4, "getAccelermeterSelfTest"

    const-string v5, "Not supported Sensor Interface"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 475
    goto :goto_0
.end method

.method public getAccelermeterXYZ(I)[Ljava/lang/String;
    .locals 13
    .param p1, "target"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 181
    const/16 v6, 0x11

    new-array v2, v6, [Ljava/lang/String;

    .line 182
    .local v2, "returnData":[Ljava/lang/String;
    if-ne p1, v9, :cond_a

    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v6, :cond_a

    .line 183
    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v6}, Lcom/sec/factory/modules/SensorReadManager;->returnAccelermeter()[F

    move-result-object v3

    .line 186
    .local v3, "tempFloat":[F
    if-eqz v3, :cond_8

    .line 187
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v9, :cond_0

    .line 188
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    const-string v5, "4"

    aput-object v5, v2, v11

    .line 192
    const-string v5, "None"

    aput-object v5, v2, v9

    .line 193
    const-wide/16 v0, 0x0

    .line 195
    .local v0, "changeBit":D
    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mAccelSensorName:Ljava/lang/String;

    const-string v6, "MAXIM"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 196
    const-wide v0, 0x4097e64c20000000L    # 1529.5743408203125

    .line 197
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ(MAXIM)"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Weight : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_0
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v9, :cond_1

    .line 212
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Weight : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_1
    iget-boolean v5, p0, Lcom/sec/factory/modules/SensorRead;->mIsAccelReverse:Z

    if-eqz v5, :cond_7

    .line 227
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v3, v9

    float-to-double v6, v6

    mul-double/2addr v6, v0

    double-to-int v6, v6

    mul-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v10

    .line 228
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v3, v11

    float-to-double v6, v6

    mul-double/2addr v6, v0

    double-to-int v6, v6

    mul-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v12

    .line 234
    :goto_1
    const/4 v5, 0x4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v3, v10

    float-to-double v8, v7

    mul-double/2addr v8, v0

    double-to-int v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 266
    .end local v0    # "changeBit":D
    .end local v3    # "tempFloat":[F
    :goto_2
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v10, :cond_2

    .line 267
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ"

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    .end local v2    # "returnData":[Ljava/lang/String;
    :cond_2
    :goto_3
    return-object v2

    .line 199
    .restart local v0    # "changeBit":D
    .restart local v2    # "returnData":[Ljava/lang/String;
    .restart local v3    # "tempFloat":[F
    :cond_3
    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mAccelBitCntXML:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 200
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v9, :cond_4

    .line 201
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ"

    const-string v7, " bitCount(feature) : Unknown"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_4
    invoke-static {}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerRawDataWeight_Spec()F

    move-result v5

    float-to-double v0, v5

    goto/16 :goto_0

    .line 205
    :cond_5
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v9, :cond_6

    .line 206
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ"

    const-string v7, " bitCount(feature) : Known"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_6
    invoke-static {}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerRawDataWeight_Feature()F

    move-result v5

    float-to-double v0, v5

    goto/16 :goto_0

    .line 230
    :cond_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v3, v11

    float-to-double v6, v6

    mul-double/2addr v6, v0

    double-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v10

    .line 231
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v3, v9

    float-to-double v6, v6

    mul-double/2addr v6, v0

    double-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v12

    goto/16 :goto_1

    .line 236
    .end local v0    # "changeBit":D
    :cond_8
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v6, v10, :cond_9

    .line 237
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZ"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move-object v2, v5

    .line 240
    goto :goto_3

    .line 242
    .end local v3    # "tempFloat":[F
    :cond_a
    if-ne p1, v10, :cond_e

    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v6, :cond_e

    .line 243
    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v7, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    invoke-virtual {v6, v7}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v4

    .line 245
    .local v4, "tempString":[Ljava/lang/String;
    if-eqz v4, :cond_c

    .line 246
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v9, :cond_b

    .line 247
    const-string v5, "SensorRead"

    const-string v6, "getAccelermeterXYZ"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_b
    const-string v5, "4"

    aput-object v5, v2, v11

    .line 251
    const-string v5, "None"

    aput-object v5, v2, v9

    .line 252
    aget-object v5, v4, v11

    aput-object v5, v2, v10

    .line 253
    aget-object v5, v4, v9

    aput-object v5, v2, v12

    .line 254
    const/4 v5, 0x4

    aget-object v6, v4, v10

    aput-object v6, v2, v5

    goto/16 :goto_2

    .line 256
    :cond_c
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v6, v10, :cond_d

    .line 257
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZ"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object v2, v5

    .line 260
    goto/16 :goto_3

    .end local v4    # "tempString":[Ljava/lang/String;
    :cond_e
    move-object v2, v5

    .line 263
    goto/16 :goto_3
.end method

.method public getAccelermeterXYZnAngle(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    .line 278
    const/16 v6, 0x11

    new-array v2, v6, [Ljava/lang/String;

    .line 279
    .local v2, "returnData":[Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v5

    .line 280
    .local v5, "tempString":[Ljava/lang/String;
    const/4 v6, 0x3

    new-array v4, v6, [I

    .line 282
    .local v4, "tempInt":[I
    if-eqz v5, :cond_2

    .line 283
    const/4 v6, 0x0

    const/4 v7, 0x2

    aget-object v7, v5, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v4, v6

    .line 284
    const/4 v6, 0x1

    const/4 v7, 0x3

    aget-object v7, v5, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v4, v6

    .line 285
    const/4 v6, 0x2

    const/4 v7, 0x4

    aget-object v7, v5, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v4, v6

    .line 286
    invoke-static {v4}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngle([I)[Ljava/lang/String;

    move-result-object v5

    .line 291
    const/4 v6, 0x1

    if-ne p1, v6, :cond_3

    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v6, :cond_3

    .line 292
    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v6}, Lcom/sec/factory/modules/SensorReadManager;->returnAccelermeter()[F

    move-result-object v3

    .line 294
    .local v3, "tempFloat":[F
    if-eqz v3, :cond_0

    if-eqz v5, :cond_0

    .line 295
    const/4 v6, 0x0

    const-string v7, "10"

    aput-object v7, v2, v6

    .line 296
    const/4 v6, 0x1

    const-string v7, "None"

    aput-object v7, v2, v6

    .line 297
    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 298
    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 299
    const/4 v6, 0x4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x2

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 300
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 301
    const/4 v6, 0x6

    const/4 v7, 0x1

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 302
    const/4 v6, 0x7

    const/4 v7, 0x2

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 303
    const/16 v6, 0x8

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v3, v8

    const/4 v9, 0x1

    aget v9, v3, v9

    const/4 v10, 0x2

    aget v10, v3, v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngleMagnitude(FFF)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 306
    const/16 v6, 0x9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x8

    aget-object v8, v2, v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-static {v8}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngleDeviation(F)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 308
    const/16 v6, 0xa

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v3, v8

    const/4 v9, 0x1

    aget v9, v3, v9

    invoke-static {v8, v9}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngleXY(FF)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 409
    .end local v3    # "tempFloat":[F
    :cond_0
    :goto_0
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v7, 0x3

    if-gt v6, v7, :cond_1

    .line 410
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterSelf"

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    .end local v2    # "returnData":[Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v2

    .line 288
    .restart local v2    # "returnData":[Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 323
    :cond_3
    const/4 v6, 0x2

    if-ne p1, v6, :cond_6

    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v6, :cond_6

    .line 324
    if-eqz v5, :cond_4

    .line 325
    const/4 v6, 0x0

    const-string v7, "7"

    aput-object v7, v2, v6

    .line 326
    const/4 v6, 0x1

    const-string v7, "None"

    aput-object v7, v2, v6

    .line 327
    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 328
    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 329
    const/4 v6, 0x4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x2

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 330
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 331
    const/4 v6, 0x6

    const/4 v7, 0x1

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 332
    const/4 v6, 0x7

    const/4 v7, 0x2

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 334
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v7, 0x2

    if-gt v6, v7, :cond_0

    .line 335
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZNAngle"

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 338
    :cond_4
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v7, 0x2

    if-gt v6, v7, :cond_5

    .line 339
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 346
    :cond_6
    const/4 v6, 0x3

    if-ne p1, v6, :cond_c

    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    if-eqz v6, :cond_c

    .line 347
    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    invoke-virtual {v6}, Lcom/sec/factory/modules/SensorReadIntent;->returnCPsAccelerometerData()[I

    move-result-object v4

    .line 348
    const/4 v6, 0x3

    new-array v3, v6, [F

    .line 350
    .restart local v3    # "tempFloat":[F
    if-eqz v4, :cond_7

    .line 351
    invoke-static {v4}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngle([I)[Ljava/lang/String;

    move-result-object v5

    .line 354
    :cond_7
    if-eqz v4, :cond_a

    if-eqz v5, :cond_a

    .line 355
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v7, 0x1

    if-gt v6, v7, :cond_8

    .line 356
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    invoke-static {v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_8
    const/4 v6, 0x0

    const-string v7, "10"

    aput-object v7, v2, v6

    .line 359
    const/4 v6, 0x1

    const-string v7, "None"

    aput-object v7, v2, v6

    .line 360
    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 361
    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 362
    const/4 v6, 0x4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x2

    aget v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 363
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 364
    const/4 v6, 0x6

    const/4 v7, 0x1

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 365
    const/4 v6, 0x7

    const/4 v7, 0x2

    aget-object v7, v5, v7

    aput-object v7, v2, v6

    .line 366
    const-wide/16 v0, 0x0

    .line 368
    .local v0, "changeBit":D
    const-string v6, "12"

    const-string v7, "SENSOR_TEST_ACC_BIT"

    invoke-static {v7}, Lcom/sec/factory/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 369
    const-wide v0, 0x405a1f58d0fac687L    # 104.48979591836734

    .line 374
    :goto_2
    const/4 v6, 0x0

    const/high16 v7, -0x40800000    # -1.0f

    const/4 v8, 0x1

    aget v8, v4, v8

    int-to-double v8, v8

    div-double/2addr v8, v0

    double-to-float v8, v8

    mul-float/2addr v7, v8

    aput v7, v3, v6

    .line 375
    const/4 v6, 0x1

    const/high16 v7, -0x40800000    # -1.0f

    const/4 v8, 0x0

    aget v8, v4, v8

    int-to-double v8, v8

    div-double/2addr v8, v0

    double-to-float v8, v8

    mul-float/2addr v7, v8

    aput v7, v3, v6

    .line 376
    const/4 v6, 0x2

    const/4 v7, 0x2

    aget v7, v4, v7

    int-to-double v8, v7

    div-double/2addr v8, v0

    double-to-float v7, v8

    aput v7, v3, v6

    .line 377
    const/16 v6, 0x8

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v3, v8

    const/4 v9, 0x1

    aget v9, v3, v9

    const/4 v10, 0x2

    aget v10, v3, v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngleMagnitude(FFF)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 380
    const/16 v6, 0x9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x8

    aget-object v8, v2, v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-static {v8}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngleDeviation(F)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 382
    const/16 v6, 0xa

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget v8, v3, v8

    const/4 v9, 0x1

    aget v9, v3, v9

    invoke-static {v8, v9}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerAngleXY(FF)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v6

    .line 384
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CP: coordinates_x :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x2

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", coordinates_y :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x3

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", coordinates_z :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CP: X_Angle :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x5

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Y_Angle :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x6

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", z_Angle :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x7

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CP: magnitude :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x8

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", deviation :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x9

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CP: XY_value :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0xa

    aget-object v9, v2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 371
    :cond_9
    const-wide/high16 v0, 0x4038000000000000L    # 24.0

    goto/16 :goto_2

    .line 397
    .end local v0    # "changeBit":D
    :cond_a
    sget v6, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v7, 0x2

    if-gt v6, v7, :cond_b

    .line 398
    const-string v6, "SensorRead"

    const-string v7, "getAccelermeterXYZnAngle"

    const-string v8, "null"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 406
    .end local v3    # "tempFloat":[F
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public getBarometer(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x2

    .line 600
    const/16 v4, 0x11

    new-array v1, v4, [Ljava/lang/String;

    .line 601
    .local v1, "returnData":[Ljava/lang/String;
    if-ne p1, v8, :cond_4

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v4, :cond_4

    .line 602
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v4}, Lcom/sec/factory/modules/SensorReadManager;->returnBarometer()[F

    move-result-object v2

    .line 604
    .local v2, "tempFloat":[F
    if-eqz v2, :cond_2

    .line 605
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v8, :cond_0

    .line 606
    const-string v3, "SensorRead"

    const-string v4, "getBarometer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_0
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v3, "#.##"

    invoke-direct {v0, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 610
    .local v0, "format":Ljava/text/DecimalFormat;
    const-string v3, "4"

    aput-object v3, v1, v9

    .line 611
    const-string v3, "None"

    aput-object v3, v1, v8

    .line 612
    aget v3, v2, v9

    float-to-double v4, v3

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v7

    .line 613
    const/4 v3, 0x3

    aget v4, v2, v8

    float-to-double v4, v4

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 614
    const/4 v3, 0x4

    aget v4, v2, v7

    float-to-double v4, v4

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 628
    invoke-direct {p0, v1}, Lcom/sec/factory/modules/SensorRead;->replaceData([Ljava/lang/String;)V

    .line 630
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_1

    .line 631
    const-string v3, "SensorRead"

    const-string v4, "getBarometer"

    invoke-direct {p0, v1}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    .end local v0    # "format":Ljava/text/DecimalFormat;
    .end local v1    # "returnData":[Ljava/lang/String;
    .end local v2    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v1

    .line 616
    .restart local v1    # "returnData":[Ljava/lang/String;
    .restart local v2    # "tempFloat":[F
    :cond_2
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v7, :cond_3

    .line 617
    const-string v4, "SensorRead"

    const-string v5, "getBarometer"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v1, v3

    .line 620
    goto :goto_0

    .line 622
    .end local v2    # "tempFloat":[F
    :cond_4
    if-ne p1, v7, :cond_5

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v4, :cond_5

    move-object v1, v3

    .line 623
    goto :goto_0

    :cond_5
    move-object v1, v3

    .line 625
    goto :goto_0
.end method

.method public getBarometerEEPROM(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 641
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 642
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 670
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 644
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 645
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 647
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 648
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 649
    const-string v2, "SensorRead"

    const-string v3, "getBarometerEEPROM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 653
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 654
    aget-object v2, v1, v8

    aput-object v2, v0, v6

    .line 666
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 667
    const-string v2, "SensorRead"

    const-string v3, "getBarometerEEPROM"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 656
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 657
    const-string v3, "SensorRead"

    const-string v4, "getBarometerEEPROM"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 660
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 663
    goto :goto_0
.end method

.method public getBio(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2373
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2375
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 2376
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnBio()[F

    move-result-object v1

    .line 2378
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 2379
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 2380
    const-string v2, "SensorRead"

    const-string v3, "getBio"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2383
    :cond_0
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 2384
    aget v2, v1, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 2385
    aget v2, v1, v7

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2386
    const/4 v2, 0x3

    aget v3, v1, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2401
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 2402
    const-string v2, "SensorRead"

    const-string v3, "getBio"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2405
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 2389
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_3

    .line 2390
    const-string v3, "SensorRead"

    const-string v4, "getLight"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 2393
    goto :goto_0

    .line 2395
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 2396
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 2398
    goto :goto_0
.end method

.method public getBodyTemp(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x2

    .line 2444
    const/16 v4, 0x11

    new-array v1, v4, [Ljava/lang/String;

    .line 2446
    .local v1, "returnData":[Ljava/lang/String;
    if-ne p1, v5, :cond_4

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v4, :cond_4

    .line 2447
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v4}, Lcom/sec/factory/modules/SensorReadManager;->returnBodyTemp()[F

    move-result-object v2

    .line 2449
    .local v2, "tempFloat":[F
    if-eqz v2, :cond_1

    .line 2450
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v5, :cond_0

    .line 2451
    const-string v3, "SensorRead"

    const-string v4, "getBodyTemp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2454
    :cond_0
    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 2455
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_6

    .line 2457
    add-int/lit8 v3, v0, 0x1

    aget v4, v2, v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 2455
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2461
    .end local v0    # "i":I
    :cond_1
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v7, :cond_2

    .line 2462
    const-string v4, "SensorRead"

    const-string v5, "getBodyTemp"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v1, v3

    .line 2477
    .end local v1    # "returnData":[Ljava/lang/String;
    .end local v2    # "tempFloat":[F
    :cond_3
    :goto_1
    return-object v1

    .line 2467
    .restart local v1    # "returnData":[Ljava/lang/String;
    :cond_4
    if-ne p1, v7, :cond_5

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v4, :cond_5

    move-object v1, v3

    .line 2468
    goto :goto_1

    :cond_5
    move-object v1, v3

    .line 2470
    goto :goto_1

    .line 2473
    .restart local v0    # "i":I
    .restart local v2    # "tempFloat":[F
    :cond_6
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_3

    .line 2474
    const-string v3, "SensorRead"

    const-string v4, "getBodyTemp"

    invoke-direct {p0, v1}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getGrip(I)[Ljava/lang/String;
    .locals 12
    .param p1, "target"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 677
    const/16 v5, 0x11

    new-array v0, v5, [Ljava/lang/String;

    .line 678
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v9, :cond_7

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v5, :cond_7

    .line 679
    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v5}, Lcom/sec/factory/modules/SensorReadManager;->returnGrip()[F

    move-result-object v1

    .line 681
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_5

    .line 682
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v9, :cond_0

    .line 683
    const-string v4, "SensorRead"

    const-string v5, "getGrip"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    :cond_0
    const-string v4, "3"

    aput-object v4, v0, v10

    .line 687
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v4, v1

    if-lez v4, :cond_2

    aget v4, v1, v10

    float-to-int v4, v4

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v9

    .line 688
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v4, v1

    if-le v4, v9, :cond_3

    aget v4, v1, v9

    float-to-int v4, v4

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v8

    .line 689
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v4, v1

    if-le v4, v8, :cond_4

    aget v4, v1, v8

    float-to-int v4, v4

    :goto_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v11

    .line 736
    .end local v1    # "tempFloat":[F
    :goto_3
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v8, :cond_1

    .line 737
    const-string v4, "SensorRead"

    const-string v5, "getGrip"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    :goto_4
    return-object v0

    .line 687
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    const/4 v4, -0x1

    goto :goto_0

    .line 688
    :cond_3
    const/4 v4, -0x1

    goto :goto_1

    .line 689
    :cond_4
    const/4 v4, -0x1

    goto :goto_2

    .line 691
    :cond_5
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v8, :cond_6

    .line 692
    const-string v5, "SensorRead"

    const-string v6, "getGrip"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object v0, v4

    .line 694
    goto :goto_4

    .line 696
    .end local v1    # "tempFloat":[F
    :cond_7
    if-ne p1, v8, :cond_b

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v5, :cond_b

    .line 697
    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GRIP_RAW:I

    invoke-virtual {v5, v6}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v3

    .line 699
    .local v3, "tempString":[Ljava/lang/String;
    if-eqz v3, :cond_9

    .line 700
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v9, :cond_8

    .line 701
    const-string v4, "SensorRead"

    const-string v5, "getGrip"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    :cond_8
    const-string v4, "3"

    aput-object v4, v0, v10

    .line 704
    aget-object v4, v3, v10

    aput-object v4, v0, v9

    .line 705
    const-string v4, "None"

    aput-object v4, v0, v8

    .line 706
    const-string v4, "None"

    aput-object v4, v0, v11

    goto :goto_3

    .line 708
    :cond_9
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v8, :cond_a

    .line 709
    const-string v5, "SensorRead"

    const-string v6, "getGrip"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move-object v0, v4

    .line 711
    goto :goto_4

    .line 713
    .end local v3    # "tempString":[Ljava/lang/String;
    :cond_b
    if-ne p1, v11, :cond_f

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    if-eqz v5, :cond_f

    .line 714
    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    invoke-virtual {v5}, Lcom/sec/factory/modules/SensorReadIntent;->returnGrip()[I

    move-result-object v2

    .line 716
    .local v2, "tempInt":[I
    if-eqz v2, :cond_d

    .line 717
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v9, :cond_c

    .line 718
    const-string v4, "SensorRead"

    const-string v5, "getGrip"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_c
    const-string v4, "3"

    aput-object v4, v0, v10

    .line 722
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v9

    .line 723
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v8

    .line 724
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v2, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v11

    goto/16 :goto_3

    .line 726
    :cond_d
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v5, v8, :cond_e

    .line 727
    const-string v5, "SensorRead"

    const-string v6, "getGrip"

    const-string v7, "null"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    move-object v0, v4

    .line 730
    goto/16 :goto_4

    .end local v2    # "tempInt":[I
    :cond_f
    move-object v0, v4

    .line 733
    goto/16 :goto_4
.end method

.method public getGripThreshold(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 747
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 748
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 777
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 750
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 751
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GRIP_THRESHOLD:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 753
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 754
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 755
    const-string v2, "SensorRead"

    const-string v3, "getGrip"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    :cond_2
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 758
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 759
    aget-object v2, v1, v7

    aput-object v2, v0, v6

    .line 760
    aget-object v2, v1, v6

    aput-object v2, v0, v9

    .line 773
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 774
    const-string v2, "SensorRead"

    const-string v3, "getGrip"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 762
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 763
    const-string v3, "SensorRead"

    const-string v4, "getGrip"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 765
    goto :goto_0

    .line 767
    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    if-ne p1, v9, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    if-eqz v3, :cond_6

    move-object v0, v2

    .line 768
    goto :goto_0

    :cond_6
    move-object v0, v2

    .line 770
    goto :goto_0
.end method

.method public getGyro(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    const v8, 0x42652ee0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 784
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 785
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 786
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnGyro()[F

    move-result-object v1

    .line 788
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 789
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 790
    const-string v2, "SensorRead"

    const-string v3, "getGyro"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    :cond_0
    const-string v2, "4"

    aput-object v2, v0, v9

    .line 794
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 795
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v1, v9

    mul-float/2addr v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 796
    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v7

    mul-float/2addr v4, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 797
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v6

    mul-float/2addr v4, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 811
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 812
    const-string v2, "SensorRead"

    const-string v3, "getGyro"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 799
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_3

    .line 800
    const-string v3, "SensorRead"

    const-string v4, "getGyro"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 803
    goto :goto_0

    .line 805
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 806
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 808
    goto :goto_0
.end method

.method public getGyroExpansion(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 859
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 860
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_5

    .line 864
    new-instance v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;

    invoke-direct {v1}, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;-><init>()V

    .line 866
    .local v1, "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mData:[S

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-eqz v3, :cond_1

    .line 868
    const-string v2, "10"

    aput-object v2, v0, v7

    .line 869
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mReturnValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 870
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 871
    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 872
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 873
    const/4 v2, 0x5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mData:[S

    aget-short v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 874
    const/4 v2, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mData:[S

    aget-short v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 875
    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mData:[S

    aget-short v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 876
    const/16 v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 877
    const/16 v2, 0x9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 878
    const/16 v2, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 903
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v5, :cond_0

    .line 904
    const-string v2, "SensorRead"

    const-string v3, "getGyroExpansion"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    :cond_0
    :goto_0
    return-object v0

    .line 881
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    :cond_1
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v5, :cond_4

    .line 882
    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-nez v3, :cond_2

    .line 883
    const-string v3, "SensorRead"

    const-string v4, "getGyroExpansion"

    const-string v5, "Noise Bias null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    :cond_2
    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mData:[S

    if-nez v3, :cond_3

    .line 887
    const-string v3, "SensorRead"

    const-string v4, "getGyroExpansion"

    const-string v5, "Data null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    :cond_3
    iget-object v3, v1, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-nez v3, :cond_4

    .line 891
    const-string v3, "SensorRead"

    const-string v4, "getGyroExpansion"

    const-string v5, "RMS null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 895
    goto :goto_0

    .line 897
    .end local v1    # "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    :cond_5
    if-ne p1, v5, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_6

    move-object v0, v2

    .line 898
    goto :goto_0

    :cond_6
    move-object v0, v2

    .line 900
    goto :goto_0
.end method

.method public getGyroPower(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 822
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 823
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 851
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 825
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 826
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 828
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 829
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 830
    const-string v2, "SensorRead"

    const-string v3, "getGyroPower"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 834
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 835
    aget-object v2, v1, v8

    aput-object v2, v0, v6

    .line 847
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 848
    const-string v2, "SensorRead"

    const-string v3, "getGyroPower"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 837
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 838
    const-string v3, "SensorRead"

    const-string v4, "getGyroPower"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 841
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 844
    goto :goto_0
.end method

.method public getGyroSelf(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    .line 951
    const/16 v7, 0x11

    new-array v2, v7, [Ljava/lang/String;

    .line 952
    .local v2, "returnData":[Ljava/lang/String;
    const/4 v7, 0x1

    if-ne p1, v7, :cond_6

    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v7, :cond_6

    .line 956
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;-><init>()V

    .line 958
    .local v3, "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    iget-object v7, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-eqz v7, :cond_3

    iget-object v7, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-eqz v7, :cond_3

    .line 959
    const/4 v7, 0x0

    const-string v8, "7"

    aput-object v8, v2, v7

    .line 960
    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mReturnValue:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 961
    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    const/4 v10, 0x0

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 962
    const/4 v7, 0x3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 963
    const/4 v7, 0x4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    const/4 v10, 0x2

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 964
    const/4 v7, 0x5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    const/4 v10, 0x0

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 965
    const/4 v7, 0x6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 966
    const/4 v7, 0x7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    const/4 v10, 0x2

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 968
    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v8, "STMICRO_SMARTPHONE"

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v8, "STMICRO_TABLET"

    if-ne v7, v8, :cond_1

    .line 970
    :cond_0
    const/4 v7, 0x0

    const-string v8, "8"

    aput-object v8, v2, v7

    .line 971
    const/16 v7, 0x8

    const-string v8, ""

    aput-object v8, v2, v7

    .line 1046
    .end local v3    # "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    :cond_1
    :goto_0
    sget v7, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v8, 0x2

    if-gt v7, v8, :cond_2

    .line 1047
    const-string v7, "SensorRead"

    const-string v8, "getGyroSelf"

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    .end local v2    # "returnData":[Ljava/lang/String;
    :cond_2
    :goto_1
    return-object v2

    .line 976
    .restart local v2    # "returnData":[Ljava/lang/String;
    .restart local v3    # "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    :cond_3
    sget v7, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v8, 0x2

    if-gt v7, v8, :cond_5

    .line 977
    iget-object v7, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-nez v7, :cond_4

    .line 978
    const-string v7, "SensorRead"

    const-string v8, "getGyroSelf"

    const-string v9, "Noise Bias null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    :cond_4
    iget-object v7, v3, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-nez v7, :cond_5

    .line 982
    const-string v7, "SensorRead"

    const-string v8, "getGyroSelf"

    const-string v9, "RMS null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 988
    .end local v3    # "returnValue":Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    :cond_6
    const/4 v7, 0x2

    if-ne p1, v7, :cond_d

    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v7, :cond_d

    .line 989
    const-string v1, ""

    .line 990
    .local v1, "resultValue":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v8, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    invoke-virtual {v7, v8}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v5

    .line 992
    .local v5, "tempString":[Ljava/lang/String;
    if-eqz v5, :cond_b

    .line 993
    sget v7, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v8, 0x1

    if-gt v7, v8, :cond_7

    .line 994
    const-string v7, "SensorRead"

    const-string v8, "getGyroSelf"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    :cond_7
    const/4 v7, 0x6

    aget-object v7, v5, v7

    const-string v8, "1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    const/4 v7, 0x7

    aget-object v7, v5, v7

    const-string v8, "1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 998
    const-string v1, "0"

    .line 1003
    :goto_2
    const/4 v7, 0x0

    const-string v8, "7"

    aput-object v8, v2, v7

    .line 1004
    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1005
    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1006
    const/4 v7, 0x3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1007
    const/4 v7, 0x4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x2

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1008
    const/4 v7, 0x5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x3

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1009
    const/4 v7, 0x6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1010
    const/4 v7, 0x7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x5

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1012
    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v8, "STMICRO_SMARTPHONE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1013
    const/4 v7, 0x0

    const-string v8, "8"

    aput-object v8, v2, v7

    .line 1014
    const/16 v7, 0x8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x6

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    goto/16 :goto_0

    .line 1000
    :cond_8
    const-string v1, "-1"

    goto/16 :goto_2

    .line 1015
    :cond_9
    iget-object v7, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v8, "STMICRO_TABLET"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1016
    const-string v7, "SensorRead"

    const-string v8, "getGyroSelfTest"

    const-string v9, "FEATURE TABLET(CAL)"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    const/4 v7, 0x0

    const-string v8, "8"

    aput-object v8, v2, v7

    .line 1018
    const-string v6, ""

    .line 1021
    .local v6, "zerocalResult":Ljava/lang/String;
    const/16 v7, 0xd

    aget-object v7, v5, v7

    const-string v8, "1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    const/16 v7, 0xe

    aget-object v7, v5, v7

    const-string v8, "1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1022
    const-string v6, "0"

    .line 1027
    :goto_3
    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1028
    const/16 v7, 0x8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0xe

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    .line 1029
    const-string v4, ""

    .line 1031
    .local v4, "rst":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_4
    array-length v7, v2

    if-ge v0, v7, :cond_1

    .line 1032
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v2, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1031
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1024
    .end local v0    # "i":I
    .end local v4    # "rst":Ljava/lang/String;
    :cond_a
    const-string v6, "-1"

    goto :goto_3

    .line 1036
    .end local v6    # "zerocalResult":Ljava/lang/String;
    :cond_b
    sget v7, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v8, 0x2

    if-gt v7, v8, :cond_c

    .line 1037
    const-string v7, "SensorRead"

    const-string v8, "getGyroSelfTest"

    const-string v9, "null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1043
    .end local v1    # "resultValue":Ljava/lang/String;
    .end local v5    # "tempString":[Ljava/lang/String;
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public getGyroTemperature(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 914
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 915
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 943
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 917
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 918
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 920
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 921
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 922
    const-string v2, "SensorRead"

    const-string v3, "getGyroTemperature"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 926
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 927
    aget-object v2, v1, v8

    aput-object v2, v0, v6

    .line 939
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 940
    const-string v2, "SensorRead"

    const-string v3, "getGyroTemperature"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 929
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 930
    const-string v3, "SensorRead"

    const-string v4, "getGyroTemperature"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 933
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 936
    goto :goto_0
.end method

.method public getHrm(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2409
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2411
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 2412
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnHrm()[F

    move-result-object v1

    .line 2414
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 2415
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 2416
    const-string v2, "SensorRead"

    const-string v3, "getHrm"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2419
    :cond_0
    const-string v2, "3"

    aput-object v2, v0, v8

    .line 2420
    aget v2, v1, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 2421
    aget v2, v1, v7

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2422
    const/4 v2, 0x3

    aget v3, v1, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2436
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 2437
    const-string v2, "SensorRead"

    const-string v3, "getHrm"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2440
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 2424
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_3

    .line 2425
    const-string v3, "SensorRead"

    const-string v4, "getLight"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 2428
    goto :goto_0

    .line 2430
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 2431
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 2433
    goto :goto_0
.end method

.method public getHumidity(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 2339
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2340
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 2341
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnHumidity()[F

    move-result-object v1

    .line 2343
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 2344
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 2345
    const-string v2, "SensorRead"

    const-string v3, "getHumidity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2348
    :cond_0
    const-string v2, "3"

    aput-object v2, v0, v8

    .line 2349
    const-string v2, "%.2f"

    new-array v3, v6, [Ljava/lang/Object;

    aget v4, v1, v8

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2350
    const-string v2, "%.2f"

    new-array v3, v6, [Ljava/lang/Object;

    aget v4, v1, v6

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 2351
    const/4 v2, 0x3

    aget v3, v1, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2365
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_1

    .line 2366
    const-string v2, "SensorRead"

    const-string v3, "getHumidity"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2369
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 2353
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_3

    .line 2354
    const-string v3, "SensorRead"

    const-string v4, "getHumidity"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 2357
    goto :goto_0

    .line 2359
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v7, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 2360
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 2362
    goto :goto_0
.end method

.method public getLight(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1057
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1059
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 1060
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnLight()[F

    move-result-object v1

    .line 1062
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 1063
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 1064
    const-string v2, "SensorRead"

    const-string v3, "getLight"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    :cond_0
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1068
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 1069
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 1083
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 1084
    const-string v2, "SensorRead"

    const-string v3, "getLight"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 1071
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_3

    .line 1072
    const-string v3, "SensorRead"

    const-string v4, "getLight"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 1075
    goto :goto_0

    .line 1077
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 1078
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 1080
    goto :goto_0
.end method

.method public getLightADC(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1130
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1131
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 1159
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 1133
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 1134
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1136
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 1137
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 1138
    const-string v2, "SensorRead"

    const-string v3, "getLightADC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1142
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 1143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 1155
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 1156
    const-string v2, "SensorRead"

    const-string v3, "getLightADC"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1145
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 1146
    const-string v3, "SensorRead"

    const-string v4, "getLightADC"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 1149
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 1152
    goto :goto_0
.end method

.method public getLightCCT(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1094
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1095
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 1096
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnLight()[F

    move-result-object v1

    .line 1098
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 1099
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 1100
    const-string v2, "SensorRead"

    const-string v3, "getLightCCT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    :cond_0
    const/4 v2, 0x0

    const-string v3, "2"

    aput-object v3, v0, v2

    .line 1104
    const-string v2, "None"

    aput-object v2, v0, v6

    .line 1105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 1119
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_1

    .line 1120
    const-string v2, "SensorRead"

    const-string v3, "getLightCCT"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 1107
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_3

    .line 1108
    const-string v3, "SensorRead"

    const-string v4, "getLightCCT"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 1111
    goto :goto_0

    .line 1113
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v7, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 1114
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 1116
    goto :goto_0
.end method

.method public getLightRGBW(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1166
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1167
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 1198
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 1169
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 1170
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_RGBW:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1172
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 1173
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 1174
    const-string v2, "SensorRead"

    const-string v3, "getLightRGBW"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    :cond_2
    const-string v2, "5"

    aput-object v2, v0, v8

    .line 1178
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 1179
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 1180
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    .line 1181
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1182
    const/4 v2, 0x5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1194
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 1195
    const-string v2, "SensorRead"

    const-string v3, "getLightRGBW"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1184
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 1185
    const-string v3, "SensorRead"

    const-string v4, "getLightRGBW"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 1188
    goto/16 :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 1191
    goto/16 :goto_0
.end method

.method public getMagnetic(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1205
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1206
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 1207
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnMagnetic()[F

    move-result-object v1

    .line 1210
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 1211
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 1212
    const-string v2, "SensorRead"

    const-string v3, "getMagnetic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    :cond_0
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1216
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 1217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 1218
    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1219
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 1233
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 1234
    const-string v2, "SensorRead"

    const-string v3, "getMagnetic"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1237
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 1221
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_3

    .line 1222
    const-string v3, "SensorRead"

    const-string v4, "getMagnetic"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 1225
    goto :goto_0

    .line 1227
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 1228
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 1230
    goto :goto_0
.end method

.method public getMagneticADC(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1823
    const/16 v2, 0x11

    new-array v0, v2, [Ljava/lang/String;

    .line 1824
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_d

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v2, :cond_d

    .line 1825
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963C_MANAGER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8973"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8975"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK09911"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK09911C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1836
    :cond_0
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1865
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 1866
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 1867
    const-string v2, "SensorRead"

    const-string v3, "getMagneticADC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    :cond_2
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1871
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v7

    .line 1872
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v6

    .line 1873
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v9

    .line 1874
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v2, v2, v9

    aput-object v2, v0, v10

    .line 1906
    :goto_1
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_3

    .line 1907
    const-string v2, "SensorRead"

    const-string v3, "getMagneticADC"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v0

    .line 1837
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS529"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1846
    :cond_5
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1847
    :cond_6
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD004"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD004A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD006A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD008A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1851
    :cond_7
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1853
    :cond_8
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_POWER_NOISE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1859
    :cond_9
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1860
    :cond_a
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "STMICRO_K303C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1861
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_STMicro(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1876
    :cond_b
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_c

    .line 1877
    const-string v2, "SensorRead"

    const-string v3, "getMagneticADC"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1882
    :cond_d
    if-ne p1, v6, :cond_11

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v2, :cond_11

    .line 1883
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1885
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_f

    array-length v2, v1

    if-ne v2, v10, :cond_f

    .line 1886
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_e

    .line 1887
    const-string v2, "SensorRead"

    const-string v3, "getMagneticADC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1890
    :cond_e
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1891
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 1892
    aget-object v2, v1, v7

    aput-object v2, v0, v6

    .line 1893
    aget-object v2, v1, v6

    aput-object v2, v0, v9

    .line 1894
    aget-object v2, v1, v9

    aput-object v2, v0, v10

    goto/16 :goto_1

    .line 1896
    :cond_f
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_10

    .line 1897
    const-string v2, "SensorRead"

    const-string v3, "getMagneticADC"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1903
    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public getMagneticDAC(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1700
    const/16 v2, 0x11

    new-array v0, v2, [Ljava/lang/String;

    .line 1701
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_d

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v2, :cond_d

    .line 1702
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963C_MANAGER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8973"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8975"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK09911"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK09911C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1713
    :cond_0
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1742
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 1743
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 1744
    const-string v2, "SensorRead"

    const-string v3, "getMagneticDAC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1747
    :cond_2
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1748
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v7

    .line 1749
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v6

    .line 1750
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v9

    .line 1751
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v2, v2, v9

    aput-object v2, v0, v10

    .line 1812
    :goto_1
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_3

    .line 1813
    const-string v2, "SensorRead"

    const-string v3, "getMagneticDAC"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1816
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v0

    .line 1714
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS529"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1723
    :cond_5
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1724
    :cond_6
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD004"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD004A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD006A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD008A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1728
    :cond_7
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v2, v10}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1730
    :cond_8
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_POWER_NOISE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1736
    :cond_9
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1737
    :cond_a
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "STMICRO_K303C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1738
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v2, v10}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_STMicro(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1753
    :cond_b
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_c

    .line 1754
    const-string v2, "SensorRead"

    const-string v3, "getMagneticDAC"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1759
    :cond_d
    if-ne p1, v6, :cond_16

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v2, :cond_16

    .line 1760
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1762
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_14

    .line 1763
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_e

    .line 1764
    const-string v2, "SensorRead"

    const-string v3, "getMagneticDAC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    :cond_e
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8963C_MANAGER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK8975"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK09911"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "AK09911C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1772
    :cond_f
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1773
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 1774
    aget-object v2, v1, v8

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1775
    const-string v2, "1"

    aput-object v2, v0, v6

    .line 1779
    :goto_3
    const-string v2, "0"

    aput-object v2, v0, v9

    .line 1780
    const-string v2, "0"

    aput-object v2, v0, v10

    goto/16 :goto_1

    .line 1777
    :cond_10
    const-string v2, "0"

    aput-object v2, v0, v6

    goto :goto_3

    .line 1782
    :cond_11
    array-length v2, v1

    if-le v2, v9, :cond_13

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS529"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1789
    :cond_12
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1790
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 1791
    aget-object v2, v1, v7

    aput-object v2, v0, v6

    .line 1792
    aget-object v2, v1, v6

    aput-object v2, v0, v9

    .line 1793
    aget-object v2, v1, v9

    aput-object v2, v0, v10

    goto/16 :goto_1

    .line 1795
    :cond_13
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 1796
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 1797
    aget-object v2, v1, v7

    aput-object v2, v0, v6

    .line 1798
    aget-object v2, v1, v6

    aput-object v2, v0, v9

    .line 1799
    aget-object v2, v1, v9

    aput-object v2, v0, v10

    goto/16 :goto_1

    .line 1802
    :cond_14
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_15

    .line 1803
    const-string v2, "SensorRead"

    const-string v3, "getMagneticDAC"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1806
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1809
    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public getMagneticOffsetH(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 2085
    const/16 v2, 0x11

    new-array v0, v2, [Ljava/lang/String;

    .line 2086
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_7

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v2, :cond_7

    .line 2087
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2094
    :cond_0
    new-instance v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v2}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 2097
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 2098
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_2

    .line 2099
    const-string v2, "SensorRead"

    const-string v3, "getMagneticOffsetH"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2102
    :cond_2
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 2103
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v6

    .line 2104
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v7

    .line 2105
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v9

    .line 2106
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v2, v2, v9

    aput-object v2, v0, v10

    .line 2150
    :cond_3
    :goto_0
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_4

    .line 2151
    const-string v2, "SensorRead"

    const-string v3, "getMagneticOffsetH"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2154
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    :goto_1
    return-object v0

    .line 2108
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_5
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_6

    .line 2109
    const-string v2, "SensorRead"

    const-string v3, "getMagneticOffsetH"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2112
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 2114
    :cond_7
    if-ne p1, v7, :cond_3

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v2, :cond_3

    .line 2115
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_OFFSETH:I

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 2117
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_c

    .line 2118
    array-length v2, v1

    const/16 v3, 0xb

    if-le v2, v3, :cond_b

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS529"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2125
    :cond_8
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_9

    .line 2126
    const-string v2, "SensorRead"

    const-string v3, "getMagneticSelf"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2129
    :cond_9
    const-string v2, "0"

    const/16 v3, 0xc

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2130
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 2131
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 2132
    const/16 v2, 0xd

    aget-object v2, v1, v2

    aput-object v2, v0, v7

    .line 2133
    const/16 v2, 0xe

    aget-object v2, v1, v2

    aput-object v2, v0, v9

    .line 2134
    const/16 v2, 0xf

    aget-object v2, v1, v2

    aput-object v2, v0, v10

    goto/16 :goto_0

    .line 2136
    :cond_a
    const-string v2, "4"

    aput-object v2, v0, v8

    .line 2137
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 2138
    const/16 v2, 0xd

    aget-object v2, v1, v2

    aput-object v2, v0, v7

    .line 2139
    const/16 v2, 0xe

    aget-object v2, v1, v2

    aput-object v2, v0, v9

    .line 2140
    const/16 v2, 0xf

    aget-object v2, v1, v2

    aput-object v2, v0, v10

    goto/16 :goto_0

    .line 2143
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2146
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public getMagneticPowerOff(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 1366
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1367
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v8, :cond_b

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_b

    .line 1368
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C_MANAGER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8973"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8975"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1379
    :cond_0
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1401
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 1402
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v8, :cond_2

    .line 1403
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOff"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v6

    .line 1407
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v8

    .line 1408
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v7

    .line 1452
    :goto_1
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_3

    .line 1453
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOff"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v0

    .line 1380
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1389
    :cond_5
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1390
    :cond_6
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1394
    :cond_7
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3, v7}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1396
    :cond_8
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "STMICRO_K303C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1397
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3, v7}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_STMicro(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1410
    :cond_9
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_a

    .line 1411
    const-string v3, "SensorRead"

    const-string v4, "getMagneticPowerOff"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move-object v0, v2

    .line 1414
    goto/16 :goto_2

    .line 1416
    :cond_b
    if-ne p1, v7, :cond_11

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_11

    .line 1417
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1419
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_f

    .line 1420
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v8, :cond_c

    .line 1421
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOff"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    :cond_c
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_COMBINATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1424
    const-string v2, "GEOMAGNETIC_SENSOR_POWER"

    const-string v3, "2"

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1425
    const-string v2, "GEOMAGNETIC_SENSOR_POWER"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 1426
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOFF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GEOMAGNETIC_SENSOR_POWER : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    const-string v2, "2"

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1428
    const-string v2, "2"

    aput-object v2, v0, v6

    .line 1429
    const-string v2, "None"

    aput-object v2, v0, v8

    .line 1430
    aget-object v2, v1, v6

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1432
    :cond_d
    const-string v2, "0"

    aput-object v2, v0, v6

    .line 1433
    const-string v2, "None"

    aput-object v2, v0, v8

    .line 1434
    aget-object v2, v1, v6

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1437
    :cond_e
    const-string v2, "2"

    aput-object v2, v0, v6

    .line 1438
    const-string v2, "None"

    aput-object v2, v0, v8

    .line 1439
    aget-object v2, v1, v6

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1442
    :cond_f
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_10

    .line 1443
    const-string v3, "SensorRead"

    const-string v4, "getMagneticPowerOff"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    move-object v0, v2

    .line 1446
    goto/16 :goto_2

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_11
    move-object v0, v2

    .line 1449
    goto/16 :goto_2
.end method

.method public getMagneticPowerOn(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1244
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1245
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_d

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_d

    .line 1246
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C_MANAGER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8973"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8975"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1257
    :cond_0
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1286
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    if-eqz v3, :cond_b

    .line 1287
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_2

    .line 1288
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOn"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1292
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v6

    .line 1293
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v7

    .line 1358
    :goto_1
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_3

    .line 1359
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOn"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v0

    .line 1258
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1267
    :cond_5
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1268
    :cond_6
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1272
    :cond_7
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3, v6}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1274
    :cond_8
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150_POWER_NOISE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1280
    :cond_9
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1281
    :cond_a
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "STMICRO_K303C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1282
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3, v6}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_STMicro(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1295
    :cond_b
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_c

    .line 1296
    const-string v3, "SensorRead"

    const-string v4, "getMagneticPowerOn"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object v0, v2

    .line 1299
    goto/16 :goto_2

    .line 1301
    :cond_d
    if-ne p1, v7, :cond_1a

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_1a

    .line 1302
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1304
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_18

    .line 1305
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_e

    .line 1306
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOn"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1309
    :cond_e
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1311
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS529"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1316
    :cond_f
    array-length v2, v1

    if-lez v2, :cond_10

    const-string v2, "0"

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1317
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1318
    const-string v2, "1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1320
    :cond_10
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 1321
    const-string v2, "-1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1323
    :cond_11
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_POWER_NOISE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1325
    :cond_12
    const-string v2, "1"

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1326
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1327
    const-string v2, "1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1329
    :cond_13
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 1330
    const-string v2, "-1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1332
    :cond_14
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_COMBINATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1333
    const-string v2, "GEOMAGNETIC_SENSOR_POWER"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1334
    const-string v2, "GEOMAGNETIC_SENSOR_POWER"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 1335
    const-string v2, "SensorRead"

    const-string v3, "getMagneticPowerOn"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GEOMAGNETIC_SENSOR_POWER : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v1, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    const-string v2, "1"

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "2"

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1337
    :cond_15
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1338
    const-string v2, "1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1340
    :cond_16
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 1341
    const-string v2, "-1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1344
    :cond_17
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 1345
    const-string v2, "-1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1348
    :cond_18
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_19

    .line 1349
    const-string v3, "SensorRead"

    const-string v4, "getMagneticPowerOn"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    move-object v0, v2

    .line 1352
    goto/16 :goto_2

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_1a
    move-object v0, v2

    .line 1355
    goto/16 :goto_2
.end method

.method public getMagneticSelf(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1918
    const/16 v4, 0x11

    new-array v1, v4, [Ljava/lang/String;

    .line 1919
    .local v1, "returnData":[Ljava/lang/String;
    if-ne p1, v8, :cond_10

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v4, :cond_10

    .line 1920
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK8963"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK8963C"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK8963C_MANAGER"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK8973"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK8975"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK09911"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "AK09911C"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1931
    :cond_0
    new-instance v4, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v4}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1960
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    if-eqz v4, :cond_e

    .line 1961
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v8, :cond_2

    .line 1962
    const-string v3, "SensorRead"

    const-string v4, "getMagneticSelf"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v6, v6, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    :cond_2
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150_POWER_NOISE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1967
    :cond_3
    const-string v3, "2"

    aput-object v3, v1, v7

    .line 1968
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v3, v3, v7

    aput-object v3, v1, v8

    .line 1969
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v3, v3, v8

    aput-object v3, v1, v9

    .line 2073
    :cond_4
    :goto_1
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v9, :cond_5

    .line 2074
    const-string v3, "SensorRead"

    const-string v4, "getMagneticSelf"

    invoke-direct {p0, v1}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2077
    .end local v1    # "returnData":[Ljava/lang/String;
    :cond_5
    :goto_2
    return-object v1

    .line 1932
    .restart local v1    # "returnData":[Ljava/lang/String;
    :cond_6
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "YAS529"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "YAS530"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "YAS530C"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "YAS532B"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "YAS532"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1941
    :cond_7
    new-instance v4, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v4}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1942
    :cond_8
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BMC150"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BMC150_POWER_NOISE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "BMC150_COMBINATION"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1949
    :cond_9
    new-instance v4, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v4}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1950
    :cond_a
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "HSCDTD004"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "HSCDTD004A"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "HSCDTD006A"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "HSCDTD008A"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1954
    :cond_b
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1956
    :cond_c
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v5, "STMICRO_K303C"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1957
    new-instance v4, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v4}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1971
    :cond_d
    const-string v3, "4"

    aput-object v3, v1, v7

    .line 1972
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v3, v3, v7

    aput-object v3, v1, v8

    .line 1973
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v3, v3, v8

    aput-object v3, v1, v9

    .line 1974
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v3, v3, v9

    aput-object v3, v1, v10

    .line 1975
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v4, v4, v10

    aput-object v4, v1, v3

    goto/16 :goto_1

    .line 1978
    :cond_e
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v9, :cond_f

    .line 1979
    const-string v4, "SensorRead"

    const-string v5, "getMagneticSelf"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    move-object v1, v3

    .line 1982
    goto/16 :goto_2

    .line 1984
    :cond_10
    if-ne p1, v9, :cond_22

    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v4, :cond_22

    .line 1985
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    invoke-virtual {v4, v5}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v2

    .line 1987
    .local v2, "tempString":[Ljava/lang/String;
    if-eqz v2, :cond_20

    .line 1988
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v8, :cond_11

    .line 1989
    const-string v3, "SensorRead"

    const-string v4, "getMagneticSelf"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1992
    :cond_11
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1996
    :cond_12
    const-string v3, "2"

    aput-object v3, v1, v7

    .line 1997
    const-string v3, "None"

    aput-object v3, v1, v8

    .line 1998
    aget-object v3, v2, v7

    aput-object v3, v1, v9

    goto/16 :goto_1

    .line 1999
    :cond_13
    array-length v3, v2

    const/16 v4, 0x9

    if-le v3, v4, :cond_15

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2006
    :cond_14
    const-string v3, "16"

    aput-object v3, v1, v7

    .line 2007
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    const/16 v3, 0x10

    if-ge v0, v3, :cond_4

    .line 2008
    add-int/lit8 v3, v0, 0x1

    aget-object v4, v2, v0

    aput-object v4, v1, v3

    .line 2007
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2012
    .end local v0    # "i":I
    :cond_15
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150_POWER_NOISE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 2014
    :cond_16
    const-string v3, "2"

    aput-object v3, v1, v7

    .line 2015
    aget-object v3, v2, v7

    aput-object v3, v1, v8

    .line 2016
    aget-object v3, v2, v8

    aput-object v3, v1, v9

    goto/16 :goto_1

    .line 2017
    :cond_17
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150_COMBINATION"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 2018
    const-string v3, "GEOMAGNETIC_SENSOR_SELFTEST"

    const-string v4, "2"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2019
    const-wide/16 v4, 0x3e8

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 2020
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v2

    .line 2021
    const-string v3, "2"

    aput-object v3, v1, v7

    .line 2022
    aget-object v3, v2, v7

    aput-object v3, v1, v8

    .line 2023
    aget-object v3, v2, v8

    aput-object v3, v1, v9

    .line 2024
    const-string v3, "GEOMAGNETIC_SENSOR_POWER"

    const-string v4, "3"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2025
    const-wide/16 v4, 0x1e

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 2026
    const-string v3, "GEOMAGNETIC_SENSOR_POWER"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 2027
    :cond_18
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 2029
    :cond_19
    array-length v3, v2

    const/16 v4, 0x9

    if-le v3, v4, :cond_1a

    .line 2030
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    const/16 v3, 0xa

    if-ge v0, v3, :cond_4

    .line 2031
    aget-object v3, v2, v0

    aput-object v3, v1, v0

    .line 2030
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2034
    .end local v0    # "i":I
    :cond_1a
    const-string v3, "-1"

    aput-object v3, v1, v7

    .line 2035
    const-string v3, "SensorRead"

    const-string v4, "getMagneticSelf"

    const-string v5, "Sysfs data is too short, wrong data"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2037
    :cond_1b
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150_NEWEST"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 2038
    array-length v3, v2

    const/4 v4, 0x6

    if-ne v3, v4, :cond_1c

    .line 2039
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    array-length v3, v2

    if-ge v0, v3, :cond_4

    .line 2040
    aget-object v3, v2, v0

    aput-object v3, v1, v0

    .line 2039
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2043
    .end local v0    # "i":I
    :cond_1c
    const-string v3, "-1"

    aput-object v3, v1, v7

    .line 2044
    const-string v3, "SensorRead"

    const-string v4, "getMagneticSelf"

    const-string v5, "Sysfs data is too short, wrong data"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2046
    :cond_1d
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "STMICRO_K303C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 2047
    const-string v3, "OK"

    aget-object v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 2048
    const-string v3, "1"

    aput-object v3, v1, v7

    .line 2052
    :goto_6
    aget-object v3, v2, v8

    aput-object v3, v1, v8

    .line 2053
    aget-object v3, v2, v9

    aput-object v3, v1, v9

    .line 2054
    aget-object v3, v2, v10

    aput-object v3, v1, v10

    goto/16 :goto_1

    .line 2050
    :cond_1e
    const-string v3, "2"

    aput-object v3, v1, v7

    goto :goto_6

    .line 2056
    :cond_1f
    const-string v3, "4"

    aput-object v3, v1, v7

    .line 2057
    aget-object v3, v2, v7

    aput-object v3, v1, v8

    .line 2058
    aget-object v3, v2, v8

    aput-object v3, v1, v9

    .line 2059
    aget-object v3, v2, v9

    aput-object v3, v1, v10

    .line 2060
    const/4 v3, 0x4

    aget-object v4, v2, v10

    aput-object v4, v1, v3

    goto/16 :goto_1

    .line 2063
    :cond_20
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v4, v9, :cond_21

    .line 2064
    const-string v4, "SensorRead"

    const-string v5, "getMagneticSelf"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_21
    move-object v1, v3

    .line 2067
    goto/16 :goto_2

    .end local v2    # "tempString":[Ljava/lang/String;
    :cond_22
    move-object v1, v3

    .line 2070
    goto/16 :goto_2
.end method

.method public getMagneticStatus(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1463
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1464
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_d

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_d

    .line 1465
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C_MANAGER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8973"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8975"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1476
    :cond_0
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1502
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    if-eqz v3, :cond_b

    .line 1503
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_2

    .line 1504
    const-string v2, "SensorRead"

    const-string v3, "getMagneticStatus"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1508
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v6

    .line 1509
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v7

    .line 1594
    :cond_3
    :goto_1
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_4

    .line 1595
    const-string v2, "SensorRead"

    const-string v3, "getMagneticStatus"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1598
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    :goto_2
    return-object v0

    .line 1477
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1486
    :cond_6
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1487
    :cond_7
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1491
    :cond_8
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadManager;->returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1493
    :cond_9
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BMC150_POWER_NOISE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1499
    :cond_a
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1511
    :cond_b
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_c

    .line 1512
    const-string v3, "SensorRead"

    const-string v4, "getMagneticStatus"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object v0, v2

    .line 1515
    goto/16 :goto_2

    .line 1517
    :cond_d
    if-ne p1, v7, :cond_18

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_18

    .line 1518
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1519
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 1520
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_e

    .line 1521
    const-string v2, "SensorRead"

    const-string v3, "getMagneticStatus"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    :cond_e
    array-length v2, v1

    if-le v2, v7, :cond_11

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS529"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS530C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "YAS532B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1531
    :cond_f
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1533
    const-string v2, "0"

    aget-object v3, v1, v7

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1534
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1535
    const-string v2, "1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1537
    :cond_10
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 1538
    const-string v2, "0"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1541
    :cond_11
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD004"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD004A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD006A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "HSCDTD008A"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_POWER_NOISE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "BMC150_COMBINATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1554
    :cond_12
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1556
    aget-object v2, v1, v8

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1557
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1562
    :goto_3
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1559
    :cond_13
    const-string v2, "NG"

    aput-object v2, v0, v6

    goto :goto_3

    .line 1563
    :cond_14
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v3, "STMICRO_K303C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1564
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1566
    aget-object v2, v1, v8

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1567
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1572
    :goto_4
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1569
    :cond_15
    const-string v2, "NG"

    aput-object v2, v0, v6

    goto :goto_4

    .line 1574
    :cond_16
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1576
    aget-object v2, v1, v8

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1577
    const-string v2, "OK"

    aput-object v2, v0, v6

    .line 1578
    const-string v2, "1"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1580
    :cond_17
    const-string v2, "NG"

    aput-object v2, v0, v6

    .line 1581
    const-string v2, "0"

    aput-object v2, v0, v7

    goto/16 :goto_1

    .line 1587
    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_18
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_19

    .line 1588
    const-string v3, "SensorRead"

    const-string v4, "getMagneticStatus"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    move-object v0, v2

    .line 1591
    goto/16 :goto_2
.end method

.method public getMagneticTemperature(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 1605
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 1607
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_9

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_9

    .line 1608
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1615
    :cond_0
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    .line 1629
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 1630
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 1631
    const-string v2, "SensorRead"

    const-string v3, "getMagneticTemperature"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1634
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1635
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v7

    .line 1636
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v6

    .line 1689
    :goto_1
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_3

    .line 1690
    const-string v2, "SensorRead"

    const-string v3, "getMagneticTemperature"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1693
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-object v0

    .line 1616
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1624
    :cond_5
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto :goto_0

    .line 1625
    :cond_6
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "STMICRO_K303C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1626
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mTemp_Magnetic:Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1638
    :cond_7
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_8

    .line 1639
    const-string v3, "SensorRead"

    const-string v4, "getMagneticTemperature"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    move-object v0, v2

    .line 1642
    goto :goto_2

    .line 1646
    :cond_9
    if-ne p1, v6, :cond_f

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_f

    .line 1647
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8973"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1649
    :cond_a
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 1652
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_c

    .line 1653
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_b

    .line 1654
    const-string v2, "SensorRead"

    const-string v3, "getMagneticTemperature"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    :cond_b
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1658
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 1659
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    goto/16 :goto_1

    .line 1661
    :cond_c
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_d

    .line 1662
    const-string v3, "SensorRead"

    const-string v4, "getMagneticTemperature"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object v0, v2

    .line 1665
    goto/16 :goto_2

    .line 1673
    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_e
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 1674
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 1675
    const-string v2, "0"

    aput-object v2, v0, v6

    .line 1677
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_3

    .line 1678
    const-string v2, "SensorRead"

    const-string v3, "getMagneticTemperature"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_f
    move-object v0, v2

    .line 1686
    goto/16 :goto_2
.end method

.method public getProximity(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2161
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2162
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 2163
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnProximity()[F

    move-result-object v1

    .line 2166
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 2167
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 2168
    const-string v2, "SensorRead"

    const-string v3, "getProximity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2171
    :cond_0
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 2172
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 2173
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v1, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2187
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_1

    .line 2188
    const-string v2, "SensorRead"

    const-string v3, "getProximity"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2191
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 2175
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_3

    .line 2176
    const-string v3, "SensorRead"

    const-string v4, "getProximity"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 2179
    goto :goto_0

    .line 2181
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 2182
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 2184
    goto :goto_0
.end method

.method public getProximityADC(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2198
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2199
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 2227
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 2201
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 2202
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 2204
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 2205
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 2206
    const-string v2, "SensorRead"

    const-string v3, "getProximityADC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2209
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 2210
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 2211
    aget-object v2, v1, v8

    aput-object v2, v0, v6

    .line 2223
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 2224
    const-string v2, "SensorRead"

    const-string v3, "getProximityADC"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2213
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 2214
    const-string v3, "SensorRead"

    const-string v4, "getProximityADC"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 2217
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 2220
    goto :goto_0
.end method

.method public getProximityAVG(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2234
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2235
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 2263
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 2237
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v6, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 2238
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 2240
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 2241
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_2

    .line 2242
    const-string v2, "SensorRead"

    const-string v3, "getProximityAVG"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2245
    :cond_2
    const-string v2, "2"

    aput-object v2, v0, v8

    .line 2246
    const-string v2, "None"

    aput-object v2, v0, v7

    .line 2247
    aget-object v2, v1, v8

    aput-object v2, v0, v6

    .line 2259
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 2260
    const-string v2, "SensorRead"

    const-string v3, "getProximityAVG"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2249
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v6, :cond_4

    .line 2250
    const-string v3, "SensorRead"

    const-string v4, "getProximityAVG"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 2253
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 2256
    goto :goto_0
.end method

.method public getProximityOffset(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 2270
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2271
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 2300
    .end local v0    # "returnData":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 2273
    .restart local v0    # "returnData":[Ljava/lang/String;
    :cond_1
    if-ne p1, v7, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    .line 2274
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    .line 2276
    .local v1, "tempString":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 2277
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_2

    .line 2278
    const-string v2, "SensorRead"

    const-string v3, "getProximityOffset"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2281
    :cond_2
    const-string v2, "3"

    aput-object v2, v0, v8

    .line 2282
    const-string v2, "None"

    aput-object v2, v0, v6

    .line 2283
    aget-object v2, v1, v8

    aput-object v2, v0, v7

    .line 2284
    const/4 v2, 0x3

    aget-object v3, v1, v6

    aput-object v3, v0, v2

    .line 2296
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_0

    .line 2297
    const-string v2, "SensorRead"

    const-string v3, "getProximityOffset"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2286
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_4

    .line 2287
    const-string v3, "SensorRead"

    const-string v4, "getProximityOffset"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 2290
    goto :goto_0

    .end local v1    # "tempString":[Ljava/lang/String;
    :cond_5
    move-object v0, v2

    .line 2293
    goto :goto_0
.end method

.method public getTemperature(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 2304
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2305
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 2306
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnTemperature()[F

    move-result-object v1

    .line 2308
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    .line 2309
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 2310
    const-string v2, "SensorRead"

    const-string v3, "getTemperature"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2313
    :cond_0
    const-string v2, "3"

    aput-object v2, v0, v8

    .line 2314
    const-string v2, "%.2f"

    new-array v3, v6, [Ljava/lang/Object;

    aget v4, v1, v8

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2315
    const-string v2, "%.2f"

    new-array v3, v6, [Ljava/lang/Object;

    aget v4, v1, v6

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 2316
    const/4 v2, 0x3

    aget v3, v1, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2331
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v7, :cond_1

    .line 2332
    const-string v2, "SensorRead"

    const-string v3, "getTemperature"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2335
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 2319
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v7, :cond_3

    .line 2320
    const-string v3, "SensorRead"

    const-string v4, "getTemperature"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 2323
    goto :goto_0

    .line 2325
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v7, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 2326
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 2328
    goto :goto_0
.end method

.method public getUV(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2481
    const/16 v3, 0x11

    new-array v0, v3, [Ljava/lang/String;

    .line 2483
    .local v0, "returnData":[Ljava/lang/String;
    if-ne p1, v6, :cond_4

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 2484
    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadManager;->returnUV()[F

    move-result-object v1

    .line 2486
    .local v1, "tempFloat":[F
    if-eqz v1, :cond_2

    array-length v3, v1

    if-lez v3, :cond_2

    .line 2487
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v6, :cond_0

    .line 2488
    const-string v2, "SensorRead"

    const-string v3, "getUV"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2491
    :cond_0
    const-string v2, "3"

    aput-object v2, v0, v7

    .line 2492
    const-string v2, "%.2f"

    new-array v3, v6, [Ljava/lang/Object;

    aget v4, v1, v7

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 2493
    const-string v2, "%.2f"

    new-array v3, v6, [Ljava/lang/Object;

    aget v4, v1, v6

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 2494
    const/4 v2, 0x3

    const-string v3, "%.2f"

    new-array v4, v6, [Ljava/lang/Object;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2508
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v2, v8, :cond_1

    .line 2509
    const-string v2, "SensorRead"

    const-string v3, "getBodyTemp"

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorRead;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2512
    .end local v0    # "returnData":[Ljava/lang/String;
    .end local v1    # "tempFloat":[F
    :cond_1
    :goto_0
    return-object v0

    .line 2496
    .restart local v0    # "returnData":[Ljava/lang/String;
    .restart local v1    # "tempFloat":[F
    :cond_2
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v3, v8, :cond_3

    .line 2497
    const-string v3, "SensorRead"

    const-string v4, "getBodyTemp"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 2500
    goto :goto_0

    .line 2502
    .end local v1    # "tempFloat":[F
    :cond_4
    if-ne p1, v8, :cond_5

    iget-object v3, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v3, :cond_5

    move-object v0, v2

    .line 2503
    goto :goto_0

    :cond_5
    move-object v0, v2

    .line 2505
    goto :goto_0
.end method

.method public isSensorOn(I)Z
    .locals 5
    .param p1, "sensorID"    # I

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 132
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v1, :cond_0

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MIN:I

    if-gt v1, p1, :cond_0

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    if-ge p1, v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v1, p1}, Lcom/sec/factory/modules/SensorReadManager;->isSensorOn(I)Z

    move-result v0

    .line 144
    :goto_0
    const-string v1, "SensorRead"

    const-string v2, "isSensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensorID["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return v0

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v1, :cond_1

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    if-gt v1, p1, :cond_1

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_1

    .line 137
    iget-object v1, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    invoke-virtual {v1, p1}, Lcom/sec/factory/modules/SensorReadFile;->isSensorOn(I)Z

    move-result v0

    goto :goto_0

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    if-eqz v1, :cond_2

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int/2addr v1, v2

    if-gt v1, p1, :cond_2

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MAX:I

    if-gt p1, v1, :cond_2

    .line 140
    iget-object v1, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    invoke-virtual {v1, p1}, Lcom/sec/factory/modules/SensorReadIntent;->isSensorOn(I)Z

    move-result v0

    goto :goto_0

    .line 142
    :cond_2
    const-string v1, "SensorRead"

    const-string v2, "SensorOn"

    const-string v3, "null / ID unknown"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isSensorReady(I)Z
    .locals 6
    .param p1, "sensorID"    # I

    .prologue
    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-nez v2, :cond_0

    .line 151
    const-string v2, "SensorRead"

    const-string v3, "isSensorReady"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SensorManager is null + , sensorID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 172
    .end local v0    # "result":Z
    .local v1, "result":I
    :goto_0
    return v1

    .line 155
    .end local v1    # "result":I
    .restart local v0    # "result":Z
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    if-eqz v2, :cond_1

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MIN:I

    if-gt v2, p1, :cond_1

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    if-ge p1, v2, :cond_1

    .line 157
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadManager:Lcom/sec/factory/modules/SensorReadManager;

    invoke-virtual {v2, p1}, Lcom/sec/factory/modules/SensorReadManager;->isSensorReady(I)Z

    move-result v0

    .line 171
    :goto_1
    const-string v2, "SensorRead"

    const-string v3, "isSensorReady"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sensorID["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 172
    .restart local v1    # "result":I
    goto :goto_0

    .line 158
    .end local v1    # "result":I
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v2, :cond_2

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    if-gt v2, p1, :cond_2

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int/2addr v2, v3

    if-ge p1, v2, :cond_2

    .line 160
    const-string v2, "SensorRead"

    const-string v3, "isSensorReady"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not support + , sensorID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x1

    goto :goto_1

    .line 163
    :cond_2
    iget-object v2, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadIntent:Lcom/sec/factory/modules/SensorReadIntent;

    if-eqz v2, :cond_3

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int/2addr v2, v3

    if-gt v2, p1, :cond_3

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MAX:I

    if-gt p1, v2, :cond_3

    .line 165
    const-string v2, "SensorRead"

    const-string v3, "isSensorReady"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Not support + , sensorID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/4 v0, 0x1

    goto :goto_1

    .line 169
    :cond_3
    const-string v2, "SensorRead"

    const-string v3, "isSensorReady"

    const-string v4, "null / ID unknown"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public setLoop_ReadFile([I)V
    .locals 5
    .param p1, "sensorID"    # [I

    .prologue
    .line 120
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    if-eqz v4, :cond_1

    if-eqz p1, :cond_1

    .line 121
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget v1, v0, v2

    .line 122
    .local v1, "i":I
    sget v4, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    if-gt v4, v1, :cond_0

    .line 123
    iget-object v4, p0, Lcom/sec/factory/modules/SensorRead;->mSensorReadFile:Lcom/sec/factory/modules/SensorReadFile;

    invoke-virtual {v4, v1}, Lcom/sec/factory/modules/SensorReadFile;->startLoop(I)V

    .line 121
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 127
    .end local v0    # "arr$":[I
    .end local v1    # "i":I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    return-void
.end method
