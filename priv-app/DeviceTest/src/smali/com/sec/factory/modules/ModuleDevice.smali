.class public Lcom/sec/factory/modules/ModuleDevice;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/ModuleDevice$HallIC;,
        Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;
    }
.end annotation


# static fields
.field public static final ACTION_EXTERNAL_STORAGE_MOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_EXTERNAL_STORAGE_MOUNTED"

.field public static final ACTION_EXTERNAL_STORAGE_UNMOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_EXTERNAL_STORAGE_UNMOUNTED"

.field public static final ACTION_OTG_RESPONSE:Ljava/lang/String; = "com.sec.factory.intent.ACTION_OTG_RESPONSE"

.field public static final ACTION_USB_STORAGE_MOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

.field public static final ACTION_USB_STORAGE_UNMOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

.field public static final CHANNEL_UP:Ljava/lang/String; = "38400,173,171,24,62,24,61,24,62,24,17,24,17,24,18,24,17,24,18,23,62,24,61,24,62,24,17,24,17,25,17,24,17,24,17,24,19,23,61,24,18,24,17,24,61,24,19,23,17,24,17,24,62,24,17,24,62,24,61,24,19,23,61,24,62,24,61,24,1880,0"

.field public static final CHANNEL_UP_HESTIA:Ljava/lang/String; = "38000,4526,4447,578,1657,578,1657,578,1657,552,500,552,500,552,500,552,500,552,500,578,1657,578,1657,578,1657,552,500,552,500,552,500,552,500,552,500,552,500,578,1657,552,500,552,500,578,1657,552,500,552,500,552,500,578,1657,552,500,578,1657,578,1657,552,500,578,1657,578,1657,578,1657,578,46447"

.field public static final CHANNEL_UP_TIME:Ljava/lang/String; = "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,552,578,1683,578,552,578,552,578,1683,578,552,578,552,578,552,578,1683,578,552,578,1683,578,1683,578,552,578,1683,578,1683,578,1683,578,23047"

.field public static final EXTERNAL_MEMORY:I = 0x1

.field public static final INTERNAL_MEMORY:I = 0x0

.field private static final IRLEDOFF:Ljava/lang/String; = "38400,5"

.field private static final IRLEDON:Ljava/lang/String; = "38400,10"

.field private static final KEYCODE_DUMMY_BACK:I = 0x11d

.field private static final KEYCODE_DUMMY_HOME:I = 0x11c

.field private static final KEYCODE_DUMMY_HOME1:I = 0x11e

.field private static final KEYCODE_DUMMY_HOME2:I = 0x11f

.field private static final KEYCODE_DUMMY_MENU:I = 0x11a

.field private static final KEYCODE_REF1:I = 0xfe

.field private static final KEYCODE_REF2:I = 0xff

.field public static final LED_STATE_BLUE:I = 0x4

.field public static final LED_STATE_GREEN:I = 0x3

.field public static final LED_STATE_OFF:I = 0x0

.field public static final LED_STATE_RED:I = 0x2

.field public static final LED_STATE_WHITE:I = 0x1

.field public static final LPA_MODE_DISABLE:Ljava/lang/String; = "0"

.field public static final LPA_MODE_ENABLE:Ljava/lang/String; = "3"

.field public static final LPM_MODE_DISABLE:Ljava/lang/String; = "0"

.field public static final MODULE_BARCODE_EMUL:I = 0x6

.field public static final MODULE_CAMERA:I = 0x0

.field public static final MODULE_EPEN:I = 0x4

.field public static final MODULE_EPEN_WITHOUT_CHECKSUM:I = 0x3

.field public static final MODULE_REAR_CAMOIS:I = 0x8

.field public static final MODULE_SENSORHUB:I = 0x5

.field public static final MODULE_TSK:I = 0x2

.field public static final MODULE_TSP:I = 0x1

.field public static final MODULE_ULTRASONIC:I = 0x7

.field private static final MSG_OTG_TEST_FINISH:B = 0x0t

.field private static final OTG_MUIC_OFF:[B

.field private static final OTG_MUIC_ON:[B

.field private static final OTG_TEST_OFF:[B

.field private static final OTG_TEST_ON:[B

.field private static final OTG_TEST_WAIT_DELAY:I = 0x7d0

.field public static final PATH_EXTERNAL_STORAGE:Ljava/lang/String; = "extSdCard"

.field public static final PATH_INTERNAL_STORAGE:Ljava/lang/String; = "sdcard"

.field public static final PATH_OTG_STORAGE:Ljava/lang/String; = "UsbStotageX"

.field private static final SENSOR_HUB_REFERENCEDATA:Ljava/lang/String; = "sys.factory.setReferenceData"

.field private static final STORAGE_PATH:[Ljava/lang/String;

.field public static final SYSTEM_STORAGE:I = 0x2

.field public static final TIMELESS_VIBRATOR:J = 0x7fffffffL

.field private static final TOUCH_KEY_PATH_DEFAULT:I = 0x0

.field private static final TOUCH_KEY_PATH_INNER:I = 0x1

.field private static final TOUCH_KEY_PATH_OUTER:I = 0x2

.field public static final TSP_CMD_CHIP_NAME_READ:Ljava/lang/String; = "get_chip_name"

.field public static final TSP_CMD_CHIP_VENDOR_READ:Ljava/lang/String; = "get_chip_vendor"

.field public static final TSP_CMD_MODULE_OFF_MASTER:Ljava/lang/String; = "module_off_master"

.field public static final TSP_CMD_MODULE_OFF_SLAVE:Ljava/lang/String; = "module_off_slave"

.field public static final TSP_CMD_MODULE_ON_MASTER:Ljava/lang/String; = "module_on_master"

.field public static final TSP_CMD_MODULE_ON_SLAVE:Ljava/lang/String; = "module_on_slave"

.field public static final TSP_CMD_MODULE_TSP_CONNECTION:Ljava/lang/String; = "get_tsp_connection"

.field public static final TSP_CMD_READ1_CM_ABS:Ljava/lang/String; = "get_cm_abs"

.field public static final TSP_CMD_READ1_GET_REF:Ljava/lang/String; = "get_reference"

.field public static final TSP_CMD_READ1_RAWCAP:Ljava/lang/String; = "get_rawcap"

.field public static final TSP_CMD_READ1_RUN_CM_ABS:Ljava/lang/String; = "run_cm_abs_read"

.field public static final TSP_CMD_READ1_RUN_RAWCAP:Ljava/lang/String; = "run_rawcap_read"

.field public static final TSP_CMD_READ1_RUN_REF:Ljava/lang/String; = "run_reference_read"

.field public static final TSP_CMD_READ2_CM_DELFA:Ljava/lang/String; = "get_cm_delta"

.field public static final TSP_CMD_READ2_GET_RAW:Ljava/lang/String; = "get_raw"

.field public static final TSP_CMD_READ2_RUN_CM_DELTA:Ljava/lang/String; = "run_cm_delta_read"

.field public static final TSP_CMD_READ2_RUN_RxToRx:Ljava/lang/String; = "run_rx_to_rx_read"

.field public static final TSP_CMD_READ2_RxToRx:Ljava/lang/String; = "get_rx_to_rx"

.field public static final TSP_CMD_READ3_GET_DELTA:Ljava/lang/String; = "get_delta"

.field public static final TSP_CMD_READ3_INTENSITY:Ljava/lang/String; = "get_intensity"

.field public static final TSP_CMD_READ3_RUN_DELTA:Ljava/lang/String; = "run_delta_read"

.field public static final TSP_CMD_READ3_RUN_INTENSITY:Ljava/lang/String; = "run_intensity_read"

.field public static final TSP_CMD_READ3_RUN_TxToTx:Ljava/lang/String; = "run_tx_to_tx_read"

.field public static final TSP_CMD_READ3_TxToTx:Ljava/lang/String; = "get_tx_to_tx"

.field public static final TSP_CMD_READ4_RUN_TxToGND:Ljava/lang/String; = "run_tx_to_gnd_read"

.field public static final TSP_CMD_READ4_TxToGND:Ljava/lang/String; = "get_tx_to_gnd"

.field public static final TSP_CMD_READ5_RUN_RAW:Ljava/lang/String; = "run_raw_read"

.field public static final TSP_CMD_SELFTEST:Ljava/lang/String; = "run_selftest"

.field public static final TSP_CMD_X_COUNT:Ljava/lang/String; = "get_x_num"

.field public static final TSP_CMD_Y_COUNT:Ljava/lang/String; = "get_y_num"

.field public static final TSP_FIRMWARE_VERSION_PANEL:Ljava/lang/String; = "get_fw_ver_ic"

.field public static final TSP_FIRMWARE_VERSION_PHONE:Ljava/lang/String; = "get_fw_ver_bin"

.field public static final TSP_RETURN_VALUE_NA:Ljava/lang/String; = "NA"

.field public static final TSP_RETURN_VALUE_NG:Ljava/lang/String; = "NG"

.field public static final TSP_RETURN_VALUE_OK:Ljava/lang/String; = "OK"

.field public static final TYPE_EXTERNAL_STORAGE:I = 0x1

.field public static final TYPE_INTERNAL_STORAGE:I = 0x0

.field public static final TYPE_USB_STORAGE:I = 0x2

.field public static final TYPE_USB_STORAGE_B:I = 0x3

.field public static final TYPE_USB_STORAGE_C:I = 0x4

.field public static final TYPE_USB_STORAGE_D:I = 0x5

.field public static final TYPE_USB_STORAGE_E:I = 0x6

.field public static final TYPE_USB_STORAGE_F:I = 0x7

.field public static final UNIT_BYTE:I = 0x0

.field public static final UNIT_GB:I = 0x40000000

.field public static final UNIT_KB:I = 0x400

.field public static final UNIT_MB:I = 0x100000

.field public static final USER_STORAGE_1:I = 0x0

.field public static final USER_STORAGE_2:I = 0x1

.field private static final isAPQ8064:Z

.field private static final isAPQ8074:Z

.field private static final isAPQ8084:Z

.field private static final isCapri:Z

.field private static final isEOS2:Z

.field private static final isEXYNOS5410:Z

.field private static final isHawai:Z

.field private static final isJava:Z

.field private static final isMSM8228:Z

.field private static final isMSM8260A:Z

.field private static final isMSM8274:Z

.field private static final isMSM8674:Z

.field private static final isMSM8928:Z

.field private static final isMSM8930:Z

.field private static final isMSM8960:Z

.field private static final isMSM8974:Z

.field private static final isPegasus:Z

.field private static final isPegasusPrime:Z

.field private static final isSPRD:Z

.field private static mInstance:Lcom/sec/factory/modules/ModuleDevice;

.field private static mRunningIRLedThread:Z


# instance fields
.field private MAGNITUDE_MAX:I

.field private final USB_MODE:[Ljava/lang/String;

.field private freqRange:[Landroid/hardware/ConsumerIrManager$CarrierFrequencyRange;

.field private irManager:Landroid/hardware/ConsumerIrManager;

.field left_motor:I

.field private mAm:Landroid/app/ActivityManager;

.field private mIRLedThread:Ljava/lang/Thread;

.field private mIsMountedExternalStorage:Z

.field private mIsMountedUsbStorage:Z

.field private mIsMountedUsbStorageB:Z

.field private mIsMountedUsbStorageC:Z

.field private mIsMountedUsbStorageD:Z

.field private mIsMountedUsbStorageE:Z

.field private mIsMountedUsbStorageF:Z

.field private mIsVibrating:Z

.field private final mLeftPattern:[B

.field private mMediaStateReceiver:Landroid/content/BroadcastReceiver;

.field private mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

.field private mMountService:Landroid/os/storage/IMountService;

.field private final mRightPattern:[B

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field public mTSPVendorName:Ljava/lang/String;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private productModelName:Ljava/lang/String;

.field right_motor:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->mInstance:Lcom/sec/factory/modules/ModuleDevice;

    .line 64
    const-string v0, "MSM8960"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8960:Z

    .line 65
    const-string v0, "MSM8974"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8974:Z

    .line 66
    const-string v0, "MSM8928"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8928:Z

    .line 67
    const-string v0, "MSM8228"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8228:Z

    .line 68
    const-string v0, "MSM8674"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8674:Z

    .line 69
    const-string v0, "MSM8274"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8274:Z

    .line 70
    const-string v0, "MSM8260A"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8260A:Z

    .line 71
    const-string v0, "MSM8930"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isMSM8930:Z

    .line 72
    const-string v0, "APQ8064"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isAPQ8064:Z

    .line 73
    const-string v0, "APQ8074"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isAPQ8074:Z

    .line 74
    const-string v0, "APQ8084"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isAPQ8084:Z

    .line 75
    const-string v0, "Pegasus"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isPegasus:Z

    .line 76
    const-string v0, "PegasusPrime"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isPegasusPrime:Z

    .line 77
    const-string v0, "EXYNOS5410"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isEXYNOS5410:Z

    .line 78
    const-string v0, "Hawai"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isHawai:Z

    .line 79
    const-string v0, "Capri"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isCapri:Z

    .line 80
    const-string v0, "java"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isJava:Z

    .line 81
    const-string v0, "EOS2"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isEOS2:Z

    .line 82
    const-string v0, "Spreadtrum"

    const-string v1, "CHIPSET_MANUFACTURE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/factory/modules/ModuleDevice;->isSPRD:Z

    .line 284
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "/mnt/sdcard"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "/mnt/extSdCard"

    aput-object v2, v0, v1

    const-string v1, "/system"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    .line 1209
    new-array v0, v5, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_ON:[B

    .line 1212
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_OFF:[B

    .line 1215
    new-array v0, v3, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_ON:[B

    .line 1218
    new-array v0, v3, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_OFF:[B

    .line 1811
    sput-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->mRunningIRLedThread:Z

    return-void

    .line 1209
    nop

    :array_0
    .array-data 1
        0x4ft
        0x4et
        0x0t
    .end array-data

    .line 1212
    :array_1
    .array-data 1
        0x4ft
        0x46t
        0x46t
        0x0t
    .end array-data

    .line 1215
    :array_2
    .array-data 1
        0x30t
        0x0t
    .end array-data

    .line 1218
    nop

    :array_3
    .array-data 1
        0x31t
        0x0t
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v6, 0x12

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 105
    const-string v1, "ModuleDevice"

    invoke-direct {p0, p1, v1}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 95
    iput-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 97
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    .line 100
    const-string v1, "ro.product.name"

    const-string v2, "NONE"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->productModelName:Ljava/lang/String;

    .line 101
    iput-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;

    .line 203
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    .line 204
    const/16 v1, 0x2710

    iput v1, p0, Lcom/sec/factory/modules/ModuleDevice;->MAGNITUDE_MAX:I

    .line 206
    iput v3, p0, Lcom/sec/factory/modules/ModuleDevice;->left_motor:I

    iput v5, p0, Lcom/sec/factory/modules/ModuleDevice;->right_motor:I

    .line 208
    new-array v1, v6, [B

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mLeftPattern:[B

    .line 215
    new-array v1, v6, [B

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mRightPattern:[B

    .line 469
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z

    .line 470
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z

    .line 471
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z

    .line 473
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z

    .line 474
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z

    .line 476
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z

    .line 477
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z

    .line 479
    new-instance v1, Lcom/sec/factory/modules/ModuleDevice$1;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleDevice$1;-><init>(Lcom/sec/factory/modules/ModuleDevice;)V

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mMediaStateReceiver:Landroid/content/BroadcastReceiver;

    .line 970
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    .line 1188
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "SUSB"

    aput-object v2, v1, v3

    const-string v2, "MTP"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, "MSTR"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "AOC"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->USB_MODE:[Ljava/lang/String;

    .line 1808
    iput-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    .line 106
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "ModuleDevice"

    const-string v3, "Create ModuleDevice"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/SystemVibrator;

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    .line 108
    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 109
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mAm:Landroid/app/ActivityManager;

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mMediaStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/factory/modules/ModuleDevice;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 115
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "ModuleDevice"

    const-string v3, "registerReceiver OK"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleDevice;->updateStorageMountState()V

    .line 117
    invoke-static {p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPVendorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    .line 119
    const-string v1, "PEEL"

    const-string v2, "SUPPORT_IRLED_CONCEPT"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    const-string v1, "consumer_ir"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/ConsumerIrManager;

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;

    .line 123
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;

    invoke-virtual {v1}, Landroid/hardware/ConsumerIrManager;->getCarrierFrequencies()[Landroid/hardware/ConsumerIrManager$CarrierFrequencyRange;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->freqRange:[Landroid/hardware/ConsumerIrManager$CarrierFrequencyRange;

    .line 125
    :cond_0
    return-void

    .line 208
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        -0x1t
        0xft
        0x0t
        0x7ft
        0x0t
        0x0t
        -0xft
    .end array-data

    .line 215
    nop

    :array_1
    .array-data 1
        0x2t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        -0x1t
        0x1ft
        0x0t
        0x7ft
        0x0t
        0x0t
        -0xft
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z

    return v0
.end method

.method static synthetic access$1002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 62
    sput-boolean p0, Lcom/sec/factory/modules/ModuleDevice;->mRunningIRLedThread:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/sec/factory/modules/ModuleDevice;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/factory/modules/ModuleDevice;)Landroid/hardware/ConsumerIrManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/factory/modules/ModuleDevice;Ljava/lang/String;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleDevice;->convertPattern(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/factory/modules/ModuleDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/factory/modules/ModuleDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDevice;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleDevice;->updateStorageMountState()V

    return-void
.end method

.method static synthetic access$800()[B
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_OFF:[B

    return-object v0
.end method

.method static synthetic access$900()[B
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_OFF:[B

    return-object v0
.end method

.method private convertPattern(Ljava/lang/String;)[I
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 1870
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1871
    .local v0, "arr":[Ljava/lang/String;
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    new-array v1, v3, [I

    .line 1873
    .local v1, "converted":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 1874
    add-int/lit8 v3, v2, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v2

    .line 1873
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1876
    :cond_0
    return-object v1
.end method

.method private declared-synchronized getMountService()Landroid/os/storage/IMountService;
    .locals 4

    .prologue
    .line 692
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mMountService:Landroid/os/storage/IMountService;

    if-nez v1, :cond_0

    .line 693
    const-string v1, "mount"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 695
    .local v0, "service":Landroid/os/IBinder;
    if-eqz v0, :cond_1

    .line 696
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mMountService:Landroid/os/storage/IMountService;

    .line 702
    .end local v0    # "service":Landroid/os/IBinder;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mMountService:Landroid/os/storage/IMountService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 698
    .restart local v0    # "service":Landroid/os/IBinder;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMountService"

    const-string v3, "Can\'t get mount service"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 692
    .end local v0    # "service":Landroid/os/IBinder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static declared-synchronized instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDevice;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    const-class v1, Lcom/sec/factory/modules/ModuleDevice;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/factory/modules/ModuleDevice;->mInstance:Lcom/sec/factory/modules/ModuleDevice;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/sec/factory/modules/ModuleDevice;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleDevice;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice;->mInstance:Lcom/sec/factory/modules/ModuleDevice;

    .line 132
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModuleDevice;->mInstance:Lcom/sec/factory/modules/ModuleDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isEpenChecksumPass()Z
    .locals 7

    .prologue
    .line 1141
    const/4 v1, 0x0

    .line 1142
    .local v1, "isfwDownloaded":Z
    const-string v3, "EPEN_CHECKSUM_CHECK"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1145
    const-wide/16 v4, 0x2bc

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1152
    :goto_0
    :try_start_1
    const-string v3, "EPEN_CHECKSUM_RESULT"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1153
    .local v2, "result":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "isEpenChecksumPass"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    const-string v3, "PASS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 1159
    .end local v2    # "result":Ljava/lang/String;
    :goto_1
    return v1

    .line 1146
    :catch_0
    move-exception v0

    .line 1148
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1154
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "result":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1155
    .end local v2    # "result":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 1156
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private startTSPTest([BI)Ljava/lang/String;
    .locals 6
    .param p1, "cmd"    # [B
    .param p2, "commandLength"    # I

    .prologue
    .line 1395
    const-string v1, ""

    .line 1396
    .local v1, "status":Ljava/lang/String;
    const-string v0, ""

    .line 1398
    .local v0, "result":Ljava/lang/String;
    const-string v2, "TSP_COMMAND_CMD"

    invoke-static {v2, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1399
    const-string v2, "TSP_COMMAND_STATUS"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1400
    const-string v2, "TSP_COMMAND_RESULT"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1402
    if-nez v1, :cond_0

    .line 1403
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Status Read => status == null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    const-string v2, "NG"

    .line 1432
    :goto_0
    return-object v2

    .line 1405
    :cond_0
    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1406
    if-nez v0, :cond_1

    .line 1407
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Result Read =>1 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    const-string v2, "NG"

    goto :goto_0

    .line 1411
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1412
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1414
    :cond_2
    const-string v2, "NOT_APPLICABLE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1415
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "N/A- Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "N/A - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    if-nez v0, :cond_3

    .line 1419
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Result Read =>2 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    const-string v2, "NG"

    goto/16 :goto_0

    .line 1424
    :cond_3
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 1426
    :cond_4
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail - Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    const-string v2, "NG"

    goto/16 :goto_0

    .line 1431
    :cond_5
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Command Write"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    const-string v2, "NG"

    goto/16 :goto_0
.end method

.method private updateStorageMountState()V
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z

    .line 707
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z

    .line 708
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z

    .line 709
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z

    .line 710
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z

    .line 711
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z

    .line 712
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z

    .line 713
    return-void
.end method


# virtual methods
.method public checkResult_IRLED()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1880
    const-string v3, "SUPPORT_IRLED_FEEDBACK_IC"

    invoke-static {v3, v2}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1882
    .local v0, "feeedBackIc":Z
    if-eqz v0, :cond_0

    .line 1883
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "checkResult_IRLED"

    const-string v5, "Feed Back IC Supported"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1884
    const-string v3, "IR_LED_RESULT_TX"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1886
    .local v1, "result":Ljava/lang/String;
    const-string v3, "1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1891
    .end local v1    # "result":Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public checkWarrnatyBit()Ljava/lang/String;
    .locals 5

    .prologue
    .line 920
    const-string v0, ""

    .line 921
    .local v0, "warrnatyBit":Ljava/lang/String;
    const-string v1, "WARRANTY_BIT_CHECK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 923
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "checkWarrnatyBit"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "warrnatyBit="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    return-object v0
.end method

.method public check_USB_Port(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "port_num"    # Ljava/lang/String;

    .prologue
    .line 1313
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "check_USB_Port"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start USB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Port Check Test"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const/4 v0, 0x0

    .line 1316
    .local v0, "resData":Ljava/lang/String;
    const-string v1, "1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1317
    const-string v1, "USB_1ST_PORT_CHECK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1319
    if-nez v0, :cond_0

    .line 1320
    const-string v1, "USB_1ST_PORT_SUPER_CHECK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1330
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "check_USB_Port"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start USB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Port Check : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    if-nez v0, :cond_2

    .line 1333
    const-string v0, "NG_NG"

    .line 1338
    :goto_1
    return-object v0

    .line 1322
    :cond_1
    const-string v1, "2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1323
    const-string v1, "USB_2ST_PORT_CHECK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1325
    if-nez v0, :cond_0

    .line 1326
    const-string v1, "USB_2ST_PORT_SUPER_CHECK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1335
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OK_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public controlIRLED(Ljava/lang/String;)V
    .locals 12
    .param p1, "control"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1814
    const-string v8, "IS_IRLED_TEST_SPLIT_COMMAND"

    invoke-static {v8}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v6

    .line 1816
    .local v6, "isSplitCmd":Z
    const-string v8, "PEEL"

    const-string v9, "SUPPORT_IRLED_CONCEPT"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1818
    .local v5, "isPeel":Z
    const-string v8, "IR_LED_SEND_TEST"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v4

    .line 1819
    .local v4, "isExistONOFF":Z
    const-string v8, "IR_LED_SEND"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v3

    .line 1820
    .local v3, "isExistCTRL":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "controlIRLED"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isExistONOFF : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " , isExistCTRL : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " , isSplitCmd : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " , isPeel : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "controlIRLED"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "control : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1826
    if-eqz v5, :cond_2

    .line 1827
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;

    invoke-virtual {v8}, Landroid/hardware/ConsumerIrManager;->hasIrEmitter()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1828
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;

    const v9, 0x9470

    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleDevice;->convertPattern(Ljava/lang/String;)[I

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/hardware/ConsumerIrManager;->transmit(I[I)V

    .line 1867
    :cond_0
    :goto_0
    return-void

    .line 1830
    :cond_1
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "controlIrED"

    const-string v10, "Device doesn\'t have IR Emitter"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1833
    :cond_2
    if-eqz v4, :cond_3

    .line 1834
    const-string v8, "IR_LED_SEND_TEST"

    const-string v9, "38400,10"

    invoke-static {v8, v9}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1837
    :cond_3
    const/4 v8, 0x1

    if-ne v3, v8, :cond_5

    .line 1838
    if-eqz v6, :cond_6

    .line 1839
    new-instance v7, Ljava/io/FileOutputStream;

    const-string v8, "IR_LED_SEND"

    invoke-direct {v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1841
    .local v7, "out":Ljava/io/FileOutputStream;
    const-string v8, ","

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1844
    .local v0, "data":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    array-length v8, v0

    if-ge v2, v8, :cond_4

    .line 1845
    aget-object v8, v0, v2

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 1844
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1848
    :cond_4
    const-string v8, "0"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 1849
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 1850
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1854
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 1863
    .end local v0    # "data":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v7    # "out":Ljava/io/FileOutputStream;
    :cond_5
    :goto_2
    if-eqz v4, :cond_0

    .line 1864
    const-string v8, "IR_LED_SEND_TEST"

    const-string v9, "38400,5"

    invoke-static {v8, v9}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 1851
    .restart local v0    # "data":[Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 1852
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    goto :goto_2

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    throw v8

    .line 1858
    .end local v0    # "data":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v7    # "out":Ljava/io/FileOutputStream;
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 1859
    .local v0, "data":[B
    const-string v8, "IR_LED_SEND"

    invoke-static {v8, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    goto :goto_2
.end method

.method public doSimSwitch()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 861
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "simActiate"

    const-string v2, "2ND RIL, Res_jiaming = "

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    invoke-virtual {p0, v3, v3}, Lcom/sec/factory/modules/ModuleDevice;->setRadioSimSlot(II)Z

    .line 863
    invoke-virtual {p0, v4, v3}, Lcom/sec/factory/modules/ModuleDevice;->setRadioSimSlot(II)Z

    .line 864
    return v4
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleDevice"

    const-string v2, "finalize ModuleDevice"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mMediaStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleDevice;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 139
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 140
    return-void
.end method

.method public firmwareDownload(I)Z
    .locals 5
    .param p1, "module"    # I

    .prologue
    .line 973
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "firmwareDownload"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "module="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    const/4 v0, 0x0

    .line 976
    .local v0, "sysfsid":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 984
    :goto_0
    :pswitch_0
    if-nez v0, :cond_0

    .line 985
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "firmwareDownload"

    const-string v3, "No supported module"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    const/4 v1, 0x0

    .line 988
    :goto_1
    return v1

    .line 980
    :pswitch_1
    const-string v0, "TSK_FIRMWARE_UPDATE"

    goto :goto_0

    .line 988
    :cond_0
    const-string v1, "S"

    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 976
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getAvailMem()Ljava/lang/String;
    .locals 8

    .prologue
    .line 401
    const/4 v2, 0x0

    .line 403
    .local v2, "resData":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mAm:Landroid/app/ActivityManager;

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 404
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mMemInfo:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v4, v3, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/16 v6, 0x400

    div-long v0, v4, v6

    .line 405
    .local v0, "available":J
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAvailMem"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Main]Available RAM Size : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "KB"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 408
    return-object v2
.end method

.method public getAvailableSize(II)J
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 306
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/factory/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 307
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getInnerMemoryDevSize(I)J
    .locals 8
    .param p1, "unit"    # I

    .prologue
    const-wide/16 v6, 0x200

    .line 323
    const-wide/16 v0, -0x1

    .line 325
    .local v0, "byteSize":J
    :try_start_0
    const-string v3, "SUPPORT_UFS"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 326
    const-string v3, "UFS_DEVICE_SIZE"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    mul-long v0, v4, v6

    .line 335
    :goto_0
    int-to-long v4, p1

    div-long v4, v0, v4

    :goto_1
    return-wide v4

    .line 328
    :cond_0
    :try_start_1
    const-string v3, "IMEM_DEVICE_SIZE"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    mul-long v0, v4, v6

    goto :goto_0

    .line 330
    :catch_0
    move-exception v2

    .line 331
    .local v2, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 332
    const-wide/16 v4, -0x1

    goto :goto_1
.end method

.method public getInnerMemorySize()J
    .locals 6

    .prologue
    .line 339
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 340
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x100000

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getReferenceDataCount()I
    .locals 7

    .prologue
    .line 2100
    const-string v3, "sys.factory.setReferenceData"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2101
    .local v2, "result":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getReferenceDataCount"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2103
    const/4 v0, 0x0

    .line 2105
    .local v0, "ivalue":I
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2109
    :goto_0
    return v0

    .line 2106
    :catch_0
    move-exception v1

    .line 2107
    .local v1, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public getSimSlot_Count()I
    .locals 1

    .prologue
    .line 856
    invoke-static {}, Lcom/sec/factory/dummy/MultiSimManager;->getSimSlotCount()I

    move-result v0

    .line 857
    .local v0, "simslot_count":I
    return v0
.end method

.method public getSize(II)J
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 301
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/factory/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 302
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getSpeakerCount()I
    .locals 1

    .prologue
    .line 1799
    const-string v0, "SPEAKER_COUNT"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getStoragePath(I)Ljava/lang/String;
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 656
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v1

    .line 658
    .local v1, "storageVolumes":[Landroid/os/storage/StorageVolume;
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 659
    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 660
    .local v0, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getStorageParh"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Storage path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    .end local v0    # "path":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 664
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getStorageParh"

    const-string v4, "Storage path : null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalMemory()Ljava/lang/String;
    .locals 9

    .prologue
    .line 364
    const/4 v2, 0x0

    .line 365
    .local v2, "resData":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 367
    .local v4, "total":J
    const-string v3, "sysfs"

    const-string v6, "RAM_SIZE_IF"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 369
    const-string v3, "DEVICE_RAM_SIZE_FILE"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v4, v3

    .line 370
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getTotalMemory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DEVICE_RAM_SIZE : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :goto_0
    const-wide/16 v6, 0xa00

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 382
    const-string v2, "3"

    .line 397
    :goto_1
    return-object v2

    .line 373
    :cond_0
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 374
    .local v0, "actManager":Landroid/app/ActivityManager;
    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 375
    .local v1, "memInfo":Landroid/app/ActivityManager$MemoryInfo;
    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 376
    iget-wide v4, v1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    .line 377
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getTotalMemory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ActivityManager.MemoryInfo : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-wide/32 v6, 0x100000

    div-long/2addr v4, v6

    goto :goto_0

    .line 383
    .end local v0    # "actManager":Landroid/app/ActivityManager;
    .end local v1    # "memInfo":Landroid/app/ActivityManager$MemoryInfo;
    :cond_1
    const-wide/16 v6, 0x81f

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 384
    const-string v2, "2.5"

    goto :goto_1

    .line 385
    :cond_2
    const-wide/16 v6, 0x600

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 386
    const-string v2, "2"

    goto :goto_1

    .line 387
    :cond_3
    const-wide/16 v6, 0x400

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    .line 388
    const-string v2, "1.5"

    goto :goto_1

    .line 389
    :cond_4
    const-wide/16 v6, 0x300

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 390
    const-string v2, "1"

    goto :goto_1

    .line 391
    :cond_5
    const-wide/16 v6, 0x200

    cmp-long v3, v4, v6

    if-lez v3, :cond_6

    .line 392
    const-string v2, "0.7"

    goto :goto_1

    .line 394
    :cond_6
    const-string v2, "0.5"

    goto :goto_1
.end method

.method public getUSBMode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1195
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->USB_MODE:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDevice;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "usb_setting_mode"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getUseSize(II)J
    .locals 4
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 311
    invoke-virtual {p0, p1, p2}, Lcom/sec/factory/modules/ModuleDevice;->getSize(II)J

    move-result-wide v0

    invoke-virtual {p0, p1, p2}, Lcom/sec/factory/modules/ModuleDevice;->getAvailableSize(II)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getWindowSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 2178
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2180
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2181
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 2182
    return-object v1
.end method

.method public gripDetectionOnOFF(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 1736
    const-string v0, "set_longpress_enable"

    .line 1737
    .local v0, "mainCmd":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1738
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onOffGripDetection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mainCmd="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1739
    const-string v1, "TSP_COMMAND_CMD"

    invoke-static {v1, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1740
    return-void
.end method

.method public hoveringOnOFF(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 1726
    const-string v0, "hover_enable"

    .line 1727
    .local v0, "mainCmd":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1728
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onOffHovering"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mainCmd="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1729
    const-string v1, "TSP_COMMAND_CMD"

    invoke-static {v1, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1730
    return-void
.end method

.method public int_extMEMOSize(I)Ljava/lang/String;
    .locals 14
    .param p1, "type"    # I

    .prologue
    .line 412
    const/4 v3, 0x0

    .line 413
    .local v3, "resData":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v10}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 414
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    aget-object v10, v5, p1

    invoke-virtual {v10}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 415
    .local v2, "path":Ljava/lang/String;
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 416
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCount()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x400

    div-long v6, v10, v12

    .line 417
    .local v6, "totalMass":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x400

    div-long v0, v10, v12

    .line 418
    .local v0, "freeMass":J
    sub-long v8, v6, v0

    .line 419
    .local v8, "usedMass":J
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[Main]TotalMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, FreeMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, UsedMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 422
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "resData="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    return-object v3
.end method

.method public int_extMEMOSize(IZ)Ljava/lang/String;
    .locals 14
    .param p1, "type"    # I
    .param p2, "isByte"    # Z

    .prologue
    .line 427
    const/4 v3, 0x0

    .line 428
    .local v3, "resData":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v10}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 429
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    aget-object v10, v5, p1

    invoke-virtual {v10}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 430
    .local v2, "path":Ljava/lang/String;
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 431
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCount()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long v6, v10, v12

    .line 432
    .local v6, "totalMass":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long v0, v10, v12

    .line 433
    .local v0, "freeMass":J
    sub-long v8, v6, v0

    .line 434
    .local v8, "usedMass":J
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[Main]TotalMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, FreeMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, UsedMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 437
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "resData="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-object v3
.end method

.method public isCompleteFileSystemBuildingNAND()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 932
    const-string v0, ""

    .line 933
    .local v0, "checksum":Ljava/lang/String;
    const/4 v4, 0x0

    .line 934
    .local v4, "result":Z
    const-string v5, "EMMC_CHECKSUM"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 936
    if-eqz v0, :cond_1

    const-string v5, "Unknown"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 939
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 940
    .local v1, "checksumInt":I
    const/4 v3, 0x0

    .line 941
    .local v3, "pass":I
    div-int/lit8 v2, v1, 0x2

    .line 942
    .local v2, "done":I
    rem-int/lit8 v3, v1, 0x2

    .line 943
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isCompleteFileSystemBuildingNAND"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "done="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "pass="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    if-ne v2, v9, :cond_0

    if-ne v3, v9, :cond_0

    .line 947
    const/4 v4, 0x1

    .line 955
    .end local v1    # "checksumInt":I
    .end local v2    # "done":I
    .end local v3    # "pass":I
    :goto_0
    return v4

    .line 949
    .restart local v1    # "checksumInt":I
    .restart local v2    # "done":I
    .restart local v3    # "pass":I
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 952
    .end local v1    # "checksumInt":I
    .end local v2    # "done":I
    .end local v3    # "pass":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isDetectExternalMemory()Z
    .locals 2

    .prologue
    .line 442
    const-string v1, "EXTERNAL_MEMORY_INSERTED"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 444
    .local v0, "state":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 445
    const-string v1, "INSERT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 447
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isEarSwitchPress()Z
    .locals 8

    .prologue
    .line 173
    const/16 v1, 0x64

    .line 174
    .local v1, "interval":I
    const/4 v3, 0x5

    .line 175
    .local v3, "retryCounter":I
    const-string v4, ""

    .line 176
    .local v4, "state":Ljava/lang/String;
    const/4 v2, 0x0

    .line 178
    .local v2, "result":Z
    :goto_0
    if-lez v3, :cond_0

    .line 179
    add-int/lit8 v3, v3, -0x1

    .line 180
    const-string v5, "EARJACK_SWITCH_STATE"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 182
    const-string v5, "1"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 183
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isEarSwitchPress"

    const-string v7, "EARJACK_SWITCH_STATE is pressed"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const/4 v2, 0x1

    .line 196
    :cond_0
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isEarSwitchPress"

    const-string v7, "EARJACK_SWITCH_STATE is released"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return v2

    .line 189
    :cond_1
    const-wide/16 v6, 0x64

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public isExternalMemoryExist()Z
    .locals 2

    .prologue
    .line 344
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, "state":Ljava/lang/String;
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "unmounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInnerMemoryExist()Z
    .locals 2

    .prologue
    .line 315
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKeyPress()Z
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 146
    const/16 v8, 0x9

    new-array v0, v8, [Ljava/lang/String;

    const-string v8, "press"

    aput-object v8, v0, v7

    const-string v8, "pressed"

    aput-object v8, v0, v6

    const/4 v8, 0x2

    const-string v9, "PRESS"

    aput-object v9, v0, v8

    const/4 v8, 0x3

    const-string v9, "PRESSED"

    aput-object v9, v0, v8

    const/4 v8, 0x4

    const-string v9, "1"

    aput-object v9, v0, v8

    const/4 v8, 0x5

    const-string v9, "2"

    aput-object v9, v0, v8

    const/4 v8, 0x6

    const-string v9, "4"

    aput-object v9, v0, v8

    const/4 v8, 0x7

    const-string v9, "7"

    aput-object v9, v0, v8

    const/16 v8, 0x8

    const-string v9, "8"

    aput-object v9, v0, v8

    .line 150
    .local v0, "PRESSED_STRING":[Ljava/lang/String;
    const-string v8, "KEY_PRESSED"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "state":Ljava/lang/String;
    const-string v8, "KEY_PRESSED_POWER"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 152
    .local v5, "state_power":Ljava/lang/String;
    const-string v8, "EARJACK_SWITCH_STATE"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 153
    .local v4, "state_ear":Ljava/lang/String;
    const-string v8, "KEY_PRESSED_3X4"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "state_3x4":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isKeyPress"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "state:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", state_power:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", state_3x4:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", state_ear:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v8, v0

    if-ge v1, v8, :cond_5

    .line 159
    if-eqz v2, :cond_0

    aget-object v8, v0, v1

    invoke-virtual {v8, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    :cond_0
    if-eqz v5, :cond_1

    aget-object v8, v0, v1

    invoke-virtual {v8, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    :cond_1
    if-eqz v4, :cond_2

    aget-object v8, v0, v1

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    if-eqz v3, :cond_4

    aget-object v8, v0, v1

    invoke-virtual {v8, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 164
    :cond_3
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "isKeyPress"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "state:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", state_power:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", state_3x4:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", state_ear:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_1
    return v6

    .line 158
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    move v6, v7

    .line 169
    goto :goto_1
.end method

.method public isMountedStorage(I)Z
    .locals 8
    .param p1, "type"    # I

    .prologue
    const/4 v3, 0x0

    .line 633
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v2

    .line 635
    .local v2, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v4, v2

    if-ge p1, v4, :cond_0

    aget-object v4, v2, p1

    if-nez v4, :cond_1

    .line 636
    :cond_0
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "isMountedStorage"

    const-string v6, "StorageVolumes[type] is null"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :goto_0
    return v3

    .line 640
    :cond_1
    aget-object v4, v2, p1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 641
    .local v0, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "isMountedStorage"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    if-eqz v0, :cond_2

    .line 644
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v3, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 645
    .local v1, "state":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "isMountedStorage"

    const-string v5, "Environment.MEDIA_MOUNTED : mounted"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "isMountedStorage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "state : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    const-string v3, "mounted"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    .line 650
    .end local v1    # "state":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "isMountedStorage"

    const-string v6, "another error"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isSimCardExist()Z
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 719
    const-string v10, "phone"

    invoke-virtual {p0, v10}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    .line 721
    .local v6, "tm":Landroid/telephony/TelephonyManager;
    const-string v10, "SUPPORT_DUAL_STANBY"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "SUPPORT_2ND_RIL"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 723
    :cond_0
    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->productModelName:Ljava/lang/String;

    const-string v11, "CTC"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/factory/modules/ModuleDevice;->productModelName:Ljava/lang/String;

    const-string v11, "ctc"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 724
    :cond_1
    const-string v8, "2"

    const-string v9, "ril.cardnoti"

    const-string v10, "0"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 794
    :cond_2
    :goto_0
    return v2

    .line 726
    :cond_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getFirst()Landroid/telephony/TelephonyManager;

    move-result-object v7

    .line 727
    .local v7, "tm2":Landroid/telephony/TelephonyManager;
    if-nez v7, :cond_4

    .line 728
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "isSimCardExist"

    const-string v11, "tm2 is null"

    invoke-static {v8, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v2, v9

    .line 729
    goto :goto_0

    .line 732
    :cond_4
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v10

    if-ne v10, v8, :cond_6

    :cond_5
    move v8, v9

    :cond_6
    move v2, v8

    goto :goto_0

    .line 741
    .end local v7    # "tm2":Landroid/telephony/TelephonyManager;
    :cond_7
    const-string v10, "SUPPORT_DUALMODE_1CP2RIL"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    const-string v10, "SUPPORT_DUALMODE_2CP2RIL"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 743
    :cond_8
    const/4 v2, 0x0

    .line 744
    .local v2, "bRes":Z
    invoke-static {}, Landroid/telephony/TelephonyManager;->getSecondary()Landroid/telephony/TelephonyManager;

    move-result-object v7

    .line 746
    .restart local v7    # "tm2":Landroid/telephony/TelephonyManager;
    if-eqz v6, :cond_9

    .line 747
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-eq v9, v8, :cond_9

    .line 748
    const/4 v2, 0x1

    .line 751
    :cond_9
    if-eq v2, v8, :cond_a

    if-eqz v7, :cond_a

    .line 752
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-eqz v9, :cond_a

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-eq v9, v8, :cond_a

    .line 753
    const/4 v2, 0x1

    .line 756
    :cond_a
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isSimCardExist"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Dualmode, Res = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 758
    .end local v2    # "bRes":Z
    .end local v7    # "tm2":Landroid/telephony/TelephonyManager;
    :cond_b
    sget-boolean v10, Lcom/sec/factory/modules/ModuleDevice;->isSPRD:Z

    if-nez v10, :cond_c

    sget-boolean v10, Lcom/sec/factory/modules/ModuleDevice;->isHawai:Z

    if-nez v10, :cond_c

    sget-boolean v10, Lcom/sec/factory/modules/ModuleDevice;->isCapri:Z

    if-nez v10, :cond_c

    sget-boolean v10, Lcom/sec/factory/modules/ModuleDevice;->isJava:Z

    if-nez v10, :cond_c

    sget-boolean v10, Lcom/sec/factory/modules/ModuleDevice;->isEOS2:Z

    if-eqz v10, :cond_10

    .line 759
    :cond_c
    const-string v10, "ril.ICC_TYPE"

    const-string v11, "0"

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 760
    .local v0, "Simcard_1":Ljava/lang/String;
    const-string v10, "ril.ICC_TYPE2"

    const-string v11, "0"

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 761
    .local v1, "Simcard_2":Ljava/lang/String;
    const-string v10, "ril.MSIMM"

    const-string v11, "0"

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 763
    .local v3, "masterSimMode":Ljava/lang/String;
    const-string v10, "0"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    const-string v10, "0"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    :cond_d
    const-string v10, "1"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    const-string v10, "0"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_f

    :cond_e
    move v2, v8

    .line 764
    goto/16 :goto_0

    :cond_f
    move v2, v9

    .line 766
    goto/16 :goto_0

    .line 768
    .end local v0    # "Simcard_1":Ljava/lang/String;
    .end local v1    # "Simcard_2":Ljava/lang/String;
    .end local v3    # "masterSimMode":Ljava/lang/String;
    :cond_10
    const-string v10, "IS_MULTI_SIM_FRAMEWORK"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_12

    const-string v10, "gsm"

    const-string v11, "MODEL_COMMUNICATION_MODE"

    invoke-static {v11}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 770
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "isSimCardExist"

    const-string v11, "Multi SIM Framework && gsm"

    invoke-static {v8, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    const/4 v2, 0x0

    .line 772
    .restart local v2    # "bRes":Z
    const-string v8, "gsm.sim.state"

    invoke-static {v8, v9}, Lcom/sec/factory/dummy/MultiSimManager;->appendPropertySimSlot(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 773
    .local v5, "multiSimState1":Ljava/lang/String;
    const-string v8, "ril.MSIMM"

    const-string v9, "0"

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 774
    .restart local v3    # "masterSimMode":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 775
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isSimCardExist"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multiSimState1 : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v8, "READY"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 777
    const/4 v2, 0x1

    .line 778
    :cond_11
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isSimCardExist2"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multiSimMODE  : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    const-string v8, "SUPPORT_DSDS_MSIMM_CHECK"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "1"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 781
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 785
    .end local v2    # "bRes":Z
    .end local v3    # "masterSimMode":Ljava/lang/String;
    .end local v5    # "multiSimState1":Ljava/lang/String;
    :cond_12
    const-string v10, "ril.MSIMM"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 786
    .local v4, "multiSimState":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v10

    if-eqz v10, :cond_13

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v10

    if-ne v10, v8, :cond_14

    :cond_13
    move v2, v9

    .line 794
    .restart local v2    # "bRes":Z
    :goto_1
    goto/16 :goto_0

    .end local v2    # "bRes":Z
    :cond_14
    move v2, v8

    .line 786
    goto :goto_1
.end method

.method public isSimCardExist2()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 799
    const-string v9, "SUPPORT_DUAL_STANBY"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "SUPPORT_2ND_RIL"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "SUPPORT_DUAL_STANBY_ONE_CP"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    const-string v9, "cdma"

    const-string v10, "MODEL_COMMUNICATION_MODE"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 802
    :cond_0
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice;->productModelName:Ljava/lang/String;

    const-string v10, "CTC"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice;->productModelName:Ljava/lang/String;

    const-string v10, "ctc"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 803
    :cond_1
    const-string v7, "2"

    const-string v8, "ril.cardnoti2"

    const-string v9, "0"

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 851
    :cond_2
    :goto_0
    return v8

    .line 805
    :cond_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getSecondary()Landroid/telephony/TelephonyManager;

    move-result-object v6

    .line 806
    .local v6, "tm2":Landroid/telephony/TelephonyManager;
    if-nez v6, :cond_4

    .line 807
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isSimCardExist"

    const-string v10, "tm2 is null"

    invoke-static {v7, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 811
    :cond_4
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-ne v9, v7, :cond_6

    :cond_5
    move v7, v8

    :cond_6
    move v8, v7

    goto :goto_0

    .line 820
    .end local v6    # "tm2":Landroid/telephony/TelephonyManager;
    :cond_7
    const-string v9, "SUPPORT_DUALMODE"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 821
    const-string v9, "phone"

    invoke-virtual {p0, v9}, Lcom/sec/factory/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 822
    .local v5, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v9

    if-ne v9, v7, :cond_9

    :cond_8
    move v2, v8

    .local v2, "bRes":Z
    :goto_1
    move v8, v2

    .line 824
    goto :goto_0

    .end local v2    # "bRes":Z
    :cond_9
    move v2, v7

    .line 822
    goto :goto_1

    .line 825
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :cond_a
    sget-boolean v9, Lcom/sec/factory/modules/ModuleDevice;->isSPRD:Z

    if-nez v9, :cond_b

    sget-boolean v9, Lcom/sec/factory/modules/ModuleDevice;->isHawai:Z

    if-nez v9, :cond_b

    sget-boolean v9, Lcom/sec/factory/modules/ModuleDevice;->isCapri:Z

    if-nez v9, :cond_b

    sget-boolean v9, Lcom/sec/factory/modules/ModuleDevice;->isJava:Z

    if-nez v9, :cond_b

    sget-boolean v9, Lcom/sec/factory/modules/ModuleDevice;->isEOS2:Z

    if-eqz v9, :cond_e

    .line 826
    :cond_b
    const-string v9, "ril.ICC_TYPE"

    const-string v10, "0"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 827
    .local v0, "Simcard_1":Ljava/lang/String;
    const-string v9, "ril.ICC_TYPE2"

    const-string v10, "0"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 828
    .local v1, "Simcard_2":Ljava/lang/String;
    const-string v9, "ril.MSIMM"

    const-string v10, "0"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 830
    .local v3, "masterSimMode":Ljava/lang/String;
    const-string v9, "0"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    const-string v9, "0"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    :cond_c
    const-string v9, "1"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "0"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    :cond_d
    move v8, v7

    .line 831
    goto/16 :goto_0

    .line 835
    .end local v0    # "Simcard_1":Ljava/lang/String;
    .end local v1    # "Simcard_2":Ljava/lang/String;
    .end local v3    # "masterSimMode":Ljava/lang/String;
    :cond_e
    const-string v9, "IS_MULTI_SIM_FRAMEWORK"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "gsm"

    const-string v10, "MODEL_COMMUNICATION_MODE"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 837
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isSimCardExist2"

    const-string v10, "Multi SIM Framework && gsm"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/4 v2, 0x0

    .line 839
    .restart local v2    # "bRes":Z
    const-string v8, "gsm.sim.state"

    invoke-static {v8, v7}, Lcom/sec/factory/dummy/MultiSimManager;->appendPropertySimSlot(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 840
    .local v4, "multiSimState2":Ljava/lang/String;
    const-string v7, "ril.MSIMM"

    const-string v8, "0"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 841
    .restart local v3    # "masterSimMode":Ljava/lang/String;
    if-eqz v4, :cond_10

    .line 842
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "isSimCardExist2"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " multiSimState2 : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    const-string v7, "READY"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 844
    const/4 v2, 0x1

    .line 845
    :cond_f
    const-string v7, "SUPPORT_DSDS_MSIMM_CHECK"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_10

    const-string v7, "1"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 847
    const/4 v2, 0x1

    :cond_10
    move v8, v2

    .line 849
    goto/16 :goto_0
.end method

.method public isUltrasonicCheckPass()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1165
    const/4 v0, 0x0

    .line 1166
    .local v0, "isfwResult":Z
    const-string v3, "ULTRASONIC_VER_CHECK"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1167
    .local v2, "tempResult":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1168
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1169
    .local v1, "result":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 1170
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "isUltrasonicCheckPass"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result[0]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", result[1]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    const-string v3, "5"

    aget-object v4, v1, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "0"

    aget-object v4, v1, v8

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1174
    const/4 v0, 0x1

    .line 1182
    .end local v1    # "result":[Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 1176
    .restart local v1    # "result":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1179
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVibrating()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    return v0
.end method

.method public mainMEMOSize()Ljava/lang/String;
    .locals 14

    .prologue
    const-wide/16 v12, 0x400

    .line 351
    const/4 v2, 0x0

    .line 352
    .local v2, "resData":Ljava/lang/String;
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 353
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v10, v10

    mul-long/2addr v8, v10

    div-long v4, v8, v12

    .line 354
    .local v4, "totalMass":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v10, v10

    mul-long/2addr v8, v10

    div-long v0, v8, v12

    .line 355
    .local v0, "freeMass":J
    sub-long v6, v4, v0

    .line 356
    .local v6, "usedMass":J
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mainMEMOSize"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Main]TotalMass : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "KB, FreeMass : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "KB, UsedMass : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "KB"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 359
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mainMEMOSize"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resData="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return-object v2
.end method

.method public readIdChipDetect()I
    .locals 7

    .prologue
    .line 2132
    const-string v3, "ID_DETECT_TEST"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2133
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "ID_DETECT_TEST"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2135
    const/4 v0, -0x1

    .line 2137
    .local v0, "iValue":I
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2141
    :goto_0
    return v0

    .line 2138
    :catch_0
    move-exception v1

    .line 2139
    .local v1, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public readIdChipVerify()I
    .locals 7

    .prologue
    .line 2116
    const-string v3, "ID_CHIP_VERIFY"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2117
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readIdChipVerify"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    const/4 v0, -0x1

    .line 2121
    .local v0, "iValue":I
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2125
    :goto_0
    return v0

    .line 2122
    :catch_0
    move-exception v1

    .line 2123
    .local v1, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public readModuleBinVersion(I)Ljava/lang/String;
    .locals 9
    .param p1, "module"    # I

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    .line 1092
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readModuleBinVersion"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "module="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    const/4 v1, 0x0

    .line 1094
    .local v1, "sysfsid":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1096
    .local v2, "version":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 1108
    :goto_0
    :pswitch_0
    if-ne p1, v7, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    const-string v4, "ATMEL"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    const-string v4, "ZINITIX"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1111
    const-string v3, "get_fw_ver_bin"

    invoke-virtual {p0, v3}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1135
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readModuleBinVersion"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "version="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    .line 1136
    :goto_2
    return-object v3

    .line 1100
    :pswitch_1
    const-string v1, "TSK_FIRMWARE_VERSION_PHONE"

    .line 1101
    goto :goto_0

    .line 1104
    :pswitch_2
    const-string v1, "EPEN_FIRMWARE_VERSION"

    goto :goto_0

    .line 1112
    :cond_1
    const/4 v3, 0x3

    if-eq p1, v3, :cond_2

    if-ne p1, v8, :cond_4

    .line 1113
    :cond_2
    if-ne p1, v8, :cond_3

    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleDevice;->isEpenChecksumPass()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1114
    const-string v3, "N"

    goto :goto_2

    .line 1118
    :cond_3
    :try_start_0
    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1119
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readModuleFirmwareVersion"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "version.split \'\t\' [0]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readModuleFirmwareVersion"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "version.split \'\t\' [1]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v2, v3, v4

    .line 1124
    const-string v3, "0x"

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto/16 :goto_1

    .line 1125
    :catch_0
    move-exception v0

    .line 1126
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1127
    const-string v2, "N"

    .line 1128
    goto/16 :goto_1

    .line 1130
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    if-eqz v1, :cond_0

    .line 1131
    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1096
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public readModuleFirmwareVersion(I)Ljava/lang/String;
    .locals 10
    .param p1, "module"    # I

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x1

    .line 1014
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readModuleFirmwareVersion"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "module="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    const/4 v2, 0x0

    .line 1016
    .local v2, "sysfsid":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1017
    .local v3, "version":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1019
    .local v0, "data":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 1045
    :goto_0
    :pswitch_0
    if-ne p1, v8, :cond_1

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    const-string v5, "ATMEL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    const-string v5, "ZINITIX"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1048
    const-string v4, "get_fw_ver_ic"

    invoke-virtual {p0, v4}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1087
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readModuleFirmwareVersion"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 1088
    :goto_2
    return-object v4

    .line 1023
    :pswitch_1
    const-string v2, "TSK_FIRMWARE_VERSION_PANEL"

    .line 1024
    goto :goto_0

    .line 1028
    :pswitch_2
    const-string v2, "EPEN_FIRMWARE_VERSION"

    .line 1029
    goto :goto_0

    .line 1032
    :pswitch_3
    const-string v2, "SENSORHUB_FIRMWARE_VERSION"

    .line 1033
    goto :goto_0

    .line 1035
    :pswitch_4
    const-string v2, "BARCODE_EMUL_FIRMWARE_VERSION"

    .line 1036
    goto :goto_0

    .line 1038
    :pswitch_5
    const-string v2, "ULTRASONIC_VER_CHECK"

    .line 1039
    goto :goto_0

    .line 1041
    :pswitch_6
    const-string v2, "CAMERA_REAR_OIS_FW_VER"

    goto :goto_0

    .line 1049
    :cond_1
    const/4 v4, 0x3

    if-eq p1, v4, :cond_2

    if-ne p1, v9, :cond_4

    .line 1050
    :cond_2
    if-ne p1, v9, :cond_3

    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleDevice;->isEpenChecksumPass()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1051
    const-string v4, "NG_CS"

    goto :goto_2

    .line 1055
    :cond_3
    :try_start_0
    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1056
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readModuleFirmwareVersion"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "version.split \'\t\' [0]="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readModuleFirmwareVersion"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "version.split \'\t\' [1]="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    const-string v4, "\t"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v3, v4, v5

    .line 1061
    const-string v4, "0x"

    const-string v5, "0"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto/16 :goto_1

    .line 1062
    :catch_0
    move-exception v1

    .line 1063
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1064
    const-string v3, "NG"

    .line 1065
    goto/16 :goto_1

    .line 1066
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    const/4 v4, 0x7

    if-ne p1, v4, :cond_6

    .line 1067
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDevice;->isUltrasonicCheckPass()Z

    move-result v4

    if-ne v4, v8, :cond_5

    .line 1069
    :try_start_1
    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1070
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1071
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readModuleFirmwareVersion"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "data[0]="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v0, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data[1]="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 1073
    :catch_1
    move-exception v1

    .line 1074
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1075
    const-string v3, "NG"

    .line 1076
    goto/16 :goto_1

    .line 1078
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    const-string v3, "NG"

    goto/16 :goto_1

    .line 1082
    :cond_6
    if-eqz v2, :cond_0

    .line 1083
    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1019
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public readModuleUpdateStatus(I)Ljava/lang/String;
    .locals 7
    .param p1, "module"    # I

    .prologue
    .line 992
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readModuleStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "module="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    const/4 v2, 0x0

    .line 995
    .local v2, "sysfsid":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 1003
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    .line 1004
    .local v0, "mStatus":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1005
    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1007
    :cond_0
    move-object v1, v0

    .line 1009
    .local v1, "status":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readModuleStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    return-object v1

    .line 999
    .end local v0    # "mStatus":Ljava/lang/String;
    .end local v1    # "status":Ljava/lang/String;
    :pswitch_1
    const-string v2, "TSK_FIRMWARE_UPDATE_STATUS"

    goto :goto_0

    .line 995
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public readSensorHubResult()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2084
    const-string v1, "SENSORHUB_MCU"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2085
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readSensorHubResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2086
    return-object v0
.end method

.method public readSerialNo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 905
    const-string v0, "SERIAL_NO"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readTSKSensitivity(I)Ljava/lang/Integer;
    .locals 5
    .param p1, "keyCode"    # I

    .prologue
    .line 1579
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readTSKSensitivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keyCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    const/4 v0, 0x0

    .line 1582
    .local v0, "value":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 1617
    :goto_0
    const-string v1, "SUBKEY_FIVE_KEY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1618
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readTSKSensitivity"

    const-string v3, "fivekey"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1619
    packed-switch p1, :pswitch_data_0

    .line 1634
    :cond_0
    :goto_1
    :pswitch_0
    const-string v1, "SUBKEY_SIX_KEY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1635
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readTSKSensitivity"

    const-string v3, "sixkey"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1636
    packed-switch p1, :pswitch_data_1

    .line 1654
    :cond_1
    :goto_2
    :pswitch_1
    const-string v1, "SUBKEY_FOUR_KEY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1655
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readTSKSensitivity"

    const-string v3, "fourkey"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1656
    packed-switch p1, :pswitch_data_2

    .line 1668
    :cond_2
    :goto_3
    :pswitch_2
    if-eqz v0, :cond_4

    .line 1669
    new-instance v1, Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    .line 1672
    :goto_4
    return-object v1

    .line 1584
    :sswitch_0
    const-string v1, "TOUCH_KEY_SENSITIVITY_MENU"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1585
    goto :goto_0

    .line 1588
    :sswitch_1
    const-string v1, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1589
    const-string v1, "TOUCH_KEY_SENSITIVITY_HOME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1591
    :cond_3
    const-string v1, "TOUCH_KEY_SENSITIVITY_OK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1594
    goto :goto_0

    .line 1596
    :sswitch_2
    const-string v1, "TOUCH_KEY_SENSITIVITY_BACK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1597
    goto :goto_0

    .line 1599
    :sswitch_3
    const-string v1, "TOUCH_KEY_SENSITIVITY_SEARCH"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1600
    goto :goto_0

    .line 1602
    :sswitch_4
    const-string v1, "TOUCH_KEY_SENSITIVITY_APP_SWITCH"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1603
    goto :goto_0

    .line 1608
    :sswitch_5
    const-string v1, "TOUCH_KEY_SENSITIVITY_RECENT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1609
    goto :goto_0

    .line 1611
    :sswitch_6
    const-string v1, "TOUCH_KEY_SENSITIVITY_REF1"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1612
    goto/16 :goto_0

    .line 1614
    :sswitch_7
    const-string v1, "TOUCH_KEY_SENSITIVITY_REF2"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1621
    :pswitch_3
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN1"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1622
    goto/16 :goto_1

    .line 1624
    :pswitch_4
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN2"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1625
    goto/16 :goto_1

    .line 1627
    :pswitch_5
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN3"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1628
    goto/16 :goto_1

    .line 1638
    :pswitch_6
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN1"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1639
    goto/16 :goto_2

    .line 1641
    :pswitch_7
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN2"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1642
    goto/16 :goto_2

    .line 1644
    :pswitch_8
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN3"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1645
    goto/16 :goto_2

    .line 1647
    :pswitch_9
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN4"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1648
    goto/16 :goto_2

    .line 1658
    :pswitch_a
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN1"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1659
    goto/16 :goto_3

    .line 1661
    :pswitch_b
    const-string v1, "TOUCH_KEY_SENSITIVITY_HIDDEN2"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1662
    goto/16 :goto_3

    .line 1672
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 1582
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x52 -> :sswitch_0
        0x54 -> :sswitch_3
        0xbb -> :sswitch_4
        0xdf -> :sswitch_5
        0xfe -> :sswitch_6
        0xff -> :sswitch_7
    .end sparse-switch

    .line 1619
    :pswitch_data_0
    .packed-switch 0x11a
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1636
    :pswitch_data_1
    .packed-switch 0x11a
        :pswitch_6
        :pswitch_1
        :pswitch_1
        :pswitch_9
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1656
    :pswitch_data_2
    .packed-switch 0x11a
        :pswitch_a
        :pswitch_2
        :pswitch_2
        :pswitch_b
    .end packed-switch
.end method

.method public readTSKSensitivity_Dual(II)Ljava/lang/Integer;
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "path"    # I

    .prologue
    .line 1676
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readTSKSensitivity_Dual"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keyCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1677
    const/4 v0, 0x0

    .line 1679
    .local v0, "value":Ljava/lang/String;
    if-nez p2, :cond_0

    .line 1680
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/ModuleDevice;->readTSKSensitivity(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1719
    :goto_0
    return-object v1

    .line 1681
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 1682
    sparse-switch p1, :sswitch_data_0

    .line 1715
    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    .line 1716
    new-instance v1, Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0

    .line 1684
    :sswitch_0
    const-string v1, "TOUCH_KEY_SENSITIVITY_BACK_INNER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1685
    goto :goto_1

    .line 1687
    :sswitch_1
    const-string v1, "TOUCH_KEY_SENSITIVITY_RECENT_INNER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1688
    goto :goto_1

    .line 1692
    :sswitch_2
    const-string v1, "TOUCH_KEY_SENSITIVITY_RECENT_INNER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1693
    goto :goto_1

    .line 1697
    :cond_2
    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    .line 1698
    sparse-switch p1, :sswitch_data_1

    goto :goto_1

    .line 1700
    :sswitch_3
    const-string v1, "TOUCH_KEY_SENSITIVITY_BACK_OUTER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1701
    goto :goto_1

    .line 1703
    :sswitch_4
    const-string v1, "TOUCH_KEY_SENSITIVITY_RECENT_OUTER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1704
    goto :goto_1

    .line 1708
    :sswitch_5
    const-string v1, "TOUCH_KEY_SENSITIVITY_RECENT_OUTER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1709
    goto :goto_1

    .line 1719
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1682
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0xbb -> :sswitch_1
        0xdf -> :sswitch_2
    .end sparse-switch

    .line 1698
    :sswitch_data_1
    .sparse-switch
        0x4 -> :sswitch_3
        0xbb -> :sswitch_4
        0xdf -> :sswitch_5
    .end sparse-switch
.end method

.method public readTSKThreshold()I
    .locals 6

    .prologue
    .line 1528
    const-string v2, "TOUCH_KEY_THRESHOLD"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1529
    .local v0, "s":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1530
    .local v1, "threshold":I
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readTSKThreshold"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "threshold="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    return v1

    .line 1529
    .end local v1    # "threshold":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public readTSKThresholdMulti()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1535
    const-string v1, "TOUCH_KEY_THRESHOLD"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1536
    .local v0, "s":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readTSKThresholdMulti"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "threshold="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1537
    return-object v0
.end method

.method public readTSKThreshold_Dual(II)I
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "path"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1541
    const/4 v1, 0x0

    .line 1543
    .local v1, "threshold":I
    const-string v3, "USE_DIFFERENT_THD"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1544
    const/4 v0, 0x0

    .line 1545
    .local v0, "s":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 1565
    const-string v3, "TOUCH_KEY_THRESHOLD"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1566
    if-eqz v0, :cond_6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1567
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readTSKThreshold_Dual"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "threshold="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    :goto_1
    return v1

    .line 1547
    :sswitch_0
    if-ne p2, v4, :cond_1

    .line 1548
    const-string v3, "TOUCH_KEY_THRESHOLD_BACK_INNER"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1552
    :cond_0
    :goto_2
    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1553
    :goto_3
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readTSKThreshold_Dual"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Back threshold="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1549
    :cond_1
    if-ne p2, v5, :cond_0

    .line 1550
    const-string v3, "TOUCH_KEY_THRESHOLD_BACK_OUTER"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v1, v2

    .line 1552
    goto :goto_3

    .line 1556
    :sswitch_1
    if-ne p2, v4, :cond_4

    .line 1557
    const-string v3, "TOUCH_KEY_THRESHOLD_RECENT_INNER"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1561
    :cond_3
    :goto_4
    if-eqz v0, :cond_5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1562
    :goto_5
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readTSKThreshold_Dual"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Recent threshold="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1558
    :cond_4
    if-ne p2, v5, :cond_3

    .line 1559
    const-string v3, "TOUCH_KEY_THRESHOLD_RECENT_OUTER"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_5
    move v1, v2

    .line 1561
    goto :goto_5

    :cond_6
    move v1, v2

    .line 1566
    goto/16 :goto_0

    .line 1571
    .end local v0    # "s":Ljava/lang/String;
    :cond_7
    const-string v3, "TOUCH_KEY_THRESHOLD"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1572
    .restart local v0    # "s":Ljava/lang/String;
    if-eqz v0, :cond_8

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1573
    :goto_6
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readTSKThreshold_Dual"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "threshold="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    move v1, v2

    .line 1572
    goto :goto_6

    .line 1545
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0xbb -> :sswitch_1
    .end sparse-switch
.end method

.method public readiSerialNo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 909
    const-string v0, "ISERIAL_NO"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public runOTGTest()V
    .locals 6

    .prologue
    .line 1223
    const-string v3, "eng"

    sget-object v4, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1224
    .local v0, "IS_ENG_BUILD":Z
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "runOTGTest"

    const-string v5, "Start OTG Test"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.factory.intent.ACTION_OTG_RESPONSE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1227
    .local v2, "response":Landroid/content/Intent;
    const-string v3, "OTG_MUIC_SET"

    sget-object v4, Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_ON:[B

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1228
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "runOTGTest"

    const-string v5, "Set OTG MUIC success!"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1233
    :goto_0
    const-string v3, "OTG_TEST_MODE"

    sget-object v4, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_ON:[B

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1234
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "runOTGTest"

    const-string v5, "OTG TEST ON"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    new-instance v1, Lcom/sec/factory/modules/ModuleDevice$2;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDevice;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3, v2}, Lcom/sec/factory/modules/ModuleDevice$2;-><init>(Lcom/sec/factory/modules/ModuleDevice;Landroid/os/Looper;Landroid/content/Intent;)V

    .line 1261
    .local v1, "handler":Landroid/os/Handler;
    const/4 v3, 0x0

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1262
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "runOTGTest"

    const-string v5, "Wait 2sec..."

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    .end local v1    # "handler":Landroid/os/Handler;
    :goto_1
    return-void

    .line 1230
    :cond_0
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "runOTGTest"

    const-string v5, "Set OTG MUIC fail!"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1264
    :cond_1
    const-string v3, "result"

    const-string v4, "NG"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1265
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public runOTGTestInThisThread()Z
    .locals 6

    .prologue
    .line 1270
    const/4 v1, 0x0

    .line 1271
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "Start OTG Test"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    const-string v2, "OTG_MUIC_SET"

    sget-object v3, Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_ON:[B

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1274
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "Set OTG MUIC success!"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    :goto_0
    const-string v2, "OTG_TEST_MODE"

    sget-object v3, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_ON:[B

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1280
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "OTG TEST ON"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1282
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "Wait 2sec..."

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1284
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1289
    :goto_1
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "OTG Test Finish"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    const-string v2, "ON"

    const-string v3, "OTG_TEST_MODE"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    .line 1292
    :goto_2
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OTG Test Result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    if-eqz v1, :cond_4

    .line 1295
    const-string v2, "OTG_TEST_MODE"

    sget-object v3, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_OFF:[B

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1296
    const/4 v1, 0x1

    .line 1304
    :goto_3
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "OTG TEST OFF"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OTG Test Result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    const-string v2, "OTG_MUIC_SET"

    sget-object v3, Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_OFF:[B

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    .line 1309
    :cond_0
    return v1

    .line 1276
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runOTGTestInThisThread"

    const-string v4, "Set OTG MUIC fail!"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1285
    :catch_0
    move-exception v0

    .line 1286
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1290
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 1298
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    .line 1301
    :cond_4
    const-string v2, "OTG_TEST_MODE"

    sget-object v3, Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_OFF:[B

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    goto :goto_3
.end method

.method public setLEDlamp(I)Z
    .locals 8
    .param p1, "state"    # I

    .prologue
    .line 1752
    const-string v4, "SVC_LED_TEST_BRIGHTNESS"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1753
    .local v1, "LED_ON":Ljava/lang/String;
    const-string v0, "0"

    .line 1754
    .local v0, "LED_OFF":Ljava/lang/String;
    const-string v2, "0xFFFFFF"

    .line 1755
    .local v2, "WHITE_LED_ON":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1757
    .local v3, "testResult":Z
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "setLEDlamp"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "state="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1759
    packed-switch p1, :pswitch_data_0

    .line 1795
    :goto_0
    return v3

    .line 1761
    :pswitch_0
    const-string v4, "LED_RED"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1762
    const-string v4, "LED_GREEN"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1763
    const-string v4, "LED_BLUE"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1764
    goto :goto_0

    .line 1767
    :pswitch_1
    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isAPQ8064:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isAPQ8074:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isAPQ8084:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8960:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8930:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8260A:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8974:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8928:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8228:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8674:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isMSM8274:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isPegasus:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isPegasusPrime:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/sec/factory/modules/ModuleDevice;->isEXYNOS5410:Z

    if-eqz v4, :cond_1

    .line 1770
    :cond_0
    const-string v4, "LED_BLINK"

    invoke-static {v4, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 1772
    :cond_1
    const-string v4, "LED_RED"

    invoke-static {v4, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1773
    const-string v4, "LED_GREEN"

    invoke-static {v4, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1774
    const-string v4, "LED_BLUE"

    invoke-static {v4, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1777
    goto :goto_0

    .line 1779
    :pswitch_2
    const-string v4, "LED_GREEN"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1780
    const-string v4, "LED_BLUE"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1781
    const-string v4, "LED_RED"

    invoke-static {v4, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1782
    goto :goto_0

    .line 1784
    :pswitch_3
    const-string v4, "LED_RED"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1785
    const-string v4, "LED_BLUE"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1786
    const-string v4, "LED_GREEN"

    invoke-static {v4, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1787
    goto/16 :goto_0

    .line 1789
    :pswitch_4
    const-string v4, "LED_RED"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1790
    const-string v4, "LED_GREEN"

    invoke-static {v4, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1791
    const-string v4, "LED_BLUE"

    invoke-static {v4, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    goto/16 :goto_0

    .line 1759
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setLPAmode(Ljava/lang/String;)V
    .locals 5
    .param p1, "lpaMode"    # Ljava/lang/String;

    .prologue
    .line 2065
    const-string v1, "LPA_MODE_SET"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2066
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLpaMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lpaMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2067
    return-void
.end method

.method public setLPMmode(Ljava/lang/String;)V
    .locals 5
    .param p1, "lpmMode"    # Ljava/lang/String;

    .prologue
    .line 2070
    const-string v1, "LPM_MODE_SET"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2071
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLpmMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lpmMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2072
    return-void
.end method

.method public setRadioSimSlot(II)Z
    .locals 12
    .param p1, "slot"    # I
    .param p2, "on"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 868
    const-string v0, "SimSlotOnOff"

    .line 872
    .local v0, "STR_METHOD_SIM_SLOT_ONOFF":Ljava/lang/String;
    const-string v7, "SUPPORT_DUAL_STANBY"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "SUPPORT_2ND_RIL"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 873
    :cond_0
    if-nez p1, :cond_2

    .line 874
    const-string v7, "phone"

    invoke-static {v7}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v4

    .line 879
    .local v4, "phone":Lcom/android/internal/telephony/ITelephony;
    :goto_0
    if-eqz v4, :cond_1

    .line 882
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "SimSlotOnOff"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 883
    .local v2, "method":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    .line 886
    .local v3, "oParam":[Ljava/lang/Object;
    :try_start_1
    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move v5, v6

    .line 898
    .end local v2    # "method":Ljava/lang/reflect/Method;
    .end local v3    # "oParam":[Ljava/lang/Object;
    .end local v4    # "phone":Lcom/android/internal/telephony/ITelephony;
    :cond_1
    :goto_1
    return v5

    .line 876
    :cond_2
    const-string v7, "phone2"

    invoke-static {v7}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v4

    .restart local v4    # "phone":Lcom/android/internal/telephony/ITelephony;
    goto :goto_0

    .line 888
    .restart local v2    # "method":Ljava/lang/reflect/Method;
    .restart local v3    # "oParam":[Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 889
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_2
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "setRadioSimSlot"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "method invoke failed - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 892
    .end local v1    # "e":Ljava/lang/Throwable;
    .end local v2    # "method":Ljava/lang/reflect/Method;
    .end local v3    # "oParam":[Ljava/lang/Object;
    :catch_1
    move-exception v1

    .line 893
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "setRadioSimSlot"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No method exception - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setReferenceDataCount(I)Z
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 2094
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setReferenceDataCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095
    const-string v0, "sys.factory.setReferenceData"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096
    const/4 v0, 0x1

    return v0
.end method

.method public setUSBMode(I)Z
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDevice;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "usb_setting_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1200
    const-string v1, "USB_MENU_SEL"

    if-nez p1, :cond_0

    const-string v0, "0"

    :goto_0
    invoke-static {v1, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "1"

    goto :goto_0
.end method

.method public startIRLedSignal(Ljava/lang/String;I)Z
    .locals 11
    .param p1, "control"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    const/4 v7, 0x0

    .line 1895
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "startIRLedSignal"

    invoke-static {v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1896
    const/4 v5, 0x0

    .line 1898
    .local v5, "out":Ljava/io/FileOutputStream;
    const-string v8, "IR_LED_SEND"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v1

    .line 1899
    .local v1, "isExistCTRL":Z
    const-string v8, "PEEL"

    const-string v9, "SUPPORT_IRLED_CONCEPT"

    invoke-static {v9}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 1901
    .local v3, "isPeel":Z
    if-nez v1, :cond_1

    if-nez v3, :cond_1

    .line 1940
    :cond_0
    :goto_0
    return v7

    .line 1905
    :cond_1
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    if-eqz v8, :cond_2

    .line 1906
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDevice;->stopIRLedSignal()Z

    .line 1909
    :cond_2
    const-string v8, "IR_LED_SEND_TEST"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v2

    .line 1910
    .local v2, "isExistONOFF":Z
    if-eqz v2, :cond_3

    .line 1911
    const-string v8, "IR_LED_SEND_TEST"

    const-string v9, "38400,10"

    invoke-static {v8, v9}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1915
    :cond_3
    const-string v8, "IS_IRLED_TEST_SPLIT_COMMAND"

    invoke-static {v8}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    .line 1918
    .local v4, "isSplitCmd":Z
    if-eqz v4, :cond_4

    .line 1920
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    const-string v8, "IR_LED_SEND"

    invoke-direct {v6, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5    # "out":Ljava/io/FileOutputStream;
    .local v6, "out":Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 1925
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "out":Ljava/io/FileOutputStream;
    :goto_1
    if-eqz v5, :cond_0

    .line 1926
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;

    invoke-direct {v9, p0, p1, p2, v5}, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;-><init>(Lcom/sec/factory/modules/ModuleDevice;Ljava/lang/String;ILjava/io/FileOutputStream;)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    .line 1927
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    .line 1936
    :goto_2
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->isAlive()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1937
    const/4 v7, 0x1

    goto :goto_0

    .line 1921
    :catch_0
    move-exception v0

    .line 1922
    .local v0, "fe":Ljava/io/FileNotFoundException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1932
    .end local v0    # "fe":Ljava/io/FileNotFoundException;
    :cond_4
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;

    const/4 v10, 0x0

    invoke-direct {v9, p0, p1, p2, v10}, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;-><init>(Lcom/sec/factory/modules/ModuleDevice;Ljava/lang/String;ILjava/io/FileOutputStream;)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    .line 1933
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto :goto_2
.end method

.method public startSensorHubTest()V
    .locals 3

    .prologue
    .line 2079
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startSensorHbTest"

    const-string v2, "Start sensorhub test"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2080
    const-string v0, "SENSORHUB_MCU"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2081
    return-void
.end method

.method public startTSPConnectionTest()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1488
    const-string v0, ""

    .line 1489
    .local v0, "status":Ljava/lang/String;
    const-string v1, "TSP_COMMAND_CONNECTION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1491
    if-nez v0, :cond_0

    .line 1492
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "tspConnectionTest"

    const-string v3, "Fail status = null"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    const-string v1, "NG"

    .line 1499
    :goto_0
    return-object v1

    .line 1494
    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1495
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "tspConnectionTest"

    const-string v3, "Success status = OK"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1496
    const-string v1, "OK"

    goto :goto_0

    .line 1498
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startTSPTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail - Status : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    const-string v1, "NG"

    goto :goto_0
.end method

.method public startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;
    .locals 10
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "x"    # I

    .prologue
    const/16 v7, 0xa

    const/4 v9, 0x0

    .line 1443
    const-string v6, "get_x_num"

    invoke-virtual {p0, v6}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1445
    .local v4, "temp":Ljava/lang/String;
    const-string v6, "NG"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1446
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "startTSPReadTest"

    const-string v8, "error - TSP_CMD_X_COUNT"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    const-string v6, "NG"

    .line 1484
    :goto_0
    return-object v6

    .line 1449
    :cond_0
    invoke-static {v4, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 1453
    .local v0, "X_AXIS_MAX":I
    const-string v6, "get_y_num"

    invoke-virtual {p0, v6}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1455
    const-string v6, "NG"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1456
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "startTSPReadTest"

    const-string v8, "error - TSP_CMD_Y_COUNT"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    const-string v6, "NG"

    goto :goto_0

    .line 1459
    :cond_1
    invoke-static {v4, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 1463
    .local v1, "Y_AXIS_MAX":I
    const-string v2, ""

    .line 1465
    .local v2, "result":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDevice;->mTSPVendorName:Ljava/lang/String;

    const-string v7, "SYNAPTICS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1466
    const/4 v5, 0x0

    .local v5, "y":I
    :goto_1
    if-ge v5, v1, :cond_5

    .line 1467
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1468
    .local v3, "strCMD":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1466
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1471
    .end local v3    # "strCMD":Ljava/lang/String;
    .end local v5    # "y":I
    :cond_2
    if-ge p2, v0, :cond_4

    .line 1472
    const/4 v5, 0x0

    .restart local v5    # "y":I
    :goto_2
    if-ge v5, v1, :cond_3

    .line 1473
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1474
    .restart local v3    # "strCMD":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1472
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1477
    .end local v3    # "strCMD":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v2, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 1479
    .end local v5    # "y":I
    :cond_4
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "startTSPReadTest"

    const-string v8, "error - X_AXIS >= X_AXIS_MAX"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    const-string v6, "NG"

    goto/16 :goto_0

    .line 1484
    .restart local v5    # "y":I
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v2, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0
.end method

.method public startTSPTest(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 1389
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startTSPTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cmd name => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 1391
    .local v0, "byteCMD":[B
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public startVibrate()V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startVibrate"

    const-string v2, "Start Vibrate pattern={0, 5000}"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    const/4 v1, 0x2

    new-array v1, v1, [J

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrate([JI)V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    .line 231
    return-void

    .line 227
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method public startVibrate(J)V
    .locals 11
    .param p1, "milliseconds"    # J

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 234
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "startVibrate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start Vibrate : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sec"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-boolean v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    if-nez v3, :cond_0

    .line 240
    const-string v3, "IS_VIBETONZ_UNSUPPORTED"

    invoke-static {v3}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, p1, v4

    if-nez v3, :cond_1

    .line 242
    const-string v3, "IS_VIBETONZ_UNSUPPORTED"

    invoke-static {v3}, Lcom/sec/factory/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "patternValues":[Ljava/lang/String;
    aget-object v3, v0, v8

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 244
    .local v1, "turnOnTime":I
    aget-object v3, v0, v9

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 245
    .local v2, "vibeDurationTime":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    const/4 v4, 0x2

    new-array v4, v4, [J

    int-to-long v6, v1

    aput-wide v6, v4, v8

    int-to-long v6, v2

    aput-wide v6, v4, v9

    invoke-virtual {v3, v4, v8}, Landroid/os/SystemVibrator;->vibrate([JI)V

    .line 252
    .end local v0    # "patternValues":[Ljava/lang/String;
    .end local v1    # "turnOnTime":I
    .end local v2    # "vibeDurationTime":I
    :goto_0
    iput-boolean v9, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    .line 254
    :cond_0
    return-void

    .line 249
    :cond_1
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v3, p1, p2}, Landroid/os/SystemVibrator;->vibrate(J)V

    goto :goto_0
.end method

.method public startVibrate_dual_motor(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/16 v2, 0x2710

    .line 257
    iget v0, p0, Lcom/sec/factory/modules/ModuleDevice;->left_motor:I

    if-ne p1, v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mLeftPattern:[B

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 259
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startVibrate_dual_motor"

    const-string v2, "Start Vibrate for left"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    .line 266
    return-void

    .line 261
    :cond_1
    iget v0, p0, Lcom/sec/factory/modules/ModuleDevice;->right_motor:I

    if-ne p1, v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->mRightPattern:[B

    invoke-virtual {v0, v1, v2}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 263
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "startVibrate_dual_motor"

    const-string v2, "Start Vibrate for Right"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopIRLedSignal()Z
    .locals 6

    .prologue
    .line 1944
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "stopIRLedSignal"

    invoke-static {v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1945
    const/16 v0, 0x14

    .line 1947
    .local v0, "counter":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1948
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    .line 1951
    :cond_0
    :goto_0
    sget-boolean v3, Lcom/sec/factory/modules/ModuleDevice;->mRunningIRLedThread:Z

    if-eqz v3, :cond_2

    .line 1952
    add-int/lit8 v0, v0, -0x1

    .line 1953
    if-gez v0, :cond_1

    .line 1954
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "stopIRLedSignal"

    const-string v5, "Can not stop IRLED Thread"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1955
    const/4 v3, 0x0

    .line 1972
    :goto_1
    return v3

    .line 1959
    :cond_1
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1960
    :catch_0
    move-exception v2

    .line 1961
    .local v2, "localInterruptedException2":Ljava/lang/InterruptedException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 1965
    .end local v2    # "localInterruptedException2":Ljava/lang/InterruptedException;
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;

    .line 1967
    const-string v3, "IR_LED_SEND_TEST"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v1

    .line 1968
    .local v1, "isExistONOFF":Z
    if-eqz v1, :cond_3

    .line 1969
    const-string v3, "IR_LED_SEND_TEST"

    const-string v4, "38400,5"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1972
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public stopVibrate()V
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "stopVibrate"

    const-string v2, "Stop Vibrate"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDevice;->mIsVibrating:Z

    .line 275
    :cond_0
    return-void
.end method

.method public turnOffTSKSensitivity()V
    .locals 4

    .prologue
    .line 1518
    new-instance v0, Ljava/io/File;

    const-string v1, "TOUCH_KEY_SENSITIVITY_POWER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1519
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1520
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "turnOffTSKSensitivity"

    const-string v3, "Turn off TSK Sensitivity"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1521
    const-string v1, "TOUCH_KEY_SENSITIVITY_POWER"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1525
    :goto_0
    return-void

    .line 1523
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "turnOffTSKSensitivity"

    const-string v3, "File does not exist"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public turnOnTSKSensitivity()V
    .locals 4

    .prologue
    .line 1507
    new-instance v0, Ljava/io/File;

    const-string v1, "TOUCH_KEY_SENSITIVITY_POWER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1509
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1510
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "turnOnTSKSensitivity"

    const-string v3, "Turn on TSK Sensitivity"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1511
    const-string v1, "TOUCH_KEY_SENSITIVITY_POWER"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1515
    :goto_0
    return-void

    .line 1513
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "turnOnTSKSensitivity"

    const-string v3, "File does not exist"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unmount(I)V
    .locals 8
    .param p1, "type"    # I

    .prologue
    .line 669
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    .line 671
    .local v3, "storageVolumes":[Landroid/os/storage/StorageVolume;
    aget-object v4, v3, p1

    if-eqz v4, :cond_0

    .line 672
    aget-object v4, v3, p1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 674
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 675
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 676
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleDevice;->getMountService()Landroid/os/storage/IMountService;

    move-result-object v1

    .line 678
    .local v1, "ms":Landroid/os/storage/IMountService;
    if-eqz v1, :cond_0

    .line 680
    const/4 v4, 0x1

    const/4 v5, 0x1

    :try_start_0
    invoke-interface {v1, v2, v4, v5}, Landroid/os/storage/IMountService;->unmountVolume(Ljava/lang/String;ZZ)V

    .line 681
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "unmount"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unmount Storage type="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    .end local v1    # "ms":Landroid/os/storage/IMountService;
    .end local v2    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 682
    .restart local v1    # "ms":Landroid/os/storage/IMountService;
    .restart local v2    # "path":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 683
    .local v0, "e":Landroid/os/RemoteException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public writeSerialNo(Ljava/lang/String;)V
    .locals 1
    .param p1, "serial"    # Ljava/lang/String;

    .prologue
    .line 913
    const-string v0, "SERIAL_NO"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    .line 914
    return-void
.end method
