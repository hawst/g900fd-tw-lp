.class public Lcom/sec/factory/modules/ModuleSensor;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleSensor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    }
.end annotation


# static fields
.field private static DUMMY:I = 0x0

.field public static final GRIP_STATUS_DETECT:I = 0x1

.field public static final GRIP_STATUS_RELEASE:I

.field public static final ID_COUNT_FILE:I

.field public static final ID_COUNT_INTENT:I

.field public static final ID_COUNT_MANAGER:I

.field public static final ID_FILE____ACCELEROMETER:I

.field public static final ID_FILE____ACCELEROMETER_CAL:I

.field public static final ID_FILE____ACCELEROMETER_INTPIN:I

.field public static final ID_FILE____ACCELEROMETER_N_ANGLE:I

.field public static final ID_FILE____ACCELEROMETER_SELF:I

.field public static final ID_FILE____ACCELEROMETER_SELFTEST:I

.field public static final ID_FILE____BAROMETER_EEPROM:I

.field public static final ID_FILE____GRIP_RAW:I

.field public static final ID_FILE____GRIP_THRESHOLD:I

.field public static final ID_FILE____GYRO_POWER:I

.field public static final ID_FILE____GYRO_SELFTEST:I

.field public static final ID_FILE____GYRO_TEMPERATURE:I

.field public static final ID_FILE____LIGHT_ADC:I

.field public static final ID_FILE____LIGHT_RGBW:I

.field public static final ID_FILE____MAGNETIC_ADC:I

.field public static final ID_FILE____MAGNETIC_DAC:I

.field public static final ID_FILE____MAGNETIC_OFFSETH:I

.field public static final ID_FILE____MAGNETIC_POWER_OFF:I

.field public static final ID_FILE____MAGNETIC_POWER_ON:I

.field public static final ID_FILE____MAGNETIC_SELF:I

.field public static final ID_FILE____MAGNETIC_STATUS:I

.field public static final ID_FILE____MAGNETIC_TEMPERATURE:I

.field public static final ID_FILE____PROXIMITY_ADC:I

.field public static final ID_FILE____PROXIMITY_AVG:I

.field public static final ID_FILE____PROXIMITY_OFFSET:I

.field public static final ID_INTENT__CP_ACCELEROMETER:I

.field public static final ID_INTENT__GRIP:I

.field public static final ID_MANAGER_ACCELEROMETER:I

.field public static final ID_MANAGER_ACCELEROMETER_N_ANGLE:I

.field public static final ID_MANAGER_ACCELEROMETER_SELF:I

.field public static final ID_MANAGER_BAROMETER:I

.field public static final ID_MANAGER_BIO:I

.field public static final ID_MANAGER_BIO_HRM:I

.field public static final ID_MANAGER_BODYTEMP:I

.field public static final ID_MANAGER_GRIP:I

.field public static final ID_MANAGER_GYRO:I

.field public static final ID_MANAGER_GYRO_EXPANSION:I

.field public static final ID_MANAGER_GYRO_POWER:I

.field public static final ID_MANAGER_GYRO_SELF:I

.field public static final ID_MANAGER_GYRO_TEMPERATURE:I

.field public static final ID_MANAGER_HUMIDITY:I

.field public static final ID_MANAGER_LIGHT:I

.field public static final ID_MANAGER_LIGHT_CCT:I

.field public static final ID_MANAGER_MAGNETIC:I

.field public static final ID_MANAGER_MAGNETIC_ADC:I

.field public static final ID_MANAGER_MAGNETIC_DAC:I

.field public static final ID_MANAGER_MAGNETIC_OFFSETH:I

.field public static final ID_MANAGER_MAGNETIC_POWER_OFF:I

.field public static final ID_MANAGER_MAGNETIC_POWER_ON:I

.field public static final ID_MANAGER_MAGNETIC_SELF:I

.field public static final ID_MANAGER_MAGNETIC_STATUS:I

.field public static final ID_MANAGER_MAGNETIC_TEMPERATURE:I

.field public static final ID_MANAGER_PROXIMITY:I

.field public static final ID_MANAGER_TEMPERATURE:I

.field public static final ID_MANAGER_UV:I

.field public static final ID_SCOPE_MAX:I

.field public static final ID_SCOPE_MIN:I

.field public static LOG_LEVEL:I

.field private static mInstance:Lcom/sec/factory/modules/ModuleSensor;


# instance fields
.field private final DEFAULT_LOG_LEVEL:I

.field private final TARGET_FILE:I

.field private final TARGET_INTENT:I

.field private final TARGET_MANAGER:I

.field private mSensorNotificationMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/modules/SensorNotification;",
            ">;"
        }
    .end annotation
.end field

.field private mSensorRead:Lcom/sec/factory/modules/SensorRead;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    .line 36
    const/4 v0, 0x0

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    .line 38
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MIN:I

    .line 40
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    .line 41
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    .line 42
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    .line 43
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    .line 44
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    .line 45
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    .line 46
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    .line 47
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    .line 48
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    .line 49
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    .line 50
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    .line 51
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    .line 52
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    .line 53
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    .line 54
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    .line 55
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    .line 56
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    .line 57
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    .line 58
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    .line 59
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    .line 60
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    .line 61
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    .line 62
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    .line 63
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    .line 64
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    .line 65
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    .line 66
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    .line 67
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BODYTEMP:I

    .line 68
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    .line 70
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    .line 71
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    .line 72
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    .line 73
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELFTEST:I

    .line 74
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    .line 75
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    .line 76
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    .line 77
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    .line 78
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    .line 79
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    .line 80
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    .line 81
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_RGBW:I

    .line 82
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    .line 83
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    .line 84
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    .line 85
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    .line 86
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    .line 87
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    .line 88
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    .line 89
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_OFFSETH:I

    .line 90
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    .line 91
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    .line 92
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    .line 93
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GRIP_RAW:I

    .line 94
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GRIP_THRESHOLD:I

    .line 95
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    .line 97
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    .line 98
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    .line 99
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_INTENT:I

    .line 101
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MAX:I

    .line 138
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleSensor;->mInstance:Lcom/sec/factory/modules/ModuleSensor;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 149
    const-string v0, "ModuleSensor"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    iput v1, p0, Lcom/sec/factory/modules/ModuleSensor;->TARGET_MANAGER:I

    .line 32
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/modules/ModuleSensor;->TARGET_FILE:I

    .line 33
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/modules/ModuleSensor;->TARGET_INTENT:I

    .line 139
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    .line 141
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorNotificationMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 144
    iput v1, p0, Lcom/sec/factory/modules/ModuleSensor;->DEFAULT_LOG_LEVEL:I

    .line 150
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v0, Lcom/sec/factory/modules/SensorRead;

    invoke-direct {v0}, Lcom/sec/factory/modules/SensorRead;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    .line 152
    invoke-static {}, Lcom/sec/factory/modules/SensorCalculator;->initialize()V

    .line 153
    return-void
.end method

.method public static getString_ID(I)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # I

    .prologue
    .line 498
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p0, v0, :cond_0

    .line 499
    const-string v0, "ID_MANAGER_ACCELEROMETER"

    .line 630
    :goto_0
    return-object v0

    .line 500
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    if-ne p0, v0, :cond_1

    .line 501
    const-string v0, "ID_FILE____ACCELEROMETER"

    goto :goto_0

    .line 502
    :cond_1
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne p0, v0, :cond_2

    .line 503
    const-string v0, "ID_MANAGER_ACCELEROMETER_N_ANGLE"

    goto :goto_0

    .line 504
    :cond_2
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    if-ne p0, v0, :cond_3

    .line 505
    const-string v0, "ID_FILE____ACCELEROMETER_N_ANGLE"

    goto :goto_0

    .line 506
    :cond_3
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne p0, v0, :cond_4

    .line 507
    const-string v0, "ID_MANAGER_ACCELEROMETER_SELF"

    goto :goto_0

    .line 508
    :cond_4
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    if-ne p0, v0, :cond_5

    .line 509
    const-string v0, "ID_FILE____ACCELEROMETER_SELF"

    goto :goto_0

    .line 510
    :cond_5
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELFTEST:I

    if-ne p0, v0, :cond_6

    .line 511
    const-string v0, "ID_FILE____ACCELEROMETER_SELFTEST"

    goto :goto_0

    .line 512
    :cond_6
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    if-ne p0, v0, :cond_7

    .line 513
    const-string v0, "ID_FILE____ACCELEROMETER_CAL"

    goto :goto_0

    .line 514
    :cond_7
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    if-ne p0, v0, :cond_8

    .line 515
    const-string v0, "ID_FILE____ACCELEROMETER_INTPIN"

    goto :goto_0

    .line 518
    :cond_8
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne p0, v0, :cond_9

    .line 519
    const-string v0, "ID_INTENT__CP_ACCELEROMETER"

    goto :goto_0

    .line 523
    :cond_9
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p0, v0, :cond_a

    .line 524
    const-string v0, "ID_MANAGER_BAROMETER"

    goto :goto_0

    .line 525
    :cond_a
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    if-ne p0, v0, :cond_b

    .line 526
    const-string v0, "ID_FILE____BAROMETER_EEPROM"

    goto :goto_0

    .line 529
    :cond_b
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p0, v0, :cond_c

    .line 530
    const-string v0, "ID_INTENT__GRIP"

    goto :goto_0

    .line 531
    :cond_c
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    if-ne p0, v0, :cond_d

    .line 532
    const-string v0, "ID_MANAGER_GRIP"

    goto :goto_0

    .line 535
    :cond_d
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne p0, v0, :cond_e

    .line 536
    const-string v0, "ID_MANAGER_GYRO"

    goto :goto_0

    .line 537
    :cond_e
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    if-ne p0, v0, :cond_f

    .line 538
    const-string v0, "ID_MANAGER_GYRO_POWER"

    goto :goto_0

    .line 539
    :cond_f
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    if-ne p0, v0, :cond_10

    .line 540
    const-string v0, "ID_FILE____GYRO_POWER"

    goto :goto_0

    .line 541
    :cond_10
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne p0, v0, :cond_11

    .line 542
    const-string v0, "ID_MANAGER_GYRO_EXPANSION"

    goto :goto_0

    .line 543
    :cond_11
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne p0, v0, :cond_12

    .line 544
    const-string v0, "ID_MANAGER_GYRO_TEMPERATURE"

    goto :goto_0

    .line 545
    :cond_12
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    if-ne p0, v0, :cond_13

    .line 546
    const-string v0, "ID_FILE____GYRO_TEMPERATURE"

    goto/16 :goto_0

    .line 547
    :cond_13
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne p0, v0, :cond_14

    .line 548
    const-string v0, "ID_MANAGER_GYRO_SELF"

    goto/16 :goto_0

    .line 549
    :cond_14
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    if-ne p0, v0, :cond_15

    .line 550
    const-string v0, "ID_FILE____GYRO_SELFTEST"

    goto/16 :goto_0

    .line 553
    :cond_15
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne p0, v0, :cond_16

    .line 554
    const-string v0, "ID_MANAGER_LIGHT"

    goto/16 :goto_0

    .line 555
    :cond_16
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    if-ne p0, v0, :cond_17

    .line 556
    const-string v0, "ID_MANAGER_LIGHT_CCT"

    goto/16 :goto_0

    .line 557
    :cond_17
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    if-ne p0, v0, :cond_18

    .line 558
    const-string v0, "ID_FILE____LIGHT_ADC"

    goto/16 :goto_0

    .line 559
    :cond_18
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_RGBW:I

    if-ne p0, v0, :cond_19

    .line 560
    const-string v0, "ID_FILE____LIGHT_RGBW"

    goto/16 :goto_0

    .line 563
    :cond_19
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne p0, v0, :cond_1a

    .line 564
    const-string v0, "ID_MANAGER_MAGNETIC"

    goto/16 :goto_0

    .line 565
    :cond_1a
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne p0, v0, :cond_1b

    .line 566
    const-string v0, "ID_MANAGER_MAGNETIC_POWER_ON"

    goto/16 :goto_0

    .line 567
    :cond_1b
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    if-ne p0, v0, :cond_1c

    .line 568
    const-string v0, "ID_FILE____MAGNETIC_POWER_ON"

    goto/16 :goto_0

    .line 569
    :cond_1c
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne p0, v0, :cond_1d

    .line 570
    const-string v0, "ID_MANAGER_MAGNETIC_POWER_OFF"

    goto/16 :goto_0

    .line 571
    :cond_1d
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    if-ne p0, v0, :cond_1e

    .line 572
    const-string v0, "ID_FILE____MAGNETIC_POWER_OFF"

    goto/16 :goto_0

    .line 573
    :cond_1e
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne p0, v0, :cond_1f

    .line 574
    const-string v0, "ID_MANAGER_MAGNETIC_STATUS"

    goto/16 :goto_0

    .line 575
    :cond_1f
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    if-ne p0, v0, :cond_20

    .line 576
    const-string v0, "ID_FILE____MAGNETIC_STATUS"

    goto/16 :goto_0

    .line 577
    :cond_20
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne p0, v0, :cond_21

    .line 578
    const-string v0, "ID_MANAGER_MAGNETIC_TEMPERATURE"

    goto/16 :goto_0

    .line 579
    :cond_21
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    if-ne p0, v0, :cond_22

    .line 580
    const-string v0, "ID_FILE____MAGNETIC_TEMPERATURE"

    goto/16 :goto_0

    .line 581
    :cond_22
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne p0, v0, :cond_23

    .line 582
    const-string v0, "ID_MANAGER_MAGNETIC_DAC"

    goto/16 :goto_0

    .line 583
    :cond_23
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p0, v0, :cond_24

    .line 584
    const-string v0, "ID_FILE____MAGNETIC_DAC"

    goto/16 :goto_0

    .line 585
    :cond_24
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne p0, v0, :cond_25

    .line 586
    const-string v0, "ID_MANAGER_MAGNETIC_ADC"

    goto/16 :goto_0

    .line 587
    :cond_25
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p0, v0, :cond_26

    .line 588
    const-string v0, "ID_FILE____MAGNETIC_ADC"

    goto/16 :goto_0

    .line 589
    :cond_26
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne p0, v0, :cond_27

    .line 590
    const-string v0, "ID_MANAGER_MAGNETIC_SELF"

    goto/16 :goto_0

    .line 591
    :cond_27
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p0, v0, :cond_28

    .line 592
    const-string v0, "ID_FILE____MAGNETIC_SELF"

    goto/16 :goto_0

    .line 593
    :cond_28
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne p0, v0, :cond_29

    .line 594
    const-string v0, "ID_MANAGER_MAGNETIC_OFFSETH"

    goto/16 :goto_0

    .line 595
    :cond_29
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_OFFSETH:I

    if-ne p0, v0, :cond_2a

    .line 596
    const-string v0, "ID_FILE____MAGNETIC_OFFSETH"

    goto/16 :goto_0

    .line 599
    :cond_2a
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne p0, v0, :cond_2b

    .line 600
    const-string v0, "ID_MANAGER_PROXIMITY"

    goto/16 :goto_0

    .line 601
    :cond_2b
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    if-ne p0, v0, :cond_2c

    .line 602
    const-string v0, "ID_FILE____PROXIMITY_ADC"

    goto/16 :goto_0

    .line 603
    :cond_2c
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    if-ne p0, v0, :cond_2d

    .line 604
    const-string v0, "ID_FILE____PROXIMITY_AVG"

    goto/16 :goto_0

    .line 605
    :cond_2d
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    if-ne p0, v0, :cond_2e

    .line 606
    const-string v0, "ID_FILE____PROXIMITY_OFFSET"

    goto/16 :goto_0

    .line 609
    :cond_2e
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    if-ne p0, v0, :cond_2f

    .line 610
    const-string v0, "ID_MANAGER_TEMPERATURE"

    goto/16 :goto_0

    .line 613
    :cond_2f
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    if-ne p0, v0, :cond_30

    .line 614
    const-string v0, "ID_MANAGER_HUMIDITY"

    goto/16 :goto_0

    .line 617
    :cond_30
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    if-ne p0, v0, :cond_31

    .line 618
    const-string v0, "ID_MANAGER_BIO"

    goto/16 :goto_0

    .line 621
    :cond_31
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    if-ne p0, v0, :cond_32

    .line 622
    const-string v0, "ID_MANAGER_BIO_HRM"

    goto/16 :goto_0

    .line 625
    :cond_32
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    if-ne p0, v0, :cond_33

    .line 626
    const-string v0, "ID_MANAGER_UV"

    goto/16 :goto_0

    .line 630
    :cond_33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown => id : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleSensor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    sget-object v0, Lcom/sec/factory/modules/ModuleSensor;->mInstance:Lcom/sec/factory/modules/ModuleSensor;

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Lcom/sec/factory/modules/ModuleSensor;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleSensor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleSensor;->mInstance:Lcom/sec/factory/modules/ModuleSensor;

    .line 160
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModuleSensor;->mInstance:Lcom/sec/factory/modules/ModuleSensor;

    return-object v0
.end method


# virtual methods
.method public SensorOff([I)V
    .locals 5
    .param p1, "sensorID"    # [I

    .prologue
    .line 212
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "SensorOff"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p1, :cond_1

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "]LOG_LEVEL : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v4, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    if-eqz p1, :cond_0

    .line 215
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/ModuleSensor;->convertSensorID([I)Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;

    move-result-object v0

    .line 216
    .local v0, "sensorIDBucket":Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    iget-object v3, v0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Manager:[I

    iget-object v4, v0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleSensor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    invoke-virtual {v2, v3, v4, v1}, Lcom/sec/factory/modules/SensorRead;->SensorOff([I[ILandroid/hardware/SensorManager;)V

    .line 218
    .end local v0    # "sensorIDBucket":Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    :cond_0
    return-void

    .line 212
    :cond_1
    const-string v1, "null"

    goto :goto_0
.end method

.method public SensorOff([ILandroid/os/Handler;)V
    .locals 4
    .param p1, "sensorID"    # [I
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 221
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorNotificationMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorNotificationMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorNotificationMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/factory/modules/SensorNotification;

    .line 223
    .local v0, "sensorNotification":Lcom/sec/factory/modules/SensorNotification;
    if-eqz v0, :cond_0

    .line 224
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SensorOff"

    const-string v3, "The sensorNotification is removed and will be received interrup"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorNotification;->interrup()V

    .line 230
    .end local v0    # "sensorNotification":Lcom/sec/factory/modules/SensorNotification;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/ModuleSensor;->SensorOff([I)V

    .line 231
    return-void
.end method

.method public SensorOn([II)Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    .locals 7
    .param p1, "sensorID"    # [I
    .param p2, "logLevel"    # I

    .prologue
    .line 170
    sput p2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    .line 171
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SensorOn"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]LOG_LEVEL : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    if-eqz p1, :cond_1

    .line 174
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/ModuleSensor;->convertSensorID([I)Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;

    move-result-object v6

    .line 175
    .local v6, "sensorIDBucket":Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    sget-object v1, Lcom/sec/factory/modules/ModuleSensor;->mContext:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleSensor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iget-object v3, v6, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Manager:[I

    iget-object v4, v6, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    iget-object v5, v6, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Intent:[I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/factory/modules/SensorRead;->SensorOn(Landroid/content/Context;Landroid/hardware/SensorManager;[I[I[I)V

    .line 180
    .end local v6    # "sensorIDBucket":Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    :goto_1
    return-object v6

    .line 171
    :cond_0
    const-string v0, "null"

    goto :goto_0

    .line 180
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public SensorOn([I)V
    .locals 1
    .param p1, "sensorID"    # [I

    .prologue
    .line 166
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/factory/modules/ModuleSensor;->SensorOn([II)Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;

    .line 167
    return-void
.end method

.method public SensorOn([ILandroid/os/Handler;I)V
    .locals 1
    .param p1, "sensorID"    # [I
    .param p2, "notiHandler"    # Landroid/os/Handler;
    .param p3, "messageDelay_SensorUpdate_millisecond"    # I

    .prologue
    .line 189
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/factory/modules/ModuleSensor;->SensorOn([ILandroid/os/Handler;II)V

    .line 190
    return-void
.end method

.method public SensorOn([ILandroid/os/Handler;II)V
    .locals 5
    .param p1, "sensorID"    # [I
    .param p2, "notiHandler"    # Landroid/os/Handler;
    .param p3, "messageDelay_SensorUpdate_millisecond"    # I
    .param p4, "logLevel"    # I

    .prologue
    .line 194
    invoke-virtual {p0, p1, p4}, Lcom/sec/factory/modules/ModuleSensor;->SensorOn([II)Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;

    move-result-object v0

    .line 195
    .local v0, "sensorIDBucket":Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    if-lez p3, :cond_1

    .line 196
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v2, p1}, Lcom/sec/factory/modules/SensorRead;->setLoop_ReadFile([I)V

    .line 201
    :goto_0
    if-eqz p1, :cond_0

    .line 202
    new-instance v1, Lcom/sec/factory/modules/SensorNotification;

    sget-object v2, Lcom/sec/factory/modules/ModuleSensor;->mContext:Landroid/content/Context;

    iget-object v3, v0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Manager:[I

    iget-object v4, v0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/factory/modules/SensorNotification;-><init>(Landroid/content/Context;[I[I)V

    .line 203
    .local v1, "sensorNotification":Lcom/sec/factory/modules/SensorNotification;
    invoke-virtual {v1, p3}, Lcom/sec/factory/modules/SensorNotification;->setLoopDelay(I)V

    .line 204
    invoke-virtual {v1, p2}, Lcom/sec/factory/modules/SensorNotification;->setHandler(Landroid/os/Handler;)V

    .line 205
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/SensorNotification;->setDaemon(Z)V

    .line 206
    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorNotification;->start()V

    .line 207
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorNotificationMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    .end local v1    # "sensorNotification":Lcom/sec/factory/modules/SensorNotification;
    :cond_0
    return-void

    .line 198
    :cond_1
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    goto :goto_0
.end method

.method public convertSensorID([I)Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
    .locals 18
    .param p1, "sensorID"    # [I

    .prologue
    .line 234
    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    new-array v4, v14, [I

    .line 235
    .local v4, "id_Manager":[I
    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    new-array v2, v14, [I

    .line 236
    .local v2, "id_File":[I
    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_INTENT:I

    new-array v3, v14, [I

    .line 238
    .local v3, "id_Intent":[I
    const/4 v9, 0x0

    .line 239
    .local v9, "index_Manager":I
    const/4 v5, 0x0

    .line 240
    .local v5, "index_File":I
    const/4 v7, 0x0

    .line 242
    .local v7, "index_Intent":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v14, v0

    if-ge v1, v14, :cond_3

    .line 243
    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MIN:I

    aget v15, p1, v1

    if-gt v14, v15, :cond_0

    aget v14, p1, v1

    sget v15, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    if-ge v14, v15, :cond_0

    .line 244
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "index_Manager":I
    .local v10, "index_Manager":I
    aget v14, p1, v1

    aput v14, v4, v9

    move v9, v10

    .line 242
    .end local v10    # "index_Manager":I
    .restart local v9    # "index_Manager":I
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    :cond_0
    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    aget v15, p1, v1

    if-gt v14, v15, :cond_1

    aget v14, p1, v1

    sget v15, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v16, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int v15, v15, v16

    if-ge v14, v15, :cond_1

    .line 247
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "index_File":I
    .local v6, "index_File":I
    aget v14, p1, v1

    aput v14, v2, v5

    move v5, v6

    .end local v6    # "index_File":I
    .restart local v5    # "index_File":I
    goto :goto_1

    .line 248
    :cond_1
    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sget v15, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    add-int/2addr v14, v15

    aget v15, p1, v1

    if-gt v14, v15, :cond_2

    aget v14, p1, v1

    sget v15, Lcom/sec/factory/modules/ModuleSensor;->ID_SCOPE_MAX:I

    if-gt v14, v15, :cond_2

    .line 250
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "index_Intent":I
    .local v8, "index_Intent":I
    aget v14, p1, v1

    aput v14, v3, v7

    move v7, v8

    .end local v8    # "index_Intent":I
    .restart local v7    # "index_Intent":I
    goto :goto_1

    .line 252
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ID : Unknown ("

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    aget v17, p1, v1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ")"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 257
    :cond_3
    if-lez v9, :cond_5

    .line 258
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    const-string v16, "Manager"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    new-array v13, v9, [I

    .line 261
    .local v13, "mIDArray_Manager":[I
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v9, :cond_4

    .line 262
    aget v14, v4, v1

    aput v14, v13, v1

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 265
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    const-string v16, " Manager ID"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v1, 0x0

    :goto_3
    array-length v14, v13

    if-ge v1, v14, :cond_6

    .line 268
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "   ["

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "] "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    aget v17, v13, v1

    invoke-static/range {v17 .. v17}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 271
    .end local v13    # "mIDArray_Manager":[I
    :cond_5
    const/4 v13, 0x0

    .line 275
    .restart local v13    # "mIDArray_Manager":[I
    :cond_6
    if-lez v5, :cond_8

    .line 276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    const-string v16, "File"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    new-array v11, v5, [I

    .line 279
    .local v11, "mIDArray_File":[I
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v5, :cond_7

    .line 280
    aget v14, v2, v1

    aput v14, v11, v1

    .line 279
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 283
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    const-string v16, " File ID"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/4 v1, 0x0

    :goto_5
    array-length v14, v11

    if-ge v1, v14, :cond_9

    .line 286
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "   ["

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "] "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    aget v17, v11, v1

    invoke-static/range {v17 .. v17}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 289
    .end local v11    # "mIDArray_File":[I
    :cond_8
    const/4 v11, 0x0

    .line 293
    .restart local v11    # "mIDArray_File":[I
    :cond_9
    if-lez v7, :cond_b

    .line 294
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    const-string v16, "Intent"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    new-array v12, v7, [I

    .line 297
    .local v12, "mIDArray_Intent":[I
    const/4 v1, 0x0

    :goto_6
    if-ge v1, v7, :cond_a

    .line 298
    aget v14, v3, v1

    aput v14, v12, v1

    .line 297
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 301
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    const-string v16, " Intent ID"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v1, 0x0

    :goto_7
    array-length v14, v12

    if-ge v1, v14, :cond_c

    .line 304
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "SensorOn"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "   ["

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "] "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    aget v17, v12, v1

    invoke-static/range {v17 .. v17}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 307
    .end local v12    # "mIDArray_Intent":[I
    :cond_b
    const/4 v12, 0x0

    .line 310
    .restart local v12    # "mIDArray_Intent":[I
    :cond_c
    new-instance v14, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;

    invoke-direct {v14, v13, v11, v12}, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;-><init>([I[I[I)V

    return-object v14
.end method

.method public getData(I)[Ljava/lang/String;
    .locals 7
    .param p1, "id"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 330
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    if-gt v0, v5, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p1, v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v0

    .line 490
    :goto_0
    return-object v0

    .line 337
    :cond_1
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    if-ne p1, v0, :cond_2

    .line 338
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 339
    :cond_2
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne p1, v0, :cond_3

    .line 340
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZnAngle(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 341
    :cond_3
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    if-ne p1, v0, :cond_4

    .line 342
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZnAngle(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 343
    :cond_4
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne p1, v0, :cond_5

    .line 344
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 345
    :cond_5
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    if-ne p1, v0, :cond_6

    .line 346
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 347
    :cond_6
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELFTEST:I

    if-ne p1, v0, :cond_7

    .line 348
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterSelfTest(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 349
    :cond_7
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    if-ne p1, v0, :cond_8

    .line 350
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterCal(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_8
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    if-ne p1, v0, :cond_9

    .line 352
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterIntpin(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 355
    :cond_9
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne p1, v0, :cond_a

    .line 356
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v6}, Lcom/sec/factory/modules/SensorRead;->getAccelermeterXYZnAngle(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_a
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p1, v0, :cond_b

    .line 361
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getBarometer(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 362
    :cond_b
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    if-ne p1, v0, :cond_c

    .line 363
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getBarometerEEPROM(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 366
    :cond_c
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p1, v0, :cond_d

    .line 367
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v6}, Lcom/sec/factory/modules/SensorRead;->getGrip(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 368
    :cond_d
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    if-ne p1, v0, :cond_e

    .line 369
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getGrip(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 370
    :cond_e
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GRIP_RAW:I

    if-ne p1, v0, :cond_f

    .line 371
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getGrip(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 372
    :cond_f
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GRIP_THRESHOLD:I

    if-ne p1, v0, :cond_10

    .line 373
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getGripThreshold(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 376
    :cond_10
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne p1, v0, :cond_11

    .line 377
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getGyro(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 378
    :cond_11
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    if-ne p1, v0, :cond_12

    .line 379
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getGyroPower(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 380
    :cond_12
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne p1, v0, :cond_13

    .line 381
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getGyroExpansion(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 382
    :cond_13
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne p1, v0, :cond_14

    .line 383
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getGyroTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 384
    :cond_14
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    if-ne p1, v0, :cond_15

    .line 385
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getGyroTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 386
    :cond_15
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne p1, v0, :cond_16

    .line 387
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getGyroSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 388
    :cond_16
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    if-ne p1, v0, :cond_17

    .line 389
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getGyroSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 407
    :cond_17
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne p1, v0, :cond_18

    .line 408
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getLight(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 409
    :cond_18
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    if-ne p1, v0, :cond_19

    .line 410
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getLightCCT(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 411
    :cond_19
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    if-ne p1, v0, :cond_1a

    .line 412
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getLightADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 413
    :cond_1a
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____LIGHT_RGBW:I

    if-ne p1, v0, :cond_1b

    .line 414
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getLightRGBW(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 417
    :cond_1b
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne p1, v0, :cond_1c

    .line 418
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagnetic(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 419
    :cond_1c
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne p1, v0, :cond_1d

    .line 420
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticPowerOn(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 421
    :cond_1d
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    if-ne p1, v0, :cond_1e

    .line 422
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticPowerOn(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 423
    :cond_1e
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne p1, v0, :cond_1f

    .line 424
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticPowerOff(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 425
    :cond_1f
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    if-ne p1, v0, :cond_20

    .line 426
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticPowerOff(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 427
    :cond_20
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne p1, v0, :cond_21

    .line 428
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticStatus(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 429
    :cond_21
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    if-ne p1, v0, :cond_22

    .line 430
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticStatus(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 431
    :cond_22
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne p1, v0, :cond_23

    .line 432
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 433
    :cond_23
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    if-ne p1, v0, :cond_24

    .line 434
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 435
    :cond_24
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne p1, v0, :cond_25

    .line 436
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticDAC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 437
    :cond_25
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p1, v0, :cond_26

    .line 438
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticDAC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 439
    :cond_26
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne p1, v0, :cond_27

    .line 440
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 441
    :cond_27
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p1, v0, :cond_28

    .line 442
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 443
    :cond_28
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne p1, v0, :cond_29

    .line 444
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 445
    :cond_29
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v0, :cond_2a

    .line 446
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 447
    :cond_2a
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne p1, v0, :cond_2b

    .line 448
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getMagneticOffsetH(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 449
    :cond_2b
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_OFFSETH:I

    if-ne p1, v0, :cond_2c

    .line 450
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getMagneticOffsetH(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 453
    :cond_2c
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne p1, v0, :cond_2d

    .line 454
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getProximity(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 455
    :cond_2d
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    if-ne p1, v0, :cond_2e

    .line 456
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getProximityADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 457
    :cond_2e
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    if-ne p1, v0, :cond_2f

    .line 458
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getProximityAVG(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 459
    :cond_2f
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    if-ne p1, v0, :cond_30

    .line 460
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v5}, Lcom/sec/factory/modules/SensorRead;->getProximityOffset(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 464
    :cond_30
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    if-ne p1, v0, :cond_31

    .line 465
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 466
    :cond_31
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    if-ne p1, v0, :cond_32

    .line 467
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getHumidity(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 471
    :cond_32
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    if-ne p1, v0, :cond_33

    .line 472
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getBio(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 473
    :cond_33
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    if-ne p1, v0, :cond_34

    .line 474
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getHrm(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 478
    :cond_34
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BODYTEMP:I

    if-ne p1, v0, :cond_35

    .line 479
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getBodyTemp(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 483
    :cond_35
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    if-ne p1, v0, :cond_36

    .line 484
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/SensorRead;->getUV(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 489
    :cond_36
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getData"

    const-string v2, "id : Unknown"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public isSensorOn(I)Z
    .locals 5
    .param p1, "sensorID"    # I

    .prologue
    .line 314
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v1, p1}, Lcom/sec/factory/modules/SensorRead;->isSensorOn(I)Z

    move-result v0

    .line 315
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isSensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensorID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    return v0
.end method

.method public isSensorReady(I)Z
    .locals 5
    .param p1, "sensorID"    # I

    .prologue
    .line 320
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->mSensorRead:Lcom/sec/factory/modules/SensorRead;

    invoke-virtual {v1, p1}, Lcom/sec/factory/modules/SensorRead;->isSensorReady(I)Z

    move-result v0

    .line 321
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isSensorReady"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensorID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    return v0
.end method

.method public logSensorName()V
    .locals 5

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - ACCELEROMETER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_ACCELEROMETER:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - BAROMETER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_BAROMETER:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - GEOMAGNETIC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - GRIP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GRIP:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - GYRO : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GYRO:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - HUMIDITY : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_HUMIDITY:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - LIGHT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_LIGHT:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - PROXIMITY : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_PROXIMITY:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XML - TEMPERATURE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_TEMPERATURE:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - ACCELEROMETER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_ACCELEROMETER:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - BAROMETER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_BAROMETER:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - GEOMAGNETIC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - GRIP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GRIP:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - GYRO : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GYRO:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - HUMIDITY : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_HUMIDITY:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - LIGHT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_LIGHT:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - PROXIMITY : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_PROXIMITY:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE - TEMPERATURE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_TEMPERATURE:I

    sget v4, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    invoke-static {v3, v4}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    return-void
.end method
