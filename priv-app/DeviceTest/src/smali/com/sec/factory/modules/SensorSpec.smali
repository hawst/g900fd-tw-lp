.class public Lcom/sec/factory/modules/SensorSpec;
.super Ljava/lang/Object;
.source "SensorSpec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/SensorSpec$Light;,
        Lcom/sec/factory/modules/SensorSpec$Proximity;,
        Lcom/sec/factory/modules/SensorSpec$Range;,
        Lcom/sec/factory/modules/SensorSpec$Accelerometer;
    }
.end annotation


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private RANGE_MAX_INTEGER:I

.field private RANGE_MIN_INTEGER:I

.field private mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

.field private mFeature_Accelerometer:Ljava/lang/String;

.field private mFeature_Light:Ljava/lang/String;

.field private mFeature_Magnetic:Ljava/lang/String;

.field private mFeature_Proximity:Ljava/lang/String;

.field private mLight:Lcom/sec/factory/modules/SensorSpec$Light;

.field private mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

.field private mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

.field private mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

.field private mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

.field private mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, "SensorSpec"

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->CLASS_NAME:Ljava/lang/String;

    .line 110
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    .line 111
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MIN_INTEGER:I

    .line 118
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    .line 119
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 120
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 121
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 122
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 123
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    .line 124
    iput-object v1, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    .line 130
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_ACCELEROMETER:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    .line 132
    const-string v0, "SensorSpec"

    const-string v1, "SensorSpec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature_Accelerometer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorSpec;->setSpecAccel()V

    .line 135
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    .line 137
    const-string v0, "SensorSpec"

    const-string v1, "SensorSpec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature_Magnetic : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorSpec;->setSpecMagnetic()V

    .line 140
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_PROXIMITY:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    .line 142
    const-string v0, "SensorSpec"

    const-string v1, "SensorSpec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature_Proximity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorSpec;->setSpecProximity()V

    .line 145
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_LIGHT:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    .line 147
    const-string v0, "SensorSpec"

    const-string v1, "SensorSpec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature_Light : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorSpec;->setSpecLight()V

    .line 149
    return-void
.end method

.method private setSpecAccel()V
    .locals 11

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    .line 275
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_SMB380"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x34

    const/16 v3, 0x34

    const/16 v4, -0x34

    const/16 v5, 0x34

    const/16 v6, 0xc0

    const/16 v7, 0x140

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto :goto_0

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x34

    const/16 v3, 0x34

    const/16 v4, -0x34

    const/16 v5, 0x34

    const/16 v6, 0xbe

    const/16 v7, 0x142

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto :goto_0

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA022"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 170
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x34

    const/16 v3, 0x34

    const/16 v4, -0x34

    const/16 v5, 0x34

    const/16 v6, 0xb9

    const/16 v7, 0x147

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto :goto_0

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA023"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 174
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x38

    const/16 v3, 0x38

    const/16 v4, -0x38

    const/16 v5, 0x38

    const/16 v6, 0xb5

    const/16 v7, 0x14b

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 177
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA222"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 178
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x8

    const/16 v2, -0xf

    const/16 v3, 0xf

    const/16 v4, -0xf

    const/16 v5, 0xf

    const/16 v6, 0x2c

    const/16 v7, 0x54

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA220"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/4 v1, 0x6

    const/4 v2, -0x6

    const/4 v3, 0x6

    const/4 v4, -0x6

    const/4 v5, 0x6

    const/16 v6, 0x9

    const/16 v7, 0x17

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 185
    :cond_6
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA254"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 186
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0xb5

    const/16 v3, 0xb5

    const/16 v4, -0xb5

    const/16 v5, 0xb5

    const/16 v6, 0x312

    const/16 v7, 0x4ee

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 189
    :cond_7
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 190
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0xb5

    const/16 v3, 0xb5

    const/16 v4, -0xb5

    const/16 v5, 0xb5

    const/16 v6, 0x312

    const/16 v7, 0x4ee

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 193
    :cond_8
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMI055"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 194
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0xbf

    const/16 v3, 0xbf

    const/16 v4, -0xbf

    const/16 v5, 0xbf

    const/16 v6, -0x4ed

    const/16 v7, -0x313

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 197
    :cond_9
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMI058"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 198
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x3dd

    const/16 v3, 0x3dd

    const/16 v4, -0x295

    const/16 v5, 0x295

    const/16 v6, 0xc7f

    const/16 v7, 0x1381

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 201
    :cond_a
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMI160"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 202
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x3dd

    const/16 v3, 0x3dd

    const/16 v4, -0x295

    const/16 v5, 0x295

    const/16 v6, 0xc7f

    const/16 v7, 0x1381

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 207
    :cond_b
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_KR3DH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 208
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x66

    const/16 v3, 0x66

    const/16 v4, -0x66

    const/16 v5, 0x66

    const/16 v6, 0x351

    const/16 v7, 0x4af

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 211
    :cond_c
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_K2DH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 212
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x9a

    const/16 v3, 0x9a

    const/16 v4, -0x9a

    const/16 v5, 0x9a

    const/16 v6, 0x31e

    const/16 v7, 0x4e2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 215
    :cond_d
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_K2HH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 216
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 220
    :cond_e
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_K2DH_REV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 221
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x9a

    const/16 v3, 0x9a

    const/16 v4, -0x9a

    const/16 v5, 0x9a

    const/16 v6, -0x4e2

    const/16 v7, -0x31e

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 224
    :cond_f
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_K3DH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 225
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x9a

    const/16 v3, 0x9a

    const/16 v4, -0x9a

    const/16 v5, 0x9a

    const/16 v6, 0x31e

    const/16 v7, 0x4e2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 228
    :cond_10
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_K2DM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 229
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x8

    const/16 v2, -0xc

    const/16 v3, 0xc

    const/16 v4, -0xc

    const/16 v5, 0xc

    const/16 v6, 0x30

    const/16 v7, 0x50

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 232
    :cond_11
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_KR3DM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 233
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x8

    const/16 v2, -0xc

    const/16 v3, 0xc

    const/16 v4, -0xc

    const/16 v5, 0xc

    const/16 v6, 0x30

    const/16 v7, 0x50

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 236
    :cond_12
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_LSM330DLC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 238
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x9a

    const/16 v3, 0x9a

    const/16 v4, -0x9a

    const/16 v5, 0x9a

    const/16 v6, 0x31e

    const/16 v7, 0x4e2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 242
    :cond_13
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "KIONIX_KXUD9"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 243
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x97

    const/16 v3, 0x97

    const/16 v4, -0x97

    const/16 v5, 0x97

    const/16 v6, 0x282

    const/16 v7, 0x3e4

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 246
    :cond_14
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "KIONIX_KXTF9"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 247
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x85

    const/16 v3, 0x85

    const/16 v4, -0x85

    const/16 v5, 0x85

    const/16 v6, 0x34e

    const/16 v7, 0x4b2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 251
    :cond_15
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6050"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 252
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 255
    :cond_16
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6051"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 256
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 259
    :cond_17
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6515M"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 260
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 263
    :cond_18
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6500"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 264
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 267
    :cond_19
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "MAXIM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 268
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/factory/modules/SensorSpec$Accelerometer;-><init>(IIIIIIIIIZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0

    .line 272
    :cond_1a
    const-string v0, "SensorSpec"

    const-string v1, "setSpecAccel"

    const-string v2, "feature : Unknown => return null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    goto/16 :goto_0
.end method

.method private setSpecLight()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 524
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 525
    iput-object v4, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    .line 572
    :goto_0
    return-void

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "SHARP_GP2AP002S00F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 532
    iput-object v4, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "SHARP_GP2AP002A00F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 539
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "SHARP_GP2AP030A00F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 540
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 544
    :cond_3
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "CAPELLA_CM3663"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 545
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 548
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "CAPELLA_CM36691"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 549
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v2, v3, v3}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 552
    :cond_5
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "CAPELLA_CM36651"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 553
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v3, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 557
    :cond_6
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "TAOS_TMD2672x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 558
    iput-object v4, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto :goto_0

    .line 561
    :cond_7
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "TAOS_TMD2771x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 562
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto/16 :goto_0

    .line 565
    :cond_8
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Light:Ljava/lang/String;

    const-string v1, "TAOS_TMD2772x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 566
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Light;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Light;-><init>(IZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto/16 :goto_0

    .line 570
    :cond_9
    iput-object v4, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    goto/16 :goto_0
.end method

.method private setSpecMagnetic()V
    .locals 14

    .prologue
    const/16 v7, 0xcc

    const/16 v9, 0x1e

    const/16 v8, -0x1e

    const/4 v13, 0x0

    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 279
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 280
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 281
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 282
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 470
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 290
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x1964

    const/16 v2, 0x1964

    const/16 v3, -0x1964

    const/16 v4, 0x1964

    const/16 v5, -0x1964

    const/16 v6, 0x1964

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 291
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 292
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0xc8

    const/16 v2, 0xc8

    const/16 v3, -0xc8

    const/16 v4, 0xc8

    const/16 v5, -0xc80

    const/16 v6, -0x320

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto :goto_0

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 297
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x1964

    const/16 v2, 0x1964

    const/16 v3, -0x1964

    const/16 v4, 0x1964

    const/16 v5, -0x1964

    const/16 v6, 0x1964

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 298
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 299
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0xc8

    const/16 v2, 0xc8

    const/16 v3, -0xc8

    const/16 v4, 0xc8

    const/16 v5, -0xc80

    const/16 v6, -0x320

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto :goto_0

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C_MANAGER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 303
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 304
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x1964

    const/16 v2, 0x1964

    const/16 v3, -0x1964

    const/16 v4, 0x1964

    const/16 v5, -0x1964

    const/16 v6, 0x1964

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 305
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 306
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0xc8

    const/16 v2, 0xc8

    const/16 v3, -0xc8

    const/16 v4, 0xc8

    const/16 v5, -0xc80

    const/16 v6, -0x320

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 309
    :cond_3
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 310
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x7e

    const/16 v4, 0x7e

    const/16 v6, 0x7e

    const/16 v7, 0x80

    const/16 v8, 0xfe

    const/16 v9, 0x80

    const/16 v10, 0xfe

    const/16 v11, 0x80

    const/16 v12, 0xfe

    move v3, v1

    move v5, v1

    invoke-direct/range {v0 .. v12}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIIIIIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 311
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, 0x58

    const/16 v2, 0xa8

    const/16 v3, 0x58

    const/16 v4, 0xa8

    const/16 v5, 0x58

    const/16 v6, 0xa8

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 312
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 313
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 316
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8975"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 317
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 318
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x7d0

    const/16 v2, 0x7d0

    const/16 v3, -0x7d0

    const/16 v4, 0x7d0

    const/16 v5, -0x7d0

    const/16 v6, 0x7d0

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 319
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 320
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x64

    const/16 v2, 0x64

    const/16 v3, -0x64

    const/16 v4, 0x64

    const/16 v5, -0x3e8

    const/16 v6, -0x12c

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 323
    :cond_5
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 324
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 325
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x640

    const/16 v2, 0x640

    const/16 v3, -0x640

    const/16 v4, 0x640

    const/16 v5, -0x640

    const/16 v6, 0x640

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 326
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 327
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, -0x190

    const/16 v6, -0x32

    move v1, v8

    move v2, v9

    move v3, v8

    move v4, v9

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 330
    :cond_6
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911C"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 331
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 332
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x640

    const/16 v2, 0x640

    const/16 v3, -0x640

    const/16 v4, 0x640

    const/16 v5, -0x640

    const/16 v6, 0x640

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 333
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 334
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, -0x190

    const/16 v6, -0x32

    move v1, v8

    move v2, v9

    move v3, v8

    move v4, v9

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 338
    :cond_7
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 339
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/4 v3, 0x5

    const/16 v4, 0x20

    const/4 v5, 0x5

    const/16 v6, 0x20

    const/4 v7, 0x5

    const/16 v8, 0x20

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 340
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x167

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 341
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 342
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, 0x50

    iget v4, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    const/16 v5, 0x6b

    iget v6, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    move v7, v1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 345
    :cond_8
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 346
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    move v3, v8

    move v4, v9

    move v5, v8

    move v6, v9

    move v7, v8

    move v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 347
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x167

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 348
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 349
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, 0x85

    iget v4, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    const/16 v5, 0xa0

    iget v6, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    move v7, v1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 352
    :cond_9
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530A"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 353
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    move v3, v8

    move v4, v9

    move v5, v8

    move v6, v9

    move v7, v8

    move v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 354
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x167

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 355
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 356
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, 0x85

    iget v4, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    const/16 v5, 0xa0

    iget v6, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    move v7, v1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 359
    :cond_a
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 360
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    move v3, v8

    move v4, v9

    move v5, v8

    move v6, v9

    move v7, v8

    move v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 361
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x167

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 362
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x190

    const/16 v4, 0x190

    const/16 v5, -0x190

    const/16 v6, 0x190

    const/16 v7, -0x190

    const/16 v8, 0x190

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 363
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, 0x85

    iget v4, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    const/16 v5, 0xa0

    iget v6, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    move v7, v1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 366
    :cond_b
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 367
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    move v3, v8

    move v4, v9

    move v5, v8

    move v6, v9

    move v7, v8

    move v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 368
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x167

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 369
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x258

    const/16 v4, 0x258

    const/16 v5, -0x258

    const/16 v6, 0x258

    const/16 v7, -0x258

    const/16 v8, 0x258

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 370
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, 0x11

    iget v4, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    const/16 v5, 0x16

    iget v6, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    move v7, v1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 373
    :cond_c
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532B"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 374
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    move v3, v8

    move v4, v9

    move v5, v8

    move v6, v9

    move v7, v8

    move v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 375
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x167

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 376
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x258

    const/16 v4, 0x258

    const/16 v5, -0x258

    const/16 v6, 0x258

    const/16 v7, -0x258

    const/16 v8, 0x258

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 377
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, 0x11

    iget v4, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    const/16 v5, 0x16

    iget v6, p0, Lcom/sec/factory/modules/SensorSpec;->RANGE_MAX_INTEGER:I

    move v7, v1

    move v8, v1

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 381
    :cond_d
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "MMC3140MS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 382
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x266

    const/16 v4, 0x266

    const/16 v6, 0x266

    move v1, v7

    move v3, v7

    move v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 383
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x266

    const/16 v4, 0x266

    const/16 v6, 0x266

    move v1, v7

    move v3, v7

    move v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 384
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 385
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 388
    :cond_e
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "MMC3280MS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 389
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x266

    const/16 v4, 0x266

    const/16 v6, 0x266

    move v1, v7

    move v3, v7

    move v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 390
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v2, 0x266

    const/16 v4, 0x266

    const/16 v6, 0x266

    move v1, v7

    move v3, v7

    move v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 391
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 392
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 396
    :cond_f
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AMOTECH"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 397
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 398
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 399
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 400
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 404
    :cond_10
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC022"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 405
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 406
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x1000

    const/16 v4, 0x1000

    const/16 v5, -0x1000

    const/16 v6, 0x1000

    const/16 v7, -0x4000

    const/16 v8, 0x4000

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 407
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 408
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 411
    :cond_11
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC050"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 412
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 413
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x1000

    const/16 v4, 0x1000

    const/16 v5, -0x1000

    const/16 v6, 0x1000

    const/16 v7, -0x4000

    const/16 v8, 0x4000

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 414
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 415
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 417
    :cond_12
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 418
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 420
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x800

    const/16 v4, 0x800

    const/16 v5, -0x800

    const/16 v6, 0x800

    const/16 v7, -0x2000

    const/16 v8, 0x2000

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 421
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 422
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 424
    :cond_13
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150_POWER_NOISE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 425
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 427
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x28a0

    const/16 v4, 0x28a0

    const/16 v5, -0x28a0

    const/16 v6, 0x28a0

    const/16 v7, -0x4e20

    const/16 v8, 0x4e20

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 428
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 429
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 431
    :cond_14
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150_COMBINATION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 432
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 434
    new-instance v2, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v3, -0x28a0

    const/16 v4, 0x28a0

    const/16 v5, -0x28a0

    const/16 v6, 0x28a0

    const/16 v7, -0x4e20

    const/16 v8, 0x4e20

    invoke-direct/range {v2 .. v8}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 435
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 436
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 440
    :cond_15
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD004A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 441
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 442
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x7d0

    const/16 v2, 0x7d0

    const/16 v3, -0x7d0

    const/16 v4, 0x7d0

    const/16 v5, -0x7d0

    const/16 v6, 0x7d0

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 443
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 444
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 447
    :cond_16
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD006A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 448
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 449
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x7d0

    const/16 v2, 0x7d0

    const/16 v3, -0x7d0

    const/16 v4, 0x7d0

    const/16 v5, -0x7d0

    const/16 v6, 0x7d0

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 450
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 451
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 452
    :cond_17
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD008A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 453
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 454
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x2000

    const/16 v2, 0x2000

    const/16 v3, -0x2000

    const/16 v4, 0x2000

    const/16 v5, -0x2000

    const/16 v6, 0x2000

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 455
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 456
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 457
    :cond_18
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "STMICRO_K303C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 458
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 459
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x4000

    const/16 v2, 0x4000

    const/16 v3, -0x4000

    const/16 v4, 0x4000

    const/16 v5, -0x4000

    const/16 v6, 0x4000

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 460
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 461
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Range;

    const/16 v1, -0x140d

    const/16 v2, -0x6af

    const/16 v3, -0x140d

    const/16 v4, -0x6af

    const/16 v5, -0x6af

    const/16 v6, -0xab

    invoke-direct/range {v0 .. v6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0

    .line 465
    :cond_19
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 466
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 467
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    .line 468
    iput-object v13, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    goto/16 :goto_0
.end method

.method private setSpecProximity()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 473
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 474
    iput-object v4, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    .line 521
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "SHARP_GP2AP002S00F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 481
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "SHARP_GP2AP002A00F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 485
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v2, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 488
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "SHARP_GP2AP030A00F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 489
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 493
    :cond_3
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "CAPELLA_CM3663"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 494
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 497
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "CAPELLA_CM36691"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 498
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v3, v3, v3}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 501
    :cond_5
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "CAPELLA_CM36651"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 502
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v3, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 506
    :cond_6
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "TAOS_TMD2672x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 507
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto :goto_0

    .line 510
    :cond_7
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "TAOS_TMD2771x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 511
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto/16 :goto_0

    .line 514
    :cond_8
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mFeature_Proximity:Ljava/lang/String;

    const-string v1, "TAOS_TMD2772x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 515
    new-instance v0, Lcom/sec/factory/modules/SensorSpec$Proximity;

    invoke-direct {v0, v2, v3, v2}, Lcom/sec/factory/modules/SensorSpec$Proximity;-><init>(ZZZ)V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto/16 :goto_0

    .line 519
    :cond_9
    iput-object v4, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    goto/16 :goto_0
.end method


# virtual methods
.method public getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mAccelerometer:Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    return-object v0
.end method

.method public getSpecGeomagnetic_ADC()Lcom/sec/factory/modules/SensorSpec$Range;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC:Lcom/sec/factory/modules/SensorSpec$Range;

    return-object v0
.end method

.method public getSpecGeomagnetic_ADC2()Lcom/sec/factory/modules/SensorSpec$Range;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeADC2:Lcom/sec/factory/modules/SensorSpec$Range;

    return-object v0
.end method

.method public getSpecGeomagnetic_DAC()Lcom/sec/factory/modules/SensorSpec$Range;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeDAC:Lcom/sec/factory/modules/SensorSpec$Range;

    return-object v0
.end method

.method public getSpecGeomagnetic_Self()Lcom/sec/factory/modules/SensorSpec$Range;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mMagneticRangeSelf:Lcom/sec/factory/modules/SensorSpec$Range;

    return-object v0
.end method

.method public getSpecLight()Lcom/sec/factory/modules/SensorSpec$Light;
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mLight:Lcom/sec/factory/modules/SensorSpec$Light;

    return-object v0
.end method

.method public getSpecProximity()Lcom/sec/factory/modules/SensorSpec$Proximity;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec;->mProximity:Lcom/sec/factory/modules/SensorSpec$Proximity;

    return-object v0
.end method
