.class Lcom/sec/factory/modules/SensorReadManager$SensorListener;
.super Ljava/lang/Object;
.source "SensorReadManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorReadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/SensorReadManager;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/SensorReadManager;)V
    .locals 3

    .prologue
    .line 562
    iput-object p1, p0, Lcom/sec/factory/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/factory/modules/SensorReadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563
    const-string v0, "SensorReadManager"

    const-string v1, "SensorListener"

    const-string v2, "NEW"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 568
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 572
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 617
    :goto_0
    return-void

    .line 574
    :sswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$002([F)[F

    goto :goto_0

    .line 578
    :sswitch_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$102([F)[F

    .line 579
    const-string v1, "SensorReadManager"

    const-string v2, "onSensorChanged-TYPE_PRESSURE"

    # getter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F
    invoke-static {}, Lcom/sec/factory/modules/SensorReadManager;->access$100()[F

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F
    invoke-static {}, Lcom/sec/factory/modules/SensorReadManager;->access$100()[F

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "null"

    goto :goto_1

    .line 582
    :sswitch_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Gyro:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$202([F)[F

    goto :goto_0

    .line 585
    :sswitch_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Light:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$302([F)[F

    goto :goto_0

    .line 588
    :sswitch_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$402([F)[F

    goto :goto_0

    .line 591
    :sswitch_5
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Proximity:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$502([F)[F

    goto :goto_0

    .line 594
    :sswitch_6
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Temperature:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$602([F)[F

    goto :goto_0

    .line 597
    :sswitch_7
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Humidity:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$702([F)[F

    goto :goto_0

    .line 600
    :sswitch_8
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Bio:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$802([F)[F

    goto/16 :goto_0

    .line 603
    :sswitch_9
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Hrm:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$902([F)[F

    goto/16 :goto_0

    .line 606
    :sswitch_a
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Grip:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$1002([F)[F

    goto/16 :goto_0

    .line 609
    :sswitch_b
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_BodyTemp:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$1102([F)[F

    goto/16 :goto_0

    .line 612
    :sswitch_c
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_UV:[F
    invoke-static {v0}, Lcom/sec/factory/modules/SensorReadManager;->access$1202([F)[F

    goto/16 :goto_0

    .line 572
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_4
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_1
        0x8 -> :sswitch_5
        0xc -> :sswitch_7
        0xd -> :sswitch_6
        0x10018 -> :sswitch_a
        0x10019 -> :sswitch_8
        0x1001a -> :sswitch_9
        0x1001d -> :sswitch_c
        0x1001e -> :sswitch_b
    .end sparse-switch
.end method
