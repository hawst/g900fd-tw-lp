.class public Lcom/sec/factory/modules/SensorSpec$Proximity;
.super Ljava/lang/Object;
.source "SensorSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proximity"
.end annotation


# instance fields
.field public mIsSupportADC:Z

.field public mIsSupportOffset:Z

.field public mIsSupportZeroDistance:Z


# direct methods
.method public constructor <init>(ZZZ)V
    .locals 0
    .param p1, "isSupportOffset"    # Z
    .param p2, "isSupportADC"    # Z
    .param p3, "isSupportZeroDistance"    # Z

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-boolean p1, p0, Lcom/sec/factory/modules/SensorSpec$Proximity;->mIsSupportOffset:Z

    .line 90
    iput-boolean p2, p0, Lcom/sec/factory/modules/SensorSpec$Proximity;->mIsSupportADC:Z

    .line 91
    iput-boolean p3, p0, Lcom/sec/factory/modules/SensorSpec$Proximity;->mIsSupportZeroDistance:Z

    .line 92
    return-void
.end method
