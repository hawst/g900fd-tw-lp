.class public Lcom/sec/factory/modules/check/CheckCxDataGap;
.super Ljava/lang/Object;
.source "CheckCxDataGap.java"


# static fields
.field public static final DIRECTION_ADJACENT_X:I = 0x3e8

.field public static final DIRECTION_ADJACENT_Y:I = 0x3e9


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private _gapMax:I

.field private _nodes:[[I

.field private xChannel:I

.field private yChannel:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, "CheckCxDataGap"

    iput-object v0, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->CLASS_NAME:Ljava/lang/String;

    .line 14
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->xChannel:I

    .line 15
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->yChannel:I

    .line 21
    iput p1, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->xChannel:I

    .line 22
    iput p2, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->yChannel:I

    .line 23
    return-void
.end method

.method private computeGap(II)I
    .locals 1
    .param p1, "lhs"    # I
    .param p2, "rhs"    # I

    .prologue
    .line 26
    sub-int v0, p1, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method private makeNearNodeGap([[II)[[I
    .locals 10
    .param p1, "temp"    # [[I
    .param p2, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_nodes:[[I

    .line 39
    const/4 v3, 0x0

    check-cast v3, [[I

    .line 41
    .local v3, "result":[[I
    :try_start_0
    iget v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->yChannel:I

    iget v8, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->xChannel:I

    filled-new-array {v7, v8}, [I

    move-result-object v7

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v8, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, [[I

    move-object v3, v0

    .line 43
    packed-switch p2, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    return-object v3

    .line 45
    :pswitch_0
    const/4 v6, 0x0

    .local v6, "y":I
    :goto_1
    iget v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->yChannel:I

    if-ge v6, v7, :cond_0

    .line 46
    const/4 v5, 0x0

    .local v5, "x":I
    :goto_2
    iget v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->xChannel:I

    add-int/lit8 v7, v7, -0x1

    if-ge v5, v7, :cond_1

    .line 47
    iget-object v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_nodes:[[I

    aget-object v7, v7, v6

    aget v2, v7, v5

    .line 48
    .local v2, "lhs":I
    iget-object v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_nodes:[[I

    aget-object v7, v7, v6

    add-int/lit8 v8, v5, 0x1

    aget v4, v7, v8

    .line 49
    .local v4, "rhs":I
    aget-object v7, v3, v6

    invoke-direct {p0, v2, v4}, Lcom/sec/factory/modules/check/CheckCxDataGap;->computeGap(II)I

    move-result v8

    aput v8, v7, v5

    .line 46
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 45
    .end local v2    # "lhs":I
    .end local v4    # "rhs":I
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 55
    .end local v5    # "x":I
    .end local v6    # "y":I
    :pswitch_1
    const/4 v6, 0x0

    .restart local v6    # "y":I
    :goto_3
    iget v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->yChannel:I

    add-int/lit8 v7, v7, -0x1

    if-ge v6, v7, :cond_0

    .line 56
    const/4 v5, 0x0

    .restart local v5    # "x":I
    :goto_4
    iget v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->xChannel:I

    if-ge v5, v7, :cond_2

    .line 57
    iget-object v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_nodes:[[I

    aget-object v7, v7, v6

    aget v2, v7, v5

    .line 58
    .restart local v2    # "lhs":I
    iget-object v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_nodes:[[I

    add-int/lit8 v8, v6, 0x1

    aget-object v7, v7, v8

    aget v4, v7, v5

    .line 59
    .restart local v4    # "rhs":I
    aget-object v7, v3, v6

    invoke-direct {p0, v2, v4}, Lcom/sec/factory/modules/check/CheckCxDataGap;->computeGap(II)I

    move-result v8

    aput v8, v7, v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 55
    .end local v2    # "lhs":I
    .end local v4    # "rhs":I
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 68
    .end local v5    # "x":I
    .end local v6    # "y":I
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "makeNearNodeGap"

    const-string v9, "NumberFormatException"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getMaxAdjacentNodeGap([[II)I
    .locals 14
    .param p1, "temp"    # [[I
    .param p2, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct/range {p0 .. p2}, Lcom/sec/factory/modules/check/CheckCxDataGap;->makeNearNodeGap([[II)[[I

    move-result-object v8

    .line 79
    .local v8, "nearGap":[[I
    const/4 v7, 0x0

    .line 81
    .local v7, "max":I
    move-object v0, v8

    .local v0, "arr$":[[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v0    # "arr$":[[I
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 82
    .local v6, "linear":[I
    move-object v1, v6

    .local v1, "arr$":[I
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .end local v3    # "i$":I
    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v5, :cond_1

    aget v9, v1, v2

    .line 83
    .local v9, "value":I
    if-le v9, v7, :cond_0

    .line 84
    move v7, v9

    .line 85
    iget-object v10, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getMaxAdjacentNodeGap"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "max: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 81
    .end local v9    # "value":I
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_0

    .line 90
    .end local v1    # "arr$":[I
    .end local v5    # "len$":I
    .end local v6    # "linear":[I
    :cond_2
    iget-object v10, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getMaxAdjacentNodeGap"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "max: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iput v7, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_gapMax:I

    .line 92
    iget v10, p0, Lcom/sec/factory/modules/check/CheckCxDataGap;->_gapMax:I

    return v10
.end method
