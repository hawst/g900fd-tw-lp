.class Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;
.super Ljava/lang/Object;
.source "TouchScreenPanel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/TouchScreenPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TSPThreadRawData"
.end annotation


# instance fields
.field mNotiHandler:Landroid/os/Handler;

.field subCommand:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/factory/modules/TouchScreenPanel;

.field tspID:I


# direct methods
.method private constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;)V
    .locals 0

    .prologue
    .line 855
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p2, "x1"    # Lcom/sec/factory/modules/TouchScreenPanel$1;

    .prologue
    .line 855
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->setParameter(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;Landroid/os/Handler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 855
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method private setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 861
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    .line 862
    return-void
.end method

.method private setParameter(ILjava/lang/String;)V
    .locals 0
    .param p1, "ID"    # I
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 865
    iput p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    .line 866
    iput-object p2, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->subCommand:Ljava/lang/String;

    .line 867
    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 870
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "TSP ID (Main) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    move/from16 v19, v0

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->mapping_IDnCommand(I)Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$000(Lcom/sec/factory/modules/TouchScreenPanel;I)Ljava/lang/String;

    move-result-object v4

    .line 873
    .local v4, "command":Ljava/lang/String;
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Main Command : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sub Command : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->subCommand:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const/4 v11, 0x0

    .line 876
    .local v11, "status":Ljava/lang/String;
    const/4 v9, 0x0

    .line 877
    .local v9, "result":Ljava/lang/String;
    const/4 v10, 0x0

    check-cast v10, [[I

    .line 880
    .local v10, "ret":[[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountX()Ljava/lang/String;

    move-result-object v12

    .line 881
    .local v12, "tmpX":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountY()Ljava/lang/String;

    move-result-object v13

    .line 882
    .local v13, "tmpY":Ljava/lang/String;
    const-string v18, "TSP_NODE_COUNT_WIDTH"

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 883
    .local v15, "xChannel":I
    const-string v18, "TSP_NODE_COUNT_HEIGHT"

    invoke-static/range {v18 .. v18}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 886
    .local v17, "yChannel":I
    :try_start_0
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 887
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    .line 892
    :goto_0
    const-string v18, "NA"

    move-object/from16 v0, v18

    if-eq v4, v0, :cond_4

    .line 894
    move/from16 v0, v17

    filled-new-array {v15, v0}, [I

    move-result-object v18

    sget-object v19, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ret":[[I
    check-cast v10, [[I

    .line 896
    .restart local v10    # "ret":[[I
    const/16 v16, 0x0

    .local v16, "y":I
    :goto_1
    move/from16 v0, v16

    if-ge v0, v15, :cond_1

    .line 897
    const/4 v14, 0x0

    .local v14, "x":I
    :goto_2
    move/from16 v0, v17

    if-ge v14, v0, :cond_0

    .line 898
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 900
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ","

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v4, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 902
    aget-object v18, v10, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v4, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$400(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    aput v19, v18, v14

    .line 903
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 897
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 888
    .end local v14    # "x":I
    .end local v16    # "y":I
    :catch_0
    move-exception v8

    .line 889
    .local v8, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 903
    .end local v8    # "ne":Ljava/lang/NumberFormatException;
    .restart local v14    # "x":I
    .restart local v16    # "y":I
    :catchall_0
    move-exception v18

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v18
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 914
    .end local v14    # "x":I
    :catch_1
    move-exception v7

    .line 916
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 918
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_3
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "status : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "result : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    if-eqz v11, :cond_2

    const-string v18, "OK"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 922
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_OK"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_OK:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v9}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 943
    .end local v16    # "y":I
    :goto_4
    return-void

    .line 896
    .restart local v14    # "x":I
    .restart local v16    # "y":I
    :cond_0
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 907
    .end local v14    # "x":I
    :cond_1
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/factory/modules/check/CheckTSPRawCap;

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-direct {v0, v15, v1}, Lcom/sec/factory/modules/check/CheckTSPRawCap;-><init>(II)V

    # setter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckTSPRawCap:Lcom/sec/factory/modules/check/CheckTSPRawCap;
    invoke-static/range {v18 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$502(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/check/CheckTSPRawCap;)Lcom/sec/factory/modules/check/CheckTSPRawCap;

    .line 908
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckTSPRawCap:Lcom/sec/factory/modules/check/CheckTSPRawCap;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$500(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckTSPRawCap;

    move-result-object v18

    const/16 v19, 0x3e8

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v10, v1}, Lcom/sec/factory/modules/check/CheckTSPRawCap;->getMaxAdjacentNodeGap([[II)F

    move-result v5

    .line 909
    .local v5, "dataX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckTSPRawCap:Lcom/sec/factory/modules/check/CheckTSPRawCap;
    invoke-static/range {v18 .. v18}, Lcom/sec/factory/modules/TouchScreenPanel;->access$500(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckTSPRawCap;

    move-result-object v18

    const/16 v19, 0x3e9

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v10, v1}, Lcom/sec/factory/modules/check/CheckTSPRawCap;->getMaxAdjacentNodeGap([[II)F

    move-result v6

    .line 910
    .local v6, "dataY":F
    const-string v18, "TouchScreenPanel"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "dataX : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "dataY : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "%.3f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.3f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v9

    goto/16 :goto_3

    .line 926
    .end local v5    # "dataX":F
    .end local v6    # "dataY":F
    :cond_2
    if-eqz v11, :cond_3

    const-string v18, "NOT_APPLICABLE"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 927
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 932
    :cond_3
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_NG"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NG:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 938
    .end local v16    # "y":I
    :cond_4
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "command==null"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    const-string v18, "TouchScreenPanel"

    const-string v19, "TSPStatus.run"

    const-string v20, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    sget v20, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->tspID:I

    move/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v19 .. v22}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4
.end method
