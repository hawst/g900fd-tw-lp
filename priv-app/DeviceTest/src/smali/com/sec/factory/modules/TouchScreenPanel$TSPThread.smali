.class Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;
.super Ljava/lang/Object;
.source "TouchScreenPanel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/TouchScreenPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TSPThread"
.end annotation


# instance fields
.field mNotiHandler:Landroid/os/Handler;

.field subCommand:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/factory/modules/TouchScreenPanel;

.field tspID:I


# direct methods
.method private constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;)V
    .locals 0

    .prologue
    .line 797
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p2, "x1"    # Lcom/sec/factory/modules/TouchScreenPanel$1;

    .prologue
    .line 797
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;Landroid/os/Handler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 797
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 797
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->setParameter(ILjava/lang/String;)V

    return-void
.end method

.method private setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 803
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    .line 804
    return-void
.end method

.method private setParameter(ILjava/lang/String;)V
    .locals 0
    .param p1, "ID"    # I
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 807
    iput p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    .line 808
    iput-object p2, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->subCommand:Ljava/lang/String;

    .line 809
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 812
    const/4 v2, 0x0

    .line 813
    .local v2, "status":Ljava/lang/String;
    const/4 v1, 0x0

    .line 814
    .local v1, "result":Ljava/lang/String;
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TSP ID (Main) : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    invoke-static {v6}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    iget v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->mapping_IDnCommand(I)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/factory/modules/TouchScreenPanel;->access$000(Lcom/sec/factory/modules/TouchScreenPanel;I)Ljava/lang/String;

    move-result-object v0

    .line 817
    .local v0, "command":Ljava/lang/String;
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Main Command : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sub Command : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->subCommand:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const-string v3, "NA"

    if-eq v0, v3, :cond_2

    .line 821
    monitor-enter p0

    .line 823
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    iget-object v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->subCommand:Ljava/lang/String;

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v0, v4}, Lcom/sec/factory/modules/TouchScreenPanel;->access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 825
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    iget-object v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->subCommand:Ljava/lang/String;

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v0, v4}, Lcom/sec/factory/modules/TouchScreenPanel;->access$200(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 826
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 831
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    const-string v5, "sendMessage == TSP_WHAT_STATUS_OK"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    sget v5, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_OK:I

    iget v6, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    invoke-virtual {v4, v5, v6, v7, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 852
    :goto_0
    return-void

    .line 826
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 835
    :cond_0
    const-string v3, "NOT_APPLICABLE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 836
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    const-string v5, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    sget v5, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    iget v6, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 841
    :cond_1
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    const-string v5, "sendMessage == TSP_WHAT_STATUS_NG"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    sget v5, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NG:I

    iget v6, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 847
    :cond_2
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    const-string v5, "command==null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const-string v3, "TouchScreenPanel"

    const-string v4, "TSPStatus.run"

    const-string v5, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    iget-object v3, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->mNotiHandler:Landroid/os/Handler;

    sget v5, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    iget v6, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->tspID:I

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
