.class public Lcom/sec/factory/modules/SensorReadIntent;
.super Ljava/lang/Object;
.source "SensorReadIntent.java"


# instance fields
.field private final ACCELERMETER_INTENT_ACTION_READ:Ljava/lang/String;

.field private final ACCELERMETER_INTENT_ACTION_STOP:Ljava/lang/String;

.field private final ACCEL_COUNT_MAX:I

.field private final CLASS_NAME:Ljava/lang/String;

.field private final CP_INTENT_ACTION_READ:Ljava/lang/String;

.field private DUMMY:I

.field private final GRIP_COUNT_MAX:I

.field private final GRIP_INTENT_ACTION_READ:Ljava/lang/String;

.field private final GRIP_INTENT_ACTION_READ_LTE:Ljava/lang/String;

.field private final GRIP_INTENT_ACTION_READ_OTHER:Ljava/lang/String;

.field private final GRIP_INTENT_ACTION_STOP:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_NAME:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2N3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2N3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_3:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0000:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0100:Ljava/lang/String;

.field private final GRIP_VALUE__INTENT_EXTRA_VALUE__RELEASE_ALL:Ljava/lang/String;

.field private final SENSOR_ON_ARRAY_INDEX_GRIP:I

.field private final SENSOR_ON_ARRAY_LENGTH:I

.field private count_Grip:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBuffer_SensorValue_CPsAccelerometer:[I

.field private mBuffer_SensorValue_Grip:[I

.field private mContext:Landroid/content/Context;

.field private mModuleSensorIDArray:[I

.field private mSensorOn:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;[I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "moduleSensorID"    # [I

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v2, "SensorReadIntent"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->CLASS_NAME:Ljava/lang/String;

    .line 18
    iput v6, p0, Lcom/sec/factory/modules/SensorReadIntent;->DUMMY:I

    .line 19
    iget v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->DUMMY:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/sec/factory/modules/SensorReadIntent;->DUMMY:I

    iput v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    .line 21
    iget v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->DUMMY:I

    iput v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    .line 22
    iget v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mSensorOn:[Z

    .line 28
    const-string v2, "com.sec.android.app.factorytest"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->CP_INTENT_ACTION_READ:Ljava/lang/String;

    .line 29
    const-string v2, "030005"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_INTENT_ACTION_READ:Ljava/lang/String;

    .line 30
    const-string v2, "03000b"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_INTENT_ACTION_READ_LTE:Ljava/lang/String;

    .line 31
    const-string v2, "07000b"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_INTENT_ACTION_READ_OTHER:Ljava/lang/String;

    .line 32
    const-string v2, "07000c"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->ACCELERMETER_INTENT_ACTION_READ:Ljava/lang/String;

    .line 39
    const-string v2, "com.android.samsungtest.GripTestStop"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_INTENT_ACTION_STOP:Ljava/lang/String;

    .line 40
    const-string v2, "COMMAND"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_NAME:Ljava/lang/String;

    .line 41
    const-string v2, "010000000000"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1:Ljava/lang/String;

    .line 44
    const-string v2, "000001000000"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2:Ljava/lang/String;

    .line 47
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_3:Ljava/lang/String;

    .line 49
    const-string v2, "010001000000"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2:Ljava/lang/String;

    .line 54
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N3:Ljava/lang/String;

    .line 56
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_2N3:Ljava/lang/String;

    .line 58
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_1N2N3:Ljava/lang/String;

    .line 60
    const-string v2, "000000000000"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__RELEASE_ALL:Ljava/lang/String;

    .line 66
    const-string v2, "0100"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0100:Ljava/lang/String;

    .line 67
    const-string v2, "0000"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_VALUE__INTENT_EXTRA_VALUE__DETECT_SENSOR_HC_0000:Ljava/lang/String;

    .line 74
    const-string v2, "com.android.samsungtest.CpAccelermeterOff"

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->ACCELERMETER_INTENT_ACTION_STOP:Ljava/lang/String;

    .line 75
    iput v5, p0, Lcom/sec/factory/modules/SensorReadIntent;->ACCEL_COUNT_MAX:I

    .line 78
    iput v5, p0, Lcom/sec/factory/modules/SensorReadIntent;->GRIP_COUNT_MAX:I

    .line 82
    iput-object v4, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    .line 83
    iput-object v4, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    .line 212
    new-instance v2, Lcom/sec/factory/modules/SensorReadIntent$1;

    invoke-direct {v2, p0}, Lcom/sec/factory/modules/SensorReadIntent$1;-><init>(Lcom/sec/factory/modules/SensorReadIntent;)V

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 88
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "Sensor On"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iput-object p1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    .line 92
    new-array v2, v5, [I

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    .line 95
    new-array v2, v5, [I

    iput-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    .line 97
    const/4 v1, -0x1

    .line 99
    .local v1, "sensorOnArrayIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v2, :cond_0

    .line 100
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mSensorOn:[Z

    aput-boolean v6, v2, v0

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 104
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne v2, v3, :cond_3

    .line 105
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mSensorOn:[Z

    iget v3, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    aget-boolean v2, v2, v3

    if-nez v2, :cond_2

    .line 106
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "Grip Sensor"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadIntent;->ConverterID(I)I

    move-result v1

    .line 117
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mSensorOn:[Z

    const/4 v3, 0x1

    aput-boolean v3, v2, v1

    .line 118
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorReadIntent;->registerReceiver_Grip()V

    .line 103
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 121
    :cond_2
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "Grip Sensor - On"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 125
    :cond_3
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    aget v2, v2, v0

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne v2, v3, :cond_1

    .line 126
    const-string v2, "SensorReadIntent"

    const-string v3, "SensorReadIntent"

    const-string v4, "CP Accelerometer Sensor - On"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorReadIntent;->registerReceiver_CPAccelerometer()V

    goto :goto_2

    .line 133
    :cond_4
    return-void
.end method

.method private ConverterID(I)I
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 363
    const/4 v0, -0x1

    .line 365
    .local v0, "sensorOnArrayIndex":I
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p1, v1, :cond_0

    .line 366
    iget v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    .line 371
    :cond_0
    return v0
.end method

.method static synthetic access$000(Lcom/sec/factory/modules/SensorReadIntent;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/SensorReadIntent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadIntent;->setValueGrip(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/SensorReadIntent;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/SensorReadIntent;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadIntent;->setValueCPsAccelerometerData(Ljava/lang/String;)V

    return-void
.end method

.method private dataCheck([I)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [I

    .prologue
    .line 377
    const-string v1, ""

    .line 379
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 380
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 381
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 383
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 384
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 380
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 388
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 391
    :cond_2
    return-object v1
.end method

.method private registerReceiver_CPAccelerometer()V
    .locals 3

    .prologue
    .line 198
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 199
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 200
    const-string v1, "com.android.samsungtest.CpAccelermeterOff"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 202
    return-void
.end method

.method private registerReceiver_Grip()V
    .locals 3

    .prologue
    .line 183
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 184
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 185
    const-string v1, "com.android.samsungtest.GripTestStop"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 187
    return-void
.end method

.method private setValueCPsAccelerometerData(Ljava/lang/String;)V
    .locals 11
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x4

    const/4 v10, 0x1

    const/16 v9, 0x10

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "xData":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x6

    const/16 v5, 0x8

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "yData":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0xa

    const/16 v5, 0xc

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x8

    const/16 v5, 0xa

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 337
    .local v2, "zData":Ljava/lang/String;
    const-string v3, "SensorReadIntent"

    const-string v4, "setValueCPsAccelerometerData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "xData=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],yData=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],zData=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    invoke-static {v1, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-short v4, v4

    mul-int/lit8 v4, v4, -0x1

    aput v4, v3, v8

    .line 341
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    invoke-static {v0, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-short v4, v4

    mul-int/lit8 v4, v4, -0x1

    aput v4, v3, v10

    .line 343
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    invoke-static {v2, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-short v4, v4

    aput v4, v3, v7

    .line 344
    const-string v3, "SensorReadIntent"

    const-string v4, "setValueCPsAccelerometerData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "x=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    aget v6, v6, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],y=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    aget v6, v6, v10

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "],z=["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method private setValueGrip(Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 257
    const-string v0, "SensorReadIntent"

    const-string v1, "setValueGrip"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v0, "34"

    invoke-static {v0}, Lcom/sec/factory/support/Support$FactoryTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->count_Grip:I

    .line 260
    const-string v0, "010000000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v4

    .line 262
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v5

    .line 263
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    .line 310
    :goto_0
    iget v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->count_Grip:I

    if-ne v0, v5, :cond_2

    .line 311
    const-string v0, "010000000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "000001000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v4

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v5

    .line 317
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    .line 319
    :cond_2
    return-void

    .line 264
    :cond_3
    const-string v0, "000001000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 265
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v4

    .line 266
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v5

    .line 267
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    goto :goto_0

    .line 268
    :cond_4
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 269
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v4

    .line 270
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v5

    .line 271
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v6

    goto :goto_0

    .line 272
    :cond_5
    const-string v0, "010001000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 273
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v4

    .line 274
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v5

    .line 275
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    goto :goto_0

    .line 276
    :cond_6
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 277
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v4

    .line 278
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v5

    .line 279
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v6

    goto :goto_0

    .line 280
    :cond_7
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 281
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v4

    .line 282
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v5

    .line 283
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v6

    goto/16 :goto_0

    .line 284
    :cond_8
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 285
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v4

    .line 286
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v5

    .line 287
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v6

    goto/16 :goto_0

    .line 288
    :cond_9
    const-string v0, "000000000000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 289
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v4

    .line 290
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v5

    .line 291
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    goto/16 :goto_0

    .line 295
    :cond_a
    const-string v0, "0100"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 296
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v4

    .line 297
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v5

    .line 298
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    goto/16 :goto_0

    .line 299
    :cond_b
    const-string v0, "0000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 300
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v5, v0, v4

    .line 301
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v5

    .line 302
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    aput v4, v0, v6

    goto/16 :goto_0

    .line 307
    :cond_c
    const-string v0, "SensorReadIntent"

    const-string v1, "setValueGrip"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private unregisterReceiver_CPAccelerometer()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 208
    :cond_0
    return-void
.end method

.method private unregisterReceiver_Grip()V
    .locals 3

    .prologue
    .line 190
    const-string v0, "SensorReadIntent"

    const-string v1, "unregisterReceiver_Grip"

    const-string v2, "unregisterReceiver_Grip"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 195
    :cond_0
    return-void
.end method


# virtual methods
.method public disableReceiver_CPsAccelerometer()V
    .locals 3

    .prologue
    .line 166
    const-string v0, "SensorReadIntent"

    const-string v1, "disableBroadcastReceiverCPsAccelerometer"

    const-string v2, "CP\'s Acceler... OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    .line 168
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorReadIntent;->unregisterReceiver_CPAccelerometer()V

    .line 169
    return-void
.end method

.method public isSensorOn(I)Z
    .locals 2
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadIntent;->ConverterID(I)I

    move-result v0

    .line 177
    .local v0, "sensorOnArrayIndex":I
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mSensorOn:[Z

    aget-boolean v1, v1, v0

    return v1
.end method

.method public returnCPsAccelerometerData()[I
    .locals 3

    .prologue
    .line 351
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 352
    const-string v0, "SensorReadIntent"

    const-string v1, "returnCPsAccelerometerData"

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadIntent;->dataCheck([I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_CPsAccelerometer:[I

    return-object v0
.end method

.method public returnGrip()[I
    .locals 3

    .prologue
    .line 324
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 325
    const-string v0, "SensorReadIntent"

    const-string v1, "returnGrip"

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadIntent;->dataCheck([I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    return-object v0
.end method

.method public sensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 136
    const-string v1, "SensorReadIntent"

    const-string v2, "sensorOff"

    const-string v3, "Grip Sensor Off"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 139
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    aget v1, v1, v0

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne v1, v2, :cond_0

    .line 140
    const-string v1, "SensorReadIntent"

    const-string v2, "sensorOff"

    const-string v3, "Grip Sensor"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iput-object v4, p0, Lcom/sec/factory/modules/SensorReadIntent;->mBuffer_SensorValue_Grip:[I

    .line 150
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorReadIntent;->unregisterReceiver_Grip()V

    .line 138
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadIntent;->mSensorOn:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :cond_2
    iput-object v4, p0, Lcom/sec/factory/modules/SensorReadIntent;->mModuleSensorIDArray:[I

    .line 162
    return-void
.end method
