.class public Lcom/sec/factory/modules/check/CheckCxDataAll;
.super Ljava/lang/Object;
.source "CheckCxDataAll.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "CxDataAllNode"


# instance fields
.field private _cxDataArr:[[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method


# virtual methods
.method public getMinMaxCxData([[I)[I
    .locals 17
    .param p1, "ref"    # [[I

    .prologue
    .line 16
    const/4 v13, 0x2

    new-array v11, v13, [I

    .line 17
    .local v11, "result":[I
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/modules/check/CheckCxDataAll;->_cxDataArr:[[I

    .line 19
    const v5, 0x1869f

    .line 20
    .local v5, "cxDataMin":I
    const/4 v4, 0x0

    .line 22
    .local v4, "cxDataMax":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/factory/modules/check/CheckCxDataAll;->_cxDataArr:[[I

    .local v2, "arr$":[[I
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v2    # "arr$":[[I
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_3

    aget-object v10, v2, v7

    .line 23
    .local v10, "linear":[I
    move-object v3, v10

    .local v3, "arr$":[I
    array-length v9, v3

    .local v9, "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_1
    if-ge v6, v9, :cond_2

    aget v12, v3, v6

    .line 24
    .local v12, "value":I
    if-le v12, v4, :cond_0

    .line 25
    move v4, v12

    .line 26
    const-string v13, "CxDataAllNode"

    const-string v14, "getMinMaxCxData"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cxDataMax: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    :cond_0
    if-ge v12, v5, :cond_1

    .line 30
    move v5, v12

    .line 31
    const-string v13, "CxDataAllNode"

    const-string v14, "getMinMaxCxData"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cxDataMin: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 22
    .end local v12    # "value":I
    :cond_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_0

    .line 36
    .end local v3    # "arr$":[I
    .end local v9    # "len$":I
    .end local v10    # "linear":[I
    :cond_3
    const-string v13, "CxDataAllNode"

    const-string v14, "getMaxCxData"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cxDataMin: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", cxDataMax: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const/4 v13, 0x0

    aput v5, v11, v13

    .line 38
    const/4 v13, 0x1

    aput v4, v11, v13

    .line 40
    return-object v11
.end method
