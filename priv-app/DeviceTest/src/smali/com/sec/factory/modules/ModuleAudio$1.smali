.class Lcom/sec/factory/modules/ModuleAudio$1;
.super Ljava/lang/Object;
.source "ModuleAudio.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/modules/ModuleAudio;->startRecording(I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleAudio;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleAudio;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 133
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_4

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 169
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 172
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iput-boolean v4, v0, Lcom/sec/factory/modules/ModuleAudio;->mIsRecording:Z

    .line 177
    :cond_3
    return-void

    .line 141
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/factory/modules/ModuleAudio;->mIsRecording:Z

    .line 142
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleAudio;->mRecorder:Landroid/media/AudioRecord;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v2, v2, Lcom/sec/factory/modules/ModuleAudio;->buffer:[B

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget v3, v3, Lcom/sec/factory/modules/ModuleAudio;->mBufferSize:I

    invoke-virtual {v1, v2, v4, v3}, Landroid/media/AudioRecord;->read([BII)I

    move-result v1

    # setter for: Lcom/sec/factory/modules/ModuleAudio;->mReadBytes:I
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleAudio;->access$002(Lcom/sec/factory/modules/ModuleAudio;I)I

    .line 156
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$1;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    # getter for: Lcom/sec/factory/modules/ModuleAudio;->mThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleAudio;->access$100(Lcom/sec/factory/modules/ModuleAudio;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method
