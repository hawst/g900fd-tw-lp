.class Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;
.super Ljava/lang/Object;
.source "TouchScreenPanel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/TouchScreenPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TSPThreadCxData"
.end annotation


# instance fields
.field mNotiHandler:Landroid/os/Handler;

.field subCommand:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/factory/modules/TouchScreenPanel;

.field tspID:I


# direct methods
.method private constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;)V
    .locals 0

    .prologue
    .line 946
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p2, "x1"    # Lcom/sec/factory/modules/TouchScreenPanel$1;

    .prologue
    .line 946
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;Landroid/os/Handler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 946
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 946
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->setParameter(ILjava/lang/String;)V

    return-void
.end method

.method private setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 952
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    .line 953
    return-void
.end method

.method private setParameter(ILjava/lang/String;)V
    .locals 0
    .param p1, "ID"    # I
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 956
    iput p1, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    .line 957
    iput-object p2, p0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->subCommand:Ljava/lang/String;

    .line 958
    return-void
.end method


# virtual methods
.method public run()V
    .locals 24

    .prologue
    .line 961
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "TSP ID (Main) : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    move/from16 v20, v0

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->mapping_IDnCommand(I)Ljava/lang/String;
    invoke-static/range {v19 .. v20}, Lcom/sec/factory/modules/TouchScreenPanel;->access$000(Lcom/sec/factory/modules/TouchScreenPanel;I)Ljava/lang/String;

    move-result-object v4

    .line 964
    .local v4, "command":Ljava/lang/String;
    const-string v11, "get_cx_data"

    .line 965
    .local v11, "second_command":Ljava/lang/String;
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Main Command : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Sub Command : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->subCommand:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    const/4 v12, 0x0

    .line 968
    .local v12, "status":Ljava/lang/String;
    const/4 v9, 0x0

    .line 969
    .local v9, "result":Ljava/lang/String;
    const/4 v10, 0x0

    check-cast v10, [[I

    .line 972
    .local v10, "ret":[[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountX()Ljava/lang/String;

    move-result-object v13

    .line 973
    .local v13, "tmpX":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPChannelCountY()Ljava/lang/String;

    move-result-object v14

    .line 974
    .local v14, "tmpY":Ljava/lang/String;
    const-string v19, "TSP_NODE_COUNT_WIDTH"

    invoke-static/range {v19 .. v19}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 975
    .local v16, "xChannel":I
    const-string v19, "TSP_NODE_COUNT_HEIGHT"

    invoke-static/range {v19 .. v19}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 978
    .local v18, "yChannel":I
    :try_start_0
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 979
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    .line 984
    :goto_0
    const-string v19, "NA"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_5

    .line 987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v4, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v4, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$400(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 991
    const-string v19, "NG"

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 992
    move/from16 v0, v18

    move/from16 v1, v16

    filled-new-array {v0, v1}, [I

    move-result-object v19

    sget-object v20, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ret":[[I
    check-cast v10, [[I

    .line 994
    .restart local v10    # "ret":[[I
    const/16 v17, 0x0

    .local v17, "y":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_2

    .line 995
    const/4 v15, 0x0

    .local v15, "x":I
    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_1

    .line 996
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 998
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v11, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1000
    aget-object v19, v10, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ","

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    # invokes: Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v11, v1}, Lcom/sec/factory/modules/TouchScreenPanel;->access$400(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    aput v20, v19, v15

    .line 1001
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 995
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 980
    .end local v15    # "x":I
    .end local v17    # "y":I
    :catch_0
    move-exception v8

    .line 981
    .local v8, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1001
    .end local v8    # "ne":Ljava/lang/NumberFormatException;
    .restart local v15    # "x":I
    .restart local v17    # "y":I
    :catchall_0
    move-exception v19

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v19
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 1012
    .end local v15    # "x":I
    :catch_1
    move-exception v7

    .line 1014
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 1016
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_3
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "status : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "result : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    if-eqz v12, :cond_3

    const-string v19, "OK"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1020
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    const-string v21, "sendMessage == TSP_WHAT_STATUS_OK"

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_OK:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    move/from16 v22, v0

    const/16 v23, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3, v9}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1042
    .end local v17    # "y":I
    :cond_0
    :goto_4
    return-void

    .line 994
    .restart local v15    # "x":I
    .restart local v17    # "y":I
    :cond_1
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 1005
    .end local v15    # "x":I
    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/factory/modules/check/CheckCxDataGap;

    move-object/from16 v0, v20

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/factory/modules/check/CheckCxDataGap;-><init>(II)V

    # setter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataGap:Lcom/sec/factory/modules/check/CheckCxDataGap;
    invoke-static/range {v19 .. v20}, Lcom/sec/factory/modules/TouchScreenPanel;->access$602(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/check/CheckCxDataGap;)Lcom/sec/factory/modules/check/CheckCxDataGap;

    .line 1006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataGap:Lcom/sec/factory/modules/check/CheckCxDataGap;
    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$600(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckCxDataGap;

    move-result-object v19

    const/16 v20, 0x3e8

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/sec/factory/modules/check/CheckCxDataGap;->getMaxAdjacentNodeGap([[II)I

    move-result v5

    .line 1007
    .local v5, "dataX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->this$0:Lcom/sec/factory/modules/TouchScreenPanel;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataGap:Lcom/sec/factory/modules/check/CheckCxDataGap;
    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/TouchScreenPanel;->access$600(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckCxDataGap;

    move-result-object v19

    const/16 v20, 0x3e9

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v10, v1}, Lcom/sec/factory/modules/check/CheckCxDataGap;->getMaxAdjacentNodeGap([[II)I

    move-result v6

    .line 1008
    .local v6, "dataY":I
    const-string v19, "TouchScreenPanel"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "dataX : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "dataY : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ","

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v9

    goto/16 :goto_3

    .line 1024
    .end local v5    # "dataX":I
    .end local v6    # "dataY":I
    :cond_3
    if-eqz v12, :cond_4

    const-string v19, "NOT_APPLICABLE"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1025
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    const-string v21, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    move/from16 v22, v0

    const/16 v23, -0x1

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 1030
    :cond_4
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    const-string v21, "sendMessage == TSP_WHAT_STATUS_NG"

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NG:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    move/from16 v22, v0

    const/16 v23, -0x1

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4

    .line 1037
    .end local v17    # "y":I
    :cond_5
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    const-string v21, "command==null"

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    const-string v19, "TouchScreenPanel"

    const-string v20, "TSPStatus.run"

    const-string v21, "sendMessage == TSP_WHAT_STATUS_NA"

    invoke-static/range {v19 .. v21}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->mNotiHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->tspID:I

    move/from16 v22, v0

    const/16 v23, -0x1

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_4
.end method
