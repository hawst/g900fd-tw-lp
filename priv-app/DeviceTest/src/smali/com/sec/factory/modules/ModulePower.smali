.class public Lcom/sec/factory/modules/ModulePower;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModulePower.java"


# static fields
.field public static final AUTO_BRIGHTNESS_MODE_OFF:I = 0x0

.field public static final AUTO_BRIGHTNESS_MODE_ON:I = 0x1

.field public static final BUTTON_KEY_LIGHT:Ljava/lang/String; = "button_key_light"

.field public static final BUTTON_KEY_LIGHT_ALWAYS_ON:I = -0x1

.field public static final BUTTON_KEY_LIGHT_OFF:I = 0x0

.field public static final BUTTON_KEY_LIGHT_ON_1500:I = 0x5dc

.field public static final BUTTON_KEY_LIGHT_ON_3000:I = 0xbb8

.field public static final BUTTON_KEY_LIGHT_ON_6000:I = 0x1770

.field public static final REBOOT_MODE_DOWNLOAD:B = 0x1t

.field public static final REBOOT_MODE_NOMAL:B

.field private static mInstance:Lcom/sec/factory/modules/ModulePower;


# instance fields
.field private mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModulePower;->mInstance:Lcom/sec/factory/modules/ModulePower;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 23
    const-string v0, "ModulePower"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 60
    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 61
    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 62
    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 24
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModulePower"

    const-string v2, "Create ModulePower"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    sget-object v0, Lcom/sec/factory/modules/ModulePower;->mInstance:Lcom/sec/factory/modules/ModulePower;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/factory/modules/ModulePower;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModulePower;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModulePower;->mInstance:Lcom/sec/factory/modules/ModulePower;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModulePower;->mInstance:Lcom/sec/factory/modules/ModulePower;

    return-object v0
.end method


# virtual methods
.method public doMediaScanWakeLock(Z)V
    .locals 5
    .param p1, "wake"    # Z

    .prologue
    .line 130
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doMediaScanWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wake="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 133
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 134
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 135
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Media"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 140
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 142
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doMediaScanWakeLock"

    const-string v3, "FULL WAKELOCK ON"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 147
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 148
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doMediaScanWakeLock"

    const-string v3, "FULL WAKELOCK OFF"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mMediaScanWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method public doPartialWakeLock(Z)V
    .locals 6
    .param p1, "wake"    # Z

    .prologue
    const/4 v5, 0x1

    .line 157
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doPartialWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wake="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    if-ne p1, v5, :cond_2

    .line 160
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 161
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 162
    .local v0, "pm":Landroid/os/PowerManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Partial"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 166
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 167
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 168
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doPartialWakeLock"

    const-string v3, "PARTIAL WAKELOCK ON"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 174
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doPartialWakeLock"

    const-string v3, "PARTIAL WAKELOCK OFF"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method public doWakeLock(Z)V
    .locals 5
    .param p1, "wake"    # Z

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wake="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 106
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 107
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 108
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 113
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 114
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 115
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doWakeLock"

    const-string v3, "FULL WAKELOCK ON"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 120
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 121
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doWakeLock"

    const-string v3, "FULL WAKELOCK OFF"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method public getBrightness()I
    .locals 5

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 265
    .local v0, "brightness":I
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBrightness"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "brightness="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return v0
.end method

.method public getScreenBrightnessMode()I
    .locals 5

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 294
    .local v0, "mode":I
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getScreenBrightnessMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    return v0
.end method

.method public getTouchLedTime()I
    .locals 3

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "button_key_light"

    const/16 v2, 0x5dc

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public gotosleep()V
    .locals 4

    .prologue
    .line 97
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "goToSleep"

    const-string v3, "Power Sleep"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 99
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 100
    return-void
.end method

.method public isBatteryAuthPass()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 494
    const-string v3, "BATTERY_AUTH"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 495
    .local v0, "temp":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 496
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 498
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 496
    goto :goto_0

    :cond_1
    move v1, v2

    .line 498
    goto :goto_0
.end method

.method public isHeldWakeLock()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 186
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    goto :goto_0
.end method

.method public isScreenOn()Z
    .locals 2

    .prologue
    .line 299
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 300
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    return v1
.end method

.method public readAFChargerTemp()Ljava/lang/String;
    .locals 5

    .prologue
    .line 509
    const-string v1, "CHG_TEMP"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "temp":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 511
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readAFChargerTemp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sysfs data="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    return-object v0
.end method

.method public readAFChargerTempAdc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 503
    const-string v1, "CHG_TEMP_ADC"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "temp":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readAFChargerTempAdc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sysfs data="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    return-object v0
.end method

.method public readApChipTemp()Ljava/lang/String;
    .locals 5

    .prologue
    .line 452
    const-string v1, "APCHIP_TEMP_ADC"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->readAllLine(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "adc":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readApChipTemp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    return-object v0
.end method

.method public readBatteryAdcCal()Ljava/lang/String;
    .locals 5

    .prologue
    .line 468
    const-string v1, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 469
    .local v0, "adc_cal":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryAdcCal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adc_cal="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    return-object v0
.end method

.method public readBatteryCal()Ljava/lang/String;
    .locals 5

    .prologue
    .line 479
    const-string v1, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 480
    const-string v1, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 481
    .local v0, "cal":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryCal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cal="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    .end local v0    # "cal":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readBatteryStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    const-string v0, ""

    return-object v0
.end method

.method public readBatteryTemp()Ljava/lang/String;
    .locals 8

    .prologue
    .line 360
    const-string v4, "BATTERY_TEMP"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 361
    .local v3, "temp":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readBatteryTemp"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sysfs temp="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-lt v4, v5, :cond_1

    .line 367
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 368
    .local v1, "decimal":I
    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 370
    .local v0, "data":I
    const/4 v4, 0x5

    if-lt v1, v4, :cond_0

    .line 371
    add-int/lit8 v0, v0, 0x1

    .line 377
    .end local v1    # "decimal":I
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 382
    .end local v0    # "data":I
    :goto_1
    iget-object v4, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readBatteryTemp"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "return temp="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    return-object v3

    .line 374
    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x2

    :try_start_1
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .restart local v0    # "data":I
    goto :goto_0

    .line 378
    .end local v0    # "data":I
    :catch_0
    move-exception v2

    .line 379
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 380
    const-string v3, "0"

    goto :goto_1
.end method

.method public readBatteryTempAdc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 397
    const-string v1, "BATTERY_TEMP_ADC"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "adc":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryTempAdc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    return-object v0
.end method

.method public readBatteryTempAdcRF2Chip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryTempAdcRTRChip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 459
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryTempRF2Chip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 393
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryTempRTRChip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 388
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryVoltage()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 321
    const-string v1, "BATTERY_VOLT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "voltage":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 323
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 324
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryVoltage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "voltage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    .line 327
    :goto_1
    return-object v1

    .line 323
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public readExternalApChipTemp()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x3

    .line 403
    const-string v3, "-1"

    .line 404
    .local v3, "ext_temp":Ljava/lang/String;
    const/4 v4, 0x0

    .line 405
    .local v4, "isMinusValue":Z
    const-string v6, "SEC_EXT_THERMISTER_TEMP"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 406
    .local v5, "rawData":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readExternalApChipTemp"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sysfs value="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    if-nez v5, :cond_0

    .line 408
    const-string v6, "NG"

    .line 442
    :goto_0
    return-object v6

    .line 411
    :cond_0
    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 412
    const/4 v4, 0x1

    .line 413
    const-string v6, "-"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 414
    iget-object v6, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readExternalApChipTemp"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "replace value="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_1
    :try_start_0
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v10, :cond_4

    .line 422
    const/4 v6, 0x2

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 423
    .local v1, "decimal":I
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 425
    .local v0, "data":I
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 426
    add-int/lit8 v0, v0, 0x1

    .line 432
    .end local v1    # "decimal":I
    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    .line 433
    mul-int/lit8 v0, v0, -0x1

    .line 436
    :cond_3
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 441
    .end local v0    # "data":I
    :goto_2
    iget-object v6, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readExternalApChipTemp"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "return ext_temp="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v3

    .line 442
    goto :goto_0

    .line 429
    :cond_4
    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .restart local v0    # "data":I
    goto :goto_1

    .line 437
    .end local v0    # "data":I
    :catch_0
    move-exception v2

    .line 438
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public readFuelGaugeSOC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 349
    const-string v1, "BATTERY_CAPACITY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "soc":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 352
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readFuelGaugeSOC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "soc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    return-object v0
.end method

.method public readInBatteryVoltage()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 332
    const-string v1, "BATTERY_INBAT_VOLTAGE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, "voltage":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 334
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 335
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readInBatteryVoltage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "voltage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    .line 338
    :goto_1
    return-object v1

    .line 334
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 338
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public read_humitemp_thermistor_PAM()Ljava/lang/String;
    .locals 5

    .prologue
    .line 446
    const-string v1, "HUMITEMP_THERMISTER_PAM"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "data":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "read_humitemp_thermistor_PAM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sysfs data="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    return-object v0
.end method

.method public reboot(B)V
    .locals 6
    .param p1, "mode"    # B

    .prologue
    .line 42
    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "reboot"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 44
    .local v1, "pm":Landroid/os/PowerManager;
    new-instance v0, Lcom/sec/factory/modules/ModulePower$1;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModulePower;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/factory/modules/ModulePower$1;-><init>(Lcom/sec/factory/modules/ModulePower;Landroid/os/Looper;)V

    .line 54
    .local v0, "handler":Landroid/os/Handler;
    invoke-virtual {v0, p1, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 55
    return-void
.end method

.method public resetFuelGaugeIC()Z
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "resetFuelGaugeIC"

    const-string v2, "Fuel Gauge IC reset"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v0, "FUEL_GAUGE_RESET"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public sendAlarmManagerOnOff(Z)V
    .locals 6
    .param p1, "alarm"    # Z

    .prologue
    .line 199
    const-string v0, "android.intent.action.START_FACTORY_TEST"

    .line 200
    .local v0, "RTC_OFF":Ljava/lang/String;
    const-string v1, "android.intent.action.STOP_FACTORY_TEST"

    .line 202
    .local v1, "RTC_ON":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 203
    iget-object v3, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendAlarmOnOffIntent"

    const-string v5, "sendAlarmOnOffIntentandroid.intent.action.START_FACTORY_TEST"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_FACTORY_TEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 205
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModulePower;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 206
    :cond_0
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 207
    iget-object v3, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendAlarmOnOffIntent"

    const-string v5, "sendAlarmOnOffIntentandroid.intent.action.STOP_FACTORY_TEST"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.STOP_FACTORY_TEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 209
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModulePower;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 211
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendAlarmOnOffIntent"

    const-string v5, "Invalid parameter"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendDvfsLockIntent()V
    .locals 5

    .prologue
    .line 192
    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "sendDvfsLockIntent"

    const-string v4, "..."

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "com.sec.android.intent.action.DVFS_FACTORY_CPU_LOCK"

    .line 194
    .local v0, "dvfsLockIntent":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.DVFS_FACTORY_CPU_LOCK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 195
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->sendBroadcast(Landroid/content/Intent;)V

    .line 196
    return-void
.end method

.method public setBrightness(I)V
    .locals 5
    .param p1, "brightness"    # I

    .prologue
    .line 254
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setBrightness"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "brightness="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 257
    .local v0, "mPowerManager":Landroid/os/PowerManager;
    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 260
    :cond_0
    return-void
.end method

.method public setDisplayColor(Landroid/view/View;I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setDisplayColor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "color="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 249
    return-void
.end method

.method public setFactoryModeAtBatteryNode(Z)V
    .locals 5
    .param p1, "factoryMode"    # Z

    .prologue
    .line 216
    const-string v1, "1"

    .line 217
    .local v1, "FACTORYMODE_ON":Ljava/lang/String;
    const-string v0, "0"

    .line 219
    .local v0, "FACTORYMODE_OFF":Ljava/lang/String;
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 220
    const-string v2, "BATTRY_FACTORYMODE"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setFactoryModeAtBatteryNode"

    const-string v4, "set : 1"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setFactoryModeAtBatteryNode"

    const-string v4, "set : on Failed"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 226
    :cond_1
    const-string v2, "BATTRY_FACTORYMODE"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 227
    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setFactoryModeAtBatteryNode"

    const-string v4, "set : 0"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :cond_2
    iget-object v2, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setFactoryModeAtBatteryNode"

    const-string v4, "set off : Failed"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setScreenBrightnessMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setScreenBrightnessMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 289
    return-void
.end method

.method public setScreenState(Z)V
    .locals 6
    .param p1, "b"    # Z

    .prologue
    const/4 v5, 0x0

    .line 270
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setScreenState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 273
    .local v0, "pm":Landroid/os/PowerManager;
    if-eqz p1, :cond_0

    .line 275
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    .line 276
    invoke-virtual {p0, v5}, Lcom/sec/factory/modules/ModulePower;->doPartialWakeLock(Z)V

    .line 284
    :goto_0
    return-void

    .line 278
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->doPartialWakeLock(Z)V

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    goto :goto_0
.end method

.method public setTouchLedTime(I)V
    .locals 2
    .param p1, "time"    # I

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "button_key_light"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 305
    return-void
.end method

.method public sleep()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sleep"

    const-string v3, "Power Sleep"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "SYS_POWER_FTM_SLEEP"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    const-string v1, "SYS_POWER_FTM_SLEEP"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sleep pgm"

    const-string v3, "set : OK"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    :goto_0
    invoke-virtual {p0, v4}, Lcom/sec/factory/modules/ModulePower;->doWakeLock(Z)V

    .line 78
    invoke-virtual {p0, v4}, Lcom/sec/factory/modules/ModulePower;->doPartialWakeLock(Z)V

    .line 79
    invoke-virtual {p0, v4}, Lcom/sec/factory/modules/ModulePower;->doMediaScanWakeLock(Z)V

    .line 80
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 81
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 82
    return-void

    .line 74
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sleep pgm"

    const-string v3, "set : NG"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeBatteryAdcCal(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 474
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "writeBatteryAdcCal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v0, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 476
    return-void
.end method

.method public writeBatteryCal(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/factory/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "writeBatteryCal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    const-string v0, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 491
    return-void
.end method
