.class public Lcom/sec/factory/modules/SensorReadFile;
.super Ljava/lang/Object;
.source "SensorReadFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/SensorReadFile$Info;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "SensorReadFile"


# instance fields
.field private SENSOR_READ_INTERVAL:I

.field private mFeature_Magnetic:Ljava/lang/String;

.field private mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

.field private mIsLoop:Z

.field private mTemp_String:[Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>([I)V
    .locals 4
    .param p1, "moduleSensorID"    # [I

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mIsLoop:Z

    .line 74
    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    .line 75
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/factory/modules/SensorReadFile;->SENSOR_READ_INTERVAL:I

    .line 76
    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 77
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorDeviceInfo;->getSensorName(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    .line 83
    const-string v1, "SensorReadFile"

    const-string v2, "SensorReadFile"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sensor On : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0}, Lcom/sec/factory/modules/SensorReadFile;->initSysfs()V

    .line 85
    invoke-virtual {p0, p1}, Lcom/sec/factory/modules/SensorReadFile;->sensorOn([I)Z

    .line 86
    return-void

    .line 83
    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method private ConverterID(I)I
    .locals 5
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 524
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_MANAGER:I

    sub-int v0, p1, v1

    .line 525
    .local v0, "infoArrayIndex":I
    const-string v1, "SensorReadFile"

    const-string v2, "ConverterID"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "moduleSensorID["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] to ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    return v0
.end method

.method private checkSpec(II)V
    .locals 10
    .param p1, "moduleSensorID"    # I
    .param p2, "infoArrayIndex"    # I

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 327
    const-string v0, ""

    .line 329
    .local v0, "specResult":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    if-nez v1, :cond_1

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p1, v1, :cond_7

    .line 338
    const-string v1, "SensorReadFile"

    const-string v2, "checkSpec"

    const-string v3, "ID_FILE____MAGNETIC_DAC"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v9, :cond_4

    .line 342
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticDAC(III)Ljava/lang/String;

    move-result-object v0

    .line 504
    :cond_2
    :goto_1
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aput-object v0, v1, v8

    goto :goto_0

    .line 347
    :cond_4
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    const/4 v2, 0x6

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532B"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 353
    :cond_5
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 355
    const-string v1, "0"

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 356
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v9

    aput-object v2, v1, v7

    .line 357
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    aput-object v2, v1, v5

    .line 358
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    aput-object v2, v1, v6

    .line 359
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticDAC(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 365
    :cond_6
    const-string v1, "SensorReadFile"

    const-string v2, "checkSpec"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DAC [errNo] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v9

    aput-object v2, v1, v7

    .line 367
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    aput-object v2, v1, v5

    .line 368
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    aput-object v2, v1, v6

    .line 369
    const-string v0, "NG"

    goto/16 :goto_1

    .line 380
    :cond_7
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p1, v1, :cond_c

    .line 381
    const-string v1, "SensorReadFile"

    const-string v2, "checkSpec"

    const-string v3, "ID_FILE____MAGNETIC_ADC"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v9, :cond_8

    .line 385
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticADC(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 392
    :cond_8
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v6, :cond_9

    .line 393
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 394
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    new-array v2, v9, [Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    .line 395
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v1, v7

    .line 396
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v1, v5

    .line 397
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v5

    aput-object v2, v1, v6

    .line 398
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticADC(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 406
    :cond_9
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    const/16 v2, 0x8

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532B"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 412
    :cond_a
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 413
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    new-array v2, v9, [Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    .line 415
    const-string v1, "0"

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 416
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    aput-object v2, v1, v7

    .line 417
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v1, v5

    .line 418
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v1, v6

    .line 419
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticADC(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 425
    :cond_b
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    aput-object v2, v1, v7

    .line 426
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v1, v5

    .line 427
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v1, v6

    .line 428
    const-string v0, "NG"

    goto/16 :goto_1

    .line 436
    :cond_c
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v1, :cond_12

    .line 437
    const-string v1, "SensorReadFile"

    const-string v2, "checkSpec"

    const-string v3, "ID_FILE____MAGNETIC_SELF"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD006A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD008A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 443
    :cond_d
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 445
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v5, :cond_0

    .line 446
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    new-array v2, v5, [Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    .line 447
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v0, v1, v8

    .line 448
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v1, v8

    goto/16 :goto_0

    .line 453
    :cond_e
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532B"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150_NEWEST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 460
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150_POWER_NOISE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BMC150_COMBINATION"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 463
    :cond_f
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 464
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    new-array v2, v5, [Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    .line 465
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v8, v8, v2}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticSelf(III)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 467
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v1, v7

    goto/16 :goto_1

    .line 468
    :cond_10
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "STMICRO_K303C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 469
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticSelf(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 476
    :cond_11
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v9, :cond_2

    .line 477
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticSelf(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 487
    :cond_12
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_OFFSETH:I

    if-ne p1, v1, :cond_2

    .line 488
    const-string v1, "SensorReadFile"

    const-string v2, "checkSpec"

    const-string v3, "ID_FILE____MAGNETIC_OFFSETH"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    .line 492
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    const/4 v2, 0x6

    if-le v1, v2, :cond_2

    .line 493
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v9

    aput-object v2, v1, v7

    .line 494
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    aput-object v2, v1, v5

    .line 495
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    aput-object v2, v1, v6

    .line 496
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v1, v1, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p2

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, p2

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->checkSpecMagneticDAC(III)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private dataCheck([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 533
    const-string v1, ""

    .line 535
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 536
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 537
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 539
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 540
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 536
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 544
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 547
    :cond_2
    return-object v1
.end method

.method private declared-synchronized initSysfs()V
    .locals 7

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    const-string v3, "SensorReadFile"

    const-string v4, "initSysfs"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ModuleSensor.ID_COUNT_FILE : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v1, 0x0

    .line 140
    .local v1, "infoArrayIndex":I
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-nez v3, :cond_0

    .line 141
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_COUNT_FILE:I

    new-array v3, v3, [Lcom/sec/factory/modules/SensorReadFile$Info;

    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    .line 147
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .local v2, "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____ACCELEROMETER"

    const-string v6, "ACCEL_SENSOR_RAW"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 150
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____ACCELEROMETER_N_ANGLE"

    const-string v6, "ACCEL_SENSOR_RAW"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 153
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____ACCELEROMETER_SELF"

    const-string v6, "ACCEL_SENSOR_RAW"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 156
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____ACCELEROMETER_SELFTEST"

    const-string v6, "ACCEL_SENSOR_SELFTEST"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 159
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____ACCELEROMETER_CAL"

    const-string v6, "ACCEL_SENSOR_CAL"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 162
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____ACCELEROMETER_INTPIN"

    const-string v6, "ACCEL_SENSOR_INTPIN"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 165
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____BAROMETER_EEPROM"

    const-string v6, "BAROME_EEPROM"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 168
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____GYRO_POWER"

    const-string v6, "GYRO_SENSOR_POWER_ON"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 171
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____GYRO_TEMPERATURE"

    const-string v6, "GYRO_SENSOR_TEMP"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 174
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____GYRO_SELF"

    const-string v6, "GYRO_SENSOR_SELFTEST"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 177
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____LIGHT_ADC"

    const-string v6, "LIGHT_SENSOR_RAW"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 179
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____LIGHT_RGBW"

    const-string v6, "LIGHT_SENSOR_RAW"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 181
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_POWER_ON"

    const-string v6, "GEOMAGNETIC_SENSOR_POWER"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 184
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_POWER_OFF"

    const-string v6, "GEOMAGNETIC_SENSOR_POWER"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 187
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_STATUS"

    const-string v6, "GEOMAGNETIC_SENSOR_STATUS"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 190
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_TEMPERATURE"

    const-string v6, "GEOMAGNETIC_SENSOR_TEMP"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 193
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_DAC"

    const-string v6, "GEOMAGNETIC_SENSOR_DAC"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 196
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_ADC"

    const-string v6, "GEOMAGNETIC_SENSOR_ADC"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 199
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_SELF"

    const-string v6, "GEOMAGNETIC_SENSOR_SELFTEST"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 202
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____MAGNETIC_OFFSETH"

    const-string v6, "GEOMAGNETIC_SENSOR_OFFSET_H"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 205
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____PROXIMITY_ADC"

    const-string v6, "PROXI_SENSOR_ADC"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 208
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____PROXIMITY_AVG"

    const-string v6, "PROXI_SENSOR_ADC_AVG"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 211
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____PROXIMITY_OFFSET"

    const-string v6, "PROXI_SENSOR_OFFSET"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 214
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____GRIP_RAW"

    const-string v6, "GRIP_SENSOR_RAWDATA"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    .line 216
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "infoArrayIndex":I
    .restart local v2    # "infoArrayIndex":I
    new-instance v4, Lcom/sec/factory/modules/SensorReadFile$Info;

    const-string v5, "ID_FILE____GRIP_THRESHOLD"

    const-string v6, "GRIP_SENSOR_THRESHOLD"

    invoke-direct {v4, v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    .line 220
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 221
    const-string v3, "SensorReadFile"

    const-string v4, "initSysfs"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] mInfo - mName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/sec/factory/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mFilePath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/sec/factory/modules/SensorReadFile$Info;->mFilePath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mIsExistFile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v6, v6, v0

    iget-boolean v6, v6, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsExistFile:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    .end local v0    # "i":I
    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    :cond_0
    const-string v3, "SensorReadFile"

    const-string v4, "init"

    const-string v5, "mInfo is already initialized "

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :goto_1
    monitor-exit p0

    return-void

    .end local v1    # "infoArrayIndex":I
    .restart local v0    # "i":I
    .restart local v2    # "infoArrayIndex":I
    :cond_1
    move v1, v2

    .end local v2    # "infoArrayIndex":I
    .restart local v1    # "infoArrayIndex":I
    goto :goto_1

    .line 137
    .end local v0    # "i":I
    .end local v1    # "infoArrayIndex":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private removeWhiteSpace(I)V
    .locals 5
    .param p1, "infoArrayIndex"    # I

    .prologue
    .line 513
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 514
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string v3, " "

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 514
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 519
    .end local v0    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getSensorReadData()V
    .locals 7

    .prologue
    .line 241
    const-string v2, ""

    .line 243
    .local v2, "mTemp":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-nez v3, :cond_1

    .line 244
    const-string v3, "SensorReadFile"

    const-string v4, "getSensorReadData"

    const-string v5, "mInfo is null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    return-void

    .line 248
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    .line 249
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorReadFile$Info;->getOn()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    iget-boolean v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsExistFile:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    iget-boolean v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsLoopTest:Z

    if-eqz v3, :cond_2

    .line 251
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_3

    .line 252
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mFilePath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 258
    :goto_1
    if-eqz v2, :cond_2

    .line 259
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 254
    :cond_3
    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/sec/factory/modules/SensorReadFile$Info;->mFilePath:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SensorReadFile"

    const-string v4, "SensorReadThread-run"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "execption : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public isSensorOn(I)Z
    .locals 6
    .param p1, "moduleSensorID"    # I

    .prologue
    const/4 v1, 0x0

    .line 227
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadFile;->ConverterID(I)I

    move-result v0

    .line 229
    .local v0, "infoArrayIndex":I
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v1

    .line 233
    :cond_1
    const-string v2, "SensorReadFile"

    const-string v3, "isSensorOn"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mInfo - mName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mFilePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mIsExistFile : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-boolean v5, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsExistFile:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", on : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lcom/sec/factory/modules/SensorReadFile$Info;->getOn()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-boolean v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsExistFile:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/factory/modules/SensorReadFile$Info;->getOn()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public returnData(I)[Ljava/lang/String;
    .locals 6
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadFile;->ConverterID(I)I

    move-result v0

    .line 290
    .local v0, "infoArrayIndex":I
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 291
    :cond_0
    const/4 v2, 0x0

    .line 323
    :goto_0
    return-object v2

    .line 295
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-boolean v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsLoopTest:Z

    if-nez v2, :cond_2

    .line 296
    const-string v1, ""

    .line 298
    .local v1, "mTemp":Ljava/lang/String;
    const/4 v2, -0x1

    if-ge v2, v0, :cond_2

    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-boolean v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsExistFile:Z

    if-eqz v2, :cond_2

    .line 299
    const-string v2, "SensorReadFile"

    const-string v3, "returnData"

    iget-object v4, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/sec/factory/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mFilePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    if-eqz v1, :cond_2

    .line 303
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    .line 308
    .end local v1    # "mTemp":Ljava/lang/String;
    :cond_2
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v3, 0x3

    if-gt v2, v3, :cond_3

    .line 311
    const-string v2, "SensorReadFile"

    const-string v3, "returnData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " => "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadFile;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/factory/modules/SensorReadFile;->removeWhiteSpace(I)V

    .line 317
    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-eq p1, v2, :cond_4

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-eq p1, v2, :cond_4

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v2, :cond_5

    .line 320
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/SensorReadFile;->checkSpec(II)V

    .line 323
    :cond_5
    iget-object v2, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/sec/factory/modules/SensorReadFile$Info;->mBuffer_SensorValue:[Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public sensorOff([I)V
    .locals 10
    .param p1, "moduleSensorID"    # [I

    .prologue
    const/4 v9, 0x0

    .line 107
    const-string v6, "SensorReadFile"

    const-string v7, "sensorOff"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sensor Off : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p1, :cond_1

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-nez v5, :cond_2

    .line 109
    const-string v5, "SensorReadFile"

    const-string v6, "sensorOff"

    const-string v7, "mInfo is null"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    :goto_1
    return-void

    .line 107
    :cond_1
    const-string v5, "null"

    goto :goto_0

    .line 113
    :cond_2
    if-eqz p1, :cond_3

    .line 114
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v4, :cond_3

    aget v5, v0, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 115
    .local v1, "i":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadFile;->ConverterID(I)I

    move-result v3

    .line 116
    .local v3, "id":I
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v3

    invoke-virtual {v5, v9}, Lcom/sec/factory/modules/SensorReadFile$Info;->setOn(Z)V

    .line 117
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v3

    iput-boolean v9, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsLoopTest:Z

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 122
    .end local v0    # "arr$":[I
    .end local v1    # "i":Ljava/lang/Integer;
    .end local v2    # "i$":I
    .end local v3    # "id":I
    .end local v4    # "len$":I
    :cond_3
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    if-eqz v5, :cond_0

    .line 123
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v1

    if-eqz v5, :cond_5

    .line 124
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lcom/sec/factory/modules/SensorReadFile$Info;->getOn()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v1

    iget-boolean v5, v5, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsLoopTest:Z

    if-nez v5, :cond_0

    .line 123
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 128
    :cond_5
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    invoke-virtual {v5}, Ljava/util/Timer;->cancel()V

    .line 129
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    .line 130
    const-string v5, "SensorReadFile"

    const-string v6, "mTimer canceled"

    const-string v7, "..."

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public sensorOn([I)Z
    .locals 10
    .param p1, "moduleSensorID"    # [I

    .prologue
    const/4 v6, 0x1

    .line 89
    const-string v7, "SensorReadFile"

    const-string v8, "sensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sensor On : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v8, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-nez v5, :cond_1

    .line 92
    const-string v5, "SensorReadFile"

    const-string v6, "sensorOn"

    const-string v7, "mInfo is null"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const/4 v5, 0x0

    .line 103
    :goto_1
    return v5

    .line 89
    :cond_0
    const-string v5, "null"

    goto :goto_0

    .line 96
    :cond_1
    if-eqz p1, :cond_2

    .line 97
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v4, :cond_2

    aget v5, v0, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 98
    .local v1, "i":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadFile;->ConverterID(I)I

    move-result v3

    .line 99
    .local v3, "id":I
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v5, v5, v3

    invoke-virtual {v5, v6}, Lcom/sec/factory/modules/SensorReadFile$Info;->setOn(Z)V

    .line 100
    const-string v5, "SensorReadFile"

    const-string v7, "sensorOn"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sensorOn : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v9}, Lcom/sec/factory/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v0    # "arr$":[I
    .end local v1    # "i":Ljava/lang/Integer;
    .end local v2    # "i$":I
    .end local v3    # "id":I
    .end local v4    # "len$":I
    :cond_2
    move v5, v6

    .line 103
    goto :goto_1
.end method

.method public startLoop(I)V
    .locals 7
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadFile;->ConverterID(I)I

    move-result v6

    .line 271
    .local v6, "infoArrayIndex":I
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v0, v0, v6

    if-nez v0, :cond_2

    .line 272
    :cond_0
    const-string v0, "SensorReadFile"

    const-string v1, "startLoop"

    const-string v2, "failed"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_1
    :goto_0
    return-void

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mInfo:[Lcom/sec/factory/modules/SensorReadFile$Info;

    aget-object v0, v0, v6

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/factory/modules/SensorReadFile$Info;->mIsLoopTest:Z

    .line 276
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 279
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    .line 280
    iget-object v0, p0, Lcom/sec/factory/modules/SensorReadFile;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/factory/modules/SensorReadFile$1;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/SensorReadFile$1;-><init>(Lcom/sec/factory/modules/SensorReadFile;)V

    const-wide/16 v2, 0x0

    iget v4, p0, Lcom/sec/factory/modules/SensorReadFile;->SENSOR_READ_INTERVAL:I

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method
