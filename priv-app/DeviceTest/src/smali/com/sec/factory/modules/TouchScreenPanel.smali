.class public Lcom/sec/factory/modules/TouchScreenPanel;
.super Ljava/lang/Object;
.source "TouchScreenPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/TouchScreenPanel$1;,
        Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;,
        Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;,
        Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;,
        Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;
    }
.end annotation


# static fields
.field public static final TSP_RESULT_VALUE_NG:Ljava/lang/String; = "NG"

.field public static final TSP_RESULT_VALUE_NOT_APPLICABLE:Ljava/lang/String; = "NA"

.field public static final TSP_RESULT_VALUE_OK:Ljava/lang/String; = "OK"

.field public static final TSP_STM_HOVER_ENABLE:Ljava/lang/String; = "hover_enable"

.field private static TSP_WHAT_DUMMY:I

.field public static final TSP_WHAT_SCOPE_MAX:I

.field public static final TSP_WHAT_SCOPE_MIN:I

.field public static final TSP_WHAT_STATUS_NA:I

.field public static final TSP_WHAT_STATUS_NG:I

.field public static final TSP_WHAT_STATUS_OK:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field public final CMD_NODE:Ljava/lang/String;

.field public final RESULT_NODE:Ljava/lang/String;

.field public final STATUS_NODE:Ljava/lang/String;

.field private final TSP_CMD__ATMEL__READ_DELTA_NODE:Ljava/lang/String;

.field private final TSP_CMD__ATMEL__READ_DELTA_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__ATMEL__READ_REFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__ATMEL__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__COMMON__FW_UPDATE:Ljava/lang/String;

.field private final TSP_CMD__COMMON__FW_UPDATE__SUB_CMD__BUILT_IN:Ljava/lang/String;

.field private final TSP_CMD__COMMON__FW_UPDATE__SUB_CMD__UMS_OR_SDCARD:Ljava/lang/String;

.field private final TSP_CMD__COMMON__POWER_OFF:Ljava/lang/String;

.field private final TSP_CMD__COMMON__POWER_OFF_SECOND_CHIP:Ljava/lang/String;

.field private final TSP_CMD__COMMON__POWER_ON:Ljava/lang/String;

.field private final TSP_CMD__COMMON__POWER_ON_SECOND_CHIP:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_CHIP_NAME:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_CONFIG_VERSION:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_FW_VERSION_BINARY:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_FW_VERSION_IC:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_MODULE_VENDOR:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_NUMBER_X_CHANNEL:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_NUMBER_Y_CHANNEL:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_THRESHOLD:Ljava/lang/String;

.field private final TSP_CMD__COMMON__READ_VENDOR_NAME:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_DIFFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_DIFFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_GLOBAL_IDAC_NODE:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_GLOBAL_IDAC_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_LOCAL_IDAC_NODE:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_LOCAL_IDAC_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_RAW_COUNT_NODE:Ljava/lang/String;

.field private final TSP_CMD__CYPRESS__READ_RAW_COUNT_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__IMAGIS__READ_CM_ABS_NODE:Ljava/lang/String;

.field private final TSP_CMD__IMAGIS__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__IMAGIS__READ_REFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__IMAGIS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_CM_ABS_NODE:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_DELTA_NODE:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_DELTA_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_INSPECTION_NODE:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_INSPECTION_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_INTENSITY_NODE:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_INTENSITY_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_REFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__MELFAS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_CM_ABS_NODE:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_CX_DATA_NODE:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_CX_DATA_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_KEY_CM_DATA:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_KEY_CX_DATA:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_MUTUAL_CAP_NODE:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_MUTUAL_CAP_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_RAWCAP_NODE:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_RAWCAP_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_REFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__STM__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_MUTUAL_CAP_NODE:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_MUTUAL_CAP_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_RAWCAP_NODE:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_RAWCAP_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_REFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_REGISTER_RESULT:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_RX_TO_RX_NODE:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_RX_TO_RX_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_TRX_SHORT_TEXT:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_TX_TO_GND_NODE:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_TX_TO_GND_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_TX_TO_TX_NODE:Ljava/lang/String;

.field private final TSP_CMD__SYNAPTICS__READ_TX_TO_TX_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_DELTA_NODE:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_DELTA_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_DND_DIFF:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_DND_NODE:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_DND_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_REFERENCE_NODE:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_SCANTIME_NODE:Ljava/lang/String;

.field private final TSP_CMD__ZINITIX__READ_SCANTIME_NODE_ALL:Ljava/lang/String;

.field private final TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:Ljava/lang/String;

.field private final TSP_HOVER_CMD__STM__READ_ABS_CAP:Ljava/lang/String;

.field private final TSP_HOVER_CMD__STM__READ_ABS_DELTA:Ljava/lang/String;

.field private final TSP_HOVER_CMD__SYNAPTICS__READ_ABS_CAP:Ljava/lang/String;

.field private final TSP_HOVER_CMD__SYNAPTICS__READ_ABS_DELTA:Ljava/lang/String;

.field private final TSP_SIDETOUCH_CMD__SYNAPTICS__READ_ABS_CAP_ALL:Ljava/lang/String;

.field private final TSP_SIDETOUCH_CMD__SYNAPTICS__READ_DELTA_ALL:Ljava/lang/String;

.field private TSP_STATUS_RUNNING_TIME_OUT:J

.field private final TSP_STATUS__COMMON__FAIL:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__NOT_APPLICABLE:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__OK:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__RUNNING:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__WAITING:Ljava/lang/String;

.field private mCheckCxDataAll:Lcom/sec/factory/modules/check/CheckCxDataAll;

.field private mCheckCxDataGap:Lcom/sec/factory/modules/check/CheckCxDataGap;

.field private mCheckTSPRawCap:Lcom/sec/factory/modules/check/CheckTSPRawCap;

.field private mContext:Landroid/content/Context;

.field private mDeviceType:Ljava/lang/String;

.field private mExevutor:Ljava/util/concurrent/ExecutorService;

.field private mPanelType:Ljava/lang/String;

.field private mVendor:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 281
    const/4 v0, 0x0

    sput v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    .line 282
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    sput v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_SCOPE_MIN:I

    .line 283
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    sput v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_OK:I

    .line 284
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    sput v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NG:I

    .line 285
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    sput v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_STATUS_NA:I

    .line 286
    sget v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_DUMMY:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_WHAT_SCOPE_MAX:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "panelType"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "TouchScreenPanel"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CLASS_NAME:Ljava/lang/String;

    .line 18
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mPanelType:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mDeviceType:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;

    .line 73
    const-string v0, "fw_update"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__FW_UPDATE:Ljava/lang/String;

    .line 74
    const-string v0, "get_fw_ver_bin"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_FW_VERSION_BINARY:Ljava/lang/String;

    .line 75
    const-string v0, "get_fw_ver_ic"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_FW_VERSION_IC:Ljava/lang/String;

    .line 76
    const-string v0, "get_config_ver"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_CONFIG_VERSION:Ljava/lang/String;

    .line 77
    const-string v0, "get_threshold"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_THRESHOLD:Ljava/lang/String;

    .line 78
    const-string v0, "module_off_master"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_OFF:Ljava/lang/String;

    .line 79
    const-string v0, "module_on_master"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_ON:Ljava/lang/String;

    .line 80
    const-string v0, "module_off_slave"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_OFF_SECOND_CHIP:Ljava/lang/String;

    .line 81
    const-string v0, "module_on_slave"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_ON_SECOND_CHIP:Ljava/lang/String;

    .line 82
    const-string v0, "get_chip_vendor"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_VENDOR_NAME:Ljava/lang/String;

    .line 83
    const-string v0, "get_module_vendor"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_MODULE_VENDOR:Ljava/lang/String;

    .line 84
    const-string v0, "get_chip_name"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_CHIP_NAME:Ljava/lang/String;

    .line 85
    const-string v0, "get_x_num"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_NUMBER_X_CHANNEL:Ljava/lang/String;

    .line 86
    const-string v0, "get_y_num"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_NUMBER_Y_CHANNEL:Ljava/lang/String;

    .line 99
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 100
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 101
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_DELTA_NODE_ALL:Ljava/lang/String;

    .line 102
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_DELTA_NODE:Ljava/lang/String;

    .line 127
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 128
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 129
    const-string v0, "run_cm_abs_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

    .line 130
    const-string v0, "get_cm_abs"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_CM_ABS_NODE:Ljava/lang/String;

    .line 131
    const-string v0, "run_inspection_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INSPECTION_NODE_ALL:Ljava/lang/String;

    .line 132
    const-string v0, "get_inspection"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INSPECTION_NODE:Ljava/lang/String;

    .line 133
    const-string v0, "run_cm_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_DELTA_NODE_ALL:Ljava/lang/String;

    .line 134
    const-string v0, "get_cm_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_DELTA_NODE:Ljava/lang/String;

    .line 135
    const-string v0, "run_intensity_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INTENSITY_NODE_ALL:Ljava/lang/String;

    .line 136
    const-string v0, "get_intensity"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INTENSITY_NODE:Ljava/lang/String;

    .line 154
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 155
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 156
    const-string v0, "run_scantime_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_SCANTIME_NODE_ALL:Ljava/lang/String;

    .line 157
    const-string v0, "get_scantime"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_SCANTIME_NODE:Ljava/lang/String;

    .line 158
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DELTA_NODE_ALL:Ljava/lang/String;

    .line 159
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DELTA_NODE:Ljava/lang/String;

    .line 160
    const-string v0, "run_dnd_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DND_NODE_ALL:Ljava/lang/String;

    .line 161
    const-string v0, "get_dnd"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DND_NODE:Ljava/lang/String;

    .line 162
    const-string v0, "run_reference_diff"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DND_DIFF:Ljava/lang/String;

    .line 187
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 188
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 189
    const-string v0, "run_rawcap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RAWCAP_NODE_ALL:Ljava/lang/String;

    .line 190
    const-string v0, "get_rawcap"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RAWCAP_NODE:Ljava/lang/String;

    .line 191
    const-string v0, "run_rx_to_rx_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RX_TO_RX_NODE_ALL:Ljava/lang/String;

    .line 192
    const-string v0, "get_rx_to_rx"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RX_TO_RX_NODE:Ljava/lang/String;

    .line 193
    const-string v0, "run_tx_to_tx_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_TX_NODE_ALL:Ljava/lang/String;

    .line 194
    const-string v0, "get_tx_to_tx"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_TX_NODE:Ljava/lang/String;

    .line 195
    const-string v0, "run_tx_to_gnd_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_GND_NODE_ALL:Ljava/lang/String;

    .line 196
    const-string v0, "get_tx_to_gnd"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_GND_NODE:Ljava/lang/String;

    .line 197
    const-string v0, "run_abscap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__SYNAPTICS__READ_ABS_CAP:Ljava/lang/String;

    .line 198
    const-string v0, "run_absdelta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__SYNAPTICS__READ_ABS_DELTA:Ljava/lang/String;

    .line 199
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_MUTUAL_CAP_NODE_ALL:Ljava/lang/String;

    .line 200
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_MUTUAL_CAP_NODE:Ljava/lang/String;

    .line 201
    const-string v0, "get_tsp_test_result"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_REGISTER_RESULT:Ljava/lang/String;

    .line 202
    const-string v0, "hover_set_edge_rx"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:Ljava/lang/String;

    .line 203
    const-string v0, "run_sidekey_abscap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_SIDETOUCH_CMD__SYNAPTICS__READ_ABS_CAP_ALL:Ljava/lang/String;

    .line 204
    const-string v0, "run_sidekey_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_SIDETOUCH_CMD__SYNAPTICS__READ_DELTA_ALL:Ljava/lang/String;

    .line 205
    const-string v0, "run_trx_short_test"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TRX_SHORT_TEXT:Ljava/lang/String;

    .line 215
    const-string v0, "run_raw_count_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_RAW_COUNT_NODE_ALL:Ljava/lang/String;

    .line 216
    const-string v0, "get_raw_count"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_RAW_COUNT_NODE:Ljava/lang/String;

    .line 217
    const-string v0, "run_difference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_DIFFERENCE_NODE_ALL:Ljava/lang/String;

    .line 218
    const-string v0, "get_difference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_DIFFERENCE_NODE:Ljava/lang/String;

    .line 219
    const-string v0, "run_local_idac_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_LOCAL_IDAC_NODE_ALL:Ljava/lang/String;

    .line 220
    const-string v0, "get_local_idac"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_LOCAL_IDAC_NODE:Ljava/lang/String;

    .line 221
    const-string v0, "run_global_idac_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_GLOBAL_IDAC_NODE_ALL:Ljava/lang/String;

    .line 222
    const-string v0, "get_global_idac"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_GLOBAL_IDAC_NODE:Ljava/lang/String;

    .line 230
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 231
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 232
    const-string v0, "run_cm_abs_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

    .line 233
    const-string v0, "get_cm_abs"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_CM_ABS_NODE:Ljava/lang/String;

    .line 241
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 242
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 243
    const-string v0, "run_cm_abs_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

    .line 244
    const-string v0, "get_cm_abs"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CM_ABS_NODE:Ljava/lang/String;

    .line 245
    const-string v0, "run_rawcap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_RAWCAP_NODE_ALL:Ljava/lang/String;

    .line 246
    const-string v0, "get_rawcap"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_RAWCAP_NODE:Ljava/lang/String;

    .line 247
    const-string v0, "run_abscap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__STM__READ_ABS_CAP:Ljava/lang/String;

    .line 248
    const-string v0, "run_absdelta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__STM__READ_ABS_DELTA:Ljava/lang/String;

    .line 249
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_MUTUAL_CAP_NODE_ALL:Ljava/lang/String;

    .line 250
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_MUTUAL_CAP_NODE:Ljava/lang/String;

    .line 252
    const-string v0, "run_cx_data_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CX_DATA_NODE_ALL:Ljava/lang/String;

    .line 253
    const-string v0, "get_cx_data"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CX_DATA_NODE:Ljava/lang/String;

    .line 255
    const-string v0, "run_key_rawcap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_KEY_CM_DATA:Ljava/lang/String;

    .line 256
    const-string v0, "run_key_cx_data_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_KEY_CX_DATA:Ljava/lang/String;

    .line 265
    const-string v0, "WAITING"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__WAITING:Ljava/lang/String;

    .line 266
    const-string v0, "RUNNING"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__RUNNING:Ljava/lang/String;

    .line 267
    const-string v0, "OK"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__OK:Ljava/lang/String;

    .line 268
    const-string v0, "FAIL"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__FAIL:Ljava/lang/String;

    .line 269
    const-string v0, "NOT_APPLICABLE"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__NOT_APPLICABLE:Ljava/lang/String;

    .line 277
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__FW_UPDATE__SUB_CMD__BUILT_IN:Ljava/lang/String;

    .line 278
    const-string v0, "1"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__FW_UPDATE__SUB_CMD__UMS_OR_SDCARD:Ljava/lang/String;

    .line 289
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS_RUNNING_TIME_OUT:J

    .line 293
    const-string v0, "TSP_COMMAND_CMD"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CMD_NODE:Ljava/lang/String;

    .line 294
    const-string v0, "TSP_COMMAND_STATUS"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->STATUS_NODE:Ljava/lang/String;

    .line 295
    const-string v0, "TSP_COMMAND_RESULT"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->RESULT_NODE:Ljava/lang/String;

    .line 1144
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    .line 34
    const-string v0, "TouchScreenPanel"

    const-string v1, "TouchScreenPanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mPanelType:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mDeviceType:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p1, "panelType"    # Ljava/lang/String;
    .param p2, "deviceType"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "TouchScreenPanel"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CLASS_NAME:Ljava/lang/String;

    .line 18
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    .line 19
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mPanelType:Ljava/lang/String;

    .line 20
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mDeviceType:Ljava/lang/String;

    .line 21
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;

    .line 73
    const-string v0, "fw_update"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__FW_UPDATE:Ljava/lang/String;

    .line 74
    const-string v0, "get_fw_ver_bin"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_FW_VERSION_BINARY:Ljava/lang/String;

    .line 75
    const-string v0, "get_fw_ver_ic"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_FW_VERSION_IC:Ljava/lang/String;

    .line 76
    const-string v0, "get_config_ver"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_CONFIG_VERSION:Ljava/lang/String;

    .line 77
    const-string v0, "get_threshold"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_THRESHOLD:Ljava/lang/String;

    .line 78
    const-string v0, "module_off_master"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_OFF:Ljava/lang/String;

    .line 79
    const-string v0, "module_on_master"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_ON:Ljava/lang/String;

    .line 80
    const-string v0, "module_off_slave"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_OFF_SECOND_CHIP:Ljava/lang/String;

    .line 81
    const-string v0, "module_on_slave"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__POWER_ON_SECOND_CHIP:Ljava/lang/String;

    .line 82
    const-string v0, "get_chip_vendor"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_VENDOR_NAME:Ljava/lang/String;

    .line 83
    const-string v0, "get_module_vendor"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_MODULE_VENDOR:Ljava/lang/String;

    .line 84
    const-string v0, "get_chip_name"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_CHIP_NAME:Ljava/lang/String;

    .line 85
    const-string v0, "get_x_num"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_NUMBER_X_CHANNEL:Ljava/lang/String;

    .line 86
    const-string v0, "get_y_num"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__READ_NUMBER_Y_CHANNEL:Ljava/lang/String;

    .line 99
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 100
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 101
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_DELTA_NODE_ALL:Ljava/lang/String;

    .line 102
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ATMEL__READ_DELTA_NODE:Ljava/lang/String;

    .line 127
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 128
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 129
    const-string v0, "run_cm_abs_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

    .line 130
    const-string v0, "get_cm_abs"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_CM_ABS_NODE:Ljava/lang/String;

    .line 131
    const-string v0, "run_inspection_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INSPECTION_NODE_ALL:Ljava/lang/String;

    .line 132
    const-string v0, "get_inspection"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INSPECTION_NODE:Ljava/lang/String;

    .line 133
    const-string v0, "run_cm_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_DELTA_NODE_ALL:Ljava/lang/String;

    .line 134
    const-string v0, "get_cm_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_DELTA_NODE:Ljava/lang/String;

    .line 135
    const-string v0, "run_intensity_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INTENSITY_NODE_ALL:Ljava/lang/String;

    .line 136
    const-string v0, "get_intensity"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__MELFAS__READ_INTENSITY_NODE:Ljava/lang/String;

    .line 154
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 155
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 156
    const-string v0, "run_scantime_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_SCANTIME_NODE_ALL:Ljava/lang/String;

    .line 157
    const-string v0, "get_scantime"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_SCANTIME_NODE:Ljava/lang/String;

    .line 158
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DELTA_NODE_ALL:Ljava/lang/String;

    .line 159
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DELTA_NODE:Ljava/lang/String;

    .line 160
    const-string v0, "run_dnd_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DND_NODE_ALL:Ljava/lang/String;

    .line 161
    const-string v0, "get_dnd"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DND_NODE:Ljava/lang/String;

    .line 162
    const-string v0, "run_reference_diff"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__ZINITIX__READ_DND_DIFF:Ljava/lang/String;

    .line 187
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 188
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 189
    const-string v0, "run_rawcap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RAWCAP_NODE_ALL:Ljava/lang/String;

    .line 190
    const-string v0, "get_rawcap"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RAWCAP_NODE:Ljava/lang/String;

    .line 191
    const-string v0, "run_rx_to_rx_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RX_TO_RX_NODE_ALL:Ljava/lang/String;

    .line 192
    const-string v0, "get_rx_to_rx"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_RX_TO_RX_NODE:Ljava/lang/String;

    .line 193
    const-string v0, "run_tx_to_tx_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_TX_NODE_ALL:Ljava/lang/String;

    .line 194
    const-string v0, "get_tx_to_tx"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_TX_NODE:Ljava/lang/String;

    .line 195
    const-string v0, "run_tx_to_gnd_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_GND_NODE_ALL:Ljava/lang/String;

    .line 196
    const-string v0, "get_tx_to_gnd"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TX_TO_GND_NODE:Ljava/lang/String;

    .line 197
    const-string v0, "run_abscap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__SYNAPTICS__READ_ABS_CAP:Ljava/lang/String;

    .line 198
    const-string v0, "run_absdelta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__SYNAPTICS__READ_ABS_DELTA:Ljava/lang/String;

    .line 199
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_MUTUAL_CAP_NODE_ALL:Ljava/lang/String;

    .line 200
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_MUTUAL_CAP_NODE:Ljava/lang/String;

    .line 201
    const-string v0, "get_tsp_test_result"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_REGISTER_RESULT:Ljava/lang/String;

    .line 202
    const-string v0, "hover_set_edge_rx"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:Ljava/lang/String;

    .line 203
    const-string v0, "run_sidekey_abscap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_SIDETOUCH_CMD__SYNAPTICS__READ_ABS_CAP_ALL:Ljava/lang/String;

    .line 204
    const-string v0, "run_sidekey_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_SIDETOUCH_CMD__SYNAPTICS__READ_DELTA_ALL:Ljava/lang/String;

    .line 205
    const-string v0, "run_trx_short_test"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__SYNAPTICS__READ_TRX_SHORT_TEXT:Ljava/lang/String;

    .line 215
    const-string v0, "run_raw_count_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_RAW_COUNT_NODE_ALL:Ljava/lang/String;

    .line 216
    const-string v0, "get_raw_count"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_RAW_COUNT_NODE:Ljava/lang/String;

    .line 217
    const-string v0, "run_difference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_DIFFERENCE_NODE_ALL:Ljava/lang/String;

    .line 218
    const-string v0, "get_difference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_DIFFERENCE_NODE:Ljava/lang/String;

    .line 219
    const-string v0, "run_local_idac_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_LOCAL_IDAC_NODE_ALL:Ljava/lang/String;

    .line 220
    const-string v0, "get_local_idac"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_LOCAL_IDAC_NODE:Ljava/lang/String;

    .line 221
    const-string v0, "run_global_idac_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_GLOBAL_IDAC_NODE_ALL:Ljava/lang/String;

    .line 222
    const-string v0, "get_global_idac"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__CYPRESS__READ_GLOBAL_IDAC_NODE:Ljava/lang/String;

    .line 230
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 231
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 232
    const-string v0, "run_cm_abs_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

    .line 233
    const-string v0, "get_cm_abs"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__IMAGIS__READ_CM_ABS_NODE:Ljava/lang/String;

    .line 241
    const-string v0, "run_reference_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_REFERENCE_NODE_ALL:Ljava/lang/String;

    .line 242
    const-string v0, "get_reference"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_REFERENCE_NODE:Ljava/lang/String;

    .line 243
    const-string v0, "run_cm_abs_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CM_ABS_NODE_ALL:Ljava/lang/String;

    .line 244
    const-string v0, "get_cm_abs"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CM_ABS_NODE:Ljava/lang/String;

    .line 245
    const-string v0, "run_rawcap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_RAWCAP_NODE_ALL:Ljava/lang/String;

    .line 246
    const-string v0, "get_rawcap"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_RAWCAP_NODE:Ljava/lang/String;

    .line 247
    const-string v0, "run_abscap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__STM__READ_ABS_CAP:Ljava/lang/String;

    .line 248
    const-string v0, "run_absdelta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_HOVER_CMD__STM__READ_ABS_DELTA:Ljava/lang/String;

    .line 249
    const-string v0, "run_delta_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_MUTUAL_CAP_NODE_ALL:Ljava/lang/String;

    .line 250
    const-string v0, "get_delta"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_MUTUAL_CAP_NODE:Ljava/lang/String;

    .line 252
    const-string v0, "run_cx_data_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CX_DATA_NODE_ALL:Ljava/lang/String;

    .line 253
    const-string v0, "get_cx_data"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_CX_DATA_NODE:Ljava/lang/String;

    .line 255
    const-string v0, "run_key_rawcap_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_KEY_CM_DATA:Ljava/lang/String;

    .line 256
    const-string v0, "run_key_cx_data_read"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__STM__READ_KEY_CX_DATA:Ljava/lang/String;

    .line 265
    const-string v0, "WAITING"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__WAITING:Ljava/lang/String;

    .line 266
    const-string v0, "RUNNING"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__RUNNING:Ljava/lang/String;

    .line 267
    const-string v0, "OK"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__OK:Ljava/lang/String;

    .line 268
    const-string v0, "FAIL"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__FAIL:Ljava/lang/String;

    .line 269
    const-string v0, "NOT_APPLICABLE"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS__COMMON__NOT_APPLICABLE:Ljava/lang/String;

    .line 277
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__FW_UPDATE__SUB_CMD__BUILT_IN:Ljava/lang/String;

    .line 278
    const-string v0, "1"

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_CMD__COMMON__FW_UPDATE__SUB_CMD__UMS_OR_SDCARD:Ljava/lang/String;

    .line 289
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS_RUNNING_TIME_OUT:J

    .line 293
    const-string v0, "TSP_COMMAND_CMD"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CMD_NODE:Ljava/lang/String;

    .line 294
    const-string v0, "TSP_COMMAND_STATUS"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->STATUS_NODE:Ljava/lang/String;

    .line 295
    const-string v0, "TSP_COMMAND_RESULT"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->RESULT_NODE:Ljava/lang/String;

    .line 1144
    iput-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    .line 27
    const-string v0, "TouchScreenPanel"

    const-string v1, "TouchScreenPanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mPanelType:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mDeviceType:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/modules/TouchScreenPanel;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel;->mapping_IDnCommand(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/factory/modules/TouchScreenPanel;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/factory/modules/TouchScreenPanel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckTSPRawCap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mCheckTSPRawCap:Lcom/sec/factory/modules/check/CheckTSPRawCap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/check/CheckTSPRawCap;)Lcom/sec/factory/modules/check/CheckTSPRawCap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # Lcom/sec/factory/modules/check/CheckTSPRawCap;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mCheckTSPRawCap:Lcom/sec/factory/modules/check/CheckTSPRawCap;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckCxDataGap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataGap:Lcom/sec/factory/modules/check/CheckCxDataGap;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/check/CheckCxDataGap;)Lcom/sec/factory/modules/check/CheckCxDataGap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # Lcom/sec/factory/modules/check/CheckCxDataGap;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataGap:Lcom/sec/factory/modules/check/CheckCxDataGap;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/factory/modules/TouchScreenPanel;)Lcom/sec/factory/modules/check/CheckCxDataAll;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataAll:Lcom/sec/factory/modules/check/CheckCxDataAll;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/check/CheckCxDataAll;)Lcom/sec/factory/modules/check/CheckCxDataAll;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/TouchScreenPanel;
    .param p1, "x1"    # Lcom/sec/factory/modules/check/CheckCxDataAll;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mCheckCxDataAll:Lcom/sec/factory/modules/check/CheckCxDataAll;

    return-object p1
.end method

.method private getTSPCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    .line 375
    if-eqz p2, :cond_0

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 379
    :cond_0
    const-string v1, "TouchScreenPanel"

    const-string v2, "getTSPCommandResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "2. get Result => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v1, "TSP_COMMAND_RESULT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 384
    const-string v1, "TouchScreenPanel"

    const-string v2, "getTSPCommandResult"

    const-string v3, "Fail - Result Read =>1 result == null"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const-string v1, "NG"

    .line 394
    :goto_0
    return-object v1

    .line 387
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 388
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 389
    const-string v1, "TouchScreenPanel"

    const-string v2, "getTSPCommandResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 390
    goto :goto_0

    .line 392
    :cond_2
    const-string v1, "TouchScreenPanel"

    const-string v2, "getTSPCommandResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(Write Command) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != (Result Command) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v1, "NG"

    goto :goto_0
.end method

.method private getTSPCommandResultData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 400
    if-eqz p2, :cond_0

    .line 401
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 405
    :cond_0
    const-string v2, "TouchScreenPanel"

    const-string v3, "getTSPCommandResult"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2. get Result => "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->RESULT_NODE:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 409
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 410
    const-string v2, "TouchScreenPanel"

    const-string v3, "getTSPCommandResult"

    const-string v4, "Fail - Result Read =>1 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string v2, "NG"

    .line 416
    :goto_0
    return-object v2

    .line 413
    :cond_1
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 414
    .local v1, "values":[Ljava/lang/String;
    aget-object v0, v1, v6

    .line 415
    const-string v2, "TouchScreenPanel"

    const-string v3, "getTSPCommandResult"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v0

    .line 416
    goto :goto_0
.end method

.method private mapping_IDnCommand(I)Ljava/lang/String;
    .locals 4
    .param p1, "tspID"    # I

    .prologue
    .line 425
    const-string v0, "TouchScreenPanel"

    const-string v1, "mapping_IDnCommand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TSP ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_UPDATE:I

    if-ne p1, v0, :cond_0

    .line 430
    const-string v0, "fw_update"

    .line 792
    :goto_0
    return-object v0

    .line 431
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_BINARY:I

    if-ne p1, v0, :cond_1

    .line 432
    const-string v0, "get_fw_ver_bin"

    goto :goto_0

    .line 433
    :cond_1
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_VERSION_IC:I

    if-ne p1, v0, :cond_2

    .line 434
    const-string v0, "get_fw_ver_ic"

    goto :goto_0

    .line 435
    :cond_2
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CONFIG_VERSION:I

    if-ne p1, v0, :cond_3

    .line 436
    const-string v0, "get_config_ver"

    goto :goto_0

    .line 437
    :cond_3
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__THRESHOLD:I

    if-ne p1, v0, :cond_4

    .line 438
    const-string v0, "get_threshold"

    goto :goto_0

    .line 439
    :cond_4
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_OFF:I

    if-ne p1, v0, :cond_5

    .line 440
    const-string v0, "module_off_master"

    goto :goto_0

    .line 441
    :cond_5
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_ON:I

    if-ne p1, v0, :cond_6

    .line 442
    const-string v0, "module_on_master"

    goto :goto_0

    .line 443
    :cond_6
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_OFF_SECOND_CHIP:I

    if-ne p1, v0, :cond_7

    .line 444
    const-string v0, "module_off_slave"

    goto :goto_0

    .line 445
    :cond_7
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__POWER_ON_SECOND_CHIP:I

    if-ne p1, v0, :cond_8

    .line 446
    const-string v0, "module_on_slave"

    goto :goto_0

    .line 447
    :cond_8
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__VENDOR_NAME:I

    if-ne p1, v0, :cond_9

    .line 448
    const-string v0, "get_chip_vendor"

    goto :goto_0

    .line 449
    :cond_9
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MODULE_VENDOR:I

    if-ne p1, v0, :cond_a

    .line 450
    const-string v0, "get_module_vendor"

    goto :goto_0

    .line 451
    :cond_a
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CHIP_NAME:I

    if-ne p1, v0, :cond_b

    .line 452
    const-string v0, "get_chip_name"

    goto :goto_0

    .line 453
    :cond_b
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_X_CHANNEL:I

    if-ne p1, v0, :cond_c

    .line 454
    const-string v0, "get_x_num"

    goto :goto_0

    .line 455
    :cond_c
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__NUMBER_Y_CHANNEL:I

    if-ne p1, v0, :cond_d

    .line 456
    const-string v0, "get_y_num"

    goto :goto_0

    .line 460
    :cond_d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_12

    .line 461
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ATMEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 462
    const-string v0, "run_reference_read"

    goto :goto_0

    .line 463
    :cond_e
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 464
    const-string v0, "run_reference_read"

    goto :goto_0

    .line 465
    :cond_f
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 466
    const-string v0, "run_reference_read"

    goto/16 :goto_0

    .line 467
    :cond_10
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "IMAGIS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 468
    const-string v0, "run_reference_read"

    goto/16 :goto_0

    .line 469
    :cond_11
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 470
    const-string v0, "run_reference_read"

    goto/16 :goto_0

    .line 472
    :cond_12
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_15

    .line 473
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 475
    :cond_13
    const-string v0, "SUPPORT_RUN_REF"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 476
    const-string v0, "run_reference_read"

    goto/16 :goto_0

    .line 478
    :cond_14
    const-string v0, "run_dnd_read"

    goto/16 :goto_0

    .line 481
    :cond_15
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REFERENCE__NODE:I

    if-ne p1, v0, :cond_1d

    .line 482
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ATMEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 483
    const-string v0, "get_reference"

    goto/16 :goto_0

    .line 484
    :cond_16
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 485
    const-string v0, "get_reference"

    goto/16 :goto_0

    .line 486
    :cond_17
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 488
    :cond_18
    const-string v0, "SUPPORT_RUN_REF"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 489
    const-string v0, "get_reference"

    goto/16 :goto_0

    .line 491
    :cond_19
    const-string v0, "get_dnd"

    goto/16 :goto_0

    .line 493
    :cond_1a
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 494
    const-string v0, "get_reference"

    goto/16 :goto_0

    .line 495
    :cond_1b
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "IMAGIS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 496
    const-string v0, "get_reference"

    goto/16 :goto_0

    .line 497
    :cond_1c
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 498
    const-string v0, "get_reference"

    goto/16 :goto_0

    .line 502
    :cond_1d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_1e

    .line 503
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 504
    const-string v0, "run_cm_delta_read"

    goto/16 :goto_0

    .line 506
    :cond_1e
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_21

    .line 507
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ATMEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 508
    const-string v0, "run_delta_read"

    goto/16 :goto_0

    .line 509
    :cond_1f
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 511
    :cond_20
    const-string v0, "run_delta_read"

    goto/16 :goto_0

    .line 513
    :cond_21
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DELTA__NODE:I

    if-ne p1, v0, :cond_26

    .line 514
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ATMEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 515
    const-string v0, "get_delta"

    goto/16 :goto_0

    .line 516
    :cond_22
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 517
    const-string v0, "get_cm_delta"

    goto/16 :goto_0

    .line 518
    :cond_23
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 520
    :cond_24
    const-string v0, "get_delta"

    goto/16 :goto_0

    .line 522
    :cond_25
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 523
    const-string v0, "get_delta"

    goto/16 :goto_0

    .line 527
    :cond_26
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_29

    .line 528
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 529
    const-string v0, "run_cm_abs_read"

    goto/16 :goto_0

    .line 530
    :cond_27
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "IMAGIS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 531
    const-string v0, "run_cm_abs_read"

    goto/16 :goto_0

    .line 532
    :cond_28
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 533
    const-string v0, "run_cm_abs_read"

    goto/16 :goto_0

    .line 535
    :cond_29
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CM_ABS__NODE:I

    if-ne p1, v0, :cond_2c

    .line 536
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 537
    const-string v0, "get_cm_abs"

    goto/16 :goto_0

    .line 538
    :cond_2a
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "IMAGIS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 539
    const-string v0, "get_cm_abs"

    goto/16 :goto_0

    .line 540
    :cond_2b
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 541
    const-string v0, "get_cm_abs"

    goto/16 :goto_0

    .line 545
    :cond_2c
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INSPECTION__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_2d

    .line 546
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 547
    const-string v0, "run_inspection_read"

    goto/16 :goto_0

    .line 549
    :cond_2d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INSPECTION__NODE:I

    if-ne p1, v0, :cond_2e

    .line 550
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 551
    const-string v0, "get_inspection"

    goto/16 :goto_0

    .line 555
    :cond_2e
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_2f

    .line 556
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 557
    const-string v0, "run_intensity_read"

    goto/16 :goto_0

    .line 559
    :cond_2f
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__INTENSITY__NODE:I

    if-ne p1, v0, :cond_30

    .line 560
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 561
    const-string v0, "get_intensity"

    goto/16 :goto_0

    .line 565
    :cond_30
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_32

    .line 566
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_31

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 568
    :cond_31
    const-string v0, "run_scantime_read"

    goto/16 :goto_0

    .line 570
    :cond_32
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__SCANTIME__NODE:I

    if-ne p1, v0, :cond_34

    .line 571
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_33

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 573
    :cond_33
    const-string v0, "get_scantime"

    goto/16 :goto_0

    .line 577
    :cond_34
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_36

    .line 578
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 579
    const-string v0, "run_rawcap_read"

    goto/16 :goto_0

    .line 580
    :cond_35
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 581
    const-string v0, "run_rawcap_read"

    goto/16 :goto_0

    .line 583
    :cond_36
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    if-ne p1, v0, :cond_38

    .line 584
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 585
    const-string v0, "get_rawcap"

    goto/16 :goto_0

    .line 586
    :cond_37
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 587
    const-string v0, "get_rawcap"

    goto/16 :goto_0

    .line 591
    :cond_38
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_3a

    .line 592
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 593
    const-string v0, "run_rx_to_rx_read"

    goto/16 :goto_0

    .line 595
    :cond_39
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 596
    const-string v0, "get_delta"

    goto/16 :goto_0

    .line 598
    :cond_3a
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RX_TO_RX__NODE:I

    if-ne p1, v0, :cond_3b

    .line 599
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 600
    const-string v0, "get_rx_to_rx"

    goto/16 :goto_0

    .line 604
    :cond_3b
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_3c

    .line 605
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 606
    const-string v0, "run_tx_to_tx_read"

    goto/16 :goto_0

    .line 608
    :cond_3c
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_TX__NODE:I

    if-ne p1, v0, :cond_3d

    .line 609
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 610
    const-string v0, "get_tx_to_tx"

    goto/16 :goto_0

    .line 614
    :cond_3d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_3e

    .line 615
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 616
    const-string v0, "run_tx_to_gnd_read"

    goto/16 :goto_0

    .line 618
    :cond_3e
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TX_TO_GND__NODE:I

    if-ne p1, v0, :cond_3f

    .line 619
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 620
    const-string v0, "get_tx_to_gnd"

    goto/16 :goto_0

    .line 624
    :cond_3f
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_40

    .line 625
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 626
    const-string v0, "run_raw_count_read"

    goto/16 :goto_0

    .line 628
    :cond_40
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAW_COUNT__NODE:I

    if-ne p1, v0, :cond_41

    .line 629
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 630
    const-string v0, "get_raw_count"

    goto/16 :goto_0

    .line 634
    :cond_41
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_42

    .line 635
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 636
    const-string v0, "run_difference_read"

    goto/16 :goto_0

    .line 638
    :cond_42
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DIFFERENCE__NODE:I

    if-ne p1, v0, :cond_43

    .line 639
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 640
    const-string v0, "get_difference"

    goto/16 :goto_0

    .line 644
    :cond_43
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_44

    .line 645
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 646
    const-string v0, "run_local_idac_read"

    goto/16 :goto_0

    .line 648
    :cond_44
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__LOCAL_IDAC__NODE:I

    if-ne p1, v0, :cond_45

    .line 649
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 650
    const-string v0, "get_local_idac"

    goto/16 :goto_0

    .line 654
    :cond_45
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_46

    .line 655
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 656
    const-string v0, "run_global_idac_read"

    goto/16 :goto_0

    .line 658
    :cond_46
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__GLOBAL_IDAC__NODE:I

    if-ne p1, v0, :cond_47

    .line 659
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 660
    const-string v0, "get_global_idac"

    goto/16 :goto_0

    .line 662
    :cond_47
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_CAP:I

    if-ne p1, v0, :cond_49

    .line 663
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 664
    const-string v0, "run_abscap_read"

    goto/16 :goto_0

    .line 666
    :cond_48
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 667
    const-string v0, "run_abscap_read"

    goto/16 :goto_0

    .line 669
    :cond_49
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_ID__ABS_DELTA:I

    if-ne p1, v0, :cond_4b

    .line 670
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 671
    const-string v0, "run_absdelta_read"

    goto/16 :goto_0

    .line 672
    :cond_4a
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 673
    const-string v0, "run_absdelta_read"

    goto/16 :goto_0

    .line 675
    :cond_4b
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_4d

    .line 676
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 677
    const-string v0, "run_delta_read"

    goto/16 :goto_0

    .line 678
    :cond_4c
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 679
    const-string v0, "run_delta_read"

    goto/16 :goto_0

    .line 681
    :cond_4d
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__MUTUALCAP__NODE:I

    if-ne p1, v0, :cond_4f

    .line 682
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 683
    const-string v0, "get_delta"

    goto/16 :goto_0

    .line 684
    :cond_4e
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 685
    const-string v0, "get_delta"

    goto/16 :goto_0

    .line 687
    :cond_4f
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__REGSITER__RESULT:I

    if-ne p1, v0, :cond_50

    .line 688
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 689
    const-string v0, "get_tsp_test_result"

    goto/16 :goto_0

    .line 691
    :cond_50
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_HOVER_CMD__HOVER_SET_EDGE_DEADLOCK:I

    if-ne p1, v0, :cond_51

    .line 692
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 693
    const-string v0, "hover_set_edge_rx"

    goto/16 :goto_0

    .line 695
    :cond_51
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__ABS_CAP:I

    if-ne p1, v0, :cond_52

    .line 696
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 697
    const-string v0, "run_sidekey_abscap_read"

    goto/16 :goto_0

    .line 699
    :cond_52
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_SIDETOUCH_ID__DELTA:I

    if-ne p1, v0, :cond_53

    .line 700
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 701
    const-string v0, "run_sidekey_delta_read"

    goto/16 :goto_0

    .line 703
    :cond_53
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__TRX_SHORT_TEST:I

    if-ne p1, v0, :cond_54

    .line 704
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 705
    const-string v0, "run_trx_short_test"

    goto/16 :goto_0

    .line 710
    :cond_54
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__ALL_NODE__RETURN_MIN_MAX:I

    if-ne p1, v0, :cond_55

    .line 711
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 712
    const-string v0, "run_dnd_read"

    goto/16 :goto_0

    .line 713
    :cond_55
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__ALL_NODE__RETURN_NONE:I

    if-ne p1, v0, :cond_56

    .line 714
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 715
    const-string v0, "run_dnd_read"

    goto/16 :goto_0

    .line 716
    :cond_56
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__NODE:I

    if-ne p1, v0, :cond_57

    .line 717
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 718
    const-string v0, "get_dnd"

    goto/16 :goto_0

    .line 719
    :cond_57
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__DND__DIFF:I

    if-ne p1, v0, :cond_58

    .line 720
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 721
    const-string v0, "run_reference_diff"

    goto/16 :goto_0

    .line 724
    :cond_58
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__KEY_CM_DATA:I

    if-ne p1, v0, :cond_59

    .line 725
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 726
    const-string v0, "run_key_rawcap_read"

    goto/16 :goto_0

    .line 728
    :cond_59
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__KEY_CX_DATA:I

    if-ne p1, v0, :cond_5a

    .line 729
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 730
    const-string v0, "run_key_cx_data_read"

    goto/16 :goto_0

    .line 733
    :cond_5a
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION:I

    if-ne p1, v0, :cond_65

    .line 736
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ATMEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 737
    const-string v0, "run_reference_read"

    goto/16 :goto_0

    .line 741
    :cond_5b
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "CYPRESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 742
    const-string v0, "run_raw_count_read"

    goto/16 :goto_0

    .line 746
    :cond_5c
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "MELFAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 747
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mPanelType:Ljava/lang/String;

    const-string v1, "TFT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 748
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mDeviceType:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 749
    const-string v0, "run_cm_abs_read"

    goto/16 :goto_0

    .line 751
    :cond_5d
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mPanelType:Ljava/lang/String;

    const-string v1, "OCTA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 752
    const-string v0, "run_cm_delta_read"

    goto/16 :goto_0

    .line 757
    :cond_5e
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SILAB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 792
    :cond_5f
    const-string v0, "NA"

    goto/16 :goto_0

    .line 761
    :cond_60
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 762
    const-string v0, "run_rawcap_read"

    goto/16 :goto_0

    .line 766
    :cond_61
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZINITIX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_62

    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "ZI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 768
    :cond_62
    const-string v0, "SUPPORT_RUN_REF"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 769
    const-string v0, "run_reference_read"

    goto/16 :goto_0

    .line 771
    :cond_63
    const-string v0, "run_dnd_read"

    goto/16 :goto_0

    .line 773
    :cond_64
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 774
    const-string v0, "run_rawcap_read"

    goto/16 :goto_0

    .line 776
    :cond_65
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__EXPANSION__CONNECTION_HOVERING:I

    if-ne p1, v0, :cond_67

    .line 777
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "SYNAPTICS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 778
    const-string v0, "run_abscap_read"

    goto/16 :goto_0

    .line 779
    :cond_66
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    const-string v1, "STM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 780
    const-string v0, "run_abscap_read"

    goto/16 :goto_0

    .line 784
    :cond_67
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    if-eq p1, v0, :cond_68

    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA__ALL_NODE__RETURN_NONE:I

    if-eq p1, v0, :cond_68

    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    if-ne p1, v0, :cond_69

    .line 787
    :cond_68
    const-string v0, "run_cx_data_read"

    goto/16 :goto_0

    .line 788
    :cond_69
    sget v0, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA__NODE:I

    if-ne p1, v0, :cond_5f

    .line 789
    const-string v0, "get_cx_data"

    goto/16 :goto_0
.end method

.method private setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 299
    const-string v1, "setTSPCommand"

    .line 300
    .local v1, "METHOD_NAME":Ljava/lang/String;
    const/4 v0, 0x0

    .line 301
    .local v0, "CMD_PATH":Ljava/lang/String;
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "command => "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "subCommand => "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    if-eqz p2, :cond_0

    .line 305
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 308
    :cond_0
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "1. set Command => "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 310
    .local v2, "cmd":[B
    const-string v3, ""

    .line 312
    .local v3, "status":Ljava/lang/String;
    const-string v6, "Unknown"

    iget-object v7, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CMD_NODE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 313
    const-string v6, "TSP_COMMAND_CMD"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 321
    :goto_0
    invoke-static {v0, v2}, Lcom/sec/factory/support/Support$Kernel;->writeToPath(Ljava/lang/String;[B)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 323
    iget-object v6, p0, Lcom/sec/factory/modules/TouchScreenPanel;->STATUS_NODE:Ljava/lang/String;

    invoke-static {v6, v10}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 325
    if-nez v3, :cond_2

    .line 326
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    const-string v8, "status == null"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v6, "FAIL"

    .line 369
    :goto_1
    return-object v6

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CMD_NODE:Ljava/lang/String;

    goto :goto_0

    .line 328
    :cond_2
    const-string v6, "FAIL"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 329
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    const-string v8, "status == fail"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v6, "FAIL"

    goto :goto_1

    .line 331
    :cond_3
    const-string v6, "NOT_APPLICABLE"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 332
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    const-string v8, "status == not applicable"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v6, "NOT_APPLICABLE"

    goto :goto_1

    .line 334
    :cond_4
    const-string v6, "OK"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 335
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    const-string v8, "status == ok"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v6, "OK"

    goto :goto_1

    .line 337
    :cond_5
    const-string v6, "RUNNING"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 338
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    const-string v8, "status == running"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v6, "TouchScreenPanel"

    const-string v7, "checkTSPStatus"

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 343
    .local v4, "timeStart":J
    :goto_2
    iget-object v6, p0, Lcom/sec/factory/modules/TouchScreenPanel;->STATUS_NODE:Ljava/lang/String;

    invoke-static {v6, v10}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 345
    if-eqz v3, :cond_6

    const-string v6, "OK"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 346
    const-string v6, "TouchScreenPanel"

    const-string v7, "checkTSPStatus"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "finish => "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    move-object v6, v3

    .line 362
    goto/16 :goto_1

    .line 351
    :cond_6
    iget-wide v6, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS_RUNNING_TIME_OUT:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    cmp-long v6, v6, v8

    if-gez v6, :cond_7

    .line 352
    const-string v6, "TouchScreenPanel"

    const-string v7, "checkTSPStatus"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "finish => Time Out("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p0, Lcom/sec/factory/modules/TouchScreenPanel;->TSP_STATUS_RUNNING_TIME_OUT:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v3, "FAIL"

    .line 355
    goto :goto_3

    .line 358
    :cond_7
    const-wide/16 v6, 0x64

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 359
    :catch_0
    move-exception v6

    goto :goto_2

    .line 364
    .end local v4    # "timeStart":J
    :cond_8
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "status == Unknown : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v6, "FAIL"

    goto/16 :goto_1

    .line 368
    :cond_9
    const-string v6, "TouchScreenPanel"

    const-string v7, "setTSPCommand"

    const-string v8, "Fail - Command Write"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const-string v6, "FAIL"

    goto/16 :goto_1
.end method


# virtual methods
.method public getTSPResult(ILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "tspID"    # I
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    .line 1176
    const/4 v3, 0x0

    .line 1177
    .local v3, "status":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1178
    .local v1, "result":Ljava/lang/String;
    const-string v4, "TouchScreenPanel"

    const-string v5, "getTSPResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TSP ID (Main) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->convertorTSPID(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/TouchScreenPanel;->mapping_IDnCommand(I)Ljava/lang/String;

    move-result-object v0

    .line 1181
    .local v0, "command":Ljava/lang/String;
    const-string v4, "TouchScreenPanel"

    const-string v5, "getTSPResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Main Command : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    const-string v4, "TouchScreenPanel"

    const-string v5, "getTSPResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Sub Command : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    const-string v4, "NA"

    if-eq v0, v4, :cond_2

    .line 1185
    monitor-enter p0

    .line 1187
    :try_start_0
    invoke-direct {p0, v0, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1189
    invoke-direct {p0, v0, p2}, Lcom/sec/factory/modules/TouchScreenPanel;->getTSPCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1190
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1191
    const-string v4, "TouchScreenPanel"

    const-string v5, "getTSPResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "status : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1192
    const-string v4, "TouchScreenPanel"

    const-string v5, "getTSPResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1195
    move-object v2, v1

    .line 1205
    .local v2, "returnValue":Ljava/lang/String;
    :goto_0
    const-string v4, "TouchScreenPanel"

    const-string v5, "getTSPResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "returnValue : ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    return-object v2

    .line 1190
    .end local v2    # "returnValue":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 1196
    :cond_0
    const-string v4, "NOT_APPLICABLE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1197
    const-string v2, "NA"

    .restart local v2    # "returnValue":Ljava/lang/String;
    goto :goto_0

    .line 1199
    .end local v2    # "returnValue":Ljava/lang/String;
    :cond_1
    const-string v2, "NG"

    .restart local v2    # "returnValue":Ljava/lang/String;
    goto :goto_0

    .line 1202
    .end local v2    # "returnValue":Ljava/lang/String;
    :cond_2
    const-string v2, "NA"

    .restart local v2    # "returnValue":Ljava/lang/String;
    goto :goto_0
.end method

.method public getTSPResult(ILjava/lang/String;Landroid/os/Handler;)V
    .locals 3
    .param p1, "tspID"    # I
    .param p2, "subCommand"    # Ljava/lang/String;
    .param p3, "notiHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v2, 0x0

    .line 1146
    iget-object v1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    if-nez v1, :cond_0

    .line 1147
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    .line 1150
    :cond_0
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__RAWCAP__NODE:I

    if-ne p1, v1, :cond_1

    .line 1151
    new-instance v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;

    invoke-direct {v0, p0, v2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V

    .line 1152
    .local v0, "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->setHandler(Landroid/os/Handler;)V
    invoke-static {v0, p3}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->access$900(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;Landroid/os/Handler;)V

    .line 1153
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->setParameter(ILjava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;->access$1000(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;ILjava/lang/String;)V

    .line 1154
    iget-object v1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 1171
    .end local v0    # "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadRawData;
    :goto_0
    return-void

    .line 1155
    :cond_1
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_GAP_NODE:I

    if-ne p1, v1, :cond_2

    .line 1156
    new-instance v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;

    invoke-direct {v0, p0, v2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V

    .line 1157
    .local v0, "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->setHandler(Landroid/os/Handler;)V
    invoke-static {v0, p3}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->access$1200(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;Landroid/os/Handler;)V

    .line 1158
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->setParameter(ILjava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;->access$1300(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;ILjava/lang/String;)V

    .line 1159
    iget-object v1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1160
    .end local v0    # "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxData;
    :cond_2
    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__CX_DATA_ALL_NODE:I

    if-ne p1, v1, :cond_3

    .line 1161
    new-instance v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;

    invoke-direct {v0, p0, v2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V

    .line 1162
    .local v0, "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->setHandler(Landroid/os/Handler;)V
    invoke-static {v0, p3}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->access$1500(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;Landroid/os/Handler;)V

    .line 1163
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->setParameter(ILjava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;->access$1600(Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;ILjava/lang/String;)V

    .line 1164
    iget-object v1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1166
    .end local v0    # "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThreadCxDataAll;
    :cond_3
    new-instance v0, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;

    invoke-direct {v0, p0, v2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;-><init>(Lcom/sec/factory/modules/TouchScreenPanel;Lcom/sec/factory/modules/TouchScreenPanel$1;)V

    .line 1167
    .local v0, "tspThread":Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->setHandler(Landroid/os/Handler;)V
    invoke-static {v0, p3}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->access$1800(Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;Landroid/os/Handler;)V

    .line 1168
    # invokes: Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->setParameter(ILjava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;->access$1900(Lcom/sec/factory/modules/TouchScreenPanel$TSPThread;ILjava/lang/String;)V

    .line 1169
    iget-object v1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mExevutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setHoverEnable(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    .line 1209
    const/4 v1, 0x0

    .line 1210
    .local v1, "isWritable":Z
    if-eqz p2, :cond_0

    .line 1211
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1213
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 1214
    .local v0, "cmd":[B
    iget-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->CMD_NODE:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/factory/support/Support$Kernel;->writeToPath(Ljava/lang/String;[B)Z

    move-result v1

    .line 1215
    return v1
.end method

.method public setVendor(Ljava/lang/String;)V
    .locals 3
    .param p1, "vendor"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    .line 41
    const-string v0, "TouchScreenPanel"

    const-string v1, "setVendor"

    iget-object v2, p0, Lcom/sec/factory/modules/TouchScreenPanel;->mVendor:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-void
.end method
