.class public Lcom/sec/factory/modules/SensorSpec$Light;
.super Ljava/lang/Object;
.source "SensorSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Light"
.end annotation


# static fields
.field public static final TYPE_LIGHT:I = 0x0

.field public static final TYPE_RGB:I = 0x1


# instance fields
.field public mIsSensorTypeLighte:I

.field public mIsSupportOffset:Z

.field public mIsSupportZeroDistance:Z


# direct methods
.method public constructor <init>(IZZ)V
    .locals 0
    .param p1, "isSensorTypeLighte"    # I
    .param p2, "isSupportOffset"    # Z
    .param p3, "isSupportZeroDistance"    # Z

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput p1, p0, Lcom/sec/factory/modules/SensorSpec$Light;->mIsSensorTypeLighte:I

    .line 103
    iput-boolean p2, p0, Lcom/sec/factory/modules/SensorSpec$Light;->mIsSupportOffset:Z

    .line 104
    iput-boolean p3, p0, Lcom/sec/factory/modules/SensorSpec$Light;->mIsSupportZeroDistance:Z

    .line 105
    return-void
.end method
