.class Lcom/sec/factory/modules/ModuleDevice$1;
.super Landroid/content/BroadcastReceiver;
.source "ModuleDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleDevice;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleDevice;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x1

    .line 481
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 482
    .local v0, "action":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_INTENT> action : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 486
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$000(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v1

    .line 487
    .local v1, "wasMountedExternalStorage":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$100(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v2

    .line 488
    .local v2, "wasMountedUsbStorage":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$200(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v3

    .line 489
    .local v3, "wasMountedUsbStorageB":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$300(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v4

    .line 490
    .local v4, "wasMountedUsbStorageC":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$400(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v5

    .line 491
    .local v5, "wasMountedUsbStorageD":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$500(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v6

    .line 492
    .local v6, "wasMountedUsbStorageE":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$600(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v7

    .line 494
    .local v7, "wasMountedUsbStorageF":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # invokes: Lcom/sec/factory/modules/ModuleDevice;->updateStorageMountState()V
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$700(Lcom/sec/factory/modules/ModuleDevice;)V

    .line 496
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedExternalStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedExternalStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$000(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedUsbStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$100(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedUsbStorage_B : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver. onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage_B : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$200(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedUsbStorage_C : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver. onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage_C : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$300(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedUsbStorage_D : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver. onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage_D : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$400(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedUsbStorage_E : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver. onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage_E : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$500(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> wasMountedUsbStorage_F : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver. onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage_F : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$600(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    if-nez v1, :cond_0

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$000(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_0

    .line 529
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_EXTERNAL_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 532
    :cond_0
    if-nez v2, :cond_1

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$100(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_1

    .line 533
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 536
    :cond_1
    if-nez v3, :cond_2

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$200(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_2

    .line 537
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 540
    :cond_2
    if-nez v3, :cond_3

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$300(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_3

    .line 541
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 544
    :cond_3
    if-nez v3, :cond_4

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$400(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_4

    .line 545
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 548
    :cond_4
    if-nez v3, :cond_5

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$500(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_5

    .line 549
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 552
    :cond_5
    if-nez v3, :cond_6

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$600(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-ne v8, v12, :cond_6

    .line 553
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 629
    .end local v1    # "wasMountedExternalStorage":Z
    .end local v2    # "wasMountedUsbStorage":Z
    .end local v3    # "wasMountedUsbStorageB":Z
    .end local v4    # "wasMountedUsbStorageC":Z
    .end local v5    # "wasMountedUsbStorageD":Z
    .end local v6    # "wasMountedUsbStorageE":Z
    .end local v7    # "wasMountedUsbStorageF":Z
    :cond_6
    :goto_0
    return-void

    .line 557
    :cond_7
    const-string v8, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 558
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$000(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v1

    .line 559
    .restart local v1    # "wasMountedExternalStorage":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$100(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v2

    .line 560
    .restart local v2    # "wasMountedUsbStorage":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$200(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v3

    .line 561
    .restart local v3    # "wasMountedUsbStorageB":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$300(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v4

    .line 562
    .restart local v4    # "wasMountedUsbStorageC":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$400(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v5

    .line 563
    .restart local v5    # "wasMountedUsbStorageD":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$500(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v6

    .line 564
    .restart local v6    # "wasMountedUsbStorageE":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$600(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v7

    .line 565
    .restart local v7    # "wasMountedUsbStorageF":Z
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # invokes: Lcom/sec/factory/modules/ModuleDevice;->updateStorageMountState()V
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$700(Lcom/sec/factory/modules/ModuleDevice;)V

    .line 567
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedExternalStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> mIsMountedExternalStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$000(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedUsbStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> mIsMountedUsbStorage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$100(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedUsbStorage_B : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> mIsMountedUsbStorage_B : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$200(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedUsbStorage_C : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> mIsMountedUsbStorage_C : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$300(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedUsbStorage_D : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> mIsMountedUsbStorage_D : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$400(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedUsbStorage_E : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> mIsMountedUsbStorage_E : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$500(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_UNMOUNTED> wasMountedUsbStorage_F : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v8, v8, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mMediaStateReceiver.onReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<ACTION_MEDIA_MOUNTED> mIsMountedUsbStorage_F : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z
    invoke-static {v11}, Lcom/sec/factory/modules/ModuleDevice;->access$600(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    if-ne v1, v12, :cond_8

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedExternalStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$000(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 601
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_EXTERNAL_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 604
    :cond_8
    if-ne v2, v12, :cond_9

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorage:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$100(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 605
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 608
    :cond_9
    if-ne v3, v12, :cond_a

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageB:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$200(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 609
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 612
    :cond_a
    if-ne v4, v12, :cond_b

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageC:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$300(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 613
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 616
    :cond_b
    if-ne v5, v12, :cond_c

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageD:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$400(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 617
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 620
    :cond_c
    if-ne v6, v12, :cond_d

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageE:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$500(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 621
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 624
    :cond_d
    if-ne v7, v12, :cond_6

    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIsMountedUsbStorageF:Z
    invoke-static {v8}, Lcom/sec/factory/modules/ModuleDevice;->access$600(Lcom/sec/factory/modules/ModuleDevice;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 625
    iget-object v8, p0, Lcom/sec/factory/modules/ModuleDevice$1;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
