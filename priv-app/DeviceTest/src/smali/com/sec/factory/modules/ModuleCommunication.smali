.class public Lcom/sec/factory/modules/ModuleCommunication;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleCommunication.java"


# static fields
.field private static final ACTION_BLUETOOTH_AUDIO_TEST_START:I = 0x8

.field private static final ACTION_BLUETOOTH_AUDIO_TEST_STOP:I = 0x9

.field private static final ACTION_BLUETOOTH_BLE_DISCOVERY_START_WITH_ACK:I = 0x5

.field private static final ACTION_BLUETOOTH_BLE_DISCOVERY_START_WITH_ACK_ON_10S:I = 0x6

.field private static final ACTION_BLUETOOTH_DISCOVERY_START:I = 0x3

.field private static final ACTION_BLUETOOTH_DISCOVERY_START_WITH_ACK:I = 0x4

.field private static final ACTION_BLUETOOTH_DISCOVERY_STOP:I = 0x7

.field private static final ACTION_BLUETOOTH_LE_CARRIER_HIGH:I = 0x16

.field private static final ACTION_BLUETOOTH_LE_CARRIER_LOW:I = 0x14

.field private static final ACTION_BLUETOOTH_LE_CARRIER_MID:I = 0x15

.field private static final ACTION_BLUETOOTH_LE_INBAND_HIGH:I = 0x19

.field private static final ACTION_BLUETOOTH_LE_INBAND_LOW:I = 0x17

.field private static final ACTION_BLUETOOTH_LE_INBAND_MID:I = 0x18

.field private static final ACTION_BLUETOOTH_LE_MOD_HIGH:I = 0x10

.field private static final ACTION_BLUETOOTH_LE_MOD_HIGH_AA:I = 0x13

.field private static final ACTION_BLUETOOTH_LE_MOD_LOW:I = 0xe

.field private static final ACTION_BLUETOOTH_LE_MOD_LOW_AA:I = 0x11

.field private static final ACTION_BLUETOOTH_LE_MOD_MID:I = 0xf

.field private static final ACTION_BLUETOOTH_LE_MOD_MID_AA:I = 0x12

.field private static final ACTION_BLUETOOTH_LE_OUTPUT_HIGH:I = 0xd

.field private static final ACTION_BLUETOOTH_LE_OUTPUT_LOW:I = 0xb

.field private static final ACTION_BLUETOOTH_LE_OUTPUT_MID:I = 0xc

.field private static final ACTION_BLUETOOTH_LE_RX_HIGH:I = 0x1c

.field private static final ACTION_BLUETOOTH_LE_RX_LOW:I = 0x1a

.field private static final ACTION_BLUETOOTH_LE_RX_MID:I = 0x1b

.field private static final ACTION_BLUETOOTH_LE_TEST_END:I = 0x1d

.field private static final ACTION_BLUETOOTH_OFF:I = 0x2

.field private static final ACTION_BLUETOOTH_ON:I = 0x0

.field private static final ACTION_BLUETOOTH_ON_WHERE_ATCMD:I = 0x1

.field private static final ACTION_BLUETOOTH_SET_DISCOVERABLE:I = 0xa

.field protected static final ACTION_BT_ID_READ:Ljava/lang/String; = "com.sec.android.app.bluetoothtest.BT_ID_READ"

.field public static final ACTION_BT_ID_RESPONSE:Ljava/lang/String; = "com.sec.android.app.bluetoothtest.BT_ID_RESPONSE"

.field protected static final ACTION_BT_ID_WRITE:Ljava/lang/String; = "com.sec.android.app.bluetoothtest.BT_ID_WRITE"

.field public static final ACTION_BT_SERVICE_REQUEST:Ljava/lang/String; = "com.sec.android.app.bluetoothtest.BT_SERVICE_REQUEST"

.field public static final ACTION_BT_SERVICE_RESPONSE:Ljava/lang/String; = "com.sec.factory.intent.ACTION_BT_SERVICE_RESPONSE"

.field private static final ACTION_FELICA_LOCK_REQUEST:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_REQUEST"

.field public static final ACTION_FELICA_RESPONSE:Ljava/lang/String; = "com.sec.factory.intent.ACTION_FELICA_RESPONSE"

.field protected static final ACTION_WIFI_ID_READ:Ljava/lang/String; = "com.sec.android.app.wlantest.WIFI_ID_READ"

.field protected static final ACTION_WIFI_ID_WRITE:Ljava/lang/String; = "com.sec.android.app.wlantest.WIFI_ID_WRITE"

.field private static final BEIDOU:I = 0x2

.field public static final BEIDOU_STARTED_PATH:Ljava/lang/String; = "/data/beidou_started"

.field public static final BT_RESULT_FOUND:Ljava/lang/String; = "FOUND"

.field public static final BT_RESULT_NG:Ljava/lang/String; = "NG"

.field public static final BT_RESULT_NOT_FOUND:Ljava/lang/String; = "NOT FOUND"

.field public static final BT_RESULT_OFF:Ljava/lang/String; = "OFF"

.field public static final BT_RESULT_ON:Ljava/lang/String; = "ON"

.field public static final EXTRA_KEY_BT_SERVICE_REQUEST:Ljava/lang/String; = "CMDID"

.field public static final EXTRA_KEY_BT_SERVICE_RESPONSE:Ljava/lang/String; = "result"

.field public static final FM_RADIO_EARPHONE:I = 0x0

.field public static final FM_RADIO_FACTORYRSSI:Ljava/lang/String; = "test.mode.radio.factoryrssi"

.field public static final FM_RADIO_FACTORYRSSI_RESSPONSE:Ljava/lang/String; = "test.mode.radio.factoryrssi.response"

.field public static final FM_RADIO_FREQUENCY_RESPONSE:Ljava/lang/String; = "test.mode.radio.freq.response"

.field public static final FM_RADIO_SPEAKER:I = 0x1

.field private static final FM_RADIO_TARGET:[Ljava/lang/String;

.field public static final FM_RADIO_TEST_OFF:Ljava/lang/String; = "test.mode.radio.off"

.field public static final FM_RADIO_TEST_OFF_RESPONSE:Ljava/lang/String; = "test.mode.radio.off.response"

.field public static final FM_RADIO_TEST_ON:Ljava/lang/String; = "test.mode.radio.on.freq"

.field public static final FM_RADIO_TEST_ON_RESPONSE:Ljava/lang/String; = "test.mode.radio.on.response"

.field private static final GLONASS:I = 0x0

.field public static final GLONASS_FCN_PATH:Ljava/lang/String; = "/data/glonass_fcn"

.field public static final GLONASS_STARTED_PATH:Ljava/lang/String; = "/data/glonass_started"

.field private static final GPS:I = 0x1

.field public static final GPS_CNO_PATH:Ljava/lang/String; = "/data/sv_cno.info"

.field public static final GPS_STARTED_PATH:Ljava/lang/String; = "/data/gps_started"

.field public static final SMD_STARTED_PATH:Ljava/lang/String; = "/data/smd_started"

.field private static isOneCommand:Z

.field private static mInstance:Lcom/sec/factory/modules/ModuleCommunication;


# instance fields
.field private final AXEFUSE_SYSTEM_PROPERTY:Ljava/lang/String;

.field private final EFUSE_OFF:Ljava/lang/String;

.field private final EFUSE_ON:Ljava/lang/String;

.field public final ETHERNET_CONNECT_AP:I

.field public final ETHERNET_CONNECT_CABLE:I

.field private final ETHERNET_STATE_OFF:Ljava/lang/String;

.field private final ETHERNET_STATE_ON:Ljava/lang/String;

.field private final ETHERNET_SWITCH_SYSTEM_PROPERTY:Ljava/lang/String;

.field private final WORKING_DELAY_TIME:I

.field public mGpsEnabledByFactory:Z

.field private mGpsReceiver:Landroid/content/BroadcastReceiver;

.field private mIsRunningBtDevice:Z

.field private mIsRunningFelicaDevice:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    sput-boolean v2, Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z

    .line 801
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "earphone"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "speaker"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunication;->FM_RADIO_TARGET:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 32
    const-string v0, "ModuleCommunication"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 80
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    .line 82
    new-instance v0, Lcom/sec/factory/modules/ModuleCommunication$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommunication$1;-><init>(Lcom/sec/factory/modules/ModuleCommunication;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsReceiver:Landroid/content/BroadcastReceiver;

    .line 289
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningBtDevice:Z

    .line 870
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningFelicaDevice:Z

    .line 955
    const-string v0, "persist.eth.on"

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->ETHERNET_SWITCH_SYSTEM_PROPERTY:Ljava/lang/String;

    .line 956
    const-string v0, "persist.eth.efuse"

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->AXEFUSE_SYSTEM_PROPERTY:Ljava/lang/String;

    .line 957
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->ETHERNET_STATE_ON:Ljava/lang/String;

    .line 958
    const-string v0, "1"

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->ETHERNET_STATE_OFF:Ljava/lang/String;

    .line 959
    const-string v0, "1"

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->EFUSE_ON:Ljava/lang/String;

    .line 960
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->EFUSE_OFF:Ljava/lang/String;

    .line 961
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->WORKING_DELAY_TIME:I

    .line 962
    iput v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->ETHERNET_CONNECT_AP:I

    .line 963
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->ETHERNET_CONNECT_CABLE:I

    .line 33
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleCommunication"

    const-string v2, "Create ModuleCommunication"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.sec.factory.intent.ACTION_GPS_SERVICE_RESPONSE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/factory/modules/ModuleCommunication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 41
    return-void
.end method

.method private ConvertAddressColonFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1081
    const-string v0, ""

    .line 1082
    .local v0, "colonadded":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1083
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-ne v1, v2, :cond_0

    .line 1084
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1082
    :goto_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 1086
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1088
    :cond_1
    return-object v0
.end method

.method private EfuseSwitch(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 981
    const-string v0, "persist.eth.efuse"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    return-void
.end method

.method private EthernetState()Z
    .locals 2

    .prologue
    .line 977
    const-string v0, "persist.eth.on"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private EthernetSwitch(Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 966
    const-string v1, "persist.eth.on"

    invoke-static {v1, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 970
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "EthernetSwitch"

    const-string v3, "deley MountTime"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 974
    :goto_0
    return-void

    .line 971
    :catch_0
    move-exception v0

    .line 972
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 28
    sget-boolean v0, Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 28
    sput-boolean p0, Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z

    return p0
.end method

.method public static deleteGpsFiles()V
    .locals 4

    .prologue
    .line 273
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/sv_cno.info"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 274
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/gps_started"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 275
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/glonass_started"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 276
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/smd_started"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 277
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/glonass_fcn"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 278
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/beidou_started"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    .local v0, "ex":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 279
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 280
    .restart local v0    # "ex":Ljava/lang/Exception;
    const-string v1, "ModuleCommunication"

    const-string v2, "deleteGpsFiles"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private gpsAction(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 115
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startService(Landroid/content/Intent;)V

    .line 118
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommunication;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    sget-object v0, Lcom/sec/factory/modules/ModuleCommunication;->mInstance:Lcom/sec/factory/modules/ModuleCommunication;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/sec/factory/modules/ModuleCommunication;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommunication;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunication;->mInstance:Lcom/sec/factory/modules/ModuleCommunication;

    .line 48
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModuleCommunication;->mInstance:Lcom/sec/factory/modules/ModuleCommunication;

    return-object v0
.end method


# virtual methods
.method public ConnectionCheckEthernet(I)Z
    .locals 6
    .param p1, "condition"    # I

    .prologue
    .line 1052
    const/4 v0, 0x0

    .line 1053
    .local v0, "connect_state":Z
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetState()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1054
    const-string v3, "0"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetSwitch(Ljava/lang/String;)V

    .line 1056
    const-wide/16 v4, 0x7d0

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 1057
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "ConnectionCheckEthernet"

    const-string v5, "deley MountTime"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1062
    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 1077
    :cond_1
    :goto_1
    return v0

    .line 1058
    :catch_0
    move-exception v1

    .line 1059
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1064
    .end local v1    # "e":Ljava/lang/Exception;
    :pswitch_0
    const-string v3, "ETHERNET_MAC_ADDRESS"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1065
    const/4 v0, 0x1

    goto :goto_1

    .line 1068
    :pswitch_1
    const/4 v2, 0x0

    .line 1069
    .local v2, "resData":Ljava/lang/String;
    const-string v3, "ETHERNET_CONNECTION"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1070
    if-eqz v2, :cond_1

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1071
    const/4 v0, 0x1

    goto :goto_1

    .line 1062
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public ReadEthernetMac()Ljava/lang/String;
    .locals 7

    .prologue
    .line 1022
    const/4 v2, 0x0

    .line 1023
    .local v2, "resData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1025
    .local v3, "resData_efs":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetState()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1026
    const-string v4, "0"

    invoke-direct {p0, v4}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetSwitch(Ljava/lang/String;)V

    .line 1029
    :cond_0
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    const/16 v4, 0xa

    if-ge v0, v4, :cond_1

    .line 1030
    const-string v4, "ETHERNET_MAC_ADDRESS"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1031
    const-string v4, "00:00:00:00:00:00"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1032
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "ReadEthernetMac"

    const-string v6, "Mount Success"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    :cond_1
    const-string v4, ":"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 1044
    const-string v4, "ETHERNET_MAC_ADDRESS_EFS"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1045
    if-eqz v2, :cond_2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1046
    :cond_2
    const-string v2, "NG"

    .line 1048
    :cond_3
    return-object v2

    .line 1036
    :cond_4
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 1037
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "ReadEthernetMac"

    const-string v6, "deley MountTime"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1029
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1038
    :catch_0
    move-exception v1

    .line 1039
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public WriteEthernetMac(Ljava/lang/String;)Z
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 985
    new-instance v1, Ljava/io/File;

    const-string v3, "/efs/eth"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 987
    .local v1, "ethDirectory":Ljava/io/File;
    const/4 v2, 0x0

    .line 989
    .local v2, "resData":Z
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 990
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "WriteEthernetMac"

    const-string v5, "Create ETHERNET_MAC_ADDRESS_EFS directory..."

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 992
    invoke-virtual {v1, v6, v6}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 993
    invoke-virtual {v1, v6, v6}, Ljava/io/File;->setWritable(ZZ)Z

    .line 994
    const/4 v3, 0x0

    invoke-virtual {v1, v6, v3}, Ljava/io/File;->setReadable(ZZ)Z

    .line 997
    :cond_0
    const-string v3, "ETHERNET_MAC_ADDRESS_EFS"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 999
    if-eqz v2, :cond_3

    .line 1000
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetState()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1001
    const-string v3, "0"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetSwitch(Ljava/lang/String;)V

    .line 1004
    :cond_1
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 1005
    const-string v3, "1"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->EfuseSwitch(Ljava/lang/String;)V

    .line 1006
    const-wide/16 v4, 0xbb8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 1007
    const-string v3, "0"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->EfuseSwitch(Ljava/lang/String;)V

    .line 1009
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetState()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1010
    const-string v3, "1"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetSwitch(Ljava/lang/String;)V

    .line 1012
    :cond_2
    const-string v3, "0"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->EthernetSwitch(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1018
    :cond_3
    :goto_0
    return v2

    .line 1013
    :catch_0
    move-exception v0

    .line 1014
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .line 1015
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bleSearchStartWithAck()V
    .locals 3

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "bleSearchStartWithAck"

    const-string v2, "Start Bluetooth BLE Search timeout=20sec"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 401
    return-void
.end method

.method public bleSearchStartWithAckOn10s()V
    .locals 3

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "bleSearchStartWithAck"

    const-string v2, "Start Bluetooth BLE Search timeout=10sec"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 409
    return-void
.end method

.method public btActivation()V
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btActivation"

    const-string v2, "Bluetooth Activation"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningBtDevice:Z

    .line 354
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 357
    return-void
.end method

.method public btActivationWhereAtcmd()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 360
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "btActivationWhereAtcmd"

    const-string v3, "Bluetooth Activation where Atcmd"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    iput-boolean v4, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningBtDevice:Z

    .line 362
    const-string v1, "Marvell"

    const-string v2, "CHIPSET_MANUFACTURE"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->isEnabledBtDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 365
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/factory/modules/ModuleCommunication$2;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleCommunication$2;-><init>(Lcom/sec/factory/modules/ModuleCommunication;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 375
    .end local v0    # "timer":Ljava/util/Timer;
    :goto_0
    return-void

    .line 372
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    goto :goto_0
.end method

.method public btAudioTestStart()V
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btAudioTestStart"

    const-string v2, "Start Bluetooth Audio Test"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 422
    return-void
.end method

.method public btAudioTestStop()V
    .locals 3

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btAudioTestStop"

    const-string v2, "Stop Bluetooth Audio Test"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 428
    return-void
.end method

.method public btDeactivation()V
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btDeactivation"

    const-string v2, "Bluetooth Deactivation"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningBtDevice:Z

    .line 380
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 382
    return-void
.end method

.method public btLeRxHigh()V
    .locals 3

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeRxHigh"

    const-string v2, "btLeRxHigh()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 559
    return-void
.end method

.method public btLeRxLow()V
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeRxLow"

    const-string v2, "btLeRxLow()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 547
    return-void
.end method

.method public btLeRxMid()V
    .locals 3

    .prologue
    .line 550
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeRxMid"

    const-string v2, "btLeRxMid()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const/16 v0, 0x1b

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 553
    return-void
.end method

.method public btLeTestEnd()V
    .locals 3

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTestEnd"

    const-string v2, "btLeTestEnd()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 565
    return-void
.end method

.method public btLeTxCarrierChHigh()V
    .locals 3

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxCarrierChHigh"

    const-string v2, "btLeTxCarrierChHigh()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 523
    return-void
.end method

.method public btLeTxCarrierChLow()V
    .locals 3

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxCarrierChLow"

    const-string v2, "btLeTxCarrierChLow()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 511
    return-void
.end method

.method public btLeTxCarrierChMid()V
    .locals 3

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxCarrierChLow"

    const-string v2, "btLeTxCarrierChLow()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 517
    return-void
.end method

.method public btLeTxInBandChHigh()V
    .locals 3

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxInBandChHigh"

    const-string v2, "btLeTxInBandChHigh()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const/16 v0, 0x19

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 541
    return-void
.end method

.method public btLeTxInBandChLow()V
    .locals 3

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxInBandChLow"

    const-string v2, "btLeTxInBandChLow()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    const/16 v0, 0x17

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 529
    return-void
.end method

.method public btLeTxInBandChMid()V
    .locals 3

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxInBandChMid"

    const-string v2, "btLeTxInBandChMid()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 535
    return-void
.end method

.method public btLeTxModChHigh()V
    .locals 3

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxModChMid"

    const-string v2, "btLeTxModChMid()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 487
    return-void
.end method

.method public btLeTxModChHighAA()V
    .locals 3

    .prologue
    .line 502
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxModChLowAA"

    const-string v2, "btLeTxModChLowAA()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 505
    return-void
.end method

.method public btLeTxModChLow()V
    .locals 3

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxModChLow"

    const-string v2, "btLeTxModChLow()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 475
    return-void
.end method

.method public btLeTxModChLowAA()V
    .locals 3

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxModChLowAA"

    const-string v2, "btLeTxModChLowAA()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 493
    return-void
.end method

.method public btLeTxModChMid()V
    .locals 3

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxModChMid"

    const-string v2, "btLeTxModChMid()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 481
    return-void
.end method

.method public btLeTxModChMidAA()V
    .locals 3

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxModChLowAA"

    const-string v2, "btLeTxModChLowAA()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 499
    return-void
.end method

.method public btLeTxOutputChHigh()V
    .locals 3

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxOutputChHigh"

    const-string v2, "btLeTxOutputChHigh()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 469
    return-void
.end method

.method public btLeTxOutputChLow()V
    .locals 3

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxOutputChLow"

    const-string v2, "btLeTxOutputChLow()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 457
    return-void
.end method

.method public btLeTxOutputChMid()V
    .locals 3

    .prologue
    .line 460
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btLeTxOutputChMid"

    const-string v2, "btLeTxOutputChMid()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 463
    return-void
.end method

.method public btRfTestStart()V
    .locals 4

    .prologue
    .line 431
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "btAudioTestStart"

    const-string v3, "Start Bluetooth RF Test"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 434
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.bluetoothtest"

    const-string v2, "com.sec.android.app.bluetoothtest.BluetoothRfTest"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 437
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startActivity(Landroid/content/Intent;)V

    .line 438
    return-void
.end method

.method public btRfTestStop()V
    .locals 4

    .prologue
    .line 441
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "btAudioTestStart"

    const-string v3, "Stop Bluetooth RF Test"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.samsungtest.BluetoothRfTestOff"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 444
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 445
    return-void
.end method

.method public btSearchStart()V
    .locals 3

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btSearchStart"

    const-string v2, "Start Bluetooth Search"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 388
    return-void
.end method

.method public btSearchStartWithAck()V
    .locals 3

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btSearchStartWithAck"

    const-string v2, "Start Bluetooth Search timeout=10sec"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 394
    return-void
.end method

.method public btSearchStop()V
    .locals 3

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btSearchStop"

    const-string v2, "Stop Bluetooth Search"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 416
    return-void
.end method

.method public btSetDiscoverable()V
    .locals 3

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "btSetDiscoverable"

    const-string v2, "btSetDiscoverable()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->requestActionToBTService(I)V

    .line 451
    return-void
.end method

.method public felicaActivationWhereAtcmd()V
    .locals 4

    .prologue
    .line 883
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "felicaActivationWhereAtcmd"

    const-string v3, "Felica Activation where Atcmd"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 886
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.felicatest"

    const-string v2, "com.sec.android.app.felicatest.FeliCaTest"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 888
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 889
    const-string v1, "FeliCa_Start_Parameter"

    const/16 v2, 0xde

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 890
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 891
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningFelicaDevice:Z

    .line 892
    return-void
.end method

.method public felicaDeactivationWhereAtCmd()V
    .locals 3

    .prologue
    .line 918
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_GETNV"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 919
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 920
    const-string v1, "I_DATA"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 921
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 922
    return-void
.end method

.method public felicaInitPassword()V
    .locals 4

    .prologue
    .line 936
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "felicaIDmRead"

    const-string v3, "Felica Init Password"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.felicalock.FELICA_LOCK_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 938
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 939
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 940
    return-void
.end method

.method public felicaReadPassword()V
    .locals 4

    .prologue
    .line 943
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "felicaReadPassword"

    const-string v3, "Felica Read Password"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.felicalock.FELICA_LOCK_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 945
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMD"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 946
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 947
    return-void
.end method

.method public felicaSearchTag()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 926
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "felicaSearchTag"

    const-string v3, "Felica TAG search"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.felicalock.FELICA_TAG_SEARCH_REQUESTN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 929
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 930
    const-string v1, "I_DATA"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 931
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 932
    return-void
.end method

.method public felicaSetDataFailWhereAtCmd()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 911
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_FAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 912
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 913
    const-string v1, "I_DATA"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 914
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 915
    return-void
.end method

.method public felicaSetDataNTxWhereAtcmd(I)V
    .locals 3
    .param p1, "i_data"    # I

    .prologue
    .line 876
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_INDICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 877
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 878
    const-string v1, "I_DATA"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 879
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 880
    return-void
.end method

.method public felicaSetDataPassWhereAtCmd()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 904
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_PASS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 905
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 906
    const-string v1, "I_DATA"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 907
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 908
    return-void
.end method

.method public felicafelicaIDmReadWhereAtCmd()V
    .locals 3

    .prologue
    .line 895
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 896
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.felicatest"

    const-string v2, "com.sec.android.app.felicatest.FeliCaTest"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 898
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 899
    const-string v1, "FeliCa_Start_Parameter"

    const/16 v2, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 900
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 901
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunication;->mInstance:Lcom/sec/factory/modules/ModuleCommunication;

    .line 58
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 59
    return-void
.end method

.method public fmRadioOff()V
    .locals 3

    .prologue
    .line 820
    new-instance v0, Landroid/content/Intent;

    const-string v1, "test.mode.radio.off"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 822
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "qualcomm"

    const-string v2, "FM_RADIO_SW_VENDOR"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 823
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 828
    :goto_0
    return-void

    .line 825
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 826
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public fmRadioOn(Ljava/lang/String;I)V
    .locals 5
    .param p1, "frequency"    # Ljava/lang/String;
    .param p2, "target"    # I

    .prologue
    .line 806
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "fmRadioOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "frequency: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    new-instance v0, Landroid/content/Intent;

    const-string v1, "test.mode.radio.on.freq"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 808
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "frequency"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 809
    const-string v1, "output"

    sget-object v2, Lcom/sec/factory/modules/ModuleCommunication;->FM_RADIO_TARGET:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 811
    const-string v1, "qualcomm"

    const-string v2, "FM_RADIO_SW_VENDOR"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 812
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 817
    :goto_0
    return-void

    .line 814
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 815
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public fmRadioReadFactoryRssi()V
    .locals 3

    .prologue
    .line 843
    new-instance v0, Landroid/content/Intent;

    const-string v1, "test.mode.radio.factoryrssi"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 845
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "qualcomm"

    const-string v2, "FM_RADIO_SW_VENDOR"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 846
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 851
    :goto_0
    return-void

    .line 848
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 849
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public fmRadioReadFrequencyIntensity(Ljava/lang/String;)V
    .locals 3
    .param p1, "frequency"    # Ljava/lang/String;

    .prologue
    .line 831
    new-instance v0, Landroid/content/Intent;

    const-string v1, "test.mode.radio.freq"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 832
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "frequency"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 834
    const-string v1, "qualcomm"

    const-string v2, "FM_RADIO_SW_VENDOR"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 835
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 840
    :goto_0
    return-void

    .line 837
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 838
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public fmRadioWriteFactoryRssi(Ljava/lang/String;)V
    .locals 3
    .param p1, "Rssi"    # Ljava/lang/String;

    .prologue
    .line 854
    new-instance v0, Landroid/content/Intent;

    const-string v1, "test.mode.radio.factoryrssi"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 855
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "signal_strength"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 857
    const-string v1, "qualcomm"

    const-string v2, "FM_RADIO_SW_VENDOR"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 863
    :goto_0
    return-void

    .line 860
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 861
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public gpsActivation(Landroid/content/Context;IZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I
    .param p3, "_isOneCommand"    # Z

    .prologue
    const/4 v7, 0x1

    .line 135
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "[GPS debug #1]gpsActivation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GPS Activation - mode(GPS : 1, GLONASS : 0, BEIDOU : 2) = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",  isOneCommand = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/sv_cno.info"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 139
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/gps_started"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 140
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/glonass_started"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 141
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/smd_started"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 142
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/beidou_started"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 144
    const/4 v1, 0x0

    .line 146
    .local v1, "filePath":Ljava/io/File;
    sput-boolean p3, Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z

    .line 148
    if-ne p2, v7, :cond_1

    .line 149
    new-instance v1, Ljava/io/File;

    .end local v1    # "filePath":Ljava/io/File;
    const-string v3, "/data/gps_started"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 156
    .restart local v1    # "filePath":Ljava/io/File;
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 157
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "[GPS debug #1]gpsSmdActivation"

    const-string v5, "filePath == null"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :goto_1
    return-void

    .line 150
    :cond_1
    if-nez p2, :cond_2

    .line 151
    new-instance v1, Ljava/io/File;

    .end local v1    # "filePath":Ljava/io/File;
    const-string v3, "/data/glonass_started"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v1    # "filePath":Ljava/io/File;
    goto :goto_0

    .line 152
    :cond_2
    const/4 v3, 0x2

    if-ne p2, v3, :cond_0

    .line 153
    new-instance v1, Ljava/io/File;

    .end local v1    # "filePath":Ljava/io/File;
    const-string v3, "/data/beidou_started"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v1    # "filePath":Ljava/io/File;
    goto :goto_0

    .line 162
    :cond_3
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 163
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1ff

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-static {v3, v4, v5, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_2
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gps"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    .line 172
    .local v2, "gpsEnabled":Z
    if-nez v2, :cond_4

    .line 174
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gps"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 176
    iput-boolean v7, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    .line 177
    const-string v3, "ACTION_GPS_START_DELAY"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->gpsAction(Ljava/lang/String;)V

    goto :goto_1

    .line 165
    .end local v2    # "gpsEnabled":Z
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 179
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "gpsEnabled":Z
    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    .line 180
    const-string v3, "ACTION_GPS_START"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->gpsAction(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public gpsDeactivation()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "gpsDeactivation"

    const-string v2, "GPS Deactivation"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->deleteGpsFiles()V

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->stopService(Landroid/content/Intent;)V

    .line 127
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 130
    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    .line 132
    :cond_0
    return-void
.end method

.method public gpsReadCN()Ljava/lang/String;
    .locals 5

    .prologue
    .line 215
    const-string v1, "/data/sv_cno.info"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleCommunication;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "cno":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 218
    :cond_0
    const-string v0, "NG"

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "gpsReadCN_SingleChannle"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CNO="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    return-object v0
.end method

.method public gpsSmdActivation(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    .line 185
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "[GPS debug #1]gpsSmdActivation"

    const-string v5, "GPS SMD Activation "

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->deleteGpsFiles()V

    .line 189
    new-instance v1, Ljava/io/File;

    const-string v3, "/data/smd_started"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 192
    .local v1, "filePath":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 193
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1ff

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-static {v3, v4, v5, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gps"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->isLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    .line 202
    .local v2, "gpsEnabled":Z
    if-nez v2, :cond_0

    .line 204
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gps"

    invoke-static {v3, v4, v7}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 206
    iput-boolean v7, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    .line 207
    const-string v3, "ACTION_SMD_START_DELAY"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->gpsAction(Ljava/lang/String;)V

    .line 212
    :goto_1
    return-void

    .line 195
    .end local v2    # "gpsEnabled":Z
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 209
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "gpsEnabled":Z
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->mGpsEnabledByFactory:Z

    .line 210
    const-string v3, "ACTION_SMD_START"

    invoke-direct {p0, v3}, Lcom/sec/factory/modules/ModuleCommunication;->gpsAction(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isEnabledBtDevice()Z
    .locals 2

    .prologue
    .line 578
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 579
    .local v0, "mBtAdapter":Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    return v1
.end method

.method public isRunningBtDevice()Z
    .locals 1

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommunication;->mIsRunningBtDevice:Z

    return v0
.end method

.method public nfcOff()V
    .locals 3

    .prologue
    .line 617
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 618
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "action"

    const-string v2, "ACTION_NFC_OFF"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 619
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startService(Landroid/content/Intent;)V

    .line 620
    return-void
.end method

.method public nfcOn()V
    .locals 3

    .prologue
    .line 611
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 612
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "action"

    const-string v2, "ACTION_NFC_ON"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 613
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->startService(Landroid/content/Intent;)V

    .line 614
    return-void
.end method

.method public readBtId()V
    .locals 2

    .prologue
    .line 583
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.bluetoothtest.BT_ID_READ"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 584
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 585
    return-void
.end method

.method protected readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 238
    const-string v5, ""

    .line 239
    .local v5, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 242
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1fa0

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 245
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "bufline":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 247
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 255
    .end local v2    # "bufline":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_4

    .line 257
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 264
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v5, :cond_2

    .line 265
    const-string v5, ""

    .line 267
    .end local v5    # "result":Ljava/lang/String;
    :cond_2
    return-object v5

    .line 258
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "result":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 259
    .local v3, "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 260
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 250
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 251
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "FileNotFoundException"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 255
    if-eqz v0, :cond_1

    .line 257
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 258
    :catch_2
    move-exception v3

    .line 259
    .restart local v3    # "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 252
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 253
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 255
    if-eqz v0, :cond_1

    .line 257
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 258
    :catch_4
    move-exception v3

    .line 259
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 255
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v0, :cond_3

    .line 257
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 260
    :cond_3
    :goto_4
    throw v6

    .line 258
    :catch_5
    move-exception v3

    .line 259
    .restart local v3    # "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 255
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 252
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_6
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 250
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_7
    move-exception v4

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public readWifiId()V
    .locals 2

    .prologue
    .line 768
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.wlantest.WIFI_ID_READ"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 769
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 770
    return-void
.end method

.method public requestActionToBTService(I)V
    .locals 5
    .param p1, "extra"    # I

    .prologue
    .line 345
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "requestActionToBTService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Request extra :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.bluetoothtest.BT_SERVICE_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 347
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "CMDID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 348
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 349
    return-void
.end method

.method protected sendResponse(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 226
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.samsungtest.GPS_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 229
    return-void
.end method

.method protected sendStartOK()V
    .locals 2

    .prologue
    .line 233
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.samsungtest.GPS_OK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 234
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 235
    return-void
.end method

.method public writeBtId(Ljava/lang/String;)V
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 588
    const-string v0, ""

    .line 590
    .local v0, "colonadded":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 591
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-ne v1, v3, :cond_0

    .line 592
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 590
    :goto_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 594
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, 0x2

    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 598
    :cond_1
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "writeBtId"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "colonadded="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.bluetoothtest.BT_ID_WRITE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 600
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "RECEIVER_FOREGROUND_FLAG"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 601
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 602
    :cond_2
    const-string v3, "MAC_DATA"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 603
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 604
    return-void
.end method

.method public writeWifiId(Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 773
    const-string v0, ""

    .line 775
    .local v0, "colonadded":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 776
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-ne v1, v3, :cond_0

    .line 777
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 775
    :goto_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 779
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, 0x2

    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 783
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.wlantest.WIFI_ID_WRITE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 784
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "MAC_DATA"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 785
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleCommunication;->sendBroadcast(Landroid/content/Intent;)V

    .line 786
    return-void
.end method
