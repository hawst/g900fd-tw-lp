.class public Lcom/sec/factory/modules/SensorFeature;
.super Ljava/lang/Object;
.source "SensorFeature.java"


# static fields
.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA022:Ljava/lang/String; = "BOSCH_BMA022"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA023:Ljava/lang/String; = "BOSCH_BMA023"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA220:Ljava/lang/String; = "BOSCH_BMA220"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA222:Ljava/lang/String; = "BOSCH_BMA222"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA250:Ljava/lang/String; = "BOSCH_BMA250"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA254:Ljava/lang/String; = "BOSCH_BMA254"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMC150:Ljava/lang/String; = "BOSCH_BMC150"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMI055:Ljava/lang/String; = "BOSCH_BMI055"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMI058:Ljava/lang/String; = "BOSCH_BMI058"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMI160:Ljava/lang/String; = "BOSCH_BMI160"

.field public static final FEATURE_ACCELEROMETER_BOSCH_SMB380:Ljava/lang/String; = "BOSCH_SMB380"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6050:Ljava/lang/String; = "INVENSENSE_MPU6050"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6051:Ljava/lang/String; = "INVENSENSE_MPU6051"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6500:Ljava/lang/String; = "INVENSENSE_MPU6500"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6515M:Ljava/lang/String; = "INVENSENSE_MPU6515M"

.field public static final FEATURE_ACCELEROMETER_KIONIX_KXTF9:Ljava/lang/String; = "KIONIX_KXTF9"

.field public static final FEATURE_ACCELEROMETER_KIONIX_KXUD9:Ljava/lang/String; = "KIONIX_KXUD9"

.field public static final FEATURE_ACCELEROMETER_MAXIM:Ljava/lang/String; = "MAXIM"

.field public static final FEATURE_ACCELEROMETER_STMICRO_K2DH:Ljava/lang/String; = "STMICRO_K2DH"

.field public static final FEATURE_ACCELEROMETER_STMICRO_K2DH_REV:Ljava/lang/String; = "STMICRO_K2DH_REV"

.field public static final FEATURE_ACCELEROMETER_STMICRO_K2DM:Ljava/lang/String; = "STMICRO_K2DM"

.field public static final FEATURE_ACCELEROMETER_STMICRO_K2HH:Ljava/lang/String; = "STMICRO_K2HH"

.field public static final FEATURE_ACCELEROMETER_STMICRO_K3DH:Ljava/lang/String; = "STMICRO_K3DH"

.field public static final FEATURE_ACCELEROMETER_STMICRO_KR3DH:Ljava/lang/String; = "STMICRO_KR3DH"

.field public static final FEATURE_ACCELEROMETER_STMICRO_KR3DM:Ljava/lang/String; = "STMICRO_KR3DM"

.field public static final FEATURE_ACCELEROMETER_STMICRO_LSM330DLC:Ljava/lang/String; = "STMICRO_LSM330DLC"

.field public static final FEATURE_GESTURE_MAX88922:Ljava/lang/String; = "MAX88922"

.field public static final FEATURE_GESTURE_TMG3992:Ljava/lang/String; = "TMG3992"

.field public static final FEATURE_GYROSCOP_BOSCH:Ljava/lang/String; = "BOSCH"

.field public static final FEATURE_GYROSCOP_INVENSENSE:Ljava/lang/String; = "INVENSENSE"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6050:Ljava/lang/String; = "INVENSENSE_MPU6050"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6051:Ljava/lang/String; = "INVENSENSE_MPU6051"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6051M:Ljava/lang/String; = "INVENSENSE_MPU6051M"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6500:Ljava/lang/String; = "INVENSENSE_MPU6500"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6515:Ljava/lang/String; = "INVENSENSE_MPU6515"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6515M:Ljava/lang/String; = "INVENSENSE_MPU6515M"

.field public static final FEATURE_GYROSCOP_MAXIM:Ljava/lang/String; = "MAXIM"

.field public static final FEATURE_GYROSCOP_STMICRO_SMARTPHONE:Ljava/lang/String; = "STMICRO_SMARTPHONE"

.field public static final FEATURE_GYROSCOP_STMICRO_TABLET:Ljava/lang/String; = "STMICRO_TABLET"

.field public static final FEATURE_LIGHT_CAPELLA_CM3663:Ljava/lang/String; = "CAPELLA_CM3663"

.field public static final FEATURE_LIGHT_CAPELLA_CM36651:Ljava/lang/String; = "CAPELLA_CM36651"

.field public static final FEATURE_LIGHT_CAPELLA_CM36691:Ljava/lang/String; = "CAPELLA_CM36691"

.field public static final FEATURE_LIGHT_SHARP_GP2AP002A00F:Ljava/lang/String; = "SHARP_GP2AP002A00F"

.field public static final FEATURE_LIGHT_SHARP_GP2AP002S00F:Ljava/lang/String; = "SHARP_GP2AP002S00F"

.field public static final FEATURE_LIGHT_SHARP_GP2AP030A00F:Ljava/lang/String; = "SHARP_GP2AP030A00F"

.field public static final FEATURE_LIGHT_TAOS_TMD2672x:Ljava/lang/String; = "TAOS_TMD2672x"

.field public static final FEATURE_LIGHT_TAOS_TMD2771x:Ljava/lang/String; = "TAOS_TMD2771x"

.field public static final FEATURE_LIGHT_TAOS_TMD2772x:Ljava/lang/String; = "TAOS_TMD2772x"

.field public static final FEATURE_MAGENTIC_ALPS_HSCDTD004:Ljava/lang/String; = "HSCDTD004"

.field public static final FEATURE_MAGENTIC_ALPS_HSCDTD004A:Ljava/lang/String; = "HSCDTD004A"

.field public static final FEATURE_MAGENTIC_ALPS_HSCDTD006A:Ljava/lang/String; = "HSCDTD006A"

.field public static final FEATURE_MAGENTIC_ALPS_HSCDTD008A:Ljava/lang/String; = "HSCDTD008A"

.field public static final FEATURE_MAGENTIC_AMOTECH:Ljava/lang/String; = "AMOTECH"

.field public static final FEATURE_MAGENTIC_ASAHI_AK09911:Ljava/lang/String; = "AK09911"

.field public static final FEATURE_MAGENTIC_ASAHI_AK09911C:Ljava/lang/String; = "AK09911C"

.field public static final FEATURE_MAGENTIC_ASAHI_AK8963:Ljava/lang/String; = "AK8963"

.field public static final FEATURE_MAGENTIC_ASAHI_AK8963C:Ljava/lang/String; = "AK8963C"

.field public static final FEATURE_MAGENTIC_ASAHI_AK8963C_MANAGER:Ljava/lang/String; = "AK8963C_MANAGER"

.field public static final FEATURE_MAGENTIC_ASAHI_AK8973:Ljava/lang/String; = "AK8973"

.field public static final FEATURE_MAGENTIC_ASAHI_AK8975:Ljava/lang/String; = "AK8975"

.field public static final FEATURE_MAGENTIC_BOSCH_BMC022:Ljava/lang/String; = "BMC022"

.field public static final FEATURE_MAGENTIC_BOSCH_BMC050:Ljava/lang/String; = "BMC050"

.field public static final FEATURE_MAGENTIC_BOSCH_BMC150:Ljava/lang/String; = "BMC150"

.field public static final FEATURE_MAGENTIC_BOSCH_BMC150_COMBINATION:Ljava/lang/String; = "BMC150_COMBINATION"

.field public static final FEATURE_MAGENTIC_BOSCH_BMC150_NEWEST:Ljava/lang/String; = "BMC150_NEWEST"

.field public static final FEATURE_MAGENTIC_BOSCH_BMC150_POWER_NOISE:Ljava/lang/String; = "BMC150_POWER_NOISE"

.field public static final FEATURE_MAGENTIC_MEMSIC_MMC3140MS:Ljava/lang/String; = "MMC3140MS"

.field public static final FEATURE_MAGENTIC_MEMSIC_MMC3280MS:Ljava/lang/String; = "MMC3280MS"

.field public static final FEATURE_MAGENTIC_STMICRO:Ljava/lang/String; = "STMICRO_K303C"

.field public static final FEATURE_MAGENTIC_YAMAHA_YAS529:Ljava/lang/String; = "YAS529"

.field public static final FEATURE_MAGENTIC_YAMAHA_YAS530:Ljava/lang/String; = "YAS530"

.field public static final FEATURE_MAGENTIC_YAMAHA_YAS530A:Ljava/lang/String; = "YAS530A"

.field public static final FEATURE_MAGENTIC_YAMAHA_YAS530C:Ljava/lang/String; = "YAS530C"

.field public static final FEATURE_MAGENTIC_YAMAHA_YAS532:Ljava/lang/String; = "YAS532"

.field public static final FEATURE_MAGENTIC_YAMAHA_YAS532B:Ljava/lang/String; = "YAS532B"

.field public static final FEATURE_PROXIMITY_CAPELLA_CM3663:Ljava/lang/String; = "CAPELLA_CM3663"

.field public static final FEATURE_PROXIMITY_CAPELLA_CM36651:Ljava/lang/String; = "CAPELLA_CM36651"

.field public static final FEATURE_PROXIMITY_CAPELLA_CM36691:Ljava/lang/String; = "CAPELLA_CM36691"

.field public static final FEATURE_PROXIMITY_SHARP_GP2AP002A00F:Ljava/lang/String; = "SHARP_GP2AP002A00F"

.field public static final FEATURE_PROXIMITY_SHARP_GP2AP002S00F:Ljava/lang/String; = "SHARP_GP2AP002S00F"

.field public static final FEATURE_PROXIMITY_SHARP_GP2AP030A00F:Ljava/lang/String; = "SHARP_GP2AP030A00F"

.field public static final FEATURE_PROXIMITY_TAOS_TMD2672x:Ljava/lang/String; = "TAOS_TMD2672x"

.field public static final FEATURE_PROXIMITY_TAOS_TMD2771x:Ljava/lang/String; = "TAOS_TMD2771x"

.field public static final FEATURE_PROXIMITY_TAOS_TMD2772x:Ljava/lang/String; = "TAOS_TMD2772x"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
