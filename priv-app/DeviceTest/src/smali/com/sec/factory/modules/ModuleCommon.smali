.class public Lcom/sec/factory/modules/ModuleCommon;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleCommon.java"


# static fields
.field public static final AUTO_ROTATE_OFF:I = 0x0

.field public static final AUTO_ROTATE_ON:I = 0x1

.field public static final FACTORY_MODE:Ljava/lang/String; = "OFF"

.field private static final FIRST_BOOT_FLAG:Ljava/lang/String; = "com.sec.android.app.factorymode.first_boot_flag"

.field private static final FTCLIENT_FLAG:Ljava/lang/String; = "com.sec.android.app.factorymode.ftclient"

.field private static final FTCLIENT_KEY:Ljava/lang/String; = "com.sec.android.app.factorymode.ftclient.key"

.field public static final HDCP_RETURN_VALUE_NG:Ljava/lang/String; = "NG"

.field public static final HDCP_RETURN_VALUE_NG_FIELD:Ljava/lang/String; = "NG_FIELD"

.field public static final HDCP_RETURN_VALUE_NG_KEY:Ljava/lang/String; = "NG_KEY"

.field public static final HDCP_RETURN_VALUE_OK:Ljava/lang/String; = "OK"

.field public static final HDMI_PATTERN_OFF:Ljava/lang/String; = "0"

.field public static final HDMI_PATTERN_ON:Ljava/lang/String; = "4"

.field public static final HIDDEN_MENU_BLOCK_OFF:Ljava/lang/String; = "OFF"

.field public static final HIDDEN_MENU_BLOCK_ON:Ljava/lang/String; = "ON"

.field public static final ID_HDCP_CHECK_1_3_ENCRYPTED_DATA_INSTALL:I = 0x3

.field public static final ID_HDCP_CHECK_1_3_INSTALL:I = 0x2

.field public static final ID_HDCP_CHECK_2_0_INSTALL:I = 0x1

.field public static final ID_HDCP_CHECK_ENCRYPTED_WIDEVINE_KEY_INSTALL:I = 0x4

.field public static final KEYSTR_BLOCK_OFF:Ljava/lang/String; = "OFF"

.field public static final KEYSTR_BLOCK_ON:Ljava/lang/String; = "ON"

.field private static final KEY_FIRST_BOOT_FLAG:Ljava/lang/String; = "factorymode.shortcut.skipped"

.field public static final KEY_LOCK:I = 0x1

.field private static final KEY_MEDIA_SCANNING_COUNTER:Ljava/lang/String; = "sys.factory.mediaScanningCount"

.field private static final KEY_RUNNING_FTCLIENT:Ljava/lang/String; = "sys.factory.runningFtClient"

.field private static final KEY_SHORTCUT_CREATED_FLAG:Ljava/lang/String; = "isShortCustCreated"

.field public static final KEY_UNLOCK:I = 0x0

.field public static final LTE:Ljava/lang/String; = "LTEMODEM"

.field public static final MODEM:Ljava/lang/String; = "MODEM"

.field public static final PDA:Ljava/lang/String; = "PDA"

.field private static final SHORTCUTS_INTENT:Ljava/lang/String; = "com.sec.android.app.factorymode.shortcuts"

.field private static final SHORTCUT_PREFS:Ljava/lang/String; = "shortcutSharedPreferences"

.field public static final SHORTCUT_TYPE_BAROMETER_WATERPROOF:I = 0x1

.field public static final SHORTCUT_TYPE_MIC2_ECHO:I = 0x2

.field public static final SHORTCUT_TYPE_SEMI_FUNCTION:I = 0x0

.field public static final SHORTCUT_TYPE_WIFI_STRESS_TEST:I = 0x3

.field private static final TEST_READY_FLAG:Ljava/lang/String; = "com.sec.android.app.factorymode.15_test_ready_flag"

.field public static final USER_MODE:Ljava/lang/String; = "ON"

.field private static mInstance:Lcom/sec/factory/modules/ModuleCommon;

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field final CONTENT_URI_NO_NOTIFICATION:Landroid/net/Uri;

.field private final HDCP_KEBOX_SIZE:J

.field private final WIDEVINE_KEBOX_SIZE:J

.field private isQualcomm:Z

.field private isSLSI:Z

.field private isSPRD:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleCommon;->mInstance:Lcom/sec/factory/modules/ModuleCommon;

    .line 55
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/factory/modules/ModuleCommon;->mSalesCode:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    const-string v0, "ModuleCommon"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    const-string v0, "S.LSI"

    const-string v1, "CHIPSET_MANUFACTURE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommon;->isSLSI:Z

    .line 36
    const-string v0, "qualcomm"

    const-string v1, "CHIPSET_MANUFACTURE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommon;->isQualcomm:Z

    .line 37
    const-string v0, "Spreadtrum"

    const-string v1, "CHIPSET_MANUFACTURE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleCommon;->isSPRD:Z

    .line 1064
    const-wide/16 v0, 0x90

    iput-wide v0, p0, Lcom/sec/factory/modules/ModuleCommon;->WIDEVINE_KEBOX_SIZE:J

    .line 1065
    const-wide/16 v0, 0x280

    iput-wide v0, p0, Lcom/sec/factory/modules/ModuleCommon;->HDCP_KEBOX_SIZE:J

    .line 1147
    const-string v0, "content://com.sec.android.app.launcher.settings/favorites?notify=false"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CONTENT_URI_NO_NOTIFICATION:Landroid/net/Uri;

    .line 60
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleCommon"

    const-string v2, "Create ModuleCommon"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method private checkTopActivity()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 82
    const/4 v0, 0x0

    .line 83
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/factory/modules/ModuleCommon;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "activityManager":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 84
    .restart local v0    # "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 85
    .local v1, "runningtask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "topActivity":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "topActivity: "

    invoke-static {v3, v6, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v3, "com.sec.factory"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "checkTopActivity(): "

    const-string v6, "sendBroadcast"

    invoke-static {v3, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 92
    :goto_0
    return v3

    :cond_0
    move v3, v5

    goto :goto_0
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    sget-object v0, Lcom/sec/factory/modules/ModuleCommon;->mInstance:Lcom/sec/factory/modules/ModuleCommon;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/factory/modules/ModuleCommon;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommon;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleCommon;->mInstance:Lcom/sec/factory/modules/ModuleCommon;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModuleCommon;->mInstance:Lcom/sec/factory/modules/ModuleCommon;

    return-object v0
.end method


# virtual methods
.method public CreateFactoyShortCut()V
    .locals 4

    .prologue
    .line 901
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "CreateFactoyShortCut"

    const-string v3, "send intent"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 903
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.factorymode.shortcuts"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 904
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 905
    const-string v1, "SUBACTION"

    const-string v2, "CREATE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 906
    sget-object v1, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 907
    return-void
.end method

.method DoShellCmd(Ljava/lang/String;)I
    .locals 10
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 1218
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "DoShellCmd"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cmd = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    const/4 v2, 0x0

    .line 1220
    .local v2, "p":Ljava/lang/Process;
    const/4 v6, 0x3

    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "/system/bin/sh"

    aput-object v7, v3, v6

    const-string v6, "-c"

    aput-object v6, v3, v4

    const/4 v6, 0x2

    aput-object p1, v3, v6

    .line 1223
    .local v3, "shell_command":[Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "DoShellCmd"

    const-string v8, "exec command"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 1225
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    .line 1226
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "DoShellCmd"

    const-string v8, "exec done"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1238
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "DoShellCmd"

    const-string v7, "DoShellCmd done"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    :goto_0
    return v4

    .line 1227
    :catch_0
    move-exception v1

    .line 1228
    .local v1, "exception":Ljava/io/IOException;
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "DoShellCmd"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DoShellCmd - IOException"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 1229
    goto :goto_0

    .line 1230
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1231
    .local v1, "exception":Ljava/lang/SecurityException;
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "DoShellCmd"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SecurityException"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 1232
    goto :goto_0

    .line 1233
    .end local v1    # "exception":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 1234
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "DoShellCmd"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "InterruptedException"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 1235
    goto :goto_0
.end method

.method public FactoryShortcutExists()Z
    .locals 11

    .prologue
    .line 1151
    sget-object v1, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1152
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 1154
    .local v7, "returnvalue":Z
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->isVoiceCapable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CTC"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v8, v7

    .line 1184
    .end local v7    # "returnvalue":Z
    .local v8, "returnvalue":I
    :goto_0
    return v8

    .line 1161
    .end local v8    # "returnvalue":I
    .restart local v7    # "returnvalue":Z
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CONTENT_URI_NO_NOTIFICATION:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1163
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 1164
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1165
    const-string v1, "title"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1166
    .local v10, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "FactoryShortcutExists"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "title : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    if-eqz v10, :cond_1

    .line 1169
    const-string v1, "Factory Mode"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1170
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "FactoryShortcutExists"

    const-string v3, "Factory Mode Exist"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    const/4 v7, 0x1

    .line 1177
    .end local v10    # "title":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1183
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "FactoryShortcutExists"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "returnvalue :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v8, v7

    .line 1184
    .restart local v8    # "returnvalue":I
    goto :goto_0

    .line 1179
    .end local v8    # "returnvalue":I
    :catch_0
    move-exception v9

    .line 1180
    .local v9, "se":Ljava/lang/SecurityException;
    invoke-static {v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public RemoveFactoyShortCut()V
    .locals 4

    .prologue
    .line 910
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "RemoveFactoyShortCut"

    const-string v3, "send intent"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 912
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.factorymode.shortcuts"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 913
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 914
    const-string v1, "SUBACTION"

    const-string v2, "REMOVE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 915
    sget-object v1, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 916
    return-void
.end method

.method public Request15FunctionTest(I)Z
    .locals 5
    .param p1, "test"    # I

    .prologue
    .line 142
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Request15FunctionTest: "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 145
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory.app.factorytest.TestMode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string v1, "testmode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 147
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommon;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    const/4 v1, 0x1

    return v1
.end method

.method public connectedJIG()Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1019
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "connectedJIG"

    const-string v7, "..."

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    const-string v5, "ANYWAY_JIG_CABLE_TYPE"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1021
    .local v1, "cable_type":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1022
    .local v3, "value":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1023
    .local v0, "JIG_ON":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1024
    .local v2, "result":Z
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "connectedJIG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cable_type = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    const-string v5, "ANYWAY_JIG"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1027
    const-string v5, "ANYWAY_JIG"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1047
    :cond_0
    :goto_0
    return v4

    .line 1031
    :cond_1
    const-string v0, "1C"

    .line 1032
    const-string v4, "ANYWAY_JIG"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1033
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "connectedJIG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "value = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", JIG_ON = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 1035
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    :cond_2
    :goto_2
    move v4, v2

    .line 1047
    goto :goto_0

    .line 1034
    :cond_3
    const-string v3, " "

    goto :goto_1

    .line 1036
    :cond_4
    const-string v5, "ANYWAY_JIG_30PIN"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1037
    const-string v5, "ANYWAY_JIG_30PIN"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1041
    const-string v0, "1"

    .line 1042
    const-string v4, "ANYWAY_JIG_30PIN"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1043
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "connectedJIG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "value = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", JIG_ON = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_2
.end method

.method public disable15Test()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 873
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "15 Test Ready flag"

    const-string v4, "blocking"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.15_test_ready_flag"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 876
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 877
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "com.sec.android.app.factorymode.15_test_ready_flag"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 878
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 879
    const/4 v2, 0x1

    return v2
.end method

.method public disableFtClient()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 983
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "disableFtClient"

    const-string v4, "..."

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.ftclient"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 986
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 987
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "com.sec.android.app.factorymode.ftclient.key"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 988
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 989
    const/4 v2, 0x1

    return v2
.end method

.method public enable15Test()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 863
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "15 Test Ready flag"

    const-string v4, "set"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.15_test_ready_flag"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 866
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 867
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "com.sec.android.app.factorymode.15_test_ready_flag"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 868
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 869
    return v5
.end method

.method public enableFtClient()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 993
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "enableFtClient"

    const-string v4, "..."

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.ftclient"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 996
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 997
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "com.sec.android.app.factorymode.ftclient.key"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 998
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 999
    return v5
.end method

.method public fsync()I
    .locals 2

    .prologue
    .line 1243
    const/4 v0, -0x1

    .line 1245
    .local v0, "nResult":I
    const-string v1, "sync"

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleCommon;->DoShellCmd(Ljava/lang/String;)I

    move-result v0

    .line 1246
    const/16 v1, 0x1f4

    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleCommon;->sleepThread(I)V

    .line 1248
    return v0
.end method

.method public getAPNameReal()Ljava/lang/String;
    .locals 5

    .prologue
    .line 769
    const-string v1, "MAIN_CHIP_NAME_AP_REAL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 770
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getAPNameReal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    return-object v0
.end method

.method public getAutoRotateState()I
    .locals 5

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 217
    .local v0, "state":I
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getAutoRotate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    return v0
.end method

.method public getBinaryType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 729
    const-string v1, "ro.build.type"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 730
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBinaryType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    return-object v0
.end method

.method public getBootVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 576
    const-string v1, "BOOT_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 577
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBootVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    return-object v0
.end method

.method public getBootloaderVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 677
    const-string v1, "BOOTLOADER_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 678
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBootVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    return-object v0
.end method

.method public getCPName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 763
    const-string v1, "MAIN_CHIP_NAME_CP"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 764
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getCPName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    return-object v0
.end method

.method public getCPState()Ljava/lang/String;
    .locals 5

    .prologue
    .line 757
    const-string v1, "DUALMODE_STATE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 758
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getCPState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    return-object v0
.end method

.method public getCSCVer()Ljava/lang/String;
    .locals 6

    .prologue
    .line 636
    const-string v2, "ro.product.model"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 637
    .local v0, "productModel":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 638
    const-string v2, "CSC_VERSION"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 640
    .local v1, "ver":Ljava/lang/String;
    const-string v2, "m3"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 641
    const/4 v2, 0x5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 644
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getCSCVer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "version="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    return-object v1
.end method

.method public getCalDate()Ljava/lang/String;
    .locals 5

    .prologue
    .line 695
    const-string v1, "EFS_CALDATE_PATH"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 696
    .local v0, "date":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getCalDate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCalDate="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    if-nez v0, :cond_0

    .line 699
    const-string v0, "none"

    .line 702
    :cond_0
    return-object v0
.end method

.method public getContentsVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 553
    const-string v1, "CONTENTS_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 554
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getContentsVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    return-object v0
.end method

.method public getFtClientEnableState()Z
    .locals 6

    .prologue
    .line 1003
    const/4 v1, 0x1

    .line 1004
    .local v1, "result":Z
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.ftclient"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1007
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 1008
    const-string v2, "com.sec.android.app.factorymode.ftclient.key"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1011
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getFtClientEnableState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    return v1
.end method

.method public getFtaHwVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 775
    const-string v0, "FTA_HW_VER"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFtaSwVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 779
    const-string v0, "FTA_SW_VER"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHWRevision()Ljava/lang/String;
    .locals 5

    .prologue
    .line 446
    const-string v1, "HW_REVISION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 448
    .local v0, "version":Ljava/lang/String;
    const-string v1, "Unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 449
    const-string v1, "ro.revision"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 453
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getHWRevision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rev="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    return-object v0
.end method

.method public getHWVer()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 464
    const/4 v8, 0x0

    .line 466
    .local v8, "ver":Ljava/lang/String;
    const-string v0, "HW_VER_EFS"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 467
    new-instance v7, Ljava/io/File;

    const-string v0, "EFS_HW_PATH"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 468
    .local v7, "mFactoryMode":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    const-string v0, "MODEL_HARDWARE_REVISION"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommon;->setHWVer(Ljava/lang/String;)Z

    .line 470
    const-string v0, "EFS_HW_PATH"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v6}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 472
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getHWVer"

    const-string v2, "EFS_HW_PATH is created..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :goto_0
    const-string v0, "EFS_HW_PATH"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 482
    .end local v7    # "mFactoryMode":Ljava/io/File;
    :goto_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getHWver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "version="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    return-object v8

    .line 474
    .restart local v7    # "mFactoryMode":Ljava/io/File;
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getHWVer"

    const-string v2, "EFS_HW_PATHis already existed..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 479
    .end local v7    # "mFactoryMode":Ljava/io/File;
    :cond_1
    const-string v0, "HW_VERSION"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_1
.end method

.method public getIFWIVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 665
    const-string v1, "SYSFS_IFWI_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 666
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getIFWIVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    return-object v0
.end method

.method public getKeyLockState()I
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "SHOULD_SHUT_DOWN"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getLTEHiddenVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 547
    const-string v1, "LTE_HIDDEN_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 548
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getLTEHiddenVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    return-object v0
.end method

.method public getLTEVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 541
    const-string v1, "LTE_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getLTEVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-object v0
.end method

.method public getMainChipName()Ljava/lang/String;
    .locals 10

    .prologue
    .line 499
    const-string v0, ""

    .line 500
    .local v0, "ap_name":Ljava/lang/String;
    const-string v2, ""

    .line 502
    .local v2, "cp_name":Ljava/lang/String;
    const-string v6, "STANDARD_JB_SYSFS"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 503
    const-string v6, "MAIN_CHIP_NAME_AP_REAL"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    :goto_0
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 509
    const-string v6, "MAIN_CHIP_NAME_CP"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 511
    :cond_0
    const-string v6, "SUPPORT_DUAL_STANBY"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "SUPPORT_2ND_CP"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 512
    :cond_1
    const-string v6, "MAIN_CHIP_NAME_CP2"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 513
    .local v5, "ver2":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 516
    .end local v5    # "ver2":Ljava/lang/String;
    :cond_2
    const-string v6, "FACTORY_TEST_COMMAND"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Version;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 517
    .local v1, "cmd_ed":I
    const/16 v6, 0x19

    if-gt v1, v6, :cond_4

    .line 518
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 519
    move-object v3, v0

    .line 520
    .local v3, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getMainChipName"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "name="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 536
    .end local v3    # "name":Ljava/lang/String;
    .local v4, "name":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 505
    .end local v1    # "cmd_ed":I
    .end local v4    # "name":Ljava/lang/String;
    :cond_3
    const-string v6, "MAIN_CHIP_NAME_AP"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 524
    .restart local v1    # "cmd_ed":I
    :cond_4
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 525
    move-object v3, v0

    .line 526
    .restart local v3    # "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getMainChipName"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "name="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 527
    .end local v3    # "name":Ljava/lang/String;
    .restart local v4    # "name":Ljava/lang/String;
    goto :goto_1

    .line 529
    .end local v4    # "name":Ljava/lang/String;
    :cond_5
    const-string v6, "exynos3470"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "SS222"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 530
    const-string v0, "SHANNON222AP"

    .line 531
    const-string v2, "SHANNON222AP"

    .line 534
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 535
    .restart local v3    # "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getMainChipName"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "name="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 536
    .end local v3    # "name":Ljava/lang/String;
    .restart local v4    # "name":Ljava/lang/String;
    goto :goto_1
.end method

.method public getMainSWVer()Ljava/lang/String;
    .locals 7

    .prologue
    .line 650
    const-string v3, "ril.sw_ver"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 651
    .local v2, "ver":Ljava/lang/String;
    const-string v3, "ro.csc.sales_code"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 654
    .local v1, "mSalesCode":Ljava/lang/String;
    const-string v3, "CTC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 655
    const-string v3, "ril.sw_ver"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 656
    .local v0, "ctc_ver":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMainSWVer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CTCversion="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    .end local v0    # "ctc_ver":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 660
    :cond_0
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMainSWVer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "version="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 661
    goto :goto_0
.end method

.method public getMediaScanningCount()I
    .locals 7

    .prologue
    .line 828
    const-string v3, "sys.factory.mediaScanningCount"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 829
    .local v2, "result":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMediaScanningCount"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const/4 v0, 0x0

    .line 833
    .local v0, "ivalue":I
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 837
    :goto_0
    return v0

    .line 834
    :catch_0
    move-exception v1

    .line 835
    .local v1, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 487
    const-string v1, "PRODUCT_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 488
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getModelName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    if-eqz v0, :cond_0

    const-string v1, "SAMSUNG-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 492
    const-string v1, "SAMSUNG-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 495
    :cond_0
    return-object v0
.end method

.method public getPDAVer()Ljava/lang/String;
    .locals 8

    .prologue
    .line 560
    const-string v4, "PDA_VERSION"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 561
    .local v1, "PDAver":Ljava/lang/String;
    const-string v4, "CONTENTS_VERSION"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 563
    .local v0, "Hiddenver":Ljava/lang/String;
    const-string v4, "factory"

    const-string v5, "BINARY_TYPE"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/sec/factory/modules/ModuleCommon;->mSalesCode:Ljava/lang/String;

    const-string v5, "VZW"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 565
    move-object v2, v0

    .line 570
    .local v2, "tempver":Ljava/lang/String;
    :goto_0
    move-object v3, v2

    .line 571
    .local v3, "ver":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getPDAVer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    return-object v3

    .line 567
    .end local v2    # "tempver":Ljava/lang/String;
    .end local v3    # "ver":Ljava/lang/String;
    :cond_0
    move-object v2, v1

    .restart local v2    # "tempver":Ljava/lang/String;
    goto :goto_0
.end method

.method public getPhone2Ver()Ljava/lang/String;
    .locals 9

    .prologue
    .line 605
    const-string v5, "ro.product.model"

    const-string v6, "Unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 606
    .local v2, "productModel":Ljava/lang/String;
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 607
    const-string v5, "ril.sw_ver"

    const-string v6, "Unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 609
    .local v3, "ver":Ljava/lang/String;
    const-string v5, "m3"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 610
    const/4 v5, 0x5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 613
    :cond_0
    const-string v5, "ro.csc.sales_code"

    const-string v6, "NONE"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 616
    .local v1, "mSalesCode2":Ljava/lang/String;
    const-string v5, "SUPPORT_DUAL_STANBY_ONE_CP"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 617
    const-string v4, "NONE"

    .line 632
    :goto_0
    return-object v4

    .line 621
    :cond_1
    const-string v5, "CTC"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 622
    const-string v5, "gsm.version.baseband"

    const-string v6, "Unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 623
    .local v0, "ctc_ver":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getPhone2Ver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CTCversion="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    .line 624
    goto :goto_0

    .line 626
    .end local v0    # "ctc_ver":Ljava/lang/String;
    :cond_2
    const-string v5, "SUPPORT_DUAL_STANBY"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "SUPPORT_2ND_CP"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 627
    :cond_3
    const-string v5, "ril.sw_ver2"

    const-string v6, "Unknown"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 628
    .local v4, "ver2":Ljava/lang/String;
    goto :goto_0

    .line 631
    .end local v4    # "ver2":Ljava/lang/String;
    :cond_4
    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getPhone2Ver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "version="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 632
    goto :goto_0
.end method

.method public getPhoneVer()Ljava/lang/String;
    .locals 8

    .prologue
    .line 583
    const-string v4, "ro.product.model"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 584
    .local v2, "productModel":Ljava/lang/String;
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 585
    const-string v4, "ril.sw_ver"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 587
    .local v3, "ver":Ljava/lang/String;
    const-string v4, "m3"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 588
    const/4 v4, 0x5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 591
    :cond_0
    const-string v4, "ro.csc.sales_code"

    const-string v5, "NONE"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 594
    .local v1, "mSalesCode":Ljava/lang/String;
    const-string v4, "CTC"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 595
    const-string v4, "ril.sw_ver"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 596
    .local v0, "ctc_ver":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getPhoneVer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CTCversion="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    .end local v0    # "ctc_ver":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 600
    :cond_1
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getPhoneVer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 601
    goto :goto_0
.end method

.method public getSWHiddenVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 458
    const-string v1, "SW_HIDDEN_VERSION"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 459
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getSWHiddenVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    return-object v0
.end method

.method public getStandardVer()Ljava/lang/String;
    .locals 5

    .prologue
    .line 671
    const-string v1, "FACTORY_TEST_COMMAND"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 672
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getStandardVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    return-object v0
.end method

.method public getTimeToString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1281
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1282
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v6, "00"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v7}, Ljava/text/DecimalFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DecimalFormatSymbols;

    move-result-object v7

    invoke-direct {v2, v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 1284
    .local v2, "df":Ljava/text/DecimalFormat;
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 1285
    .local v5, "month":Ljava/lang/String;
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 1286
    .local v1, "day":Ljava/lang/String;
    const/16 v6, 0xb

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 1287
    .local v3, "hour":Ljava/lang/String;
    const/16 v6, 0xc

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 1289
    .local v4, "min":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public getUartPath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 412
    const-string v1, "UART_SELECT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getUartPath"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    return-object v0
.end method

.method public getUsbPath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 401
    const-string v1, "USB_SELECT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getUsbPath"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    return-object v0
.end method

.method public getUserDataVer()Ljava/lang/String;
    .locals 6

    .prologue
    .line 683
    const-string v0, "/data/version.txt"

    .line 684
    .local v0, "path":Ljava/lang/String;
    const-string v1, "N"

    .line 685
    .local v1, "ver":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 687
    if-nez v1, :cond_0

    .line 688
    const-string v1, "N"

    .line 690
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getUserDataVer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "version="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    return-object v1
.end method

.method public go15mode()V
    .locals 7

    .prologue
    .line 122
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "go15mode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin Type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "BINARY_TYPE"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "FACTORY_TEST_UNDER_USER_BINARY"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 125
    :cond_0
    const/16 v0, 0xf

    .line 126
    .local v0, "FACTORY_TEST_START":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "go15mode"

    const-string v5, "Launch 15 test Activity"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 129
    .local v1, "closeDialog":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleCommon;->sendBroadcast(Landroid/content/Intent;)V

    .line 131
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 132
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "com.sec.factory"

    const-string v4, "com.sec.factory.app.factorytest.FactoryTestMain"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const/high16 v3, 0x34000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 136
    const-string v3, "atfunctest"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 137
    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleCommon;->startActivity(Landroid/content/Intent;)V

    .line 139
    .end local v0    # "FACTORY_TEST_START":I
    .end local v1    # "closeDialog":Landroid/content/Intent;
    .end local v2    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public goIdle()V
    .locals 5

    .prologue
    .line 97
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "goIdle"

    const-string v4, "Launch Home Activity"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommon;->checkTopActivity()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 101
    .local v1, "stopFactoryIntent":Landroid/content/Intent;
    const-string v2, "com.sec.samsung.STOP_FACTORY_TEST"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v1}, Lcom/sec/factory/modules/ModuleCommon;->sendBroadcast(Landroid/content/Intent;)V

    .line 105
    .end local v1    # "stopFactoryIntent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 106
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 109
    const/high16 v2, 0x14000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommon;->startActivity(Landroid/content/Intent;)V

    .line 111
    return-void
.end method

.method public goLcdtest()V
    .locals 4

    .prologue
    .line 114
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "goLcdtest"

    const-string v3, "Launch Lcd test Activity"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 116
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.hwmoduletest"

    const-string v2, "com.sec.android.app.hwmoduletest.HwModuleTest"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 118
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommon;->startActivity(Landroid/content/Intent;)V

    .line 119
    return-void
.end method

.method public goSemiFunctionTest()V
    .locals 5

    .prologue
    .line 153
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "goSemiFunctionTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bin Type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "BINARY_TYPE"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FACTORY_TEST_UNDER_USER_BINARY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "goSemiFunctionTest"

    const-string v3, "Launch SEMI Activity"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 160
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.factorytest.SEMIFunctionTestLauncher"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommon;->startActivity(Landroid/content/Intent;)V

    .line 166
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public hdcpCheck(I)Ljava/lang/String;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 1078
    const-wide/16 v0, -0x1

    .line 1080
    .local v0, "length":J
    packed-switch p1, :pswitch_data_0

    .line 1105
    const-string v2, "NG"

    :goto_0
    return-object v2

    .line 1082
    :pswitch_0
    const-string v2, "HDCP_CHECK_2_0"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1085
    :pswitch_1
    const-string v2, "HDCP_CHECK_1_3_KEYBOX"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->getFileLength(Ljava/lang/String;)J

    move-result-wide v0

    .line 1086
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "checkHDCPKey"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "length : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    const-wide/16 v2, 0x280

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1089
    const-string v2, "OK"

    goto :goto_0

    .line 1091
    :cond_0
    const-string v2, "NG"

    goto :goto_0

    .line 1095
    :pswitch_2
    const-string v2, "HDCP_CHECK_WIDEVINE_KEYBOX"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->getFileLength(Ljava/lang/String;)J

    move-result-wide v0

    .line 1096
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "checkWidevineKey"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "length : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    const-wide/16 v2, 0x90

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 1099
    const-string v2, "OK"

    goto :goto_0

    .line 1101
    :cond_1
    const-string v2, "NG"

    goto :goto_0

    .line 1080
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public hdcpReadSN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1110
    const-string v0, "HDCP_SN"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public is15TestEnable()Z
    .locals 6

    .prologue
    .line 883
    const/4 v1, 0x1

    .line 884
    .local v1, "result":Z
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.15_test_ready_flag"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 887
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 888
    const-string v2, "com.sec.android.app.factorymode.15_test_ready_flag"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 891
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "is15TestEnable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TEST_READY_FLAG : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    return v1
.end method

.method public isConnectionModeNone()Z
    .locals 6

    .prologue
    .line 720
    const/4 v1, 0x0

    .line 721
    .local v1, "ret":Z
    const/4 v0, 0x0

    .line 722
    .local v0, "mConnectionMode":Ljava/lang/String;
    const-string v2, "MODEL_COMMUNICATION_MODE"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 723
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "isConnectionModeNone"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mConnectionMode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    const-string v2, "none"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 725
    :goto_0
    return v1

    .line 724
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFirstBoot()Z
    .locals 6

    .prologue
    .line 799
    const/4 v1, 0x1

    .line 800
    .local v1, "result":Z
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.first_boot_flag"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 803
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 804
    const-string v2, "factorymode.shortcut.skipped"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 807
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "isFirstBoot"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    return v1
.end method

.method public isKeyStringBlock()Z
    .locals 2

    .prologue
    .line 1054
    const-string v0, "ON"

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->readKeyStringBlock()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1055
    const/4 v0, 0x1

    .line 1057
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRunningFtClient()Z
    .locals 5

    .prologue
    .line 852
    const-string v1, "sys.factory.runningFtClient"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 853
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isRunningFtClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RUNNING_FTCLIENT : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isVoiceCapable()Z
    .locals 5

    .prologue
    .line 735
    const-string v1, "true"

    const-string v2, "SUPPORT_VOICE_CALL_FORCIBLY"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 736
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isVoiceCapable"

    const-string v3, "SUPPORT_VOICE_CALL_FORCIBLY true return true"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const/4 v0, 0x1

    .line 753
    :goto_0
    return v0

    .line 740
    :cond_0
    const-string v1, "true"

    const-string v2, "LIVE_DEMO_MODEL"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 741
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isVoiceCapable"

    const-string v3, "LIVE_DEMO_MODEL true return false"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const/4 v0, 0x0

    goto :goto_0

    .line 745
    :cond_1
    const/4 v0, 0x1

    .line 746
    .local v0, "r":Z
    sget-object v1, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 747
    sget-object v1, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1120044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 749
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isVoiceCapable"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "config_voice_capable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "isVoiceCapable"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadShortcutPreferences()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1135
    const/4 v1, 0x0

    .line 1136
    .local v1, "result":Z
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "shortcutSharedPreferences"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1139
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 1140
    const-string v2, "isShortCustCreated"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1143
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "loadShortcutPreferences"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    return v1
.end method

.method public notifyFactoryMode2Kernel()V
    .locals 8

    .prologue
    .line 1202
    const-string v0, "1"

    .line 1203
    .local v0, "FTM_FACTORY":Ljava/lang/String;
    const-string v1, "0"

    .line 1207
    .local v1, "FTM_USER":Ljava/lang/String;
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1208
    const-string v2, "1"

    .line 1213
    .local v2, "mode":Ljava/lang/String;
    :goto_0
    const-string v4, "SWITCH_KERNEL_FTM"

    invoke-static {v4, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1214
    .local v3, "result":Z
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "notifyFactoryMode2Kernel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", result="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    return-void

    .line 1210
    .end local v2    # "mode":Ljava/lang/String;
    .end local v3    # "result":Z
    :cond_0
    const-string v2, "0"

    .restart local v2    # "mode":Ljava/lang/String;
    goto :goto_0
.end method

.method public readBatteryType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 269
    const-string v1, "BATTERY_TYPE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-object v0
.end method

.method public readBatteryVF()Ljava/lang/String;
    .locals 6

    .prologue
    .line 275
    const-string v2, "BATTERY_ADC_NOTSUPPORT"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    .local v1, "health":Ljava/lang/String;
    const-string v2, "BATTERY_TEMP_ADC"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "adc":Ljava/lang/String;
    const-string v2, "SUPPORT_BATTERY_ADC"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readBatteryVF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    .end local v1    # "health":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 282
    .restart local v1    # "health":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "readBatteryVF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 283
    goto :goto_0
.end method

.method public readCameraFrontFWver()Ljava/lang/String;
    .locals 5

    .prologue
    .line 306
    const-string v1, "CAMERA_FRONT_FW_VER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCameraFrontFWver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    return-object v0
.end method

.method public readCameraFrontType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 294
    const-string v1, "CAMERA_FRONT_TYPE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCameraFrontType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-object v0
.end method

.method public readCameraOisFWver()Ljava/lang/String;
    .locals 5

    .prologue
    .line 312
    const-string v1, "CAMERA_REAR_OIS_FW_VER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCameraOisFWver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    return-object v0
.end method

.method public readCameraRearFWver()Ljava/lang/String;
    .locals 5

    .prologue
    .line 300
    const-string v1, "CAMERA_REAR_FW_VER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCameraRearFWver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    return-object v0
.end method

.method public readCameraRearType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 288
    const-string v1, "CAMERA_REAR_TYPE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCameraRearType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    return-object v0
.end method

.method public readCpuOnline()Ljava/lang/String;
    .locals 5

    .prologue
    .line 239
    const-string v1, "CPU_ONLINE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCpuOnline"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-object v0
.end method

.method public readCpuStatus_BorL()Ljava/lang/String;
    .locals 5

    .prologue
    .line 245
    const-string v1, "CPU_BIG_LITTLE_STATUS"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->readAllLine(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "status":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readCpuStatus_LorB"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    return-object v0
.end method

.method public readFactoryMode()Ljava/lang/String;
    .locals 5

    .prologue
    .line 324
    const-string v1, "FACTORY_MODE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "mode":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readFactoryMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    return-object v0
.end method

.method public readHDMITestResult()Ljava/lang/String;
    .locals 5

    .prologue
    .line 318
    const-string v1, "HDMI_TEST_RESULT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readHDMICheck"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    return-object v0
.end method

.method public readHiddenMenuBlock()Ljava/lang/String;
    .locals 5

    .prologue
    .line 371
    const-string v1, "HIDDEN_MENU_BLOCK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "block":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readHiddenMenuBlock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "block="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    return-object v0
.end method

.method public readIMEI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1117
    sget-object v0, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readKeyStringBlock()Ljava/lang/String;
    .locals 5

    .prologue
    .line 353
    const-string v1, "KEYSTRING_BLOCK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 354
    .local v0, "block":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readKeyStringBlock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "block="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    return-object v0
.end method

.method public readLcdIdType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 263
    const-string v1, "LCD_ID"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "id":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readLcdType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-object v0
.end method

.method public readLcdType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 257
    const-string v1, "LCD_TYPE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readLcdType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    return-object v0
.end method

.method public readPrePay()Ljava/lang/String;
    .locals 5

    .prologue
    .line 342
    const-string v1, "PRE_PAY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "mode":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readPrePay"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    return-object v0
.end method

.method public readProxiADC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 251
    const-string v1, "PROXI_SENSOR_ADC"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readProxiADC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    return-object v0
.end method

.method public readProxiOffset()Ljava/lang/String;
    .locals 7

    .prologue
    .line 222
    const/4 v1, 0x0

    .line 223
    .local v1, "proxiOffestValue":[Ljava/lang/String;
    const-string v3, "PROXI_SENSOR_OFFSET"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "offsetValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 226
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 233
    const/4 v3, 0x0

    aget-object v2, v1, v3

    .line 234
    .local v2, "type":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readProxiOffset"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    .end local v2    # "type":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 229
    :cond_0
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readProxiOffset"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public readRTC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x7

    .line 177
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMddHHmmss"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 178
    .local v1, "formatter":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 179
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v2, v3, -0x2

    .line 181
    .local v2, "week":I
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 179
    .end local v2    # "week":I
    :cond_0
    const/4 v2, 0x6

    goto :goto_0
.end method

.method public readRamSizeInfo()Ljava/lang/String;
    .locals 5

    .prologue
    .line 424
    const-string v1, "DEVICE_RAM_SIZE_FILE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 425
    .local v0, "mRamSize":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readRamSizeInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mRamSize: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    return-object v0
.end method

.method public readmBaroDelta()Ljava/lang/String;
    .locals 5

    .prologue
    .line 389
    const-string v1, "BAROMETE_DELTA"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "delta":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readmBaroDelta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delta="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    return-object v0
.end method

.method public requestCreateShortCut(I)V
    .locals 8
    .param p1, "type"    # I

    .prologue
    const/4 v7, -0x1

    .line 924
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestShortCut() : type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    const-string v0, "Unknown"

    .line 926
    .local v0, "appName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 928
    .local v1, "iconResource":Landroid/os/Parcelable;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 929
    .local v3, "shortcutIntent":Landroid/content/Intent;
    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 930
    const/high16 v4, 0x10200000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 932
    packed-switch p1, :pswitch_data_0

    .line 962
    iget-object v4, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "requestShortCut() : Wrong request"

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    :goto_0
    return-void

    .line 934
    :pswitch_0
    const-string v4, "com.sec.factory"

    const-string v5, "com.sec.factory.app.factorytest.SEMIFunctionTestLauncher"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 936
    const-string v4, "testname"

    const-string v5, "semifunctiontest"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 937
    const-string v0, "SEMI"

    .line 938
    sget-object v4, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const v5, 0x7f020002

    invoke-static {v4, v5}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 966
    :goto_1
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 967
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 968
    const-string v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 969
    const-string v4, "duplicate"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 970
    const-string v4, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 972
    sget-object v4, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 941
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_1
    const-string v4, "com.sec.factory"

    const-string v5, "com.sec.factory.app.ui.UIBarometerWaterProofTest"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 943
    const-string v4, "testname"

    const-string v5, "Waterproof"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 944
    const-string v0, "Waterproof"

    .line 945
    sget-object v4, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const v5, 0x7f020003

    invoke-static {v4, v5}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 946
    goto :goto_1

    .line 948
    :pswitch_2
    const-string v4, "com.sec.factory"

    const-string v5, "com.sec.factory.app.ui.UIEchoTest"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 949
    const-string v4, "requestCode"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 950
    const-string v0, "MIC2 Echo"

    .line 951
    sget-object v4, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const v5, 0x7f020001

    invoke-static {v4, v5}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 953
    goto :goto_1

    .line 955
    :pswitch_3
    const-string v4, "com.sec.android.app.factorykeystring"

    const-string v5, "com.sec.android.app.status.WiFiStressTest"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 956
    const-string v4, "requestCode"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 957
    const-string v0, "WIFI Stress"

    .line 958
    sget-object v4, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const v5, 0x7f020004

    invoke-static {v4, v5}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 960
    goto :goto_1

    .line 932
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public saveShortcutPreferences()V
    .locals 5

    .prologue
    .line 1127
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "shortcutSharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1129
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1130
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "isShortCustCreated"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1131
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1132
    return-void
.end method

.method public setAutoRotate(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setAutoRotate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 212
    return-void
.end method

.method public setCalDate(Ljava/lang/String;)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setCalDate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCalDate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    const-string v0, "EFS_CALDATE_PATH"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setFirstBoot()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 789
    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "setFirstBoot"

    const-string v4, "set"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    sget-object v2, Lcom/sec/factory/modules/ModuleCommon;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.app.factorymode.first_boot_flag"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 792
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 793
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "factorymode.shortcut.skipped"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 794
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 795
    const/4 v2, 0x1

    return v2
.end method

.method public setFtClientState(Z)Z
    .locals 2
    .param p1, "state"    # Z

    .prologue
    .line 846
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setFtClientState"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    const-string v0, "sys.factory.runningFtClient"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const/4 v0, 0x1

    return v0
.end method

.method public setHDMIPatternSwitch(Ljava/lang/String;)Z
    .locals 5
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string v1, "HDMI_PATTERN_SWITCH"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 205
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setHDMIPatternSwitch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return v0
.end method

.method public setHWVer(Ljava/lang/String;)Z
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 712
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setHWVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    const-string v1, "\""

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 714
    .local v0, "hw_version":Ljava/lang/String;
    const-string v1, "~"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 715
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setHWVer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "write version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    const-string v1, "EFS_HW_PATH"

    invoke-static {v1, v0}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public setKeyLock(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "SHOULD_SHUT_DOWN"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 174
    return-void
.end method

.method public setMediasSanningCount()Z
    .locals 2

    .prologue
    .line 822
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMediascanningCounter"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    const-string v0, "sys.factory.mediaScanningCount"

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getMediaScanningCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    const/4 v0, 0x1

    return v0
.end method

.method public setMediasSanningCount(I)Z
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 816
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMediascanningCounter : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    const-string v0, "sys.factory.mediaScanningCount"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const/4 v0, 0x1

    return v0
.end method

.method public setSwitchFactoryState()V
    .locals 4

    .prologue
    .line 1192
    const-string v0, "FACTORY_START"

    .line 1194
    .local v0, "FACTORY_START":Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommon;->isSLSI:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommon;->isQualcomm:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/factory/modules/ModuleCommon;->isSPRD:Z

    if-eqz v1, :cond_1

    .line 1195
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setSwitchFactoryState"

    const-string v3, "SwitchFactory= + FACTORY_START"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    const-string v1, "SWITCH_FACTORY"

    const-string v2, "FACTORY_START"

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1200
    :goto_0
    return-void

    .line 1198
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setSwitchFactoryState"

    const-string v3, "Not SLSI or Qualcomm model"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setUartPath(Ljava/lang/String;)Z
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setUartPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v0, "UART_SELECT"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setUsbPath(Ljava/lang/String;)Z
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setUsbPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v0, "USB_SELECT"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public sleepThread(I)V
    .locals 5
    .param p1, "time"    # I

    .prologue
    .line 72
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sleepThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Thread sleep during : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    int-to-long v2, p1

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public updateFailItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "strProcess"    # Ljava/lang/String;
    .param p2, "strItem"    # Ljava/lang/String;
    .param p3, "strSpec"    # Ljava/lang/String;
    .param p4, "strValue"    # Ljava/lang/String;

    .prologue
    .line 1252
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1253
    .local v0, "cmd":Ljava/lang/String;
    const-string v3, "FAILHIST_VERSION"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Version;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1254
    .local v1, "mVersion":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "NA"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1258
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1259
    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1260
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "NA"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1264
    :goto_1
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "setProperties"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cmd = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    const-string v3, "V1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1267
    const-string v3, "FACTORYAPP_FAILHIST"

    invoke-static {v3, v0}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1268
    const-string v3, "FACTORYAPP_FAILHIST_TIME"

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getTimeToString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    :cond_0
    :goto_2
    return-void

    .line 1257
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1262
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1269
    :cond_3
    const-string v3, "V2"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1270
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommon;->getTimeToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1271
    const-string v3, "FACTORY_FAILHIST"

    invoke-static {v3, v0}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 1272
    .local v2, "writeResult":Z
    if-eqz v2, :cond_4

    .line 1273
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "updateFailItem"

    const-string v5, "result = pass"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1275
    :cond_4
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "updateFailItem"

    const-string v5, "result = fail"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public writeFactoryMode(Ljava/lang/String;)Z
    .locals 5
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 330
    const/4 v0, 0x0

    .line 331
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "writeFactoryMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v1, "FACTORY_MODE"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 334
    if-eqz v0, :cond_0

    .line 335
    const-string v1, "FACTORY_MODE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 338
    :cond_0
    return v0
.end method

.method public writeHiddenMenuBlock(Ljava/lang/String;)Z
    .locals 5
    .param p1, "block"    # Ljava/lang/String;

    .prologue
    .line 377
    const/4 v0, 0x0

    .line 378
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "writeHiddenMenuBlock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HiddenMenuBlock="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v1, "HIDDEN_MENU_BLOCK"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 381
    if-eqz v0, :cond_0

    .line 382
    const-string v1, "HIDDEN_MENU_BLOCK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 385
    :cond_0
    return v0
.end method

.method public writeKeyStringBlock(Ljava/lang/String;)Z
    .locals 5
    .param p1, "block"    # Ljava/lang/String;

    .prologue
    .line 359
    const/4 v0, 0x0

    .line 360
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "writeKeyStringBlock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KeyStringBlock="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v1, "KEYSTRING_BLOCK"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 363
    if-eqz v0, :cond_0

    .line 364
    const-string v1, "KEYSTRING_BLOCK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 367
    :cond_0
    return v0
.end method

.method public writePrePay(Ljava/lang/String;)V
    .locals 4
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "writePrePay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const-string v0, "PRE_PAY"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    .line 350
    return-void
.end method

.method public writeRTC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "year"    # Ljava/lang/String;
    .param p2, "month"    # Ljava/lang/String;
    .param p3, "day"    # Ljava/lang/String;
    .param p4, "hour"    # Ljava/lang/String;
    .param p5, "minute"    # Ljava/lang/String;
    .param p6, "sec"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0xa

    const/16 v4, 0x9

    const/4 v3, 0x1

    .line 186
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 187
    .local v0, "cal":Ljava/util/Calendar;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 188
    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 189
    const/4 v1, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 191
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v1, v6, :cond_0

    .line 192
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0xc

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 193
    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 199
    :goto_0
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v6, v1}, Ljava/util/Calendar;->set(II)V

    .line 200
    const/16 v1, 0xd

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 201
    return-void

    .line 195
    :cond_0
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 196
    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    goto :goto_0
.end method

.method public writeRamSizeInfo(Ljava/lang/String;)Z
    .locals 5
    .param p1, "mRamSize"    # Ljava/lang/String;

    .prologue
    .line 431
    const/4 v0, 0x0

    .line 432
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "writeFactoryMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mRamSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v1, "DEVICE_RAM_SIZE_FILE"

    invoke-static {v1, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 435
    if-eqz v0, :cond_0

    .line 436
    const-string v1, "DEVICE_RAM_SIZE_FILE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 439
    :cond_0
    return v0
.end method

.method public writemBaroDelta(Ljava/lang/String;)V
    .locals 4
    .param p1, "delta"    # Ljava/lang/String;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommon;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "writemBaroDelta"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BaroDelta="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v0, "BAROMETE_DELTA"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 397
    const-string v0, "BAROMETE_CALIBRATION"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 398
    return-void
.end method
