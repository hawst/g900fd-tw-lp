.class public Lcom/sec/factory/modules/SensorCalculator;
.super Ljava/lang/Object;
.source "SensorCalculator.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "SensorCalculator"

.field private static final DEBUG:Z = true

.field private static RESULT_VALUE_NG:Ljava/lang/String; = null

.field private static RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String; = null

.field private static RESULT_VALUE_OK:Ljava/lang/String; = null

.field private static final STANDARD_GRAVITY:F = 9.80665f

.field private static mAccelerometerRawDataWeight:F

.field private static mSensorSpec:Lcom/sec/factory/modules/SensorSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "OK"

    sput-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_OK:Ljava/lang/String;

    .line 16
    const-string v0, "NG"

    sput-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    .line 17
    const-string v0, "None"

    sput-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    sput v0, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkSpecMagneticADC(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 191
    const-string v1, "SensorCalculator"

    const-string v2, "checkSpecMagneticADC"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_ADC()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    const-string v2, "MagneticADC"

    invoke-static {v1, v2}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 195
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_ADC()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    invoke-static {p0, p1, p2, v1}, Lcom/sec/factory/modules/SensorCalculator;->getResult(IIILcom/sec/factory/modules/SensorSpec$Range;)Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public static checkSpecMagneticADC2(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 201
    const-string v1, "SensorCalculator"

    const-string v2, "checkSpecMagneticADC2"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_ADC2()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    const-string v2, "MagneticADC2"

    invoke-static {v1, v2}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 205
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_ADC2()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    invoke-static {p0, p1, p2, v1}, Lcom/sec/factory/modules/SensorCalculator;->getResult(IIILcom/sec/factory/modules/SensorSpec$Range;)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public static checkSpecMagneticDAC(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 181
    const-string v1, "SensorCalculator"

    const-string v2, "checkSpecMagneticDAC"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_DAC()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    const-string v2, "MagneticDAC"

    invoke-static {v1, v2}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 185
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_DAC()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    invoke-static {p0, p1, p2, v1}, Lcom/sec/factory/modules/SensorCalculator;->getResult(IIILcom/sec/factory/modules/SensorSpec$Range;)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public static checkSpecMagneticSelf(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 211
    const-string v1, "SensorCalculator"

    const-string v2, "checkSpecMagneticSelf"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_Self()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    const-string v2, "MagneticSelf"

    invoke-static {v1, v2}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 215
    sget-object v1, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v1}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_Self()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v1

    invoke-static {p0, p1, p2, v1}, Lcom/sec/factory/modules/SensorCalculator;->getResult(IIILcom/sec/factory/modules/SensorSpec$Range;)Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public static getAccelerometerAngle([I)[Ljava/lang/String;
    .locals 10
    .param p0, "data"    # [I

    .prologue
    const v9, 0x42652ee1

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 88
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    .line 89
    .local v0, "angle":[Ljava/lang/String;
    aget v2, p0, v6

    aget v3, p0, v6

    mul-int/2addr v2, v3

    aget v3, p0, v7

    aget v4, p0, v7

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aget v3, p0, v8

    aget v4, p0, v8

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 101
    .local v1, "realg":F
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v6

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v9

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v7

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v9

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v8

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v9

    const/high16 v4, 0x42b40000    # 90.0f

    sub-float/2addr v3, v4

    const/high16 v4, -0x40800000    # -1.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 106
    return-object v0
.end method

.method public static getAccelerometerAngleDeviation(F)F
    .locals 1
    .param p0, "magnitude"    # F

    .prologue
    .line 114
    const v0, 0x411ce80a

    sub-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public static getAccelerometerAngleMagnitude(FFF)F
    .locals 2
    .param p0, "xValue"    # F
    .param p1, "yValue"    # F
    .param p2, "zValue"    # F

    .prologue
    .line 110
    mul-float v0, p0, p0

    mul-float v1, p1, p1

    add-float/2addr v0, v1

    mul-float v1, p2, p2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static getAccelerometerAngleXY(FF)I
    .locals 4
    .param p0, "xValue"    # F
    .param p1, "yValue"    # F

    .prologue
    .line 118
    float-to-double v0, p0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    neg-double v0, v0

    double-to-float v0, v0

    const v1, 0x42652ee1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static getAccelerometerRawDataWeight(I)F
    .locals 4
    .param p0, "bitCount"    # I

    .prologue
    .line 43
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    add-int/lit8 v2, p0, -0x2

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x411ce80a

    div-float/2addr v0, v1

    return v0
.end method

.method public static getAccelerometerRawDataWeight_Feature()F
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 64
    sget v1, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 65
    const-string v1, "SENSOR_TEST_ACC_BIT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "bit":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    const-string v1, "SensorCalculator"

    const-string v2, "getAccelerometerRawDataWeight_Feature"

    const-string v3, " bitCount(feature) : None => return 0"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    sput v4, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    .line 77
    :goto_0
    const-string v1, "IS_ACC_FULL_SCALE_4G"

    invoke-static {v1}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    sget v1, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sput v1, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    .line 79
    const-string v1, "SensorCalculator"

    const-string v2, "getAccelerometerRawDataWeight_Feature"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " mAccelerometerRawDataWeight : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_0
    sget v1, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    return v1

    .line 72
    :cond_1
    const-string v1, "SensorCalculator"

    const-string v2, "getAccelerometerRawDataWeight_Feature"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " bitCount(feature) : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerRawDataWeight(I)F

    move-result v1

    sput v1, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    goto :goto_0
.end method

.method public static getAccelerometerRawDataWeight_Spec()F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47
    sget v0, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 48
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 49
    const-string v0, "SensorCalculator"

    const-string v1, "getAccelerometerRawDataWeight_Spec"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitCount(Spec) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v3}, Lcom/sec/factory/modules/SensorSpec;->getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    move-result-object v3

    iget v3, v3, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mBitCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    move-result-object v0

    iget v0, v0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mBitCount:I

    invoke-static {v0}, Lcom/sec/factory/modules/SensorCalculator;->getAccelerometerRawDataWeight(I)F

    move-result v0

    sput v0, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    .line 60
    :cond_0
    :goto_0
    sget v0, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    return v0

    .line 54
    :cond_1
    const-string v0, "SensorCalculator"

    const-string v1, "getAccelerometerRawDataWeight_Spec"

    const-string v2, "bitCount(Spec) : None => return 0"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    sput v3, Lcom/sec/factory/modules/SensorCalculator;->mAccelerometerRawDataWeight:F

    goto :goto_0
.end method

.method private static getResult(IIILcom/sec/factory/modules/SensorSpec$Range;)Ljava/lang/String;
    .locals 3
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I
    .param p3, "range"    # Lcom/sec/factory/modules/SensorSpec$Range;

    .prologue
    .line 225
    if-eqz p3, :cond_e

    .line 226
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRangeCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 227
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "mRangeCount : 1"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    if-eqz v0, :cond_0

    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    invoke-static {p0, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    .line 318
    :goto_0
    return-object v0

    .line 237
    :cond_0
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    if-eqz v0, :cond_1

    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    invoke-static {p1, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_1

    .line 239
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto :goto_0

    .line 244
    :cond_1
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    if-eqz v0, :cond_2

    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    invoke-static {p2, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_2

    .line 246
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto :goto_0

    .line 250
    :cond_2
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_OK:Ljava/lang/String;

    goto :goto_0

    .line 251
    :cond_3
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRangeCount:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 252
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "mRangeCount : 2"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_X:Z

    if-eqz v0, :cond_4

    .line 256
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    invoke-static {p0, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Max:I

    invoke-static {p0, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    .line 258
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[All] Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto :goto_0

    .line 261
    :cond_4
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_X:Z

    if-nez v0, :cond_5

    .line 262
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    invoke-static {p0, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    .line 263
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range1] Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 266
    :cond_5
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    if-nez v0, :cond_6

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_X:Z

    if-eqz v0, :cond_6

    .line 267
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Max:I

    invoke-static {p0, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    .line 268
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range2] Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 274
    :cond_6
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Y:Z

    if-eqz v0, :cond_7

    .line 275
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    invoke-static {p1, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Max:I

    invoke-static {p1, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    .line 277
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[All] Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 280
    :cond_7
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Y:Z

    if-nez v0, :cond_8

    .line 281
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    invoke-static {p1, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    .line 282
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range1] Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 285
    :cond_8
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    if-nez v0, :cond_9

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Y:Z

    if-eqz v0, :cond_9

    .line 286
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Max:I

    invoke-static {p1, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    .line 287
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range2] Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 293
    :cond_9
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Z:Z

    if-eqz v0, :cond_a

    .line 294
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    invoke-static {p2, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Max:I

    invoke-static {p2, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    .line 296
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[All] Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 299
    :cond_a
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Z:Z

    if-nez v0, :cond_b

    .line 300
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    invoke-static {p2, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    .line 301
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range1] Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 304
    :cond_b
    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    if-nez v0, :cond_c

    iget-boolean v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Z:Z

    if-eqz v0, :cond_c

    .line 305
    iget v0, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Min:I

    iget v1, p3, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Max:I

    invoke-static {p2, v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    .line 306
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range2] Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 311
    :cond_c
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_OK:Ljava/lang/String;

    goto/16 :goto_0

    .line 313
    :cond_d
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "mRangeCount : Unknown"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

    goto/16 :goto_0

    .line 317
    :cond_e
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - Spec null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static getResultAccelerometerSelf(III)Ljava/lang/String;
    .locals 6
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 123
    sget-object v2, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v2}, Lcom/sec/factory/modules/SensorSpec;->getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    move-result-object v2

    const-string v3, "AccelSelf"

    invoke-static {v2, v3}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Accelerometer;Ljava/lang/String;)V

    .line 126
    sget-object v2, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v2}, Lcom/sec/factory/modules/SensorSpec;->getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    move-result-object v1

    .line 127
    .local v1, "spec":Lcom/sec/factory/modules/SensorSpec$Accelerometer;
    const-string v0, ""

    .line 129
    .local v0, "returnValue":Ljava/lang/String;
    if-eqz v1, :cond_9

    .line 130
    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget-boolean v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    if-eqz v2, :cond_4

    .line 131
    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    if-lt p0, v2, :cond_0

    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    if-ge v2, p0, :cond_3

    .line 132
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "F"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    :goto_0
    const-string v2, "SensorCalculator"

    const-string v3, "getResultAccelerometerSelf"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v5, v5, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v5, v5, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :goto_1
    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget-boolean v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    if-eqz v2, :cond_6

    .line 144
    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    if-lt p1, v2, :cond_1

    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    if-ge v2, p1, :cond_5

    .line 145
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "F"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    :goto_2
    const-string v2, "SensorCalculator"

    const-string v3, "getResultAccelerometerSelf"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v5, v5, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v5, v5, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :goto_3
    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget-boolean v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    if-eqz v2, :cond_8

    .line 157
    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    if-lt p2, v2, :cond_2

    iget-object v2, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v2, v2, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    if-ge v2, p2, :cond_7

    .line 158
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "F"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    :goto_4
    const-string v2, "SensorCalculator"

    const-string v3, "getResultAccelerometerSelf"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v5, v5, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    iget v5, v5, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_5
    const-string v2, "SensorCalculator"

    const-string v3, "getResultAccelerometerSelf"

    invoke-static {v2, v3, v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v0

    .line 173
    :goto_6
    return-object v2

    .line 134
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "P"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 140
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "P"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 147
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "P"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 153
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "P"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 160
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "P"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 166
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "P"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 172
    :cond_9
    const-string v2, "SensorCalculator"

    const-string v3, "getResultAccelerometerSelf"

    const-string v4, "FFF - Spec null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v2, "FFF"

    goto/16 :goto_6
.end method

.method public static initialize()V
    .locals 3

    .prologue
    .line 28
    const-string v0, "SensorCalculator"

    const-string v1, "initialize"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/sec/factory/modules/SensorSpec;

    invoke-direct {v0}, Lcom/sec/factory/modules/SensorSpec;-><init>()V

    sput-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    .line 32
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecAccel()Lcom/sec/factory/modules/SensorSpec$Accelerometer;

    move-result-object v0

    const-string v1, "AccelSelf"

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Accelerometer;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_DAC()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v0

    const-string v1, "MagneticDAC"

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 34
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_ADC()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v0

    const-string v1, "MagneticADC"

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_ADC2()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v0

    const-string v1, "MagneticADC2"

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/sec/factory/modules/SensorCalculator;->mSensorSpec:Lcom/sec/factory/modules/SensorSpec;

    invoke-virtual {v0}, Lcom/sec/factory/modules/SensorSpec;->getSpecGeomagnetic_Self()Lcom/sec/factory/modules/SensorSpec$Range;

    move-result-object v0

    const-string v1, "MagneticSelf"

    invoke-static {v0, v1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method private static isSpecIn(III)Z
    .locals 4
    .param p0, "value"    # I
    .param p1, "rangeMin"    # I
    .param p2, "rangeMax"    # I

    .prologue
    .line 325
    if-gt p1, p0, :cond_0

    if-gt p0, p2, :cond_0

    .line 326
    const-string v0, "SensorCalculator"

    const-string v1, "isSpecIn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pass => ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <= ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const/4 v0, 0x1

    .line 332
    :goto_0
    return v0

    .line 330
    :cond_0
    const-string v0, "SensorCalculator"

    const-string v1, "isSpecIn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail => ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <= ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static specLog(Lcom/sec/factory/modules/SensorSpec$Accelerometer;Ljava/lang/String;)V
    .locals 1
    .param p0, "data"    # Lcom/sec/factory/modules/SensorSpec$Accelerometer;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 367
    if-eqz p0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/factory/modules/SensorSpec$Accelerometer;->mRange:Lcom/sec/factory/modules/SensorSpec$Range;

    invoke-static {v0, p1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    .line 372
    :goto_0
    return-void

    .line 370
    :cond_0
    const/4 v0, 0x0

    check-cast v0, Lcom/sec/factory/modules/SensorSpec$Range;

    invoke-static {v0, p1}, Lcom/sec/factory/modules/SensorCalculator;->specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static specLog(Lcom/sec/factory/modules/SensorSpec$Range;Ljava/lang/String;)V
    .locals 3
    .param p0, "data"    # Lcom/sec/factory/modules/SensorSpec$Range;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    .local v0, "message":Ljava/lang/String;
    if-eqz p0, :cond_3

    .line 342
    iget-boolean v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    if-eqz v1, :cond_0

    .line 343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "X("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 348
    :goto_0
    iget-boolean v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    if-eqz v1, :cond_1

    .line 349
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Y("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 354
    :goto_1
    iget-boolean v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    if-eqz v1, :cond_2

    .line 355
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Z("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    :goto_2
    const-string v1, "SensorCalculator"

    const-string v2, "specLog"

    invoke-static {v1, v2, v0}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    return-void

    .line 345
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "X(not supported) , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Y(not supported) , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 357
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Z(not supported)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 360
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
