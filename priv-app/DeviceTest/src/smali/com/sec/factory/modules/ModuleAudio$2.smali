.class Lcom/sec/factory/modules/ModuleAudio$2;
.super Ljava/lang/Object;
.source "ModuleAudio.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZILandroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleAudio;

.field final synthetic val$mhandler:Landroid/os/Handler;

.field final synthetic val$resId:I


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleAudio;ILandroid/os/Handler;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleAudio$2;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iput p2, p0, Lcom/sec/factory/modules/ModuleAudio$2;->val$resId:I

    iput-object p3, p0, Lcom/sec/factory/modules/ModuleAudio$2;->val$mhandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$2;->this$0:Lcom/sec/factory/modules/ModuleAudio;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleAudio;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "playMedia"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudio$2;->val$resId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onCompletion "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudio$2;->val$mhandler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 277
    return-void
.end method
