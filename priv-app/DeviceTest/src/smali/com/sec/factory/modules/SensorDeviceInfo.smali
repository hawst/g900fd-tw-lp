.class public Lcom/sec/factory/modules/SensorDeviceInfo;
.super Ljava/lang/Object;
.source "SensorDeviceInfo.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "SensorDeviceInfo"

.field private static DUMMY:I

.field public static final TARGET_FILE:I

.field public static final TARGET_XML:I

.field public static final TYPE_ACCELEROMETER:I

.field public static final TYPE_BAROMETER:I

.field public static final TYPE_GEOMAGNETIC:I

.field public static final TYPE_GESTURE:I

.field public static final TYPE_GRIP:I

.field public static final TYPE_GYRO:I

.field public static final TYPE_HUMIDITY:I

.field public static final TYPE_LIGHT:I

.field public static final TYPE_PROXIMITY:I

.field public static final TYPE_TEMPERATURE:I

.field private static mSensorNameFileAccelerometer:Ljava/lang/String;

.field private static mSensorNameFileBarometer:Ljava/lang/String;

.field private static mSensorNameFileGeomagnetic:Ljava/lang/String;

.field private static mSensorNameFileGesture:Ljava/lang/String;

.field private static mSensorNameFileGrip:Ljava/lang/String;

.field private static mSensorNameFileGyro:Ljava/lang/String;

.field private static mSensorNameFileHumidity:Ljava/lang/String;

.field private static mSensorNameFileLight:Ljava/lang/String;

.field private static mSensorNameFileProximity:Ljava/lang/String;

.field private static mSensorNameFileTemperature:Ljava/lang/String;

.field private static mSensorNameXMLAccelerometer:Ljava/lang/String;

.field private static mSensorNameXMLBarometer:Ljava/lang/String;

.field private static mSensorNameXMLGeomagnetic:Ljava/lang/String;

.field private static mSensorNameXMLGesture:Ljava/lang/String;

.field private static mSensorNameXMLGrip:Ljava/lang/String;

.field private static mSensorNameXMLGyro:Ljava/lang/String;

.field private static mSensorNameXMLHumidity:Ljava/lang/String;

.field private static mSensorNameXMLLight:Ljava/lang/String;

.field private static mSensorNameXMLProximity:Ljava/lang/String;

.field private static mSensorNameXMLTemperature:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10
    const/4 v0, 0x0

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    .line 11
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    .line 12
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    .line 13
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_ACCELEROMETER:I

    .line 14
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_BAROMETER:I

    .line 15
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    .line 16
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GRIP:I

    .line 17
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GYRO:I

    .line 18
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_LIGHT:I

    .line 19
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_PROXIMITY:I

    .line 20
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_TEMPERATURE:I

    .line 21
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_HUMIDITY:I

    .line 22
    sget v0, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/factory/modules/SensorDeviceInfo;->DUMMY:I

    sput v0, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GESTURE:I

    .line 24
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLAccelerometer:Ljava/lang/String;

    .line 25
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLBarometer:Ljava/lang/String;

    .line 26
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGeomagnetic:Ljava/lang/String;

    .line 27
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGrip:Ljava/lang/String;

    .line 28
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGyro:Ljava/lang/String;

    .line 29
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLLight:Ljava/lang/String;

    .line 30
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLProximity:Ljava/lang/String;

    .line 31
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLTemperature:Ljava/lang/String;

    .line 32
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLHumidity:Ljava/lang/String;

    .line 33
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGesture:Ljava/lang/String;

    .line 34
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileAccelerometer:Ljava/lang/String;

    .line 35
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileBarometer:Ljava/lang/String;

    .line 36
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGeomagnetic:Ljava/lang/String;

    .line 37
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGrip:Ljava/lang/String;

    .line 38
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGyro:Ljava/lang/String;

    .line 39
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileLight:Ljava/lang/String;

    .line 40
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileProximity:Ljava/lang/String;

    .line 41
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileTemperature:Ljava/lang/String;

    .line 42
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileHumidity:Ljava/lang/String;

    .line 43
    sput-object v2, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGesture:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSensorName(II)Ljava/lang/String;
    .locals 5
    .param p0, "sensorType"    # I
    .param p1, "readTarget"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 49
    .local v0, "returnSensorName":Ljava/lang/String;
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    if-ne p1, v1, :cond_11

    .line 51
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_ACCELEROMETER:I

    if-ne p0, v1, :cond_2

    .line 52
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLAccelerometer:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 53
    const-string v1, "SENSOR_NAME_ACCELEROMETER"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLAccelerometer:Ljava/lang/String;

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLAccelerometer:Ljava/lang/String;

    .line 195
    :goto_0
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_XML:I

    if-ne p1, v1, :cond_23

    .line 196
    const-string v1, "SensorDeviceInfo"

    const-string v2, "getSensorName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "XML => return : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_1
    :goto_1
    return-object v0

    .line 59
    :cond_2
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_BAROMETER:I

    if-ne p0, v1, :cond_3

    .line 60
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLBarometer:Ljava/lang/String;

    goto :goto_0

    .line 63
    :cond_3
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    if-ne p0, v1, :cond_5

    .line 64
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGeomagnetic:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 65
    const-string v1, "SENSOR_NAME_MAGNETIC"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGeomagnetic:Ljava/lang/String;

    .line 68
    :cond_4
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGeomagnetic:Ljava/lang/String;

    goto :goto_0

    .line 71
    :cond_5
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GRIP:I

    if-ne p0, v1, :cond_6

    .line 72
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGrip:Ljava/lang/String;

    goto :goto_0

    .line 75
    :cond_6
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GYRO:I

    if-ne p0, v1, :cond_8

    .line 76
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGyro:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 77
    const-string v1, "SENSOR_NAME_GYROSCOPE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGyro:Ljava/lang/String;

    .line 80
    :cond_7
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGyro:Ljava/lang/String;

    goto :goto_0

    .line 83
    :cond_8
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_LIGHT:I

    if-ne p0, v1, :cond_a

    .line 84
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLLight:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 85
    const-string v1, "SENSOR_NAME_LIGHT"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLLight:Ljava/lang/String;

    .line 88
    :cond_9
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLLight:Ljava/lang/String;

    goto :goto_0

    .line 91
    :cond_a
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_PROXIMITY:I

    if-ne p0, v1, :cond_c

    .line 92
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLProximity:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 93
    const-string v1, "SENSOR_NAME_PROXIMITY"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLProximity:Ljava/lang/String;

    .line 96
    :cond_b
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLProximity:Ljava/lang/String;

    goto :goto_0

    .line 99
    :cond_c
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_TEMPERATURE:I

    if-ne p0, v1, :cond_d

    .line 100
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLTemperature:Ljava/lang/String;

    goto :goto_0

    .line 103
    :cond_d
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_HUMIDITY:I

    if-ne p0, v1, :cond_e

    .line 104
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLHumidity:Ljava/lang/String;

    goto/16 :goto_0

    .line 107
    :cond_e
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GESTURE:I

    if-ne p0, v1, :cond_10

    .line 108
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGesture:Ljava/lang/String;

    if-nez v1, :cond_f

    .line 109
    const-string v1, "SENSOR_NAME_GESTURE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGesture:Ljava/lang/String;

    .line 112
    :cond_f
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameXMLGesture:Ljava/lang/String;

    goto/16 :goto_0

    .line 116
    :cond_10
    const-string v1, "SensorDeviceInfo"

    const-string v2, "getSensorName"

    const-string v3, "TARGET_XML - sensorType : Unknown"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :cond_11
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    if-ne p1, v1, :cond_22

    .line 122
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_ACCELEROMETER:I

    if-ne p0, v1, :cond_13

    .line 123
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileAccelerometer:Ljava/lang/String;

    if-nez v1, :cond_12

    .line 124
    const-string v1, "ACCEL_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileAccelerometer:Ljava/lang/String;

    .line 127
    :cond_12
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileAccelerometer:Ljava/lang/String;

    goto/16 :goto_0

    .line 130
    :cond_13
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_BAROMETER:I

    if-ne p0, v1, :cond_15

    .line 131
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileBarometer:Ljava/lang/String;

    if-nez v1, :cond_14

    .line 132
    const-string v1, "BAROMETER_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileBarometer:Ljava/lang/String;

    .line 135
    :cond_14
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileBarometer:Ljava/lang/String;

    goto/16 :goto_0

    .line 138
    :cond_15
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GEOMAGNETIC:I

    if-ne p0, v1, :cond_17

    .line 139
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGeomagnetic:Ljava/lang/String;

    if-nez v1, :cond_16

    .line 140
    const-string v1, "GEOMAGNETIC_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGeomagnetic:Ljava/lang/String;

    .line 143
    :cond_16
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGeomagnetic:Ljava/lang/String;

    goto/16 :goto_0

    .line 146
    :cond_17
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GRIP:I

    if-ne p0, v1, :cond_18

    .line 147
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGrip:Ljava/lang/String;

    goto/16 :goto_0

    .line 150
    :cond_18
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GYRO:I

    if-ne p0, v1, :cond_1a

    .line 151
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGyro:Ljava/lang/String;

    if-nez v1, :cond_19

    .line 152
    const-string v1, "GYRO_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGyro:Ljava/lang/String;

    .line 155
    :cond_19
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGyro:Ljava/lang/String;

    goto/16 :goto_0

    .line 158
    :cond_1a
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_LIGHT:I

    if-ne p0, v1, :cond_1c

    .line 159
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileLight:Ljava/lang/String;

    if-nez v1, :cond_1b

    .line 160
    const-string v1, "LIGHT_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileLight:Ljava/lang/String;

    .line 163
    :cond_1b
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileLight:Ljava/lang/String;

    goto/16 :goto_0

    .line 166
    :cond_1c
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_PROXIMITY:I

    if-ne p0, v1, :cond_1e

    .line 167
    sget-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileProximity:Ljava/lang/String;

    if-nez v1, :cond_1d

    .line 168
    const-string v1, "PROXI_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileProximity:Ljava/lang/String;

    .line 171
    :cond_1d
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileProximity:Ljava/lang/String;

    goto/16 :goto_0

    .line 174
    :cond_1e
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_TEMPERATURE:I

    if-ne p0, v1, :cond_1f

    .line 175
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileTemperature:Ljava/lang/String;

    goto/16 :goto_0

    .line 178
    :cond_1f
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_HUMIDITY:I

    if-ne p0, v1, :cond_20

    .line 179
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileHumidity:Ljava/lang/String;

    goto/16 :goto_0

    .line 182
    :cond_20
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TYPE_GESTURE:I

    if-ne p0, v1, :cond_21

    .line 183
    sget-object v0, Lcom/sec/factory/modules/SensorDeviceInfo;->mSensorNameFileGesture:Ljava/lang/String;

    goto/16 :goto_0

    .line 187
    :cond_21
    const-string v1, "SensorDeviceInfo"

    const-string v2, "getSensorName"

    const-string v3, "TARGET_FILE - sensorType : Unknown"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 192
    :cond_22
    const-string v1, "SensorDeviceInfo"

    const-string v2, "getSensorName"

    const-string v3, "readTarget : Unknown"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 197
    :cond_23
    sget v1, Lcom/sec/factory/modules/SensorDeviceInfo;->TARGET_FILE:I

    if-ne p1, v1, :cond_1

    .line 198
    const-string v1, "SensorDeviceInfo"

    const-string v2, "getSensorName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File => return : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
