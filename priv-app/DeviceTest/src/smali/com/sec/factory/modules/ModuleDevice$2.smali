.class Lcom/sec/factory/modules/ModuleDevice$2;
.super Landroid/os/Handler;
.source "ModuleDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/modules/ModuleDevice;->runOTGTest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleDevice;

.field final synthetic val$response:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleDevice;Landroid/os/Looper;Landroid/content/Intent;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iput-object p3, p0, Lcom/sec/factory/modules/ModuleDevice$2;->val$response:Landroid/content/Intent;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1237
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    .line 1238
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "runOTGTest"

    const-string v3, "OTG Test Finish"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    const-string v1, "OTG_TEST_MODE"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "OK"

    .line 1241
    .local v0, "resData":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "runOTGTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OTG Test Result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1244
    const-string v1, "OTG_TEST_MODE"

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_OFF:[B
    invoke-static {}, Lcom/sec/factory/modules/ModuleDevice;->access$800()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1245
    const-string v0, "OK"

    .line 1253
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "runOTGTest"

    const-string v3, "OTG TEST OFF"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "runOTGTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OTG Test Result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    const-string v1, "OTG_MUIC_SET"

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->OTG_MUIC_OFF:[B
    invoke-static {}, Lcom/sec/factory/modules/ModuleDevice;->access$900()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    .line 1256
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->val$response:Landroid/content/Intent;

    const-string v2, "result"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1257
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDevice$2;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDevice$2;->val$response:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleDevice;->sendBroadcast(Landroid/content/Intent;)V

    .line 1259
    .end local v0    # "resData":Ljava/lang/String;
    :cond_0
    return-void

    .line 1239
    :cond_1
    const-string v0, "NG"

    goto :goto_0

    .line 1247
    .restart local v0    # "resData":Ljava/lang/String;
    :cond_2
    const-string v0, "NG"

    goto :goto_1

    .line 1250
    :cond_3
    const-string v1, "OTG_TEST_MODE"

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->OTG_TEST_OFF:[B
    invoke-static {}, Lcom/sec/factory/modules/ModuleDevice;->access$800()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    goto :goto_1
.end method
