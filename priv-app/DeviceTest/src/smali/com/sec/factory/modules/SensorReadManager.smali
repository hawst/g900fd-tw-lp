.class public Lcom/sec/factory/modules/SensorReadManager;
.super Ljava/lang/Object;
.source "SensorReadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;,
        Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;,
        Lcom/sec/factory/modules/SensorReadManager$SensorListener;
    }
.end annotation


# static fields
.field public static final MAGNETIC_DATA_NUMBER_ADC:I = 0x5

.field public static final MAGNETIC_DATA_NUMBER_ALL:I = 0x8

.field public static final MAGNETIC_DATA_NUMBER_DAC:I = 0x4

.field public static final MAGNETIC_DATA_NUMBER_OFFSET_H:I = 0x7

.field public static final MAGNETIC_DATA_NUMBER_POWER_OFF:I = 0x2

.field public static final MAGNETIC_DATA_NUMBER_POWER_ON:I = 0x1

.field public static final MAGNETIC_DATA_NUMBER_SELF:I = 0x6

.field public static final MAGNETIC_DATA_NUMBER_STATUS:I = 0x3

.field public static final MAGNETIC_DATA_NUMBER_TEMP:I = 0x2

.field private static mBuffer_SensorValue_Accelerometer:[F

.field private static mBuffer_SensorValue_Barometer:[F

.field private static mBuffer_SensorValue_Bio:[F

.field private static mBuffer_SensorValue_BodyTemp:[F

.field private static mBuffer_SensorValue_Grip:[F

.field private static mBuffer_SensorValue_Gyro:[F

.field private static mBuffer_SensorValue_Hrm:[F

.field private static mBuffer_SensorValue_Humidity:[F

.field private static mBuffer_SensorValue_Light:[F

.field private static mBuffer_SensorValue_Magnetic:[F

.field private static mBuffer_SensorValue_Proximity:[F

.field private static mBuffer_SensorValue_Temperature:[F

.field private static mBuffer_SensorValue_UV:[F


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private DUMMY:I

.field private final SENSOR_ON_ARRAY_INDEX_ACCELEROMETER:I

.field private final SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

.field private final SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_SELF:I

.field private final SENSOR_ON_ARRAY_INDEX_BAROMETER:I

.field private final SENSOR_ON_ARRAY_INDEX_BIO:I

.field private final SENSOR_ON_ARRAY_INDEX_BODYTEMP:I

.field private final SENSOR_ON_ARRAY_INDEX_GRIP:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_EXPANSION:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_POWER:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_SELF:I

.field private final SENSOR_ON_ARRAY_INDEX_GYRO_TEMPERATURE:I

.field private final SENSOR_ON_ARRAY_INDEX_HRM:I

.field private final SENSOR_ON_ARRAY_INDEX_HUMIDITY:I

.field private final SENSOR_ON_ARRAY_INDEX_LIGHT:I

.field private final SENSOR_ON_ARRAY_INDEX_LIGHT_CCT:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_ADC:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_DAC:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_OFFSETH:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_ON:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_SELF:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_STATUS:I

.field private final SENSOR_ON_ARRAY_INDEX_MAGNETIC_TEMPERATURE:I

.field private final SENSOR_ON_ARRAY_INDEX_PROXIMITY:I

.field private final SENSOR_ON_ARRAY_INDEX_TEMPERATURE:I

.field private final SENSOR_ON_ARRAY_INDEX_UV:I

.field private final SENSOR_ON_ARRAY_LENGTH:I

.field private mAccelerometerSensor:Landroid/hardware/Sensor;

.field private mBarometerSensor:Landroid/hardware/Sensor;

.field private mBioSensor:Landroid/hardware/Sensor;

.field private mBodyTempSensor:Landroid/hardware/Sensor;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mGyroscopeSensor:Landroid/hardware/Sensor;

.field private mHrmSensor:Landroid/hardware/Sensor;

.field private mHumiditySensor:Landroid/hardware/Sensor;

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mMagneticSensor:Landroid/hardware/Sensor;

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorOn:[Z

.field private mTemperatureSensor:Landroid/hardware/Sensor;

.field private mUVSensor:Landroid/hardware/Sensor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    .line 76
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    .line 77
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Gyro:[F

    .line 78
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Light:[F

    .line 79
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F

    .line 81
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Proximity:[F

    .line 84
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Temperature:[F

    .line 85
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Humidity:[F

    .line 86
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Grip:[F

    .line 87
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Bio:[F

    .line 88
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Hrm:[F

    .line 89
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_BodyTemp:[F

    .line 90
    sput-object v1, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_UV:[F

    return-void

    .line 81
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>([ILandroid/hardware/SensorManager;)V
    .locals 5
    .param p1, "sensorID"    # [I
    .param p2, "sm"    # Landroid/hardware/SensorManager;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v1, "SensorReadManager"

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadManager;->CLASS_NAME:Ljava/lang/String;

    .line 16
    iput v4, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    .line 17
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER:I

    .line 18
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

    .line 19
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_SELF:I

    .line 20
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BAROMETER:I

    .line 21
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO:I

    .line 22
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_EXPANSION:I

    .line 23
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_POWER:I

    .line 24
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_SELF:I

    .line 25
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_TEMPERATURE:I

    .line 26
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_LIGHT:I

    .line 27
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_LIGHT_CCT:I

    .line 28
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC:I

    .line 29
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_ON:I

    .line 30
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_STATUS:I

    .line 31
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_TEMPERATURE:I

    .line 32
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_DAC:I

    .line 33
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_ADC:I

    .line 34
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_SELF:I

    .line 35
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_OFFSETH:I

    .line 36
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

    .line 37
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_PROXIMITY:I

    .line 38
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_TEMPERATURE:I

    .line 39
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_HUMIDITY:I

    .line 40
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    .line 41
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BIO:I

    .line 42
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_HRM:I

    .line 43
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BODYTEMP:I

    .line 44
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_UV:I

    .line 45
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->DUMMY:I

    iput v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    .line 46
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    .line 59
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 60
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    .line 61
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 62
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 63
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 64
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 65
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 66
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 67
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mTemperatureSensor:Landroid/hardware/Sensor;

    .line 68
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mHumiditySensor:Landroid/hardware/Sensor;

    .line 69
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mGripSensor:Landroid/hardware/Sensor;

    .line 70
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mBioSensor:Landroid/hardware/Sensor;

    .line 71
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mHrmSensor:Landroid/hardware/Sensor;

    .line 72
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mBodyTempSensor:Landroid/hardware/Sensor;

    .line 73
    iput-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mUVSensor:Landroid/hardware/Sensor;

    .line 94
    const-string v1, "SensorReadManager"

    const-string v2, "SensorReadManager"

    const-string v3, "Sensor On"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_LENGTH:I

    if-ge v0, v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean v4, v1, v0

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    if-nez v1, :cond_1

    .line 100
    new-instance v1, Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/SensorReadManager$SensorListener;-><init>(Lcom/sec/factory/modules/SensorReadManager;)V

    iput-object v1, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    .line 103
    :cond_1
    iput-object p2, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 104
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v1}, Lcom/sec/factory/modules/SensorReadManager;->turnOnOffSensor([ILandroid/hardware/SensorManager;Z)Z

    .line 105
    return-void
.end method

.method private ConverterID(I)I
    .locals 5
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 1625
    const/4 v0, -0x1

    .line 1627
    .local v0, "sensorOnArrayIndex":I
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p1, v1, :cond_1

    .line 1628
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER:I

    .line 1685
    :cond_0
    :goto_0
    const-string v1, "SensorReadManager"

    const-string v2, "ConverterID"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1688
    return v0

    .line 1629
    :cond_1
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne p1, v1, :cond_2

    .line 1630
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_N_ANGLE:I

    goto :goto_0

    .line 1631
    :cond_2
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne p1, v1, :cond_3

    .line 1632
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_ACCELEROMETER_SELF:I

    goto :goto_0

    .line 1633
    :cond_3
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p1, v1, :cond_4

    .line 1634
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BAROMETER:I

    goto :goto_0

    .line 1635
    :cond_4
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne p1, v1, :cond_5

    .line 1636
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO:I

    goto :goto_0

    .line 1637
    :cond_5
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    if-ne p1, v1, :cond_6

    .line 1638
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_POWER:I

    goto :goto_0

    .line 1639
    :cond_6
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne p1, v1, :cond_7

    .line 1640
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_EXPANSION:I

    goto :goto_0

    .line 1641
    :cond_7
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne p1, v1, :cond_8

    .line 1642
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_SELF:I

    goto :goto_0

    .line 1643
    :cond_8
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne p1, v1, :cond_9

    .line 1644
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GYRO_TEMPERATURE:I

    goto :goto_0

    .line 1645
    :cond_9
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne p1, v1, :cond_a

    .line 1646
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_LIGHT:I

    goto :goto_0

    .line 1647
    :cond_a
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    if-ne p1, v1, :cond_b

    .line 1648
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_LIGHT_CCT:I

    goto :goto_0

    .line 1649
    :cond_b
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne p1, v1, :cond_c

    .line 1650
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC:I

    goto :goto_0

    .line 1651
    :cond_c
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne p1, v1, :cond_d

    .line 1652
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_ON:I

    goto :goto_0

    .line 1653
    :cond_d
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne p1, v1, :cond_e

    .line 1654
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_POWER_OFF:I

    goto :goto_0

    .line 1655
    :cond_e
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne p1, v1, :cond_f

    .line 1656
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_STATUS:I

    goto :goto_0

    .line 1657
    :cond_f
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne p1, v1, :cond_10

    .line 1658
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_TEMPERATURE:I

    goto/16 :goto_0

    .line 1659
    :cond_10
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne p1, v1, :cond_11

    .line 1660
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_DAC:I

    goto/16 :goto_0

    .line 1661
    :cond_11
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne p1, v1, :cond_12

    .line 1662
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_ADC:I

    goto/16 :goto_0

    .line 1663
    :cond_12
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne p1, v1, :cond_13

    .line 1664
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_SELF:I

    goto/16 :goto_0

    .line 1665
    :cond_13
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne p1, v1, :cond_14

    .line 1666
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_MAGNETIC_OFFSETH:I

    goto/16 :goto_0

    .line 1667
    :cond_14
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne p1, v1, :cond_15

    .line 1668
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_PROXIMITY:I

    goto/16 :goto_0

    .line 1669
    :cond_15
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    if-ne p1, v1, :cond_16

    .line 1670
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_TEMPERATURE:I

    goto/16 :goto_0

    .line 1671
    :cond_16
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    if-ne p1, v1, :cond_17

    .line 1672
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_HUMIDITY:I

    goto/16 :goto_0

    .line 1673
    :cond_17
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    if-ne p1, v1, :cond_18

    .line 1674
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_GRIP:I

    goto/16 :goto_0

    .line 1675
    :cond_18
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    if-ne p1, v1, :cond_19

    .line 1676
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BIO:I

    goto/16 :goto_0

    .line 1677
    :cond_19
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    if-ne p1, v1, :cond_1a

    .line 1678
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_HRM:I

    goto/16 :goto_0

    .line 1679
    :cond_1a
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BODYTEMP:I

    if-ne p1, v1, :cond_1b

    .line 1680
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_BODYTEMP:I

    goto/16 :goto_0

    .line 1681
    :cond_1b
    sget v1, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    if-ne p1, v1, :cond_0

    .line 1682
    iget v0, p0, Lcom/sec/factory/modules/SensorReadManager;->SENSOR_ON_ARRAY_INDEX_UV:I

    goto/16 :goto_0
.end method

.method static synthetic access$002([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    return-object p0
.end method

.method static synthetic access$100()[F
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    return-object v0
.end method

.method static synthetic access$1002([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Grip:[F

    return-object p0
.end method

.method static synthetic access$102([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    return-object p0
.end method

.method static synthetic access$1102([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_BodyTemp:[F

    return-object p0
.end method

.method static synthetic access$1202([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_UV:[F

    return-object p0
.end method

.method static synthetic access$202([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Gyro:[F

    return-object p0
.end method

.method static synthetic access$302([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Light:[F

    return-object p0
.end method

.method static synthetic access$402([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F

    return-object p0
.end method

.method static synthetic access$502([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Proximity:[F

    return-object p0
.end method

.method static synthetic access$602([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Temperature:[F

    return-object p0
.end method

.method static synthetic access$702([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Humidity:[F

    return-object p0
.end method

.method static synthetic access$802([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Bio:[F

    return-object p0
.end method

.method static synthetic access$902([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 13
    sput-object p0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Hrm:[F

    return-object p0
.end method

.method private dataCheck([F)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [F

    .prologue
    .line 1694
    const-string v1, ""

    .line 1696
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 1697
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 1698
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1700
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 1701
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1697
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1705
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 1708
    :cond_2
    return-object v1
.end method


# virtual methods
.method public isSensorOn(I)Z
    .locals 5
    .param p1, "moduleSensorID"    # I

    .prologue
    .line 520
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v0

    .line 521
    .local v0, "sensorOnArrayIndex":I
    const-string v1, "SensorReadManager"

    const-string v2, "isSensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensorOnArrayIndex["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aget-boolean v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget-object v1, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aget-boolean v1, v1, v0

    return v1
.end method

.method public isSensorReady(I)Z
    .locals 7
    .param p1, "moduleSensorID"    # I

    .prologue
    const/4 v5, 0x0

    .line 526
    const/4 v1, 0x0

    .line 528
    .local v1, "result":Z
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p1, v3, :cond_1

    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    if-eqz v3, :cond_1

    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    array-length v3, v3

    if-lez v3, :cond_1

    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_1

    .line 530
    const/4 v1, 0x1

    .line 554
    :cond_0
    :goto_0
    const-string v4, "SensorReadManager"

    const-string v5, "isSensorReady"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "] is "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v1, :cond_6

    const-string v3, "ready"

    :goto_1
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    return v1

    .line 531
    :cond_1
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-eq p1, v3, :cond_2

    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne p1, v3, :cond_3

    :cond_2
    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F

    if-eqz v3, :cond_3

    .line 541
    const/4 v1, 0x1

    goto :goto_0

    .line 542
    :cond_3
    sget v3, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p1, v3, :cond_5

    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    if-eqz v3, :cond_5

    .line 543
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 544
    sget-object v3, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    aget v3, v3, v0

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_4

    .line 545
    const/4 v1, 0x1

    .line 546
    goto :goto_0

    .line 543
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 550
    .end local v0    # "i":I
    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 551
    .local v2, "sensorOnArrayIndex":I
    const-string v3, "SensorReadManager"

    const-string v4, "isSensorReady"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Not support Sensor ID ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aget-boolean v6, v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 554
    .end local v2    # "sensorOnArrayIndex":I
    :cond_6
    const-string v3, "not yet"

    goto/16 :goto_1
.end method

.method public returnAccelermeter()[F
    .locals 3

    .prologue
    .line 623
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 624
    const-string v0, "SensorReadManager"

    const-string v1, "returnAccelermeter"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    return-object v0
.end method

.method public returnBarometer()[F
    .locals 3

    .prologue
    .line 631
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 632
    const-string v0, "SensorReadManager"

    const-string v1, "returnBarometer"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    return-object v0
.end method

.method public returnBio()[F
    .locals 3

    .prologue
    .line 687
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 688
    const-string v0, "SensorReadManager"

    const-string v1, "returnBio"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Bio:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Bio:[F

    return-object v0
.end method

.method public returnBodyTemp()[F
    .locals 3

    .prologue
    .line 711
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 712
    const-string v0, "SensorReadManager"

    const-string v1, "returnBodyTemp"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_BodyTemp:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_BodyTemp:[F

    return-object v0
.end method

.method public returnGrip()[F
    .locals 3

    .prologue
    .line 703
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 704
    const-string v0, "SensorReadManager"

    const-string v1, "returnGrip"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Grip:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Grip:[F

    return-object v0
.end method

.method public returnGyro()[F
    .locals 3

    .prologue
    .line 639
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 640
    const-string v0, "SensorReadManager"

    const-string v1, "returnGyro"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Gyro:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Gyro:[F

    return-object v0
.end method

.method public returnGyroExpansion()Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
    .locals 1

    .prologue
    .line 768
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnGyroPower()I
    .locals 1

    .prologue
    .line 772
    const/4 v0, 0x1

    return v0
.end method

.method public returnHrm()[F
    .locals 3

    .prologue
    .line 695
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 696
    const-string v0, "SensorReadManager"

    const-string v1, "returnHrm"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Hrm:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Hrm:[F

    return-object v0
.end method

.method public returnHumidity()[F
    .locals 3

    .prologue
    .line 679
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 680
    const-string v0, "SensorReadManager"

    const-string v1, "returnHumidity"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Humidity:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Humidity:[F

    return-object v0
.end method

.method public returnLight()[F
    .locals 3

    .prologue
    .line 647
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 648
    const-string v0, "SensorReadManager"

    const-string v1, "returnLight"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Light:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Light:[F

    return-object v0
.end method

.method public returnMagnetic()[F
    .locals 3

    .prologue
    .line 655
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 656
    const-string v0, "SensorReadManager"

    const-string v1, "returnMagnetic"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F

    return-object v0
.end method

.method public returnMagneticExpansion_Alps(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    .locals 10
    .param p1, "testNo"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1205
    new-array v0, v9, [I

    .line 1206
    .local v0, "parameter1":[I
    new-array v1, v9, [I

    .line 1207
    .local v1, "parameter2":[I
    new-array v2, v9, [I

    .line 1208
    .local v2, "parameter3":[I
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    .line 1210
    .local v3, "returnData":Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    packed-switch p1, :pswitch_data_0

    .line 1336
    :cond_0
    :goto_0
    :pswitch_0
    if-ne p1, v8, :cond_1

    .line 1337
    aput v8, v0, v6

    .line 1339
    if-eqz v1, :cond_1

    .line 1340
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    .line 1342
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_8

    .line 1343
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1344
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    .line 1352
    :cond_1
    :goto_1
    return-object v3

    .line 1212
    :pswitch_1
    aput v7, v0, v6

    .line 1214
    if-eqz v1, :cond_0

    .line 1215
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    .line 1217
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_2

    .line 1218
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1219
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    goto :goto_0

    .line 1221
    :cond_2
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 1222
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto :goto_0

    .line 1228
    :pswitch_2
    aput v7, v0, v6

    .line 1229
    aput v9, v0, v6

    .line 1231
    if-eqz v1, :cond_0

    .line 1232
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    .line 1234
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_3

    .line 1235
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1240
    :goto_2
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    goto :goto_0

    .line 1237
    :cond_3
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_2

    .line 1247
    :pswitch_3
    aput v7, v0, v6

    .line 1248
    aput v9, v0, v6

    .line 1249
    aput v5, v0, v6

    .line 1251
    if-eqz v1, :cond_0

    .line 1252
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    .line 1254
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_4

    .line 1255
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1260
    :goto_3
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1261
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1262
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1257
    :cond_4
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_3

    .line 1267
    :pswitch_4
    aput v7, v0, v6

    .line 1268
    aput v9, v0, v6

    .line 1269
    aput v5, v0, v6

    .line 1270
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1272
    if-eqz v1, :cond_0

    .line 1273
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    .line 1275
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_5

    .line 1276
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1281
    :goto_4
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1282
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v8

    .line 1283
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1278
    :cond_5
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_4

    .line 1288
    :pswitch_5
    aput v7, v0, v6

    .line 1289
    aput v9, v0, v6

    .line 1290
    aput v5, v0, v6

    .line 1291
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1292
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 1294
    if-eqz v1, :cond_0

    .line 1295
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    .line 1297
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_6

    .line 1298
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1303
    :goto_5
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1304
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1305
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1300
    :cond_6
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_5

    .line 1310
    :pswitch_6
    aput v7, v0, v6

    .line 1311
    aput v9, v0, v6

    .line 1312
    aput v5, v0, v6

    .line 1313
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1314
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 1315
    const/4 v4, 0x7

    aput v4, v0, v6

    .line 1317
    if-eqz v1, :cond_0

    .line 1318
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    .line 1320
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_7

    .line 1321
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1326
    :goto_6
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1327
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1328
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1323
    :cond_7
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_6

    .line 1346
    :cond_8
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 1347
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto/16 :goto_1

    .line 1210
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public returnMagneticExpansion_Asahi(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    .locals 1
    .param p1, "testNo"    # I

    .prologue
    .line 991
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnMagneticExpansion_Bosch(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    .locals 1
    .param p1, "testNo"    # I

    .prologue
    .line 1619
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnMagneticExpansion_STMicro(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    .locals 10
    .param p1, "testNo"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1357
    new-array v0, v9, [I

    .line 1358
    .local v0, "parameter1":[I
    new-array v1, v9, [I

    .line 1359
    .local v1, "parameter2":[I
    new-array v2, v9, [I

    .line 1360
    .local v2, "parameter3":[I
    new-instance v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    .line 1362
    .local v3, "returnData":Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    packed-switch p1, :pswitch_data_0

    .line 1488
    :cond_0
    :goto_0
    :pswitch_0
    if-ne p1, v8, :cond_1

    .line 1489
    aput v8, v0, v6

    .line 1491
    if-eqz v1, :cond_1

    .line 1492
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    .line 1494
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_8

    .line 1495
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1496
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    .line 1504
    :cond_1
    :goto_1
    return-object v3

    .line 1364
    :pswitch_1
    aput v7, v0, v6

    .line 1366
    if-eqz v1, :cond_0

    .line 1367
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    .line 1369
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_2

    .line 1370
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1371
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v7

    goto :goto_0

    .line 1373
    :cond_2
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 1374
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto :goto_0

    .line 1380
    :pswitch_2
    aput v7, v0, v6

    .line 1381
    aput v9, v0, v6

    .line 1383
    if-eqz v1, :cond_0

    .line 1384
    new-array v4, v8, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    .line 1386
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_3

    .line 1387
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1392
    :goto_2
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    goto :goto_0

    .line 1389
    :cond_3
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_2

    .line 1399
    :pswitch_3
    aput v7, v0, v6

    .line 1400
    aput v9, v0, v6

    .line 1401
    aput v5, v0, v6

    .line 1403
    if-eqz v1, :cond_0

    .line 1404
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    .line 1406
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_4

    .line 1407
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1412
    :goto_3
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1413
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1414
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1409
    :cond_4
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_3

    .line 1419
    :pswitch_4
    aput v7, v0, v6

    .line 1420
    aput v9, v0, v6

    .line 1421
    aput v5, v0, v6

    .line 1422
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1424
    if-eqz v1, :cond_0

    .line 1425
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    .line 1427
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_5

    .line 1428
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1433
    :goto_4
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1434
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v8

    .line 1435
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1430
    :cond_5
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_4

    .line 1440
    :pswitch_5
    aput v7, v0, v6

    .line 1441
    aput v9, v0, v6

    .line 1442
    aput v5, v0, v6

    .line 1443
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1444
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 1446
    if-eqz v1, :cond_0

    .line 1447
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    .line 1449
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_6

    .line 1450
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1455
    :goto_5
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1456
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1457
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1452
    :cond_6
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_5

    .line 1462
    :pswitch_6
    aput v7, v0, v6

    .line 1463
    aput v9, v0, v6

    .line 1464
    aput v5, v0, v6

    .line 1465
    const/4 v4, 0x5

    aput v4, v0, v6

    .line 1466
    const/4 v4, 0x6

    aput v4, v0, v6

    .line 1467
    const/4 v4, 0x7

    aput v4, v0, v6

    .line 1469
    if-eqz v1, :cond_0

    .line 1470
    new-array v4, v5, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    .line 1472
    iget v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mReturnValue:I

    if-ltz v4, :cond_7

    .line 1473
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "OK"

    aput-object v5, v4, v6

    .line 1478
    :goto_6
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 1479
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1480
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget v5, v1, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_0

    .line 1475
    :cond_7
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    goto :goto_6

    .line 1498
    :cond_8
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "NG"

    aput-object v5, v4, v6

    .line 1499
    iget-object v4, v3, Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    const-string v5, "0"

    aput-object v5, v4, v7

    goto/16 :goto_1

    .line 1362
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public returnMagneticExpansion_Yamaha(I)Lcom/sec/factory/modules/SensorReadManager$MagneticExpansionData;
    .locals 1
    .param p1, "testNo"    # I

    .prologue
    .line 1201
    const/4 v0, 0x0

    return-object v0
.end method

.method public returnProximity()[F
    .locals 3

    .prologue
    .line 663
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 664
    const-string v0, "SensorReadManager"

    const-string v1, "returnProximity"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Proximity:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Proximity:[F

    return-object v0
.end method

.method public returnTemperature()[F
    .locals 3

    .prologue
    .line 671
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 672
    const-string v0, "SensorReadManager"

    const-string v1, "returnTemperature"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Temperature:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Temperature:[F

    return-object v0
.end method

.method public returnUV()[F
    .locals 3

    .prologue
    .line 719
    sget v0, Lcom/sec/factory/modules/ModuleSensor;->LOG_LEVEL:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 720
    const-string v0, "SensorReadManager"

    const-string v1, "returnUV"

    sget-object v2, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_UV:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/modules/SensorReadManager;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_UV:[F

    return-object v0
.end method

.method public sensorOff([ILandroid/hardware/SensorManager;)V
    .locals 3
    .param p1, "sensorID"    # [I
    .param p2, "sm"    # Landroid/hardware/SensorManager;

    .prologue
    .line 108
    const-string v0, "SensorReadManager"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iput-object p2, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 110
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/factory/modules/SensorReadManager;->turnOnOffSensor([ILandroid/hardware/SensorManager;Z)Z

    .line 141
    return-void
.end method

.method public declared-synchronized turnOnOffSensor([ILandroid/hardware/SensorManager;Z)Z
    .locals 9
    .param p1, "sensorID"    # [I
    .param p2, "sm"    # Landroid/hardware/SensorManager;
    .param p3, "on"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 144
    monitor-enter p0

    :try_start_0
    const-string v6, "SensorReadManager"

    const-string v7, "turnOnOffSensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorID = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p1, :cond_2

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", on = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v2, -0x1

    .line 146
    .local v2, "sensorOnArrayIndex":I
    const/4 v1, 0x0

    .line 147
    .local v1, "mReturn":Z
    iput-object p2, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 149
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v5, :cond_30

    if-eqz p1, :cond_30

    .line 150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v5, p1

    if-ge v0, v5, :cond_31

    .line 151
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-eq v5, v6, :cond_0

    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-eq v5, v6, :cond_0

    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne v5, v6, :cond_a

    .line 154
    :cond_0
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 156
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_3

    .line 514
    .end local v0    # "i":I
    :cond_1
    :goto_2
    monitor-exit p0

    return v3

    .line 144
    .end local v1    # "mReturn":Z
    .end local v2    # "sensorOnArrayIndex":I
    :cond_2
    :try_start_1
    const-string v5, "null"

    goto :goto_0

    .line 159
    .restart local v0    # "i":I
    .restart local v1    # "mReturn":Z
    .restart local v2    # "sensorOnArrayIndex":I
    :cond_3
    if-eqz p3, :cond_7

    .line 160
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 162
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-AccelerometerSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    if-eqz v1, :cond_4

    .line 165
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne v5, v6, :cond_5

    .line 166
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 167
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    .line 150
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 168
    :cond_5
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne v5, v6, :cond_6

    .line 169
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 170
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 144
    .end local v0    # "i":I
    .end local v1    # "mReturn":Z
    .end local v2    # "sensorOnArrayIndex":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 171
    .restart local v0    # "i":I
    .restart local v1    # "mReturn":Z
    .restart local v2    # "sensorOnArrayIndex":I
    :cond_6
    :try_start_2
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne v5, v6, :cond_4

    .line 172
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 173
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto :goto_3

    .line 177
    :cond_7
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 178
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 179
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Accelerometer:[F

    .line 180
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-AccelerometerSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne v5, v6, :cond_8

    .line 183
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 184
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto :goto_3

    .line 185
    :cond_8
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne v5, v6, :cond_9

    .line 186
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 187
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto :goto_3

    .line 188
    :cond_9
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne v5, v6, :cond_4

    .line 189
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 190
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto :goto_3

    .line 193
    :cond_a
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne v5, v6, :cond_c

    .line 194
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 195
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 198
    if-eqz p3, :cond_b

    .line 199
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 201
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-BarometerSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    if-eqz v1, :cond_4

    .line 204
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 205
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 208
    :cond_b
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 209
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBarometerSensor:Landroid/hardware/Sensor;

    .line 210
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Barometer:[F

    .line 211
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-BarometerSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 213
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 215
    :cond_c
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne v5, v6, :cond_e

    .line 216
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 217
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 220
    if-eqz p3, :cond_d

    .line 221
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 223
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-GyroscopeSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    if-eqz v1, :cond_4

    .line 226
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 227
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 230
    :cond_d
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 231
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mGyroscopeSensor:Landroid/hardware/Sensor;

    .line 232
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Gyro:[F

    .line 233
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-GyroscopeSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 235
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 237
    :cond_e
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-eq v5, v6, :cond_f

    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    if-ne v5, v6, :cond_11

    .line 239
    :cond_f
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 240
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 243
    if-eqz p3, :cond_10

    .line 244
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 246
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-LightSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    if-eqz v1, :cond_4

    .line 249
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 250
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    .line 251
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 252
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 255
    :cond_10
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 256
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 257
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Light:[F

    .line 258
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-LightSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 260
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    .line 261
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_LIGHT_CCT:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 262
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 264
    :cond_11
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne v5, v6, :cond_13

    .line 265
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 266
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 269
    if-eqz p3, :cond_12

    .line 270
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 272
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-MagneticSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    if-eqz v1, :cond_4

    .line 275
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 276
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 279
    :cond_12
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 280
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 281
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Magnetic:[F

    .line 282
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-MagneticSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 284
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 286
    :cond_13
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne v5, v6, :cond_15

    .line 287
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 288
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 291
    if-eqz p3, :cond_14

    .line 292
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 294
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-ProximitySensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    if-eqz v1, :cond_4

    .line 297
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 298
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 301
    :cond_14
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 302
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 303
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Proximity:[F

    .line 304
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-ProximitySensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 306
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 308
    :cond_15
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    if-ne v5, v6, :cond_17

    .line 309
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mTemperatureSensor:Landroid/hardware/Sensor;

    .line 311
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mTemperatureSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 314
    if-eqz p3, :cond_16

    .line 315
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mTemperatureSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 317
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-Temperature, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    if-eqz v1, :cond_4

    .line 320
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 321
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 324
    :cond_16
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mTemperatureSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 325
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mTemperatureSensor:Landroid/hardware/Sensor;

    .line 326
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Temperature:[F

    .line 327
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-Temperature"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_TEMPERATURE:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 329
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 331
    :cond_17
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    if-ne v5, v6, :cond_19

    .line 332
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mHumiditySensor:Landroid/hardware/Sensor;

    .line 334
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mHumiditySensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 337
    if-eqz p3, :cond_18

    .line 338
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mHumiditySensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 340
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-Humidity, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    if-eqz v1, :cond_4

    .line 343
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 344
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 347
    :cond_18
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mHumiditySensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 348
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mHumiditySensor:Landroid/hardware/Sensor;

    .line 349
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Humidity:[F

    .line 350
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-Humidity"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_HUMIDITY:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 352
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 354
    :cond_19
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    if-ne v5, v6, :cond_1b

    .line 355
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v6, 0x10018

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mGripSensor:Landroid/hardware/Sensor;

    .line 356
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mGripSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 359
    if-eqz p3, :cond_1a

    .line 360
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 362
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-mGripSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    if-eqz v1, :cond_4

    .line 365
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 366
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 369
    :cond_1a
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mGripSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 370
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mGripSensor:Landroid/hardware/Sensor;

    .line 371
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Grip:[F

    .line 372
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-mGripSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GRIP:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 374
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 376
    :cond_1b
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    if-ne v5, v6, :cond_1d

    .line 377
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v6, 0x10019

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBioSensor:Landroid/hardware/Sensor;

    .line 378
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBioSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 381
    if-eqz p3, :cond_1c

    .line 382
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mBioSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 384
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-mBioSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    if-eqz v1, :cond_4

    .line 387
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 388
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 391
    :cond_1c
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mBioSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 392
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBioSensor:Landroid/hardware/Sensor;

    .line 393
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Bio:[F

    .line 394
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-mBioSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 396
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 398
    :cond_1d
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    if-ne v5, v6, :cond_1f

    .line 399
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v6, 0x1001a

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mHrmSensor:Landroid/hardware/Sensor;

    .line 400
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mHrmSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 403
    if-eqz p3, :cond_1e

    .line 404
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mHrmSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 406
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-mHrmSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    if-eqz v1, :cond_4

    .line 409
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 410
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 413
    :cond_1e
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mHrmSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 414
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_Hrm:[F

    .line 415
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mHrmSensor:Landroid/hardware/Sensor;

    .line 416
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-mHrmSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BIO_HRM:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 418
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 422
    :cond_1f
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BODYTEMP:I

    if-ne v5, v6, :cond_21

    .line 423
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v6, 0x1001e

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBodyTempSensor:Landroid/hardware/Sensor;

    .line 424
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBodyTempSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 427
    if-eqz p3, :cond_20

    .line 428
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mBodyTempSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 429
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-mBodyTempSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    if-eqz v1, :cond_4

    .line 431
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BODYTEMP:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 432
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 435
    :cond_20
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mBodyTempSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 436
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_BodyTemp:[F

    .line 437
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mBodyTempSensor:Landroid/hardware/Sensor;

    .line 438
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-mBodyTempSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BODYTEMP:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 440
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 443
    :cond_21
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    if-ne v5, v6, :cond_23

    .line 444
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    const v6, 0x1001d

    invoke-virtual {v5, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mUVSensor:Landroid/hardware/Sensor;

    .line 445
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mUVSensor:Landroid/hardware/Sensor;

    if-eqz v5, :cond_1

    .line 448
    if-eqz p3, :cond_22

    .line 449
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mUVSensor:Landroid/hardware/Sensor;

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 450
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "register-mUVSensor, mReturn : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    if-eqz v1, :cond_4

    .line 452
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 453
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 456
    :cond_22
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v6, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorListener:Lcom/sec/factory/modules/SensorReadManager$SensorListener;

    iget-object v7, p0, Lcom/sec/factory/modules/SensorReadManager;->mUVSensor:Landroid/hardware/Sensor;

    invoke-virtual {v5, v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 457
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/factory/modules/SensorReadManager;->mBuffer_SensorValue_UV:[F

    .line 458
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mUVSensor:Landroid/hardware/Sensor;

    .line 459
    const-string v5, "SensorReadManager"

    const-string v6, "SensorReadManager"

    const-string v7, "unregister-mUVSensor"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_UV:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 461
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    const/4 v6, 0x0

    aput-boolean v6, v5, v2

    goto/16 :goto_3

    .line 465
    :cond_23
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne v5, v6, :cond_24

    .line 466
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 467
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 468
    :cond_24
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    if-ne v5, v6, :cond_25

    .line 469
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 470
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 471
    :cond_25
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne v5, v6, :cond_26

    .line 472
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 473
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 474
    :cond_26
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne v5, v6, :cond_27

    .line 475
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 476
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 479
    :cond_27
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne v5, v6, :cond_28

    .line 480
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 481
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 482
    :cond_28
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne v5, v6, :cond_29

    .line 483
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 484
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 485
    :cond_29
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne v5, v6, :cond_2a

    .line 486
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 487
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 488
    :cond_2a
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne v5, v6, :cond_2b

    .line 489
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 490
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 491
    :cond_2b
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne v5, v6, :cond_2c

    .line 492
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 493
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 494
    :cond_2c
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne v5, v6, :cond_2d

    .line 495
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 496
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 497
    :cond_2d
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne v5, v6, :cond_2e

    .line 498
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 499
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 500
    :cond_2e
    aget v5, p1, v0

    sget v6, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne v5, v6, :cond_2f

    .line 501
    sget v5, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    invoke-direct {p0, v5}, Lcom/sec/factory/modules/SensorReadManager;->ConverterID(I)I

    move-result v2

    .line 502
    iget-object v5, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    aput-boolean p3, v5, v2

    goto/16 :goto_3

    .line 504
    :cond_2f
    const-string v4, "SensorReadManager"

    const-string v5, "turnOnOffSensor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unregistered-ETC(ModuleSensor ID) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, p1, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 510
    .end local v0    # "i":I
    :cond_30
    const-string v4, "SensorReadManager"

    const-string v5, "turnOnOffSensor"

    const-string v6, "SensorManager null !!!"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 513
    .restart local v0    # "i":I
    :cond_31
    const-string v5, "SensorReadManager"

    const-string v6, "turnOnOffSensor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    if-eqz v3, :cond_32

    iget-object v3, p0, Lcom/sec/factory/modules/SensorReadManager;->mSensorOn:[Z

    invoke-static {v3}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v6, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 514
    goto/16 :goto_2

    .line 513
    :cond_32
    const-string v3, "null"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method
