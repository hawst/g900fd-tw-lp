.class public Lcom/sec/factory/modules/ModuleCommunicationService;
.super Landroid/app/Service;
.source "ModuleCommunicationService.java"


# static fields
.field protected static final ACTION_GPS_SERVICE_RESPONSE:Ljava/lang/String; = "com.sec.factory.intent.ACTION_GPS_SERVICE_RESPONSE"

.field public static final ACTION_GPS_START:Ljava/lang/String; = "ACTION_GPS_START"

.field public static final ACTION_GPS_START_DELAY:Ljava/lang/String; = "ACTION_GPS_START_DELAY"

.field public static final ACTION_NFC_OFF:Ljava/lang/String; = "ACTION_NFC_OFF"

.field public static final ACTION_NFC_ON:Ljava/lang/String; = "ACTION_NFC_ON"

.field public static final ACTION_SMD_START:Ljava/lang/String; = "ACTION_SMD_START"

.field public static final ACTION_SMD_START_DELAY:Ljava/lang/String; = "ACTION_SMD_START_DELAY"

.field private static final CLASS_NAME:Ljava/lang/String; = "ModuleCommunicationService"

.field public static GPS_MODE:Ljava/lang/String; = null

.field private static MAX_RETRY:I = 0x0

.field private static final MAX_RETRY_GPS:I = 0xb

.field private static final MAX_RETRY_SMD:I = 0x14

.field private static RETRY_INTERVAL:I = 0x0

.field private static final RETRY_INTERVAL_GPS:I = 0x1f4

.field private static final RETRY_INTERVAL_SMD:I = 0x12c

.field public static final SMD_NOK:Ljava/lang/String; = "-1"

.field private static mNFCTurnningOnCount:I

.field private static mNfcAdapter:Landroid/nfc/NfcAdapter;


# instance fields
.field private final MSG_GPS_READ:B

.field private final MSG_GPS_START:B

.field private final MSG_GPS_START_DELAY:B

.field private final MSG_SMD_START:B

.field private final MSG_SMD_START_DELAY:B

.field private final NFC_ENABLE_DELAY:B

.field private mData:Ljava/lang/String;

.field private mGpsRetryCount:I

.field private mHandler:Landroid/os/Handler;

.field private mLocationListener:Landroid/location/LocationListener;

.field private mLocationManager:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    sput v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    .line 317
    const/16 v0, 0xa

    sput v0, Lcom/sec/factory/modules/ModuleCommunicationService;->MAX_RETRY:I

    .line 320
    const/16 v0, 0x1f4

    sput v0, Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I

    .line 324
    const-string v0, "GPS"

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 35
    new-instance v0, Lcom/sec/factory/modules/ModuleCommunicationService$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommunicationService$1;-><init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    .line 216
    const/16 v0, 0xf

    iput-byte v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->NFC_ENABLE_DELAY:B

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;

    .line 287
    new-instance v0, Lcom/sec/factory/modules/ModuleCommunicationService$4;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommunicationService$4;-><init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationListener:Landroid/location/LocationListener;

    .line 310
    const/16 v0, 0xa

    iput-byte v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->MSG_GPS_START:B

    .line 311
    const/16 v0, 0xb

    iput-byte v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->MSG_SMD_START:B

    .line 313
    const/16 v0, 0xc

    iput-byte v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->MSG_GPS_READ:B

    .line 314
    const/16 v0, 0xd

    iput-byte v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->MSG_GPS_START_DELAY:B

    .line 315
    const/16 v0, 0xe

    iput-byte v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->MSG_SMD_START_DELAY:B

    .line 326
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 31
    sput p0, Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I

    return p0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/sec/factory/modules/ModuleCommunicationService;->MAX_RETRY:I

    return v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 31
    sput p0, Lcom/sec/factory/modules/ModuleCommunicationService;->MAX_RETRY:I

    return p0
.end method

.method static synthetic access$200(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/factory/modules/ModuleCommunicationService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/factory/modules/ModuleCommunicationService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I

    return p1
.end method

.method static synthetic access$508(Lcom/sec/factory/modules/ModuleCommunicationService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleCommunicationService;->gpsResponse(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/factory/modules/ModuleCommunicationService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleCommunicationService;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunicationService;->nfcEnable()V

    return-void
.end method

.method static synthetic access$900()Landroid/nfc/NfcAdapter;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    return-object v0
.end method

.method private gpsResponse(Ljava/lang/String;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 330
    const-string v1, "ModuleCommunicationService"

    const-string v2, "gpsResponse"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result  = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.intent.ACTION_GPS_SERVICE_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 332
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 334
    return-void
.end method

.method private nfcDisable()V
    .locals 4

    .prologue
    .line 255
    const-string v0, "ModuleCommunicationService"

    const-string v1, "nfcDisable"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    sget v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    .line 258
    sget v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    if-lez v0, :cond_0

    .line 259
    const-string v0, "ModuleCommunicationService"

    const-string v1, "nfcDisable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mNFCTurnningOnCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_0
    return-void

    .line 263
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunicationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 265
    sget-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isAllEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    new-instance v0, Lcom/sec/factory/modules/ModuleCommunicationService$3;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommunicationService$3;-><init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommunicationService$3;->start()V

    goto :goto_0

    .line 278
    :cond_1
    const-string v0, "ModuleCommunicationService"

    const-string v1, "nfcDisable"

    const-string v2, "NFC state is already off."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private nfcEnable()V
    .locals 3

    .prologue
    .line 220
    const-string v0, "ModuleCommunicationService"

    const-string v1, "nfcEnable"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    sget v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    .line 222
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunicationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 224
    sget-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    const-string v0, "ModuleCommunicationService"

    const-string v1, "nfcEnable"

    const-string v2, "NFC is not enabled."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.nfctest.NFC_ON_NO_DISCOVERY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :goto_0
    new-instance v0, Lcom/sec/factory/modules/ModuleCommunicationService$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleCommunicationService$2;-><init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommunicationService$2;->start()V

    .line 252
    :goto_1
    return-void

    .line 248
    :cond_0
    sget v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNFCTurnningOnCount:I

    .line 249
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.nfctest.NFC_DISCOVERY_DISABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 250
    const-string v0, "ModuleCommunicationService"

    const-string v1, "nfcEnable"

    const-string v2, "NFC is enabled."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 232
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 153
    const-string v0, "ModuleCommunicationService"

    const-string v1, "onCreate()"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 156
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleCommunicationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 158
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;

    .line 159
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 202
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 203
    const-string v0, "ModuleCommunicationService"

    const-string v1, "onDestroy()"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/16 v8, 0xe

    const/16 v7, 0xd

    const/16 v6, 0xb

    const/16 v5, 0xa

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "action":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 165
    const-string v1, "action"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    :cond_0
    if-eqz v0, :cond_1

    .line 169
    const-string v1, "ModuleCommunicationService"

    const-string v2, "onStartCommand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v1, "ACTION_NFC_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunicationService;->nfcEnable()V

    .line 195
    :cond_1
    :goto_0
    const/4 v1, 0x2

    return v1

    .line 173
    :cond_2
    const-string v1, "ACTION_NFC_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 174
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleCommunicationService;->nfcDisable()V

    goto :goto_0

    .line 177
    :cond_3
    const-string v1, "ACTION_GPS_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 178
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 179
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 180
    :cond_4
    const-string v1, "ACTION_GPS_START_DELAY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 181
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 182
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 183
    :cond_5
    const-string v1, "ACTION_SMD_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 184
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 185
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 186
    :cond_6
    const-string v1, "ACTION_SMD_START_DELAY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 188
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
