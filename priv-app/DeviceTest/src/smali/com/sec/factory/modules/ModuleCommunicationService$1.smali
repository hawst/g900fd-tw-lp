.class Lcom/sec/factory/modules/ModuleCommunicationService$1;
.super Landroid/os/Handler;
.source "ModuleCommunicationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleCommunicationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleCommunicationService;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0xd

    const/16 v12, 0xb

    const/4 v4, 0x0

    const-wide/16 v2, 0x3e8

    const/16 v11, 0xc

    .line 37
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 139
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "Wrong MSG"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 41
    :pswitch_0
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v5, "MSG_GPS_START"

    invoke-static {v0, v1, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v0, "GPS"

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    .line 43
    const/16 v0, 0x1f4

    # setter for: Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$002(I)I

    .line 44
    # setter for: Lcom/sec/factory/modules/ModuleCommunicationService;->MAX_RETRY:I
    invoke-static {v12}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$102(I)I

    .line 45
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$300(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "gps"

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationListener:Landroid/location/LocationListener;
    invoke-static {v5}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$200(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 47
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 48
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 51
    :pswitch_1
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "MSG_GPS_START_DELAY"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 53
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 56
    :pswitch_2
    const-string v0, "SMD"

    sput-object v0, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    .line 57
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v5, "MSG_SMD_START"

    invoke-static {v0, v1, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const/16 v0, 0x12c

    # setter for: Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$002(I)I

    .line 59
    const/16 v0, 0x14

    # setter for: Lcom/sec/factory/modules/ModuleCommunicationService;->MAX_RETRY:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$102(I)I

    .line 60
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$300(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "gps"

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationListener:Landroid/location/LocationListener;
    invoke-static {v5}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$200(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 62
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 63
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 66
    :pswitch_3
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "MSG_SMD_START_DELAY"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 68
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 71
    :pswitch_4
    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$000()I

    move-result v0

    const/16 v1, 0x12c

    if-ne v0, v1, :cond_1

    const-string v10, "SMD"

    .line 72
    .local v10, "msg_type":Ljava/lang/String;
    :goto_1
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHandler : MESSAGE_GPS_READ for ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") mGpsRetryCount = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$500(Lcom/sec/factory/modules/ModuleCommunicationService;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/4 v6, 0x0

    .line 76
    .local v6, "buf":Ljava/io/BufferedReader;
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$500(Lcom/sec/factory/modules/ModuleCommunicationService;)I

    move-result v0

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->MAX_RETRY:I
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$100()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 77
    const-string v0, "SMD"

    sget-object v1, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    const-string v1, "-1"

    # invokes: Lcom/sec/factory/modules/ModuleCommunicationService;->gpsResponse(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$600(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)V

    .line 79
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "mHandler : we can not get SMD responsed value!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :goto_2
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$502(Lcom/sec/factory/modules/ModuleCommunicationService;I)I

    .line 88
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 89
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$300(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mLocationListener:Landroid/location/LocationListener;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$200(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/location/LocationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 90
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->deleteGpsFiles()V

    goto/16 :goto_0

    .line 71
    .end local v6    # "buf":Ljava/io/BufferedReader;
    .end local v10    # "msg_type":Ljava/lang/String;
    :cond_1
    const-string v10, "GPS"

    goto :goto_1

    .line 82
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    .restart local v10    # "msg_type":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    const-string v1, "NG_CN0"

    # invokes: Lcom/sec/factory/modules/ModuleCommunicationService;->gpsResponse(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$600(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)V

    .line 83
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "mHandler : we can not get C/No!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 95
    :cond_3
    :try_start_0
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    const-string v1, "/data/sv_cno.info"

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x1fa0

    invoke-direct {v7, v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    .end local v6    # "buf":Ljava/io/BufferedReader;
    .local v7, "buf":Ljava/io/BufferedReader;
    :try_start_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$702(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 100
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # operator++ for: Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$508(Lcom/sec/factory/modules/ModuleCommunicationService;)I

    .line 101
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$000()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 124
    :cond_5
    :goto_3
    if-eqz v7, :cond_9

    .line 126
    :try_start_2
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    move-object v6, v7

    .line 129
    .end local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_0

    .line 103
    .end local v6    # "buf":Ljava/io/BufferedReader;
    .restart local v7    # "buf":Ljava/io/BufferedReader;
    :cond_6
    :try_start_3
    const-string v0, "SMD"

    sget-object v1, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 104
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GPS SMD CONFIRM with responsed data = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->deleteGpsFiles()V

    .line 107
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/factory/modules/ModuleCommunicationService;->gpsResponse(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$600(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    .line 113
    :catch_0
    move-exception v9

    move-object v6, v7

    .line 114
    .end local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    .local v9, "ex":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_4
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "C/No is not detection yet, try again after 500ms"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # operator++ for: Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$508(Lcom/sec/factory/modules/ModuleCommunicationService;)I

    .line 117
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$000()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 124
    if-eqz v6, :cond_0

    .line 126
    :try_start_5
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 127
    :catch_1
    move-exception v8

    .line 128
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 108
    .end local v6    # "buf":Ljava/io/BufferedReader;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "ex":Ljava/io/FileNotFoundException;
    .restart local v7    # "buf":Ljava/io/BufferedReader;
    :cond_7
    :try_start_6
    const-string v0, "GPS"

    sget-object v1, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 109
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GPS START CONFIRM with C/No = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mData:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$700(Lcom/sec/factory/modules/ModuleCommunicationService;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/factory/modules/ModuleCommunicationService;->gpsResponse(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$600(Lcom/sec/factory/modules/ModuleCommunicationService;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_3

    .line 119
    :catch_2
    move-exception v8

    move-object v6, v7

    .line 120
    .end local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    .restart local v8    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # operator++ for: Lcom/sec/factory/modules/ModuleCommunicationService;->mGpsRetryCount:I
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$508(Lcom/sec/factory/modules/ModuleCommunicationService;)I

    .line 121
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$400(Lcom/sec/factory/modules/ModuleCommunicationService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->RETRY_INTERVAL:I
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$000()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 124
    if-eqz v6, :cond_0

    .line 126
    :try_start_8
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    .line 127
    :catch_3
    move-exception v8

    .line 128
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 127
    .end local v6    # "buf":Ljava/io/BufferedReader;
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v7    # "buf":Ljava/io/BufferedReader;
    :catch_4
    move-exception v8

    .line 128
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    move-object v6, v7

    .line 129
    .end local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_0

    .line 124
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v6, :cond_8

    .line 126
    :try_start_9
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 129
    :cond_8
    :goto_7
    throw v0

    .line 127
    :catch_5
    move-exception v8

    .line 128
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 135
    .end local v6    # "buf":Ljava/io/BufferedReader;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "msg_type":Ljava/lang/String;
    :pswitch_5
    const-string v0, "ModuleCommunicationService"

    const-string v1, "mHandler.handleMessage"

    const-string v2, "NFC_ENABLE_DELAY"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleCommunicationService$1;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    # invokes: Lcom/sec/factory/modules/ModuleCommunicationService;->nfcEnable()V
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$800(Lcom/sec/factory/modules/ModuleCommunicationService;)V

    goto/16 :goto_0

    .line 124
    .restart local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v10    # "msg_type":Ljava/lang/String;
    :catchall_1
    move-exception v0

    move-object v6, v7

    .end local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    goto :goto_6

    .line 119
    :catch_6
    move-exception v8

    goto :goto_5

    .line 113
    :catch_7
    move-exception v9

    goto/16 :goto_4

    .end local v6    # "buf":Ljava/io/BufferedReader;
    .restart local v7    # "buf":Ljava/io/BufferedReader;
    :cond_9
    move-object v6, v7

    .end local v7    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_0

    .line 37
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
