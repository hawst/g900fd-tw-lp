.class public Lcom/sec/factory/modules/SensorSpec$Range;
.super Ljava/lang/Object;
.source "SensorSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Range"
.end annotation


# instance fields
.field public mIsSupportRange1_X:Z

.field public mIsSupportRange1_Y:Z

.field public mIsSupportRange1_Z:Z

.field public mIsSupportRange2_X:Z

.field public mIsSupportRange2_Y:Z

.field public mIsSupportRange2_Z:Z

.field public mRange1_X_Max:I

.field public mRange1_X_Min:I

.field public mRange1_Y_Max:I

.field public mRange1_Y_Min:I

.field public mRange1_Z_Max:I

.field public mRange1_Z_Min:I

.field public mRange2_X_Max:I

.field public mRange2_X_Min:I

.field public mRange2_Y_Max:I

.field public mRange2_Y_Min:I

.field public mRange2_Z_Max:I

.field public mRange2_Z_Min:I

.field public mRangeCount:I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 3
    .param p1, "xMin"    # I
    .param p2, "xMax"    # I
    .param p3, "yMin"    # I
    .param p4, "yMax"    # I
    .param p5, "zMin"    # I
    .param p6, "zMax"    # I

    .prologue
    const/4 v2, 0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    .line 38
    iput p2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    .line 39
    iput p3, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    .line 40
    iput p4, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    .line 41
    iput p5, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    .line 42
    iput p6, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    .line 43
    iput v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRangeCount:I

    .line 45
    iget v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Min:I

    iget v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_X_Max:I

    if-eq v0, v1, :cond_0

    .line 46
    iput-boolean v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_X:Z

    .line 49
    :cond_0
    iget v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Min:I

    iget v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Y_Max:I

    if-eq v0, v1, :cond_1

    .line 50
    iput-boolean v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Y:Z

    .line 53
    :cond_1
    iget v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Min:I

    iget v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange1_Z_Max:I

    if-eq v0, v1, :cond_2

    .line 54
    iput-boolean v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange1_Z:Z

    .line 56
    :cond_2
    return-void
.end method

.method public constructor <init>(IIIIIIIIIIII)V
    .locals 3
    .param p1, "range1_X_Min"    # I
    .param p2, "range1_X_Max"    # I
    .param p3, "range1_Y_Min"    # I
    .param p4, "range1_Y_Max"    # I
    .param p5, "range1_Z_Min"    # I
    .param p6, "range1_Z_Max"    # I
    .param p7, "range2_X_Min"    # I
    .param p8, "range2_X_Max"    # I
    .param p9, "range2_Y_Min"    # I
    .param p10, "range2_Y_Max"    # I
    .param p11, "range2_Z_Min"    # I
    .param p12, "range2_Z_Max"    # I

    .prologue
    const/4 v2, 0x1

    .line 61
    invoke-direct/range {p0 .. p6}, Lcom/sec/factory/modules/SensorSpec$Range;-><init>(IIIIII)V

    .line 62
    iput p7, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Min:I

    .line 63
    iput p8, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Max:I

    .line 64
    iput p9, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Min:I

    .line 65
    iput p10, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Max:I

    .line 66
    iput p11, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Min:I

    .line 67
    iput p12, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Max:I

    .line 68
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRangeCount:I

    .line 70
    iget v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Min:I

    iget v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_X_Max:I

    if-eq v0, v1, :cond_0

    .line 71
    iput-boolean v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_X:Z

    .line 74
    :cond_0
    iget v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Min:I

    iget v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Y_Max:I

    if-eq v0, v1, :cond_1

    .line 75
    iput-boolean v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Y:Z

    .line 78
    :cond_1
    iget v0, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Min:I

    iget v1, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mRange2_Z_Max:I

    if-eq v0, v1, :cond_2

    .line 79
    iput-boolean v2, p0, Lcom/sec/factory/modules/SensorSpec$Range;->mIsSupportRange2_Z:Z

    .line 81
    :cond_2
    return-void
.end method
