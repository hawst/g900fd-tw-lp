.class public Lcom/sec/factory/modules/ModuleDFT;
.super Lcom/sec/factory/modules/ModuleObject;
.source "ModuleDFT.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;
    }
.end annotation


# static fields
.field private static final CAMERA_LAUNCH_ACTION:Ljava/lang/String; = "android.media.action.IMAGE_CAPTURE"

.field private static final KEYCODE_NETWORK_SELECT:I = 0xe8

.field private static mInstance:Lcom/sec/factory/modules/ModuleDFT;


# instance fields
.field private mDownTime:J

.field private mIsSlateCommand:Z

.field private mProduct:Ljava/lang/String;

.field private mTouchLongX1:F

.field private mTouchLongY1:F

.field private mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

.field private x1:Ljava/lang/String;

.field private y1:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/modules/ModuleDFT;->mInstance:Lcom/sec/factory/modules/ModuleDFT;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const-string v0, "ModuleDFT"

    invoke-direct {p0, p1, v0}, Lcom/sec/factory/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    .line 42
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->INIT:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    .line 43
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mDownTime:J

    .line 48
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleDFT"

    const-string v2, "Create ModuleDFT"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method private ActionDFTTouch(Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 25
    .param p1, "action_type"    # Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;
    .param p2, "x1_value"    # Ljava/lang/String;
    .param p3, "y1_value"    # Ljava/lang/String;
    .param p4, "x2_value"    # Ljava/lang/String;
    .param p5, "y2_value"    # Ljava/lang/String;

    .prologue
    .line 290
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "ActionDFTTouch"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "type="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", x1="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", y1="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", x2="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", y2="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v4, "window"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/factory/modules/ModuleDFT;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v18

    .line 294
    .local v18, "display":Landroid/view/Display;
    new-instance v22, Landroid/graphics/Point;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Point;-><init>()V

    .line 295
    .local v22, "outpoint":Landroid/graphics/Point;
    invoke-virtual/range {v18 .. v18}, Landroid/view/Display;->getWidth()I

    move-result v4

    move-object/from16 v0, v22

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 296
    invoke-virtual/range {v18 .. v18}, Landroid/view/Display;->getHeight()I

    move-result v4

    move-object/from16 v0, v22

    iput v4, v0, Landroid/graphics/Point;->y:I

    .line 297
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "ActionDFTTouch"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Display w="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v22

    iget v9, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ", h="

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v22

    iget v9, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :try_start_0
    invoke-static/range {p2 .. p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    .line 301
    .local v7, "x1":F
    invoke-static/range {p3 .. p3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    .line 302
    .local v8, "y1":F
    invoke-static/range {p4 .. p4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v15

    .line 303
    .local v15, "x2":F
    invoke-static/range {p5 .. p5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v16

    .line 304
    .local v16, "y2":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "ActionDFTTouch"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "x1"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "y1"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "x2"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "y2"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 308
    .local v2, "eventTime":J
    sget-object v4, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->DOWN:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_1

    .line 309
    const/4 v6, 0x0

    const/4 v9, 0x0

    move-wide v4, v2

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v19

    .line 311
    .local v19, "down":Landroid/view/MotionEvent;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v4}, Lcom/sec/factory/modules/ModuleDFT;->injectTouchEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    .line 351
    .end local v2    # "eventTime":J
    .end local v7    # "x1":F
    .end local v8    # "y1":F
    .end local v15    # "x2":F
    .end local v16    # "y2":F
    .end local v19    # "down":Landroid/view/MotionEvent;
    :cond_0
    :goto_0
    return-void

    .line 312
    .restart local v2    # "eventTime":J
    .restart local v7    # "x1":F
    .restart local v8    # "y1":F
    .restart local v15    # "x2":F
    .restart local v16    # "y2":F
    :cond_1
    sget-object v4, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->UP:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_2

    .line 313
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/factory/modules/ModuleDFT;->mDownTime:J

    const/4 v14, 0x1

    const/16 v17, 0x0

    move-wide v12, v2

    invoke-static/range {v10 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v24

    .line 315
    .local v24, "up":Landroid/view/MotionEvent;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v4}, Lcom/sec/factory/modules/ModuleDFT;->injectTouchEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 346
    .end local v2    # "eventTime":J
    .end local v7    # "x1":F
    .end local v8    # "y1":F
    .end local v15    # "x2":F
    .end local v16    # "y2":F
    .end local v24    # "up":Landroid/view/MotionEvent;
    :catch_0
    move-exception v21

    .line 347
    .local v21, "nfe":Ljava/lang/NumberFormatException;
    const-string p2, "0000"

    .line 348
    const-string p3, "0000"

    .line 350
    goto :goto_0

    .line 316
    .end local v21    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v2    # "eventTime":J
    .restart local v7    # "x1":F
    .restart local v8    # "y1":F
    .restart local v15    # "x2":F
    .restart local v16    # "y2":F
    :cond_2
    :try_start_1
    sget-object v4, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->LONG_PRESS:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_3

    .line 317
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "ActionDFTTouch"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Touch Event = down = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " , "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/factory/modules/ModuleDFT;->mTouchLongX1:F

    .line 321
    move-object/from16 v0, p0

    iput v8, v0, Lcom/sec/factory/modules/ModuleDFT;->mTouchLongY1:F

    .line 322
    new-instance v23, Lcom/sec/factory/modules/ModuleDFT$3;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/factory/modules/ModuleDFT$3;-><init>(Lcom/sec/factory/modules/ModuleDFT;)V

    .line 340
    .local v23, "thread":Ljava/lang/Thread;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 349
    .end local v2    # "eventTime":J
    .end local v7    # "x1":F
    .end local v8    # "y1":F
    .end local v15    # "x2":F
    .end local v16    # "y2":F
    .end local v23    # "thread":Ljava/lang/Thread;
    :catch_1
    move-exception v4

    goto :goto_0

    .line 341
    .restart local v2    # "eventTime":J
    .restart local v7    # "x1":F
    .restart local v8    # "y1":F
    .restart local v15    # "x2":F
    .restart local v16    # "y2":F
    :cond_3
    sget-object v4, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->MOVE:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_0

    .line 342
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/factory/modules/ModuleDFT;->mDownTime:J

    const/4 v14, 0x2

    const/16 v17, 0x0

    move-wide v12, v2

    invoke-static/range {v10 .. v17}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v20

    .line 344
    .local v20, "move":Landroid/view/MotionEvent;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v4}, Lcom/sec/factory/modules/ModuleDFT;->injectTouchEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0
.end method

.method private ActionDFTkey(IZ)V
    .locals 25
    .param p1, "keyCode"    # I
    .param p2, "isKeyHold"    # Z

    .prologue
    .line 83
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 84
    .local v4, "eventTime":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey start"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "keyCode="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", hold="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const/4 v6, 0x6

    move/from16 v0, p1

    if-ne v0, v6, :cond_0

    const/4 v6, 0x1

    move/from16 v0, p2

    if-eq v0, v6, :cond_1

    :cond_0
    const/16 v6, 0x1a

    move/from16 v0, p1

    if-ne v0, v6, :cond_3

    const/4 v6, 0x1

    move/from16 v0, p2

    if-ne v0, v6, :cond_3

    .line 87
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    const-string v9, "KEYCODE_ENDCALL, KEYCODE_POWER hold=true"

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    new-instance v20, Landroid/content/Intent;

    const-string v6, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v20, "i":Landroid/content/Intent;
    const-string v6, "android.intent.extra.KEY_CONFIRM"

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    const/high16 v6, 0x10000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 91
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/factory/modules/ModuleDFT;->startActivity(Landroid/content/Intent;)V

    .line 180
    .end local v20    # "i":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 92
    :cond_3
    const/4 v6, 0x6

    move/from16 v0, p1

    if-ne v0, v6, :cond_4

    if-nez p2, :cond_4

    .line 93
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    const-string v9, "KEYCODE_ENDCALL hold=false"

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v6, "phone"

    invoke-static {v6}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v22

    .line 97
    .local v22, "phoneServ":Lcom/android/internal/telephony/ITelephony;
    if-eqz v22, :cond_2

    .line 99
    :try_start_0
    invoke-interface/range {v22 .. v22}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v19

    .line 101
    .local v19, "e":Landroid/os/RemoteException;
    invoke-static/range {v19 .. v19}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 104
    .end local v19    # "e":Landroid/os/RemoteException;
    .end local v22    # "phoneServ":Lcom/android/internal/telephony/ITelephony;
    :cond_4
    const/16 v6, 0x41

    move/from16 v0, p1

    if-ne v0, v6, :cond_5

    if-nez p2, :cond_5

    .line 106
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    const-string v9, "KEYCODE_ENVELOPE hold=false"

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    new-instance v21, Landroid/content/Intent;

    const-string v6, "android.intent.action.SEND"

    move-object/from16 v0, v21

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 108
    .local v21, "intent":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 109
    const-string v6, "com.android.mms"

    const-string v8, "com.android.mms.ui.ConversationComposer"

    move-object/from16 v0, v21

    invoke-virtual {v0, v6, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/factory/modules/ModuleDFT;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 112
    .end local v21    # "intent":Landroid/content/Intent;
    :cond_5
    const/16 v6, 0x1b

    move/from16 v0, p1

    if-ne v0, v6, :cond_8

    .line 113
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    const-string v9, "KEYCODE_CAMERA"

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const/16 v18, 0x0

    .line 116
    .local v18, "activityManager":Landroid/app/ActivityManager;
    const-string v6, "activity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/factory/modules/ModuleDFT;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "activityManager":Landroid/app/ActivityManager;
    check-cast v18, Landroid/app/ActivityManager;

    .line 117
    .restart local v18    # "activityManager":Landroid/app/ActivityManager;
    const/4 v6, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v23

    .line 118
    .local v23, "runningtask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v6, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v24

    .line 119
    .local v24, "topActivity":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "topActivity = "

    move-object/from16 v0, v24

    invoke-static {v6, v8, v0}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v6, "com.sec.android.app.camera.Camera"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "com.android.camera.CameraActivity"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "com.android.camera.CameraLauncher"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 123
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Camera App is operating"

    const-string v9, "KEYCODE_CAMERA"

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    new-instance v3, Landroid/view/KeyEvent;

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-wide v6, v4

    move/from16 v9, p1

    invoke-direct/range {v3 .. v10}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 125
    .local v3, "down":Landroid/view/KeyEvent;
    new-instance v7, Landroid/view/KeyEvent;

    const/4 v12, 0x1

    const/4 v14, 0x0

    move-wide v8, v4

    move-wide v10, v4

    move/from16 v13, p1

    invoke-direct/range {v7 .. v14}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 126
    .local v7, "up":Landroid/view/KeyEvent;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7}, Lcom/sec/factory/modules/ModuleDFT;->injectKeyEvent(Landroid/view/KeyEvent;Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 128
    .end local v3    # "down":Landroid/view/KeyEvent;
    .end local v7    # "up":Landroid/view/KeyEvent;
    :cond_7
    new-instance v20, Landroid/content/Intent;

    const-string v6, "android.media.action.IMAGE_CAPTURE"

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 129
    .restart local v20    # "i":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 130
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/factory/modules/ModuleDFT;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 133
    .end local v18    # "activityManager":Landroid/app/ActivityManager;
    .end local v20    # "i":Landroid/content/Intent;
    .end local v23    # "runningtask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v24    # "topActivity":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Another key keyCode="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", hold="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/16 v18, 0x0

    .line 136
    .restart local v18    # "activityManager":Landroid/app/ActivityManager;
    const-string v6, "activity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/factory/modules/ModuleDFT;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "activityManager":Landroid/app/ActivityManager;
    check-cast v18, Landroid/app/ActivityManager;

    .line 137
    .restart local v18    # "activityManager":Landroid/app/ActivityManager;
    const/4 v6, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v23

    .line 138
    .restart local v23    # "runningtask":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v6, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v24

    .line 139
    .restart local v24    # "topActivity":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "topactivity call = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/16 v6, 0x1a

    move/from16 v0, p1

    if-ne v0, v6, :cond_9

    if-nez p2, :cond_9

    .line 142
    const-string v6, "com.sec.factory.app.ui.UIHardKey"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 143
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/modules/ModuleDFT;->isSleep()V

    goto/16 :goto_0

    .line 148
    :cond_9
    const/16 v6, 0x42

    move/from16 v0, p1

    if-ne v0, v6, :cond_c

    const-string v6, "com.android.dialer.DialtactsActivity"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    const-string v6, "com.android.contacts.DialtactsActivity"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    const-string v6, "com.android.contacts.activities.DialtactsActivity"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    const-string v6, "com.android.contacts.activities.PeopleActivity"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    const-string v6, "CONTACT_DIALTACTS_CLASS"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    if-nez v6, :cond_c

    .line 154
    new-instance v20, Landroid/content/Intent;

    const-string v6, "android.intent.action.DIAL"

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    .restart local v20    # "i":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 157
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v6

    if-nez v6, :cond_b

    .line 158
    const-string v6, "com.android.contacts"

    const-string v8, "com.android.contacts.activities.DialtactsActivity"

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    :cond_a
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/factory/modules/ModuleDFT;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 161
    :cond_b
    const-string v6, "USE_SPECIAL_CONTACT_DIALTACTS_PACKAGE"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_a

    .line 162
    const-string v6, "CONTACT_DIALTACTS_PACKAGE"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 163
    .local v17, "CONTACT_DIALTACTS_PACKAGE":Ljava/lang/String;
    const-string v6, "CONTACT_DIALTACTS_CLASS"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "CONTACT_DIALTACTS_CLASS":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CONTACT_DIALTACTS_PACKAGE = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CONTACT_DIALTACTS_CLASS = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 171
    .end local v2    # "CONTACT_DIALTACTS_CLASS":Ljava/lang/String;
    .end local v17    # "CONTACT_DIALTACTS_PACKAGE":Ljava/lang/String;
    .end local v20    # "i":Landroid/content/Intent;
    :cond_c
    if-nez p2, :cond_2

    .line 172
    new-instance v3, Landroid/view/KeyEvent;

    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object v9, v3

    move-wide v10, v4

    move-wide v12, v4

    move/from16 v15, p1

    invoke-direct/range {v9 .. v16}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 173
    .restart local v3    # "down":Landroid/view/KeyEvent;
    new-instance v7, Landroid/view/KeyEvent;

    const/4 v12, 0x1

    const/4 v14, 0x0

    move-wide v8, v4

    move-wide v10, v4

    move/from16 v13, p1

    invoke-direct/range {v7 .. v14}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 174
    .restart local v7    # "up":Landroid/view/KeyEvent;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "ActionDFTKey"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Inject keyevent: keycode"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7}, Lcom/sec/factory/modules/ModuleDFT;->injectKeyEvent(Landroid/view/KeyEvent;Landroid/view/KeyEvent;)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/factory/modules/ModuleDFT;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDFT;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mDownTime:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/ModuleDFT;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDFT;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchLongX1:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/factory/modules/ModuleDFT;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDFT;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchLongY1:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/factory/modules/ModuleDFT;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleDFT;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Landroid/view/MotionEvent;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/modules/ModuleDFT;->injectTouchEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private hexToDec(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 243
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x45800000    # 4096.0f

    mul-float/2addr v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x43800000    # 256.0f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x41800000    # 16.0f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v2

    int-to-float v2, v2

    add-float v0, v1, v2

    .line 247
    .local v0, "result":F
    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private injectKeyEvent(Landroid/view/KeyEvent;Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "event1"    # Landroid/view/KeyEvent;
    .param p2, "event2"    # Landroid/view/KeyEvent;

    .prologue
    .line 196
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleDFT$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/factory/modules/ModuleDFT$1;-><init>(Lcom/sec/factory/modules/ModuleDFT;Landroid/view/KeyEvent;Landroid/view/KeyEvent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 203
    return-void
.end method

.method private injectTouchEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "touch1"    # Landroid/view/MotionEvent;
    .param p2, "touch2"    # Landroid/view/MotionEvent;

    .prologue
    .line 206
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleDFT$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/factory/modules/ModuleDFT$2;-><init>(Lcom/sec/factory/modules/ModuleDFT;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDFT;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT;->mInstance:Lcom/sec/factory/modules/ModuleDFT;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/sec/factory/modules/ModuleDFT;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleDFT;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/factory/modules/ModuleDFT;->mInstance:Lcom/sec/factory/modules/ModuleDFT;

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT;->mInstance:Lcom/sec/factory/modules/ModuleDFT;

    return-object v0
.end method

.method private isSleep()V
    .locals 4

    .prologue
    .line 183
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/factory/modules/ModuleDFT;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 185
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 193
    :goto_0
    return-void

    .line 188
    :cond_0
    const v2, 0x3000001a

    const-string v3, "wake_up"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 191
    .local v1, "sCpuWakeLock":Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0
.end method

.method private keymapping(Ljava/lang/String;Z)I
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "isKeyHold"    # Z

    .prologue
    const/4 v7, 0x1

    .line 635
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 636
    .local v1, "eventCode":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "keymapping: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    const/4 v2, -0x1

    .line 638
    .local v2, "hostnumber":I
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    .line 639
    packed-switch v1, :pswitch_data_0

    .line 871
    :pswitch_0
    const/4 v2, 0x0

    .line 875
    :goto_0
    return v2

    .line 641
    :pswitch_1
    const/4 v2, 0x3

    .line 642
    goto :goto_0

    .line 644
    :pswitch_2
    const/4 v2, 0x4

    .line 645
    goto :goto_0

    .line 651
    :pswitch_3
    sget-object v3, Lcom/sec/factory/modules/ModuleDFT;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 652
    .local v0, "callstate":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "ActionDFTKey"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "callState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    if-eq v0, v7, :cond_0

    .line 654
    const/16 v2, 0x42

    goto :goto_0

    .line 657
    :cond_0
    const/4 v2, 0x5

    .line 659
    goto :goto_0

    .line 662
    .end local v0    # "callstate":I
    :pswitch_4
    const/4 v2, 0x6

    .line 663
    goto :goto_0

    .line 665
    :pswitch_5
    const/16 v2, 0x4d

    .line 666
    goto :goto_0

    .line 668
    :pswitch_6
    if-eqz p2, :cond_1

    const/16 v2, 0x51

    .line 669
    :goto_1
    goto :goto_0

    .line 668
    :cond_1
    const/4 v2, 0x7

    goto :goto_1

    .line 671
    :pswitch_7
    const/16 v2, 0x8

    .line 672
    goto :goto_0

    .line 674
    :pswitch_8
    const/16 v2, 0x9

    .line 675
    goto :goto_0

    .line 677
    :pswitch_9
    const/16 v2, 0xa

    .line 678
    goto :goto_0

    .line 680
    :pswitch_a
    const/16 v2, 0xb

    .line 681
    goto :goto_0

    .line 683
    :pswitch_b
    const/16 v2, 0xc

    .line 684
    goto :goto_0

    .line 686
    :pswitch_c
    const/16 v2, 0xd

    .line 687
    goto :goto_0

    .line 689
    :pswitch_d
    const/16 v2, 0xe

    .line 690
    goto :goto_0

    .line 692
    :pswitch_e
    const/16 v2, 0xf

    .line 693
    goto :goto_0

    .line 695
    :pswitch_f
    const/16 v2, 0x10

    .line 696
    goto :goto_0

    .line 698
    :pswitch_10
    const/16 v2, 0x12

    .line 699
    goto :goto_0

    .line 701
    :pswitch_11
    const/16 v2, 0x11

    .line 702
    goto :goto_0

    .line 704
    :pswitch_12
    const/16 v2, 0x13

    .line 705
    goto :goto_0

    .line 707
    :pswitch_13
    const/16 v2, 0x14

    .line 708
    goto :goto_0

    .line 710
    :pswitch_14
    const/16 v2, 0x15

    .line 711
    goto :goto_0

    .line 713
    :pswitch_15
    const/16 v2, 0x16

    .line 714
    goto :goto_0

    .line 716
    :pswitch_16
    iput-boolean v7, p0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    .line 717
    const/16 v2, 0x42

    .line 718
    goto :goto_0

    .line 720
    :pswitch_17
    iput-boolean v7, p0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    .line 721
    const/16 v2, 0x42

    .line 722
    goto :goto_0

    .line 724
    :pswitch_18
    const/16 v2, 0x18

    .line 725
    goto :goto_0

    .line 727
    :pswitch_19
    const/16 v2, 0x19

    .line 728
    goto :goto_0

    .line 731
    :pswitch_1a
    const/16 v2, 0x1a

    .line 732
    goto :goto_0

    .line 734
    :pswitch_1b
    const/16 v2, 0x1b

    .line 735
    goto/16 :goto_0

    .line 737
    :pswitch_1c
    const/16 v2, 0x43

    .line 738
    goto/16 :goto_0

    .line 740
    :pswitch_1d
    const-string v3, "SUPPORT_LISTEN_KEY"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v7, :cond_2

    .line 742
    const/16 v2, 0xbb

    goto/16 :goto_0

    .line 746
    :cond_2
    const/16 v2, 0x52

    .line 748
    goto/16 :goto_0

    .line 750
    :pswitch_1e
    const/4 v2, 0x4

    .line 751
    goto/16 :goto_0

    .line 753
    :pswitch_1f
    const/16 v2, 0x1d

    .line 754
    goto/16 :goto_0

    .line 756
    :pswitch_20
    const/16 v2, 0x1e

    .line 757
    goto/16 :goto_0

    .line 759
    :pswitch_21
    const/16 v2, 0x1f

    .line 760
    goto/16 :goto_0

    .line 762
    :pswitch_22
    const/16 v2, 0x20

    .line 763
    goto/16 :goto_0

    .line 765
    :pswitch_23
    const/16 v2, 0x21

    .line 766
    goto/16 :goto_0

    .line 768
    :pswitch_24
    const/16 v2, 0x22

    .line 769
    goto/16 :goto_0

    .line 771
    :pswitch_25
    const/16 v2, 0x23

    .line 772
    goto/16 :goto_0

    .line 774
    :pswitch_26
    const/16 v2, 0x24

    .line 775
    goto/16 :goto_0

    .line 777
    :pswitch_27
    const/16 v2, 0x25

    .line 778
    goto/16 :goto_0

    .line 780
    :pswitch_28
    const/16 v2, 0x26

    .line 781
    goto/16 :goto_0

    .line 783
    :pswitch_29
    const/16 v2, 0x27

    .line 784
    goto/16 :goto_0

    .line 786
    :pswitch_2a
    const/16 v2, 0x28

    .line 787
    goto/16 :goto_0

    .line 789
    :pswitch_2b
    const/16 v2, 0x29

    .line 790
    goto/16 :goto_0

    .line 792
    :pswitch_2c
    const/16 v2, 0x2a

    .line 793
    goto/16 :goto_0

    .line 795
    :pswitch_2d
    const/16 v2, 0x2b

    .line 796
    goto/16 :goto_0

    .line 798
    :pswitch_2e
    const/16 v2, 0x2c

    .line 799
    goto/16 :goto_0

    .line 801
    :pswitch_2f
    const/16 v2, 0x2d

    .line 802
    goto/16 :goto_0

    .line 804
    :pswitch_30
    const/16 v2, 0x2e

    .line 805
    goto/16 :goto_0

    .line 807
    :pswitch_31
    const/16 v2, 0x2f

    .line 808
    goto/16 :goto_0

    .line 810
    :pswitch_32
    const/16 v2, 0x30

    .line 811
    goto/16 :goto_0

    .line 813
    :pswitch_33
    const/16 v2, 0x31

    .line 814
    goto/16 :goto_0

    .line 816
    :pswitch_34
    const/16 v2, 0x32

    .line 817
    goto/16 :goto_0

    .line 819
    :pswitch_35
    const/16 v2, 0x33

    .line 820
    goto/16 :goto_0

    .line 822
    :pswitch_36
    const/16 v2, 0x34

    .line 823
    goto/16 :goto_0

    .line 825
    :pswitch_37
    const/16 v2, 0x35

    .line 826
    goto/16 :goto_0

    .line 828
    :pswitch_38
    const/16 v2, 0x36

    .line 829
    goto/16 :goto_0

    .line 831
    :pswitch_39
    const/16 v2, 0x39

    .line 832
    goto/16 :goto_0

    .line 834
    :pswitch_3a
    const/16 v2, 0x3c

    .line 835
    goto/16 :goto_0

    .line 837
    :pswitch_3b
    const/16 v2, 0x3f

    .line 838
    goto/16 :goto_0

    .line 840
    :pswitch_3c
    const/16 v2, 0x40

    .line 841
    goto/16 :goto_0

    .line 843
    :pswitch_3d
    const/16 v2, 0x37

    .line 844
    goto/16 :goto_0

    .line 846
    :pswitch_3e
    const/16 v2, 0x3e

    .line 847
    goto/16 :goto_0

    .line 849
    :pswitch_3f
    const/16 v2, 0x38

    .line 850
    goto/16 :goto_0

    .line 852
    :pswitch_40
    const/16 v2, 0x112

    .line 853
    goto/16 :goto_0

    .line 855
    :pswitch_41
    const-string v3, "ro.product.model"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->mProduct:Ljava/lang/String;

    .line 856
    const-string v3, "SAMSUNG-SM-T217A"

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDFT;->mProduct:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 857
    const/16 v2, 0x41

    .line 858
    goto/16 :goto_0

    .line 860
    :cond_3
    const/4 v2, 0x0

    .line 861
    goto/16 :goto_0

    .line 865
    :pswitch_42
    const/16 v2, 0xbb

    .line 866
    goto/16 :goto_0

    .line 868
    :pswitch_43
    const/16 v2, 0x134

    .line 869
    goto/16 :goto_0

    .line 639
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_10
        :pswitch_11
        :pswitch_2
        :pswitch_43
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_14
        :pswitch_15
        :pswitch_1b
        :pswitch_16
        :pswitch_1d
        :pswitch_1e
        :pswitch_1a
        :pswitch_42
        :pswitch_1
        :pswitch_1a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3e
        :pswitch_0
        :pswitch_39
        :pswitch_17
        :pswitch_3f
        :pswitch_1c
        :pswitch_3d
        :pswitch_0
        :pswitch_3b
        :pswitch_40
        :pswitch_41
        :pswitch_3c
        :pswitch_3a
        :pswitch_42
    .end packed-switch
.end method

.method private keymappingCPO(Ljava/lang/String;Z)I
    .locals 10
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "isKeyHold"    # Z

    .prologue
    const/16 v4, 0x42

    const/4 v5, 0x5

    const/4 v9, 0x1

    .line 412
    const/16 v3, 0x10

    invoke-static {p1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 413
    .local v1, "eventCode":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "keymapping: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "keymapping: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "keymapping: "

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const/4 v2, -0x1

    .line 417
    .local v2, "hostnumber":I
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    .line 419
    sparse-switch v1, :sswitch_data_0

    .line 627
    const/4 v2, 0x0

    .line 631
    :goto_0
    return v2

    .line 421
    :sswitch_0
    const/4 v2, 0x3

    .line 422
    goto :goto_0

    .line 424
    :sswitch_1
    const/4 v2, 0x4

    .line 425
    goto :goto_0

    .line 427
    :sswitch_2
    sget-object v3, Lcom/sec/factory/modules/ModuleDFT;->mContext:Landroid/content/Context;

    const-string v6, "phone"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    .line 429
    .local v0, "callstate":I
    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "ActionDFTKey"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "callState = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    if-eq v0, v9, :cond_0

    move v2, v4

    .line 432
    :goto_1
    goto :goto_0

    :cond_0
    move v2, v5

    .line 430
    goto :goto_1

    .line 434
    .end local v0    # "callstate":I
    :sswitch_3
    const/4 v2, 0x6

    .line 435
    goto :goto_0

    .line 438
    :sswitch_4
    if-eqz p2, :cond_1

    const/16 v2, 0x51

    .line 439
    :goto_2
    goto :goto_0

    .line 438
    :cond_1
    const/4 v2, 0x7

    goto :goto_2

    .line 441
    :sswitch_5
    const/16 v2, 0x8

    .line 442
    goto :goto_0

    .line 444
    :sswitch_6
    const/16 v2, 0x9

    .line 445
    goto :goto_0

    .line 447
    :sswitch_7
    const/16 v2, 0xa

    .line 448
    goto :goto_0

    .line 450
    :sswitch_8
    const/16 v2, 0xb

    .line 451
    goto :goto_0

    .line 453
    :sswitch_9
    const/16 v2, 0xc

    .line 454
    goto :goto_0

    .line 456
    :sswitch_a
    const/16 v2, 0xd

    .line 457
    goto :goto_0

    .line 459
    :sswitch_b
    const/16 v2, 0xe

    .line 460
    goto :goto_0

    .line 462
    :sswitch_c
    const/16 v2, 0xf

    .line 463
    goto :goto_0

    .line 465
    :sswitch_d
    const/16 v2, 0x10

    .line 466
    goto :goto_0

    .line 468
    :sswitch_e
    const/16 v2, 0x12

    .line 469
    goto :goto_0

    .line 471
    :sswitch_f
    const/16 v2, 0x11

    .line 472
    goto :goto_0

    .line 474
    :sswitch_10
    const/16 v2, 0x13

    .line 475
    goto :goto_0

    .line 477
    :sswitch_11
    const/16 v2, 0x14

    .line 478
    goto :goto_0

    .line 480
    :sswitch_12
    const/16 v2, 0x15

    .line 481
    goto :goto_0

    .line 483
    :sswitch_13
    const/16 v2, 0x16

    .line 484
    goto :goto_0

    .line 487
    :sswitch_14
    iput-boolean v9, p0, Lcom/sec/factory/modules/ModuleDFT;->mIsSlateCommand:Z

    .line 488
    const/16 v2, 0x42

    .line 489
    goto :goto_0

    .line 492
    :sswitch_15
    const/16 v2, 0x18

    .line 493
    goto :goto_0

    .line 496
    :sswitch_16
    const/16 v2, 0x19

    .line 497
    goto :goto_0

    .line 500
    :sswitch_17
    const/16 v2, 0x1a

    .line 501
    goto :goto_0

    .line 503
    :sswitch_18
    const/16 v2, 0x1b

    .line 504
    goto :goto_0

    .line 507
    :sswitch_19
    const/16 v2, 0x52

    .line 508
    goto :goto_0

    .line 512
    :sswitch_1a
    const/4 v2, 0x4

    .line 513
    goto :goto_0

    .line 515
    :sswitch_1b
    const/16 v2, 0x1d

    .line 516
    goto/16 :goto_0

    .line 518
    :sswitch_1c
    const/16 v2, 0x1e

    .line 519
    goto/16 :goto_0

    .line 521
    :sswitch_1d
    const/16 v2, 0x1f

    .line 522
    goto/16 :goto_0

    .line 524
    :sswitch_1e
    const/16 v2, 0x20

    .line 525
    goto/16 :goto_0

    .line 527
    :sswitch_1f
    const/16 v2, 0x21

    .line 528
    goto/16 :goto_0

    .line 530
    :sswitch_20
    const/16 v2, 0x22

    .line 531
    goto/16 :goto_0

    .line 533
    :sswitch_21
    const/16 v2, 0x23

    .line 534
    goto/16 :goto_0

    .line 536
    :sswitch_22
    const/16 v2, 0x24

    .line 537
    goto/16 :goto_0

    .line 539
    :sswitch_23
    const/16 v2, 0x25

    .line 540
    goto/16 :goto_0

    .line 542
    :sswitch_24
    const/16 v2, 0x26

    .line 543
    goto/16 :goto_0

    .line 545
    :sswitch_25
    const/16 v2, 0x27

    .line 546
    goto/16 :goto_0

    .line 548
    :sswitch_26
    const/16 v2, 0x28

    .line 549
    goto/16 :goto_0

    .line 551
    :sswitch_27
    const/16 v2, 0x29

    .line 552
    goto/16 :goto_0

    .line 554
    :sswitch_28
    const/16 v2, 0x2a

    .line 555
    goto/16 :goto_0

    .line 557
    :sswitch_29
    const/16 v2, 0x2b

    .line 558
    goto/16 :goto_0

    .line 560
    :sswitch_2a
    const/16 v2, 0x2c

    .line 561
    goto/16 :goto_0

    .line 563
    :sswitch_2b
    const/16 v2, 0x2d

    .line 564
    goto/16 :goto_0

    .line 566
    :sswitch_2c
    const/16 v2, 0x2e

    .line 567
    goto/16 :goto_0

    .line 569
    :sswitch_2d
    const/16 v2, 0x2f

    .line 570
    goto/16 :goto_0

    .line 572
    :sswitch_2e
    const/16 v2, 0x30

    .line 573
    goto/16 :goto_0

    .line 575
    :sswitch_2f
    const/16 v2, 0x31

    .line 576
    goto/16 :goto_0

    .line 578
    :sswitch_30
    const/16 v2, 0x32

    .line 579
    goto/16 :goto_0

    .line 581
    :sswitch_31
    const/16 v2, 0x33

    .line 582
    goto/16 :goto_0

    .line 584
    :sswitch_32
    const/16 v2, 0x34

    .line 585
    goto/16 :goto_0

    .line 587
    :sswitch_33
    const/16 v2, 0x35

    .line 588
    goto/16 :goto_0

    .line 590
    :sswitch_34
    const/16 v2, 0x36

    .line 591
    goto/16 :goto_0

    .line 593
    :sswitch_35
    const/16 v2, 0x39

    .line 594
    goto/16 :goto_0

    .line 596
    :sswitch_36
    const/16 v2, 0x3c

    .line 597
    goto/16 :goto_0

    .line 599
    :sswitch_37
    const/16 v2, 0x3f

    .line 600
    goto/16 :goto_0

    .line 602
    :sswitch_38
    const/16 v2, 0x54

    .line 603
    goto/16 :goto_0

    .line 605
    :sswitch_39
    const/16 v2, 0x37

    .line 606
    goto/16 :goto_0

    .line 608
    :sswitch_3a
    const/16 v2, 0x3e

    .line 609
    goto/16 :goto_0

    .line 611
    :sswitch_3b
    const/16 v2, 0x38

    .line 612
    goto/16 :goto_0

    .line 615
    :sswitch_3c
    const/16 v2, 0x4c

    .line 616
    goto/16 :goto_0

    .line 618
    :sswitch_3d
    const/16 v2, 0x41

    .line 619
    goto/16 :goto_0

    .line 621
    :sswitch_3e
    const/16 v2, 0xbb

    .line 622
    goto/16 :goto_0

    .line 624
    :sswitch_3f
    const/16 v2, 0xe8

    .line 625
    goto/16 :goto_0

    .line 419
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_3d
        0x10 -> :sswitch_4
        0x15 -> :sswitch_1
        0x23 -> :sswitch_e
        0x2a -> :sswitch_f
        0x30 -> :sswitch_4
        0x31 -> :sswitch_5
        0x32 -> :sswitch_6
        0x33 -> :sswitch_7
        0x34 -> :sswitch_8
        0x35 -> :sswitch_9
        0x36 -> :sswitch_a
        0x37 -> :sswitch_b
        0x38 -> :sswitch_c
        0x39 -> :sswitch_d
        0x50 -> :sswitch_2
        0x51 -> :sswitch_3
        0x52 -> :sswitch_1a
        0x54 -> :sswitch_15
        0x55 -> :sswitch_16
        0x5b -> :sswitch_19
        0x5c -> :sswitch_1a
        0x63 -> :sswitch_10
        0x64 -> :sswitch_11
        0x65 -> :sswitch_12
        0x66 -> :sswitch_13
        0x72 -> :sswitch_16
        0x73 -> :sswitch_15
        0x8c -> :sswitch_14
        0x8d -> :sswitch_18
        0x95 -> :sswitch_17
        0x96 -> :sswitch_3f
        0x97 -> :sswitch_19
        0x98 -> :sswitch_0
        0x9d -> :sswitch_3e
        0xb5 -> :sswitch_38
        0xb6 -> :sswitch_36
        0xc0 -> :sswitch_2b
        0xc1 -> :sswitch_2e
        0xc2 -> :sswitch_33
        0xc3 -> :sswitch_2f
        0xc4 -> :sswitch_23
        0xc5 -> :sswitch_29
        0xc6 -> :sswitch_2a
        0xc7 -> :sswitch_31
        0xc8 -> :sswitch_39
        0xca -> :sswitch_37
        0xde -> :sswitch_17
        0xe1 -> :sswitch_35
        0xe3 -> :sswitch_1b
        0xe4 -> :sswitch_2d
        0xe5 -> :sswitch_1e
        0xe6 -> :sswitch_20
        0xe7 -> :sswitch_21
        0xe8 -> :sswitch_22
        0xe9 -> :sswitch_24
        0xea -> :sswitch_25
        0xeb -> :sswitch_26
        0xec -> :sswitch_2c
        0xed -> :sswitch_34
        0xee -> :sswitch_32
        0xef -> :sswitch_1d
        0xf0 -> :sswitch_30
        0xf1 -> :sswitch_1c
        0xf2 -> :sswitch_28
        0xf3 -> :sswitch_27
        0xf4 -> :sswitch_14
        0xf5 -> :sswitch_3b
        0xf6 -> :sswitch_1a
        0xf7 -> :sswitch_3a
        0xf8 -> :sswitch_1f
        0xfe -> :sswitch_3c
    .end sparse-switch
.end method


# virtual methods
.method public DftKey(Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 63
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftKey"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-direct {p0, p1, v4}, Lcom/sec/factory/modules/ModuleDFT;->keymapping(Ljava/lang/String;Z)I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTkey(IZ)V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-direct {p0, p1, v4}, Lcom/sec/factory/modules/ModuleDFT;->keymappingCPO(Ljava/lang/String;Z)I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTkey(IZ)V

    goto :goto_0
.end method

.method public DftKeyHold(Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 73
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftKeyHold"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0, p1, v4}, Lcom/sec/factory/modules/ModuleDFT;->keymapping(Ljava/lang/String;Z)I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTkey(IZ)V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-direct {p0, p1, v4}, Lcom/sec/factory/modules/ModuleDFT;->keymappingCPO(Ljava/lang/String;Z)I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTkey(IZ)V

    goto :goto_0
.end method

.method public DftMicsd()Ljava/lang/String;
    .locals 5

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 359
    .local v0, "res":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDFT;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/factory/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleDevice;->isDetectExternalMemory()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 360
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDFT;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/factory/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDevice;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-nez v1, :cond_0

    .line 364
    const-string v0, "\u0002"

    .line 365
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "DftMicsd"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SDdetect<swmount> = ok:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :goto_0
    return-object v0

    .line 367
    :cond_0
    const-string v0, "AT+MICSD:SON\r\nOK\r\n"

    goto :goto_0

    .line 371
    :cond_1
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-nez v1, :cond_2

    .line 372
    const-string v0, "\u0001"

    .line 373
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "DftMicsd"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SDdetect<swunmount> = ok:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 375
    :cond_2
    const-string v0, "AT+MICSD:HON\r\nOK\r\n"

    goto :goto_0

    .line 379
    :cond_3
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-nez v1, :cond_4

    .line 380
    const-string v0, "\u0000"

    .line 381
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "DftMicsd"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SDdetect = nok:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 383
    :cond_4
    const-string v0, "AT+MICSD:OFF\r\nOK\r\n"

    goto :goto_0
.end method

.method public DftTouchDown(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "x"    # Ljava/lang/String;
    .param p2, "y"    # Ljava/lang/String;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftTouchDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleDFT;->hexToDec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 231
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleDFT;->hexToDec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftTouchDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    .line 236
    iput-object p2, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    .line 237
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mDownTime:J

    .line 238
    sget-object v1, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->DOWN:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTTouch(Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->DOWN:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    .line 240
    return-void
.end method

.method public DftTouchLong(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "x"    # Ljava/lang/String;
    .param p2, "y"    # Ljava/lang/String;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftTouchLong"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    .line 277
    iput-object p2, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    sget-object v1, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->DOWN:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    if-ne v0, v1, :cond_0

    .line 280
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->LONG_PRESS:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    .line 282
    :cond_0
    return-void
.end method

.method public DftTouchMove(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "x"    # Ljava/lang/String;
    .param p2, "y"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftTouchMove"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    sget-object v1, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->MOVE:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTTouch(Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->MOVE:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    .line 271
    return-void
.end method

.method public DftTouchUp(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "x2"    # Ljava/lang/String;
    .param p2, "y2"    # Ljava/lang/String;

    .prologue
    .line 251
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    invoke-direct {p0, p1}, Lcom/sec/factory/modules/ModuleDFT;->hexToDec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 253
    invoke-direct {p0, p2}, Lcom/sec/factory/modules/ModuleDFT;->hexToDec(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "DftTouchUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    sget-object v1, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->LONG_PRESS:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    if-ne v0, v1, :cond_1

    .line 259
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTTouch(Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :goto_0
    sget-object v0, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->UP:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleDFT;->mTouchMoveStatus:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    .line 265
    return-void

    .line 261
    :cond_1
    sget-object v1, Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;->UP:Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleDFT;->x1:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleDFT;->y1:Ljava/lang/String;

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/modules/ModuleDFT;->ActionDFTTouch(Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public makeSMS(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "smsBox"    # Landroid/net/Uri;
    .param p2, "smsNumber"    # Ljava/lang/String;
    .param p3, "smsMessage"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 394
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 395
    .local v2, "valuesSMSInBox":Landroid/content/ContentValues;
    const-string v4, "read"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 396
    const-string v4, "date"

    new-instance v5, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 397
    const-string v4, "address"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const-string v4, "body"

    const/4 v5, 0x2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {p3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleDFT;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 402
    .local v1, "resolver":Landroid/content/ContentResolver;
    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    :goto_0
    return v3

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 405
    const/4 v3, 0x0

    goto :goto_0
.end method
