.class public Lcom/sec/factory/modules/ModuleAudioService;
.super Landroid/app/Service;
.source "ModuleAudioService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/modules/ModuleAudioService$LocalBinder;
    }
.end annotation


# static fields
.field private static final AUDIO_FORMAT:I = 0x2

.field private static final CHANNEL_CONFIG:I = 0x3

.field private static final CLASS_NAME:Ljava/lang/String; = "ModuleAudioService"

.field private static final DELAY:I = 0x1f4

.field private static final LOOPBACK_TIME:I

.field private static final RING_BUFFER_SIZE:I = 0x6

.field private static final SAMPLE_RATE:I = 0xac44

.field public static final STATE_EAR_EAR:I = 0x0

.field public static final STATE_MIC2_SPK:I = 0x2

.field public static final STATE_MIC_RCV:I = 0x1

.field public static final STOP_LOOP:I = 0x1


# instance fields
.field private audioSource:I

.field buffer:[B

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioTrack:Landroid/media/AudioTrack;

.field private final mBinder:Landroid/os/IBinder;

.field private mBufferSize:I

.field private mConnectionMode:Ljava/lang/String;

.field private mCurrentState:I

.field public mHandler:Landroid/os/Handler;

.field private mIsHeadsetPlugged:Z

.field private mIsPlaying:Z

.field private mIsRecording:Z

.field private mReadBytes:I

.field private mRecorder:Landroid/media/AudioRecord;

.field private mSupportRingBufferMode:Z

.field private mThread:Ljava/lang/Thread;

.field private mWrittenBytes:I

.field private rInx:I

.field ringBuffer:[[B

.field private wInx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "LOOPBACK_BUFFER_MARGIN"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/factory/modules/ModuleAudioService;->LOOPBACK_TIME:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 28
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->audioSource:I

    .line 35
    iput v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mCurrentState:I

    .line 36
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    .line 37
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    .line 39
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsHeadsetPlugged:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    .line 41
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsPlaying:Z

    .line 42
    iput v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    iput v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    .line 43
    iput v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    .line 44
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->buffer:[B

    .line 45
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mThread:Ljava/lang/Thread;

    .line 46
    iput-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mConnectionMode:Ljava/lang/String;

    .line 47
    new-instance v0, Lcom/sec/factory/modules/ModuleAudioService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleAudioService$LocalBinder;-><init>(Lcom/sec/factory/modules/ModuleAudioService;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBinder:Landroid/os/IBinder;

    .line 49
    iput-boolean v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mSupportRingBufferMode:Z

    .line 50
    iput v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->rInx:I

    .line 51
    iput v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->wInx:I

    .line 61
    new-instance v0, Lcom/sec/factory/modules/ModuleAudioService$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/modules/ModuleAudioService$1;-><init>(Lcom/sec/factory/modules/ModuleAudioService;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/modules/ModuleAudioService;)Landroid/media/AudioTrack;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleAudioService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/factory/modules/ModuleAudioService;Landroid/media/AudioTrack;)Landroid/media/AudioTrack;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleAudioService;
    .param p1, "x1"    # Landroid/media/AudioTrack;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/factory/modules/ModuleAudioService;)Landroid/media/AudioRecord;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleAudioService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/factory/modules/ModuleAudioService;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/modules/ModuleAudioService;
    .param p1, "x1"    # Landroid/media/AudioRecord;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    return-object p1
.end method

.method private isConnectionModeNone()Z
    .locals 5

    .prologue
    .line 259
    const/4 v0, 0x0

    .line 260
    .local v0, "ret":Z
    const-string v1, "ModuleAudioService"

    const-string v2, "ModuleAudioService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mConnectionMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/modules/ModuleAudioService;->mConnectionMode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v1, "none"

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mConnectionMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 262
    :goto_0
    return v0

    .line 261
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setStreamMusicVolume(I)V
    .locals 6
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v5, 0x3

    const/4 v0, 0x0

    .line 289
    if-le p1, v1, :cond_0

    move p1, v1

    .line 290
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 291
    :cond_1
    const-string v1, "ModuleAudioService"

    const-string v2, "setStreamMusicVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volme="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    if-nez p1, :cond_2

    .line 294
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v5, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 299
    :goto_0
    return-void

    .line 296
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v5, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private setStreamMusicVolumeMax()V
    .locals 1

    .prologue
    .line 270
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/ModuleAudioService;->setStreamMusicVolume(I)V

    .line 271
    return-void
.end method

.method private setStreamVoiceVolume(I)V
    .locals 5
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 274
    if-le p1, v1, :cond_0

    move p1, v1

    .line 275
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 276
    :cond_1
    const-string v1, "ModuleAudioService"

    const-string v2, "setStreamVoiceVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "volme="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    if-nez p1, :cond_2

    .line 279
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 286
    :goto_0
    return-void

    .line 281
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v0, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private setStreamVoiceVolumeMax()V
    .locals 1

    .prologue
    .line 266
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/sec/factory/modules/ModuleAudioService;->setStreamVoiceVolume(I)V

    .line 267
    return-void
.end method

.method private setStreamVolumeMax()V
    .locals 3

    .prologue
    .line 253
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "setVolumeMax"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudioService;->setStreamMusicVolumeMax()V

    .line 256
    return-void
.end method


# virtual methods
.method public InitLoopBack()V
    .locals 7

    .prologue
    const v2, 0xac44

    const/4 v4, 0x2

    const/4 v3, 0x3

    .line 93
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v5, "InitLoopBack"

    invoke-static {v0, v1, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/factory/modules/ModuleAudioService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    .line 96
    const-string v0, "MODEL_COMMUNICATION_MODE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mConnectionMode:Ljava/lang/String;

    .line 97
    const-string v0, "SUPPORT_RING_BUFFER_MODE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mSupportRingBufferMode:Z

    .line 99
    invoke-static {v2, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    .line 100
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mSupportRingBufferMode:Z

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->rInx:I

    .line 102
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->wInx:I

    .line 103
    const/4 v0, 0x6

    iget v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->ringBuffer:[[B

    .line 109
    :goto_0
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->audioSource:I

    iget v5, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    .line 114
    new-instance v0, Landroid/media/AudioTrack;

    iget v5, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    const/4 v6, 0x1

    move v1, v3

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    .line 116
    return-void

    .line 105
    :cond_0
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    sget v1, Lcom/sec/factory/modules/ModuleAudioService;->LOOPBACK_TIME:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    .line 106
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->buffer:[B

    goto :goto_0
.end method

.method public StartLoopBack()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, -0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 155
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "StartLoopBack"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    .line 158
    invoke-direct {p0}, Lcom/sec/factory/modules/ModuleAudioService;->setStreamVolumeMax()V

    .line 159
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 160
    iput-boolean v5, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    .line 163
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mSupportRingBufferMode:Z

    if-eqz v0, :cond_a

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_3

    .line 203
    :cond_1
    :goto_0
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "StartLoopBack finished"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_2
    :goto_1
    return-void

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_4

    .line 169
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->rInx:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->rInx:I

    .line 170
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->ringBuffer:[[B

    iget v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->rInx:I

    aget-object v1, v1, v2

    iget v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    invoke-virtual {v0, v1, v4, v2}, Landroid/media/AudioRecord;->read([BII)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    .line 172
    :cond_4
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    if-eq v6, v0, :cond_8

    .line 173
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_1

    .line 176
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    if-nez v0, :cond_5

    .line 177
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "mIsRecording: false"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_6

    .line 182
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->wInx:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->wInx:I

    .line 183
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->ringBuffer:[[B

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudioService;->wInx:I

    aget-object v2, v2, v3

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    invoke-virtual {v1, v2, v4, v3}, Landroid/media/AudioTrack;->write([BII)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    .line 185
    :cond_6
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Audio recorder written bytes = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :goto_2
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsPlaying:Z

    if-nez v0, :cond_7

    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    iget v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    div-int/lit8 v1, v1, 0x2

    if-le v0, v1, :cond_7

    .line 191
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_9

    .line 192
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    if-eq v0, v7, :cond_7

    .line 193
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "AudioTrack start playing..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 196
    iput-boolean v5, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsPlaying:Z

    .line 202
    :cond_7
    :goto_3
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 188
    :cond_8
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "Audio recording failed!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 199
    :cond_9
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "AudioTrack create fail"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 206
    :cond_a
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->buffer:[B

    iget v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    invoke-virtual {v0, v1, v4, v2}, Landroid/media/AudioRecord;->read([BII)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    .line 212
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    if-eq v6, v0, :cond_e

    .line 213
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_2

    .line 216
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    if-nez v0, :cond_b

    .line 217
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "mIsRecording: false"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 220
    :cond_b
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_c

    .line 221
    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    iget-object v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    iget-object v2, p0, Lcom/sec/factory/modules/ModuleAudioService;->buffer:[B

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudioService;->mReadBytes:I

    invoke-virtual {v1, v2, v4, v3}, Landroid/media/AudioTrack;->write([BII)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    .line 223
    :cond_c
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Audio recorder written bytes = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :goto_4
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsPlaying:Z

    if-nez v0, :cond_d

    iget v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mWrittenBytes:I

    iget v1, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBufferSize:I

    div-int/lit8 v1, v1, 0x2

    if-le v0, v1, :cond_d

    .line 230
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_f

    .line 231
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    if-eq v0, v7, :cond_d

    .line 232
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "AudioTrack start playing..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 235
    iput-boolean v5, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsPlaying:Z

    .line 241
    :cond_d
    :goto_5
    iget-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    if-nez v0, :cond_a

    goto/16 :goto_1

    .line 226
    :cond_e
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "Audio recording failed!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 238
    :cond_f
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "AudioTrack create fail"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public StopLoopBack()V
    .locals 4

    .prologue
    .line 247
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "StopLoopBack"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mIsRecording:Z

    .line 249
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 250
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "onBind"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 124
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudioService;->StopLoopBack()V

    .line 120
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 121
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x1

    .line 128
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "onStartCommand"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    if-nez p1, :cond_0

    .line 130
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    const-string v2, "intent == null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :goto_0
    return v4

    .line 133
    :cond_0
    const-string v0, "STATE"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mCurrentState:I

    .line 134
    const-string v0, "ModuleAudioService"

    const-string v1, "ModuleAudioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/modules/ModuleAudioService;->mCurrentState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/factory/modules/ModuleAudioService;->InitLoopBack()V

    .line 136
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/modules/ModuleAudioService$2;

    invoke-direct {v1, p0}, Lcom/sec/factory/modules/ModuleAudioService$2;-><init>(Lcom/sec/factory/modules/ModuleAudioService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mThread:Ljava/lang/Thread;

    .line 141
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleAudioService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 146
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
