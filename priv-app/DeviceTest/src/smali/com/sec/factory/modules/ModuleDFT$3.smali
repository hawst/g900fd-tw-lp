.class Lcom/sec/factory/modules/ModuleDFT$3;
.super Ljava/lang/Thread;
.source "ModuleDFT.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/modules/ModuleDFT;->ActionDFTTouch(Lcom/sec/factory/modules/ModuleDFT$TOUCH_EVENT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleDFT;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleDFT;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 326
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Lcom/sec/factory/modules/ModuleDFT$3;->sleep(J)V

    .line 327
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 328
    .local v2, "upTime":J
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    # getter for: Lcom/sec/factory/modules/ModuleDFT;->mDownTime:J
    invoke-static {v0}, Lcom/sec/factory/modules/ModuleDFT;->access$000(Lcom/sec/factory/modules/ModuleDFT;)J

    move-result-wide v0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    # getter for: Lcom/sec/factory/modules/ModuleDFT;->mTouchLongX1:F
    invoke-static {v5}, Lcom/sec/factory/modules/ModuleDFT;->access$100(Lcom/sec/factory/modules/ModuleDFT;)F

    move-result v5

    iget-object v6, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    # getter for: Lcom/sec/factory/modules/ModuleDFT;->mTouchLongY1:F
    invoke-static {v6}, Lcom/sec/factory/modules/ModuleDFT;->access$200(Lcom/sec/factory/modules/ModuleDFT;)F

    move-result v6

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 331
    .local v9, "up":Landroid/view/MotionEvent;
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v1, 0x0

    # invokes: Lcom/sec/factory/modules/ModuleDFT;->injectTouchEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    invoke-static {v0, v9, v1}, Lcom/sec/factory/modules/ModuleDFT;->access$300(Lcom/sec/factory/modules/ModuleDFT;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    .line 332
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    iget-object v0, v0, Lcom/sec/factory/modules/ModuleDFT;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ActionDFTTouch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Touch Event (Thread) = up = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    # getter for: Lcom/sec/factory/modules/ModuleDFT;->mTouchLongX1:F
    invoke-static {v5}, Lcom/sec/factory/modules/ModuleDFT;->access$100(Lcom/sec/factory/modules/ModuleDFT;)F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/modules/ModuleDFT$3;->this$0:Lcom/sec/factory/modules/ModuleDFT;

    # getter for: Lcom/sec/factory/modules/ModuleDFT;->mTouchLongY1:F
    invoke-static {v5}, Lcom/sec/factory/modules/ModuleDFT;->access$200(Lcom/sec/factory/modules/ModuleDFT;)F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    .end local v2    # "upTime":J
    .end local v9    # "up":Landroid/view/MotionEvent;
    :goto_0
    return-void

    .line 335
    :catch_0
    move-exception v8

    .line 336
    .local v8, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
