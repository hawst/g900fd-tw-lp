.class public Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;
.super Ljava/lang/Object;
.source "SensorReadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/SensorReadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GyroExpansionData"
.end annotation


# instance fields
.field public mData:[S

.field public mNoiseBias:[F

.field public mRMSValue:[F

.field public mReturnValue:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 734
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    .line 735
    new-array v0, v1, [S

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mData:[S

    .line 736
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/factory/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    .line 737
    return-void
.end method
