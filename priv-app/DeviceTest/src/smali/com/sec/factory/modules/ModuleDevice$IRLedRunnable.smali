.class public Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;
.super Ljava/lang/Object;
.source "ModuleDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "IRLedRunnable"
.end annotation


# instance fields
.field private control:Ljava/lang/String;

.field private duration:I

.field private fos:Ljava/io/FileOutputStream;

.field final synthetic this$0:Lcom/sec/factory/modules/ModuleDevice;


# direct methods
.method public constructor <init>(Lcom/sec/factory/modules/ModuleDevice;Ljava/lang/String;ILjava/io/FileOutputStream;)V
    .locals 0
    .param p2, "control"    # Ljava/lang/String;
    .param p3, "duration"    # I
    .param p4, "fos"    # Ljava/io/FileOutputStream;

    .prologue
    .line 1980
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981
    iput-object p2, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->control:Ljava/lang/String;

    .line 1982
    iput p3, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->duration:I

    .line 1983
    iput-object p4, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    .line 1984
    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 1987
    const/4 v9, 0x1

    # setter for: Lcom/sec/factory/modules/ModuleDevice;->mRunningIRLedThread:Z
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1002(Z)Z

    .line 1989
    const-string v9, "IS_IRLED_TEST_SPLIT_COMMAND"

    invoke-static {v9}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v5

    .line 1991
    .local v5, "isSplitCmd":Z
    const-string v9, "PEEL"

    const-string v10, "SUPPORT_IRLED_CONCEPT"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1994
    .local v4, "isPeel":Z
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_3

    .line 1995
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->control:Ljava/lang/String;

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1997
    .local v8, "splitData":[Ljava/lang/String;
    :goto_0
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1100(Lcom/sec/factory/modules/ModuleDevice;)Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v9

    if-nez v9, :cond_1

    .line 1999
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    array-length v9, v8

    if-ge v2, v9, :cond_0

    .line 2000
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    aget-object v10, v8, v2

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/FileOutputStream;->write([B)V

    .line 1999
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2003
    :cond_0
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    const-string v10, "0"

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/FileOutputStream;->write([B)V

    .line 2004
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V

    .line 2005
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2007
    :try_start_1
    iget v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->duration:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2008
    :catch_0
    move-exception v7

    .line 2009
    .local v7, "localInterruptedException2":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2016
    .end local v2    # "i":I
    .end local v7    # "localInterruptedException2":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_2

    .line 2018
    :try_start_3
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->fos:Ljava/io/FileOutputStream;

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2052
    .end local v8    # "splitData":[Ljava/lang/String;
    :cond_2
    :goto_2
    const/4 v9, 0x0

    # setter for: Lcom/sec/factory/modules/ModuleDevice;->mRunningIRLedThread:Z
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1002(Z)Z

    .line 2053
    return-void

    .line 2012
    .restart local v2    # "i":I
    .restart local v8    # "splitData":[Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 2013
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 2019
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i":I
    :catch_2
    move-exception v3

    .line 2020
    .local v3, "ie":Ljava/io/IOException;
    invoke-static {v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2

    .line 2024
    .end local v3    # "ie":Ljava/io/IOException;
    .end local v8    # "splitData":[Ljava/lang/String;
    :cond_3
    if-eqz v4, :cond_5

    .line 2025
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1200(Lcom/sec/factory/modules/ModuleDevice;)Landroid/hardware/ConsumerIrManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/hardware/ConsumerIrManager;->hasIrEmitter()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2026
    :goto_3
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1100(Lcom/sec/factory/modules/ModuleDevice;)Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v9

    if-nez v9, :cond_2

    .line 2027
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->irManager:Landroid/hardware/ConsumerIrManager;
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1200(Lcom/sec/factory/modules/ModuleDevice;)Landroid/hardware/ConsumerIrManager;

    move-result-object v9

    const v10, 0x9470

    iget-object v11, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v12, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->control:Ljava/lang/String;

    # invokes: Lcom/sec/factory/modules/ModuleDevice;->convertPattern(Ljava/lang/String;)[I
    invoke-static {v11, v12}, Lcom/sec/factory/modules/ModuleDevice;->access$1300(Lcom/sec/factory/modules/ModuleDevice;Ljava/lang/String;)[I

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/hardware/ConsumerIrManager;->transmit(I[I)V

    .line 2030
    :try_start_4
    iget v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->duration:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_3

    .line 2031
    :catch_3
    move-exception v6

    .line 2032
    .local v6, "localInterruptedException1":Ljava/lang/InterruptedException;
    invoke-static {v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2

    .line 2037
    .end local v6    # "localInterruptedException1":Ljava/lang/InterruptedException;
    :cond_4
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    iget-object v9, v9, Lcom/sec/factory/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "controlIrED"

    const-string v11, "Device doesn\'t have IR Emitter"

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2040
    :cond_5
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->control:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2041
    .local v0, "arrayOfByte":[B
    :goto_4
    iget-object v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->this$0:Lcom/sec/factory/modules/ModuleDevice;

    # getter for: Lcom/sec/factory/modules/ModuleDevice;->mIRLedThread:Ljava/lang/Thread;
    invoke-static {v9}, Lcom/sec/factory/modules/ModuleDevice;->access$1100(Lcom/sec/factory/modules/ModuleDevice;)Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v9

    if-nez v9, :cond_2

    .line 2042
    const-string v9, "IR_LED_SEND"

    invoke-static {v9, v0}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    .line 2044
    :try_start_5
    iget v9, p0, Lcom/sec/factory/modules/ModuleDevice$IRLedRunnable;->duration:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_4

    .line 2045
    :catch_4
    move-exception v6

    .line 2046
    .restart local v6    # "localInterruptedException1":Ljava/lang/InterruptedException;
    invoke-static {v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2
.end method
