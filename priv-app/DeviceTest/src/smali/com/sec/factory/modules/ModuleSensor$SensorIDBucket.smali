.class public Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;
.super Ljava/lang/Object;
.source "ModuleSensor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SensorIDBucket"
.end annotation


# static fields
.field public static final TARGET_FILE:I = 0x2

.field public static final TARGET_INTENT:I = 0x3

.field public static final TARGET_MANAGER:I = 0x1


# instance fields
.field public mIDArray_File:[I

.field public mIDArray_Intent:[I

.field public mIDArray_Manager:[I


# direct methods
.method constructor <init>([I[I[I)V
    .locals 1
    .param p1, "manager"    # [I
    .param p2, "file"    # [I
    .param p3, "intent"    # [I

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object v0, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Manager:[I

    .line 109
    iput-object v0, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    .line 110
    iput-object v0, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Intent:[I

    .line 113
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Manager:[I

    .line 114
    iput-object p2, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    .line 115
    iput-object p3, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Intent:[I

    .line 116
    return-void
.end method


# virtual methods
.method public getID(I)[I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 119
    packed-switch p1, :pswitch_data_0

    .line 127
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 121
    :pswitch_0
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Manager:[I

    goto :goto_0

    .line 123
    :pswitch_1
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_File:[I

    goto :goto_0

    .line 125
    :pswitch_2
    iget-object v0, p0, Lcom/sec/factory/modules/ModuleSensor$SensorIDBucket;->mIDArray_Intent:[I

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
