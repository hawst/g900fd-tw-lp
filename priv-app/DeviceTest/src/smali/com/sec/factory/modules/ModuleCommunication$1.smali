.class Lcom/sec/factory/modules/ModuleCommunication$1;
.super Landroid/content/BroadcastReceiver;
.source "ModuleCommunication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleCommunication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleCommunication;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleCommunication;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 85
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.factory.intent.ACTION_GPS_SERVICE_RESPONSE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    const-string v1, "result"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    iget-object v1, v1, Lcom/sec/factory/modules/ModuleCommunication;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "mGpsReceiver.onReceive"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTION_GPS_SERVICE_RESPONSE result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v1, "NG_CN0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    const-string v2, "NG_CN0"

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->sendResponse(Ljava/lang/String;)V

    .line 105
    :goto_0
    const-string v1, "SMD"

    sget-object v2, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    # getter for: Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->access$000()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    :cond_0
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->deleteGpsFiles()V

    .line 107
    const/4 v1, 0x0

    # setter for: Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z
    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommunication;->access$002(Z)Z

    .line 108
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommunication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->stopService(Landroid/content/Intent;)V

    .line 111
    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    return-void

    .line 92
    .restart local v0    # "result":Ljava/lang/String;
    :cond_2
    const-string v1, "-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 93
    const-string v1, "SMD"

    sget-object v2, Lcom/sec/factory/modules/ModuleCommunicationService;->GPS_MODE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    const-string v2, "NG"

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->sendResponse(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    const-string v2, "NG_TO"

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->sendResponse(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_4
    # getter for: Lcom/sec/factory/modules/ModuleCommunication;->isOneCommand:Z
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunication;->access$000()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 100
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1, v0}, Lcom/sec/factory/modules/ModuleCommunication;->sendResponse(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_5
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunication$1;->this$0:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->sendStartOK()V

    goto :goto_0
.end method
