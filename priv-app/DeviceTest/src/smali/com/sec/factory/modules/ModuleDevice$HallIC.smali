.class public final enum Lcom/sec/factory/modules/ModuleDevice$HallIC;
.super Ljava/lang/Enum;
.source "ModuleDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/modules/ModuleDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HallIC"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/factory/modules/ModuleDevice$HallIC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/factory/modules/ModuleDevice$HallIC;

.field public static final CLASS_NAME:Ljava/lang/String; = "ModuleDevice.HallIC"

.field public static final enum FOLDER_STATE_CLOSE:Lcom/sec/factory/modules/ModuleDevice$HallIC;

.field public static final enum FOLDER_STATE_ERROR:Lcom/sec/factory/modules/ModuleDevice$HallIC;

.field public static final enum FOLDER_STATE_OPEN:Lcom/sec/factory/modules/ModuleDevice$HallIC;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2146
    new-instance v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;

    const-string v1, "FOLDER_STATE_OPEN"

    invoke-direct {v0, v1, v2}, Lcom/sec/factory/modules/ModuleDevice$HallIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_OPEN:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    new-instance v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;

    const-string v1, "FOLDER_STATE_CLOSE"

    invoke-direct {v0, v1, v3}, Lcom/sec/factory/modules/ModuleDevice$HallIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_CLOSE:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    new-instance v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;

    const-string v1, "FOLDER_STATE_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/sec/factory/modules/ModuleDevice$HallIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_ERROR:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    .line 2145
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/factory/modules/ModuleDevice$HallIC;

    sget-object v1, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_OPEN:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_CLOSE:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_ERROR:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;->$VALUES:[Lcom/sec/factory/modules/ModuleDevice$HallIC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getHallICState()Lcom/sec/factory/modules/ModuleDevice$HallIC;
    .locals 6

    .prologue
    .line 2150
    const-string v2, "ModuleDevice.HallIC"

    const-string v3, "hallIc_by_sysfs"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2153
    :try_start_0
    const-string v2, "PATH_HALLIC_STATE"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2154
    .local v1, "state":Ljava/lang/String;
    const-string v2, "ModuleDevice.HallIC"

    const-string v3, "getHallICState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "state value ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2156
    const-string v2, "SUPPORT_BOOK_COVER"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2157
    const-string v2, "OPEN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2158
    sget-object v2, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_OPEN:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    .line 2173
    :goto_0
    return-object v2

    .line 2159
    :cond_0
    const-string v2, "CLOSE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2160
    sget-object v2, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_CLOSE:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    goto :goto_0

    .line 2163
    :cond_1
    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2164
    sget-object v2, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_CLOSE:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    goto :goto_0

    .line 2165
    :cond_2
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2166
    sget-object v2, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_OPEN:Lcom/sec/factory/modules/ModuleDevice$HallIC;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2169
    :catch_0
    move-exception v0

    .line 2170
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ModuleDevice.HallIC"

    const-string v3, "getHallICState"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2173
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-object v2, Lcom/sec/factory/modules/ModuleDevice$HallIC;->FOLDER_STATE_ERROR:Lcom/sec/factory/modules/ModuleDevice$HallIC;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/factory/modules/ModuleDevice$HallIC;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2145
    const-class v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;

    return-object v0
.end method

.method public static values()[Lcom/sec/factory/modules/ModuleDevice$HallIC;
    .locals 1

    .prologue
    .line 2145
    sget-object v0, Lcom/sec/factory/modules/ModuleDevice$HallIC;->$VALUES:[Lcom/sec/factory/modules/ModuleDevice$HallIC;

    invoke-virtual {v0}, [Lcom/sec/factory/modules/ModuleDevice$HallIC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/factory/modules/ModuleDevice$HallIC;

    return-object v0
.end method
