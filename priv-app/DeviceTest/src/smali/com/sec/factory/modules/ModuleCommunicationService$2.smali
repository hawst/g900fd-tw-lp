.class Lcom/sec/factory/modules/ModuleCommunicationService$2;
.super Ljava/lang/Thread;
.source "ModuleCommunicationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/modules/ModuleCommunicationService;->nfcEnable()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/modules/ModuleCommunicationService;


# direct methods
.method constructor <init>(Lcom/sec/factory/modules/ModuleCommunicationService;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$2;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 236
    # getter for: Lcom/sec/factory/modules/ModuleCommunicationService;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    invoke-static {}, Lcom/sec/factory/modules/ModuleCommunicationService;->access$900()Landroid/nfc/NfcAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->enable()Z

    move-result v0

    .line 238
    .local v0, "on":Z
    if-eqz v0, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/factory/modules/ModuleCommunicationService$2;->this$0:Lcom/sec/factory/modules/ModuleCommunicationService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.nfctest.NFC_DISCOVERY_DISABLE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunicationService;->sendBroadcast(Landroid/content/Intent;)V

    .line 241
    const-string v1, "ModuleCommunicationService"

    const-string v2, "nfcEnable"

    const-string v3, "NFC is turned on!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :goto_0
    return-void

    .line 243
    :cond_0
    const-string v1, "ModuleCommunicationService"

    const-string v2, "nfcEnable"

    const-string v3, "NFC is not turned on!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
