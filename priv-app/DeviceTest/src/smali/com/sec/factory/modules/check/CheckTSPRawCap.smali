.class public Lcom/sec/factory/modules/check/CheckTSPRawCap;
.super Ljava/lang/Object;
.source "CheckTSPRawCap.java"


# static fields
.field public static final DIRECTION_ADJACENT_X:I = 0x3e8

.field public static final DIRECTION_ADJACENT_Y:I = 0x3e9


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private _gapMax:F

.field private _nodes:[[I

.field private xChannel:I

.field private yChannel:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, "CheckTSPRawCap"

    iput-object v0, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->CLASS_NAME:Ljava/lang/String;

    .line 14
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->xChannel:I

    .line 15
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->yChannel:I

    .line 21
    iput p1, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->xChannel:I

    .line 22
    iput p2, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->yChannel:I

    .line 23
    return-void
.end method

.method private computeGap(DD)D
    .locals 7
    .param p1, "lhs"    # D
    .param p3, "rhs"    # D

    .prologue
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    .line 26
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    mul-double/2addr v0, v4

    sub-double v0, v4, v0

    return-wide v0
.end method

.method private makeNearNodeGap([[II)[[F
    .locals 14
    .param p1, "temp"    # [[I
    .param p2, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_nodes:[[I

    .line 39
    const/4 v4, 0x0

    check-cast v4, [[F

    .line 41
    .local v4, "result":[[F
    :try_start_0
    iget v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->xChannel:I

    iget v9, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->yChannel:I

    filled-new-array {v8, v9}, [I

    move-result-object v8

    sget-object v9, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v9, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, [[F

    move-object v4, v0

    .line 43
    packed-switch p2, :pswitch_data_0

    .line 69
    :cond_0
    :goto_0
    return-object v4

    .line 45
    :pswitch_0
    const/4 v7, 0x0

    .local v7, "y":I
    :goto_1
    iget v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->xChannel:I

    if-ge v7, v8, :cond_0

    .line 46
    const/4 v6, 0x0

    .local v6, "x":I
    :goto_2
    iget v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->yChannel:I

    add-int/lit8 v8, v8, -0x1

    if-ge v6, v8, :cond_1

    .line 47
    iget-object v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_nodes:[[I

    aget-object v8, v8, v7

    aget v8, v8, v6

    int-to-float v3, v8

    .line 48
    .local v3, "lhs":F
    iget-object v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_nodes:[[I

    aget-object v8, v8, v7

    add-int/lit8 v9, v6, 0x1

    aget v8, v8, v9

    int-to-float v5, v8

    .line 49
    .local v5, "rhs":F
    aget-object v8, v4, v7

    float-to-double v10, v3

    float-to-double v12, v5

    invoke-direct {p0, v10, v11, v12, v13}, Lcom/sec/factory/modules/check/CheckTSPRawCap;->computeGap(DD)D

    move-result-wide v10

    double-to-float v9, v10

    aput v9, v8, v6

    .line 46
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 45
    .end local v3    # "lhs":F
    .end local v5    # "rhs":F
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 55
    .end local v6    # "x":I
    .end local v7    # "y":I
    :pswitch_1
    const/4 v7, 0x0

    .restart local v7    # "y":I
    :goto_3
    iget v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->xChannel:I

    add-int/lit8 v8, v8, -0x1

    if-ge v7, v8, :cond_0

    .line 56
    const/4 v6, 0x0

    .restart local v6    # "x":I
    :goto_4
    iget v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->yChannel:I

    if-ge v6, v8, :cond_2

    .line 57
    iget-object v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_nodes:[[I

    aget-object v8, v8, v7

    aget v8, v8, v6

    int-to-float v3, v8

    .line 58
    .restart local v3    # "lhs":F
    iget-object v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_nodes:[[I

    add-int/lit8 v9, v7, 0x1

    aget-object v8, v8, v9

    aget v8, v8, v6

    int-to-float v5, v8

    .line 59
    .restart local v5    # "rhs":F
    aget-object v8, v4, v7

    float-to-double v10, v3

    float-to-double v12, v5

    invoke-direct {p0, v10, v11, v12, v13}, Lcom/sec/factory/modules/check/CheckTSPRawCap;->computeGap(DD)D

    move-result-wide v10

    double-to-float v9, v10

    aput v9, v8, v6
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 55
    .end local v3    # "lhs":F
    .end local v5    # "rhs":F
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 66
    .end local v6    # "x":I
    .end local v7    # "y":I
    :catch_0
    move-exception v2

    .line 67
    .local v2, "e":Ljava/lang/NumberFormatException;
    iget-object v8, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "makeNearNodeGap"

    const-string v10, "NumberFormatException"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getMaxAdjacentNodeGap([[II)F
    .locals 14
    .param p1, "temp"    # [[I
    .param p2, "direction"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct/range {p0 .. p2}, Lcom/sec/factory/modules/check/CheckTSPRawCap;->makeNearNodeGap([[II)[[F

    move-result-object v8

    .line 77
    .local v8, "nearGap":[[F
    const/4 v7, 0x0

    .line 79
    .local v7, "max":F
    move-object v0, v8

    .local v0, "arr$":[[F
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v0    # "arr$":[[F
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v6, v0, v3

    .line 80
    .local v6, "linear":[F
    move-object v1, v6

    .local v1, "arr$":[F
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .end local v3    # "i$":I
    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v5, :cond_1

    aget v9, v1, v2

    .line 81
    .local v9, "value":F
    cmpl-float v10, v9, v7

    if-lez v10, :cond_0

    .line 82
    move v7, v9

    .line 83
    iget-object v10, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getMaxAdjacentNodeGap"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "max: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 79
    .end local v9    # "value":F
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_0

    .line 88
    .end local v1    # "arr$":[F
    .end local v5    # "len$":I
    .end local v6    # "linear":[F
    :cond_2
    iget-object v10, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getMaxAdjacentNodeGap"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "max: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iput v7, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_gapMax:F

    .line 90
    iget v10, p0, Lcom/sec/factory/modules/check/CheckTSPRawCap;->_gapMax:F

    return v10
.end method
