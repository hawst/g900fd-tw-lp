.class public Lcom/sec/factory/dummy/HealthCoverManager;
.super Ljava/lang/Object;
.source "HealthCoverManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHealthCoverManager()Lcom/sec/factory/dummy/HealthCoverManager;
    .locals 2

    .prologue
    .line 10
    const-string v0, "HealthCoverManager"

    const-string v1, "getHealthCoverManager"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public registerListener(Lcom/sec/factory/dummy/HealthCoverListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/factory/dummy/HealthCoverListener;

    .prologue
    .line 17
    const-string v0, "HealthCoverManager"

    const-string v1, "getHealthCoverManager"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public registerListener(Lcom/sec/factory/dummy/HealthCoverListener;Landroid/os/Handler;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/factory/dummy/HealthCoverListener;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 20
    const-string v0, "HealthCoverManager"

    const-string v1, "getHealthCoverManager"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public unregisterListener(Lcom/sec/factory/dummy/HealthCoverListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/factory/dummy/HealthCoverListener;

    .prologue
    .line 14
    const-string v0, "HealthCoverManager"

    const-string v1, "getHealthCoverManager"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method public writeData([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 23
    const-string v0, "HealthCoverManager"

    const-string v1, "getHealthCoverManager"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-void
.end method
