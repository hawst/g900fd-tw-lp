.class public Lcom/sec/factory/dummy/MultiSimManager;
.super Ljava/lang/Object;
.source "MultiSimManager.java"


# static fields
.field public static final SIMSLOT1:I = 0x0

.field public static final SIMSLOT2:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static appendPropertySimSlot(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "simSlot"    # I

    .prologue
    .line 12
    const-string v0, "MultiSimManager"

    const-string v1, "appendPropertySimSlot"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, ""

    return-object v0
.end method

.method public static getSimSlotCount()I
    .locals 2

    .prologue
    .line 7
    const-string v0, "MultiSimManager"

    const-string v1, "getSimSlotCount"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    const/4 v0, 0x1

    return v0
.end method
