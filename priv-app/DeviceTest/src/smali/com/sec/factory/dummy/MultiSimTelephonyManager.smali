.class public Lcom/sec/factory/dummy/MultiSimTelephonyManager;
.super Ljava/lang/Object;
.source "MultiSimTelephonyManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefault(I)Lcom/sec/factory/dummy/MultiSimTelephonyManager;
    .locals 2
    .param p0, "simSlot"    # I

    .prologue
    .line 7
    const-string v0, "MultiSimTelephonyManager"

    const-string v1, "getDefault"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getSimState()I
    .locals 2

    .prologue
    .line 12
    const-string v0, "MultiSimTelephonyManager"

    const-string v1, "getSimState"

    invoke-static {v0, v1}, Lcom/sec/factory/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const/4 v0, 0x1

    return v0
.end method
