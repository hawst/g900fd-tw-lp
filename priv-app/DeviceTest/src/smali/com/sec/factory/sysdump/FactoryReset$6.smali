.class Lcom/sec/factory/sysdump/FactoryReset$6;
.super Landroid/os/Handler;
.source "FactoryReset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/sysdump/FactoryReset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/sysdump/FactoryReset;


# direct methods
.method constructor <init>(Lcom/sec/factory/sysdump/FactoryReset;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/sec/factory/sysdump/FactoryReset$6;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x7cf

    .line 603
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 621
    :goto_0
    return-void

    .line 605
    :pswitch_0
    const-string v0, "FactoryReset"

    const-string v1, "handleMessage"

    const-string v2, "UNMOUNT_WAIT_DELAY!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$6;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-virtual {v0}, Lcom/sec/factory/sysdump/FactoryReset;->chekExtSDUnmounted()Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I
    invoke-static {}, Lcom/sec/factory/sysdump/FactoryReset;->access$300()I

    move-result v0

    if-gez v0, :cond_1

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$6;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    # getter for: Lcom/sec/factory/sysdump/FactoryReset;->mHandlerSDUnmounted:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/sysdump/FactoryReset;->access$400(Lcom/sec/factory/sysdump/FactoryReset;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$6;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-virtual {v0}, Lcom/sec/factory/sysdump/FactoryReset;->StartThreadDeleteAllFiles()V

    goto :goto_0

    .line 613
    :cond_1
    # operator-- for: Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I
    invoke-static {}, Lcom/sec/factory/sysdump/FactoryReset;->access$310()I

    .line 614
    const-string v0, "FactoryReset"

    const-string v1, "handleMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UNMOUNT_WAIT_DELAY!! (checkRetry : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I
    invoke-static {}, Lcom/sec/factory/sysdump/FactoryReset;->access$300()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$6;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    # getter for: Lcom/sec/factory/sysdump/FactoryReset;->mHandlerSDUnmounted:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/sysdump/FactoryReset;->access$400(Lcom/sec/factory/sysdump/FactoryReset;)Landroid/os/Handler;

    move-result-object v0

    # getter for: Lcom/sec/factory/sysdump/FactoryReset;->checkDelay:I
    invoke-static {}, Lcom/sec/factory/sysdump/FactoryReset;->access$500()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch 0x7cf
        :pswitch_0
    .end packed-switch
.end method
