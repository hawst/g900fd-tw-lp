.class public Lcom/sec/factory/sysdump/ModemReset;
.super Landroid/app/Service;
.source "ModemReset.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "ModemReset"

.field private static final QUERT_SERVM2_DONE:I = 0x3f2

.field private static final QUERT_SERVM_CMC_DONE:I = 0x3f1

.field private static final QUERT_SERVM_DONE:I = 0x3f0

.field private static final mSalesCode:Ljava/lang/String;


# instance fields
.field public mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private modemRstDone:I

.field productModel:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/factory/sysdump/ModemReset;->mSalesCode:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 60
    iput-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I

    .line 62
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->productModel:Ljava/lang/String;

    .line 80
    new-instance v0, Lcom/sec/factory/sysdump/ModemReset$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/sysdump/ModemReset$1;-><init>(Lcom/sec/factory/sysdump/ModemReset;)V

    iput-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/sysdump/ModemReset;)Lcom/samsung/android/sec_platform_library/FactoryPhone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/sysdump/ModemReset;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/sysdump/ModemReset;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/sysdump/ModemReset;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I

    return v0
.end method

.method static synthetic access$108(Lcom/sec/factory/sysdump/ModemReset;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/factory/sysdump/ModemReset;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/factory/sysdump/ModemReset;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/sysdump/ModemReset;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/factory/sysdump/ModemReset;->runAndroidFactoryReset()V

    return-void
.end method

.method private isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 130
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "f":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private runAndroidFactoryReset()V
    .locals 3

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "SPR/BST/VMU/XAS"

    sget-object v2, Lcom/sec/factory/sysdump/ModemReset;->mSalesCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    const-string v1, "WipeCustomerPartiotion"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 126
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/factory/sysdump/ModemReset;->sendBroadcast(Landroid/content/Intent;)V

    .line 127
    return-void
.end method


# virtual methods
.method SendResetCommandToRIL()V
    .locals 5

    .prologue
    .line 65
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v1, 0xc

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 67
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 68
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sysdump/ModemReset;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3f0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 69
    return-void
.end method

.method SendResetCommandToRIL_CP2()V
    .locals 5

    .prologue
    .line 72
    const-string v1, "ModemReset"

    const-string v2, "onStart"

    const-string v3, "modem CP2 reset start"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v1, 0xc

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 75
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 76
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sysdump/ModemReset;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3f2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 78
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 136
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 142
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-direct {v0, p0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 143
    const-string v0, "SUPPORT_2ND_CP"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 146
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 151
    const-string v0, "SUPPORT_2ND_CP"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/factory/sysdump/ModemReset;->mPhone2:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 154
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 155
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 158
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 159
    if-nez p1, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    const-string v1, "FACTORY"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 165
    .local v0, "callByFactory":Z
    if-eqz v0, :cond_0

    .line 169
    const-string v1, "ModemReset"

    const-string v2, "onStart"

    const-string v3, "modem reset start"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v1, "SUPPORT_2ND_CP"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/ModemReset;->SendResetCommandToRIL_CP2()V

    .line 174
    :cond_2
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/ModemReset;->SendResetCommandToRIL()V

    goto :goto_0
.end method
