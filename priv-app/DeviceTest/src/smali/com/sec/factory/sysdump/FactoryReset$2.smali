.class Lcom/sec/factory/sysdump/FactoryReset$2;
.super Ljava/lang/Thread;
.source "FactoryReset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/sysdump/FactoryReset;->runSDFormatThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/sysdump/FactoryReset;

.field final synthetic val$interStoragePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/factory/sysdump/FactoryReset;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    iput-object p2, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->val$interStoragePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 138
    iget-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    # invokes: Lcom/sec/factory/sysdump/FactoryReset;->getMountService()Landroid/os/storage/IMountService;
    invoke-static {v2}, Lcom/sec/factory/sysdump/FactoryReset;->access$100(Lcom/sec/factory/sysdump/FactoryReset;)Landroid/os/storage/IMountService;

    move-result-object v1

    .line 141
    .local v1, "mountService":Landroid/os/storage/IMountService;
    if-eqz v1, :cond_0

    .line 142
    :try_start_0
    iget-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->val$interStoragePath:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/os/storage/IMountService;->formatVolume(Ljava/lang/String;)I

    .line 143
    const-string v2, "FactoryReset"

    const-string v3, "runSDFormatThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PATH = format "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->val$interStoragePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->val$interStoragePath:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/os/storage/IMountService;->mountVolume(Ljava/lang/String;)I

    .line 146
    const-string v2, "FactoryReset"

    const-string v3, "runSDFormatThread"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PATH = mount "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->val$interStoragePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-virtual {v2}, Lcom/sec/factory/sysdump/FactoryReset;->phoneReset()V

    .line 156
    :goto_0
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "FactoryReset"

    const-string v3, "runSDFormatThread"

    const-string v4, "Unable to invoke IMountService.formatMedia()"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    iget-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-virtual {v2}, Lcom/sec/factory/sysdump/FactoryReset;->phoneReset()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset$2;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-virtual {v3}, Lcom/sec/factory/sysdump/FactoryReset;->phoneReset()V

    throw v2
.end method
