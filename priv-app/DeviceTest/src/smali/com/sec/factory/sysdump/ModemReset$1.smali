.class Lcom/sec/factory/sysdump/ModemReset$1;
.super Landroid/os/Handler;
.source "ModemReset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/sysdump/ModemReset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/sysdump/ModemReset;


# direct methods
.method constructor <init>(Lcom/sec/factory/sysdump/ModemReset;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x2

    .line 83
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 85
    :pswitch_0
    const-string v1, "ModemReset"

    const-string v2, "handleMessage"

    const-string v3, "modem CP1 reset done"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v1, "ModemReset"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "productModel= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    iget-object v4, v4, Lcom/sec/factory/sysdump/ModemReset;->productModel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v1, "SHV-E210L"

    iget-object v2, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    iget-object v2, v2, Lcom/sec/factory/sysdump/ModemReset;->productModel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    const-string v1, "ModemReset"

    const-string v2, "handleMessage"

    const-string v3, "modem VIA reset done"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/samsung/android/sec_platform_library/PacketBuilder;

    const/16 v1, 0x70

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sec_platform_library/PacketBuilder;-><init>(BB)V

    .line 91
    .local v0, "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    invoke-virtual {v0, v5}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->addData(B)Lcom/samsung/android/sec_platform_library/PacketBuilder;

    .line 92
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # getter for: Lcom/sec/factory/sysdump/ModemReset;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$000(Lcom/sec/factory/sysdump/ModemReset;)Lcom/samsung/android/sec_platform_library/FactoryPhone;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/PacketBuilder;->getPacket()[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    iget-object v3, v3, Lcom/sec/factory/sysdump/ModemReset;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x3f1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 94
    .end local v0    # "packet":Lcom/samsung/android/sec_platform_library/PacketBuilder;
    :cond_1
    const-string v1, "SUPPORT_2ND_CP"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # operator++ for: Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$108(Lcom/sec/factory/sysdump/ModemReset;)I

    .line 96
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # getter for: Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$100(Lcom/sec/factory/sysdump/ModemReset;)I

    move-result v1

    if-ne v5, v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # invokes: Lcom/sec/factory/sysdump/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$200(Lcom/sec/factory/sysdump/ModemReset;)V

    goto :goto_0

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # invokes: Lcom/sec/factory/sysdump/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$200(Lcom/sec/factory/sysdump/ModemReset;)V

    goto :goto_0

    .line 104
    :pswitch_1
    const-string v1, "ModemReset"

    const-string v2, "handleMessage"

    const-string v3, "modem CP2 reset done"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # operator++ for: Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$108(Lcom/sec/factory/sysdump/ModemReset;)I

    .line 106
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # getter for: Lcom/sec/factory/sysdump/ModemReset;->modemRstDone:I
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$100(Lcom/sec/factory/sysdump/ModemReset;)I

    move-result v1

    if-ne v5, v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # invokes: Lcom/sec/factory/sysdump/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$200(Lcom/sec/factory/sysdump/ModemReset;)V

    goto/16 :goto_0

    .line 110
    :pswitch_2
    const-string v1, "ModemReset"

    const-string v2, "handleMessage"

    const-string v3, "modem CMC reset done"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/sec/factory/sysdump/ModemReset$1;->this$0:Lcom/sec/factory/sysdump/ModemReset;

    # invokes: Lcom/sec/factory/sysdump/ModemReset;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/factory/sysdump/ModemReset;->access$200(Lcom/sec/factory/sysdump/ModemReset;)V

    goto/16 :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x3f0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
