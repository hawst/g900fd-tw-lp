.class public Lcom/sec/factory/sysdump/FactoryReset;
.super Landroid/app/Activity;
.source "FactoryReset.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "FactoryReset"

.field private static final EXTERNAL_SD_MOUNT_POINT:Ljava/lang/String; = "/mnt/extSdCard"

.field private static final FUSE_SDCARD_PROPERTY:Ljava/lang/String; = "ro.crypto.fuse_sdcard"

.field private static final LOCAL_FOLDER_NAME:Ljava/lang/String; = "/mnt/sdcard/"

.field private static final MY_ACTIVITY_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.sysdump"

.field private static final PRE_LOAD_DIC:Ljava/lang/String; = "/sdcard/DioDict3B"

.field private static final PRE_LOAD_NAVI:Ljava/lang/String; = "/sdcard/autonavidata50"

.field private static final SETUP_WIZARD_PACKAGE_NAME:Ljava/lang/String; = "com.google.android.setupwizard"

.field private static final UNMOUNT_WAIT_DELAY:I = 0x7cf

.field private static final arg1:Z

.field private static checkDelay:I

.field private static checkRetry:I

.field private static mDBOPContentsCount:I

.field private static mExistFileCount:I

.field private static mProtectFileNameArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mStartPhoneResetTimer:Ljava/util/Timer;

.field private static final salesCode:Ljava/lang/String;


# instance fields
.field private CONTENTS_LIST_DB:Ljava/lang/String;

.field private final CONTENTS_LIST_DB_TABLE_NAME:Ljava/lang/String;

.field private final MINUTES:I

.field private final NOW:I

.field private final TIMER_REPEATING_DELAY:I

.field private mActivityPackageNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mDBIsExist:Z

.field private mDialogHandler:Landroid/os/Handler;

.field private mHandlerSDUnmounted:Landroid/os/Handler;

.field private mModelCommunicationMode:Ljava/lang/String;

.field private mMountService:Landroid/os/storage/IMountService;

.field private mNonExistContentHandler:Landroid/os/Handler;

.field private mStorageListener:Landroid/os/storage/StorageEventListener;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mSupportInternalSDFormat:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field productModel:Ljava/lang/String;

.field private service:Landroid/os/storage/IMountService;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    const-string v0, "ro.csc.sales_code"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/factory/sysdump/FactoryReset;->salesCode:Ljava/lang/String;

    .line 91
    sput v2, Lcom/sec/factory/sysdump/FactoryReset;->mExistFileCount:I

    .line 92
    sput v2, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    .line 100
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/factory/sysdump/FactoryReset;->checkDelay:I

    .line 101
    const/16 v0, 0xa

    sput v0, Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mSupportInternalSDFormat:Z

    .line 57
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->TIMER_REPEATING_DELAY:I

    .line 58
    const v0, 0xea60

    iput v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->MINUTES:I

    .line 59
    iput v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->NOW:I

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mNonExistContentHandler:Landroid/os/Handler;

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDialogHandler:Landroid/os/Handler;

    .line 76
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mModelCommunicationMode:Ljava/lang/String;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    .line 86
    iput-boolean v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDBIsExist:Z

    .line 87
    const-string v0, "/system/csc/contents.db"

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->CONTENTS_LIST_DB:Ljava/lang/String;

    .line 88
    const-string v0, "contents"

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->CONTENTS_LIST_DB_TABLE_NAME:Ljava/lang/String;

    .line 96
    iput-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->productModel:Ljava/lang/String;

    .line 108
    iput-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset;->mMountService:Landroid/os/storage/IMountService;

    .line 109
    new-instance v0, Lcom/sec/factory/sysdump/FactoryReset$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/sysdump/FactoryReset$1;-><init>(Lcom/sec/factory/sysdump/FactoryReset;)V

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageListener:Landroid/os/storage/StorageEventListener;

    .line 599
    new-instance v0, Lcom/sec/factory/sysdump/FactoryReset$6;

    invoke-direct {v0, p0}, Lcom/sec/factory/sysdump/FactoryReset$6;-><init>(Lcom/sec/factory/sysdump/FactoryReset;)V

    iput-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mHandlerSDUnmounted:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/sysdump/FactoryReset;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/sysdump/FactoryReset;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->runSDFormatThread()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/factory/sysdump/FactoryReset;)Landroid/os/storage/IMountService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/sysdump/FactoryReset;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->getMountService()Landroid/os/storage/IMountService;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/factory/sysdump/FactoryReset;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/sysdump/FactoryReset;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->isFileDeleteComplete()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 48
    sget v0, Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I

    return v0
.end method

.method static synthetic access$310()I
    .locals 2

    .prologue
    .line 48
    sget v0, Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/sec/factory/sysdump/FactoryReset;->checkRetry:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/factory/sysdump/FactoryReset;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/sysdump/FactoryReset;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mHandlerSDUnmounted:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 48
    sget v0, Lcom/sec/factory/sysdump/FactoryReset;->checkDelay:I

    return v0
.end method

.method public static countExistFile(Ljava/io/File;)Z
    .locals 3
    .param p0, "folder"    # Ljava/io/File;

    .prologue
    .line 341
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 342
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 344
    .local v0, "files":[Ljava/io/File;
    if-eqz v0, :cond_1

    .line 345
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 346
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 347
    aget-object v2, v0, v1

    invoke-static {v2}, Lcom/sec/factory/sysdump/FactoryReset;->countExistFile(Ljava/io/File;)Z

    .line 345
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 349
    :cond_0
    sget v2, Lcom/sec/factory/sysdump/FactoryReset;->mExistFileCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/factory/sysdump/FactoryReset;->mExistFileCount:I

    goto :goto_1

    .line 355
    .end local v0    # "files":[Ljava/io/File;
    .end local v1    # "i":I
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method private countOPContentsAndMakeContentList()V
    .locals 7

    .prologue
    .line 295
    const/4 v3, 0x0

    sput v3, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    .line 296
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "SELECT * from contents"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 297
    .local v1, "dbCursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 298
    const-string v3, "filePath"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 299
    .local v2, "pathIndex":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/sec/factory/sysdump/FactoryReset;->mProtectFileNameArray:Ljava/util/ArrayList;

    .line 301
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_3

    .line 302
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "currentPath":Ljava/lang/String;
    const-string v3, "/sdcard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "sdcard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 305
    :cond_0
    sget v3, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    .line 307
    const-string v3, "sdcard"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 308
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 311
    :cond_1
    sget-object v3, Lcom/sec/factory/sysdump/FactoryReset;->mProtectFileNameArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    const-string v3, "FactoryReset"

    const-string v4, "countOPContentsAndMakeContentList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FilePath: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> add filepath to list"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 319
    .end local v0    # "currentPath":Ljava/lang/String;
    :cond_3
    const-string v3, "FactoryReset"

    const-string v4, "countOPContentsAndMakeContentList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SDCardContent Count "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 322
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 323
    return-void
.end method

.method private declared-synchronized getMountService()Landroid/os/storage/IMountService;
    .locals 4

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    const-string v1, "FactoryReset"

    const-string v2, "getMountService"

    const-string v3, "getMountService ::"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mMountService:Landroid/os/storage/IMountService;

    if-nez v1, :cond_0

    .line 164
    const-string v1, "mount"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 166
    .local v0, "service":Landroid/os/IBinder;
    if-eqz v0, :cond_1

    .line 167
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mMountService:Landroid/os/storage/IMountService;

    .line 173
    .end local v0    # "service":Landroid/os/IBinder;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mMountService:Landroid/os/storage/IMountService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 169
    .restart local v0    # "service":Landroid/os/IBinder;
    :cond_1
    :try_start_1
    const-string v1, "FactoryReset"

    const-string v2, "getMountService"

    const-string v3, "Can\'t get mount service"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 161
    .end local v0    # "service":Landroid/os/IBinder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private isFileDeleteComplete()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 326
    sput v0, Lcom/sec/factory/sysdump/FactoryReset;->mExistFileCount:I

    .line 327
    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/sdcard/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/factory/sysdump/FactoryReset;->countExistFile(Ljava/io/File;)Z

    .line 328
    const-string v1, "FactoryReset"

    const-string v2, "isFileDeleteComplete"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SDcard :Exists File:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/factory/sysdump/FactoryReset;->mExistFileCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / OP\'s Contents File:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    sget v1, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    sget v2, Lcom/sec/factory/sysdump/FactoryReset;->mExistFileCount:I

    if-ne v1, v2, :cond_0

    .line 332
    const/4 v0, 0x1

    .line 334
    :cond_0
    return v0
.end method

.method private isOPContentsExist()Z
    .locals 3

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDBIsExist:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/factory/sysdump/FactoryReset;->mDBOPContentsCount:I

    if-nez v0, :cond_1

    .line 286
    :cond_0
    const-string v0, "FactoryReset"

    const-string v1, "isOPContentsExist"

    const-string v2, "Opeartor\'s ContentsDB is not exits!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const/4 v0, 0x0

    .line 290
    :goto_0
    return v0

    .line 289
    :cond_1
    const-string v0, "FactoryReset"

    const-string v1, "isOPContentsExist"

    const-string v2, "Opeartor\'s ContentsDB is exits!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private killAllActivity()V
    .locals 7

    .prologue
    .line 521
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 522
    const-string v4, "FactoryReset"

    const-string v5, "killAllActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PackageName: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "com.sec.android.app.sysdump"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "com.google.android.setupwizard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 521
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 530
    :cond_1
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/sec/factory/sysdump/FactoryReset;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 531
    .local v1, "mgr":Landroid/app/ActivityManager;
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 532
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/app/ActivityManager;->restartPackage(Ljava/lang/String;)V

    goto :goto_1

    .line 534
    .end local v1    # "mgr":Landroid/app/ActivityManager;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private makeActivityList()V
    .locals 14

    .prologue
    .line 490
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 491
    .local v7, "manager":Landroid/content/pm/PackageManager;
    new-instance v8, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v8}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 492
    .local v8, "mi":Landroid/app/ActivityManager$MemoryInfo;
    const-string v12, "activity"

    invoke-virtual {p0, v12}, Lcom/sec/factory/sysdump/FactoryReset;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 493
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v8}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 497
    const/16 v12, 0x1e

    :try_start_0
    invoke-virtual {v0, v12}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v11

    .line 498
    .local v11, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    .line 499
    .local v5, "li":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const-string v9, "empty"

    .line 501
    .local v9, "packageName":Ljava/lang/String;
    :cond_0
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 502
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v12, v12, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v12}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 503
    .local v1, "content":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/factory/sysdump/FactoryReset;->getNewIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    const/high16 v13, 0x10000

    invoke-virtual {v7, v12, v13}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 505
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v10

    .line 507
    .local v10, "rli":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Landroid/content/pm/ResolveInfo;>;"
    :goto_0
    invoke-interface {v10}, Ljava/util/ListIterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 508
    invoke-interface {v10}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 509
    .local v4, "ii":Landroid/content/pm/ResolveInfo;
    iget-object v12, p0, Lcom/sec/factory/sysdump/FactoryReset;->mActivityPackageNameList:Ljava/util/ArrayList;

    iget-object v13, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 512
    .end local v1    # "content":Ljava/lang/String;
    .end local v4    # "ii":Landroid/content/pm/ResolveInfo;
    .end local v5    # "li":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v6    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "rli":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Landroid/content/pm/ResolveInfo;>;"
    .end local v11    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v2

    .line 513
    .local v2, "e":Ljava/lang/SecurityException;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception: ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] encountered while getting process and task info"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 515
    .local v3, "errMsg":Ljava/lang/String;
    const-string v12, "FactoryReset"

    const-string v13, "makeActivityList"

    invoke-static {v12, v13, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    .end local v2    # "e":Ljava/lang/SecurityException;
    .end local v3    # "errMsg":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private runSDFormatThread()V
    .locals 5

    .prologue
    .line 133
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "interStoragePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageListener:Landroid/os/storage/StorageEventListener;

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->unregisterListener(Landroid/os/storage/StorageEventListener;)V

    .line 135
    const-string v1, "FactoryReset"

    const-string v2, "runSDFormatThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "--runSDFormatThread >> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    new-instance v1, Lcom/sec/factory/sysdump/FactoryReset$2;

    invoke-direct {v1, p0, v0}, Lcom/sec/factory/sysdump/FactoryReset$2;-><init>(Lcom/sec/factory/sysdump/FactoryReset;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/factory/sysdump/FactoryReset$2;->start()V

    .line 158
    return-void
.end method

.method private startFactoryReset()V
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mSupportInternalSDFormat:Z

    if-eqz v0, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->sdcardFormatAndPhoneReset()V

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->phoneReset()V

    goto :goto_0
.end method


# virtual methods
.method StartThreadDeleteAllFiles()V
    .locals 2

    .prologue
    .line 576
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/sysdump/FactoryReset$5;

    invoke-direct {v1, p0}, Lcom/sec/factory/sysdump/FactoryReset$5;-><init>(Lcom/sec/factory/sysdump/FactoryReset;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 594
    .local v0, "threadDeleteAllFiles":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 595
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 596
    return-void
.end method

.method chekExtSDUnmounted()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 549
    const-string v2, "mounted"

    .line 552
    .local v2, "statusExternal":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->getMountService()Landroid/os/storage/IMountService;

    move-result-object v0

    .line 554
    .local v0, "mountService":Landroid/os/storage/IMountService;
    if-nez v0, :cond_1

    .line 555
    const-string v4, "FactoryReset"

    const-string v5, "chekExtSDUnmounted"

    const-string v6, "Cannot get mountservice"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    .end local v0    # "mountService":Landroid/os/storage/IMountService;
    :cond_0
    :goto_0
    return v3

    .line 559
    .restart local v0    # "mountService":Landroid/os/storage/IMountService;
    :cond_1
    const-string v4, "/mnt/extSdCard"

    invoke-interface {v0, v4}, Landroid/os/storage/IMountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 564
    .end local v0    # "mountService":Landroid/os/storage/IMountService;
    :goto_1
    const-string v4, "FactoryReset"

    const-string v5, "chekExtSDUnmounted"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "statusExternal : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const-string v4, "unmounted"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "removed"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 568
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 560
    :catch_0
    move-exception v1

    .line 561
    .local v1, "rex":Ljava/lang/Exception;
    const-string v2, "removed"

    goto :goto_1
.end method

.method public deleteDirectory(Ljava/io/File;)Z
    .locals 6
    .param p1, "folder"    # Ljava/io/File;

    .prologue
    .line 435
    const-string v3, "FactoryReset"

    const-string v4, "deleteDirectory"

    const-string v5, "deleteDirectory()"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 438
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 441
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_3

    .line 442
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    array-length v3, v1

    if-ge v2, v3, :cond_3

    .line 443
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 444
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/sdcard/autonavidata50"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 442
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 446
    :cond_1
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/sdcard/DioDict3B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 450
    aget-object v3, v1, v2

    invoke-virtual {p0, v3}, Lcom/sec/factory/sysdump/FactoryReset;->deleteDirectory(Ljava/io/File;)Z

    goto :goto_1

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v3, 0x0

    .line 465
    .end local v0    # "ex":Ljava/lang/NullPointerException;
    .end local v1    # "files":[Ljava/io/File;
    .end local v2    # "i":I
    :goto_2
    return v3

    .line 452
    .restart local v1    # "files":[Ljava/io/File;
    .restart local v2    # "i":I
    :cond_2
    aget-object v3, v1, v2

    invoke-virtual {p0, v3}, Lcom/sec/factory/sysdump/FactoryReset;->isDeleteFile(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 453
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 465
    .end local v1    # "files":[Ljava/io/File;
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v3

    goto :goto_2
.end method

.method protected getNewIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "a"    # Ljava/lang/String;

    .prologue
    .line 538
    move-object v0, p1

    .line 539
    .local v0, "activityName":Ljava/lang/String;
    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 540
    .local v3, "lastDot":I
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 541
    .local v1, "className":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 542
    .local v2, "i":Landroid/content/Intent;
    new-instance v4, Landroid/content/ComponentName;

    invoke-direct {v4, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 543
    return-object v2
.end method

.method public init()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 213
    const-string v3, "NONE"

    sget-object v4, Lcom/sec/factory/sysdump/FactoryReset;->salesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/system/csc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/factory/sysdump/FactoryReset;->salesCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sysdump/FactoryReset;->CONTENTS_LIST_DB:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 215
    .local v2, "new_DB_FILE":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 217
    .local v1, "mFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 218
    iput-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset;->CONTENTS_LIST_DB:Ljava/lang/String;

    .line 222
    .end local v1    # "mFile":Ljava/io/File;
    .end local v2    # "new_DB_FILE":Ljava/lang/String;
    :cond_0
    const-string v3, "FactoryReset"

    const-string v4, "init"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "contents.db path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/factory/sysdump/FactoryReset;->CONTENTS_LIST_DB:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->CONTENTS_LIST_DB:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 227
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDBIsExist:Z

    .line 228
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->countOPContentsAndMakeContentList()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :goto_0
    const-string v3, "ro.crypto.fuse_sdcard"

    const-string v4, "none"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 236
    :cond_1
    iput-boolean v7, p0, Lcom/sec/factory/sysdump/FactoryReset;->mSupportInternalSDFormat:Z

    .line 237
    const-string v3, "FactoryReset"

    const-string v4, "init"

    const-string v5, "fuse_sdcard true. Skip formatting sdcard"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_2
    :goto_1
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    iput-boolean v7, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDBIsExist:Z

    .line 231
    const-string v3, "FactoryReset"

    const-string v4, "init"

    const-string v5, "DB for contents NOT found!"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 241
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_3
    const-string v3, "mount"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mMountService:Landroid/os/storage/IMountService;

    .line 243
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v3, :cond_2

    .line 244
    const-string v3, "storage"

    invoke-virtual {p0, v3}, Lcom/sec/factory/sysdump/FactoryReset;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    iput-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 246
    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v3, :cond_2

    .line 247
    const-string v3, "FactoryReset"

    const-string v4, "init"

    const-string v5, "Failed to get StorageManager"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method isDeleteFile(Ljava/io/File;)Z
    .locals 7
    .param p1, "f"    # Ljava/io/File;

    .prologue
    .line 470
    const/4 v2, 0x0

    .line 471
    .local v2, "protectFileName":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "currentFileName":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v3, Lcom/sec/factory/sysdump/FactoryReset;->mProtectFileNameArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 474
    sget-object v3, Lcom/sec/factory/sysdump/FactoryReset;->mProtectFileNameArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "protectFileName":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 476
    .restart local v2    # "protectFileName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 477
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 478
    const-string v3, "FactoryReset"

    const-string v4, "isDeleteFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "protected file! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    const/4 v3, 0x0

    .line 485
    :goto_1
    return v3

    .line 473
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 484
    :cond_1
    const-string v3, "FactoryReset"

    const-string v4, "isDeleteFile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete file! : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const/4 v3, 0x1

    goto :goto_1
.end method

.method launchDialLogForAbnormal()V
    .locals 4

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mDialogHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/factory/sysdump/FactoryReset$3;

    invoke-direct {v1, p0}, Lcom/sec/factory/sysdump/FactoryReset$3;-><init>(Lcom/sec/factory/sysdump/FactoryReset;)V

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 261
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 182
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/factory/sysdump/FactoryReset;->setContentView(I)V

    .line 183
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->powerSavingOff()V

    .line 185
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->init()V

    .line 187
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->startFactoryReset()V

    .line 188
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 264
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Abnormal factory reset!\nRestart factory reset plz!"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Ok"

    new-instance v2, Lcom/sec/factory/sysdump/FactoryReset$4;

    invoke-direct {v2, p0}, Lcom/sec/factory/sysdump/FactoryReset$4;-><init>(Lcom/sec/factory/sysdump/FactoryReset;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public phoneReset()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 360
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->powerSavingOn()V

    .line 362
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/factory/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 363
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 366
    :cond_0
    const-string v2, "MODEL_COMMUNICATION_MODE"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/sysdump/FactoryReset;->mModelCommunicationMode:Ljava/lang/String;

    .line 368
    const-string v2, "FactoryReset"

    const-string v3, "phoneReset"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mModelCommunicationMode= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/factory/sysdump/FactoryReset;->mModelCommunicationMode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v2, "none"

    iget-object v3, p0, Lcom/sec/factory/sysdump/FactoryReset;->mModelCommunicationMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 372
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 374
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "SPR/BST/VMU/XAS"

    sget-object v3, Lcom/sec/factory/sysdump/FactoryReset;->salesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 375
    const-string v2, "WipeCustomerPartiotion"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 378
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/factory/sysdump/FactoryReset;->sendBroadcast(Landroid/content/Intent;)V

    .line 385
    .end local v1    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 381
    :cond_2
    const-string v2, "FactoryReset"

    const-string v3, "phoneReset"

    const-string v4, "phoneReset : call Modem Reset"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 383
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/factory/sysdump/ModemReset;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "FACTORY"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public powerSavingOff()V
    .locals 4

    .prologue
    .line 191
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/sysdump/FactoryReset;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 192
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string v2, "FactoryReset"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 195
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    const-string v1, "FactoryReset"

    const-string v2, "powerSavingOff"

    const-string v3, "wake lock acquire!!!!!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    const-string v1, "FactoryReset"

    const-string v2, "powerSavingOff"

    const-string v3, "wake lock isHeld!!!!!!!!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public powerSavingOn()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 207
    :cond_0
    return-void
.end method

.method public sdcardFormatAndPhoneReset()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 389
    const-string v1, "FactoryReset"

    const-string v2, "sdcardFormatAndPhoneReset"

    const-string v3, "SDCARD_format : start"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->isOPContentsExist()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 398
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->service:Landroid/os/storage/IMountService;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->chekExtSDUnmounted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 399
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->service:Landroid/os/storage/IMountService;

    const-string v2, "/mnt/extSdCard"

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/os/storage/IMountService;->unmountVolume(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 405
    :cond_0
    :goto_0
    const-string v1, "Killing all activities which is running!!"

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 407
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->makeActivityList()V

    .line 408
    invoke-direct {p0}, Lcom/sec/factory/sysdump/FactoryReset;->killAllActivity()V

    .line 409
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->mHandlerSDUnmounted:Landroid/os/Handler;

    const/16 v2, 0x7cf

    sget v3, Lcom/sec/factory/sysdump/FactoryReset;->checkDelay:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 431
    :goto_1
    return-void

    .line 416
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->service:Landroid/os/storage/IMountService;

    if-eqz v1, :cond_2

    .line 418
    :try_start_1
    iget-object v1, p0, Lcom/sec/factory/sysdump/FactoryReset;->service:Landroid/os/storage/IMountService;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/os/storage/IMountService;->formatVolume(Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 428
    :goto_2
    const-string v1, "FactoryReset"

    const-string v2, "sdcardFormatAndPhoneReset"

    const-string v3, "SDCard_format : end"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-virtual {p0}, Lcom/sec/factory/sysdump/FactoryReset;->phoneReset()V

    goto :goto_1

    .line 419
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SDCard_format"

    const-string v2, "Unable to invoke IMountService.formatMedia()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 424
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    const-string v1, "FactoryReset"

    const-string v2, "sdcardFormatAndPhoneReset"

    const-string v3, "Unable to locate IMountService"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 401
    :catch_1
    move-exception v1

    goto :goto_0
.end method
