.class Lcom/sec/factory/sysdump/FactoryReset$5;
.super Ljava/lang/Object;
.source "FactoryReset.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/sysdump/FactoryReset;->StartThreadDeleteAllFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/sysdump/FactoryReset;


# direct methods
.method constructor <init>(Lcom/sec/factory/sysdump/FactoryReset;)V
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/sec/factory/sysdump/FactoryReset$5;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 579
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$5;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/sdcard/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/factory/sysdump/FactoryReset;->deleteDirectory(Ljava/io/File;)Z

    .line 582
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$5;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    # invokes: Lcom/sec/factory/sysdump/FactoryReset;->isFileDeleteComplete()Z
    invoke-static {v0}, Lcom/sec/factory/sysdump/FactoryReset;->access$200(Lcom/sec/factory/sysdump/FactoryReset;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    const-string v0, "FactoryReset"

    const-string v1, "StartThreadDeleteAllFiles"

    const-string v2, "recursive deletion completed"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/sysdump/FactoryReset$5;->this$0:Lcom/sec/factory/sysdump/FactoryReset;

    invoke-virtual {v0}, Lcom/sec/factory/sysdump/FactoryReset;->phoneReset()V

    .line 592
    return-void

    .line 586
    :cond_0
    const-string v0, "FactoryReset"

    const-string v1, "StartThreadDeleteAllFiles"

    const-string v2, "recursive deletion failed"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
