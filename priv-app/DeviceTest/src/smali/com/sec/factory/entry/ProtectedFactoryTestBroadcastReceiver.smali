.class public Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProtectedFactoryTestBroadcastReceiver.java"


# static fields
.field private static final ACTION_GET_FTA:Ljava/lang/String; = "android.intent.action.GET_FTA"

.field public static final ACTION_GET_MEDIASCANNER_FINISHED:Ljava/lang/String; = "com.sec.factory.entry.ACTION_GET_MEDIASCANNER_FINISHED"

.field private static final ACTION_REQUEST_FTCLIENT_START:Ljava/lang/String; = "com.sec.factory.entry.REQUEST_FTCLIENT_START"

.field private static final ACTION_SECPHONE_READY:Ljava/lang/String; = "android.intent.action.SECPHONE_READY"

.field private static final ACTION_SECRET_CODE:Ljava/lang/String; = "android.provider.Telephony.SECRET_CODE"

.field private static final ACTION_WAKELOCK_FROM_RIL:Ljava/lang/String; = "com.sec.android.app.factorytest"

.field private static final REQUEST_FACTORY_RESET:Ljava/lang/String; = "com.sec.factory.entry.REQUEST_FACTORY_RESET"

.field public static final REQUEST_UPDATE:Ljava/lang/String; = "com.sec.factory.app.systeminfo.update"

.field private static final TAG:Ljava/lang/String; = "UnprotectedFactoryTestBroadcastReceiver"

.field public static mPrepare15Test:Z

.field public static misSecphoneReady:Z


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    sput-boolean v0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->misSecphoneReady:Z

    .line 44
    sput-boolean v0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mPrepare15Test:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private createFileNotiCheck()V
    .locals 6

    .prologue
    .line 194
    const-string v0, "/efs/FactoryApp/reset_flag"

    .line 195
    .local v0, "SecondAckCheckFile":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    .local v1, "SecondAckFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 199
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 200
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/io/File;->setReadable(ZZ)Z

    .line 201
    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 204
    :cond_0
    const-string v3, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v4, "createFileNotiCheck"

    const-string v5, "createFileNotiCheck - SecondAckFile"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 212
    :goto_0
    return-void

    .line 205
    :catch_0
    move-exception v2

    .line 207
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 208
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 210
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private launchFactoryHistoryView()V
    .locals 4

    .prologue
    .line 250
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "launchFactoryHistoryView"

    const-string v3, "Launch UIFactoryTestNVView"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 253
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.ui.UIFactoryTestNVView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 255
    iget-object v1, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 259
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 257
    :cond_0
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "launchFactoryHistoryView"

    const-string v3, "XML data parsing was not completed."

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private launchFactoryTest()V
    .locals 4

    .prologue
    .line 224
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "launchFactoryTest"

    const-string v3, "Launch FactoryTestMain"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.factorytest.FactoryTestMain"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 230
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 231
    iget-object v1, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 235
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 233
    :cond_0
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "launchFactoryTest"

    const-string v3, "XML data parsing was not completed."

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private launchMainHistoryView()V
    .locals 4

    .prologue
    .line 238
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "launchMainHistoryView"

    const-string v3, "Launch UIFactoryHistory"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 241
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.ui.UIFactoryHistory"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 243
    iget-object v1, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 247
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 245
    :cond_0
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "launchMainHistoryView"

    const-string v3, "XML data parsing was not completed."

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendSystemInfoViewUpdate()V
    .locals 4

    .prologue
    .line 262
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "sendSystemInfoViewUpdate"

    const-string v3, "com.sec.factory.app.systeminfo.update"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.app.systeminfo.update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .local v0, "updateIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 266
    return-void
.end method

.method private startFactoryTestClientServiceAPO()V
    .locals 4

    .prologue
    .line 215
    const-string v1, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v2, "startFactoryTestClientServiceAPO"

    const-string v3, "start FactoryTestClient service for APO"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 218
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.aporiented.FtClient"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 219
    iget-object v1, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 220
    iget-object v1, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommon;->setFtClientState(Z)Z

    .line 221
    return-void
.end method

.method private systemInfoWidgetUpdate()V
    .locals 5

    .prologue
    .line 269
    const-string v2, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v3, "SystemInfoWidgetUpdate"

    const-string v4, "Request SystemInfoWidgetUpdate"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v0, "android.intent.action.SYSTEMINFO_UPDATE"

    .line 271
    .local v0, "WIDGET_UPDATE":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SYSTEMINFO_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    .local v1, "systemInfoWidgetIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 273
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 48
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 49
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "action":Ljava/lang/String;
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "onReceive action="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v14, "com.sec.factory.entry.REQUEST_FTCLIENT_START"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 53
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 54
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "com.sec.factory.entry.REQUEST_FTCLIENT_START => XML data parsing was failed."

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->startFactoryTestClientServiceAPO()V

    .line 191
    :cond_1
    :goto_0
    return-void

    .line 58
    :cond_2
    const-string v14, "com.sec.factory.entry.ACTION_GET_MEDIASCANNER_FINISHED"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 59
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 60
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "ACTION_GET_MEDIASCANNER_FINISHED => XML data parsing was failed."

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_3
    const-string v14, "SUPPORT_SEMI_FUNCTION_TEST"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 66
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleCommon;->requestCreateShortCut(I)V

    .line 70
    :cond_4
    const-string v14, "SUPPORT_WATERPROOF_TEST"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 71
    new-instance v10, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver$1;-><init>(Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;)V

    .line 76
    .local v10, "mTimerTask":Ljava/util/TimerTask;
    new-instance v13, Ljava/util/Timer;

    invoke-direct {v13}, Ljava/util/Timer;-><init>()V

    .line 77
    .local v13, "timer":Ljava/util/Timer;
    const-wide/16 v14, 0xc8

    invoke-virtual {v13, v10, v14, v15}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 81
    .end local v10    # "mTimerTask":Ljava/util/TimerTask;
    .end local v13    # "timer":Ljava/util/Timer;
    :cond_5
    const-string v14, "SUPPORT_MIC2_ECHO_TEST"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 82
    new-instance v10, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver$2;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver$2;-><init>(Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;)V

    .line 87
    .restart local v10    # "mTimerTask":Ljava/util/TimerTask;
    new-instance v13, Ljava/util/Timer;

    invoke-direct {v13}, Ljava/util/Timer;-><init>()V

    .line 88
    .restart local v13    # "timer":Ljava/util/Timer;
    const-wide/16 v14, 0x190

    invoke-virtual {v13, v10, v14, v15}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 92
    .end local v10    # "mTimerTask":Ljava/util/TimerTask;
    .end local v13    # "timer":Ljava/util/Timer;
    :cond_6
    const-string v14, "SUPPORT_WIFI_STRESS_TEST"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 93
    new-instance v10, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver$3;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver$3;-><init>(Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;)V

    .line 98
    .restart local v10    # "mTimerTask":Ljava/util/TimerTask;
    new-instance v13, Ljava/util/Timer;

    invoke-direct {v13}, Ljava/util/Timer;-><init>()V

    .line 99
    .restart local v13    # "timer":Ljava/util/Timer;
    const-wide/16 v14, 0x258

    invoke-virtual {v13, v10, v14, v15}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 102
    .end local v10    # "mTimerTask":Ljava/util/TimerTask;
    .end local v13    # "timer":Ljava/util/Timer;
    :cond_7
    const-string v14, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 103
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "android.provider.Telephony.SECRET_CODE"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v14}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    .line 105
    .local v7, "host":Ljava/lang/String;
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Host="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 108
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "ACTION_SECRET_CODE => XML data parsing was failed."

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_8
    const-string v14, "$$15"

    invoke-virtual {v14, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v14

    if-nez v14, :cond_9

    const-string v14, "FACTORY_TEST_UNDER_USER_BINARY"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 112
    :cond_9
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/factory/modules/ModuleCommon;->is15TestEnable()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 113
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->launchFactoryTest()V

    .line 114
    const/4 v14, 0x0

    sput-boolean v14, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mPrepare15Test:Z

    goto/16 :goto_0

    .line 116
    :cond_a
    const/4 v14, 0x1

    sput-boolean v14, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mPrepare15Test:Z

    goto/16 :goto_0

    .line 118
    :cond_b
    const-string v14, "7547"

    invoke-virtual {v14, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 119
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->launchMainHistoryView()V

    goto/16 :goto_0

    .line 120
    :cond_c
    const-string v14, "08"

    invoke-virtual {v14, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v14

    if-nez v14, :cond_d

    const-string v14, "FACTORY_TEST_UNDER_USER_BINARY"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 121
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->launchFactoryHistoryView()V

    goto/16 :goto_0

    .line 123
    .end local v7    # "host":Ljava/lang/String;
    :cond_e
    const-string v14, "android.intent.action.SECPHONE_READY"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 124
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "android.intent.action.SECPHONE_READY"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/4 v14, 0x1

    sput-boolean v14, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->misSecphoneReady:Z

    .line 126
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 130
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->systemInfoWidgetUpdate()V

    goto/16 :goto_0

    .line 132
    :cond_f
    const-string v14, "android.intent.action.GET_FTA"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_14

    .line 133
    const-string v4, ""

    .line 134
    .local v4, "fta_hw_ver":Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_10

    .line 135
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "ACTION_SECRET_CODE => XML data parsing was failed."

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_10
    const-string v14, "FTA_HW_VER"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 139
    .local v5, "fta_hw_ver_array":[Ljava/lang/String;
    const-string v14, "HW_REVISION"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 141
    .local v8, "hwid":Ljava/lang/String;
    array-length v14, v5

    const/4 v15, 0x1

    if-le v14, v15, :cond_12

    .line 142
    const/4 v11, -0x1

    .line 143
    .local v11, "max":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v14, v5

    if-ge v9, v14, :cond_13

    .line 144
    aget-object v14, v5, v9

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 145
    .local v12, "temp":[Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    const/4 v15, 0x0

    aget-object v15, v12, v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    if-lt v14, v15, :cond_11

    .line 146
    const/4 v14, 0x0

    aget-object v14, v12, v14

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    if-le v14, v11, :cond_11

    .line 147
    const/4 v14, 0x0

    aget-object v14, v12, v14

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 148
    const/4 v14, 0x1

    aget-object v14, v12, v14

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 143
    :cond_11
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 153
    .end local v9    # "i":I
    .end local v11    # "max":I
    .end local v12    # "temp":[Ljava/lang/String;
    :cond_12
    const-string v14, "FTA_HW_VER"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 155
    :cond_13
    const-string v14, "FTA_SW_VER"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 156
    .local v6, "fta_sw_ver":Ljava/lang/String;
    new-instance v9, Landroid/content/Intent;

    const-string v14, "android.intent.action.GET_FTA_RESPONSE"

    invoke-direct {v9, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .local v9, "i":Landroid/content/Intent;
    const-string v14, "fta_hw_ver"

    invoke-virtual {v9, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v14, "fta_sw_ver"

    invoke-virtual {v9, v14, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 160
    .end local v4    # "fta_hw_ver":Ljava/lang/String;
    .end local v5    # "fta_hw_ver_array":[Ljava/lang/String;
    .end local v6    # "fta_sw_ver":Ljava/lang/String;
    .end local v8    # "hwid":Ljava/lang/String;
    .end local v9    # "i":Landroid/content/Intent;
    :cond_14
    const-string v14, "com.sec.factory.entry.REQUEST_FACTORY_RESET"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_15

    .line 161
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->createFileNotiCheck()V

    goto/16 :goto_0

    .line 162
    :cond_15
    const-string v14, "com.sec.android.app.factorytest"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 163
    const-string v14, "COMMAND"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 164
    .local v3, "data":Ljava/lang/String;
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "COMMAND : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/4 v14, 0x6

    const/16 v15, 0x8

    invoke-virtual {v3, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 166
    const-string v14, "00"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_17

    .line 167
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModulePower;->setFactoryModeAtBatteryNode(Z)V

    .line 169
    const-string v14, "NEED_NOTI_AUDIO_MANAGER"

    const/4 v15, 0x1

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_16

    .line 170
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleAudio;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleAudio;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->sendToAudioManagerFTAOnOff(Z)V

    .line 173
    :cond_16
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "Release wakelock because of RIL\'s intent"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    :cond_17
    const-string v14, "01"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 175
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModulePower;->doWakeLock(Z)V

    .line 176
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/factory/modules/ModulePower;->sendDvfsLockIntent()V

    .line 177
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModulePower;->sendAlarmManagerOnOff(Z)V

    .line 178
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModulePower;->setFactoryModeAtBatteryNode(Z)V

    .line 179
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/factory/modules/ModuleCommon;->setSwitchFactoryState()V

    .line 181
    const-string v14, "NEED_NOTI_AUDIO_MANAGER"

    const/4 v15, 0x1

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_18

    .line 182
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleAudio;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleAudio;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->sendToAudioManagerFTAOnOff(Z)V

    .line 184
    :cond_18
    const-string v14, "NEED_LPM_MODE_SET"

    const/4 v15, 0x1

    invoke-static {v14, v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_19

    .line 185
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDevice;

    move-result-object v14

    const-string v15, "0"

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleDevice;->setLPMmode(Ljava/lang/String;)V

    .line 188
    :cond_19
    const-string v14, "UnprotectedFactoryTestBroadcastReceiver"

    const-string v15, "onReceive"

    const-string v16, "Acquire wakelock because of RIL\'s intent"

    invoke-static/range {v14 .. v16}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
