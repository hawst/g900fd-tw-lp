.class public Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FactoryTestBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;
    }
.end annotation


# static fields
.field private static final ACTION_REQUEST_RECONNECT:Ljava/lang/String; = "com.sec.atd.request_reconnect"

.field private static final NOTI_BAR_BODY_BOOT:Ljava/lang/String; = "Booting is completed"

.field private static final NOTI_BAR_TITLE_BOOT:Ljava/lang/String; = "Boot Completed"

.field private static final TAG:Ljava/lang/String; = "FactoryTestBroadcastReceiver"


# instance fields
.field private final MSG_CHECK_FAIL_CONNECTION_DIGITIZER:B

.field private final MSG_FINISH_COPYING_CP_DUMP:B

.field private fin:Ljava/io/FileInputStream;

.field private fout:Ljava/io/FileOutputStream;

.field private handler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 435
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->MSG_FINISH_COPYING_CP_DUMP:B

    .line 436
    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->MSG_CHECK_FAIL_CONNECTION_DIGITIZER:B

    .line 438
    new-instance v0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$1;-><init>(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)V

    iput-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->handler:Landroid/os/Handler;

    .line 453
    iput-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    .line 454
    iput-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    return-void
.end method

.method private CheckDigitizerConnection()V
    .locals 3

    .prologue
    .line 391
    const-string v0, "NG"

    const-string v1, "EPEN_DIGITIZER_CHECK"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v1, "CheckDigitizer"

    const-string v2, "Checking Fail Connection Digitize."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 395
    :cond_0
    return-void
.end method

.method private CheckTouchkeyLedOnTime()V
    .locals 5

    .prologue
    .line 516
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModulePower;->getTouchLedTime()I

    move-result v0

    .line 517
    .local v0, "mButtonLedTime":I
    if-eqz v0, :cond_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 518
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "button_key_light"

    const/16 v3, 0x5dc

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 520
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "CheckTouchkeyLedOnTime"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Forcely TouchKey Led Normal On Because mButtonLedTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private initFactoryAppFiles()V
    .locals 15

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 240
    new-instance v10, Ljava/io/File;

    const-string v0, "EFS_FACTORYAPP_ROOT_PATH"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    .local v10, "factoryAppRoot":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v3, "ModuleCommon"

    const-string v4, "Create DIR_EFS_FACTORYAPP_ROOT"

    invoke-static {v0, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v0, "EFS_FACTORYAPP_ROOT_PATH"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->mkDir(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const-string v0, "EFS_FACTORYAPP_ROOT_PATH"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v6}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->readFactoryMode()Ljava/lang/String;

    move-result-object v14

    .line 252
    .local v14, "value":Ljava/lang/String;
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 253
    const-string v0, "OFF"

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    const-string v3, "OFF"

    invoke-virtual {v0, v3}, Lcom/sec/factory/modules/ModuleCommon;->writeFactoryMode(Ljava/lang/String;)Z

    .line 255
    const-string v0, "FACTORY_MODE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v2

    invoke-static/range {v3 .. v9}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 257
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "OFFis changed..."

    invoke-static {v0, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_1
    :goto_0
    new-instance v11, Ljava/io/File;

    const-string v0, "KEYSTRING_BLOCK"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    .local v11, "mKeyStringBlock":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_6

    .line 271
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    const-string v3, "OFF"

    invoke-virtual {v0, v3}, Lcom/sec/factory/modules/ModuleCommon;->writeKeyStringBlock(Ljava/lang/String;)Z

    .line 272
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "KEYSTRING_BLOCK is created..."

    invoke-static {v0, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :goto_1
    const-string v0, "KEYSTRING_BLOCK"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v2

    invoke-static/range {v3 .. v9}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 282
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 283
    const-string v0, "sysfs"

    const-string v1, "RAM_SIZE_IF"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    const-string v0, "DEVICE_RAM_SIZE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 285
    .local v12, "mRamInfo":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->readRamSizeInfo()Ljava/lang/String;

    move-result-object v13

    .line 287
    .local v13, "mRamInfoFile":Ljava/lang/String;
    if-eqz v12, :cond_2

    .line 288
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 291
    :cond_2
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v1, "onReceive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEVICE_RAM_SIZE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v1, "onReceive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEVICE_RAM_SIZE_FILE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    if-eqz v13, :cond_3

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 295
    :cond_3
    if-eqz v12, :cond_7

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 296
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/sec/factory/modules/ModuleCommon;->writeRamSizeInfo(Ljava/lang/String;)Z

    .line 297
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v1, "onReceive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEVICE_RAM_SIZE_FILE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is updated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    .end local v12    # "mRamInfo":Ljava/lang/String;
    .end local v13    # "mRamInfoFile":Ljava/lang/String;
    :cond_4
    :goto_2
    return-void

    .line 260
    .end local v11    # "mKeyStringBlock":Ljava/io/File;
    :cond_5
    const-string v0, "ON"

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    const-string v3, "ON"

    invoke-virtual {v0, v3}, Lcom/sec/factory/modules/ModuleCommon;->writeFactoryMode(Ljava/lang/String;)Z

    .line 262
    const-string v0, "FACTORY_MODE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v2

    invoke-static/range {v3 .. v9}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 264
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "ONis changed..."

    invoke-static {v0, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 275
    .restart local v11    # "mKeyStringBlock":Ljava/io/File;
    :cond_6
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "KEYSTRING_BLOCK is already existed..."

    invoke-static {v0, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 299
    .restart local v12    # "mRamInfo":Ljava/lang/String;
    .restart local v13    # "mRamInfoFile":Ljava/lang/String;
    :cond_7
    if-nez v12, :cond_8

    .line 300
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v1, "onReceive"

    const-string v2, "mRamInfo is null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 301
    :cond_8
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 302
    iget-object v0, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/sec/factory/modules/ModuleCommon;->writeRamSizeInfo(Ljava/lang/String;)Z

    .line 303
    const-string v0, "FactoryTestBroadcastReceiver"

    const-string v1, "onReceive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEVICE_RAM_SIZE_FILE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is updated"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private launchFactoryTest()V
    .locals 4

    .prologue
    .line 346
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 347
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "launchFactoryTest"

    const-string v3, "Launch FactoryTestMain"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 349
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.factorytest.FactoryTestMain"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const/high16 v1, 0x34000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 352
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 353
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 358
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 355
    :cond_0
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "launchFactoryTest"

    const-string v3, "XML data parsing was not completed."

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    const-string v2, "XML data parsing was not completed."

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_0
.end method

.method private launchSystemInfo()V
    .locals 4

    .prologue
    .line 330
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "launchSystemInfo"

    const-string v3, "Start SystemInfoService"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v1, "SUPPORT_SYSTEM_INFO"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 333
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.systeminfo.SystemInfoService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 336
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private sendRTCAlarmOffIntent()V
    .locals 5

    .prologue
    .line 384
    const-string v2, "FactoryTestBroadcastReceiver"

    const-string v3, "sendRTCAlarmOffIntent"

    const-string v4, "android.intent.action.START_FACTORY_TEST"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    const-string v0, "android.intent.action.START_FACTORY_TEST"

    .line 386
    .local v0, "RTC_OFF":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.START_FACTORY_TEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 387
    .local v1, "alarmManagerOffIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 388
    return-void
.end method

.method private startDummyFtClientForBootCompleted()V
    .locals 4

    .prologue
    .line 321
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "startDummyFtClientForBootCompleted"

    const-string v3, "start DummyFtClient service for APO"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 324
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.aporiented.DummyFtClient"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 326
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 327
    return-void
.end method

.method private startFactoryTestClientServiceAPO()V
    .locals 4

    .prologue
    .line 312
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "startFactoryTestClientServiceAPO"

    const-string v3, "start FactoryTestClient service for APO"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 315
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.aporiented.FtClient"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 317
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommon;->setFtClientState(Z)Z

    .line 318
    return-void
.end method

.method private startFactoryTestSupportService()V
    .locals 4

    .prologue
    .line 361
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "startFactoryTestSupportService"

    const-string v3, "Start FactoryTestSupportService"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 363
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.factorytest.FactoryTestSupportService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 365
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 366
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 367
    return-void
.end method

.method private startServiceBatterLED()V
    .locals 4

    .prologue
    .line 370
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "startServiceBatteryLED"

    const-string v3, "Start ServiceBatteryLED"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 372
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.service.ServiceBatteryLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 374
    return-void
.end method

.method private startServiceTouchkeyLED()V
    .locals 4

    .prologue
    .line 377
    const-string v1, "FactoryTestBroadcastReceiver"

    const-string v2, "startServiceTouchkeyLED"

    const-string v3, "Start ServiceTouchkeyLED"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 379
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.app.service.ServiceTouchkeyLED"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 380
    iget-object v1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 381
    return-void
.end method

.method private systemInfoWidgetUpdate()V
    .locals 5

    .prologue
    .line 339
    const-string v2, "FactoryTestBroadcastReceiver"

    const-string v3, "SystemInfoWidgetUpdate"

    const-string v4, "Request SystemInfoWidgetUpdate"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v0, "android.intent.action.SYSTEMINFO_UPDATE"

    .line 341
    .local v0, "WIDGET_UPDATE":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SYSTEMINFO_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .local v1, "systemInfoWidgetIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 343
    return-void
.end method

.method private writeSOC()V
    .locals 5

    .prologue
    .line 221
    const/4 v0, 0x0

    .line 222
    .local v0, "isresetneeded":Z
    const-string v3, "AT_BATTEST_RESET_WHEN_READ"

    invoke-static {v3}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 224
    if-eqz v0, :cond_1

    .line 225
    iget-object v3, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModulePower;->resetFuelGaugeIC()Z

    .line 230
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModulePower;->readBatteryVoltage()Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "result":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v3, "efs/FactoryApp/SOC_Data"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 233
    .local v2, "socDataFile":Ljava/io/File;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 234
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/factory/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 236
    :cond_0
    return-void

    .line 227
    .end local v1    # "result":Ljava/lang/String;
    .end local v2    # "socDataFile":Ljava/io/File;
    :cond_1
    const-string v3, "BATTERY_UPDATE_BEFORE_READ"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method public copyDirectory(Ljava/io/File;Ljava/io/File;)V
    .locals 11
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dest"    # Ljava/io/File;

    .prologue
    .line 457
    const-string v7, "FactoryTestBroadcastReceiver"

    const-string v8, "copyDirectory"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "src : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", dest : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 460
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 461
    invoke-virtual {p2}, Ljava/io/File;->mkdir()Z

    .line 464
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 465
    .local v3, "fileList":[Ljava/lang/String;
    invoke-virtual {p2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 467
    .local v1, "destfileList":[Ljava/lang/String;
    if-eqz v3, :cond_1

    array-length v7, v3

    if-gtz v7, :cond_2

    .line 513
    .end local v1    # "destfileList":[Ljava/lang/String;
    .end local v3    # "fileList":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 471
    .restart local v1    # "destfileList":[Ljava/lang/String;
    .restart local v3    # "fileList":[Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v7, v3

    if-ge v5, v7, :cond_1

    .line 472
    if-eqz v1, :cond_3

    array-length v7, v1

    if-ge v5, v7, :cond_3

    aget-object v7, v3, v5

    aget-object v8, v1, v5

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 473
    const-string v7, "FactoryTestBroadcastReceiver"

    const-string v8, "copyDirectory"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File already Exists: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v1, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 477
    :cond_3
    new-instance v7, Ljava/io/File;

    aget-object v8, v3, v5

    invoke-direct {v7, p1, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    aget-object v9, v3, v5

    invoke-direct {v8, p2, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v7, v8}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    goto :goto_2

    .line 481
    .end local v1    # "destfileList":[Ljava/lang/String;
    .end local v3    # "fileList":[Ljava/lang/String;
    .end local v5    # "i":I
    :cond_4
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    .line 482
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    .line 484
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 487
    .local v0, "buffer":[B
    :goto_3
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    invoke-virtual {v7, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    .local v6, "len":I
    if-lez v6, :cond_6

    .line 488
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v8, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 493
    .end local v0    # "buffer":[B
    .end local v6    # "len":I
    :catch_0
    move-exception v4

    .line 494
    .local v4, "fnfe":Ljava/io/FileNotFoundException;
    :try_start_1
    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v8, "// Exception from"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 498
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    if-eqz v7, :cond_5

    .line 500
    :try_start_2
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8

    .line 505
    :cond_5
    :goto_4
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    if-eqz v7, :cond_1

    .line 507
    :try_start_3
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 508
    :catch_1
    move-exception v7

    goto/16 :goto_0

    .line 491
    .end local v4    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v0    # "buffer":[B
    .restart local v6    # "len":I
    :cond_6
    :try_start_4
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 492
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/FileDescriptor;->sync()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 498
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    if-eqz v7, :cond_7

    .line 500
    :try_start_5
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9

    .line 505
    :cond_7
    :goto_5
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    if-eqz v7, :cond_1

    .line 507
    :try_start_6
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 508
    :catch_2
    move-exception v7

    goto/16 :goto_0

    .line 495
    .end local v0    # "buffer":[B
    .end local v6    # "len":I
    :catch_3
    move-exception v2

    .line 496
    .local v2, "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 498
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    if-eqz v7, :cond_8

    .line 500
    :try_start_8
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    .line 505
    :cond_8
    :goto_6
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    if-eqz v7, :cond_1

    .line 507
    :try_start_9
    iget-object v7, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_0

    .line 508
    :catch_4
    move-exception v7

    goto/16 :goto_0

    .line 498
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    if-eqz v8, :cond_9

    .line 500
    :try_start_a
    iget-object v8, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fin:Ljava/io/FileInputStream;

    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 505
    :cond_9
    :goto_7
    iget-object v8, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    if-eqz v8, :cond_a

    .line 507
    :try_start_b
    iget-object v8, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->fout:Ljava/io/FileOutputStream;

    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    .line 509
    :cond_a
    :goto_8
    throw v7

    .line 508
    :catch_5
    move-exception v8

    goto :goto_8

    .line 501
    :catch_6
    move-exception v8

    goto :goto_7

    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v7

    goto :goto_6

    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v4    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_8
    move-exception v7

    goto :goto_4

    .end local v4    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v0    # "buffer":[B
    .restart local v6    # "len":I
    :catch_9
    move-exception v7

    goto :goto_5
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 43
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 44
    .local v5, "action":Ljava/lang/String;
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "onReceive action="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v15, "android.intent.action.PRE_BOOT_COMPLETED"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 47
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 48
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "ACTION_PRE_BOOT_COMPLETED => XML data parsing was failed."

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->initFactoryAppFiles()V

    .line 217
    :cond_1
    :goto_0
    return-void

    .line 53
    :cond_2
    const-string v15, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_19

    .line 54
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 55
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "ACTION_BOOT_COMPLETED => XML data parsing was failed."

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->initFactoryAppFiles()V

    .line 60
    const-string v15, "INBATT_SAVE_SOC"

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    if-eqz v15, :cond_4

    const-string v15, "JIG_CONNECTION_CHECK"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "JIG"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 62
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->writeSOC()V

    .line 65
    :cond_4
    const-string v15, "factory"

    const-string v16, "BINARY_TYPE"

    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 66
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "Boot completed, IS_FACTORY_BINARY = factory"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "screen_off_timeout"

    const v17, 0x927c0

    invoke-static/range {v15 .. v17}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 69
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "multi_window_enabled"

    const/16 v17, 0x0

    const/16 v18, -0x2

    invoke-static/range {v15 .. v18}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 70
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v15

    const/16 v16, 0x5dc

    invoke-virtual/range {v15 .. v16}, Lcom/sec/factory/modules/ModulePower;->setTouchLedTime(I)V

    .line 72
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->setSwitchFactoryState()V

    .line 73
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->enableFtClient()Z

    .line 74
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->sendRTCAlarmOffIntent()V

    .line 75
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startFactoryTestSupportService()V

    .line 78
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->systemInfoWidgetUpdate()V

    .line 79
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->CheckTouchkeyLedOnTime()V

    .line 80
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->CheckDigitizerConnection()V

    .line 81
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->enable15Test()Z

    .line 83
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 84
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startFactoryTestClientServiceAPO()V

    .line 87
    :cond_5
    const-string v15, "FRS_TYPE_EFS"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 88
    new-instance v10, Ljava/io/File;

    const-string v15, "FACTORY_FAILHIST_FRS"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v10, "frs":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 91
    const-string v15, "FACTORY_FAILHIST_FRS"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 92
    .local v12, "res":Ljava/lang/String;
    const-string v15, ":"

    invoke-virtual {v12, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 93
    .local v13, "splitStr":[Ljava/lang/String;
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "readFailHist for FRS"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "res = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v15, "CPDUMP_SET"

    const/16 v16, 0x0

    aget-object v16, v13, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 95
    const-string v15, "MDM"

    const-string v16, "CHIPSET_NAME_CP"

    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x3

    invoke-virtual/range {v16 .. v18}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 96
    const-string v15, "FAILDUMP_TIME_CP_CRASH"

    const/16 v16, 0x1

    aget-object v16, v13, v16

    invoke-static/range {v15 .. v16}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v2, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;-><init>(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)V

    .line 98
    .local v2, "Excuete_CopyDump":Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;
    new-instance v14, Ljava/lang/Thread;

    invoke-direct {v14, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 99
    .local v14, "t":Ljava/lang/Thread;
    invoke-virtual {v14}, Ljava/lang/Thread;->start()V

    .line 121
    .end local v2    # "Excuete_CopyDump":Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;
    .end local v10    # "frs":Ljava/io/File;
    .end local v12    # "res":Ljava/lang/String;
    .end local v13    # "splitStr":[Ljava/lang/String;
    .end local v14    # "t":Ljava/lang/Thread;
    :cond_6
    :goto_1
    const-string v15, "FLS_TYPE_EFS"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 122
    new-instance v9, Ljava/io/File;

    const-string v15, "FACTORY_FAILHIST_FLS"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v9, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v9, "fls":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_e

    .line 124
    const-string v15, "FACTORY_FAILHIST_FLS"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 125
    .restart local v12    # "res":Ljava/lang/String;
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "readFailHist for FLS"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "res = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "clear failhist for FLS"

    const-string v17, "file exits"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 136
    .end local v9    # "fls":Ljava/io/File;
    .end local v12    # "res":Ljava/lang/String;
    :goto_2
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "Display NotiBar"

    const-string v17, "Display NotiBar with [Boot Completed]"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v15, "Boot Completed"

    const-string v16, "Booting is completed"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v15, v1}, Lcom/sec/factory/entry/DisplayNotiBar;->createNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v15, "JIG_CONNECTION_CHECK"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, "JigConnection":Ljava/lang/String;
    const-string v15, "OCTA_CONNECTION_CHECK"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 143
    .local v4, "OctaConnection":Ljava/lang/String;
    const-string v15, "0 0 0"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_7

    const-string v15, "00 00 00"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 144
    :cond_7
    if-eqz v3, :cond_10

    const-string v15, "JIG"

    invoke-virtual {v3, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 145
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "BOOT_CHECK_TOUCHKEYLED"

    const-string v17, "Jig connected"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_8
    :goto_3
    const/4 v6, 0x0

    .line 157
    .local v6, "autoBrightness":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 159
    .local v7, "contentResolver":Landroid/content/ContentResolver;
    :try_start_0
    const-string v15, "screen_brightness_mode"

    invoke-static {v7, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_12

    const/4 v6, 0x1

    .line 164
    :goto_4
    const/4 v15, 0x1

    if-ne v6, v15, :cond_9

    .line 165
    const-string v15, "screen_brightness_mode"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v7, v15, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 169
    :cond_9
    sget-boolean v15, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mPrepare15Test:Z

    if-eqz v15, :cond_1

    .line 170
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->launchFactoryTest()V

    .line 171
    const/4 v15, 0x0

    sput-boolean v15, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->mPrepare15Test:Z

    goto/16 :goto_0

    .line 101
    .end local v3    # "JigConnection":Ljava/lang/String;
    .end local v4    # "OctaConnection":Ljava/lang/String;
    .end local v6    # "autoBrightness":Z
    .end local v7    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v10    # "frs":Ljava/io/File;
    .restart local v12    # "res":Ljava/lang/String;
    .restart local v13    # "splitStr":[Ljava/lang/String;
    :cond_a
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "clear failhist for FRS"

    const-string v17, "file exits"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 105
    :cond_b
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "clear failhist for FRS"

    const-string v17, "file not exits"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 109
    .end local v10    # "frs":Ljava/io/File;
    .end local v12    # "res":Ljava/lang/String;
    .end local v13    # "splitStr":[Ljava/lang/String;
    :cond_c
    const-string v15, "CPDUMP_SET"

    const-string v16, "FAILDUMP_CP"

    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 110
    const-string v15, "MDM"

    const-string v16, "CHIPSET_NAME_CP"

    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x3

    invoke-virtual/range {v16 .. v18}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 111
    new-instance v2, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;-><init>(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)V

    .line 112
    .restart local v2    # "Excuete_CopyDump":Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;
    new-instance v14, Ljava/lang/Thread;

    invoke-direct {v14, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 113
    .restart local v14    # "t":Ljava/lang/Thread;
    invoke-virtual {v14}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 115
    .end local v2    # "Excuete_CopyDump":Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;
    .end local v14    # "t":Ljava/lang/Thread;
    :cond_d
    const-string v15, "FAILDUMP_CP"

    const-string v16, "NONE"

    invoke-static/range {v15 .. v16}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v15, "FAILDUMP_TIME_CP_CRASH"

    const-string v16, "NONE"

    invoke-static/range {v15 .. v16}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 129
    .restart local v9    # "fls":Ljava/io/File;
    :cond_e
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "clear failhist for FLS"

    const-string v17, "file not exits"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 132
    .end local v9    # "fls":Ljava/io/File;
    :cond_f
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "Read Prop : FACTORYAPP_FAILHIST_FLS"

    const-string v17, "FACTORYAPP_FAILHIST_FLS"

    invoke-static/range {v17 .. v17}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v15, "FACTORYAPP_FAILHIST_FLS"

    const-string v16, "NONE"

    invoke-static/range {v15 .. v16}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 147
    .restart local v3    # "JigConnection":Ljava/lang/String;
    .restart local v4    # "OctaConnection":Ljava/lang/String;
    :cond_10
    const-string v15, "BOOT_CHECK_TOUCHKEY_WO_SVCLED"

    invoke-static {v15}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 148
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startServiceTouchkeyLED()V

    goto/16 :goto_3

    .line 150
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startServiceBatterLED()V

    goto/16 :goto_3

    .line 159
    .restart local v6    # "autoBrightness":Z
    .restart local v7    # "contentResolver":Landroid/content/ContentResolver;
    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 161
    :catch_0
    move-exception v8

    .line 162
    .local v8, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v8}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_4

    .line 174
    .end local v3    # "JigConnection":Ljava/lang/String;
    .end local v4    # "OctaConnection":Ljava/lang/String;
    .end local v6    # "autoBrightness":Z
    .end local v7    # "contentResolver":Landroid/content/ContentResolver;
    .end local v8    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_13
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "Boot completed, IS_FACTORY_BINARY = USER MODE"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->getFtClientEnableState()Z

    move-result v15

    if-nez v15, :cond_14

    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->connectedJIG()Z

    move-result v15

    if-eqz v15, :cond_16

    .line 179
    :cond_14
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v15

    if-eqz v15, :cond_15

    .line 180
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startFactoryTestClientServiceAPO()V

    .line 190
    :cond_15
    :goto_5
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v15

    if-eqz v15, :cond_18

    .line 191
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "ACTION_BOOT_COMPLETED : isFactoryMode == true"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->enableFtClient()Z

    .line 193
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->sendRTCAlarmOffIntent()V

    .line 194
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startFactoryTestSupportService()V

    .line 197
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->systemInfoWidgetUpdate()V

    goto/16 :goto_0

    .line 182
    :cond_16
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v15

    if-eqz v15, :cond_17

    .line 183
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "ACTION_BOOT_COMPLETED startFactoryTestClientServiceAPO"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startFactoryTestClientServiceAPO()V

    goto :goto_5

    .line 186
    :cond_17
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->isRunningFtClient()Z

    move-result v15

    if-nez v15, :cond_15

    .line 187
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startDummyFtClientForBootCompleted()V

    goto :goto_5

    .line 199
    :cond_18
    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->disableFtClient()Z

    goto/16 :goto_0

    .line 202
    :cond_19
    const-string v15, "com.sec.atd.request_reconnect"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1b

    .line 204
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "com.sec.atd.request_reconnect"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    new-instance v15, Landroid/content/Intent;

    const-class v16, Lcom/sec/factory/aporiented/FtClient;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 207
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_1a

    .line 208
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "ACTION_REQUEST_RECONNECT => XML data parsing was failed."

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->startFactoryTestClientServiceAPO()V

    goto/16 :goto_0

    .line 212
    :cond_1b
    const-string v15, "android.intent.action.CSC_MODEM_SETTING"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 213
    const-string v15, "FactoryTestBroadcastReceiver"

    const-string v16, "onReceive"

    const-string v17, "get CSC_MODEM_SETTING in FactoryTestBroadcastReceiver"

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    new-instance v11, Landroid/content/Intent;

    const-string v15, "android.intent.action.CSC_MODEM_SETTING_FACTORY"

    invoke-direct {v11, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 215
    .local v11, "mIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v15, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
