.class Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;
.super Ljava/lang/Object;
.source "FactoryTestBroadcastReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Excuete_CopyDump"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;->this$0:Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 399
    const-string v8, "FactoryTestBroadcastReceiver"

    const-string v9, "Excuete_CopyDump"

    const-string v10, "COPY"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    new-instance v6, Ljava/io/File;

    const-string v8, "/mnt/sdcard/"

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v6, "sdcardDirectory":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const-string v8, "/mnt/sdcard/log"

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 403
    .local v7, "sdcardLogDirectory":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v8, "/tombstones/mdm"

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 404
    .local v2, "dataCPCrashLogDirectory":Ljava/io/File;
    const-string v8, "FAILDUMP_TIME_CP_CRASH"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 405
    .local v1, "currentTime":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/sdcard/log/CpDump_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, "cpdumpdatefolder":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 408
    .local v5, "sdcardCPCrashLogDirectory":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 409
    invoke-virtual {v7}, Ljava/io/File;->mkdir()Z

    .line 413
    :cond_0
    const-wide/16 v8, 0x2710

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :goto_0
    iget-object v8, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;->this$0:Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;

    invoke-virtual {v8, v2, v5}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 420
    const-string v8, "FRS_TYPE_EFS"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 421
    new-instance v4, Ljava/io/File;

    const-string v8, "FACTORY_FAILHIST_FRS"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 422
    .local v4, "frs":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 423
    const-string v8, "FactoryTestBroadcastReceiver"

    const-string v9, "clear failhist for FRS"

    const-string v10, "file exits"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string v8, "FAILDUMP_TIME_CP_CRASH"

    const-string v9, "NONE"

    invoke-static {v8, v9}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 431
    .end local v4    # "frs":Ljava/io/File;
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;->this$0:Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;

    # getter for: Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->access$000(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver$Excuete_CopyDump;->this$0:Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;

    # getter for: Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->handler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;->access$000(Lcom/sec/factory/entry/FactoryTestBroadcastReceiver;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 432
    return-void

    .line 414
    :catch_0
    move-exception v3

    .line 415
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 428
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_2
    const-string v8, "FAILDUMP_CP"

    const-string v9, "NONE"

    invoke-static {v8, v9}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v8, "FAILDUMP_TIME_CP_CRASH"

    const-string v9, "NONE"

    invoke-static {v8, v9}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
