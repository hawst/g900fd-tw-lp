.class public Lcom/sec/factory/app/ui/UIFactoryHistory;
.super Landroid/app/Activity;
.source "UIFactoryHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;,
        Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "UIFactoryHistory"


# instance fields
.field private final GET_HISTORY_VIEW_ITEM_ACTION:Ljava/lang/String;

.field private mAdaptor:Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mHistoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    .line 62
    const-string v0, "com.android.samsungtest.RilOmissionCommand"

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->GET_HISTORY_VIEW_ITEM_ACTION:Ljava/lang/String;

    .line 101
    new-instance v0, Lcom/sec/factory/app/ui/UIFactoryHistory$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/app/ui/UIFactoryHistory$1;-><init>(Lcom/sec/factory/app/ui/UIFactoryHistory;)V

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 250
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/app/ui/UIFactoryHistory;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/app/ui/UIFactoryHistory;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/factory/app/ui/UIFactoryHistory;->getItemIDforPGM(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/app/ui/UIFactoryHistory;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/app/ui/UIFactoryHistory;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/factory/app/ui/UIFactoryHistory;->parseNVHistoryCPO(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/factory/app/ui/UIFactoryHistory;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/app/ui/UIFactoryHistory;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryHistory;->stopReceiver()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/factory/app/ui/UIFactoryHistory;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/app/ui/UIFactoryHistory;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    return-object v0
.end method

.method private convertResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 185
    const-string v0, "N"

    .line 187
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "50"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    const-string v0, "P"

    .line 197
    :cond_0
    :goto_0
    return-object v0

    .line 189
    :cond_1
    const-string v1, "45"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 190
    const-string v0, "E"

    goto :goto_0

    .line 191
    :cond_2
    const-string v1, "46"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    const-string v0, "F"

    goto :goto_0

    .line 193
    :cond_3
    const-string v1, "4E"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    const-string v0, "N"

    goto :goto_0
.end method

.method private getItemIDforPGM(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 201
    const-string v0, "NA"

    .line 203
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "01"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    const-string v0, "SMD FUNCTION"

    .line 247
    :cond_0
    :goto_0
    return-object v0

    .line 205
    :cond_1
    const-string v1, "04"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    const-string v0, "PBA TEST"

    goto :goto_0

    .line 207
    :cond_2
    const-string v1, "07"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 208
    const-string v0, "RF CAL"

    goto :goto_0

    .line 209
    :cond_3
    const-string v1, "08"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 210
    const-string v0, "RF CAL2"

    goto :goto_0

    .line 211
    :cond_4
    const-string v1, "0A"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 212
    const-string v0, "LTE CAL"

    goto :goto_0

    .line 213
    :cond_5
    const-string v1, "0B"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 214
    const-string v0, "FINAL 1"

    goto :goto_0

    .line 215
    :cond_6
    const-string v1, "0C"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 216
    const-string v0, "LTE FINAL"

    goto :goto_0

    .line 217
    :cond_7
    const-string v1, "0E"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 218
    const-string v0, "WLAN"

    goto :goto_0

    .line 219
    :cond_8
    const-string v1, "0F"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 220
    const-string v0, "FINAL 2"

    goto :goto_0

    .line 221
    :cond_9
    const-string v1, "10"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 222
    const-string v0, "IMEI WRITE"

    goto :goto_0

    .line 223
    :cond_a
    const-string v1, "12"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 224
    const-string v0, "FUNC 3"

    goto/16 :goto_0

    .line 225
    :cond_b
    const-string v1, "13"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 226
    const-string v0, "FUNC 1"

    goto/16 :goto_0

    .line 227
    :cond_c
    const-string v1, "14"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 228
    const-string v0, "FUNC 2"

    goto/16 :goto_0

    .line 229
    :cond_d
    const-string v1, "62"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 230
    const-string v0, "PBA Repair"

    goto/16 :goto_0

    .line 231
    :cond_e
    const-string v1, "63"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 232
    const-string v0, "MAIN Repair"

    goto/16 :goto_0

    .line 233
    :cond_f
    const-string v1, "1F"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 234
    const-string v0, "MAIN FUNCTION"

    goto/16 :goto_0

    .line 235
    :cond_10
    const-string v1, "41"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 236
    const-string v0, "MOT TEST"

    goto/16 :goto_0

    .line 237
    :cond_11
    const-string v1, "42"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 238
    const-string v0, "CAMERA MAIN"

    goto/16 :goto_0

    .line 239
    :cond_12
    const-string v1, "B1"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 240
    const-string v0, "S_CAL"

    goto/16 :goto_0

    .line 241
    :cond_13
    const-string v1, "5A"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 242
    const-string v0, "Pre_FINAL"

    goto/16 :goto_0

    .line 243
    :cond_14
    const-string v1, "69"

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    const-string v0, "Pre_WLAN"

    goto/16 :goto_0
.end method

.method private parseNVHistory()V
    .locals 11

    .prologue
    const/16 v10, 0xa

    .line 116
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Response (NV History) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " millisecond"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/sec/factory/support/NVAccessor;->getNVHistory()Ljava/lang/String;

    move-result-object v2

    .line 120
    .local v2, "history":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 121
    :cond_0
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    const-string v6, "No Item"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 123
    :cond_1
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Data] size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-static {}, Lcom/sec/factory/support/NVAccessor;->supportExpandedNV()Z

    move-result v4

    if-nez v4, :cond_2

    .line 129
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    const-string v6, "NV XX"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 132
    add-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "NVKey":Ljava/lang/String;
    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v5, v3, 0x3

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "NVValue":Ljava/lang/String;
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NVKey : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", NVValue : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-static {v0, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    iget-object v4, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    new-instance v5, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;

    invoke-direct {v5, p0, v0, v1}, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;-><init>(Lcom/sec/factory/app/ui/UIFactoryHistory;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    add-int/lit8 v3, v3, 0x3

    goto :goto_1

    .line 140
    .end local v0    # "NVKey":Ljava/lang/String;
    .end local v1    # "NVValue":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_2
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    const-string v6, "NV XXX"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 143
    add-int/lit8 v4, v3, 0x3

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 144
    .restart local v0    # "NVKey":Ljava/lang/String;
    add-int/lit8 v4, v3, 0x3

    add-int/lit8 v5, v3, 0x4

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 145
    .restart local v1    # "NVValue":Ljava/lang/String;
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NVKey : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", NVValue : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-static {v0, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    iget-object v4, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    new-instance v5, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;

    invoke-direct {v5, p0, v0, v1}, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;-><init>(Lcom/sec/factory/app/ui/UIFactoryHistory;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v3, v3, 0x4

    goto :goto_2

    .line 153
    .end local v0    # "NVKey":Ljava/lang/String;
    .end local v1    # "NVValue":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mAdaptor:Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;

    invoke-virtual {v4}, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method private parseNVHistoryCPO(Ljava/lang/String;)V
    .locals 10
    .param p1, "histNVData"    # Ljava/lang/String;

    .prologue
    .line 158
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Response (NV History) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " millisecond"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    move-object v2, p1

    .line 162
    .local v2, "history":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 163
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    const-string v6, "No Item"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :goto_0
    return-void

    .line 165
    :cond_0
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Data] size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 169
    add-int/lit8 v4, v3, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "NVKey":Ljava/lang/String;
    add-int/lit8 v4, v3, 0x2

    add-int/lit8 v5, v3, 0x4

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "NVValue":Ljava/lang/String;
    const-string v4, "UIFactoryHistory"

    const-string v5, "parseNVHistory"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NVKey : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", NVValue : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v4, "00"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 175
    iget-object v4, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    new-instance v5, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;

    invoke-direct {p0, v1}, Lcom/sec/factory/app/ui/UIFactoryHistory;->convertResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p0, v0, v6}, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryItem;-><init>(Lcom/sec/factory/app/ui/UIFactoryHistory;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_1
    add-int/lit8 v3, v3, 0x4

    goto :goto_1

    .line 180
    .end local v0    # "NVKey":Ljava/lang/String;
    .end local v1    # "NVValue":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mAdaptor:Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;

    invoke-virtual {v4}, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private setupReceiver()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.android.samsungtest.RilOmissionCommand"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/factory/app/ui/UIFactoryHistory;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 95
    return-void
.end method

.method private stopReceiver()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/factory/app/ui/UIFactoryHistory;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 99
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/factory/app/ui/UIFactoryHistory;->setContentView(I)V

    .line 67
    new-instance v0, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;

    const v1, 0x7f030002

    iget-object v2, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;-><init>(Lcom/sec/factory/app/ui/UIFactoryHistory;Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mAdaptor:Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;

    .line 68
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lcom/sec/factory/app/ui/UIFactoryHistory;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mListView:Landroid/widget/ListView;

    .line 69
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mAdaptor:Lcom/sec/factory/app/ui/UIFactoryHistory$HistoryAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 70
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 74
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryHistory;->mHistoryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 76
    const-string v0, "UIFactoryHistory"

    const-string v1, "onResume"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Request (NV History) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " millisecond"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryHistory;->parseNVHistory()V

    .line 91
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryHistory;->setupReceiver()V

    goto :goto_0
.end method
