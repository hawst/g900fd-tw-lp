.class public Lcom/sec/factory/app/ui/UIFactoryTestNVView;
.super Landroid/app/Activity;
.source "UIFactoryTestNVView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final ITEM_MAX_SIZE:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mItemValue:[B

.field mPassItemValue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResultValue:[C

.field private mView1:Landroid/widget/TextView;

.field private mView2:Landroid/widget/TextView;

.field private mView3:Landroid/widget/TextView;

.field private mView4:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x5a

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const-string v0, "FactoryTestNVView"

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->CLASS_NAME:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->ITEM_MAX_SIZE:I

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mPassItemValue:Ljava/util/List;

    .line 32
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mItemValue:[B

    .line 33
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mResultValue:[C

    .line 35
    new-instance v0, Lcom/sec/factory/app/ui/UIFactoryTestNVView$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView$1;-><init>(Lcom/sec/factory/app/ui/UIFactoryTestNVView;)V

    iput-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private Change_NV_Value(B)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # B

    .prologue
    .line 115
    const/16 v0, 0x4e

    if-ne v0, p1, :cond_0

    .line 116
    const-string v0, "N"

    .line 124
    :goto_0
    return-object v0

    .line 117
    :cond_0
    const/16 v0, 0x45

    if-ne v0, p1, :cond_1

    .line 118
    const-string v0, "E"

    goto :goto_0

    .line 119
    :cond_1
    const/16 v0, 0x50

    if-ne v0, p1, :cond_2

    .line 120
    const-string v0, "P"

    goto :goto_0

    .line 121
    :cond_2
    const/16 v0, 0x46

    if-ne v0, p1, :cond_3

    .line 122
    const-string v0, "F"

    goto :goto_0

    .line 124
    :cond_3
    const-string v0, "NONE"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/factory/app/ui/UIFactoryTestNVView;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/app/ui/UIFactoryTestNVView;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->getFactoryTestNVValueCPO(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/factory/app/ui/UIFactoryTestNVView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/app/ui/UIFactoryTestNVView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->getFactoryTestNVValueCPO()V

    return-void
.end method

.method private getFactoryTestNVValue()V
    .locals 3

    .prologue
    .line 84
    const-string v0, "FactoryTestNVView"

    const-string v1, "getFactoryTestNVValue"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView1:Landroid/widget/TextView;

    const/4 v1, 0x1

    const/16 v2, 0x17

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_Value(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView2:Landroid/widget/TextView;

    const/16 v1, 0x18

    const/16 v2, 0x2e

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_Value(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView3:Landroid/widget/TextView;

    const/16 v1, 0x2f

    const/16 v2, 0x45

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_Value(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView4:Landroid/widget/TextView;

    const/16 v1, 0x46

    const/16 v2, 0x5a

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_Value(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    return-void
.end method

.method private getFactoryTestNVValueCPO()V
    .locals 3

    .prologue
    .line 156
    const-string v0, "FactoryTestNVView"

    const-string v1, "getFactoryTestNVValueCPO"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView1:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/16 v2, 0x16

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_ValueCPO(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView2:Landroid/widget/TextView;

    const/16 v1, 0x17

    const/16 v2, 0x2d

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_ValueCPO(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView3:Landroid/widget/TextView;

    const/16 v1, 0x2e

    const/16 v2, 0x44

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_ValueCPO(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView4:Landroid/widget/TextView;

    const/16 v1, 0x45

    const/16 v2, 0x59

    invoke-direct {p0, v1, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->get_NV_ValueCPO(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    return-void
.end method

.method private getFactoryTestNVValueCPO(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 182
    const-string v5, "FactoryTestNVView"

    const-string v6, "getFactoryTestNVValueCPO"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v5, "COMMAND"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 185
    .local v4, "valuesString":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 186
    const-string v4, ""

    .line 193
    :cond_0
    const/4 v0, 0x6

    .local v0, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 194
    const-string v1, "PASS"

    .line 195
    .local v1, "mTempItemValue":Ljava/lang/String;
    const-string v2, "FAIL"

    .line 196
    .local v2, "mTempItemValue2":Ljava/lang/String;
    add-int/lit8 v5, v0, 0x2

    add-int/lit8 v6, v0, 0x4

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 197
    .local v3, "mTempResultValue":Ljava/lang/String;
    const-string v5, "FactoryTestNVView"

    const-string v6, "getFactoryTestNVValueCPO"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mTempResultValue : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v5, "50"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 200
    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mPassItemValue:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    :goto_1
    add-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 202
    :cond_1
    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mPassItemValue:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 206
    .end local v1    # "mTempItemValue":Ljava/lang/String;
    .end local v2    # "mTempItemValue2":Ljava/lang/String;
    .end local v3    # "mTempResultValue":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    :goto_2
    const/16 v5, 0x5a

    if-ge v0, v5, :cond_4

    .line 207
    const-string v5, "FactoryTestNVView"

    const-string v6, "getFactoryTestNVValueCPO"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "i : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v6, "FactoryTestNVView"

    const-string v7, "getFactoryTestNVValueCPO"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mPassItemValue.get(i) : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mPassItemValue:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v5, "PASS"

    iget-object v6, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mPassItemValue:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 211
    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mItemValue:[B

    add-int/lit8 v6, v0, 0x1

    int-to-byte v6, v6

    aput-byte v6, v5, v0

    .line 212
    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mResultValue:[C

    const/16 v6, 0x50

    aput-char v6, v5, v0

    .line 206
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 214
    :cond_3
    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mItemValue:[B

    add-int/lit8 v6, v0, 0x1

    int-to-byte v6, v6

    aput-byte v6, v5, v0

    .line 215
    iget-object v5, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mResultValue:[C

    const/16 v6, 0x4e

    aput-char v6, v5, v0

    goto :goto_3

    .line 218
    :cond_4
    return-void
.end method

.method private get_NV_Value(II)Ljava/lang/String;
    .locals 6
    .param p1, "nv_key_start"    # I
    .param p2, "nv_key_end"    # I

    .prologue
    .line 92
    const-string v2, "FactoryTestNVView"

    const-string v3, "get_NV_Value"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v1, ""

    .line 96
    .local v1, "mTemp":Ljava/lang/String;
    :goto_0
    if-gt p1, p2, :cond_1

    .line 97
    int-to-byte v2, p1

    invoke-static {v2}, Lcom/sec/factory/support/NVAccessor;->getNV(I)B

    move-result v0

    .line 98
    .local v0, "mNV_Value":B
    const-string v2, "FactoryTestNVView"

    const-string v3, "get_NV_Value"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "key: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "NV: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->Change_NV_Value(B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 101
    if-ge p1, p2, :cond_0

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 106
    .end local v0    # "mNV_Value":B
    :cond_1
    return-object v1
.end method

.method private get_NV_ValueCPO(II)Ljava/lang/String;
    .locals 7
    .param p1, "nv_key_start"    # I
    .param p2, "nv_key_end"    # I

    .prologue
    .line 164
    const-string v3, "FactoryTestNVView"

    const-string v4, "get_NV_ValueCPO"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v2, ""

    .line 168
    .local v2, "mTemp":Ljava/lang/String;
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-gt v0, p2, :cond_1

    .line 169
    iget-object v3, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mResultValue:[C

    aget-char v1, v3, v0

    .line 170
    .local v1, "mNV_Value":C
    const-string v3, "FactoryTestNVView"

    const-string v4, "get_NV_ValueCPO"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "NV: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 173
    if-ge p1, p2, :cond_0

    .line 174
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 168
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 178
    .end local v1    # "mNV_Value":C
    :cond_1
    return-object v2
.end method

.method private set_NV_Value(B)V
    .locals 4
    .param p1, "key"    # B

    .prologue
    .line 110
    const/16 v0, 0x50

    invoke-static {p1, v0}, Lcom/sec/factory/support/NVAccessor;->setNV(IB)I

    .line 111
    const-string v0, "FactoryTestNVView"

    const-string v1, "set_NV_Value"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method private set_NV_ValueCPO(BB)V
    .locals 0
    .param p1, "itemid"    # B
    .param p2, "result"    # B

    .prologue
    .line 224
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v5, 0x50

    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 129
    const-string v0, "FactoryTestNVView"

    const-string v1, "onClick"

    const-string v2, "start_update_in_nvitem"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 153
    :goto_0
    return-void

    .line 134
    :pswitch_0
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-direct {p0, v3}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->set_NV_Value(B)V

    .line 136
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->getFactoryTestNVValue()V

    goto :goto_0

    .line 138
    :cond_0
    invoke-direct {p0, v3, v5}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->set_NV_ValueCPO(BB)V

    goto :goto_0

    .line 144
    :pswitch_1
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    invoke-direct {p0, v4}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->set_NV_Value(B)V

    .line 146
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->getFactoryTestNVValue()V

    goto :goto_0

    .line 148
    :cond_1
    invoke-direct {p0, v4, v5}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->set_NV_ValueCPO(BB)V

    goto :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x7f080008
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const-string v2, "FactoryTestNVView"

    const-string v3, "onCreate"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->setContentView(I)V

    .line 53
    const v2, 0x7f080008

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 54
    .local v1, "smdbutton":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v2, 0x7f080009

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "pbabutton":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    const v2, 0x7f080004

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView1:Landroid/widget/TextView;

    .line 58
    const v2, 0x7f080005

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView2:Landroid/widget/TextView;

    .line 59
    const v2, 0x7f080006

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView3:Landroid/widget/TextView;

    .line 60
    const v2, 0x7f080007

    invoke-virtual {p0, v2}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mView4:Landroid/widget/TextView;

    .line 61
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 81
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 64
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 66
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->getFactoryTestNVValue()V

    .line 77
    :goto_0
    return-void

    .line 69
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 70
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.android.samsungtest.RilOmissionCommand"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/factory/app/ui/UIFactoryTestNVView;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 74
    const-string v1, "FactoryTestNVView"

    const-string v2, "onResume"

    const-string v3, "Request for TestNV!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
