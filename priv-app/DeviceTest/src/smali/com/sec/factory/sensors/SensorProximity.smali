.class public Lcom/sec/factory/sensors/SensorProximity;
.super Lcom/sec/factory/sensors/SensorBase;
.source "SensorProximity.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private isProximityADCOn:Z

.field private isProximityAVGOn:Z

.field private isProximityMGROn:Z

.field private isProximityOFFSETOn:Z

.field private mBuffer_SensorValue_Proximity:[F

.field private mContext:Landroid/content/Context;

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mSensorListener:Lcom/sec/factory/sensors/SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorValues:[Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorBase;-><init>()V

    .line 17
    const-string v0, "SensorProximity"

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->CLASS_NAME:Ljava/lang/String;

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->DEBUG:Z

    .line 22
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mTimer:Ljava/util/Timer;

    .line 23
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityMGROn:Z

    .line 24
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    .line 25
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    .line 26
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityOFFSETOn:Z

    .line 27
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 30
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mProximitySensor:Landroid/hardware/Sensor;

    .line 31
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    .line 36
    const-string v0, "SensorProximity"

    const-string v1, "SensorProximity"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorProximity;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private dataCheck([F)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [F

    .prologue
    .line 179
    const-string v1, ""

    .line 181
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 182
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 185
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 182
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 193
    :cond_2
    return-object v1
.end method


# virtual methods
.method public SensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 110
    const-string v0, "SensorProximity"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 114
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mTimer:Ljava/util/Timer;

    .line 115
    const-string v0, "SensorProximity"

    const-string v1, "mTimer canceled"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 122
    :cond_1
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    if-eqz v0, :cond_3

    .line 123
    :cond_2
    const-string v0, "PROXI_SENSOR_ADC_AVG"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 126
    :cond_3
    iput-boolean v3, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityMGROn:Z

    .line 127
    iput-boolean v3, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    .line 128
    iput-boolean v3, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    .line 129
    iput-boolean v3, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityOFFSETOn:Z

    .line 130
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 131
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mProximitySensor:Landroid/hardware/Sensor;

    .line 132
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    .line 133
    return-void
.end method

.method public SensorOn(Lcom/sec/factory/sensors/SensorListener;[II)V
    .locals 7
    .param p1, "mSensorListener"    # Lcom/sec/factory/sensors/SensorListener;
    .param p2, "sensorID"    # [I
    .param p3, "interval"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 41
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 42
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 44
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_8

    .line 45
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, p2

    if-ge v6, v0, :cond_5

    .line 46
    aget v0, p2, v6

    if-ne v0, v4, :cond_1

    .line 47
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mProximitySensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mProximitySensor:Landroid/hardware/Sensor;

    .line 49
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mProximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 50
    const-string v0, "SensorProximity"

    const-string v1, "SensorOn"

    const-string v2, "register-ProximitySensor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 52
    :cond_1
    aget v0, p2, v6

    if-ne v0, v5, :cond_2

    .line 53
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    goto :goto_1

    .line 54
    :cond_2
    aget v0, p2, v6

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 55
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    goto :goto_1

    .line 56
    :cond_3
    aget v0, p2, v6

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 57
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityOFFSETOn:Z

    goto :goto_1

    .line 59
    :cond_4
    const-string v0, "SensorProximity"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregistered-ETC(ModuleSensor ID) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p2, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 63
    :cond_5
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    if-eqz v0, :cond_7

    .line 64
    :cond_6
    const-string v0, "PROXI_SENSOR_ADC_AVG"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 70
    .end local v6    # "i":I
    :cond_7
    :goto_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mTimer:Ljava/util/Timer;

    .line 71
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/factory/sensors/SensorProximity$1;

    invoke-direct {v1, p0}, Lcom/sec/factory/sensors/SensorProximity$1;-><init>(Lcom/sec/factory/sensors/SensorProximity;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 76
    return-void

    .line 67
    :cond_8
    const-string v0, "SensorProximity"

    const-string v1, "SensorOn"

    const-string v2, "SensorManager null !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public isSensorOn(I)Z
    .locals 2
    .param p1, "sensorID"    # I

    .prologue
    const/4 v0, 0x0

    .line 148
    packed-switch p1, :pswitch_data_0

    .line 165
    :cond_0
    :goto_0
    return v0

    .line 151
    :pswitch_0
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mProximitySensor:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    .line 154
    const/4 v0, 0x1

    goto :goto_0

    .line 158
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    goto :goto_0

    .line 160
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    goto :goto_0

    .line 162
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityOFFSETOn:Z

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 136
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 139
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 145
    :goto_0
    return-void

    .line 141
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityMGROn:Z

    .line 142
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public returnProximity()[F
    .locals 3

    .prologue
    .line 170
    const-string v0, "SensorProximity"

    const-string v1, "returnProximity"

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/sensors/SensorProximity;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    return-object v0
.end method

.method public returnSensorValues()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 80
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityMGROn:Z

    if-ne v1, v6, :cond_0

    .line 81
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    if-eqz v1, :cond_4

    .line 82
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mBuffer_SensorValue_Proximity:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 88
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityADCOn:Z

    if-ne v1, v6, :cond_1

    .line 89
    const-string v1, "PROXI_SENSOR_ADC"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 93
    :cond_1
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityAVGOn:Z

    if-ne v1, v6, :cond_2

    .line 94
    const-string v1, "PROXI_SENSOR_ADC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    .line 95
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 98
    :cond_2
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorProximity;->isProximityOFFSETOn:Z

    if-ne v1, v6, :cond_3

    .line 99
    const-string v1, "PROXI_SENSOR_OFFSET"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorValues:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 107
    :goto_1
    return-void

    .line 84
    :cond_4
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorProximity;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x1

    const-string v3, "null"

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
