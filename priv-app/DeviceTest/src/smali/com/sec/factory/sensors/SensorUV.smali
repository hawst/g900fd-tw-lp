.class public Lcom/sec/factory/sensors/SensorUV;
.super Lcom/sec/factory/sensors/SensorBase;
.source "SensorUV.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/factory/sensors/SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/sensors/SensorUV$Maxim;
    }
.end annotation


# static fields
.field public static final ADI:Ljava/lang/String; = "ADI"

.field private static final INTERVAL:I = 0x1f4

.field public static final MAXIM:Ljava/lang/String; = "MAXIM"

.field public static final STM:Ljava/lang/String; = "STMicroelectronics"

.field public static final STM_ADC:I = 0x2

.field public static final UVA:I = 0x1

.field public static final UVADC:I = 0x1

.field public static final UVB:I = 0x2

.field public static final UVIndex:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private isUVMGROn:Z

.field private mBuffer_SensorValue_Cloud:[I

.field private mBuffer_SensorValue_UV:[F

.field private mContext:Landroid/content/Context;

.field private mMaximSensor:Lcom/sec/factory/sensors/SensorUV$Maxim;

.field private mSensorListener:Lcom/sec/factory/sensors/SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTimer:Ljava/util/Timer;

.field private mUVSensor:Landroid/hardware/Sensor;

.field private mVendor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorBase;-><init>()V

    .line 27
    const-string v0, "SensorUV"

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->CLASS_NAME:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 30
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 33
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 37
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    .line 40
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    .line 41
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    .line 56
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    .line 58
    new-instance v0, Lcom/sec/factory/sensors/SensorUV$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/sensors/SensorUV$1;-><init>(Lcom/sec/factory/sensors/SensorUV;)V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 63
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorUV;->CheckVendorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    .line 64
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV Sensor Vendor is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/sec/factory/sensors/SensorListener;Landroid/content/Context;)V
    .locals 4
    .param p1, "mSensorListener"    # Lcom/sec/factory/sensors/SensorListener;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorBase;-><init>()V

    .line 27
    const-string v0, "SensorUV"

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->CLASS_NAME:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 30
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 33
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 37
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    .line 40
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    .line 41
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    .line 68
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 70
    iput-object p2, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    .line 71
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorUV;->CheckVendorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    .line 72
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV Sensor Vendor is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method private CheckVendorName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 247
    const-string v1, ""

    .line 250
    .local v1, "mVendor":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    const v3, 0x1001d

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 257
    :goto_0
    return-object v1

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 254
    const-string v2, "SENSOR_NAME_UV"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private SensorOn(II)V
    .locals 6
    .param p1, "sensorID"    # I
    .param p2, "interval"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 90
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/sec/factory/sensors/SensorUV;->isSensorOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1001d

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 93
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 94
    const-string v0, "SensorUV"

    const-string v1, "SensorOn"

    const-string v2, "register-UVSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    :goto_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 101
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/factory/sensors/SensorUV$2;

    invoke-direct {v1, p0}, Lcom/sec/factory/sensors/SensorUV$2;-><init>(Lcom/sec/factory/sensors/SensorUV;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 106
    return-void

    .line 97
    :cond_1
    const-string v0, "SensorUV"

    const-string v1, "SensorOn"

    const-string v2, "SensorManager null !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/factory/sensors/SensorUV;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/sensors/SensorUV;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/factory/sensors/SensorUV;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/sensors/SensorUV;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/factory/sensors/SensorUV;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/sensors/SensorUV;
    .param p1, "x1"    # [I

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/factory/sensors/SensorUV;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/sensors/SensorUV;
    .param p1, "x1"    # [F

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    return-object p1
.end method


# virtual methods
.method public SensorOff()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 109
    const-string v0, "SensorUV"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mMaximSensor:Lcom/sec/factory/sensors/SensorUV$Maxim;

    invoke-virtual {v0}, Lcom/sec/factory/sensors/SensorUV$Maxim;->SensorOff()V

    .line 125
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    .line 126
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    .line 127
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    .line 128
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    .line 129
    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 116
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mTimer:Ljava/util/Timer;

    .line 117
    const-string v0, "SensorUV"

    const-string v1, "mTimer canceled"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method

.method public SensorOn()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mMaximSensor:Lcom/sec/factory/sensors/SensorUV$Maxim;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/sec/factory/sensors/SensorUV$Maxim;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/factory/sensors/SensorUV$Maxim;-><init>(Lcom/sec/factory/sensors/SensorUV;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mMaximSensor:Lcom/sec/factory/sensors/SensorUV$Maxim;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mMaximSensor:Lcom/sec/factory/sensors/SensorUV$Maxim;

    invoke-virtual {v0}, Lcom/sec/factory/sensors/SensorUV$Maxim;->SensorOn()Z

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_1
    const v0, 0x1001d

    const/16 v1, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/sensors/SensorUV;->SensorOn(II)V

    goto :goto_0
.end method

.method public getADC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 148
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v0, v0

    if-le v0, v2, :cond_1

    .line 150
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v0, v0, v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 157
    :goto_0
    return-object v0

    .line 152
    :cond_0
    const-string v0, "STMicroelectronics"

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v0, v0

    if-le v0, v3, :cond_1

    .line 154
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v0, v0, v3

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method public getIntensity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const-string v0, "0"

    return-object v0
.end method

.method public getUV()[F
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    return-object v0
.end method

.method public getUVA()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 161
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v0, v0

    if-le v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public getUVB()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 168
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v0, v0

    if-le v0, v1, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public getUVIndex()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v0, v0

    if-lez v0, :cond_0

    .line 181
    const-string v0, "%.1f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public getVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    return-object v0
.end method

.method public isSensorOn()Z
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 134
    .local v0, "res":Z
    const-string v1, "MAXIM"

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mMaximSensor:Lcom/sec/factory/sensors/SensorUV$Maxim;

    invoke-virtual {v1}, Lcom/sec/factory/sensors/SensorUV$Maxim;->isSensorOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    const/4 v0, 0x1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorUV;->mUVSensor:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    .line 140
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 227
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 231
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 239
    :goto_0
    return-void

    .line 233
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    .line 234
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x1001d
        :pswitch_0
    .end packed-switch
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 0
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 244
    return-void
.end method

.method public returnCloudValues([I)V
    .locals 6
    .param p1, "buffer_VB"    # [I

    .prologue
    const/16 v5, 0xf

    const/4 v4, 0x0

    .line 208
    iget-boolean v2, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 209
    if-eqz p1, :cond_2

    .line 210
    const-string v1, ""

    .line 211
    .local v1, "res":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 215
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    invoke-interface {v2, v5, v1}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 221
    .end local v0    # "i":I
    .end local v1    # "res":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v3, ""

    invoke-interface {v2, v4, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 222
    return-void

    .line 217
    :cond_2
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v3, "null"

    invoke-interface {v2, v5, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public returnSensorValues()V
    .locals 6

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x0

    .line 191
    iget-boolean v2, p0, Lcom/sec/factory/sensors/SensorUV;->isUVMGROn:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 192
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    if-eqz v2, :cond_2

    .line 193
    const-string v1, ""

    .line 194
    .local v1, "res":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 195
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorUV;->mBuffer_SensorValue_UV:[F

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    invoke-interface {v2, v5, v1}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 204
    .end local v0    # "i":I
    .end local v1    # "res":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v3, ""

    invoke-interface {v2, v4, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 205
    return-void

    .line 200
    :cond_2
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorUV;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v3, "null"

    invoke-interface {v2, v5, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_1
.end method
