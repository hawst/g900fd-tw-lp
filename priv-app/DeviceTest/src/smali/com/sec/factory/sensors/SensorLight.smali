.class public Lcom/sec/factory/sensors/SensorLight;
.super Lcom/sec/factory/sensors/SensorBase;
.source "SensorLight.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private isLightADCOn:Z

.field private isLightCCTOn:Z

.field private isLightMGROn:Z

.field private mBuffer_SensorValue_Light:[F

.field private mContext:Landroid/content/Context;

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mSensorListener:Lcom/sec/factory/sensors/SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorValues:[Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorBase;-><init>()V

    .line 15
    const-string v0, "SensorLight"

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->CLASS_NAME:Ljava/lang/String;

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorLight;->DEBUG:Z

    .line 19
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mTimer:Ljava/util/Timer;

    .line 20
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorLight;->isLightMGROn:Z

    .line 21
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorLight;->isLightCCTOn:Z

    .line 22
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorLight;->isLightADCOn:Z

    .line 23
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    .line 25
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mLightSensor:Landroid/hardware/Sensor;

    .line 26
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    .line 30
    const-string v0, "SensorLight"

    const-string v1, "SensorLight"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorLight;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private dataCheck([F)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [F

    .prologue
    .line 162
    const-string v1, ""

    .line 164
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 165
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 168
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 165
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 176
    :cond_2
    return-object v1
.end method


# virtual methods
.method public SensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 102
    const-string v0, "SensorLight"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 106
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorLight;->mTimer:Ljava/util/Timer;

    .line 107
    const-string v0, "SensorLight"

    const-string v1, "mTimer canceled"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 114
    :cond_1
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorLight;->isLightCCTOn:Z

    .line 115
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorLight;->isLightADCOn:Z

    .line 116
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    .line 117
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorLight;->mLightSensor:Landroid/hardware/Sensor;

    .line 118
    iput-object v3, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    .line 119
    return-void
.end method

.method public SensorOn(Lcom/sec/factory/sensors/SensorListener;[II)V
    .locals 7
    .param p1, "mSensorListener"    # Lcom/sec/factory/sensors/SensorListener;
    .param p2, "sensorID"    # [I
    .param p3, "interval"    # I

    .prologue
    const/4 v4, 0x1

    .line 35
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 36
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    .line 38
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 39
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, p2

    if-ge v6, v0, :cond_2

    .line 40
    aget v0, p2, v6

    packed-switch v0, :pswitch_data_0

    .line 55
    :pswitch_0
    const-string v0, "SensorLight"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregistered-ETC(ModuleSensor ID) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p2, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 42
    :pswitch_1
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mLightSensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mLightSensor:Landroid/hardware/Sensor;

    .line 44
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 45
    const-string v0, "SensorLight"

    const-string v1, "SensorOn"

    const-string v2, "register-LightSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 49
    :pswitch_2
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorLight;->isLightCCTOn:Z

    goto :goto_1

    .line 52
    :pswitch_3
    iput-boolean v4, p0, Lcom/sec/factory/sensors/SensorLight;->isLightADCOn:Z

    goto :goto_1

    .line 60
    .end local v6    # "i":I
    :cond_1
    const-string v0, "SensorLight"

    const-string v1, "SensorOn"

    const-string v2, "SensorManager null !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mTimer:Ljava/util/Timer;

    .line 64
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/factory/sensors/SensorLight$1;

    invoke-direct {v1, p0}, Lcom/sec/factory/sensors/SensorLight$1;-><init>(Lcom/sec/factory/sensors/SensorLight;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 69
    return-void

    .line 40
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public isSensorOn(I)Z
    .locals 2
    .param p1, "sensorID"    # I

    .prologue
    const/4 v0, 0x0

    .line 135
    packed-switch p1, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 137
    :pswitch_1
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mLightSensor:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    .line 140
    const/4 v0, 0x1

    goto :goto_0

    .line 143
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorLight;->isLightCCTOn:Z

    goto :goto_0

    .line 145
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorLight;->isLightADCOn:Z

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 121
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 124
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 132
    :goto_0
    return-void

    .line 126
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorLight;->isLightMGROn:Z

    .line 127
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public returnProximity()[F
    .locals 3

    .prologue
    .line 155
    const-string v0, "SensorLight"

    const-string v1, "returnProximity"

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/sensors/SensorLight;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    return-object v0
.end method

.method public returnSensorValues()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 72
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorLight;->isLightMGROn:Z

    if-ne v1, v6, :cond_0

    .line 73
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    if-eqz v1, :cond_3

    .line 74
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorLight;->mBuffer_SensorValue_Light:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 80
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorLight;->isLightCCTOn:Z

    if-ne v1, v6, :cond_1

    .line 81
    const-string v1, "PROXI_SENSOR_ADC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "sensorValue":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 83
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    .line 86
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 89
    .end local v0    # "sensorValue":Ljava/lang/String;
    :cond_1
    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorLight;->isLightADCOn:Z

    if-ne v1, v6, :cond_2

    .line 90
    const-string v1, "LIGHT_SENSOR_RAW"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .restart local v0    # "sensorValue":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 92
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    .line 95
    :goto_2
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const/16 v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 98
    .end local v0    # "sensorValue":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v2, ""

    invoke-interface {v1, v5, v2}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 99
    return-void

    .line 76
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v2, "null"

    invoke-interface {v1, v4, v2}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_0

    .line 85
    .restart local v0    # "sensorValue":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    aput-object v7, v1, v5

    goto :goto_1

    .line 94
    :cond_5
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorLight;->mSensorValues:[Ljava/lang/String;

    aput-object v7, v1, v5

    goto :goto_2
.end method
