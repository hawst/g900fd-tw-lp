.class public Lcom/sec/factory/sensors/SensorBank;
.super Ljava/lang/Object;
.source "SensorBank.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/sensors/SensorBank$SensorAccount;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "SensorBank"

.field public static final ID_ACCELEROMETER:I = 0x1

.field public static final ID_BASE:I = -0x1

.field public static final ID_GYRO:I = 0x0

.field public static final ID_MAGNETIC:I = 0x2


# instance fields
.field private mAutoRotateState:I

.field private mContext:Landroid/content/Context;

.field private mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

.field private mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/modules/ModuleSensor;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensor"    # Lcom/sec/factory/modules/ModuleSensor;

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    .line 21
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/sensors/SensorBank;->mAutoRotateState:I

    .line 23
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mContext:Landroid/content/Context;

    .line 56
    const-string v0, "SensorBank"

    const-string v1, "SensorBank"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorBank;->mContext:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    .line 59
    return-void
.end method


# virtual methods
.method public restoreAutoRotateSetting()V
    .locals 3

    .prologue
    .line 109
    const-string v0, "SensorBank"

    const-string v1, "restoreAutoRotateSetting"

    const-string v2, "Do nothing"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public restoreStatus()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    const-string v4, "SensorBank"

    const-string v5, "restoreStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSensorAccount = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    :goto_0
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    if-nez v1, :cond_1

    .line 84
    const-string v1, "SensorBank"

    const-string v3, "restoreStatus"

    const-string v4, "mModuleSensor = null"

    invoke-static {v1, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 100
    :goto_1
    return v1

    .line 82
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    if-nez v1, :cond_2

    .line 89
    const-string v1, "SensorBank"

    const-string v3, "restoreStatus"

    const-string v4, "mSensorAccount = null"

    invoke-static {v1, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 90
    goto :goto_1

    .line 93
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    invoke-virtual {v1}, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->length()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 94
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    iget-object v1, v1, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_3

    .line 95
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    new-array v4, v3, [I

    iget-object v5, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    iget-object v5, v5, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    aget v5, v5, v0

    aput v5, v4, v2

    invoke-virtual {v1, v4}, Lcom/sec/factory/modules/ModuleSensor;->SensorOn([I)V

    .line 93
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    new-array v4, v3, [I

    iget-object v5, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    iget-object v5, v5, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    aget v5, v5, v0

    aput v5, v4, v2

    invoke-virtual {v1, v4}, Lcom/sec/factory/modules/ModuleSensor;->SensorOff([I)V

    goto :goto_3

    :cond_4
    move v1, v3

    .line 100
    goto :goto_1
.end method

.method public saveAutoRotateSetting()V
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorBank;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->getAutoRotateState()I

    move-result v0

    iput v0, p0, Lcom/sec/factory/sensors/SensorBank;->mAutoRotateState:I

    .line 105
    const-string v0, "SensorBank"

    const-string v1, "saveAutoRotateSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mAutoRotateState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/sensors/SensorBank;->mAutoRotateState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public saveStatus([I)Z
    .locals 6
    .param p1, "sensorID"    # [I

    .prologue
    const/4 v2, 0x0

    .line 62
    const-string v3, "SensorBank"

    const-string v4, "saveStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sensorID = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p1, :cond_0

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v4, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    if-nez v1, :cond_1

    .line 64
    const-string v1, "SensorBank"

    const-string v3, "saveStatus"

    const-string v4, "mModuleSensor = null"

    invoke-static {v1, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 78
    :goto_1
    return v1

    .line 62
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 68
    :cond_1
    if-nez p1, :cond_2

    .line 69
    const-string v1, "SensorBank"

    const-string v3, "saveStatus"

    const-string v4, "sensorID = null"

    invoke-static {v1, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 70
    goto :goto_1

    .line 73
    :cond_2
    new-instance v1, Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    array-length v2, p1

    invoke-direct {v1, v2}, Lcom/sec/factory/sensors/SensorBank$SensorAccount;-><init>(I)V

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 75
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    iget-object v1, v1, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    aget v2, p1, v0

    aput v2, v1, v0

    .line 76
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorBank;->mSensorAccount:Lcom/sec/factory/sensors/SensorBank$SensorAccount;

    iget-object v1, v1, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorBank;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModuleSensor;->isSensorOn(I)Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 78
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method
