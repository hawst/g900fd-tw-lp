.class public Lcom/sec/factory/sensors/SensorBank$SensorAccount;
.super Ljava/lang/Object;
.source "SensorBank.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/sensors/SensorBank;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SensorAccount"
.end annotation


# instance fields
.field public ID:[I

.field public on:[Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "length"    # I

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    .line 27
    iput-object v0, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    .line 30
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    .line 31
    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    .line 32
    return-void
.end method


# virtual methods
.method public length()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    array-length v0, v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    array-length v2, v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    array-length v3, v3

    if-eq v2, v3, :cond_2

    .line 43
    :cond_0
    const/4 v1, 0x0

    .line 49
    :cond_1
    return-object v1

    .line 45
    :cond_2
    const-string v1, ""

    .line 46
    .local v1, "s":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 47
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->ID:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorBank$SensorAccount;->on:[Z

    aget-boolean v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
