.class public Lcom/sec/factory/sensors/SensorGesture;
.super Lcom/sec/factory/sensors/SensorBase;
.source "SensorGesture.java"


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private isGestureMGROn:Z

.field private mBuffer_SensorValue_Gesture:[F

.field private mContext:Landroid/content/Context;

.field private mResult:Z

.field private mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private mSensorListener:Lcom/sec/factory/sensors/SensorListener;

.field private mSensorValues:[Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/sec/factory/sensors/SensorBase;-><init>()V

    .line 14
    const-string v0, "SensorGesture"

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->CLASS_NAME:Ljava/lang/String;

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorGesture;->DEBUG:Z

    .line 19
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mTimer:Ljava/util/Timer;

    .line 20
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorGesture;->isGestureMGROn:Z

    .line 21
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorValues:[Ljava/lang/String;

    .line 23
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 24
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 25
    iput-boolean v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    .line 26
    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    .line 135
    new-instance v0, Lcom/sec/factory/sensors/SensorGesture$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/sensors/SensorGesture$2;-><init>(Lcom/sec/factory/sensors/SensorGesture;)V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    .line 31
    const-string v0, "SensorGesture"

    const-string v1, "SensorGesture"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorGesture;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method static synthetic access$002(Lcom/sec/factory/sensors/SensorGesture;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/sensors/SensorGesture;
    .param p1, "x1"    # [F

    .prologue
    .line 13
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    return-object p1
.end method

.method private dataCheck([F)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [F

    .prologue
    .line 145
    const-string v1, ""

    .line 146
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 147
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 157
    :cond_2
    return-object v1
.end method


# virtual methods
.method public SensorOff()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 93
    const-string v0, "SensorGesture"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 97
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorGesture;->mTimer:Ljava/util/Timer;

    .line 98
    const-string v0, "SensorGesture"

    const-string v1, "mTimer canceled"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 104
    :cond_1
    const-string v0, "SensorGesture"

    const-string v1, "isSensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSensorHub : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , mResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 108
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 109
    iput-boolean v5, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    .line 110
    iput-boolean v5, p0, Lcom/sec/factory/sensors/SensorGesture;->isGestureMGROn:Z

    .line 111
    iput-object v4, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    .line 112
    return-void
.end method

.method public SensorOn(Lcom/sec/factory/sensors/SensorListener;[II)V
    .locals 7
    .param p1, "mSensorListener"    # Lcom/sec/factory/sensors/SensorListener;
    .param p2, "sensorID"    # [I
    .param p3, "interval"    # I

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    .line 37
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mContext:Landroid/content/Context;

    const-string v1, "sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 39
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 41
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, p2

    if-ge v6, v0, :cond_2

    .line 42
    aget v0, p2, v6

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    .line 44
    const-string v0, "SensorGesture"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "register-GestureSensor , mResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 46
    :cond_0
    const-string v0, "SensorGesture"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregistered: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p2, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 50
    .end local v6    # "i":I
    :cond_1
    const-string v0, "SensorGesture"

    const-string v1, "SensorOn"

    const-string v2, "SensorManager null !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mTimer:Ljava/util/Timer;

    .line 54
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/factory/sensors/SensorGesture$1;

    invoke-direct {v1, p0}, Lcom/sec/factory/sensors/SensorGesture$1;-><init>(Lcom/sec/factory/sensors/SensorGesture;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 59
    return-void
.end method

.method public SensorOn([II)V
    .locals 5
    .param p1, "sensorID"    # [I
    .param p2, "interval"    # I

    .prologue
    .line 62
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mContext:Landroid/content/Context;

    const-string v2, "sensorhub"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sensorhub/SensorHubManager;

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 64
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    if-eqz v1, :cond_1

    .line 65
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 67
    aget v1, p1, v0

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 68
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    iget-object v3, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    .line 69
    const-string v1, "SensorGesture"

    const-string v2, "SensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "register-GestureSensor , mResult: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_0
    const-string v1, "SensorGesture"

    const-string v2, "SensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unregistered: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 75
    .end local v0    # "i":I
    :cond_1
    const-string v1, "SensorGesture"

    const-string v2, "SensorOn"

    const-string v3, "SensorManager null !!!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_2
    return-void
.end method

.method public isSensorOn(I)Z
    .locals 2
    .param p1, "sensorID"    # I

    .prologue
    const/4 v0, 0x0

    .line 115
    packed-switch p1, :pswitch_data_0

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 117
    :pswitch_0
    iget-object v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/factory/sensors/SensorGesture;->mResult:Z

    if-eqz v1, :cond_0

    .line 118
    const/4 v0, 0x1

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public returnGesture()[F
    .locals 3

    .prologue
    .line 129
    const-string v0, "SensorGesture"

    const-string v1, "returnGesture"

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    invoke-direct {p0, v2}, Lcom/sec/factory/sensors/SensorGesture;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    return-object v0
.end method

.method public returnSensorValues()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    iget-boolean v0, p0, Lcom/sec/factory/sensors/SensorGesture;->isGestureMGROn:Z

    if-ne v0, v4, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/sensors/SensorGesture;->mBuffer_SensorValue_Gesture:[F

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v5, v1}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 88
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v1, ""

    invoke-interface {v0, v3, v1}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 89
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/sensors/SensorGesture;->mSensorListener:Lcom/sec/factory/sensors/SensorListener;

    const-string v1, "null"

    invoke-interface {v0, v5, v1}, Lcom/sec/factory/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_0
.end method
