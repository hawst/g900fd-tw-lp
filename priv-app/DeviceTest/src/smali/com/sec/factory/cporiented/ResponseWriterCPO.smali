.class public Lcom/sec/factory/cporiented/ResponseWriterCPO;
.super Ljava/lang/Object;
.source "ResponseWriterCPO.java"


# static fields
.field public static final BASIC_FILE_SIZE:I = 0x6

.field private static final BOOT_COMPLETED_MSG:I = 0x3e8

.field public static final CLASS_NAME:Ljava/lang/String; = "ResponseWriterCPO"

.field public static final COMMAND_RECEIVE_FAIL:I = 0xff

.field public static final COMMAND_RECEIVE_SUCCESS:I = 0x1

.field private static final CP_MODEM_SYSDUMP_SUCCESS:I = 0x68

.field private static final DFMS_CONFIRM_SENT_SUCCESS:I = 0x64

.field private static final DFMS_EVENT_SENT_SUCCESS:I = 0x65

.field private static final DFT_CONFIRM_SENT_SUCCESS:I = 0x66

.field private static final DFT_EVENT_SENT_SUCCESS:I = 0x67

.field public static final FACTORY_FILE_SIZE:I = 0x4

.field private static final NA_ATTRIBUTE:Ljava/lang/String; = "fe"

.field private static final NG_ATTRIBUTE:Ljava/lang/String; = "ff"

.field public static final OEM_DFT_CFRM:I = 0x6

.field public static final OEM_DFT_EVENT:I = 0x5

.field public static final OEM_DFT_LENGTH:I = 0x1

.field public static final OEM_FACTORY_CFRM:I = 0x2

.field public static final OEM_FACTORY_CFRM_FILESIZE:I = 0x5

.field public static final OEM_FACTORY_EVENT:I = 0x1

.field public static final OEM_FACTORY_LENGTH:I = 0x2

.field public static final OEM_FUNCTION_ID_FACTORY:I = 0x12

.field public static final OEM_FUNCTION_ID_POWER:I = 0x5

.field private static final OEM_MODEM_LOG:I = 0x12

.field public static final OEM_PWK_SET_PHONE_OFF:I = 0x4

.field public static final OEM_PWK_SlAVE_PHONE_OFF:I = 0x5

.field private static final OEM_SYSDUMP_FUNCTAG:I = 0x7

.field private static final POWER_CMD_SENT_SUCCESS:I = 0xc8

.field public static final POWER_FILE_SIZE:I = 0x4


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field public mMessenger:Landroid/os/Messenger;

.field public mResoponseMessenger:Landroid/os/Messenger;

.field private mResponse:Landroid/os/Message;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResoponseMessenger:Landroid/os/Messenger;

    .line 52
    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mMessenger:Landroid/os/Messenger;

    .line 422
    new-instance v0, Lcom/sec/factory/cporiented/ResponseWriterCPO$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/cporiented/ResponseWriterCPO$1;-><init>(Lcom/sec/factory/cporiented/ResponseWriterCPO;)V

    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    .line 67
    const-string v0, "ResponseWriterCPO"

    const-string v1, "ResponseWriter"

    const-string v2, "Create ResponseWriterCPO Without Messenger"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/os/Messenger;)V
    .locals 3
    .param p1, "messenger"    # Landroid/os/Messenger;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResoponseMessenger:Landroid/os/Messenger;

    .line 52
    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mMessenger:Landroid/os/Messenger;

    .line 422
    new-instance v0, Lcom/sec/factory/cporiented/ResponseWriterCPO$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/cporiented/ResponseWriterCPO$1;-><init>(Lcom/sec/factory/cporiented/ResponseWriterCPO;)V

    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    .line 56
    const-string v0, "ResponseWriterCPO"

    const-string v1, "ResponseWriter"

    const-string v2, "Create ResponseWriterCPO"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResoponseMessenger:Landroid/os/Messenger;

    .line 59
    if-eqz p1, :cond_0

    .line 60
    iput-object p1, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mMessenger:Landroid/os/Messenger;

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    const-string v0, "ResponseWriterCPO"

    const-string v1, "ResponseWriterCPO"

    const-string v2, "messenger is not available"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 19
    .param p1, "mainCMD"    # Ljava/lang/String;
    .param p2, "attr"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 203
    const/4 v10, 0x2

    .line 204
    .local v10, "nLength":I
    const/4 v4, 0x1

    .line 205
    .local v4, "eventType":I
    const/4 v5, 0x0

    .line 207
    .local v5, "isdft":Z
    const-string v12, "F7"

    const/4 v13, 0x0

    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 208
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    const-string v14, "Set Event type to DFT"

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v4, 0x5

    .line 210
    const/4 v10, 0x1

    .line 211
    const/4 v12, 0x2

    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 212
    const/4 v5, 0x1

    .line 215
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 216
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 217
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/4 v9, -0x1

    .line 218
    .local v9, "nAttr":I
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v12

    mul-int/lit8 v12, v12, 0x10

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-static {v13}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v13

    add-int v11, v12, v13

    .line 221
    .local v11, "nMainCMD":I
    if-eqz p2, :cond_1

    .line 222
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "attr.charAt(0) : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "attr.charAt(1) : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v12

    mul-int/lit8 v12, v12, 0x10

    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-static {v13}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v13

    add-int v9, v12, v13

    .line 227
    :cond_1
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "nAttr : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    if-eqz p3, :cond_2

    .line 230
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v10, v12

    .line 231
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "nLength : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "data.length(): "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_2
    int-to-byte v7, v10

    .line 236
    .local v7, "length2":B
    shr-int/lit8 v12, v10, 0x8

    int-to-byte v6, v12

    .line 237
    .local v6, "length1":B
    add-int/lit8 v8, v10, 0x6

    .line 240
    .local v8, "mFileSize":I
    const/16 v12, 0x12

    :try_start_0
    invoke-virtual {v2, v12}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 241
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 242
    invoke-virtual {v2, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 243
    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 244
    invoke-virtual {v2, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 245
    invoke-virtual {v2, v11}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 247
    const/4 v12, -0x1

    if-eq v9, v12, :cond_3

    if-nez v5, :cond_3

    .line 248
    invoke-virtual {v2, v9}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 251
    :cond_3
    if-eqz p3, :cond_4

    .line 252
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 253
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mData : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_4
    const-string v12, "ResponseWriterCPO"

    const-string v13, "setResultEndModeData"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "send : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "%x %x %x %x %x %x %x %s"

    const/16 v16, 0x8

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x12

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x3

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x4

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x6

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x7

    aput-object p3, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v13, 0x65

    invoke-virtual {v12, v13}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 267
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v12

    :goto_0
    return-object v12

    .line 262
    :catch_0
    move-exception v3

    .line 263
    .local v3, "e":Ljava/io/IOException;
    const/4 v12, 0x0

    goto :goto_0
.end method

.method private setResultEndModeData_DFT(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)[B
    .locals 18
    .param p1, "mainCMD"    # Ljava/lang/String;
    .param p2, "attr"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;
    .param p4, "iskeycmd"    # Z

    .prologue
    .line 272
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    const-string v13, "Event Response type: DFT"

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const/4 v9, -0x1

    .line 274
    .local v9, "nLength":I
    if-eqz p4, :cond_4

    const/4 v9, 0x3

    .line 275
    :goto_0
    const/4 v4, 0x5

    .line 276
    .local v4, "eventType":I
    const/4 v11, 0x2

    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 277
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 278
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 279
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/4 v8, -0x1

    .line 280
    .local v8, "nAttr":I
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v11

    mul-int/lit8 v11, v11, 0x10

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v12

    add-int v10, v11, v12

    .line 283
    .local v10, "nMainCMD":I
    if-eqz p2, :cond_0

    .line 284
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "attr.charAt(0) : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "attr.charAt(1) : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v11

    mul-int/lit8 v11, v11, 0x10

    const/4 v12, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/sec/factory/support/FtUtil;->charToNum(C)I

    move-result v12

    add-int v8, v11, v12

    .line 289
    :cond_0
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "nAttr : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    if-eqz p3, :cond_2

    .line 292
    if-nez p4, :cond_1

    .line 293
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v9, v11

    .line 296
    :cond_1
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "nLength : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " data.length(): "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_2
    int-to-byte v6, v9

    .line 301
    .local v6, "length2":B
    shr-int/lit8 v11, v9, 0x8

    int-to-byte v5, v11

    .line 302
    .local v5, "length1":B
    add-int/lit8 v7, v9, 0x6

    .line 305
    .local v7, "mFileSize":I
    const/16 v11, 0x12

    :try_start_0
    invoke-virtual {v2, v11}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 306
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 307
    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 308
    invoke-virtual {v2, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 309
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 310
    invoke-virtual {v2, v10}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 312
    if-eqz p3, :cond_3

    .line 313
    if-eqz p4, :cond_5

    .line 314
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v2, v11}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 319
    :goto_1
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mData : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_3
    const-string v11, "ResponseWriterCPO"

    const-string v12, "setResultEndModeData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "send : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "%x %x %x %x %x %x %s"

    const/4 v15, 0x7

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const/16 v17, 0x12

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x6

    aput-object p3, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v12, 0x67

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 334
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v11

    :goto_2
    return-object v11

    .line 274
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v4    # "eventType":I
    .end local v5    # "length1":B
    .end local v6    # "length2":B
    .end local v7    # "mFileSize":I
    .end local v8    # "nAttr":I
    .end local v10    # "nMainCMD":I
    :cond_4
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 316
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    .restart local v4    # "eventType":I
    .restart local v5    # "length1":B
    .restart local v6    # "length2":B
    .restart local v7    # "mFileSize":I
    .restart local v8    # "nAttr":I
    .restart local v10    # "nMainCMD":I
    :cond_5
    :try_start_1
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 328
    :catch_0
    move-exception v3

    .line 330
    .local v3, "e":Ljava/io/IOException;
    const/4 v11, 0x0

    goto :goto_2
.end method

.method private setResultEndModeData_cpdump()[B
    .locals 6

    .prologue
    .line 338
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 339
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 340
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    .line 343
    .local v3, "fileSize":I
    const/4 v4, 0x7

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 344
    const/16 v4, 0x12

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 345
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 346
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    if-eqz v1, :cond_0

    .line 352
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 359
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x68

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 360
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4

    .line 353
    :catch_0
    move-exception v2

    .line 354
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 347
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 348
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 350
    if-eqz v1, :cond_0

    .line 352
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 353
    :catch_2
    move-exception v2

    .line 354
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 350
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_1

    .line 352
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 355
    :cond_1
    :goto_1
    throw v4

    .line 353
    :catch_3
    move-exception v2

    .line 354
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public sendRILBootMsg()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 364
    const/4 v0, 0x6

    .line 365
    .local v0, "OEM_FUNCTION_ID_IMEI":B
    const/4 v1, 0x5

    .line 370
    .local v1, "OEM_IMEI_EVENT_START_IMEI":B
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 371
    .local v2, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 372
    .local v4, "dos":Ljava/io/DataOutputStream;
    const/4 v10, 0x2

    new-array v3, v10, [B

    .line 373
    .local v3, "command":[B
    const/4 v10, 0x6

    aput-byte v10, v3, v9

    .line 374
    const/4 v10, 0x5

    aput-byte v10, v3, v8

    .line 375
    const/4 v7, 0x2

    .line 376
    .local v7, "sizeofShort":S
    array-length v10, v3

    add-int/2addr v10, v7

    int-to-short v6, v10

    .line 379
    .local v6, "length":S
    const/4 v10, 0x0

    :try_start_0
    array-length v11, v3

    invoke-virtual {v4, v3, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    .line 380
    invoke-virtual {v4, v6}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :try_start_1
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 386
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 392
    :goto_0
    iget-object v9, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v10, 0x3e8

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 393
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write([B)V

    .line 394
    :goto_1
    return v8

    .line 387
    :catch_0
    move-exception v5

    .line 388
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 381
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 385
    .restart local v5    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 386
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    move v8, v9

    .line 389
    goto :goto_1

    .line 387
    :catch_2
    move-exception v5

    .line 388
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 384
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 385
    :try_start_3
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 386
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 389
    :goto_3
    throw v8

    .line 387
    :catch_3
    move-exception v5

    .line 388
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public sendSleepCmd()Z
    .locals 8

    .prologue
    .line 398
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 399
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 402
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x5

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 403
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 404
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 411
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 417
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 418
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write([B)V

    .line 419
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 412
    :catch_0
    move-exception v2

    .line 413
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "ResponseWriterCPO"

    const-string v4, "sendSleepCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IO Exception on close(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 406
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "ResponseWriterCPO"

    const-string v4, "sendSleepCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IO Exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 407
    const/4 v3, 0x0

    .line 410
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 411
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 412
    :catch_2
    move-exception v2

    .line 413
    const-string v4, "ResponseWriterCPO"

    const-string v5, "sendSleepCmd"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IO Exception on close(): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 409
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 410
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 411
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 414
    :goto_2
    throw v3

    .line 412
    :catch_3
    move-exception v2

    .line 413
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v4, "ResponseWriterCPO"

    const-string v5, "sendSleepCmd"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IO Exception on close(): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setAckEndModeData(I)[B
    .locals 5
    .param p1, "ack"    # I

    .prologue
    .line 149
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 150
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 153
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x12

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 154
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 155
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 157
    if-nez p1, :cond_1

    .line 158
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 163
    :goto_0
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    if-eqz v1, :cond_0

    .line 169
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 176
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 177
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 160
    :cond_1
    const/16 v3, 0xff

    :try_start_2
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 164
    :catch_0
    move-exception v2

    .line 165
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 167
    if-eqz v1, :cond_0

    .line 169
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 170
    :catch_1
    move-exception v2

    .line 171
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 170
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 171
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 167
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_2

    .line 169
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 172
    :cond_2
    :goto_2
    throw v3

    .line 170
    :catch_3
    move-exception v2

    .line 171
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public setAckEndModeData_DFT(I)[B
    .locals 5
    .param p1, "ack"    # I

    .prologue
    .line 181
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 182
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 185
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x12

    :try_start_0
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 186
    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 187
    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 189
    if-nez p1, :cond_0

    .line 190
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x66

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 199
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    :goto_1
    return-object v3

    .line 192
    :cond_0
    const/16 v3, 0xff

    :try_start_1
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 194
    :catch_0
    move-exception v2

    .line 195
    .local v2, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "result"    # I
    .param p2, "mainCMD"    # Ljava/lang/String;
    .param p3, "attr"    # Ljava/lang/String;
    .param p4, "data"    # Ljava/lang/String;

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 104
    .local v0, "bData":[B
    packed-switch p1, :pswitch_data_0

    .line 135
    const-string v1, "ResponseWriterCPO"

    const-string v2, "write"

    const-string v3, "Cannot accept result!!!"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v1, "\r\n+CME Error:NA\r\n\r\nOK\r\n"

    invoke-direct {p0, p2, p3, v1}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 140
    :goto_0
    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p0, v0}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write([B)V

    .line 142
    const-string v2, "ResponseWriterCPO"

    const-string v3, "write"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bData: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_2
    return-void

    .line 107
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setAckEndModeData(I)[B

    move-result-object v0

    .line 108
    goto :goto_0

    .line 111
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setAckEndModeData_DFT(I)[B

    move-result-object v0

    .line 112
    goto :goto_0

    .line 114
    :pswitch_2
    const-string v1, "fe"

    invoke-direct {p0, p2, v1, p4}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 115
    goto :goto_0

    .line 117
    :pswitch_3
    const-string v1, "ff"

    invoke-direct {p0, p2, v1, p4}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 118
    goto :goto_0

    .line 122
    :pswitch_4
    const-string v1, "ResponseWriterCPO"

    const-string v2, "write"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bData: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 124
    goto :goto_0

    .line 126
    :pswitch_5
    const/4 v1, 0x1

    invoke-direct {p0, p2, p3, p4, v1}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData_DFT(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 127
    goto :goto_0

    .line 129
    :pswitch_6
    const/4 v1, 0x0

    invoke-direct {p0, p2, p3, p4, v1}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData_DFT(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 130
    goto :goto_0

    .line 132
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->setResultEndModeData_cpdump()[B

    move-result-object v0

    .line 133
    goto :goto_0

    .line 142
    :cond_0
    const-string v1, "null"

    goto :goto_1

    .line 144
    :cond_1
    const-string v1, "ResponseWriterCPO"

    const-string v2, "write"

    const-string v3, "bData: null"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 104
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public write(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 454
    return-void
.end method

.method public write([B)V
    .locals 7
    .param p1, "data"    # [B

    .prologue
    .line 75
    const-string v3, "ResponseWriterCPO"

    const-string v4, "write"

    const-string v5, "Send Response Message to SecPhone"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v4, "ResponseWriterCPO"

    const-string v5, "write"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Response "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz p1, :cond_1

    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 78
    .local v1, "req":Landroid/os/Bundle;
    const-string v3, "request"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 79
    iget-object v2, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 80
    .local v2, "response":Landroid/os/Message;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResponse:Landroid/os/Message;

    .line 82
    if-nez v2, :cond_0

    .line 83
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x65

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 86
    :cond_0
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 87
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mResoponseMessenger:Landroid/os/Messenger;

    iput-object v3, v2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 90
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_2

    .line 91
    iget-object v3, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_1
    return-void

    .line 76
    .end local v1    # "req":Landroid/os/Bundle;
    .end local v2    # "response":Landroid/os/Message;
    :cond_1
    const-string v3, "null"

    goto :goto_0

    .line 93
    .restart local v1    # "req":Landroid/os/Bundle;
    .restart local v2    # "response":Landroid/os/Message;
    :cond_2
    :try_start_1
    const-string v3, "ResponseWriterCPO"

    const-string v4, "write"

    const-string v5, "send failed!!!"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
