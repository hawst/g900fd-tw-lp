.class Lcom/sec/factory/cporiented/ResponseWriterCPO$1;
.super Landroid/os/Handler;
.source "ResponseWriterCPO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/cporiented/ResponseWriterCPO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/cporiented/ResponseWriterCPO;


# direct methods
.method constructor <init>(Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO$1;->this$0:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 424
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 450
    :goto_0
    return-void

    .line 426
    :sswitch_0
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "DFMS Confirm received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    :sswitch_1
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "DFMS Event received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :sswitch_2
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "DFT Confirm received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 435
    :sswitch_3
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "DFT Event received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 438
    :sswitch_4
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "CP Sysdump done"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v0, p0, Lcom/sec/factory/cporiented/ResponseWriterCPO$1;->this$0:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const/4 v1, 0x2

    const-string v2, "5A"

    const-string v3, "00"

    const-string v4, "OK"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 442
    :sswitch_5
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "Send BOOT COMPLETED done"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 445
    :sswitch_6
    const-string v0, "ResponseWriterCPO"

    const-string v1, "handleMessage"

    const-string v2, "Send POWER CMD done"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 424
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x67 -> :sswitch_3
        0x68 -> :sswitch_4
        0xc8 -> :sswitch_6
        0x3e8 -> :sswitch_5
    .end sparse-switch
.end method
