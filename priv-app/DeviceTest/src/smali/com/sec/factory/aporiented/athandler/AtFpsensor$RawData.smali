.class Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;
.super Ljava/lang/Object;
.source "AtFpsensor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtFpsensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RawData"
.end annotation


# instance fields
.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mMax:F

.field private mMin:F

.field private mSum:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1393
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMax:F

    .line 1394
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMin:F

    .line 1397
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mData:Ljava/util/ArrayList;

    .line 1398
    return-void
.end method

.method private round(FI)F
    .locals 6
    .param p1, "t"    # F
    .param p2, "decimal"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 1408
    float-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method private roundString(FI)Ljava/lang/String;
    .locals 6
    .param p1, "t"    # F
    .param p2, "decimal"    # I

    .prologue
    .line 1411
    const-string v1, ""

    .line 1413
    .local v1, "form":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p2, :cond_0

    .line 1414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1413
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1416
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v3, "#"

    invoke-direct {v0, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 1419
    .local v0, "df":Ljava/text/DecimalFormat;
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->round(FI)F

    move-result v3

    float-to-double v4, v3

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1417
    .end local v0    # "df":Ljava/text/DecimalFormat;
    :cond_1
    new-instance v0, Ljava/text/DecimalFormat;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "#."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .restart local v0    # "df":Ljava/text/DecimalFormat;
    goto :goto_1
.end method


# virtual methods
.method public addData(F)V
    .locals 2
    .param p1, "t"    # F

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mData:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1402
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMax:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMax:F

    .line 1403
    :cond_0
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMin:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    iput p1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMin:F

    .line 1404
    :cond_1
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mSum:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mSum:F

    .line 1405
    return-void
.end method

.method public getAvg()F
    .locals 2

    .prologue
    .line 1425
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mSum:F

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getAvg(I)Ljava/lang/String;
    .locals 2
    .param p1, "round"    # I

    .prologue
    .line 1426
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mSum:F

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {p0, v0, p1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->roundString(FI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMax()F
    .locals 1

    .prologue
    .line 1423
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMax:F

    return v0
.end method

.method public getMax(I)Ljava/lang/String;
    .locals 1
    .param p1, "round"    # I

    .prologue
    .line 1424
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMax:F

    invoke-direct {p0, v0, p1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->roundString(FI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMin()F
    .locals 1

    .prologue
    .line 1421
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMin:F

    return v0
.end method

.method public getMin(I)Ljava/lang/String;
    .locals 1
    .param p1, "round"    # I

    .prologue
    .line 1422
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mMin:F

    invoke-direct {p0, v0, p1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->roundString(FI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 5
    .param p1, "round"    # I

    .prologue
    .line 1428
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1429
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 1430
    .local v2, "t":F
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v2, p1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->roundString(FI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1432
    .end local v2    # "t":F
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1433
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
