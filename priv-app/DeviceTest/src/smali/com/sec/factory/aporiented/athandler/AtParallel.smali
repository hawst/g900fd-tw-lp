.class public Lcom/sec/factory/aporiented/athandler/AtParallel;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtParallel.java"


# static fields
.field private static final PARALLEL_BASE_INDEX:Ljava/lang/String; = "00000"

.field private static mReceivedEndNotification_MSP:Z

.field private static mReceivedEndNotification_RIL:Z

.field private static mReceivedStopNotification_MSP:Z

.field private static mReceivedStopNotification_RIL:Z


# instance fields
.field private mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

.field private mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

.field private mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_RIL:Z

    .line 21
    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_RIL:Z

    .line 22
    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_MSP:Z

    .line 23
    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_MSP:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;[Lcom/sec/factory/aporiented/ProcessThread;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/sec/factory/aporiented/CommandListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;
    .param p3, "thread"    # [Lcom/sec/factory/aporiented/ProcessThread;
    .param p5, "listener"    # Lcom/sec/factory/aporiented/CommandListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/factory/aporiented/ResponseWriter;",
            "[",
            "Lcom/sec/factory/aporiented/ProcessThread;",
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;",
            ">;",
            "Lcom/sec/factory/aporiented/CommandListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p4, "list":Ljava/util/concurrent/ConcurrentLinkedQueue;, "Ljava/util/concurrent/ConcurrentLinkedQueue<Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;>;"
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 27
    const-string v0, "PARALLEL"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CMD_NAME:Ljava/lang/String;

    .line 28
    const-string v0, "AtParallel"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    .line 29
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->NUM_ARGS:I

    .line 30
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 31
    iput-object p3, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 32
    iput-object p4, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 33
    iput-object p5, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

    .line 34
    return-void
.end method

.method public static isRILCommandDone()Z
    .locals 1

    .prologue
    .line 154
    sget-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_RIL:Z

    return v0
.end method


# virtual methods
.method public checkEND()Z
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkEND"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RIL = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_RIL:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MSP = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_MSP:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    sget-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_RIL:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_MSP:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkSTOP()Z
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkSTOP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RIL = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_RIL:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", MSP = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_MSP:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sget-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_RIL:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_MSP:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 49
    monitor-enter p0

    const/4 v6, 0x0

    .line 59
    .local v6, "resData":Ljava/lang/String;
    const/4 v7, 0x3

    :try_start_0
    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "00000"

    aput-object v9, v7, v8

    invoke-virtual {p0, p1, v7}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 60
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    const-string v9, "Start CMD"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->STAGE_PARALLEL:Z

    .line 62
    const-string v7, "00000"

    iput-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    .line 64
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_RIL:Z

    .line 65
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_RIL:Z

    .line 66
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_MSP:Z

    .line 67
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_MSP:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v6

    .line 73
    :cond_1
    const/4 v7, 0x2

    :try_start_1
    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "1"

    aput-object v9, v7, v8

    invoke-virtual {p0, p1, v7}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 74
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    const-string v9, "End CMD"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 80
    :cond_2
    const/4 v7, 0x2

    :try_start_2
    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "2"

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkArgu([Ljava/lang/String;[Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 81
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    const-string v9, "Stop CMD"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_5

    .line 84
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;

    .line 85
    .local v4, "node":Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;
    if-eqz v4, :cond_3

    .line 86
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clean Waiting List : CMD="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->getCMD()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Index="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->getIndex()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ID="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->getID()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 89
    .end local v4    # "node":Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;
    :cond_4
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 91
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v7, :cond_8

    .line 92
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .local v0, "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v3, :cond_7

    aget-object v5, v0, v2

    .line 93
    .local v5, "p":Lcom/sec/factory/aporiented/ProcessThread;
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->isAlive()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 94
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Stop Thread : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->kill()V

    .line 92
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 99
    .end local v5    # "p":Lcom/sec/factory/aporiented/ProcessThread;
    :cond_7
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 101
    .end local v0    # "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_8
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->STAGE_PARALLEL:Z

    .line 102
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    .line 103
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_MSP:Z

    .line 104
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkSTOP()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 105
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    const-string v8, "0"

    invoke-virtual {p0, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 109
    :cond_9
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "2"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkArgu([Ljava/lang/String;[Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 110
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received Sequential command = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x2

    aget-object v10, p1, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AT+"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x2

    aget-object v8, p1, v8

    const/4 v9, 0x1

    const/4 v10, 0x2

    aget-object v10, p1, v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "cmd":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

    invoke-interface {v7, v1}, Lcom/sec/factory/aporiented/CommandListener;->onRunCommand(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 116
    .end local v1    # "cmd":Ljava/lang/String;
    :cond_a
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "END"

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkArgu([Ljava/lang/String;[Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 117
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    const-string v9, "Received End Notification from RIL"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_RIL:Z

    .line 120
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkEND()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 121
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    const-string v8, "0"

    invoke-virtual {p0, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 122
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->STAGE_PARALLEL:Z

    .line 123
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    goto/16 :goto_0

    .line 126
    :cond_b
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "STOP"

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkArgu([Ljava/lang/String;[Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 127
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    const-string v9, "Received Stop Notification from RIL"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedStopNotification_RIL:Z

    .line 129
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkSTOP()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 130
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    const-string v8, "0"

    invoke-virtual {p0, v8}, Lcom/sec/factory/aporiented/athandler/AtParallel;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 136
    :cond_c
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtParallel;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    goto/16 :goto_0
.end method

.method public onCompleted(Ljava/lang/String;)Z
    .locals 5
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 37
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCompleted"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Parallel Commands are working done : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    sput-boolean v4, Lcom/sec/factory/aporiented/athandler/AtParallel;->mReceivedEndNotification_MSP:Z

    .line 39
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtParallel;->checkEND()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    const-string v1, "0"

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtParallel;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtParallel;->STAGE_PARALLEL:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtParallel;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    .line 44
    :cond_0
    return v4
.end method
