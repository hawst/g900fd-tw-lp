.class public Lcom/sec/factory/aporiented/athandler/AtFactorst;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFactorst.java"


# instance fields
.field contextTemp:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 15
    const-string v0, "FACTORST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->CMD_NAME:Ljava/lang/String;

    .line 16
    const-string v0, "AtFactorst"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->CLASS_NAME:Ljava/lang/String;

    .line 17
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->NUM_ARGS:I

    .line 18
    return-void
.end method

.method private DoFactoryReset()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 22
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory"

    const-string v2, "com.sec.factory.sysdump.FactoryReset"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 25
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 26
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 36
    monitor-enter p0

    const/4 v0, 0x0

    .line 38
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtFactorst;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 39
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 59
    :goto_1
    monitor-exit p0

    return-object v1

    .line 38
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->NUM_ARGS:I

    goto :goto_0

    .line 42
    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 43
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "handleCommand"

    const-string v3, "run factory reset"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->DoFactoryReset()V

    .line 45
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 59
    goto :goto_1

    .line 47
    :cond_2
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "2"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 48
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "handleCommand"

    const-string v3, "run CP reset, It is a CP CMD"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x0

    goto :goto_2

    .line 51
    :cond_3
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "4"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 52
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactorst;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "handleCommand"

    const-string v3, "run CP2 reset, It is a CP CMD"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x0

    goto :goto_2

    .line 56
    :cond_4
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFactorst;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 36
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
