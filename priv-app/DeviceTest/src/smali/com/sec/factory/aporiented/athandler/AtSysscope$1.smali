.class Lcom/sec/factory/aporiented/athandler/AtSysscope$1;
.super Landroid/content/BroadcastReceiver;
.source "AtSysscope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtSysscope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtSysscope;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtSysscope;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtSysscope;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 34
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.sec.intent.action.SYSSCOPESTATUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 37
    const-string v1, "status"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SysScope scanning finished"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtSysscope;

    const-string v2, "Result"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I
    invoke-static {v1, v2}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->access$002(Lcom/sec/factory/aporiented/athandler/AtSysscope;I)I

    .line 39
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtSysscope;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtSysscope;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "mSysScopeReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSysScopeStatus : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtSysscope;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I
    invoke-static {v4}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->access$000(Lcom/sec/factory/aporiented/athandler/AtSysscope;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtSysscope;

    # invokes: Lcom/sec/factory/aporiented/athandler/AtSysscope;->changeSysScopeStatus()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->access$100(Lcom/sec/factory/aporiented/athandler/AtSysscope;)Ljava/lang/String;

    .line 43
    :cond_0
    return-void
.end method
