.class public Lcom/sec/factory/aporiented/FailDumpService;
.super Landroid/app/Service;
.source "FailDumpService.java"


# static fields
.field private static final ACQUIRE_WAKE_LOCK:I = 0x3ed

.field private static final BTLOG_PATH:Ljava/lang/String; = "/mnt/sdcard/log/bt.log"

.field private static final CLASS_NAME:Ljava/lang/String; = "FailDumpService"

.field private static final DUMP_DESTPATH:Ljava/lang/String; = "/data/log"

.field static final FACTORY_CMD_PROPERTY:Ljava/lang/String; = "ril.factory_cmd"

.field private static final ISEOS2_MODEMLOG_DONE:I = 0x3f5

.field private static final QUERY_APLOG_DONE_FAIL:I = 0x3f8

.field private static final QUERY_APLOG_DONE_SUCCESS:I = 0x3f7

.field private static final QUERY_COPY_LOG_DONE:I = 0x3ef

.field private static final QUERY_MODEMLOG_DONE:I = 0x3f6

.field private static final RELEASE_WAKE_LOCK:I = 0x3ee

.field static final STAGE_PROPERTY:Ljava/lang/String; = "ril.factory_mode"


# instance fields
.field private PastMainCMD:Ljava/lang/String;

.field private currentStage:Ljava/lang/String;

.field private isEOS2:Z

.field private mAPLogsDoneFlag:Z

.field private mCPLogsDoneFlag:Z

.field private mConnectionSuccess:Z

.field private mCopyLogDoneFlag:Z

.field public mHandler:Landroid/os/Handler;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private sysdump_time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 41
    iput-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mServiceMessenger:Landroid/os/Messenger;

    .line 51
    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z

    .line 52
    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z

    .line 53
    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z

    .line 54
    iput-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 55
    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mConnectionSuccess:Z

    .line 61
    const-string v1, "00"

    iput-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->PastMainCMD:Ljava/lang/String;

    .line 62
    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EOS2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "u2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->isEOS2:Z

    .line 64
    new-instance v0, Lcom/sec/factory/aporiented/FailDumpService$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/FailDumpService$1;-><init>(Lcom/sec/factory/aporiented/FailDumpService;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 76
    new-instance v0, Lcom/sec/factory/aporiented/FailDumpService$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/FailDumpService$2;-><init>(Lcom/sec/factory/aporiented/FailDumpService;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    .line 163
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mSvcModeMessenger:Landroid/os/Messenger;

    return-void
.end method

.method private DoShellCmd(Ljava/lang/String;)Z
    .locals 9
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 340
    const-string v6, "FailDumpService"

    const-string v7, "DoShellCmd : "

    const-string v8, "cmd"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const/4 v2, 0x0

    .line 342
    .local v2, "p":Ljava/lang/Process;
    const/4 v6, 0x3

    new-array v3, v6, [Ljava/lang/String;

    const-string v6, "/system/bin/sh"

    aput-object v6, v3, v5

    const-string v6, "-c"

    aput-object v6, v3, v4

    const/4 v6, 0x2

    aput-object p1, v3, v6

    .line 347
    .local v3, "shell_command":[Ljava/lang/String;
    :try_start_0
    const-string v6, "FailDumpService"

    const-string v7, "exec command"

    invoke-static {v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    .line 349
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    .line 350
    const-string v6, "FailDumpService"

    const-string v7, "exec done"

    invoke-static {v6, v7}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 363
    const-string v5, "FailDumpService"

    const-string v6, "DoShellCmd done"

    invoke-static {v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :goto_0
    return v4

    .line 351
    :catch_0
    move-exception v1

    .line 352
    .local v1, "exception":Ljava/io/IOException;
    const-string v4, "FailDumpService"

    const-string v6, "DoShellCmd - IOException"

    invoke-static {v4, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 353
    goto :goto_0

    .line 354
    .end local v1    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 355
    .local v1, "exception":Ljava/lang/SecurityException;
    const-string v4, "FailDumpService"

    const-string v6, "DoShellCmd - SecurityException"

    invoke-static {v4, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 356
    goto :goto_0

    .line 357
    .end local v1    # "exception":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v4, "FailDumpService"

    const-string v6, "DoShellCmd - InterruptedException"

    invoke-static {v4, v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v4, v5

    .line 360
    goto :goto_0
.end method

.method private WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "out"    # Ljava/lang/String;
    .param p3, "DumpType"    # Ljava/lang/String;

    .prologue
    .line 428
    const-string v10, "FailDumpService"

    const-string v11, "write to sdcard DumpType : "

    move-object/from16 v0, p3

    invoke-static {v10, v11, v0}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const/4 v9, 0x1

    .line 432
    .local v9, "result":Z
    const/4 v3, 0x0

    .line 433
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 435
    .local v6, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 437
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    :try_start_2
    new-array v1, v10, [B

    .line 439
    .local v1, "buffer":[B
    :goto_0
    invoke-virtual {v4, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    .local v8, "n":I
    const/4 v10, -0x1

    if-le v8, v10, :cond_2

    .line 440
    const/4 v10, 0x0

    invoke-virtual {v7, v1, v10, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 443
    .end local v1    # "buffer":[B
    .end local v8    # "n":I
    :catch_0
    move-exception v5

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 444
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .local v5, "fnfe":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v10, "FailDumpService"

    const-string v11, "fnfe : "

    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const/4 v9, 0x0

    .line 446
    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v11, "// Exception from"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 447
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 452
    if-eqz v6, :cond_0

    .line 454
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 459
    :goto_2
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 465
    :cond_0
    :goto_3
    if-eqz v3, :cond_1

    .line 467
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 474
    .end local v5    # "fnfe":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_4
    return v9

    .line 442
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "n":I
    :cond_2
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_f
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 452
    if-eqz v7, :cond_3

    .line 454
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 459
    :goto_5
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    .line 465
    :cond_3
    :goto_6
    if-eqz v4, :cond_7

    .line 467
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 470
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 455
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 456
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 460
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 461
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 468
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v2

    .line 469
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .line 470
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 455
    .end local v1    # "buffer":[B
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v8    # "n":I
    .restart local v5    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v2

    .line 456
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 460
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v2

    .line 461
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 468
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v2

    .line 469
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 448
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_7
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_7
    const/4 v9, 0x0

    .line 450
    :try_start_b
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 452
    if-eqz v6, :cond_4

    .line 454
    :try_start_c
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    .line 459
    :goto_8
    :try_start_d
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    .line 465
    :cond_4
    :goto_9
    if-eqz v3, :cond_1

    .line 467
    :try_start_e
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    goto/16 :goto_4

    .line 468
    :catch_8
    move-exception v2

    .line 469
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 455
    :catch_9
    move-exception v2

    .line 456
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 460
    :catch_a
    move-exception v2

    .line 461
    const-string v10, "FailDumpService"

    const-string v11, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 452
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    :goto_a
    if-eqz v6, :cond_5

    .line 454
    :try_start_f
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/FileDescriptor;->sync()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    .line 459
    :goto_b
    :try_start_10
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_c

    .line 465
    :cond_5
    :goto_c
    if-eqz v3, :cond_6

    .line 467
    :try_start_11
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_d

    .line 470
    :cond_6
    :goto_d
    throw v10

    .line 455
    :catch_b
    move-exception v2

    .line 456
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b

    .line 460
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v2

    .line 461
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    .line 468
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v2

    .line 469
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d

    .line 452
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v10

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v10

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 448
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_e
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_f
    move-exception v2

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .line 443
    :catch_10
    move-exception v5

    goto/16 :goto_1

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_11
    move-exception v5

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "n":I
    :cond_7
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method static synthetic access$002(Lcom/sec/factory/aporiented/FailDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/factory/aporiented/FailDumpService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/FailDumpService;->doWakeLock(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/factory/aporiented/FailDumpService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FailDumpService;->getCopyLog()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/factory/aporiented/FailDumpService;Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # Ljava/io/File;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/aporiented/FailDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/factory/aporiented/FailDumpService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/factory/aporiented/FailDumpService;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/factory/aporiented/FailDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/factory/aporiented/FailDumpService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/factory/aporiented/FailDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/factory/aporiented/FailDumpService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/factory/aporiented/FailDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/factory/aporiented/FailDumpService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/factory/aporiented/FailDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mConnectionSuccess:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/factory/aporiented/FailDumpService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->isEOS2:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/factory/aporiented/FailDumpService;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FailDumpService;->getModemLogIPC()[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/factory/aporiented/FailDumpService;[BLandroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # [B
    .param p2, "x2"    # Landroid/os/Message;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/factory/aporiented/FailDumpService;->sendMessage([BLandroid/os/Message;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/factory/aporiented/FailDumpService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FailDumpService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/FailDumpService;->DoShellCmd(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private connectToRilService()V
    .locals 4

    .prologue
    .line 306
    const-string v1, "FailDumpService"

    const-string v2, "connectToRilService"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 308
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/factory/aporiented/FailDumpService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mConnectionSuccess:Z

    .line 310
    return-void
.end method

.method private copyDirectory(Ljava/io/File;Ljava/io/File;)V
    .locals 15
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dest"    # Ljava/io/File;

    .prologue
    .line 368
    const-string v11, "FailDumpService"

    const-string v12, "copyDirectory : "

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " / "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 371
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 372
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->mkdir()Z

    .line 375
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 377
    .local v3, "fileList":[Ljava/lang/String;
    if-eqz v3, :cond_1

    array-length v11, v3

    if-gtz v11, :cond_2

    .line 425
    .end local v3    # "fileList":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 381
    .restart local v3    # "fileList":[Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v11, v3

    if-ge v9, v11, :cond_1

    .line 382
    new-instance v11, Ljava/io/File;

    aget-object v12, v3, v9

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    aget-object v13, v3, v9

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v11, v12}, Lcom/sec/factory/aporiented/FailDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 381
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 386
    .end local v3    # "fileList":[Ljava/lang/String;
    .end local v9    # "i":I
    :cond_3
    const/4 v4, 0x0

    .line 387
    .local v4, "fin":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 390
    .local v7, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .local v5, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 393
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .local v8, "fout":Ljava/io/FileOutputStream;
    const/16 v11, 0x400

    :try_start_2
    new-array v1, v11, [B

    .line 396
    .local v1, "buffer":[B
    :goto_2
    invoke-virtual {v5, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    .local v10, "len":I
    if-lez v10, :cond_5

    .line 397
    const/4 v11, 0x0

    invoke-virtual {v8, v1, v11, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 400
    .end local v1    # "buffer":[B
    .end local v10    # "len":I
    :catch_0
    move-exception v6

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 401
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .local v6, "fnfe":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_3
    sget-object v11, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v12, "// Exception from"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 402
    const-string v11, "FailDumpService"

    const-string v12, "FileNotFoundException : "

    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 406
    if-eqz v4, :cond_4

    .line 408
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 409
    const/4 v4, 0x0

    .line 415
    :cond_4
    :goto_4
    if-eqz v7, :cond_1

    .line 417
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 418
    const/4 v7, 0x0

    goto :goto_0

    .line 399
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v10    # "len":I
    :cond_5
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 406
    if-eqz v5, :cond_6

    .line 408
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 409
    const/4 v4, 0x0

    .line 415
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :goto_5
    if-eqz v8, :cond_1

    .line 417
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 418
    const/4 v7, 0x0

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 410
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 411
    .local v2, "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_6
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_5

    .line 419
    :catch_2
    move-exception v2

    .line 420
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 410
    .end local v1    # "buffer":[B
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .end local v10    # "len":I
    .restart local v6    # "fnfe":Ljava/io/FileNotFoundException;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    .line 411
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 419
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 420
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 403
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v2

    .line 404
    .restart local v2    # "e":Ljava/lang/Exception;
    :goto_6
    :try_start_9
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 406
    if-eqz v4, :cond_7

    .line 408
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    .line 409
    const/4 v4, 0x0

    .line 415
    :cond_7
    :goto_7
    if-eqz v7, :cond_1

    .line 417
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    .line 418
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 410
    :catch_6
    move-exception v2

    .line 411
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 419
    :catch_7
    move-exception v2

    .line 420
    const-string v11, "FailDumpService"

    const-string v12, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 406
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    :goto_8
    if-eqz v4, :cond_8

    .line 408
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    .line 409
    const/4 v4, 0x0

    .line 415
    :cond_8
    :goto_9
    if-eqz v7, :cond_9

    .line 417
    :try_start_d
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    .line 418
    const/4 v7, 0x0

    .line 421
    :cond_9
    :goto_a
    throw v11

    .line 410
    :catch_8
    move-exception v2

    .line 411
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v12, "FailDumpService"

    const-string v13, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 419
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v2

    .line 420
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v12, "FailDumpService"

    const-string v13, "Exception : "

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 406
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_8

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v11

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_8

    .line 403
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_a
    move-exception v2

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_6

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fout":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v2

    move-object v7, v8

    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_6

    .line 400
    :catch_c
    move-exception v6

    goto/16 :goto_3

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_d
    move-exception v6

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method private doWakeLock(Z)V
    .locals 5
    .param p1, "wake"    # Z

    .prologue
    .line 478
    const-string v1, "FailDumpService"

    const-string v2, "doWakeLock : "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wake="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 481
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 482
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/FailDumpService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 483
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string v2, "FailDumpService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 487
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 488
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 489
    const-string v1, "FailDumpService"

    const-string v2, "doWakeLock : "

    const-string v3, "FULL WAKELOCK ON"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :cond_1
    :goto_0
    return-void

    .line 492
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 493
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 494
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 495
    const-string v1, "FailDumpService"

    const-string v2, "doWakeLock : "

    const-string v3, "FULL WAKELOCK OFF"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method private generateAPDumpFileName()Ljava/lang/String;
    .locals 12

    .prologue
    .line 505
    const-string v3, ""

    .line 507
    .local v3, "dumpfilename":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 509
    .local v0, "cal":Ljava/util/Calendar;
    const-string v8, "FailDumpService"

    const-string v9, "generateAPDumpFileName"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getFailDump() : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/factory/aporiented/FailDumpService;->currentStage:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    .line 511
    .local v6, "month":Ljava/lang/String;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x5

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 512
    .local v2, "day":Ljava/lang/String;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 513
    .local v4, "hour":Ljava/lang/String;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 514
    .local v5, "min":Ljava/lang/String;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xd

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v7

    .line 515
    .local v7, "sec":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService;->sysdump_time:Ljava/lang/String;

    .line 516
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "factory_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/factory/aporiented/FailDumpService;->currentStage:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/factory/aporiented/FailDumpService;->PastMainCMD:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_dumpState_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/factory/aporiented/FailDumpService;->sysdump_time:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 519
    new-instance v1, Ljava/io/File;

    const-string v8, "/data/log"

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 520
    .local v1, "dataLogDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 521
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 523
    :cond_0
    const-string v8, "FailDumpService"

    const-string v9, "generateAPDumpFileName"

    invoke-static {v8, v9, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    return-object v3
.end method

.method private getCopyLog()V
    .locals 3

    .prologue
    .line 256
    const-string v1, "FailDumpService"

    const-string v2, "getCopyLog"

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 259
    const-string v1, "External SD Card UnMounted!!\nplease check if USB cable is connected or SD card is not inserted"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 297
    :goto_0
    return-void

    .line 264
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/aporiented/FailDumpService$4;

    invoke-direct {v1, p0}, Lcom/sec/factory/aporiented/FailDumpService$4;-><init>(Lcom/sec/factory/aporiented/FailDumpService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 296
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private getModemLogIPC()[B
    .locals 8

    .prologue
    .line 313
    const/4 v1, 0x7

    .line 314
    .local v1, "OEM_SYSDUMP_FUNCTAG":I
    const/16 v0, 0x12

    .line 315
    .local v0, "OEM_MODEM_LOG":I
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 316
    .local v2, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 319
    .local v3, "dos":Ljava/io/DataOutputStream;
    const/4 v5, 0x7

    :try_start_0
    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 320
    const/16 v5, 0x12

    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 321
    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 322
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    if-eqz v3, :cond_0

    .line 329
    :try_start_1
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 336
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    :cond_1
    :goto_1
    return-object v5

    .line 330
    :catch_0
    move-exception v4

    .line 331
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 323
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 324
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    const-string v5, "FailDumpService"

    const-string v6, "IOException in getModemLogIPC: "

    const-string v7, "e"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 325
    const/4 v5, 0x0

    .line 327
    if-eqz v3, :cond_1

    .line 329
    :try_start_3
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 330
    :catch_2
    move-exception v4

    .line 331
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 327
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_2

    .line 329
    :try_start_4
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 332
    :cond_2
    :goto_2
    throw v5

    .line 330
    :catch_3
    move-exception v4

    .line 331
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private sendMessage([BLandroid/os/Message;)V
    .locals 6
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 166
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 167
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "request"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 168
    invoke-virtual {p2, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 169
    iget-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v3, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 170
    const/4 v0, 0x0

    .line 173
    .local v0, "cnt":I
    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xa

    if-ge v0, v3, :cond_0

    .line 174
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_1

    .line 175
    iget-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 188
    :cond_0
    :goto_1
    return-void

    .line 179
    :cond_1
    :try_start_1
    const-string v3, "FailDumpService"

    const-string v4, "mServiceMessenger is NULL "

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-wide/16 v4, 0xc8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 173
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :catch_0
    move-exception v1

    .line 182
    .local v1, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 186
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 196
    const-string v0, "FailDumpService"

    const-string v1, "onCreate()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FailDumpService;->connectToRilService()V

    .line 198
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 300
    const-string v0, "FailDumpService"

    const-string v1, "onDestroy()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/factory/aporiented/FailDumpService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 303
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 202
    const-string v4, "FailDumpService"

    const-string v5, "onStartCommand()"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    if-nez p1, :cond_1

    move v1, v3

    .line 204
    .local v1, "hasCP":Z
    :goto_0
    invoke-direct {p0, v3}, Lcom/sec/factory/aporiented/FailDumpService;->doWakeLock(Z)V

    .line 205
    const-string v3, "ril.factory_mode"

    const-string v4, "none"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->currentStage:Ljava/lang/String;

    .line 206
    const-string v3, "ril.factory_cmd"

    const-string v4, "00"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->PastMainCMD:Ljava/lang/String;

    .line 209
    iget-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->currentStage:Ljava/lang/String;

    const-string v4, "[0-9a-zA-Z]+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/factory/aporiented/FailDumpService;->PastMainCMD:Ljava/lang/String;

    const-string v4, "[0-9a-zA-Z]+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 210
    :cond_0
    const-string v3, "FailDumpService"

    const-string v4, "generateAPDumpFileName"

    const-string v5, "Invalid Input"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/FailDumpService;->stopSelf()V

    .line 252
    :goto_1
    return v8

    .line 203
    .end local v1    # "hasCP":Z
    :cond_1
    const-string v4, "has_modem"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 214
    .restart local v1    # "hasCP":Z
    :cond_2
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FailDumpService;->generateAPDumpFileName()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "filename":Ljava/lang/String;
    iput-boolean v7, p0, Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z

    .line 217
    iput-boolean v7, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z

    .line 218
    iput-boolean v7, p0, Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z

    .line 221
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/factory/aporiented/FailDumpService$3;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/factory/aporiented/FailDumpService$3;-><init>(Lcom/sec/factory/aporiented/FailDumpService;ZLjava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 251
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_1
.end method
