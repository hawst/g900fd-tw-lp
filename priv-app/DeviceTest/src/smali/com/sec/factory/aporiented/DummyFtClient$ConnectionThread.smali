.class Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;
.super Ljava/lang/Thread;
.source "DummyFtClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/DummyFtClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionThread"
.end annotation


# instance fields
.field private active:Z

.field private final context:Landroid/content/Context;

.field private in:Ljava/io/BufferedReader;

.field final synthetic this$0:Lcom/sec/factory/aporiented/DummyFtClient;


# direct methods
.method public constructor <init>(Lcom/sec/factory/aporiented/DummyFtClient;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 210
    iput-object p2, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->context:Landroid/content/Context;

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z

    .line 212
    return-void
.end method


# virtual methods
.method public kill()V
    .locals 3

    .prologue
    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z

    .line 295
    const-string v0, "DummyFtClient"

    const-string v1, "run"

    const-string v2, "Kill ConnectionThread"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 216
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    const-string v8, "ConnectionThread start"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const/4 v0, 0x0

    .line 218
    .local v0, "connected":Z
    const/4 v2, 0x0

    .line 219
    .local v2, "garbage_count":I
    new-instance v4, Landroid/net/LocalSocketAddress;

    const-string v6, "FactoryClientRecv"

    invoke-direct {v4, v6}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    .line 220
    .local v4, "lsa_recv":Landroid/net/LocalSocketAddress;
    new-instance v5, Landroid/net/LocalSocketAddress;

    const-string v6, "FactoryClientSend"

    invoke-direct {v5, v6}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    .line 222
    .local v5, "lsa_send":Landroid/net/LocalSocketAddress;
    :cond_0
    if-nez v0, :cond_5

    .line 224
    :try_start_0
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v6, v6, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v6}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v6

    if-nez v6, :cond_1

    .line 225
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v6, v6, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v6, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 226
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    const-string v8, "Connect client socket(receiver)"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_1
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v6, v6, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v6}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v6

    if-nez v6, :cond_2

    .line 230
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v6, v6, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v6, v5}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 231
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    const-string v8, "Connect client socket(sender)"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 237
    :cond_2
    :goto_0
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v6, v6, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v6}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v6, v6, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v6}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 238
    const/4 v0, 0x1

    .line 239
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    const-string v8, "Client socket connect success"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :goto_1
    if-eqz v0, :cond_0

    .line 251
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    const-string v8, "Connected to AT Core"

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z

    .line 253
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    new-instance v7, Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v8, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v8, v8, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-direct {v7, v8}, Lcom/sec/factory/aporiented/ResponseWriter;-><init>(Landroid/net/LocalSocket;)V

    iput-object v7, v6, Lcom/sec/factory/aporiented/DummyFtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 256
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    iget-object v8, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/DummyFtClient;

    iget-object v8, v8, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v8}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->in:Ljava/io/BufferedReader;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 266
    :cond_3
    :goto_2
    iget-boolean v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z

    if-eqz v6, :cond_0

    .line 268
    :try_start_2
    iget-object v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->in:Ljava/io/BufferedReader;

    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 270
    .local v3, "incoming":Ljava/lang/String;
    if-nez v3, :cond_3

    .line 273
    add-int/lit8 v2, v2, 0x1

    .line 274
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    const-string v8, "Garbage data incoming..."

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/16 v6, 0xa

    if-le v2, v6, :cond_3

    .line 277
    const-string v6, "DummyFtClient"

    const-string v7, "run"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Stop DummyFtClient(Garbage data count="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 283
    .end local v3    # "incoming":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 284
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 285
    iput-boolean v10, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z

    .line 286
    const/4 v0, 0x0

    .line 287
    goto :goto_2

    .line 233
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 234
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 241
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    const/4 v0, 0x0

    .line 244
    const-wide/16 v6, 0xbb8

    :try_start_3
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 245
    :catch_2
    move-exception v1

    .line 246
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 258
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v1

    .line 259
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 260
    iput-boolean v10, p0, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->active:Z

    .line 261
    const/4 v0, 0x0

    goto :goto_2

    .line 291
    .end local v1    # "e":Ljava/io/IOException;
    :cond_5
    return-void
.end method
