.class public Lcom/sec/factory/aporiented/athandler/AtMicsd;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtMicsd.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 13
    const-string v0, "MICSD"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->CMD_NAME:Ljava/lang/String;

    .line 14
    const-string v0, "AtMicsd"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->CLASS_NAME:Ljava/lang/String;

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->NUM_ARGS:I

    .line 16
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 19
    monitor-enter p0

    const/4 v0, 0x0

    .line 21
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtMicsd;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 22
    const-string v1, "wrong"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :goto_1
    monitor-exit p0

    return-object v1

    .line 21
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->NUM_ARGS:I

    goto :goto_0

    .line 25
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->context:Landroid/content/Context;

    invoke-static {v1}, Landroid/os/FactoryTest;->isAutomaticTestMode(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 26
    const/4 v1, 0x0

    goto :goto_1

    .line 29
    :cond_2
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtMicsd;->getCmdType()I

    move-result v1

    if-nez v1, :cond_3

    .line 30
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtMicsd;->setResultType(I)V

    .line 33
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtMicsd;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/factory/modules/ModuleDFT;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDFT;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleDFT;->DftMicsd()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    move-object v1, v0

    .line 34
    goto :goto_1

    .line 19
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
