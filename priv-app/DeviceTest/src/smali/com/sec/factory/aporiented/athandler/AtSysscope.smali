.class public Lcom/sec/factory/aporiented/athandler/AtSysscope;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtSysscope.java"


# static fields
.field private static final ACTION_SYSSCOPESTATUS:Ljava/lang/String; = "com.sec.intent.action.SYSSCOPESTATUS"

.field private static final NOK:I = 0x2

.field private static final NOT_SCANED:I = -0x1

.field private static final OK:I = 0x1


# instance fields
.field private isSysScopeStatus:I

.field private mSysScopeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    .line 31
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtSysscope;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->mSysScopeReceiver:Landroid/content/BroadcastReceiver;

    .line 23
    const-string v0, "SYSSCOPE"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->CMD_NAME:Ljava/lang/String;

    .line 24
    const-string v0, "AtSysscope"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->CLASS_NAME:Ljava/lang/String;

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->NUM_ARGS:I

    .line 26
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 27
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->mSysScopeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.sec.intent.action.SYSSCOPESTATUS"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 28
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->changeSysScopeStatus()Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    .line 31
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtSysscope;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->mSysScopeReceiver:Landroid/content/BroadcastReceiver;

    .line 48
    const-string v0, "SYSSCOPE"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->CMD_NAME:Ljava/lang/String;

    .line 49
    const-string v0, "AtSysscope"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->CLASS_NAME:Ljava/lang/String;

    .line 50
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->NUM_ARGS:I

    .line 51
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/athandler/AtSysscope;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtSysscope;

    .prologue
    .line 14
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/factory/aporiented/athandler/AtSysscope;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtSysscope;
    .param p1, "x1"    # I

    .prologue
    .line 14
    iput p1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/factory/aporiented/athandler/AtSysscope;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtSysscope;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->changeSysScopeStatus()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private changeSysScopeStatus()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "status":Ljava/lang/String;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 58
    .local v2, "ut":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 59
    const-wide/16 v2, 0x1

    .line 62
    :cond_0
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    if-ne v1, v8, :cond_1

    const-wide/16 v4, 0x78

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 63
    const-string v0, "MODIFIED"

    .line 74
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "changeSysScopeStatus : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ", elapsedRealtime()="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    return-object v0

    .line 65
    :cond_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 66
    const-string v0, "MODIFIED"

    goto :goto_0

    .line 67
    :cond_2
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->isSysScopeStatus:I

    if-ne v1, v8, :cond_3

    .line 68
    const-string v0, "SCANNING"

    goto :goto_0

    .line 70
    :cond_3
    const-string v0, "NORMAL"

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 79
    monitor-enter p0

    const/4 v0, 0x0

    .line 81
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtSysscope;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 92
    :goto_1
    monitor-exit p0

    return-object v1

    .line 81
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtSysscope;->NUM_ARGS:I

    goto :goto_0

    .line 85
    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->changeSysScopeStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 92
    goto :goto_1

    .line 89
    :cond_2
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSysscope;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 79
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
