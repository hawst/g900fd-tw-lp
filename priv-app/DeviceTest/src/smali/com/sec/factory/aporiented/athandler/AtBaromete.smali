.class public Lcom/sec/factory/aporiented/athandler/AtBaromete;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtBaromete.java"


# static fields
.field public static final PREF_KEY:Ljava/lang/String; = "WATER_PROOF"

.field public static final PREF_NAME:Ljava/lang/String; = "UIBarometerWaterProofTest"


# instance fields
.field private WHAT_GET_DATA:I

.field private WHAT_UPDATE:I

.field private final dummyData:C

.field public mCurrentIndex:I

.field mPressureValue:[F

.field mSenserID:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 29
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 20
    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_UPDATE:I

    .line 21
    iput v2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mPressureValue:[F

    .line 23
    iput v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    .line 24
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->dummyData:C

    .line 25
    new-array v0, v2, [I

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    aput v2, v0, v6

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mSenserID:[I

    .line 30
    const-string v0, "BAROMETE"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CMD_NAME:Ljava/lang/String;

    .line 31
    const-string v0, "AtBaromete"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    .line 32
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->NUM_ARGS:I

    .line 34
    new-instance v7, Ljava/io/File;

    const-string v0, "BAROMETE_DELTA"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v7, "mBaroDelta":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v2, "0"

    invoke-virtual {v0, v2}, Lcom/sec/factory/modules/ModuleCommon;->writemBaroDelta(Ljava/lang/String;)V

    .line 38
    const-string v0, "BAROMETE_DELTA"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v6}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 40
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "AtBaromete"

    const-string v2, "BAROMETE_DELTA is created..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :goto_0
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 47
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "AtBaromete"

    const-string v2, "BAROMETE_DELTAis already existed..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    const/4 v2, 0x2

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 20
    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_UPDATE:I

    .line 21
    iput v2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mPressureValue:[F

    .line 23
    iput v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    .line 24
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->dummyData:C

    .line 25
    new-array v0, v2, [I

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    aput v2, v0, v6

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mSenserID:[I

    .line 51
    const-string v0, "BAROMETE"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CMD_NAME:Ljava/lang/String;

    .line 52
    const-string v0, "AtBaromete"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    .line 53
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->NUM_ARGS:I

    .line 55
    new-instance v7, Ljava/io/File;

    const-string v0, "BAROMETE_DELTA"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 57
    .local v7, "mBaroDelta":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v2, "0"

    invoke-virtual {v0, v2}, Lcom/sec/factory/modules/ModuleCommon;->writemBaroDelta(Ljava/lang/String;)V

    .line 59
    const-string v0, "BAROMETE_DELTA"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v6}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 61
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "AtBaromete"

    const-string v2, "BAROMETE_DELTA is created..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :goto_0
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 68
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "AtBaromete"

    const-string v2, "BAROMETE_DELTAis already existed..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/athandler/AtBaromete;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtBaromete;

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/factory/aporiented/athandler/AtBaromete;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtBaromete;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getDataBaro()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/factory/aporiented/athandler/AtBaromete;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtBaromete;

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/factory/aporiented/athandler/AtBaromete;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtBaromete;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getPressureResultUpdate()V

    return-void
.end method

.method private getDataBaro()V
    .locals 6

    .prologue
    .line 298
    const/4 v0, 0x0

    .line 299
    .local v0, "mData":[Ljava/lang/String;
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v2, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 302
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mPressureValue:[F

    iget v2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    const/4 v3, 0x2

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aput v3, v1, v2

    .line 303
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDataBaro : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mPressureValue:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mPressureValue:[F

    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mCurrentIndex:I

    .line 307
    :cond_0
    return-void
.end method

.method private getDataBaroHandler()V
    .locals 4

    .prologue
    .line 277
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtBaromete$1;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBaromete$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtBaromete;Landroid/os/Looper;)V

    .line 289
    .local v0, "mHandler":Landroid/os/Handler;
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 290
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 291
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 292
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 293
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_GET_DATA:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 294
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->WHAT_UPDATE:I

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 295
    return-void
.end method

.method private getPressureResultUpdate()V
    .locals 10

    .prologue
    .line 311
    const-string v4, ""

    .line 313
    .local v4, "resData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 314
    .local v5, "total":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mPressureValue:[F

    array-length v2, v6

    .line 316
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 317
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mPressureValue:[F

    aget v6, v6, v1

    add-float/2addr v5, v6

    .line 316
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 320
    :cond_0
    int-to-float v6, v2

    div-float v3, v5, v6

    .line 321
    .local v3, "pressureValue":F
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v6, "#.#"

    invoke-direct {v0, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 322
    .local v0, "format":Ljava/text/DecimalFormat;
    float-to-double v6, v3

    invoke-virtual {v0, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 323
    const-string v6, ","

    const-string v7, "."

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 324
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getPressureResultUpdate"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "result :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getCmdType()I

    move-result v6

    if-nez v6, :cond_1

    .line 327
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const/4 v7, 0x2

    const-string v8, "6D"

    const-string v9, "01"

    invoke-virtual {v6, v7, v8, v9, v4}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :goto_1
    return-void

    .line 329
    :cond_1
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    const-string v7, "1"

    invoke-virtual {p0, v7, v4}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private getWaterProofTestData()Ljava/lang/String;
    .locals 7

    .prologue
    .line 334
    const-string v0, "/data/data/com.sec.android.app.hwmoduletest/files/barometerData"

    .line 335
    .local v0, "BAROMETER_DATA_FILE":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getWaterProofTestData"

    invoke-static {v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v3, "/data/data/com.sec.android.app.hwmoduletest/files/barometerData"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 339
    .local v2, "result":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 340
    :cond_0
    const-string v2, ""

    .line 341
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0x16

    if-ge v1, v3, :cond_1

    .line 342
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-1.00,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 341
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 344
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-1.00"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 347
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getWaterProofTestData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    return-object v2
.end method

.method private getWaterProofTestDataFromFTA()Ljava/lang/String;
    .locals 6

    .prologue
    .line 354
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->context:Landroid/content/Context;

    const-string v3, "UIBarometerWaterProofTest"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 355
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "WATER_PROOF"

    const-string v3, "NG_-1.00,-1.00"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getWaterProofTestDataFromFTA"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    return-object v1
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 17
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 73
    monitor-enter p0

    const/4 v9, 0x0

    .line 75
    .local v9, "resData":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p1

    array-length v14, v0

    sget-boolean v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->STAGE_PARALLEL:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->NUM_ARGS:I

    add-int/lit8 v13, v13, 0x1

    :goto_0
    if-eq v14, v13, :cond_1

    .line 76
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 273
    :goto_1
    monitor-exit p0

    return-object v13

    .line 75
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->NUM_ARGS:I

    goto :goto_0

    .line 83
    :cond_1
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 84
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mSenserID:[I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->SensorOff([I)V

    .line 85
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorOn(I)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 86
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "0,0 - Sensor Off : NG"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_2
    :goto_2
    move-object v13, v9

    .line 273
    goto :goto_1

    .line 89
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "0,0 - Sensor Off : OK"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 97
    :cond_4
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 99
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v14, "0"

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleCommon;->writemBaroDelta(Ljava/lang/String;)V

    .line 100
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mSenserID:[I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->SensorOn([I)V

    .line 101
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "0,1,0 "

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-wide/16 v14, 0x1f4

    invoke-static {v14, v15}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 106
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorOn(I)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 107
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "0,1 - Sensor ON : OK"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 110
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "0,1 - Sensor ON : NG"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 115
    :cond_6
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 116
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorOn(I)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 117
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v10

    .line 119
    .local v10, "result":[Ljava/lang/String;
    if-eqz v10, :cond_8

    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "1,0 - Total Read : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x1

    aget-object v16, v10, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v13, 0x2

    aget-object v13, v10, v13

    const-string v14, "1"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 123
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 125
    :cond_7
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 128
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,0 - Total Read : NG (null)"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 132
    .end local v10    # "result":[Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,0 - Total Read : NG (Sensor off)"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 137
    :cond_a
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 138
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorOn(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v13

    if-eqz v13, :cond_e

    .line 141
    const-wide/16 v14, 0x1f4

    :try_start_2
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 146
    :goto_3
    const/16 v3, 0x14

    .line 147
    .local v3, "POLLING_TIME":I
    const/16 v2, 0x64

    .line 149
    .local v2, "POLLING_INTERVAL":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_4
    const/16 v13, 0x14

    if-ge v7, v13, :cond_b

    .line 150
    :try_start_3
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorReady(I)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 157
    :cond_b
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v10

    .line 159
    .restart local v10    # "result":[Ljava/lang/String;
    if-eqz v10, :cond_d

    .line 160
    const/4 v13, 0x0

    aget-object v13, p1, v13

    const/4 v14, 0x4

    aget-object v14, v10, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 161
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "1,1 - Test result : Temperature = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x4

    aget-object v16, v10, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 73
    .end local v2    # "POLLING_INTERVAL":I
    .end local v3    # "POLLING_TIME":I
    .end local v7    # "i":I
    .end local v10    # "result":[Ljava/lang/String;
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 142
    :catch_0
    move-exception v6

    .line 143
    .local v6, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 153
    .end local v6    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "POLLING_INTERVAL":I
    .restart local v3    # "POLLING_TIME":I
    .restart local v7    # "i":I
    :cond_c
    const-wide/16 v14, 0x64

    invoke-static {v14, v15}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 149
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 164
    .restart local v10    # "result":[Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,1 - Test result : NG (null)"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 168
    .end local v2    # "POLLING_INTERVAL":I
    .end local v3    # "POLLING_TIME":I
    .end local v7    # "i":I
    .end local v10    # "result":[Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,1 - Test result : NG (Sensor Off)"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 173
    :cond_f
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_14

    .line 174
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,2,0 "

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorOn(I)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v13

    if-eqz v13, :cond_13

    .line 179
    const-wide/16 v14, 0x1f4

    :try_start_5
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 184
    :goto_5
    const/16 v3, 0x14

    .line 185
    .restart local v3    # "POLLING_TIME":I
    const/16 v2, 0x64

    .line 187
    .restart local v2    # "POLLING_INTERVAL":I
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_6
    const/16 v13, 0x14

    if-ge v7, v13, :cond_10

    .line 188
    :try_start_6
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->isSensorReady(I)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 194
    :cond_10
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    sget v14, Lcom/sec/factory/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v10

    .line 196
    .restart local v10    # "result":[Ljava/lang/String;
    if-eqz v10, :cond_12

    .line 202
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getDataBaroHandler()V

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getCmdType()I

    move-result v13

    if-nez v13, :cond_2

    .line 205
    const-string v9, "WAIT"

    goto/16 :goto_2

    .line 180
    .end local v2    # "POLLING_INTERVAL":I
    .end local v3    # "POLLING_TIME":I
    .end local v7    # "i":I
    .end local v10    # "result":[Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 181
    .restart local v6    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_5

    .line 191
    .end local v6    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "POLLING_INTERVAL":I
    .restart local v3    # "POLLING_TIME":I
    .restart local v7    # "i":I
    :cond_11
    const-wide/16 v14, 0x64

    invoke-static {v14, v15}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 187
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 208
    .restart local v10    # "result":[Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,2 - Test result : NG (null)"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 212
    .end local v2    # "POLLING_INTERVAL":I
    .end local v3    # "POLLING_TIME":I
    .end local v7    # "i":I
    .end local v10    # "result":[Ljava/lang/String;
    :cond_13
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "handleCommand"

    const-string v15, "1,2 - Test result : NG (Sensor Off)"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 217
    :cond_14
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_18

    .line 218
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleCommon;->readmBaroDelta()Ljava/lang/String;

    move-result-object v11

    .line 219
    .local v11, "value":Ljava/lang/String;
    const-string v13, "BAROMETE_VENDOR"

    invoke-static {v13}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 220
    .local v12, "vendor":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "readmBaroDelta"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "vendor="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v10, 0x0

    .line 222
    .local v10, "result":F
    invoke-static {v11}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    .line 224
    const-string v13, "STM"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_16

    .line 225
    const/high16 v13, 0x45800000    # 4096.0f

    div-float/2addr v10, v13

    .line 230
    :cond_15
    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtBaromete;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "readmBaroDelta"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "result="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-static {v10}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v11

    .line 233
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v11}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 235
    goto/16 :goto_2

    .line 226
    :cond_16
    const-string v13, "BOCH"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_17

    const-string v13, "BOSCH"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_15

    .line 227
    :cond_17
    const/high16 v13, 0x42c80000    # 100.0f

    div-float/2addr v10, v13

    goto :goto_7

    .line 237
    .end local v10    # "result":F
    .end local v11    # "value":Ljava/lang/String;
    .end local v12    # "vendor":Ljava/lang/String;
    :cond_18
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "4"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_19

    .line 238
    const/4 v13, 0x0

    aget-object v13, p1, v13

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getWaterProofTestData()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 241
    :cond_19
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "5"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1a

    .line 242
    const/4 v13, 0x0

    aget-object v13, p1, v13

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->getWaterProofTestDataFromFTA()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 245
    :cond_1a
    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1e

    .line 246
    const/4 v4, 0x0

    .line 247
    .local v4, "castoffset":I
    const-string v8, ""

    .line 248
    .local v8, "offSet":Ljava/lang/String;
    const-string v13, "BAROMETE_VENDOR"

    invoke-static {v13}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 249
    .restart local v12    # "vendor":Ljava/lang/String;
    const/4 v5, 0x0

    .line 255
    .local v5, "delta":F
    const/4 v13, 0x2

    aget-object v13, p1, v13

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 257
    const-string v13, "STM"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1c

    .line 258
    const/high16 v13, 0x45800000    # 4096.0f

    mul-float/2addr v5, v13

    .line 264
    :cond_1b
    :goto_8
    float-to-int v4, v5

    .line 265
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    .line 266
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtBaromete;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v13, v8}, Lcom/sec/factory/modules/ModuleCommon;->writemBaroDelta(Ljava/lang/String;)V

    .line 267
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 268
    goto/16 :goto_2

    .line 259
    :cond_1c
    const-string v13, "BOCH"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1d

    const-string v13, "BOSCH"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1b

    .line 260
    :cond_1d
    const/high16 v13, 0x42c80000    # 100.0f

    mul-float/2addr v5, v13

    goto :goto_8

    .line 270
    .end local v4    # "castoffset":I
    .end local v5    # "delta":F
    .end local v8    # "offSet":Ljava/lang/String;
    .end local v12    # "vendor":Ljava/lang/String;
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtBaromete;->responseNA()Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v9

    goto/16 :goto_2
.end method
