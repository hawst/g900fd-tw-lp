.class public Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFeliCaRfCal.java"


# static fields
.field private static final ACTION_FELICA_CHANGE_CAL_RESPONSE:Ljava/lang/String; = "com.sec.android.app.felicatest.FELICA_TEST_RESPONSE"

.field private static final ACTION_FELICA_END_RESPONSE:Ljava/lang/String; = "com.sec.android.app.felicatest.FELICA_TEST_GETNV_RESPONSE"

.field private static final ACTION_FELICA_FAIL_RESPONSE:Ljava/lang/String; = "com.sec.android.app.felicatest.FELICA_TEST_FAIL_RESPONSE"

.field private static final ACTION_FELICA_GETIDM_RESPONSE:Ljava/lang/String; = "com.sec.android.app.felicatest.FELICA_GETIDM_RESPONSE"

.field private static final ACTION_FELICA_LOCK_REQUEST:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_REQUEST"

.field private static final ACTION_FELICA_LOCK_RESET_FAIL_RESPONSE:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_RESET_FAIL_RESPONSE"

.field private static final ACTION_FELICA_LOCK_RESET_PASS_RESPONSE:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_RESET_PASS_RESPONSE"

.field private static final ACTION_FELICA_LOCK_STATUS_ERROR_RESPONSE:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_STATUS_ERROR_RESPONSE"

.field private static final ACTION_FELICA_LOCK_STATUS_LOCK_RESPONSE:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_STATUS_LOCK_RESPONSE"

.field private static final ACTION_FELICA_LOCK_STATUS_UNLOCK_RESPONSE:Ljava/lang/String; = "com.samsung.felicalock.FELICA_LOCK_STATUS_UNLOCK_RESPONSE"

.field private static final ACTION_FELICA_PASS_RESPONSE:Ljava/lang/String; = "com.sec.android.app.felicatest.FELICA_TEST_PASS_RESPONSE"

.field private static final ACTION_FELICA_START_RESPONSE:Ljava/lang/String; = "com.sec.android.app.felicatest.FELICA_TEST_SETNV_RESPONSE"

.field private static final MSG_FELICA_TEST_FAIL:I = 0x1001

.field private static final MSG_FELICA_TEST_GETNV:I = 0x1003

.field private static final MSG_FELICA_TEST_INDICATION:I = 0x1004

.field private static final MSG_FELICA_TEST_PASS:I = 0x1002

.field private static isLaunchFeliCaTest:Z


# instance fields
.field private mFeliCaTestHandler:Landroid/os/Handler;

.field public mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->isLaunchFeliCaTest:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 72
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 42
    const-string v0, "FLCRFCAL"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CMD_NAME:Ljava/lang/String;

    .line 43
    const-string v0, "AtFeliCaRfCal"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->NUM_ARGS:I

    .line 46
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 47
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 15
    sput-boolean p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->isLaunchFeliCaTest:Z

    return p0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 143
    monitor-enter p0

    const/4 v0, 0x0

    .line 145
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "handleCommand"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 148
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 189
    :goto_1
    monitor-exit p0

    return-object v1

    .line 147
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->NUM_ARGS:I

    goto :goto_0

    .line 151
    :cond_1
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->startReceiver()V

    .line 154
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 155
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicaDeactivationWhereAtCmd()V

    .line 156
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=0,0"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    move-object v1, v0

    .line 189
    goto :goto_1

    .line 158
    :cond_2
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 159
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicaActivationWhereAtcmd()V

    .line 160
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=0,1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 143
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 162
    :cond_3
    const/4 v1, 0x1

    :try_start_2
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 163
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->felicaSetDataNTxWhereAtcmd(I)V

    .line 164
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AT+FLCRFCAL=1,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 166
    :cond_4
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "2"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 167
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicaSetDataPassWhereAtCmd()V

    .line 168
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=2,1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 170
    :cond_5
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "2"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 171
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicaSetDataFailWhereAtCmd()V

    .line 172
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=2,0"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 174
    :cond_6
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "3"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 175
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicafelicaIDmReadWhereAtCmd()V

    .line 176
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=3,0"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 178
    :cond_7
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "4"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 179
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicaInitPassword()V

    .line 180
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=4,1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 182
    :cond_8
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "4"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "2"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 183
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->felicaReadPassword()V

    .line 184
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "AT+FLCRFCAL=4,2"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 187
    :cond_9
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto/16 :goto_2
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 51
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Receiver is started"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 53
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_SETNV_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 54
    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 55
    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_GETNV_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_PASS_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 57
    const-string v1, "com.sec.android.app.felicatest.FELICA_TEST_FAIL_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 58
    const-string v1, "com.sec.android.app.felicatest.FELICA_GETIDM_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 59
    const-string v1, "com.samsung.felicalock.FELICA_LOCK_STATUS_LOCK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 60
    const-string v1, "com.samsung.felicalock.FELICA_LOCK_STATUS_UNLOCK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 61
    const-string v1, "com.samsung.felicalock.FELICA_LOCK_STATUS_ERROR_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    const-string v1, "com.samsung.felicalock.FELICA_LOCK_RESET_PASS_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 63
    const-string v1, "com.samsung.felicalock.FELICA_LOCK_RESET_FAIL_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 65
    return-void
.end method

.method public stopReceiver()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Receiver is stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 70
    return-void
.end method
