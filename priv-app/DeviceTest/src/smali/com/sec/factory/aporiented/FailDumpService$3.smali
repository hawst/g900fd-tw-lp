.class Lcom/sec/factory/aporiented/FailDumpService$3;
.super Ljava/lang/Object;
.source "FailDumpService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/FailDumpService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FailDumpService;

.field final synthetic val$filename:Ljava/lang/String;

.field final synthetic val$hasCP:Z


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FailDumpService;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iput-boolean p2, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->val$hasCP:Z

    iput-object p3, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->val$filename:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->val$hasCP:Z

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->isEOS2:Z
    invoke-static {v0}, Lcom/sec/factory/aporiented/FailDumpService;->access$600(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "sys.trace.control"

    const-string v1, "path=dump"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iget-object v0, v0, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3f5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 242
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bugreport > /data/log/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->val$filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->DoShellCmd(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$900(Lcom/sec/factory/aporiented/FailDumpService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iget-object v0, v0, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3f7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 248
    :goto_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->getCopyLog()V
    invoke-static {v0}, Lcom/sec/factory/aporiented/FailDumpService;->access$1000(Lcom/sec/factory/aporiented/FailDumpService;)V

    .line 249
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->getModemLogIPC()[B
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$700(Lcom/sec/factory/aporiented/FailDumpService;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iget-object v2, v2, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->sendMessage([BLandroid/os/Message;)V
    invoke-static {v0, v1, v2}, Lcom/sec/factory/aporiented/FailDumpService;->access$800(Lcom/sec/factory/aporiented/FailDumpService;[BLandroid/os/Message;)V

    goto :goto_0

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const/4 v1, 0x1

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v0, v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$202(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    .line 240
    const-string v0, "FailDumpService"

    const-string v1, "onStartCommand(), No CP"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$3;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iget-object v0, v0, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3f8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
