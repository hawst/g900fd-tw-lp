.class public Lcom/sec/factory/aporiented/athandler/AtPowreset;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtPowreset.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 14
    const-string v0, "POWRESET"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->CMD_NAME:Ljava/lang/String;

    .line 15
    const-string v0, "AtPowreset"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->CLASS_NAME:Ljava/lang/String;

    .line 16
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->NUM_ARGS:I

    .line 17
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 21
    monitor-enter p0

    const/4 v2, 0x0

    .line 23
    .local v2, "resData":Ljava/lang/String;
    :try_start_0
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtPowreset;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 47
    :goto_1
    monitor-exit p0

    return-object v3

    .line 23
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->NUM_ARGS:I

    goto :goto_0

    .line 27
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 28
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 29
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtPowreset;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModulePower;->reboot(B)V

    :goto_2
    move-object v3, v2

    .line 47
    goto :goto_1

    .line 30
    :cond_2
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    .line 32
    :try_start_2
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 33
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.extra.KEY_CONFIRM"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 34
    const-string v3, "SHUTDOWN_NO_BATTERY"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 35
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 36
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->context:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 37
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtPowreset;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "power off"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->responseOK(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_2

    .line 39
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 41
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    goto :goto_2

    .line 44
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPowreset;->responseNA()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_2

    .line 21
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
