.class public Lcom/sec/factory/aporiented/athandler/AtSms;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtSms.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 12
    const-string v0, "SMS"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSms;->CMD_NAME:Ljava/lang/String;

    .line 13
    const-string v0, "AtSms"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSms;->CLASS_NAME:Ljava/lang/String;

    .line 14
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSms;->NUM_ARGS:I

    .line 15
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 20
    monitor-enter p0

    const/4 v0, 0x0

    .line 21
    .local v0, "resData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 23
    .local v1, "result":Z
    :try_start_0
    array-length v6, p1

    sget-boolean v5, Lcom/sec/factory/aporiented/athandler/AtSms;->STAGE_PARALLEL:Z

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtSms;->NUM_ARGS:I

    add-int/lit8 v5, v5, 0x1

    :goto_0
    if-eq v6, v5, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 71
    :goto_1
    monitor-exit p0

    return-object v5

    .line 23
    :cond_0
    :try_start_1
    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtSms;->NUM_ARGS:I

    goto :goto_0

    .line 27
    :cond_1
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtSms;->context:Landroid/content/Context;

    invoke-static {v5}, Landroid/os/FactoryTest;->isAutomaticTestMode(Landroid/content/Context;)Z

    move-result v5

    if-ne v5, v7, :cond_2

    .line 28
    const/4 v5, 0x0

    goto :goto_1

    .line 31
    :cond_2
    const/4 v5, 0x0

    aget-object v5, p1, v5

    const-string v6, "\""

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "smsBox":Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v5, p1, v5

    const-string v6, "\""

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, "smsNumber":Ljava/lang/String;
    const/4 v5, 0x2

    aget-object v3, p1, v5

    .line 35
    .local v3, "smsMessage":Ljava/lang/String;
    const-string v5, "INBOX"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 36
    sget-object v5, Lcom/sec/factory/aporiented/athandler/AtSms;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const-string v6, "content://sms/inbox"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4, v3}, Lcom/sec/factory/modules/ModuleDFT;->makeSMS(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 38
    if-eqz v1, :cond_3

    .line 39
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v5, v0

    .line 71
    goto :goto_1

    .line 41
    :cond_3
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 43
    :cond_4
    const-string v5, "DRAFTS"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 44
    sget-object v5, Lcom/sec/factory/aporiented/athandler/AtSms;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const-string v6, "content://sms/draft"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4, v3}, Lcom/sec/factory/modules/ModuleDFT;->makeSMS(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 46
    if-eqz v1, :cond_5

    .line 47
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 49
    :cond_5
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 51
    :cond_6
    const-string v5, "OUTBOX"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 52
    sget-object v5, Lcom/sec/factory/aporiented/athandler/AtSms;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const-string v6, "content://sms/outbox"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4, v3}, Lcom/sec/factory/modules/ModuleDFT;->makeSMS(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 54
    if-eqz v1, :cond_7

    .line 55
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 57
    :cond_7
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 59
    :cond_8
    const-string v5, "SENTBOX"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 60
    sget-object v5, Lcom/sec/factory/aporiented/athandler/AtSms;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const-string v6, "content://sms/sent"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4, v3}, Lcom/sec/factory/modules/ModuleDFT;->makeSMS(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 62
    if-eqz v1, :cond_9

    .line 63
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 65
    :cond_9
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 68
    :cond_a
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSms;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 20
    .end local v2    # "smsBox":Ljava/lang/String;
    .end local v3    # "smsMessage":Ljava/lang/String;
    .end local v4    # "smsNumber":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method
