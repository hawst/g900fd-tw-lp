.class public Lcom/sec/factory/aporiented/athandler/AtFaildump;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFaildump.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/athandler/AtFaildump$Excuete_Faildump;,
        Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;
    }
.end annotation


# static fields
.field private static final CPDUMP_SET:Ljava/lang/String; = "CPDUMP_SET"

.field static final FACTORY_CMD_PROPERTY:Ljava/lang/String; = "ril.factory_cmd"

.field private static final FAILDUMP_DONE:Ljava/lang/String; = "com.android.sec.FAILDUMP.DONE"

.field private static final HAS_MODEM:Ljava/lang/String; = "has_modem"

.field static final STAGE_PROPERTY:Ljava/lang/String; = "ril.factory_mode"


# instance fields
.field public mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 129
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFaildump$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFaildump;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 37
    const-string v0, "FAILDUMP"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CMD_NAME:Ljava/lang/String;

    .line 38
    const-string v0, "AtFaildump"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    .line 39
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->NUM_ARGS:I

    .line 40
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 129
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFaildump$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFaildump;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 29
    const-string v0, "FAILDUMP"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CMD_NAME:Ljava/lang/String;

    .line 30
    const-string v0, "AtFaildump"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    .line 31
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->NUM_ARGS:I

    .line 32
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 33
    return-void
.end method

.method private generateCPDumpDirName()Ljava/lang/String;
    .locals 10

    .prologue
    .line 228
    const-string v1, ""

    .line 230
    .local v1, "cpdumpdirname":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 232
    .local v0, "cal":Ljava/util/Calendar;
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "generateCPDumpDirName"

    const-string v9, "CP Crash"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x2

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 235
    .local v5, "month":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x5

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "day":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xb

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 237
    .local v3, "hour":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 238
    .local v4, "min":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xd

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    .line 239
    .local v6, "sec":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 241
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "generateCPDumpFileName"

    invoke-static {v7, v8, v1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    return-object v1
.end method

.method private getAPCPDump()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 108
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getAPCPDump"

    const-string v3, "..."

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->startReceiver()V

    .line 110
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->context:Landroid/content/Context;

    const-class v2, Lcom/sec/factory/aporiented/FailDumpService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "none"

    const-string v2, "MODEL_COMMUNICATION_MODE"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    const-string v1, "has_modem"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 119
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 120
    return-void

    .line 114
    :cond_0
    const-string v1, "SUPPORT_MARVELL_RIL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    const-string v1, "has_modem"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 117
    :cond_1
    const-string v1, "has_modem"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private startSilentLogService()V
    .locals 4

    .prologue
    .line 148
    const/4 v1, 0x0

    .line 149
    .local v1, "silentlogging":Ljava/lang/String;
    const-string v2, "dev.silentlog.on"

    const-string v3, "NULL"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 152
    const-string v2, "On"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Silent saving : start now! "

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 155
    .local v0, "SvcIntent":Landroid/content/Intent;
    const-string v2, "com.sec.modem.settings"

    const-string v3, "com.sec.modem.settings.cplogging.SilentLogService"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const-string v2, "modem_profile"

    const-string v3, "default"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v2, "action"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 159
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->context:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 161
    .end local v0    # "SvcIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method DoShellCmd(Ljava/lang/String;)I
    .locals 12
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, -0x1

    .line 164
    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "DoShellCmd"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "cmd = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/4 v4, 0x0

    .line 166
    .local v4, "p":Ljava/lang/Process;
    const/4 v8, 0x3

    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "/system/bin/sh"

    aput-object v9, v5, v8

    const-string v8, "-c"

    aput-object v8, v5, v6

    const/4 v8, 0x2

    aput-object p1, v5, v8

    .line 169
    .local v5, "shell_command":[Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "DoShellCmd"

    const-string v10, "exec command"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    .line 171
    invoke-virtual {v4}, Ljava/lang/Process;->waitFor()I

    .line 172
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;

    invoke-virtual {v4}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v1, p0, v8}, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;-><init>(Lcom/sec/factory/aporiented/athandler/AtFaildump;Ljava/io/InputStream;)V

    .line 173
    .local v1, "err":Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;
    new-instance v3, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v3, p0, v8}, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;-><init>(Lcom/sec/factory/aporiented/athandler/AtFaildump;Ljava/io/InputStream;)V

    .line 174
    .local v3, "output":Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;
    invoke-virtual {v1}, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;->start()V

    .line 175
    invoke-virtual {v3}, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;->start()V

    .line 176
    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "DoShellCmd"

    const-string v10, "exec done"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 188
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    const-string v9, "DoShellCmd done"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    .end local v1    # "err":Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;
    .end local v3    # "output":Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;
    :goto_0
    return v6

    .line 177
    :catch_0
    move-exception v2

    .line 178
    .local v2, "exception":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DoShellCmd - IOException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 179
    goto :goto_0

    .line 180
    .end local v2    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 181
    .local v2, "exception":Ljava/lang/SecurityException;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SecurityException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 182
    goto :goto_0

    .line 183
    .end local v2    # "exception":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "InterruptedException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 185
    goto :goto_0
.end method

.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 52
    monitor-enter p0

    const/4 v2, 0x0

    .line 54
    .local v2, "resData":Ljava/lang/String;
    :try_start_0
    array-length v5, p1

    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->NUM_ARGS:I

    if-ge v5, v6, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 104
    :goto_0
    monitor-exit p0

    return-object v5

    .line 58
    :cond_0
    const/4 v5, 0x2

    :try_start_1
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 59
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->getCmdType()I

    move-result v5

    if-nez v5, :cond_1

    .line 60
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->getAPCPDump()V

    .line 61
    const-string v2, "WAIT"

    :goto_1
    move-object v5, v2

    .line 104
    goto :goto_0

    .line 63
    :cond_1
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->getAPCPDump()V

    .line 64
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->startSilentLogService()V

    .line 65
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInformation()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 67
    :cond_2
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 68
    const-string v5, "factory"

    const-string v6, "BINARY_TYPE"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 69
    const-string v5, "none"

    const-string v6, "MODEL_COMMUNICATION_MODE"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 70
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseNA()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 73
    :cond_3
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->generateCPDumpDirName()Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "mCurrentTime":Ljava/lang/String;
    const-string v5, "FRS_TYPE_EFS"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 75
    const-string v5, "FACTORY_FAILHIST_FRS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CPDUMP_SET:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 76
    .local v3, "result":Z
    if-nez v3, :cond_4

    .line 77
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v6, " write failhist for FRS"

    const-string v7, "result = fail"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .end local v3    # "result":Z
    :goto_2
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    goto :goto_1

    .line 79
    .restart local v3    # "result":Z
    :cond_4
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "write failhist for FRS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 52
    .end local v1    # "mCurrentTime":Ljava/lang/String;
    .end local v3    # "result":Z
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 82
    .restart local v1    # "mCurrentTime":Ljava/lang/String;
    :cond_5
    :try_start_2
    const-string v5, "FAILDUMP_CP"

    const-string v6, "CPDUMP_SET"

    invoke-static {v5, v6}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v5, "FAILDUMP_TIME_CP_CRASH"

    invoke-static {v5, v1}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "setProperties"

    const-string v7, "cmd = CPDUMP_SET"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 89
    .end local v1    # "mCurrentTime":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseNA()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 91
    :cond_7
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "2"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 92
    const-string v5, "factory"

    const-string v6, "BINARY_TYPE"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 93
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFaildump$Excuete_Faildump;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump$Excuete_Faildump;-><init>(Lcom/sec/factory/aporiented/athandler/AtFaildump;)V

    .line 94
    .local v0, "Excute_Faildump":Lcom/sec/factory/aporiented/athandler/AtFaildump$Excuete_Faildump;
    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 95
    .local v4, "t":Ljava/lang/Thread;
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    .line 98
    .end local v0    # "Excute_Faildump":Lcom/sec/factory/aporiented/athandler/AtFaildump$Excuete_Faildump;
    .end local v4    # "t":Ljava/lang/Thread;
    :cond_8
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseNA()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 101
    :cond_9
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFaildump;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto/16 :goto_1
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 124
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 125
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.sec.FAILDUMP.DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 127
    return-void
.end method
