.class Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;
.super Landroid/os/Handler;
.source "AtBakupchk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/athandler/AtBakupchk;->doSystemCall(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtBakupchk;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->context:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 95
    .local v0, "pm":Landroid/os/PowerManager;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 120
    :goto_0
    return-void

    .line 97
    :pswitch_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "nvBackupHandler"

    const-string v3, "I_NVBACKUP "

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v1, "nvbackup"

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "nvBackupHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NVBACKUP done, mNResult="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtBakupchk;->mNResult:I
    invoke-static {v4}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->access$000(Lcom/sec/factory/aporiented/athandler/AtBakupchk;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :pswitch_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "nvBackupHandler"

    const-string v3, "I_NVBACKUP "

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v1, "nverase"

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "nvBackupHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NVERASE done, mNResult="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtBakupchk;->mNResult:I
    invoke-static {v4}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->access$000(Lcom/sec/factory/aporiented/athandler/AtBakupchk;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :pswitch_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "nvBackupHandler"

    const-string v3, "I_NVRESTORE "

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v1, "nvrestore"

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "nvBackupHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NVRESTORE done, mNResult="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtBakupchk;->mNResult:I
    invoke-static {v4}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->access$000(Lcom/sec/factory/aporiented/athandler/AtBakupchk;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
