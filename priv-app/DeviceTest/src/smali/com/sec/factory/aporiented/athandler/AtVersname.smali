.class public Lcom/sec/factory/aporiented/athandler/AtVersname;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtVersname.java"


# instance fields
.field private isNon_LiveDemo:Z

.field private mCameraDevice:Landroid/hardware/Camera;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 14
    const-string v0, "false"

    const-string v1, "LIVE_DEMO_MODEL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->isNon_LiveDemo:Z

    .line 20
    const-string v0, "VERSNAME"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CMD_NAME:Ljava/lang/String;

    .line 21
    const-string v0, "AtVersname"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    .line 22
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->NUM_ARGS:I

    .line 23
    return-void
.end method

.method private getPhone2Ver(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 10
    .param p1, "tempResult"    # Ljava/lang/StringBuffer;

    .prologue
    .line 86
    const/16 v1, 0x4e

    .line 87
    .local v1, "obsolete":C
    const/16 v3, 0x2c

    .line 88
    .local v3, "separator":C
    const-string v4, "NONE"

    .line 89
    .local v4, "terminator":Ljava/lang/String;
    const-string v5, "Unknown"

    .line 90
    .local v5, "unknown":Ljava/lang/String;
    const-string v0, "Not Active"

    .line 91
    .local v0, "Not_Active":Ljava/lang/String;
    const-string v6, "SUPPORT_DUAL_STANBY"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "SUPPORT_2ND_CP"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "SUPPORT_DUAL_STANBY_ONE_CP"

    invoke-static {v6}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 95
    :cond_0
    sget-object v6, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v6}, Lcom/sec/factory/modules/ModuleCommon;->getPhone2Ver()Ljava/lang/String;

    move-result-object v2

    .line 98
    .local v2, "phone2_ver":Ljava/lang/String;
    const-string v6, "-"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    const-string v6, "Unknown"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "Not Active"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 101
    :cond_1
    const/16 v6, 0x4e

    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 106
    :goto_0
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "handleCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "(PHONE2 version)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .end local v2    # "phone2_ver":Ljava/lang/String;
    :cond_2
    return-object p1

    .line 103
    .restart local v2    # "phone2_ver":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private removePrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 27
    .local v0, "modelNumber":Ljava/lang/String;
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommon;->getModelName()Ljava/lang/String;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "removePrefix"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    if-eqz v0, :cond_2

    const-string v1, "SC-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 34
    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "removePrefix"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 46
    :cond_1
    return-object p1

    .line 36
    :cond_2
    if-eqz v0, :cond_0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "removePrefix"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startCameraforFwRead()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 50
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startCameraforFwRead"

    const-string v4, "openCameraDevice"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    .line 54
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    .line 56
    const-string v2, "EXYNOS5410"

    const-string v3, "CHIPSET_NAME"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v2, :cond_0

    .line 58
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private stopCameraforFwRead()Z
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "stopCameraforFwRead"

    const-string v2, "releaseCameraDevice"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    .line 74
    const-string v0, "EXYNOS5410"

    const-string v1, "CHIPSET_NAME"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtVersname;->mCameraDevice:Landroid/hardware/Camera;

    .line 82
    :cond_1
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 44
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    const-string v25, "NOT_APPLICABLE"

    .line 116
    .local v25, "resData":Ljava/lang/String;
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v41, v0

    sget-boolean v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->STAGE_PARALLEL:Z

    if-eqz v40, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->NUM_ARGS:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, 0x1

    :goto_0
    move/from16 v0, v41

    move/from16 v1, v40

    if-eq v0, v1, :cond_1

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v40

    .line 691
    :goto_1
    monitor-exit p0

    return-object v40

    .line 116
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->NUM_ARGS:I

    move/from16 v40, v0

    goto :goto_0

    .line 120
    :cond_1
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_5

    .line 121
    const-string v40, "HW_VER_EFS"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_2

    .line 122
    const-string v40, "NOT_APPLICABLE"

    goto :goto_1

    .line 125
    :cond_2
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getHWVer()Ljava/lang/String;

    move-result-object v24

    .line 127
    .local v24, "res":Ljava/lang/String;
    if-eqz v24, :cond_3

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v40

    if-nez v40, :cond_4

    .line 128
    :cond_3
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v41, "MODEL_HARDWARE_REVISION"

    invoke-static/range {v41 .. v41}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->setHWVer(Ljava/lang/String;)Z

    .line 130
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getHWVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .end local v24    # "res":Ljava/lang/String;
    :goto_2
    move-object/from16 v40, v25

    .line 691
    goto :goto_1

    .line 132
    .restart local v24    # "res":Ljava/lang/String;
    :cond_4
    const/16 v40, 0x0

    aget-object v40, p1, v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto :goto_2

    .line 135
    .end local v24    # "res":Ljava/lang/String;
    :cond_5
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "2"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_7

    .line 138
    const-string v40, "USE_MODEL_NUMBER"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_6

    .line 139
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getModelName()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto :goto_2

    .line 141
    :cond_6
    const/16 v40, 0x0

    aget-object v40, p1, v40

    const-string v41, "MODEL_NUMBER"

    invoke-static/range {v41 .. v41}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto :goto_2

    .line 149
    :cond_7
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "3"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_8

    .line 150
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getMainChipName()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 152
    :cond_8
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "6"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_a

    .line 153
    const-string v40, "USE_USERDATA_VERSION"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_9

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v40

    if-nez v40, :cond_9

    .line 154
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getUserDataVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 156
    :cond_9
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getContentsVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 159
    :cond_a
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "7"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_b

    .line 160
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getPDAVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 167
    :cond_b
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "8"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_c

    .line 168
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getCSCVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 175
    :cond_c
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "9"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_12

    .line 176
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleDevice;->getSimSlot_Count()I

    move-result v28

    .line 177
    .local v28, "simSlotCount":I
    const-string v40, "FEATURE_ENABLE_DYNAMIC_MULTI_SIM"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_f

    .line 178
    const/16 v40, 0x1

    move/from16 v0, v28

    move/from16 v1, v40

    if-le v0, v1, :cond_e

    const-string v40, "SUPPORT_DUAL_STANBY"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_d

    const-string v40, "SUPPORT_2ND_CP"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_d

    const-string v40, "SUPPORT_DUAL_STANBY_ONE_CP"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_e

    .line 179
    :cond_d
    const/16 v40, 0x0

    aget-object v40, p1, v40

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v42, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/factory/modules/ModuleCommon;->getMainSWVer()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "-"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    sget-object v42, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/factory/modules/ModuleCommon;->getPhone2Ver()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 181
    :cond_e
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getMainSWVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 184
    :cond_f
    const-string v40, "SUPPORT_DUAL_STANBY"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_10

    const-string v40, "SUPPORT_2ND_CP"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_10

    const-string v40, "SUPPORT_DUAL_STANBY_ONE_CP"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_11

    .line 185
    :cond_10
    const/16 v40, 0x0

    aget-object v40, p1, v40

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v42, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/factory/modules/ModuleCommon;->getMainSWVer()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "-"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    sget-object v42, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/factory/modules/ModuleCommon;->getPhone2Ver()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 187
    :cond_11
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getMainSWVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 196
    .end local v28    # "simSlotCount":I
    :cond_12
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "9"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "1"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_13

    .line 197
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getIFWIVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 200
    :cond_13
    const/16 v40, 0x2

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "2"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "1"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_16

    .line 202
    const-string v40, "HW_VER_EFS"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_14

    .line 203
    const-string v40, "NOT_APPLICABLE"

    goto/16 :goto_1

    .line 206
    :cond_14
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const/16 v41, 0x2

    aget-object v41, p1, v41

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->setHWVer(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_15

    .line 207
    const/16 v40, 0x0

    aget-object v40, p1, v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 209
    :cond_15
    const/16 v40, 0x0

    aget-object v40, p1, v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 212
    :cond_16
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "3"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "0"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_17

    .line 214
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getStandardVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 222
    :cond_17
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "3"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "1"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_18

    .line 224
    const/16 v40, 0x0

    aget-object v40, p1, v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getBootloaderVer()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    goto/16 :goto_2

    .line 231
    :cond_18
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "3"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "2"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_4d

    .line 233
    const/16 v39, 0x0

    .line 234
    .local v39, "ver":Ljava/lang/String;
    new-instance v29, Ljava/lang/StringBuffer;

    const/16 v40, 0x1e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 235
    .local v29, "tempResult":Ljava/lang/StringBuffer;
    const/16 v17, 0x4e

    .line 236
    .local v17, "obsolete":C
    const/16 v27, 0x2c

    .line 237
    .local v27, "separator":C
    const-string v31, "NONE"

    .line 238
    .local v31, "terminator":Ljava/lang/String;
    const-string v37, "Unknown"

    .line 239
    .local v37, "unknown":Ljava/lang/String;
    const-string v4, "Not Active"

    .line 240
    .local v4, "Not_Active":Ljava/lang/String;
    const/16 v16, 0x0

    .line 244
    .local v16, "model":Ljava/lang/String;
    const-string v40, "USE_MODEL_NUMBER"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_28

    .line 245
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getModelName()Ljava/lang/String;

    move-result-object v16

    .line 250
    :goto_3
    if-eqz v16, :cond_1a

    .line 251
    const-string v40, "Unknown"

    move-object/from16 v0, v16

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_19

    const-string v40, "Not Active"

    move-object/from16 v0, v16

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_29

    .line 252
    :cond_19
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 258
    :cond_1a
    :goto_4
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(Model version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getHWVer()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    .line 264
    .local v14, "hw_ver":Ljava/lang/String;
    :try_start_2
    const-string v40, "Unknown"

    move-object/from16 v0, v40

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_1b

    const-string v40, "Not Active"

    move-object/from16 v0, v40

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_2a

    .line 265
    :cond_1b
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 273
    :goto_5
    const/16 v40, 0x2c

    :try_start_3
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(H/W version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getBootVer()Ljava/lang/String;

    move-result-object v5

    .line 278
    .local v5, "boot_ver":Ljava/lang/String;
    const-string v40, "Unknown"

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_1c

    const-string v40, "Not Active"

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_2b

    .line 279
    :cond_1c
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 284
    :goto_6
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(Boot version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getPDAVer()Ljava/lang/String;

    move-result-object v18

    .line 289
    .local v18, "pda_ver":Ljava/lang/String;
    const-string v40, "Unknown"

    move-object/from16 v0, v18

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_1d

    const-string v40, "Not Active"

    move-object/from16 v0, v18

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_2c

    .line 290
    :cond_1d
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 295
    :goto_7
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(PDA version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getPhoneVer()Ljava/lang/String;

    move-result-object v19

    .line 300
    .local v19, "phone_ver":Ljava/lang/String;
    const-string v40, "Unknown"

    move-object/from16 v0, v19

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_1e

    const-string v40, "Not Active"

    move-object/from16 v0, v19

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_2d

    .line 301
    :cond_1e
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 306
    :goto_8
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleDevice;->getSimSlot_Count()I

    move-result v28

    .line 307
    .restart local v28    # "simSlotCount":I
    const-string v40, "FEATURE_ENABLE_DYNAMIC_MULTI_SIM"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_2e

    .line 308
    const/16 v40, 0x1

    move/from16 v0, v28

    move/from16 v1, v40

    if-le v0, v1, :cond_1f

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtVersname;->getPhone2Ver(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v29

    .line 313
    :cond_1f
    :goto_9
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(PHONE version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-string v40, "USE_USERDATA_VERSION"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_30

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v40

    if-nez v40, :cond_30

    .line 317
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getUserDataVer()Ljava/lang/String;

    move-result-object v38

    .line 318
    .local v38, "userdata_ver":Ljava/lang/String;
    const-string v40, "Unknown"

    move-object/from16 v0, v38

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_20

    const-string v40, "Not Active"

    move-object/from16 v0, v38

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_2f

    .line 319
    :cond_20
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 323
    :goto_a
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(userdata_ver)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    .end local v38    # "userdata_ver":Ljava/lang/String;
    :goto_b
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getIFWIVer()Ljava/lang/String;

    move-result-object v15

    .line 337
    .local v15, "ifwiversion":Ljava/lang/String;
    if-nez v15, :cond_21

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "ifwiversion not found"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v15, "Unknown"

    .line 342
    :cond_21
    const-string v40, "Unknown"

    move-object/from16 v0, v40

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_22

    const-string v40, "Not Active"

    move-object/from16 v0, v40

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_31

    .line 343
    :cond_22
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 348
    :goto_c
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(Hidden version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->getCSCVer()Ljava/lang/String;

    move-result-object v3

    .line 354
    .local v3, "CSCVersion":Ljava/lang/String;
    const-string v40, "Unknown"

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_23

    const-string v40, "Not Active"

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_32

    .line 355
    :cond_23
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 360
    :goto_d
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(CSC version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v40, "NEED_CAMDRIVER_OPEN"

    const/16 v41, 0x1

    invoke-static/range {v40 .. v41}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v40

    if-eqz v40, :cond_24

    .line 366
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtVersname;->startCameraforFwRead()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 370
    :cond_24
    const/16 v22, 0x0

    .line 371
    .local v22, "rear_cam_version":[Ljava/lang/String;
    const/16 v23, 0x0

    .line 372
    .local v23, "rear_cam_version_":Ljava/lang/String;
    const/16 v21, 0x0

    .line 375
    .local v21, "rear_cam_isp_version":Ljava/lang/String;
    :try_start_4
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraRearFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, " "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_33

    .line 376
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraRearFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, " "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v22

    .line 377
    const/16 v40, 0x0

    aget-object v21, v22, v40
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 390
    :goto_e
    :try_start_5
    const-string v40, "NEED_CAMDRIVER_OPEN"

    const/16 v41, 0x1

    invoke-static/range {v40 .. v41}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v40

    if-eqz v40, :cond_25

    .line 392
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtVersname;->stopCameraforFwRead()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 404
    :cond_25
    :goto_f
    if-eqz v21, :cond_26

    :try_start_7
    const-string v40, "N"

    move-object/from16 v0, v21

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_37

    .line 405
    :cond_26
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraRearType()Ljava/lang/String;

    move-result-object v36

    .line 407
    .local v36, "type":Ljava/lang/String;
    if-eqz v36, :cond_36

    .line 408
    const-string v40, "_"

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_35

    .line 409
    const-string v40, "_"

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 410
    .local v8, "cameraType_rear":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "rearCamType : "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const/16 v43, 0x1

    aget-object v43, v8, v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/16 v40, 0x1

    aget-object v40, v8, v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 426
    .end local v8    # "cameraType_rear":[Ljava/lang/String;
    .end local v36    # "type":Ljava/lang/String;
    :goto_10
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(CAM 1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v40, "SUPPORT_CAM_ISP"

    const/16 v41, 0x1

    invoke-static/range {v40 .. v41}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v40

    if-eqz v40, :cond_3a

    .line 433
    if-eqz v22, :cond_38

    .line 434
    const/16 v40, 0x1

    aget-object v20, v22, v40

    .line 441
    .local v20, "rear_cam_fw_phone_version":Ljava/lang/String;
    :goto_11
    if-nez v20, :cond_39

    .line 442
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 447
    :goto_12
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 453
    .end local v20    # "rear_cam_fw_phone_version":Ljava/lang/String;
    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(CAM1-1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 454
    const/4 v12, 0x0

    .line 455
    .local v12, "front_cam_version":[Ljava/lang/String;
    const/4 v13, 0x0

    .line 456
    .local v13, "front_cam_version_":Ljava/lang/String;
    const/4 v11, 0x0

    .line 459
    .local v11, "front_cam_isp_version":Ljava/lang/String;
    :try_start_8
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraFrontFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, " "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_3b

    .line 460
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraFrontFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, " "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 461
    const/16 v40, 0x0

    aget-object v11, v12, v40
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 478
    :goto_14
    if-eqz v11, :cond_27

    :try_start_9
    const-string v40, "N"

    move-object/from16 v0, v40

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_3f

    .line 479
    :cond_27
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraFrontType()Ljava/lang/String;

    move-result-object v36

    .line 481
    .restart local v36    # "type":Ljava/lang/String;
    if-eqz v36, :cond_3e

    .line 482
    const-string v40, "_"

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_3d

    .line 483
    const-string v40, "_"

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 484
    .local v7, "cameraType_front":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "frontCamType : "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const/16 v43, 0x1

    aget-object v43, v7, v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    const/16 v40, 0x1

    aget-object v40, v7, v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 500
    .end local v7    # "cameraType_front":[Ljava/lang/String;
    .end local v36    # "type":Ljava/lang/String;
    :goto_15
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(SUB CAM 1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v40, "SUPPORT_CAM_ISP"

    const/16 v41, 0x1

    invoke-static/range {v40 .. v41}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v40

    if-eqz v40, :cond_42

    const-string v40, "SUPPORT_CAM_FRONT_NOT_ISP"

    const/16 v41, 0x0

    invoke-static/range {v40 .. v41}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v40

    if-nez v40, :cond_42

    .line 508
    if-eqz v12, :cond_40

    .line 509
    const/16 v40, 0x1

    aget-object v10, v12, v40

    .line 515
    .local v10, "front_cam_fw_phone_version":Ljava/lang/String;
    :goto_16
    if-nez v10, :cond_41

    .line 516
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 521
    :goto_17
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 527
    .end local v10    # "front_cam_fw_phone_version":Ljava/lang/String;
    :goto_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(SUB CAM 1-1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x2

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v33

    .line 532
    .local v33, "touchkey_fw_version":Ljava/lang/String;
    if-nez v33, :cond_43

    .line 533
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 538
    :goto_19
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(TOUCH 1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x2

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleBinVersion(I)Ljava/lang/String;

    move-result-object v32

    .line 544
    .local v32, "touchkey_fw_phone_version":Ljava/lang/String;
    if-nez v32, :cond_44

    .line 545
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 550
    :goto_1a
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(TOUCH 1-1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string v40, "SUPPORT_EPEN"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_45

    .line 557
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x3

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 559
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 561
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x3

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleBinVersion(I)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 562
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 572
    :goto_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(E-Write)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleTouchScreen;->isTSPSupport()Z

    move-result v40

    if-eqz v40, :cond_46

    .line 577
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPFirmwareVersionIC()Ljava/lang/String;

    move-result-object v35

    .line 583
    .local v35, "tsp_fw_version":Ljava/lang/String;
    :goto_1c
    if-nez v35, :cond_47

    .line 584
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    :goto_1d
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(TSP 1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleTouchScreen;->isTSPSupport()Z

    move-result v40

    if-eqz v40, :cond_48

    .line 595
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPFirmwareVersionBinary()Ljava/lang/String;

    move-result-object v34

    .line 601
    .local v34, "tsp_fw_phone_version":Ljava/lang/String;
    :goto_1e
    if-nez v34, :cond_49

    .line 602
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 607
    :goto_1f
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(TSP 1-1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 611
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(TSP 2)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 615
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(TSP 2-1)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 621
    :try_start_a
    const-string v40, "SUPPORT_SENSORHUB"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_4a

    .line 622
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x5

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v40

    const-string v41, ","

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 623
    .local v26, "sensorHubFirm":[Ljava/lang/String;
    const/16 v40, 0x0

    aget-object v40, v26, v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 631
    .end local v26    # "sensorHubFirm":[Ljava/lang/String;
    :goto_20
    const/16 v40, 0x2c

    :try_start_b
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "SensorHub MCU"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 636
    :try_start_c
    const-string v40, "SUPPORT_SENSORHUB"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_4b

    .line 637
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x5

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v40

    const-string v41, ","

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 638
    .restart local v26    # "sensorHubFirm":[Ljava/lang/String;
    const/16 v40, 0x1

    aget-object v40, v26, v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 646
    .end local v26    # "sensorHubFirm":[Ljava/lang/String;
    :goto_21
    const/16 v40, 0x2c

    :try_start_d
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "SensorHub Bin"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 653
    :try_start_e
    const-string v40, "SUPPORT_REAR_CAM_OIS"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_4c

    .line 654
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v41, 0x8

    invoke-virtual/range {v40 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v40

    const-string v41, " "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 655
    .local v6, "camOisFirm":[Ljava/lang/String;
    const/16 v40, 0x0

    aget-object v40, v6, v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 656
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 657
    const/16 v40, 0x1

    aget-object v40, v6, v40

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 670
    .end local v6    # "camOisFirm":[Ljava/lang/String;
    :goto_22
    const/16 v40, 0x2c

    :try_start_f
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 671
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "rearCamera OIS\'s F/W - Module / Binary"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    const-string v40, "NONE"

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 675
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v39

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "result : "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const/16 v40, 0x0

    aget-object v40, p1, v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 678
    goto/16 :goto_2

    .line 247
    .end local v3    # "CSCVersion":Ljava/lang/String;
    .end local v5    # "boot_ver":Ljava/lang/String;
    .end local v11    # "front_cam_isp_version":Ljava/lang/String;
    .end local v12    # "front_cam_version":[Ljava/lang/String;
    .end local v13    # "front_cam_version_":Ljava/lang/String;
    .end local v14    # "hw_ver":Ljava/lang/String;
    .end local v15    # "ifwiversion":Ljava/lang/String;
    .end local v18    # "pda_ver":Ljava/lang/String;
    .end local v19    # "phone_ver":Ljava/lang/String;
    .end local v21    # "rear_cam_isp_version":Ljava/lang/String;
    .end local v22    # "rear_cam_version":[Ljava/lang/String;
    .end local v23    # "rear_cam_version_":Ljava/lang/String;
    .end local v28    # "simSlotCount":I
    .end local v32    # "touchkey_fw_phone_version":Ljava/lang/String;
    .end local v33    # "touchkey_fw_version":Ljava/lang/String;
    .end local v34    # "tsp_fw_phone_version":Ljava/lang/String;
    .end local v35    # "tsp_fw_version":Ljava/lang/String;
    :cond_28
    const-string v40, "MODEL_NAME"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_3

    .line 254
    :cond_29
    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_4

    .line 115
    .end local v4    # "Not_Active":Ljava/lang/String;
    .end local v16    # "model":Ljava/lang/String;
    .end local v17    # "obsolete":C
    .end local v25    # "resData":Ljava/lang/String;
    .end local v27    # "separator":C
    .end local v29    # "tempResult":Ljava/lang/StringBuffer;
    .end local v31    # "terminator":Ljava/lang/String;
    .end local v37    # "unknown":Ljava/lang/String;
    .end local v39    # "ver":Ljava/lang/String;
    :catchall_0
    move-exception v40

    monitor-exit p0

    throw v40

    .line 267
    .restart local v4    # "Not_Active":Ljava/lang/String;
    .restart local v14    # "hw_ver":Ljava/lang/String;
    .restart local v16    # "model":Ljava/lang/String;
    .restart local v17    # "obsolete":C
    .restart local v25    # "resData":Ljava/lang/String;
    .restart local v27    # "separator":C
    .restart local v29    # "tempResult":Ljava/lang/StringBuffer;
    .restart local v31    # "terminator":Ljava/lang/String;
    .restart local v37    # "unknown":Ljava/lang/String;
    .restart local v39    # "ver":Ljava/lang/String;
    :cond_2a
    :try_start_10
    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_5

    .line 269
    :catch_0
    move-exception v9

    .line 270
    .local v9, "e":Ljava/lang/Exception;
    const/16 v40, 0x4e

    :try_start_11
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_5

    .line 281
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v5    # "boot_ver":Ljava/lang/String;
    :cond_2b
    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_6

    .line 292
    .restart local v18    # "pda_ver":Ljava/lang/String;
    :cond_2c
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_7

    .line 303
    .restart local v19    # "phone_ver":Ljava/lang/String;
    :cond_2d
    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_8

    .line 310
    .restart local v28    # "simSlotCount":I
    :cond_2e
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtVersname;->getPhone2Ver(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v29

    goto/16 :goto_9

    .line 321
    .restart local v38    # "userdata_ver":Ljava/lang/String;
    :cond_2f
    move-object/from16 v0, v29

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_a

    .line 326
    .end local v38    # "userdata_ver":Ljava/lang/String;
    :cond_30
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 327
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const-string v43, "(Inand/Movinad version)"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 345
    .restart local v15    # "ifwiversion":Ljava/lang/String;
    :cond_31
    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_c

    .line 357
    .restart local v3    # "CSCVersion":Ljava/lang/String;
    :cond_32
    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_d

    .line 378
    .restart local v21    # "rear_cam_isp_version":Ljava/lang/String;
    .restart local v22    # "rear_cam_version":[Ljava/lang/String;
    .restart local v23    # "rear_cam_version_":Ljava/lang/String;
    :cond_33
    :try_start_12
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraRearFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, "_"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_34

    .line 379
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraRearFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, "_"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v22

    .line 380
    const/16 v40, 0x1

    aget-object v21, v22, v40

    goto/16 :goto_e

    .line 382
    :cond_34
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraRearFWver()Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    move-result-object v23

    .line 383
    move-object/from16 v21, v23

    goto/16 :goto_e

    .line 385
    :catch_1
    move-exception v9

    .line 386
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "Failed to read rearCamera\'s F/W"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 393
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v9

    .line 394
    .restart local v9    # "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "stopCamera failed"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 413
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v36    # "type":Ljava/lang/String;
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "rearCamType : "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    .line 417
    :cond_36
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    .line 423
    .end local v36    # "type":Ljava/lang/String;
    :cond_37
    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    .line 436
    :cond_38
    move-object/from16 v20, v23

    .restart local v20    # "rear_cam_fw_phone_version":Ljava/lang/String;
    goto/16 :goto_11

    .line 444
    :cond_39
    move-object/from16 v0, v29

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_12

    .line 449
    .end local v20    # "rear_cam_fw_phone_version":Ljava/lang/String;
    :cond_3a
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 450
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_13

    .line 462
    .restart local v11    # "front_cam_isp_version":Ljava/lang/String;
    .restart local v12    # "front_cam_version":[Ljava/lang/String;
    .restart local v13    # "front_cam_version_":Ljava/lang/String;
    :cond_3b
    :try_start_14
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraFrontFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, "_"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_3c

    .line 463
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraFrontFWver()Ljava/lang/String;

    move-result-object v40

    const-string v41, "_"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 464
    const/16 v40, 0x1

    aget-object v11, v12, v40

    goto/16 :goto_14

    .line 466
    :cond_3c
    sget-object v40, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v40 .. v40}, Lcom/sec/factory/modules/ModuleCommon;->readCameraFrontFWver()Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_3
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    move-result-object v13

    .line 467
    move-object v11, v13

    goto/16 :goto_14

    .line 469
    :catch_3
    move-exception v9

    .line 470
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "Failed to read frontCamera\'s F/W"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 489
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v36    # "type":Ljava/lang/String;
    :cond_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "frontCamType : "

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_15

    .line 493
    :cond_3e
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_15

    .line 497
    .end local v36    # "type":Ljava/lang/String;
    :cond_3f
    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_15

    .line 512
    :cond_40
    move-object v10, v13

    .restart local v10    # "front_cam_fw_phone_version":Ljava/lang/String;
    goto/16 :goto_16

    .line 518
    :cond_41
    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_17

    .line 523
    .end local v10    # "front_cam_fw_phone_version":Ljava/lang/String;
    :cond_42
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 524
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_18

    .line 535
    .restart local v33    # "touchkey_fw_version":Ljava/lang/String;
    :cond_43
    move-object/from16 v0, v29

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_19

    .line 547
    .restart local v32    # "touchkey_fw_phone_version":Ljava/lang/String;
    :cond_44
    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1a

    .line 565
    :cond_45
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 566
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 568
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 569
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_1b

    .line 579
    :cond_46
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "TSP 1 : isTSPSupport() == false"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    const/16 v35, 0x0

    .restart local v35    # "tsp_fw_version":Ljava/lang/String;
    goto/16 :goto_1c

    .line 586
    :cond_47
    move-object/from16 v0, v29

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1d

    .line 597
    :cond_48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "TSP 1-1 : isTSPSupport() == false"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    const/16 v34, 0x0

    .restart local v34    # "tsp_fw_phone_version":Ljava/lang/String;
    goto/16 :goto_1e

    .line 604
    :cond_49
    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/16 :goto_1f

    .line 625
    :cond_4a
    const/16 v40, 0x4e

    :try_start_16
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_20

    .line 627
    :catch_4
    move-exception v9

    .line 628
    .restart local v9    # "e":Ljava/lang/Exception;
    const/16 v40, 0x4e

    :try_start_17
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto/16 :goto_20

    .line 640
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_4b
    const/16 v40, 0x4e

    :try_start_18
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_5
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_21

    .line 642
    :catch_5
    move-exception v9

    .line 643
    .restart local v9    # "e":Ljava/lang/Exception;
    const/16 v40, 0x4e

    :try_start_19
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    goto/16 :goto_21

    .line 659
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_4c
    const/16 v40, 0x4e

    :try_start_1a
    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 660
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 661
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_6
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    goto/16 :goto_22

    .line 663
    :catch_6
    move-exception v9

    .line 664
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtVersname;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "handleCommand"

    const-string v42, "Failed to read rearCamera OIS\'s F/W"

    invoke-static/range {v40 .. v42}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 666
    const/16 v40, 0x2c

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 667
    const/16 v40, 0x4e

    move-object/from16 v0, v29

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_22

    .line 678
    .end local v3    # "CSCVersion":Ljava/lang/String;
    .end local v4    # "Not_Active":Ljava/lang/String;
    .end local v5    # "boot_ver":Ljava/lang/String;
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v11    # "front_cam_isp_version":Ljava/lang/String;
    .end local v12    # "front_cam_version":[Ljava/lang/String;
    .end local v13    # "front_cam_version_":Ljava/lang/String;
    .end local v14    # "hw_ver":Ljava/lang/String;
    .end local v15    # "ifwiversion":Ljava/lang/String;
    .end local v16    # "model":Ljava/lang/String;
    .end local v17    # "obsolete":C
    .end local v18    # "pda_ver":Ljava/lang/String;
    .end local v19    # "phone_ver":Ljava/lang/String;
    .end local v21    # "rear_cam_isp_version":Ljava/lang/String;
    .end local v22    # "rear_cam_version":[Ljava/lang/String;
    .end local v23    # "rear_cam_version_":Ljava/lang/String;
    .end local v27    # "separator":C
    .end local v28    # "simSlotCount":I
    .end local v29    # "tempResult":Ljava/lang/StringBuffer;
    .end local v31    # "terminator":Ljava/lang/String;
    .end local v32    # "touchkey_fw_phone_version":Ljava/lang/String;
    .end local v33    # "touchkey_fw_version":Ljava/lang/String;
    .end local v34    # "tsp_fw_phone_version":Ljava/lang/String;
    .end local v35    # "tsp_fw_version":Ljava/lang/String;
    .end local v37    # "unknown":Ljava/lang/String;
    .end local v39    # "ver":Ljava/lang/String;
    :cond_4d
    const/16 v40, 0x3

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    const-string v42, "3"

    aput-object v42, v40, v41

    const/16 v41, 0x1

    const-string v42, "5"

    aput-object v42, v40, v41

    const/16 v41, 0x2

    const-string v42, "0"

    aput-object v42, v40, v41

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v40

    if-eqz v40, :cond_4f

    .line 679
    const/16 v30, 0x0

    .line 681
    .local v30, "tempVer":Ljava/lang/String;
    const-string v40, "HW_VER_EFS"

    invoke-static/range {v40 .. v40}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_4e

    .line 682
    const-string v40, "NOT_APPLICABLE"

    goto/16 :goto_1

    .line 684
    :cond_4e
    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getPDAVer()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, ","

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getFtaSwVer()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, ","

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleCommon;->getFtaHwVer()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, ","

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    sget-object v41, Lcom/sec/factory/aporiented/athandler/AtVersname;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/factory/modules/ModuleDevice;->readSerialNo()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 685
    const/16 v40, 0x0

    aget-object v40, p1, v40

    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 686
    goto/16 :goto_2

    .line 688
    .end local v30    # "tempVer":Ljava/lang/String;
    :cond_4f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtVersname;->responseNA()Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    move-result-object v25

    goto/16 :goto_2
.end method
