.class Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;
.super Ljava/lang/Object;
.source "DIServiceConnectionManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/DIServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/DIServiceConnectionManager;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/16 v3, 0x3e9

    .line 97
    const-string v0, "DIServiceConnectionManager"

    const-string v1, "onServiceConnected"

    const-string v2, "* onServiceConnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->access$102(Lcom/sec/factory/aporiented/DIServiceConnectionManager;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 99
    iget-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->send(ILjava/lang/String;)Z

    .line 100
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 103
    const-string v0, "DIServiceConnectionManager"

    const-string v1, "onServiceDisconnected"

    const-string v2, "* onServiceDisconnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    const/4 v1, 0x0

    # setter for: Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->access$102(Lcom/sec/factory/aporiented/DIServiceConnectionManager;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 106
    return-void
.end method
