.class public Lcom/sec/factory/aporiented/athandler/AtRamsizec;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtRamsizec.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 10
    const-string v0, "RAMSIZEC"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->CMD_NAME:Ljava/lang/String;

    .line 11
    const-string v0, "AtRamSizec"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->CLASS_NAME:Ljava/lang/String;

    .line 12
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->NUM_ARGS:I

    .line 13
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 18
    monitor-enter p0

    const/4 v1, 0x0

    .line 19
    .local v1, "resData":Ljava/lang/String;
    const/4 v2, 0x0

    .line 21
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 22
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 37
    :goto_1
    monitor-exit p0

    return-object v3

    .line 21
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->NUM_ARGS:I

    goto :goto_0

    .line 25
    :cond_1
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 27
    :try_start_2
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleDevice;->getTotalMemory()Ljava/lang/String;

    move-result-object v2

    .line 28
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3, v2}, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    :goto_2
    move-object v3, v1

    .line 37
    goto :goto_1

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v3, 0x0

    :try_start_3
    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    goto :goto_2

    .line 34
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtRamsizec;->responseNA()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    goto :goto_2

    .line 18
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
