.class public Lcom/sec/factory/aporiented/ParallelCommandsParser;
.super Ljava/lang/Object;
.source "ParallelCommandsParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;,
        Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "ParallelCommandsParser"


# instance fields
.field private mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 162
    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 164
    invoke-static {}, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->getCommandAttr()Ljava/util/ArrayList;

    move-result-object v0

    .line 165
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;>;"
    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    .line 167
    .local v1, "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    iget-object v3, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 170
    .end local v1    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;)Z
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 173
    const-string v3, "ParallelCommandsParser"

    const-string v4, "add"

    invoke-static {v3, v4, p1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 175
    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "commandName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    .line 177
    .local v0, "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {v0, p1}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->addToQueue(Ljava/lang/String;)Z

    .line 187
    :goto_0
    const/4 v2, 0x1

    .end local v0    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .end local v1    # "commandName":Ljava/lang/String;
    :goto_1
    return v2

    .line 180
    .restart local v0    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .restart local v1    # "commandName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v3, "AT+NONE"

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v2, p1}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->addToQueue(Ljava/lang/String;)Z

    goto :goto_0

    .line 183
    .end local v0    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .end local v1    # "commandName":Ljava/lang/String;
    :cond_1
    const-string v3, "ParallelCommandsParser"

    const-string v4, "wrong command"

    invoke-static {v3, v4, p1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getHitCommandsList()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v2, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 194
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 195
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    .line 196
    .local v0, "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    invoke-virtual {v0}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 197
    iget-object v2, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 204
    .end local v0    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    :cond_1
    iget-object v2, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    return-object v2
.end method

.method public getMap()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser;->mCategoryMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method
