.class public Lcom/sec/factory/aporiented/athandler/AtTouch;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtTouch.java"


# instance fields
.field private PROTOCOL:Ljava/lang/String;

.field touchMoveStatus:I

.field x1:Ljava/lang/String;

.field x2:Ljava/lang/String;

.field y1:Ljava/lang/String;

.field y2:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->touchMoveStatus:I

    .line 15
    const-string v0, "TOUCH"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->CMD_NAME:Ljava/lang/String;

    .line 16
    const-string v0, "AtTouch"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->CLASS_NAME:Ljava/lang/String;

    .line 17
    const-string v0, "FACTORY_TEST_PROTOCOL"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->PROTOCOL:Ljava/lang/String;

    .line 19
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->PROTOCOL:Ljava/lang/String;

    const-string v1, "NEW_AT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 20
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->NUM_ARGS:I

    .line 35
    :goto_0
    return-void

    .line 22
    :cond_0
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->NUM_ARGS:I

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 38
    monitor-enter p0

    const/4 v0, 0x0

    .line 40
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 41
    const-string v1, "wrong"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :goto_1
    monitor-exit p0

    return-object v1

    .line 40
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->NUM_ARGS:I

    goto :goto_0

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->context:Landroid/content/Context;

    invoke-static {v1}, Landroid/os/FactoryTest;->isAutomaticTestMode(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v5, :cond_2

    .line 45
    const/4 v1, 0x0

    goto :goto_1

    .line 49
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->PROTOCOL:Ljava/lang/String;

    const-string v2, "NEW_AT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-ne v1, v5, :cond_8

    .line 50
    const/4 v1, 0x2

    aget-object v1, p1, v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 51
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchDown(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->PROTOCOL:Ljava/lang/String;

    const-string v2, "NEW_AT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-ne v1, v5, :cond_c

    .line 75
    const-string v0, "OK\r\n"

    :cond_4
    :goto_3
    move-object v1, v0

    .line 82
    goto :goto_1

    .line 52
    :cond_5
    const/4 v1, 0x2

    aget-object v1, p1, v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 53
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchUp(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 38
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 54
    :cond_6
    const/4 v1, 0x2

    :try_start_2
    aget-object v1, p1, v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 55
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchMove(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 56
    :cond_7
    const/4 v1, 0x2

    aget-object v1, p1, v1

    const-string v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 57
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchLong(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 62
    :cond_8
    const/4 v1, 0x0

    aget-object v1, p1, v1

    const-string v2, "0F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 63
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x5

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchDown(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 64
    :cond_9
    const/4 v1, 0x0

    aget-object v1, p1, v1

    const-string v2, "11"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 65
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x5

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchUp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 66
    :cond_a
    const/4 v1, 0x0

    aget-object v1, p1, v1

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 67
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x5

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchMove(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 68
    :cond_b
    const/4 v1, 0x0

    aget-object v1, p1, v1

    const-string v2, "12"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 69
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtTouch;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x5

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/modules/ModuleDFT;->DftTouchLong(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 78
    :cond_c
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtTouch;->PROTOCOL:Ljava/lang/String;

    const-string v2, "NEW_AT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v1

    if-nez v1, :cond_4

    .line 79
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtTouch;->responseOK(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3
.end method
