.class public Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;
.super Ljava/lang/Object;
.source "ParallelCommandsParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/ParallelCommandsParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommandAttr"
.end annotation


# static fields
.field static table:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getCommandAttr()Ljava/util/ArrayList;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    const-class v19, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;

    monitor-enter v19

    :try_start_0
    sget-object v18, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;

    if-nez v18, :cond_1

    .line 110
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    sput-object v18, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :try_start_1
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v18

    const-string v20, "ParallelCommand"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/factory/support/XMLDataStorage;->getChildElementSet(Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v11

    .line 118
    .local v11, "items":[Lorg/w3c/dom/Element;
    move-object v3, v11

    .local v3, "arr$":[Lorg/w3c/dom/Element;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v12, :cond_2

    aget-object v10, v3, v9

    .line 119
    .local v10, "item":Lorg/w3c/dom/Element;
    invoke-interface {v10}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v18

    const-string v20, "item"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v10}, Lorg/w3c/dom/Element;->getNodeType()S

    move-result v18

    const/16 v20, 0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 122
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "AT+"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface {v10}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v20

    const-string v21, "id"

    invoke-interface/range {v20 .. v21}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 124
    .local v15, "name":Ljava/lang/String;
    invoke-interface {v10}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v18

    const-string v20, "priority"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v17

    .line 126
    .local v17, "priority":Ljava/lang/String;
    const/4 v14, 0x1

    .line 128
    .local v14, "nPriority":I
    :try_start_2
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v14

    .line 133
    :goto_1
    :try_start_3
    invoke-interface {v10}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v18

    const-string v20, "dependencies"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    .line 135
    .local v6, "dependencies":Ljava/lang/String;
    if-eqz v6, :cond_3

    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 137
    .local v5, "depArray":[Ljava/lang/String;
    :goto_2
    invoke-interface {v10}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v18

    const-string v20, "gap"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    .line 138
    .local v8, "gap":Ljava/lang/String;
    const/16 v13, 0x64

    .line 140
    .local v13, "nGap":I
    :try_start_4
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v13

    .line 145
    :goto_3
    :try_start_5
    const-string v18, "ParallelCommandsParser"

    const-string v20, "CommandAttr"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "getCommandAttr-"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v4, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-direct {v4, v15, v14, v5, v13}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;-><init>(Ljava/lang/String;I[Ljava/lang/String;I)V

    .line 149
    .local v4, "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    sget-object v18, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 118
    .end local v4    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .end local v5    # "depArray":[Ljava/lang/String;
    .end local v6    # "dependencies":Ljava/lang/String;
    .end local v8    # "gap":Ljava/lang/String;
    .end local v13    # "nGap":I
    .end local v14    # "nPriority":I
    .end local v15    # "name":Ljava/lang/String;
    .end local v17    # "priority":Ljava/lang/String;
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 112
    .end local v3    # "arr$":[Lorg/w3c/dom/Element;
    .end local v9    # "i$":I
    .end local v10    # "item":Lorg/w3c/dom/Element;
    .end local v11    # "items":[Lorg/w3c/dom/Element;
    .end local v12    # "len$":I
    :cond_1
    :try_start_6
    sget-object v18, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 157
    .restart local v3    # "arr$":[Lorg/w3c/dom/Element;
    .restart local v11    # "items":[Lorg/w3c/dom/Element;
    :goto_4
    monitor-exit v19

    return-object v18

    .line 129
    .restart local v9    # "i$":I
    .restart local v10    # "item":Lorg/w3c/dom/Element;
    .restart local v12    # "len$":I
    .restart local v14    # "nPriority":I
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v17    # "priority":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 130
    .local v16, "ne":Ljava/lang/NumberFormatException;
    :try_start_7
    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 152
    .end local v9    # "i$":I
    .end local v10    # "item":Lorg/w3c/dom/Element;
    .end local v12    # "len$":I
    .end local v14    # "nPriority":I
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "ne":Ljava/lang/NumberFormatException;
    .end local v17    # "priority":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 153
    .local v7, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-static {v7}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 154
    const/16 v18, 0x0

    sput-object v18, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;

    .line 157
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_2
    sget-object v18, Lcom/sec/factory/aporiented/ParallelCommandsParser$CommandAttr;->table:Ljava/util/ArrayList;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    .line 135
    .restart local v6    # "dependencies":Ljava/lang/String;
    .restart local v9    # "i$":I
    .restart local v10    # "item":Lorg/w3c/dom/Element;
    .restart local v12    # "len$":I
    .restart local v14    # "nPriority":I
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v17    # "priority":Ljava/lang/String;
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 141
    .restart local v5    # "depArray":[Ljava/lang/String;
    .restart local v8    # "gap":Ljava/lang/String;
    .restart local v13    # "nGap":I
    :catch_2
    move-exception v16

    .line 142
    .restart local v16    # "ne":Ljava/lang/NumberFormatException;
    :try_start_9
    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 109
    .end local v5    # "depArray":[Ljava/lang/String;
    .end local v6    # "dependencies":Ljava/lang/String;
    .end local v8    # "gap":Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "item":Lorg/w3c/dom/Element;
    .end local v12    # "len$":I
    .end local v13    # "nGap":I
    .end local v14    # "nPriority":I
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "ne":Ljava/lang/NumberFormatException;
    .end local v17    # "priority":Ljava/lang/String;
    :catchall_0
    move-exception v18

    monitor-exit v19

    throw v18
.end method
