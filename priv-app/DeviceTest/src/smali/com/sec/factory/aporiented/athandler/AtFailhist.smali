.class public Lcom/sec/factory/aporiented/athandler/AtFailhist;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFailhist.java"


# static fields
.field static final FACTORY_CMD_PROPERTY:Ljava/lang/String; = "ril.factory_cmd"

.field private static final FAILHIST_ACTION:Ljava/lang/String; = "com.android.sec.FAILHIST"

.field private static final FAILHIST_DONE:Ljava/lang/String; = "com.android.sec.FAILHIST.DONE"

.field private static final FTAT_KEY:Ljava/lang/String; = "FILENAME"

.field static final STAGE_PROPERTY:Ljava/lang/String; = "ril.factory_mode"


# instance fields
.field public mReceiver:Landroid/content/BroadcastReceiver;

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    .line 167
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFailhist$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFailhist;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 34
    const-string v0, "FAILHIST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CMD_NAME:Ljava/lang/String;

    .line 35
    const-string v0, "AtFailhist"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    .line 36
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->NUM_ARGS:I

    .line 37
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    .line 167
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFailhist$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFailhist;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 26
    const-string v0, "FAILHIST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CMD_NAME:Ljava/lang/String;

    .line 27
    const-string v0, "AtFailhist"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    .line 28
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->NUM_ARGS:I

    .line 29
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 30
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x3

    .line 42
    monitor-enter p0

    const/4 v3, 0x0

    .line 44
    .local v3, "resData":Ljava/lang/String;
    :try_start_0
    array-length v7, p1

    iget v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->NUM_ARGS:I

    if-ge v7, v8, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 157
    :goto_0
    monitor-exit p0

    return-object v7

    .line 47
    :cond_0
    :try_start_1
    const-string v7, "FAILHIST_VERSION"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Version;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    .line 49
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "0"

    aput-object v9, v7, v8

    invoke-virtual {p0, p1, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 52
    array-length v8, p1

    sget-boolean v7, Lcom/sec/factory/aporiented/athandler/AtFailhist;->STAGE_PARALLEL:Z

    if-eqz v7, :cond_1

    iget v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->NUM_ARGS:I

    add-int/lit8 v7, v7, 0x1

    :goto_1
    if-eq v8, v7, :cond_2

    .line 53
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNA()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 52
    :cond_1
    iget v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->NUM_ARGS:I

    goto :goto_1

    .line 56
    :cond_2
    const-string v7, "Unknown"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 57
    const-string v7, "FACTORYAPP_FAILHIST"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "res":Ljava/lang/String;
    if-eqz v2, :cond_3

    const-string v7, "Unknown"

    if-ne v2, v7, :cond_5

    .line 60
    :cond_3
    const/4 v7, 0x0

    aget-object v7, p1, v7

    const-string v8, "NONE"

    invoke-virtual {p0, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .end local v2    # "res":Ljava/lang/String;
    :cond_4
    :goto_2
    move-object v7, v3

    .line 157
    goto :goto_0

    .line 62
    .restart local v2    # "res":Ljava/lang/String;
    :cond_5
    const-string v7, "\""

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "replace_res":Ljava/lang/String;
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7, v1}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    goto :goto_2

    .line 65
    .end local v1    # "replace_res":Ljava/lang/String;
    .end local v2    # "res":Ljava/lang/String;
    :cond_6
    const-string v7, "V1"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 66
    const-string v7, "FACTORYAPP_FAILHIST"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .restart local v2    # "res":Ljava/lang/String;
    if-eqz v2, :cond_7

    const-string v7, "Unknown"

    if-eq v2, v7, :cond_7

    const-string v7, "NONE"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 69
    :cond_7
    const/4 v7, 0x0

    aget-object v7, p1, v7

    const-string v8, "NONE"

    invoke-virtual {p0, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 71
    :cond_8
    const-string v7, "\""

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    .restart local v1    # "replace_res":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "FACTORYAPP_FAILHIST_TIME"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7, v1}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 74
    goto :goto_2

    .line 75
    .end local v1    # "replace_res":Ljava/lang/String;
    .end local v2    # "res":Ljava/lang/String;
    :cond_9
    const-string v7, "V2"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 76
    const-string v7, "FACTORY_FAILHIST"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 77
    .restart local v2    # "res":Ljava/lang/String;
    if-eqz v2, :cond_a

    const-string v7, "NONE"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "N"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 78
    :cond_a
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "readFailHist"

    const-string v9, "res = none"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v7, 0x0

    aget-object v7, p1, v7

    const-string v8, "NONE"

    invoke-virtual {p0, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 81
    :cond_b
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7, v2}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 84
    .end local v2    # "res":Ljava/lang/String;
    :cond_c
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "1"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "0"

    aput-object v9, v7, v8

    invoke-virtual {p0, p1, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 85
    const-string v7, "Unknown"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 86
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNA()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 88
    :cond_d
    const/4 v7, 0x0

    aget-object v7, p1, v7

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 90
    :cond_e
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "2"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "0"

    aput-object v9, v7, v8

    invoke-virtual {p0, p1, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 91
    array-length v7, p1

    if-eq v7, v10, :cond_f

    .line 92
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNA()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 95
    :cond_f
    const-string v7, "Unknown"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 96
    const/4 v7, 0x2

    aget-object v7, p1, v7

    const-string v8, "\""

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    .restart local v1    # "replace_res":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "setProperties"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cmd = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v7, "FACTORYAPP_FAILHIST"

    invoke-static {v7, v1}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 101
    goto/16 :goto_2

    .end local v1    # "replace_res":Ljava/lang/String;
    :cond_10
    const-string v7, "V1"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 102
    const/4 v7, 0x2

    aget-object v7, p1, v7

    const-string v8, "\""

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    .restart local v1    # "replace_res":Ljava/lang/String;
    const-string v7, "FACTORYAPP_FAILHIST"

    invoke-static {v7, v1}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v7, "FACTORYAPP_FAILHIST_TIME"

    sget-object v8, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v8}, Lcom/sec/factory/modules/ModuleCommon;->getTimeToString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 106
    goto/16 :goto_2

    .end local v1    # "replace_res":Ljava/lang/String;
    :cond_11
    const-string v7, "V2"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "mResultTemp":Ljava/lang/String;
    const/4 v7, 0x2

    aget-object v7, p1, v7

    const-string v8, "\""

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .restart local v1    # "replace_res":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "write failhist"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "failhist = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v8}, Lcom/sec/factory/modules/ModuleCommon;->getTimeToString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    const-string v7, "FLS_TYPE_EFS"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 113
    const-string v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 114
    .local v5, "splitStr":[Ljava/lang/String;
    const-string v7, "FACTORY_FAILHIST_FLS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    aget-object v9, v5, v9

    const-string v10, "\\p{space}"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v5, v9

    const-string v10, "\\p{space}"

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 116
    .local v4, "result":Z
    if-nez v4, :cond_12

    .line 117
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, " write failhist for FLS"

    const-string v9, "result = fail"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .end local v4    # "result":Z
    :goto_3
    const-string v7, "FACTORY_FAILHIST"

    invoke-static {v7, v0}, Lcom/sec/factory/support/Support$Kernel;->writeNsync(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 127
    .restart local v4    # "result":Z
    if-nez v4, :cond_14

    .line 128
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, " write failhist"

    const-string v9, "result = fail"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNA()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 119
    :cond_12
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "write failhist for FLS"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "result = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 42
    .end local v0    # "mResultTemp":Ljava/lang/String;
    .end local v1    # "replace_res":Ljava/lang/String;
    .end local v4    # "result":Z
    .end local v5    # "splitStr":[Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 122
    .restart local v0    # "mResultTemp":Ljava/lang/String;
    .restart local v1    # "replace_res":Ljava/lang/String;
    :cond_13
    :try_start_2
    const-string v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 123
    .restart local v5    # "splitStr":[Ljava/lang/String;
    const-string v7, "FACTORYAPP_FAILHIST_FLS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 131
    .restart local v4    # "result":Z
    :cond_14
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "write failhist"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "result = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 135
    .end local v0    # "mResultTemp":Ljava/lang/String;
    .end local v1    # "replace_res":Ljava/lang/String;
    .end local v4    # "result":Z
    .end local v5    # "splitStr":[Ljava/lang/String;
    :cond_15
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "0"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "0"

    aput-object v9, v7, v8

    invoke-virtual {p0, p1, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_19

    .line 136
    const-string v7, "V1"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 137
    const-string v7, "FACTORYAPP_FAILHIST"

    const-string v8, "NONE"

    invoke-static {v7, v8}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v7, "FACTORYAPP_FAILHIST_TIME"

    const-string v8, "NONE"

    invoke-static {v7, v8}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "clear failhist"

    const-string v9, "failhist = null"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 141
    :cond_16
    const-string v7, "V2"

    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 142
    new-instance v6, Ljava/io/File;

    const-string v7, "FACTORY_FAILHIST"

    invoke-static {v7}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 144
    .local v6, "ssfh":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_18

    .line 145
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "clear failhist"

    const-string v9, "file exits"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v7

    if-eqz v7, :cond_17

    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 147
    :cond_17
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 149
    :cond_18
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "clear failhist"

    const-string v9, "file not exits"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const/4 v7, 0x0

    aget-object v7, p1, v7

    invoke-virtual {p0, v7}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 154
    .end local v6    # "ssfh":Ljava/io/File;
    :cond_19
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFailhist;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    goto/16 :goto_2
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 162
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 163
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.sec.FAILHIST.DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFailhist;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 165
    return-void
.end method
