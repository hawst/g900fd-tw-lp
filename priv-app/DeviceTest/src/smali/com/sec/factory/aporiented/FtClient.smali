.class public Lcom/sec/factory/aporiented/FtClient;
.super Landroid/app/Service;
.source "FtClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/FtClient$ConnectionThread;
    }
.end annotation


# static fields
.field public static final ACTION_SEND_TO_RIL:Ljava/lang/String; = "com.sec.factory.SEND_TO_RIL"

.field public static final CLASS_NAME:Ljava/lang/String; = "FtClient"

.field public static final CSC_MODEM_SETTING:Ljava/lang/String; = "android.intent.action.CSC_MODEM_SETTING"

.field public static final CSC_MODEM_SETTING_FACTORY:Ljava/lang/String; = "android.intent.action.CSC_MODEM_SETTING_FACTORY"

.field private static mIsReceivedCscModemSetting:Z

.field private static mIsReceivedMediaScanningFinishedExternalMem:Z

.field public static mWl:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private final MAX_TIMEOUT:I

.field clientSocket_recv:Landroid/net/LocalSocket;

.field clientSocket_send:Landroid/net/LocalSocket;

.field private connectionThread:Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

.field public diCommandManager:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCPverWaitCount:I

.field private mContext:Landroid/content/Context;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mIsFirstCmd:Z

.field public parser:Lcom/sec/factory/aporiented/AtParser;

.field public writer:Lcom/sec/factory/aporiented/ResponseWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    sput-boolean v0, Lcom/sec/factory/aporiented/FtClient;->mIsReceivedCscModemSetting:Z

    .line 63
    sput-boolean v0, Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x50

    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FtClient;->mIsFirstCmd:Z

    .line 58
    iput v1, p0, Lcom/sec/factory/aporiented/FtClient;->MAX_TIMEOUT:I

    .line 59
    iput v1, p0, Lcom/sec/factory/aporiented/FtClient;->mCount:I

    .line 60
    const/16 v0, 0x28

    iput v0, p0, Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I

    .line 158
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;

    .line 160
    new-instance v0, Lcom/sec/factory/aporiented/FtClient$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/FtClient$1;-><init>(Lcom/sec/factory/aporiented/FtClient;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/FtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 340
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/FtClient;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/factory/aporiented/FtClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/FtClient;->mIsFirstCmd:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/factory/aporiented/FtClient;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/factory/aporiented/FtClient;->mIsFirstCmd:Z

    return p1
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 41
    sput-boolean p0, Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->setForeGroundService()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->setPendingIntent()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/factory/aporiented/FtClient;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/factory/aporiented/FtClient;)Lcom/sec/factory/aporiented/FtClient$ConnectionThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient;->connectionThread:Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->sendBootCompleted()V

    return-void
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/sec/factory/aporiented/FtClient;->mIsReceivedCscModemSetting:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 41
    sput-boolean p0, Lcom/sec/factory/aporiented/FtClient;->mIsReceivedCscModemSetting:Z

    return p0
.end method

.method static synthetic access$400(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->sendResetCompleted()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->writeSOC()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/factory/aporiented/FtClient;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I

    return v0
.end method

.method static synthetic access$610(Lcom/sec/factory/aporiented/FtClient;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/factory/aporiented/FtClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->checkPhoneVer()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/factory/aporiented/FtClient;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/factory/aporiented/FtClient;->checkPhone2Ver()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/factory/aporiented/FtClient;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/factory/aporiented/FtClient;->mCount:I

    return v0
.end method

.method static synthetic access$910(Lcom/sec/factory/aporiented/FtClient;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/factory/aporiented/FtClient;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/factory/aporiented/FtClient;->mCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/factory/aporiented/FtClient;->mCount:I

    return v0
.end method

.method private checkPhone2Ver()Z
    .locals 6

    .prologue
    .line 514
    const/4 v0, 0x0

    .line 515
    .local v0, "cp2_status":Z
    const-string v2, "ril.sw_ver2"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 516
    .local v1, "cp2_ver":Ljava/lang/String;
    const-string v2, "NONE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 517
    const/4 v0, 0x0

    .line 522
    :goto_0
    const-string v2, "FtClient"

    const-string v3, "checkPhone2Ver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cp2_status="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    return v0

    .line 519
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private checkPhoneVer()Z
    .locals 6

    .prologue
    .line 501
    const/4 v0, 0x0

    .line 502
    .local v0, "cp1_status":Z
    const-string v2, "ril.sw_ver"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 503
    .local v1, "cp1_ver":Ljava/lang/String;
    const-string v2, "NONE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 504
    const/4 v0, 0x0

    .line 509
    :goto_0
    const-string v2, "FtClient"

    const-string v3, "checkPhoneVer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cp1_status="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    return v0

    .line 506
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private sendBootCompleted()V
    .locals 5

    .prologue
    .line 249
    invoke-static {p0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v2

    if-nez v2, :cond_3

    .line 250
    const/4 v0, 0x5

    .line 253
    .local v0, "counter":I
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 255
    :cond_1
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_0
    add-int/lit8 v0, v0, -0x1

    .line 263
    if-gez v0, :cond_0

    .line 264
    const-string v2, "FtClient"

    const-string v3, "mBroadcastReceiver"

    const-string v4, "Can not send BOOTING COMPLETED!!"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    .end local v0    # "counter":I
    :goto_1
    return-void

    .line 256
    .restart local v0    # "counter":I
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 270
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v2, p0, Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/factory/aporiented/FtClient$2;

    invoke-direct {v3, p0}, Lcom/sec/factory/aporiented/FtClient$2;-><init>(Lcom/sec/factory/aporiented/FtClient;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 323
    .end local v0    # "counter":I
    :cond_3
    iget-object v2, p0, Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/factory/aporiented/FtClient$3;

    invoke-direct {v3, p0}, Lcom/sec/factory/aporiented/FtClient$3;-><init>(Lcom/sec/factory/aporiented/FtClient;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method private sendResetCompleted()V
    .locals 6

    .prologue
    .line 528
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/sec/factory/aporiented/FtClient$4;

    invoke-direct {v4, p0}, Lcom/sec/factory/aporiented/FtClient$4;-><init>(Lcom/sec/factory/aporiented/FtClient;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 542
    const-string v1, "Reset Completed"

    .line 543
    .local v1, "NOTI_BAR_TITLE_RESET":Ljava/lang/String;
    const-string v0, "Factory Reset is Completed"

    .line 544
    .local v0, "NOTI_BAR_BODY_RESET":Ljava/lang/String;
    const-string v3, "ro.csc.sales_code"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 545
    .local v2, "SALES_CODE":Ljava/lang/String;
    const-string v3, "ATT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 546
    const-string v3, "FtClient"

    const-string v4, "sendResetCompleteds"

    const-string v5, "Display NotiBar with [Reset Completed]"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v3, "Reset Completed"

    const-string v4, "Factory Reset is Completed"

    invoke-static {p0, v3, v4}, Lcom/sec/factory/entry/DisplayNotiBar;->createNotification(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    :cond_0
    return-void
.end method

.method private setForeGroundService()V
    .locals 6

    .prologue
    .line 477
    const-string v2, "FtClient"

    const-string v3, "setForeGroundService"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 481
    .local v0, "am":Landroid/app/IActivityManager;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/sec/factory/aporiented/FtClient;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x1

    invoke-interface {v0, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :goto_0
    return-void

    .line 482
    :catch_0
    move-exception v1

    .line 483
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "FtClient"

    const-string v3, "setForeGroundService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cant set to foreground"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setPendingIntent()V
    .locals 11

    .prologue
    const/16 v10, 0x15b3

    const/4 v7, 0x0

    .line 488
    const-string v4, "FtClient"

    const-string v5, "setPendingIntent"

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    iget-object v4, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "factory"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v7, v5, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 490
    .local v3, "pI":Landroid/app/PendingIntent;
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 491
    .local v0, "NotiBuilder":Landroid/app/Notification$Builder;
    iget-object v4, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    const-string v5, "notification"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 493
    .local v1, "mgr":Landroid/app/NotificationManager;
    const v4, 0x7f020005

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v4

    const-string v5, "FT Service"

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    const-string v5, "on Factory command"

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    const-string v5, "on Factory mode"

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 496
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    .line 497
    .local v2, "noti":Landroid/app/Notification;
    invoke-virtual {v1, v10, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 498
    invoke-virtual {p0, v10, v2}, Lcom/sec/factory/aporiented/FtClient;->startForeground(ILandroid/app/Notification;)V

    .line 499
    return-void
.end method

.method private writeSOC()V
    .locals 5

    .prologue
    .line 231
    const/4 v0, 0x0

    .line 232
    .local v0, "isresetneeded":Z
    const-string v3, "AT_BATTEST_RESET_WHEN_READ"

    invoke-static {v3}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 234
    if-eqz v0, :cond_1

    .line 235
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModulePower;->resetFuelGaugeIC()Z

    .line 240
    :goto_0
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModulePower;->readBatteryVoltage()Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "result":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v3, "efs/FactoryApp/SOC_Data"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 243
    .local v2, "socDataFile":Ljava/io/File;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/factory/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 246
    :cond_0
    return-void

    .line 237
    .end local v1    # "result":Ljava/lang/String;
    .end local v2    # "socDataFile":Ljava/io/File;
    :cond_1
    const-string v3, "BATTERY_UPDATE_BEFORE_READ"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/FtClient;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    .line 79
    const-string v3, "FtClient"

    const-string v4, "onCreate"

    const-string v5, "Create FtClient service"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/FtClient;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 82
    const-string v3, "FtClient"

    const-string v4, "onCreate"

    const-string v5, "FtClient => XML data parsing was failed."

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    const-string v3, "SUPPORT_BOOST_MEDIASCAN"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->connectedJIG()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 86
    const-string v3, "FtClient"

    const-string v4, "onCreate"

    const-string v5, "SUPPORT_BOOST_MEDIASCAN"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-static {p0}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModulePower;->doMediaScanWakeLock(Z)V

    .line 90
    :cond_1
    new-instance v3, Landroid/net/LocalSocket;

    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    .line 91
    new-instance v3, Landroid/net/LocalSocket;

    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    iput-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    .line 92
    new-instance v3, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

    invoke-direct {v3, p0, p0}, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;-><init>(Lcom/sec/factory/aporiented/FtClient;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->connectionThread:Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

    .line 93
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->connectionThread:Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

    invoke-virtual {v3}, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->start()V

    .line 95
    const-string v3, "FtClient"

    const-string v4, "onCreate"

    const-string v5, "registBroadCastReceiver"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 98
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.CSC_MODEM_SETTING"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    const-string v3, "com.sec.factory.SEND_TO_RIL"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    const-string v3, "android.intent.action.CSC_MODEM_SETTING_FACTORY"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    const-string v3, "factory"

    const-string v4, "BINARY_TYPE"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "INBATT_SAVE_SOC"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 103
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 105
    :cond_2
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/sec/factory/aporiented/FtClient;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 108
    .local v1, "intentFilterMediaScanning":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 109
    const-string v3, "file"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 110
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/factory/aporiented/FtClient;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 112
    sget-object v3, Lcom/sec/factory/aporiented/FtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    if-nez v3, :cond_3

    .line 113
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 114
    .local v2, "pm":Landroid/os/PowerManager;
    const/16 v3, 0x1a

    const-string v4, "by FtClient"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    sput-object v3, Lcom/sec/factory/aporiented/FtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 117
    .end local v2    # "pm":Landroid/os/PowerManager;
    :cond_3
    sget-object v3, Lcom/sec/factory/aporiented/FtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "SUPPORT_AUTO_WAKELOCK"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 119
    sget-object v3, Lcom/sec/factory/aporiented/FtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 120
    const-string v3, "FtClient"

    const-string v4, "onCreate"

    const-string v5, "acquire wakelock from DummyFtClient"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_4
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 127
    const-string v1, "FtClient"

    const-string v2, "onDestroy"

    const-string v3, "Destroy FtClient service"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->parser:Lcom/sec/factory/aporiented/AtParser;

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->parser:Lcom/sec/factory/aporiented/AtParser;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/AtParser;->terminateThreads()Z

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->connectionThread:Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->kill()V

    .line 132
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/FtClient;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 134
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    if-eqz v1, :cond_1

    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V

    .line 137
    const-string v1, "FtClient"

    const-string v2, "onDestroy"

    const-string v3, "Close client socket(receiver)"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    if-eqz v1, :cond_2

    .line 145
    :try_start_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V

    .line 146
    const-string v1, "FtClient"

    const-string v2, "onDestroy"

    const-string v3, "Close client socket(sender)"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 152
    :cond_2
    :goto_1
    const-string v1, "SUPPORT_BOOST_MEDIASCAN"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 153
    const-string v1, "FtClient"

    const-string v2, "onDestroy"

    const-string v3, "SUPPORT_BOOST_MEDIASCAN"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-static {p0}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModulePower;->doMediaScanWakeLock(Z)V

    .line 156
    :cond_3
    return-void

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 147
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 148
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 72
    const-string v0, "FtClient"

    const-string v1, "onStartCommand"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const/4 v0, 0x1

    return v0
.end method
