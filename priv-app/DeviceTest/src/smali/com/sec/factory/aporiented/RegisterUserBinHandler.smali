.class public Lcom/sec/factory/aporiented/RegisterUserBinHandler;
.super Ljava/lang/Object;
.source "RegisterUserBinHandler.java"


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "RegisterUserBinHandler"

.field private static final IS_PRODUCT_SHIP:Z


# instance fields
.field private mAtHandlers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/aporiented/athandler/AtCommandHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCountryCode:Ljava/lang/String;

.field mWriter:Lcom/sec/factory/aporiented/ResponseWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "ro.product_ship"

    const-string v1, "FALSE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TRUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->IS_PRODUCT_SHIP:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/HashMap;Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/aporiented/athandler/AtCommandHandler;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/sec/factory/aporiented/ResponseWriter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "atHandlers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/factory/aporiented/athandler/AtCommandHandler;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v0, "ro.csc.country_code"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mCountryCode:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    .line 73
    iput-object p1, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    .line 74
    iput-object p3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 75
    return-void
.end method


# virtual methods
.method public registerHandler()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 78
    const-string v0, "RegisterUserBinHandler"

    const-string v1, "registerHandler"

    const-string v2, "Register AT command handler for USER BIN"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+KEY"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtKey;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtKey;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+KEYHOLD"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtKeyhold;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtKeyhold;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+TOUCH"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtTouch;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtTouch;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+MICSD"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtMicsd;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtMicsd;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+IMEMTEST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtImemtest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtImemtest;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+BATTTEST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtBatttest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtBatttest;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+BTIDTEST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtBtidtest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+WIFIIDRW"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FUELGAIC"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+SERIALNO"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtSerialno;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtSerialno;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+SPKSTEST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtSpkstest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+EMEMTEST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtEmemtest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtEmemtest;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+HWINDICK"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtHwindick;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtHwindick;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+HMACMISM"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtHmacmism;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtHmacmism;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+VERSNAME"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtVersname;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtVersname;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FACTOLOG"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFactolog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+UARTSWIT"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtUartswit;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtUartswit;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v0, "KOREA"

    iget-object v1, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+LOCKCODE"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtLockCode;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtLockCode;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+PRECONFG"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtPreconfg;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_1
    const-string v0, "SUPPORT_MARVELL_RIL"

    invoke-static {v0}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v0

    if-ne v0, v5, :cond_3

    const-string v0, "AT_CALIDATE"

    invoke-static {v0}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+CALIDATE"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtCalidate;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtCalidate;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FACTORST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFactorst;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFactorst;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FAILDUMP"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFaildump;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtFaildump;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FAILHIST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFailhist;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtFailhist;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+RAMSIZEC"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtRamsizec;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtRamsizec;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+SECUREBT"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtSecurebt;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+WPROTECT"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtWprotect;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtWprotect;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+POWRESET"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtPowreset;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtPowreset;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+WIFITEST"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtWifitest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtWifitest;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+PAYMENTS"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtPayments;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FIRMVERS"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+EWRITECK"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+SYSSCOPE"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtSysscope;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtSysscope;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FLCRFCAL"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+FPSENSOR"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+BAROMETE"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtBaromete;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtBaromete;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+SWDLMODE"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtSwdlmode;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtSwdlmode;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-boolean v0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->IS_PRODUCT_SHIP:Z

    if-nez v0, :cond_4

    .line 142
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+SMS"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtSms;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtSms;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mAtHandlers:Ljava/util/HashMap;

    const-string v1, "AT+KSTRINGB"

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtKstringb;

    iget-object v3, p0, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    return-void
.end method
