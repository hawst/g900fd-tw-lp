.class public Lcom/sec/factory/aporiented/athandler/AtUartswit;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtUartswit.java"


# static fields
.field private static final ACTION_SET_PATH:Ljava/lang/String; = "com.sec.factory.aporiented.athandler.AtUartswit.SetUartPath"

.field private static final EXTRA_KEY:Ljava/lang/String; = "PATH"


# instance fields
.field public mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtUartswit$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtUartswit$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtUartswit;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 20
    const-string v0, "UARTSWIT"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CMD_NAME:Ljava/lang/String;

    .line 21
    const-string v0, "AtUartswit"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    .line 22
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->NUM_ARGS:I

    .line 23
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 24
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->startReceiver()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtUartswit$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtUartswit$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtUartswit;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 29
    const-string v0, "UARTSWIT"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CMD_NAME:Ljava/lang/String;

    .line 30
    const-string v0, "AtUartswit"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    .line 31
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->NUM_ARGS:I

    .line 32
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 33
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 58
    monitor-enter p0

    const/4 v1, 0x0

    .line 59
    .local v1, "resData":Ljava/lang/String;
    const/4 v2, 0x0

    .line 61
    .local v2, "result":Z
    :try_start_0
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtUartswit;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 62
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 131
    :goto_1
    monitor-exit p0

    return-object v3

    .line 61
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->NUM_ARGS:I

    goto :goto_0

    .line 65
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 66
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    const-string v5, "MODEM"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "CP"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->setUartPath(Ljava/lang/String;)Z

    move-result v2

    .line 69
    if-eqz v2, :cond_2

    .line 70
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    move-object v3, v1

    .line 131
    goto :goto_1

    .line 72
    :cond_2
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 75
    :cond_3
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 76
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    const-string v5, "PDA"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "AP"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->setUartPath(Ljava/lang/String;)Z

    move-result v2

    .line 79
    if-eqz v2, :cond_4

    .line 80
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 82
    :cond_4
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 85
    :cond_5
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "2"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 86
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    const-string v5, "LTE"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "LTE"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->setUartPath(Ljava/lang/String;)Z

    move-result v2

    .line 89
    if-eqz v2, :cond_6

    .line 90
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 92
    :cond_6
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 95
    :cond_7
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "3"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 96
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    const-string v5, "CP2"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "CP2"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->setUartPath(Ljava/lang/String;)Z

    move-result v2

    .line 99
    if-eqz v2, :cond_8

    .line 100
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 102
    :cond_8
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 105
    :cond_9
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 106
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    const-string v5, "Read UART"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->getUartPath()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "currentUart":Ljava/lang/String;
    if-eqz v0, :cond_e

    .line 110
    const-string v3, "CP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 111
    const-string v0, "UART1"

    .line 125
    :goto_3
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3, v0}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    goto/16 :goto_2

    .line 112
    :cond_a
    const-string v3, "AP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 113
    const-string v0, "UART2"

    goto :goto_3

    .line 114
    :cond_b
    const-string v3, "LTE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 115
    const-string v0, "UART3"

    goto :goto_3

    .line 116
    :cond_c
    const-string v3, "CP2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 117
    const-string v0, "UART4"

    goto :goto_3

    .line 119
    :cond_d
    const-string v0, "NG"

    goto :goto_3

    .line 122
    :cond_e
    const-string v0, "NG"

    goto :goto_3

    .line 128
    .end local v0    # "currentUart":Ljava/lang/String;
    :cond_f
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtUartswit;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto/16 :goto_2

    .line 58
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 38
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.factory.aporiented.athandler.AtUartswit.SetUartPath"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 39
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtUartswit;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 40
    return-void
.end method
