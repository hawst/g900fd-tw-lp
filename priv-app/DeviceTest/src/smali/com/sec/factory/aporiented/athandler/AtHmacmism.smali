.class public Lcom/sec/factory/aporiented/athandler/AtHmacmism;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtHmacmism.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 13
    const-string v0, "HMACMISM"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->CMD_NAME:Ljava/lang/String;

    .line 14
    const-string v0, "AtHmacmism"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->CLASS_NAME:Ljava/lang/String;

    .line 15
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->NUM_ARGS:I

    .line 16
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 21
    monitor-enter p0

    const/4 v0, 0x0

    .line 23
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v3, p1

    sget-boolean v2, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->STAGE_PARALLEL:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->NUM_ARGS:I

    add-int/lit8 v2, v2, 0x1

    :goto_0
    if-eq v3, v2, :cond_1

    .line 24
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 42
    :goto_1
    monitor-exit p0

    return-object v2

    .line 23
    :cond_0
    :try_start_1
    iget v2, p0, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->NUM_ARGS:I

    goto :goto_0

    .line 27
    :cond_1
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "0"

    aput-object v4, v2, v3

    invoke-virtual {p0, p1, v2}, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 30
    const-string v2, "HMAC_RESULT"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->readAllLine(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "result":Ljava/lang/String;
    const-string v2, "mac mismatch"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 33
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .end local v1    # "result":Ljava/lang/String;
    :goto_2
    move-object v2, v0

    .line 42
    goto :goto_1

    .line 35
    .restart local v1    # "result":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 39
    .end local v1    # "result":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtHmacmism;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 21
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
