.class public Lcom/sec/factory/aporiented/DummyFtClient;
.super Landroid/app/Service;
.source "DummyFtClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "DummyFtClient"

.field public static final FINISH:I = 0x66

.field public static final SEND_BOOT_COMPLETED:I = 0x3ea

.field public static mWl:Landroid/os/PowerManager$WakeLock;


# instance fields
.field clientSocket_recv:Landroid/net/LocalSocket;

.field clientSocket_send:Landroid/net/LocalSocket;

.field private connectionThread:Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field public mCounter:I

.field private mHandler:Landroid/os/Handler;

.field public writer:Lcom/sec/factory/aporiented/ResponseWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    .line 39
    new-instance v0, Landroid/net/LocalSocket;

    invoke-direct {v0}, Landroid/net/LocalSocket;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mCounter:I

    .line 128
    new-instance v0, Lcom/sec/factory/aporiented/DummyFtClient$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/DummyFtClient$1;-><init>(Lcom/sec/factory/aporiented/DummyFtClient;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mHandler:Landroid/os/Handler;

    .line 184
    new-instance v0, Lcom/sec/factory/aporiented/DummyFtClient$4;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/DummyFtClient$4;-><init>(Lcom/sec/factory/aporiented/DummyFtClient;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 202
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/DummyFtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/DummyFtClient;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/factory/aporiented/DummyFtClient;->stopService()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/factory/aporiented/DummyFtClient;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/DummyFtClient;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/factory/aporiented/DummyFtClient;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/DummyFtClient;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/factory/aporiented/DummyFtClient;->sendBootCompletedAndFinish()V

    return-void
.end method

.method private sendBootCompletedAndFinish()V
    .locals 3

    .prologue
    .line 144
    const-string v0, "DummyFtClient"

    const-string v1, "sendBootCompletedAndFinish"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-static {p0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;-><init>(Landroid/content/Context;)V

    .line 173
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/factory/aporiented/DummyFtClient$3;

    invoke-direct {v1, p0}, Lcom/sec/factory/aporiented/DummyFtClient$3;-><init>(Lcom/sec/factory/aporiented/DummyFtClient;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 178
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/factory/aporiented/DummyFtClient$2;

    invoke-direct {v1, p0}, Lcom/sec/factory/aporiented/DummyFtClient$2;-><init>(Lcom/sec/factory/aporiented/DummyFtClient;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private stopService()V
    .locals 0

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/DummyFtClient;->stopSelf()V

    .line 182
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 56
    const-string v2, "DummyFtClient"

    const-string v3, "onCreate"

    const-string v4, "Create DummyFtClient service"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/sec/factory/support/XMLDataStorage;->instance()Lcom/sec/factory/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/factory/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    const-string v2, "DummyFtClient"

    const-string v3, "onReceive"

    const-string v4, "ACTION_SIM_STATE_CHANGED => XML data parsing was failed."

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 64
    .local v0, "intentFilterMediaScanning":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 65
    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 66
    iget-object v2, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/factory/aporiented/DummyFtClient;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    invoke-static {p0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    new-instance v2, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;

    invoke-direct {v2, p0, p0}, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;-><init>(Lcom/sec/factory/aporiented/DummyFtClient;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/factory/aporiented/DummyFtClient;->connectionThread:Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;

    .line 70
    iget-object v2, p0, Lcom/sec/factory/aporiented/DummyFtClient;->connectionThread:Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->start()V

    .line 74
    :cond_1
    sget-object v2, Lcom/sec/factory/aporiented/DummyFtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    if-nez v2, :cond_2

    .line 75
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/factory/aporiented/DummyFtClient;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 76
    .local v1, "pm":Landroid/os/PowerManager;
    const/16 v2, 0x1a

    const-string v3, "by DummyFtClient"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    sput-object v2, Lcom/sec/factory/aporiented/DummyFtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 79
    .end local v1    # "pm":Landroid/os/PowerManager;
    :cond_2
    sget-object v2, Lcom/sec/factory/aporiented/DummyFtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "SUPPORT_AUTO_WAKELOCK"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 81
    sget-object v2, Lcom/sec/factory/aporiented/DummyFtClient;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 82
    const-string v2, "DummyFtClient"

    const-string v3, "onCreate"

    const-string v4, "acquire wakelock from DummyFtClient"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_3
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 95
    const-string v1, "DummyFtClient"

    const-string v2, "onDestroy"

    const-string v3, "Destroy DummyFtClient service"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/sec/factory/aporiented/DummyFtClient;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/DummyFtClient;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 98
    invoke-static {p0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/sec/factory/aporiented/DummyFtClient;->connectionThread:Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/DummyFtClient$ConnectionThread;->kill()V

    .line 101
    iget-object v1, p0, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    if-eqz v1, :cond_0

    .line 103
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V

    .line 104
    const-string v1, "DummyFtClient"

    const-string v2, "onDestroy"

    const-string v3, "Close client socket(receiver)"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    if-eqz v1, :cond_1

    .line 112
    :try_start_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/DummyFtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V

    .line 113
    const-string v1, "DummyFtClient"

    const-string v2, "onDestroy"

    const-string v3, "Close client socket(sender)"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 120
    :cond_1
    :goto_1
    invoke-static {p0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommon;->isRunningFtClient()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    const-string v1, "DummyFtClient"

    const-string v2, "onDestroy"

    const-string v3, "now running FtClient"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_2
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 115
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 123
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    const-string v1, "DummyFtClient"

    const-string v2, "onDestroy"

    const-string v3, "kill process"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    goto :goto_2
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 88
    const-string v0, "DummyFtClient"

    const-string v1, "onStartCommand"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x2

    return v0
.end method
