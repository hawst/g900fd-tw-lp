.class public Lcom/sec/factory/aporiented/athandler/AtWprotect;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtWprotect.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 14
    const-string v0, "WPROTECT"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->CMD_NAME:Ljava/lang/String;

    .line 15
    const-string v0, "AtWprotect"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->CLASS_NAME:Ljava/lang/String;

    .line 16
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->NUM_ARGS:I

    .line 17
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 22
    monitor-enter p0

    const/4 v3, 0x0

    .line 24
    .local v3, "resData":Ljava/lang/String;
    :try_start_0
    array-length v5, p1

    sget-boolean v4, Lcom/sec/factory/aporiented/athandler/AtWprotect;->STAGE_PARALLEL:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->NUM_ARGS:I

    add-int/lit8 v4, v4, 0x1

    :goto_0
    if-eq v5, v4, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 63
    :goto_1
    monitor-exit p0

    return-object v4

    .line 24
    :cond_0
    :try_start_1
    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->NUM_ARGS:I

    goto :goto_0

    .line 28
    :cond_1
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 32
    const-string v4, "MMCBLK_DEVICE_CSD"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "mCsd":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    invoke-static {v4, v5, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    if-nez v1, :cond_2

    .line 36
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 37
    goto :goto_1

    .line 40
    :cond_2
    const/16 v4, 0x1c

    const/16 v5, 0x1d

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "mWprotect":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    invoke-static {v4, v5, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 43
    .local v0, "ch":I
    and-int/lit8 v0, v0, 0x3

    .line 44
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWprotect;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    packed-switch v0, :pswitch_data_0

    .end local v0    # "ch":I
    .end local v1    # "mCsd":Ljava/lang/String;
    .end local v2    # "mWprotect":Ljava/lang/String;
    :goto_2
    move-object v4, v3

    .line 63
    goto :goto_1

    .line 48
    .restart local v0    # "ch":I
    .restart local v1    # "mCsd":Ljava/lang/String;
    .restart local v2    # "mWprotect":Ljava/lang/String;
    :pswitch_0
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "NONE"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 49
    goto :goto_2

    .line 51
    :pswitch_1
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "TEMP"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 52
    goto :goto_2

    .line 55
    :pswitch_2
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "PERM"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 60
    .end local v0    # "ch":I
    .end local v1    # "mCsd":Ljava/lang/String;
    .end local v2    # "mWprotect":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWprotect;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    goto :goto_2

    .line 22
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
