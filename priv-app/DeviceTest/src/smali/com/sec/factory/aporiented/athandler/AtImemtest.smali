.class public Lcom/sec/factory/aporiented/athandler/AtImemtest;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtImemtest.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 15
    const-string v0, "IMEMTEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtImemtest;->CMD_NAME:Ljava/lang/String;

    .line 16
    const-string v0, "AtImemtest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtImemtest;->CLASS_NAME:Ljava/lang/String;

    .line 17
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtImemtest;->NUM_ARGS:I

    .line 18
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    const/high16 v13, 0x40800000    # 4.0f

    .line 23
    monitor-enter p0

    const/4 v2, 0x0

    .line 25
    .local v2, "resData":Ljava/lang/String;
    :try_start_0
    array-length v11, p1

    sget-boolean v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->STAGE_PARALLEL:Z

    if-eqz v10, :cond_0

    iget v10, p0, Lcom/sec/factory/aporiented/athandler/AtImemtest;->NUM_ARGS:I

    add-int/lit8 v10, v10, 0x1

    :goto_0
    if-eq v11, v10, :cond_1

    .line 26
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 109
    :goto_1
    monitor-exit p0

    return-object v10

    .line 25
    :cond_0
    :try_start_1
    iget v10, p0, Lcom/sec/factory/aporiented/athandler/AtImemtest;->NUM_ARGS:I

    goto :goto_0

    .line 29
    :cond_1
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "0"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "1"

    aput-object v12, v10, v11

    invoke-virtual {p0, p1, v10}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    :goto_2
    move-object v10, v2

    .line 109
    goto :goto_1

    .line 32
    :cond_2
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "1"

    aput-object v12, v10, v11

    invoke-virtual {p0, p1, v10}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 34
    sget-object v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v10}, Lcom/sec/factory/modules/ModuleDevice;->isInnerMemoryExist()Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v10, 0x0

    aget-object v10, p1, v10

    invoke-virtual {p0, v10}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    aget-object v10, p1, v10

    invoke-virtual {p0, v10}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 37
    :cond_4
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "2"

    aput-object v12, v10, v11

    invoke-virtual {p0, p1, v10}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 39
    const/4 v10, 0x0

    aget-object v10, p1, v10

    sget-object v11, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v11}, Lcom/sec/factory/modules/ModuleDevice;->getInnerMemorySize()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 41
    :cond_5
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "4"

    aput-object v12, v10, v11

    invoke-virtual {p0, p1, v10}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 58
    sget-object v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v11, 0x2

    const/16 v12, 0x400

    invoke-virtual {v10, v11, v12}, Lcom/sec/factory/modules/ModuleDevice;->getAvailableSize(II)J

    move-result-wide v4

    .line 60
    .local v4, "systemSize":J
    sget-object v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v11, 0x0

    const/16 v12, 0x400

    invoke-virtual {v10, v11, v12}, Lcom/sec/factory/modules/ModuleDevice;->getAvailableSize(II)J

    move-result-wide v0

    .line 62
    .local v0, "internalSize":J
    sget-object v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v11, 0x2

    const/16 v12, 0x400

    invoke-virtual {v10, v11, v12}, Lcom/sec/factory/modules/ModuleDevice;->getSize(II)J

    move-result-wide v8

    .line 64
    .local v8, "totalsystemSize":J
    const/4 v3, 0x0

    .line 66
    .local v3, "totalSize":F
    const-string v10, "NEED_IMEM_CHECK_DEVICESIZE"

    invoke-static {v10}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 67
    sget-object v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/16 v11, 0x400

    invoke-virtual {v10, v11}, Lcom/sec/factory/modules/ModuleDevice;->getInnerMemoryDevSize(I)J

    move-result-wide v6

    .line 68
    .local v6, "tempSize":J
    long-to-float v10, v6

    const/high16 v11, 0x49800000    # 1048576.0f

    div-float/2addr v10, v11

    float-to-long v10, v10

    long-to-float v3, v10

    .line 70
    cmpg-float v10, v3, v13

    if-gtz v10, :cond_6

    .line 71
    const/high16 v3, 0x40800000    # 4.0f

    .line 102
    .end local v6    # "tempSize":J
    :goto_4
    const/4 v10, 0x0

    aget-object v10, p1, v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v12, v3

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 104
    goto/16 :goto_2

    .line 72
    .restart local v6    # "tempSize":J
    :cond_6
    const/high16 v10, 0x41000000    # 8.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_7

    .line 73
    const/high16 v3, 0x41000000    # 8.0f

    goto :goto_4

    .line 74
    :cond_7
    const/high16 v10, 0x41800000    # 16.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_8

    .line 75
    const/high16 v3, 0x41800000    # 16.0f

    goto :goto_4

    .line 76
    :cond_8
    const/high16 v10, 0x42000000    # 32.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_9

    .line 77
    const/high16 v3, 0x42000000    # 32.0f

    goto :goto_4

    .line 78
    :cond_9
    const/high16 v10, 0x42800000    # 64.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_a

    .line 79
    const/high16 v3, 0x42800000    # 64.0f

    goto :goto_4

    .line 81
    :cond_a
    const/high16 v3, 0x43000000    # 128.0f

    goto :goto_4

    .line 84
    .end local v6    # "tempSize":J
    :cond_b
    sget-object v10, Lcom/sec/factory/aporiented/athandler/AtImemtest;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v11, 0x0

    const/16 v12, 0x400

    invoke-virtual {v10, v11, v12}, Lcom/sec/factory/modules/ModuleDevice;->getSize(II)J

    move-result-wide v10

    long-to-float v6, v10

    .line 86
    .local v6, "tempSize":F
    long-to-float v10, v8

    add-float/2addr v6, v10

    .line 87
    const/high16 v10, 0x49800000    # 1048576.0f

    div-float v3, v6, v10

    .line 89
    cmpg-float v10, v3, v13

    if-gtz v10, :cond_c

    .line 90
    const/high16 v3, 0x40800000    # 4.0f

    goto :goto_4

    .line 91
    :cond_c
    const/high16 v10, 0x41000000    # 8.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_d

    .line 92
    const/high16 v3, 0x41000000    # 8.0f

    goto :goto_4

    .line 93
    :cond_d
    const/high16 v10, 0x41800000    # 16.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_e

    .line 94
    const/high16 v3, 0x41800000    # 16.0f

    goto :goto_4

    .line 95
    :cond_e
    const/high16 v10, 0x42000000    # 32.0f

    cmpg-float v10, v3, v10

    if-gtz v10, :cond_f

    .line 96
    const/high16 v3, 0x42000000    # 32.0f

    goto/16 :goto_4

    .line 98
    :cond_f
    const/high16 v3, 0x42800000    # 64.0f

    goto/16 :goto_4

    .line 106
    .end local v0    # "internalSize":J
    .end local v3    # "totalSize":F
    .end local v4    # "systemSize":J
    .end local v6    # "tempSize":F
    .end local v8    # "totalsystemSize":J
    :cond_10
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtImemtest;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto/16 :goto_2

    .line 23
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10
.end method
