.class public Lcom/sec/factory/aporiented/athandler/AtBatttest;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtBatttest.java"


# instance fields
.field public mBatteryHealth:I

.field public mBatteryStatus:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, -0x1

    .line 13
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 14
    const-string v0, "BATTTEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CMD_NAME:Ljava/lang/String;

    .line 15
    const-string v0, "AtBatttest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    .line 16
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->NUM_ARGS:I

    .line 17
    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->mBatteryStatus:I

    .line 18
    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->mBatteryHealth:I

    .line 19
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 24
    monitor-enter p0

    const/4 v1, 0x0

    .line 26
    .local v1, "resData":Ljava/lang/String;
    :try_start_0
    array-length v5, p1

    sget-boolean v4, Lcom/sec/factory/aporiented/athandler/AtBatttest;->STAGE_PARALLEL:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->NUM_ARGS:I

    add-int/lit8 v4, v4, 0x1

    :goto_0
    if-eq v5, v4, :cond_1

    .line 27
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 138
    :goto_1
    monitor-exit p0

    return-object v4

    .line 26
    :cond_0
    :try_start_1
    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->NUM_ARGS:I

    goto :goto_0

    .line 30
    :cond_1
    const-string v4, "factory"

    const-string v5, "BINARY_TYPE"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 31
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "1"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 33
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNA()Ljava/lang/String;

    move-result-object v1

    .line 119
    :cond_2
    :goto_2
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "7"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 120
    const-string v4, "INBATT_SAVE_SOC"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 121
    const-string v4, "efs/FactoryApp/SOC_Data"

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "result":Ljava/lang/String;
    if-eqz v2, :cond_11

    .line 124
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4, v2}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    .end local v2    # "result":Ljava/lang/String;
    :cond_3
    :goto_3
    if-nez v1, :cond_4

    .line 135
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNA()Ljava/lang/String;

    move-result-object v1

    :cond_4
    move-object v4, v1

    .line 138
    goto :goto_1

    .line 45
    :cond_5
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "2"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "isresetneeded":Z
    const-string v4, "AT_BATTEST_RESET_WHEN_READ"

    invoke-static {v4}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 49
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isresetneeded: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    if-eqz v0, :cond_6

    .line 52
    sget-object v4, Lcom/sec/factory/aporiented/athandler/AtBatttest;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    invoke-virtual {v4}, Lcom/sec/factory/modules/ModulePower;->resetFuelGaugeIC()Z

    .line 57
    :goto_4
    const/4 v4, 0x0

    aget-object v4, p1, v4

    sget-object v5, Lcom/sec/factory/aporiented/athandler/AtBatttest;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    invoke-virtual {v5}, Lcom/sec/factory/modules/ModulePower;->readBatteryVoltage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    goto :goto_2

    .line 54
    :cond_6
    const-string v4, "BATTERY_UPDATE_BEFORE_READ"

    const-string v5, "1"

    invoke-static {v4, v5}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 24
    .end local v0    # "isresetneeded":Z
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 67
    :cond_7
    const/4 v4, 0x2

    :try_start_2
    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "4"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 68
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "read capacity"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "BATTERY_CAPACITY"

    invoke-static {v5}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 71
    :cond_8
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "5"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 72
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "Decide that refined article or not"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v4, "BATTERY_DECISION"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 74
    .restart local v2    # "result":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 75
    const-string v4, "passed"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 76
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 77
    :cond_9
    const-string v4, "failed"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 78
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "NG_NG"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 80
    :cond_a
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "Failed to read status"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 85
    .end local v2    # "result":Ljava/lang/String;
    :cond_b
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "6"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 86
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "read IN-Battery voltage"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v4, Lcom/sec/factory/aporiented/athandler/AtBatttest;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    invoke-virtual {v4}, Lcom/sec/factory/modules/ModulePower;->readInBatteryVoltage()Ljava/lang/String;

    move-result-object v2

    .line 89
    .restart local v2    # "result":Ljava/lang/String;
    const-string v4, "INBATT_SAVE_SOC"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 90
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNA()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 92
    :cond_c
    if-eqz v2, :cond_d

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_d

    .line 93
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4, v2}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 95
    :cond_d
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "Failed to IN-Battery voltage"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 100
    .end local v2    # "result":Ljava/lang/String;
    :cond_e
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "2"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "1"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 101
    const/4 v3, 0x0

    .line 102
    .local v3, "supportChargeCount":Z
    const-string v4, "SUPPORT_CHARGE_COUNT"

    invoke-static {v4}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    .line 104
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtBatttest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isbatterydischarge: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    if-eqz v3, :cond_10

    .line 107
    const-string v4, "PATH_BATTERY_CHARGE_COUNT"

    const-string v5, "0"

    invoke-static {v4, v5}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 108
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 111
    :cond_f
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 114
    :cond_10
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 126
    .end local v3    # "supportChargeCount":Z
    .restart local v2    # "result":Ljava/lang/String;
    :cond_11
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 130
    .end local v2    # "result":Ljava/lang/String;
    :cond_12
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBatttest;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto/16 :goto_3
.end method
