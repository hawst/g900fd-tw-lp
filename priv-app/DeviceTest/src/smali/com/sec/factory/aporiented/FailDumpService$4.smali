.class Lcom/sec/factory/aporiented/FailDumpService$4;
.super Ljava/lang/Object;
.source "FailDumpService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/FailDumpService;->getCopyLog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FailDumpService;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FailDumpService;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 266
    new-instance v4, Ljava/io/File;

    const-string v8, "/data/log"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 267
    .local v4, "dataLogDirectory":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v8, "/data/log/err"

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 268
    .local v2, "dataCPLogDirectory":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v8, "/efs/root/ERR"

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    .local v3, "dataCPLogDirectoryEfs":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v8, "/tombstones/mdm"

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    .local v1, "dataCPCrashLogDirectory":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    const-string v8, "/mnt/sdcard/log"

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 271
    .local v7, "sdcardLogDirectory":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    const-string v8, "/mnt/sdcard/log/cp"

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    .local v6, "sdcardCPCrashLogDirectory":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v8, "/data/app/bt.log"

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    .local v0, "btlog":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V
    invoke-static {v8, v4, v7}, Lcom/sec/factory/aporiented/FailDumpService;->access$1100(Lcom/sec/factory/aporiented/FailDumpService;Ljava/io/File;Ljava/io/File;)V

    .line 274
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V
    invoke-static {v8, v1, v6}, Lcom/sec/factory/aporiented/FailDumpService;->access$1100(Lcom/sec/factory/aporiented/FailDumpService;Ljava/io/File;Ljava/io/File;)V

    .line 276
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 277
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V
    invoke-static {v8, v2, v7}, Lcom/sec/factory/aporiented/FailDumpService;->access$1100(Lcom/sec/factory/aporiented/FailDumpService;Ljava/io/File;Ljava/io/File;)V

    .line 280
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 281
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->copyDirectory(Ljava/io/File;Ljava/io/File;)V
    invoke-static {v8, v3, v7}, Lcom/sec/factory/aporiented/FailDumpService;->access$1100(Lcom/sec/factory/aporiented/FailDumpService;Ljava/io/File;Ljava/io/File;)V

    .line 284
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 285
    const-string v8, "FailDumpService"

    const-string v9, "btlog.exists == true"

    invoke-static {v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v9, "/data/app/bt.log"

    const-string v10, "/mnt/sdcard/log/bt.log"

    const-string v11, "bt.log"

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->WriteToSDcard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v8, v9, v10, v11}, Lcom/sec/factory/aporiented/FailDumpService;->access$1200(Lcom/sec/factory/aporiented/FailDumpService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 289
    :cond_2
    const-string v8, "FailDumpService"

    const-string v9, "broadcast media mounted = "

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "file://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 292
    .local v5, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-virtual {v8, v5}, Lcom/sec/factory/aporiented/FailDumpService;->sendBroadcast(Landroid/content/Intent;)V

    .line 293
    iget-object v8, p0, Lcom/sec/factory/aporiented/FailDumpService$4;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    iget-object v8, v8, Lcom/sec/factory/aporiented/FailDumpService;->mHandler:Landroid/os/Handler;

    const/16 v9, 0x3ef

    const-wide/16 v10, 0x2710

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 294
    return-void
.end method
