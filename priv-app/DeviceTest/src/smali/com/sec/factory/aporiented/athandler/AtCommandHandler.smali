.class public abstract Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.super Ljava/lang/Object;
.source "AtCommandHandler.java"


# static fields
.field protected static final NG_RESPONE_STRING:Ljava/lang/String; = "NG"

.field public static final NOT_APPLICABLE:Ljava/lang/String; = "NOT_APPLICABLE"

.field public static final N_ACK_DFT_RESPONSE:I = 0x7

.field public static final N_ACK_RESPONSE:I = 0x0

.field public static final N_NACK_DFT_RESPONSE:I = 0x8

.field public static final N_NACK_RESPONSE:I = 0x1

.field public static final N_NA_RESPONSE:I = 0x5

.field public static final N_NG_RESPONSE:I = 0x4

.field public static final N_NON_RESULT_RESPONSE:I = 0x3

.field public static final N_NO_ARG_RESPONSE:I = 0x6

.field public static N_RESULT:I = 0x0

.field public static final N_RESULT_CP_SYSDUMP:I = 0xb

.field public static final N_RESULT_DFTKEY_RESPONSE:I = 0x9

.field public static final N_RESULT_DFT_RESPONSE:I = 0xa

.field public static final N_RESULT_RESPONSE:I = 0x2

.field protected static final OK_RESPONE_STRING:Ljava/lang/String; = "OK"

.field public static STAGE_PARALLEL:Z = false

.field public static final TYPE_APO:I = 0x1

.field public static final TYPE_CPO:I

.field protected static mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

.field protected static mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

.field protected static mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

.field protected static mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

.field protected static mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

.field protected static mModulePower:Lcom/sec/factory/modules/ModulePower;

.field protected static mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

.field protected static mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;


# instance fields
.field protected CLASS_NAME:Ljava/lang/String;

.field protected CMD_NAME:Ljava/lang/String;

.field public CMD_TYPE:I

.field protected final DELIMITER:Ljava/lang/String;

.field public NUM_ARGS:I

.field public PARALLEL_CMD_INDEX:Ljava/lang/String;

.field protected context:Landroid/content/Context;

.field protected mReceiver:Landroid/content/BroadcastReceiver;

.field public mResult:I

.field protected writer:Lcom/sec/factory/aporiented/ResponseWriter;

.field protected writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    const/4 v0, -0x1

    sput v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->N_RESULT:I

    .line 63
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    .line 64
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    .line 65
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    .line 66
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    .line 67
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    .line 68
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    .line 69
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    .line 70
    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    .line 72
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_TYPE:I

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mResult:I

    .line 54
    const-string v0, ","

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->DELIMITER:Ljava/lang/String;

    .line 56
    const-string v0, "CMD_NAME"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    .line 57
    const-string v0, "AtCommandHandler"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CLASS_NAME:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->NUM_ARGS:I

    .line 73
    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->context:Landroid/content/Context;

    .line 90
    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 91
    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 92
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleAudio;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleAudio;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    .line 93
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    :goto_1
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    .line 94
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommunication;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommunication;

    move-result-object v0

    :goto_2
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    .line 96
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDevice;

    move-result-object v0

    :goto_3
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    .line 97
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    if-nez v0, :cond_4

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleDFT;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleDFT;

    move-result-object v0

    :goto_4
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    .line 98
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    if-nez v0, :cond_5

    invoke-static {p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v0

    :goto_5
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    .line 99
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    if-nez v0, :cond_6

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleSensor;

    move-result-object v0

    :goto_6
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    .line 100
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    if-nez v0, :cond_7

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleTouchScreen;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleTouchScreen;

    move-result-object v0

    :goto_7
    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    .line 102
    return-void

    .line 92
    :cond_0
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    goto :goto_0

    .line 93
    :cond_1
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    goto :goto_1

    .line 94
    :cond_2
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    goto :goto_2

    .line 96
    :cond_3
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    goto :goto_3

    .line 97
    :cond_4
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    goto :goto_4

    .line 98
    :cond_5
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    goto :goto_5

    .line 99
    :cond_6
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleSensor:Lcom/sec/factory/modules/ModuleSensor;

    goto :goto_6

    .line 100
    :cond_7
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    goto :goto_7
.end method


# virtual methods
.method protected checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5
    .param p1, "argu"    # [Ljava/lang/String;
    .param p2, "argv"    # [Ljava/lang/String;

    .prologue
    .line 129
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    .line 130
    aget-object v1, p1, v0

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    const/4 v1, 0x0

    .line 140
    :goto_1
    return v1

    .line 129
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_1
    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_2

    .line 136
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "checkArgu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "This stage is parallel stage, index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method protected checkArgu([Ljava/lang/String;[Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "argu"    # [Ljava/lang/String;
    .param p2, "argv"    # [Ljava/lang/String;
    .param p3, "isParallel"    # Z

    .prologue
    .line 144
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "checkArgu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isParallel = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    if-eqz p3, :cond_0

    .line 146
    invoke-virtual {p0, p1, p2}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    .line 155
    :goto_0
    return v1

    .line 149
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 150
    aget-object v1, p1, v0

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    const/4 v1, 0x0

    goto :goto_0

    .line 149
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCmdType()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_TYPE:I

    return v0
.end method

.method public getResultType()I
    .locals 1

    .prologue
    .line 125
    sget v0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->N_RESULT:I

    return v0
.end method

.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 105
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method protected readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 249
    const-string v5, ""

    .line 250
    .local v5, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 253
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1fa0

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 256
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 258
    .local v2, "bufline":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 259
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 272
    .end local v2    # "bufline":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_4

    .line 274
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 281
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v5, :cond_2

    .line 282
    const-string v5, ""

    .line 284
    .end local v5    # "result":Ljava/lang/String;
    :cond_2
    return-object v5

    .line 275
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "result":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 276
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 277
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 262
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 263
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 272
    if-eqz v0, :cond_1

    .line 274
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 275
    :catch_2
    move-exception v3

    .line 276
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 264
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 265
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 272
    if-eqz v0, :cond_1

    .line 274
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 275
    :catch_4
    move-exception v3

    .line 276
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 267
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_7
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 272
    if-eqz v0, :cond_1

    .line 274
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_0

    .line 275
    :catch_6
    move-exception v3

    .line 276
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 272
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v0, :cond_3

    .line 274
    :try_start_9
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 277
    :cond_3
    :goto_5
    throw v6

    .line 275
    :catch_7
    move-exception v3

    .line 276
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 272
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 266
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_8
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 264
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_9
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 262
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_a
    move-exception v4

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method protected responseNA()Ljava/lang/String;
    .locals 3

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->getCmdType()I

    move-result v1

    if-nez v1, :cond_0

    .line 233
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->setResultType(I)V

    .line 234
    const/4 v0, 0x0

    .line 245
    :goto_0
    return-object v0

    .line 238
    :cond_0
    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_1

    .line 239
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+CME Error:NA,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\nOK\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "result":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z

    goto :goto_0

    .line 241
    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    const-string v0, "\r\n+CME Error:NA\r\n\r\nOK\r\n"

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method protected responseNG(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "arg"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->getCmdType()I

    move-result v1

    if-nez v1, :cond_0

    .line 181
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->setResultType(I)V

    .line 182
    const-string v0, "NG"

    .line 194
    :goto_0
    return-object v0

    .line 186
    :cond_0
    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_1

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",NG"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "result":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z

    .line 193
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    const-string v3, "Unknown"

    invoke-virtual {v1, v2, v3}, Lcom/sec/factory/aporiented/AtCommandInfo;->setFailInfo(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 189
    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",NG\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method protected responseOK(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "arg"    # Ljava/lang/String;

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->getCmdType()I

    move-result v1

    if-nez v1, :cond_0

    .line 199
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->setResultType(I)V

    .line 200
    const-string v0, "OK"

    .line 211
    :goto_0
    return-object v0

    .line 204
    :cond_0
    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_1

    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",OK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "result":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z

    goto :goto_0

    .line 207
    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",OK\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method protected responseOKNoNewLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "arg"    # Ljava/lang/String;

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->getCmdType()I

    move-result v1

    if-nez v1, :cond_0

    .line 216
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->setResultType(I)V

    .line 217
    const-string v0, "OK"

    .line 228
    :goto_0
    return-object v0

    .line 221
    :cond_0
    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_1

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",OK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "result":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z

    goto :goto_0

    .line 224
    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",OK\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method protected responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "arg"    # Ljava/lang/String;
    .param p2, "input"    # Ljava/lang/String;

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->getCmdType()I

    move-result v1

    if-nez v1, :cond_0

    .line 160
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->setResultType(I)V

    .line 161
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "responseString"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "arg: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  input: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .end local p2    # "input":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 166
    .restart local p2    # "input":Ljava/lang/String;
    :cond_0
    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_2

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->PARALLEL_CMD_INDEX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "result":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z

    .line 173
    const-string v1, "NG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2, p2}, Lcom/sec/factory/aporiented/AtCommandInfo;->setFailInfo(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    move-object p2, v0

    .line 176
    goto :goto_0

    .line 169
    .end local v0    # "result":Ljava/lang/String;
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n\r\nOK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method public setCmdName(Ljava/lang/String;)V
    .locals 0
    .param p1, "cmdName"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_NAME:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setCmdType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CMD_TYPE:I

    .line 110
    return-void
.end method

.method public setResultType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 121
    sput p1, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->N_RESULT:I

    .line 122
    return-void
.end method

.method protected startReceiver()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method protected stopReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 2
    .param p1, "mReceiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "stopReceiver"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 82
    const/4 p1, 0x0

    .line 84
    :cond_0
    return-void
.end method

.method protected writeFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 288
    const/4 v1, 0x0

    .line 291
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 293
    invoke-virtual {v2}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 301
    if-eqz v2, :cond_0

    .line 303
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 309
    :cond_0
    :goto_0
    const/4 v3, 0x1

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_1
    :goto_1
    return v3

    .line 304
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 294
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :catch_1
    move-exception v0

    .line 295
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 296
    const/4 v3, 0x0

    .line 301
    if-eqz v1, :cond_1

    .line 303
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 304
    :catch_2
    move-exception v0

    .line 305
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 301
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_3
    if-eqz v1, :cond_2

    .line 303
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 306
    :cond_2
    :goto_4
    throw v3

    .line 304
    :catch_3
    move-exception v0

    .line 305
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 301
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_3

    .line 294
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2
.end method
