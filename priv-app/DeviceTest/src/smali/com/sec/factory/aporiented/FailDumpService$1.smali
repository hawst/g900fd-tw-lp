.class Lcom/sec/factory/aporiented/FailDumpService$1;
.super Ljava/lang/Object;
.source "FailDumpService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/FailDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FailDumpService;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FailDumpService;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/factory/aporiented/FailDumpService$1;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 66
    const-string v0, "FailDumpService"

    const-string v1, "onServiceConnected()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$1;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$002(Lcom/sec/factory/aporiented/FailDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 68
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 71
    const-string v0, "FailDumpService"

    const-string v1, "onServiceDisconnected()"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/factory/aporiented/FailDumpService$1;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$002(Lcom/sec/factory/aporiented/FailDumpService;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 73
    return-void
.end method
