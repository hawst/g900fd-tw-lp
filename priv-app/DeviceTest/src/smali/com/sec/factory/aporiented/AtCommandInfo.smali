.class public Lcom/sec/factory/aporiented/AtCommandInfo;
.super Ljava/lang/Object;
.source "AtCommandInfo.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "AtCommandInfo"

.field private static final N_HISTORY_COMMAND_SIZE:I = 0xa

.field private static final N_HISTORY_LOG_SIZE:I = 0x1e

.field private static mFailCommand:Ljava/lang/String;

.field private static mFailReason:Ljava/lang/String;

.field private static mInstance:Lcom/sec/factory/aporiented/AtCommandInfo;


# instance fields
.field private mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(I)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 17
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(I)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    return-void
.end method

.method public static getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/factory/aporiented/AtCommandInfo;->mInstance:Lcom/sec/factory/aporiented/AtCommandInfo;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/sec/factory/aporiented/AtCommandInfo;

    invoke-direct {v0}, Lcom/sec/factory/aporiented/AtCommandInfo;-><init>()V

    sput-object v0, Lcom/sec/factory/aporiented/AtCommandInfo;->mInstance:Lcom/sec/factory/aporiented/AtCommandInfo;

    .line 26
    :cond_0
    sget-object v0, Lcom/sec/factory/aporiented/AtCommandInfo;->mInstance:Lcom/sec/factory/aporiented/AtCommandInfo;

    return-object v0
.end method


# virtual methods
.method public addCommandHistory(Ljava/lang/String;)Z
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    if-nez v0, :cond_0

    .line 35
    const-string v0, "AtCommandInfo"

    const-string v1, "addCommandHistory"

    const-string v2, "mHistoryCommandStack is null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    .line 45
    :goto_0
    return v0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 40
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->removeLast()Ljava/lang/Object;

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->push(Ljava/lang/Object;)V

    .line 45
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addLogHistory(Ljava/lang/String;)Z
    .locals 3
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    if-nez v0, :cond_0

    .line 65
    const-string v0, "AtCommandInfo"

    const-string v1, "AddCommandHistory"

    const-string v2, "mHistoryLogStack is null"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_1

    .line 70
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->removeLast()Ljava/lang/Object;

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->push(Ljava/lang/Object;)V

    .line 75
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getCommandHistory()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    if-nez v1, :cond_0

    .line 50
    const-string v1, "AtCommandInfo"

    const-string v2, "getCommandHistory"

    const-string v3, "mHistoryCommandStack is null"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v1, 0x0

    .line 60
    :goto_0
    return-object v1

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .local v0, "sb":Ljava/lang/StringBuilder;
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryCommandStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 60
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getFailHistroy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string v0, "FACTORY_FAILHIST"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFailInfo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Command : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/factory/aporiented/AtCommandInfo;->mFailCommand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Reason : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/factory/aporiented/AtCommandInfo;->mFailReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInformation()Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    const-string v1, "AtCommandInfo"

    const-string v2, "getInformation"

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "\r\n================== MSP Debug Information Start =================="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n\r\n=> FailHist info : \r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/AtCommandInfo;->getFailHistroy()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n\r\n=> Failed command info : \r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/AtCommandInfo;->getFailInfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n\r\n=> Histroy command info : \r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/AtCommandInfo;->getCommandHistory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n=> Histroy Log info : \r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/AtCommandInfo;->getLogHistory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, "================== MSP Debug Information End =================="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, "AtCommandInfo"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLogHistory()Ljava/lang/String;
    .locals 4

    .prologue
    .line 79
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    if-nez v1, :cond_0

    .line 80
    const-string v1, "AtCommandInfo"

    const-string v2, "getLogHistory"

    const-string v3, "mHistoryLogStack is null"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v1, 0x0

    .line 90
    :goto_0
    return-object v1

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v0, "sb":Ljava/lang/StringBuilder;
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtCommandInfo;->mHistoryLogStack:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 90
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setFailInfo(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "fail"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 94
    const-string v0, "AtCommandInfo"

    const-string v1, "setFailInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Command : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sput-object p1, Lcom/sec/factory/aporiented/AtCommandInfo;->mFailCommand:Ljava/lang/String;

    .line 96
    sput-object p2, Lcom/sec/factory/aporiented/AtCommandInfo;->mFailReason:Ljava/lang/String;

    .line 97
    const/4 v0, 0x1

    return v0
.end method
