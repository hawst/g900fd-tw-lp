.class Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;
.super Landroid/os/Handler;
.source "DIServiceConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/DIServiceConnectionManager;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/DIServiceConnectionManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 51
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "handleMessage"

    const-string v9, "get message from FtClient"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    .local v0, "bundle":Landroid/os/Bundle;
    iget v6, p1, Landroid/os/Message;->what:I

    .line 55
    .local v6, "what":I
    const-string v7, "command"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "cmd":Ljava/lang/String;
    const-string v7, "type"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 57
    .local v5, "type":Ljava/lang/String;
    const-string v7, "nv_index"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    .line 58
    .local v3, "nvIndex":B
    const-string v7, "nv_value"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v4

    .line 60
    .local v4, "nvValue":B
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "handleMessage"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "whar:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",  cmd: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    packed-switch v6, :pswitch_data_0

    .line 87
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "get"

    const-string v9, "wrong message"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    iget-object v7, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    # getter for: Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;
    invoke-static {v7}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->access$000(Lcom/sec/factory/aporiented/DIServiceConnectionManager;)Lcom/sec/factory/aporiented/ResponseWriter;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 66
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "WHAT_RESPONSE_CMD"

    const-string v9, "Success"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :pswitch_1
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "WHAT_REQUEST_CMD"

    const-string v9, "Not support request"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :pswitch_2
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "handleMessage"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "WHAT_REQUEST_SET_NV,  nvIndex: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", nvValue: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-static {v3, v4}, Lcom/sec/factory/support/NVAccessor;->setNV(IB)I

    .line 77
    iget-object v7, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    const/16 v8, 0x3ee

    invoke-virtual {v7, v8, v3, v4}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->send(IBB)Z

    goto :goto_0

    .line 80
    :pswitch_3
    const-string v7, "DIServiceConnectionManager"

    const-string v8, "handleMessage"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "WHAT_REQUEST_GET_NV,  nvIndex: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", nvValue: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {v3}, Lcom/sec/factory/support/NVAccessor;->getNV(I)B

    move-result v2

    .line 83
    .local v2, "nv":B
    iget-object v7, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;->this$0:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    const/16 v8, 0x3ef

    invoke-virtual {v7, v8, v3, v2}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->send(IBB)Z

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x3ea
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
