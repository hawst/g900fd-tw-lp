.class public Lcom/sec/factory/aporiented/ResponseWriter;
.super Ljava/lang/Object;
.source "ResponseWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "ResponseWriter"

.field private static mWritable:Z


# instance fields
.field private mResponseListener:Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;

.field private mStringQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private out:Ljava/io/DataOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/factory/aporiented/ResponseWriter;->mWritable:Z

    return-void
.end method

.method public constructor <init>(Landroid/net/LocalSocket;)V
    .locals 5
    .param p1, "clientSocketSend"    # Landroid/net/LocalSocket;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mResponseListener:Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;

    .line 37
    const-string v1, "ResponseWriter"

    const-string v2, "ResponseWriter"

    const-string v3, "Create ResponseWriter"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mStringQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 41
    invoke-virtual {p1}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-virtual {p1}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    const/16 v4, 0x2000

    invoke-direct {v2, v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 49
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const-string v1, "ResponseWriter"

    const-string v2, "ResponseWriter"

    const-string v3, "Socket is closed"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_0

    .line 101
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;

    .line 108
    :cond_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public declared-synchronized getWritable()Z
    .locals 1

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/sec/factory/aporiented/ResponseWriter;->mWritable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setResponseListener(Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mResponseListener:Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;

    .line 112
    return-void
.end method

.method public declared-synchronized setWritable(Z)V
    .locals 4
    .param p1, "w"    # Z

    .prologue
    .line 25
    monitor-enter p0

    :try_start_0
    const-string v0, "ResponseWriter"

    const-string v1, "ResponseWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWritable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    sput-boolean p1, Lcom/sec/factory/aporiented/ResponseWriter;->mWritable:Z

    .line 27
    if-eqz p1, :cond_0

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :cond_0
    monitor-exit p0

    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write(Ljava/lang/String;)Z
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mStringQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-nez v1, :cond_1

    .line 55
    const-string v1, "ResponseWriter"

    const-string v2, "ResponseWriter"

    const-string v3, "write - Queue is null"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    const/4 v0, 0x0

    .line 77
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 59
    :cond_1
    const/4 v0, 0x0

    .line 60
    .local v0, "result":Z
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ResponseWriter;->getWritable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 61
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mStringQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 62
    const-string v1, "ResponseWriter"

    const-string v2, "ResponseWriter"

    const-string v3, "write - poll queue"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mStringQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/ResponseWriter;->writeToSocket(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 65
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/factory/aporiented/ResponseWriter;->writeToSocket(Ljava/lang/String;)Z

    move-result v0

    .line 73
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mResponseListener:Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mResponseListener:Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;

    invoke-interface {v1, p1}, Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;->onResponse(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 54
    .end local v0    # "result":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 67
    .restart local v0    # "result":Z
    :cond_4
    :try_start_2
    const-string v1, "ResponseWriter"

    const-string v2, "ResponseWriter"

    const-string v3, "write - need to queueing"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    if-eqz p1, :cond_3

    .line 69
    iget-object v1, p0, Lcom/sec/factory/aporiented/ResponseWriter;->mStringQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_2
.end method

.method public writeToSocket(Ljava/lang/String;)Z
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 83
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 85
    :try_start_0
    iget-object v2, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lcom/sec/factory/aporiented/ResponseWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    const/4 v1, 0x1

    .line 95
    :goto_0
    return v1

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 92
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const-string v2, "ResponseWriter"

    const-string v3, "write"

    const-string v4, "out is null"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
