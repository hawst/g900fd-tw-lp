.class public Lcom/sec/factory/aporiented/ProcessThread;
.super Ljava/lang/Thread;
.source "ProcessThread.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "ProcessThread"

.field private static final POLLING_TIME:I = 0x32


# instance fields
.field private ID:I

.field public active:Z

.field private mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

.field private mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

.field public mIsKilled:Z

.field private mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

.field private pastCMD:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/factory/aporiented/CommandListener;Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;[Lcom/sec/factory/aporiented/ProcessThread;I)V
    .locals 5
    .param p1, "listener"    # Lcom/sec/factory/aporiented/CommandListener;
    .param p2, "category"    # Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .param p3, "threads"    # [Lcom/sec/factory/aporiented/ProcessThread;
    .param p4, "id"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 12
    iput-boolean v1, p0, Lcom/sec/factory/aporiented/ProcessThread;->mIsKilled:Z

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    .line 18
    iput-object v4, p0, Lcom/sec/factory/aporiented/ProcessThread;->pastCMD:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

    .line 24
    iput-boolean v1, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    .line 25
    iput-object p2, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    .line 26
    iput-object p3, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 27
    iput p4, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    .line 28
    iget-object v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    if-eqz v0, :cond_0

    .line 29
    const-string v0, "ProcessThread"

    const-string v1, "ProcessThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "category ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v3}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], priority ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v3}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getPriority()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    :goto_0
    return-void

    .line 31
    :cond_0
    const-string v0, "ProcessThread"

    const-string v1, "ProcessThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "category ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCategoryPriority()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getPriority()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public kill()V
    .locals 4

    .prologue
    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->mIsKilled:Z

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    .line 165
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->interrupt()V

    .line 166
    const-string v0, "ProcessThread"

    const-string v1, "run"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Kill ProcessThread ID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public run()V
    .locals 13

    .prologue
    .line 46
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ProcessThread ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] start"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getDependencyList()Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "dependencies":Ljava/lang/String;
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ProcessThread ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] dependencies = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v6, 0x0

    .line 52
    .local v6, "isMyTurn":Z
    :cond_0
    if-nez v6, :cond_1

    .line 53
    const/4 v6, 0x1

    .line 54
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-nez v9, :cond_5

    .line 55
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", mProcessThread = null"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_1
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    .line 120
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ProcessThread ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] activate"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_2
    :goto_0
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->isEnabled()Z

    move-result v9

    if-eqz v9, :cond_b

    iget-boolean v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    if-eqz v9, :cond_b

    .line 122
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->pollCommand()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "cmd":Ljava/lang/String;
    const-string v9, "ProcessThread"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "run["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ProcessThread cmd = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", ID="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    if-eqz v0, :cond_4

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getCommandGap()I

    move-result v9

    if-lez v9, :cond_4

    .line 129
    const/4 v9, 0x0

    :try_start_0
    const-string v10, ","

    invoke-virtual {v0, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 130
    .local v8, "str":Ljava/lang/String;
    if-eqz v8, :cond_3

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->pastCMD:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 131
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cmd = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Sleep : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v12}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getCommandGap()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "ms"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getCommandGap()I

    move-result v9

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 134
    :cond_3
    iput-object v8, p0, Lcom/sec/factory/aporiented/ProcessThread;->pastCMD:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2

    .line 140
    .end local v8    # "str":Ljava/lang/String;
    :cond_4
    :goto_1
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

    invoke-interface {v9, v0}, Lcom/sec/factory/aporiented/CommandListener;->onRunCommand(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    iget v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    if-lez v9, :cond_2

    .line 141
    const-string v9, "ProcessThread"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Waiting response..."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

    iget v10, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    invoke-interface {v9, v0, v10}, Lcom/sec/factory/aporiented/CommandListener;->onWaitingResponse(Ljava/lang/String;I)Z

    .line 143
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    .line 144
    :goto_2
    iget-boolean v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    if-nez v9, :cond_a

    .line 146
    const-wide/16 v10, 0x32

    :try_start_1
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 147
    :catch_0
    move-exception v4

    .line 148
    .local v4, "ie":Ljava/lang/InterruptedException;
    const-string v9, "ProcessThread"

    const-string v10, "run"

    const-string v11, "The thread interrupted"

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    goto :goto_2

    .line 59
    .end local v0    # "cmd":Ljava/lang/String;
    .end local v4    # "ie":Ljava/lang/InterruptedException;
    :cond_5
    if-nez v1, :cond_6

    .line 60
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", dependencies = null"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v1, ""

    .line 64
    :cond_6
    const-string v9, "AT+RILCPCMD"

    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 65
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Waiting RIL-Command End notification form RIL. Start"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :goto_3
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtParallel;->isRILCommandDone()Z

    move-result v9

    if-nez v9, :cond_7

    .line 68
    const-wide/16 v10, 0x32

    invoke-static {v10, v11}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    goto :goto_3

    .line 70
    :cond_7
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Waiting RIL-Command End notification form RIL. End"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_8
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_4
    iget v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    if-ge v3, v9, :cond_0

    .line 75
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    iget-boolean v9, v9, Lcom/sec/factory/aporiented/ProcessThread;->mIsKilled:Z

    if-nez v9, :cond_9

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ProcessThread;->isAlive()Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ProcessThread;->isInterrupted()Z

    move-result v9

    if-nez v9, :cond_9

    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 79
    :try_start_2
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v7

    .line 80
    .local v7, "joinThreadName":Ljava/lang/String;
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Start Join[ "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v9, v9, v3

    invoke-virtual {v9}, Lcom/sec/factory/aporiented/ProcessThread;->join()V

    .line 82
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", End Join[ "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 74
    .end local v7    # "joinThreadName":Ljava/lang/String;
    :cond_9
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    .line 83
    :catch_1
    move-exception v2

    .line 84
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_5

    .line 135
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "i":I
    .restart local v0    # "cmd":Ljava/lang/String;
    :catch_2
    move-exception v5

    .line 136
    .local v5, "ioobe":Ljava/lang/IndexOutOfBoundsException;
    invoke-static {v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 152
    .end local v5    # "ioobe":Ljava/lang/IndexOutOfBoundsException;
    :cond_a
    const-string v9, "ProcessThread"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Catch response..."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 157
    .end local v0    # "cmd":Ljava/lang/String;
    :cond_b
    const-string v9, "ProcessThread"

    const-string v10, "run"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ProcessThread ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] done"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCommandListener:Lcom/sec/factory/aporiented/CommandListener;

    iget-object v10, p0, Lcom/sec/factory/aporiented/ProcessThread;->mCategory:Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    invoke-virtual {v10}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getName()Ljava/lang/String;

    move-result-object v10

    iget v11, p0, Lcom/sec/factory/aporiented/ProcessThread;->ID:I

    invoke-interface {v9, v10, v11}, Lcom/sec/factory/aporiented/CommandListener;->onFinishAllCommand(Ljava/lang/String;I)Z

    .line 159
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/factory/aporiented/ProcessThread;->active:Z

    .line 160
    return-void
.end method
