.class Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;
.super Ljava/lang/Object;
.source "AtFpsensor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtFpsensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Method4Data"
.end annotation


# instance fields
.field private mSingnalLossCSTVFail:I

.field private mSingnalLossCalSNR:I

.field private mSingnalLossPixelFail:I

.field private mSingnalLossRate:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "rate"    # Ljava/lang/String;
    .param p2, "pixelFail"    # Ljava/lang/String;
    .param p3, "cSTVFail"    # Ljava/lang/String;
    .param p4, "calSNR"    # Ljava/lang/String;

    .prologue
    .line 1373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossRate:I

    .line 1375
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossPixelFail:I

    .line 1376
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossCSTVFail:I

    .line 1377
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossCalSNR:I

    .line 1378
    return-void
.end method

.method private round(FI)F
    .locals 6
    .param p1, "t"    # F
    .param p2, "decimal"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 1381
    float-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getCSTVFail()I
    .locals 1

    .prologue
    .line 1386
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossCSTVFail:I

    return v0
.end method

.method public getCalSNR()I
    .locals 1

    .prologue
    .line 1387
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossCalSNR:I

    return v0
.end method

.method public getPixelFail()I
    .locals 1

    .prologue
    .line 1385
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossPixelFail:I

    return v0
.end method

.method public getRate()I
    .locals 1

    .prologue
    .line 1384
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->mSingnalLossRate:I

    return v0
.end method
