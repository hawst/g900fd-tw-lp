.class public Lcom/sec/factory/aporiented/athandler/AtSerialno;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtSerialno.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 13
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 14
    const-string v2, "SERIALNO"

    iput-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtSerialno;->CMD_NAME:Ljava/lang/String;

    .line 15
    const-string v2, "AtSerialno"

    iput-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtSerialno;->CLASS_NAME:Ljava/lang/String;

    .line 16
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/factory/aporiented/athandler/AtSerialno;->NUM_ARGS:I

    .line 17
    const-string v2, "SERIAL_NO"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 19
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 20
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtSerialno;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const-string v3, "00000000000"

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModuleDevice;->writeSerialNo(Ljava/lang/String;)V

    .line 21
    const/4 v6, 0x0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v6}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 23
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 28
    monitor-enter p0

    const/4 v1, 0x0

    .line 30
    .local v1, "resData":Ljava/lang/String;
    :try_start_0
    array-length v3, p1

    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtSerialno;->NUM_ARGS:I

    if-ge v3, v4, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 58
    :goto_0
    monitor-exit p0

    return-object v3

    .line 34
    :cond_0
    const/4 v3, 0x2

    :try_start_1
    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 35
    const/4 v3, 0x0

    aget-object v3, p1, v3

    sget-object v4, Lcom/sec/factory/aporiented/athandler/AtSerialno;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v4}, Lcom/sec/factory/modules/ModuleDevice;->readSerialNo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v3, v1

    .line 58
    goto :goto_0

    .line 37
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 40
    const/4 v3, 0x0

    aget-object v3, p1, v3

    sget-object v4, Lcom/sec/factory/aporiented/athandler/AtSerialno;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v4}, Lcom/sec/factory/modules/ModuleDevice;->readiSerialNo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 42
    :cond_2
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "2"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .local v2, "serial_no":Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_2
    array-length v3, p1

    if-ge v0, v3, :cond_3

    .line 46
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 50
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 51
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtSerialno;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleDevice;->writeSerialNo(Ljava/lang/String;)V

    .line 52
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 53
    goto :goto_1

    .line 55
    .end local v0    # "i":I
    .end local v2    # "serial_no":Ljava/lang/StringBuilder;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSerialno;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_1

    .line 28
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "aFile"    # Ljava/lang/String;

    .prologue
    .line 62
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method
