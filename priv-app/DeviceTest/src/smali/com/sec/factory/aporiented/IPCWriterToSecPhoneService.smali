.class public Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;
.super Ljava/lang/Object;
.source "IPCWriterToSecPhoneService.java"


# static fields
.field public static final BASIC_FILE_SIZE:I = 0x6

.field private static final BOOT_COMPLETED_MSG:I = 0x3e8

.field public static final CLASS_NAME:Ljava/lang/String; = "IPCWriterToSecPhoneService"

.field public static final COMMAND_RECEIVE_FAIL:I = 0xff

.field public static final COMMAND_RECEIVE_SUCCESS:I = 0x1

.field private static final CP_MODEM_SYSDUMP_SUCCESS:I = 0x68

.field private static final DFMS_CONFIRM_SENT_SUCCESS:I = 0x64

.field private static final DFMS_EVENT_SENT_SUCCESS:I = 0x65

.field private static final DFT_CONFIRM_SENT_SUCCESS:I = 0x66

.field private static final DFT_EVENT_SENT_SUCCESS:I = 0x67

.field private static final NA_ATTRIBUTE:Ljava/lang/String; = "fe"

.field private static final NG_ATTRIBUTE:Ljava/lang/String; = "ff"

.field public static final OEM_DFT_CFRM:I = 0x6

.field public static final OEM_DFT_EVENT:I = 0x5

.field public static final OEM_DFT_LENGTH:I = 0x1

.field public static final OEM_FACTORY_CFRM:I = 0x2

.field public static final OEM_FACTORY_CFRM_FILESIZE:I = 0x5

.field public static final OEM_FACTORY_EVENT:I = 0x1

.field public static final OEM_FACTORY_LENGTH:I = 0x2

.field public static final OEM_FUNCTION_ID_FACTORY:I = 0x12

.field private static final OEM_MODEM_LOG:I = 0x12

.field private static final OEM_SYSDUMP_FUNCTAG:I = 0x7


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFactoryTestServiceConnection:Landroid/content/ServiceConnection;

.field private final mHandler:Landroid/os/Handler;

.field public mResoponseMessenger:Landroid/os/Messenger;

.field private mResponse:Landroid/os/Message;

.field private mSecPhoneServiceMessenger:Landroid/os/Messenger;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResoponseMessenger:Landroid/os/Messenger;

    .line 50
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResponse:Landroid/os/Message;

    .line 51
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mContext:Landroid/content/Context;

    .line 52
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mSecPhoneServiceMessenger:Landroid/os/Messenger;

    .line 109
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$1;-><init>(Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mFactoryTestServiceConnection:Landroid/content/ServiceConnection;

    .line 164
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$2;-><init>(Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mHandler:Landroid/os/Handler;

    .line 68
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "ResponseWriter"

    const-string v2, "Create IPCWriterToSecPhoneService Without Messenger"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResoponseMessenger:Landroid/os/Messenger;

    .line 50
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResponse:Landroid/os/Message;

    .line 51
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mContext:Landroid/content/Context;

    .line 52
    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mSecPhoneServiceMessenger:Landroid/os/Messenger;

    .line 109
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$1;-><init>(Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mFactoryTestServiceConnection:Landroid/content/ServiceConnection;

    .line 164
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$2;-><init>(Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mHandler:Landroid/os/Handler;

    .line 55
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "ResponseWriter"

    const-string v2, "Create IPCWriterToSecPhoneService"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    if-eqz p1, :cond_0

    .line 58
    iput-object p1, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mContext:Landroid/content/Context;

    .line 63
    :goto_0
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResoponseMessenger:Landroid/os/Messenger;

    .line 64
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->connectToSecPhoneService()V

    .line 65
    return-void

    .line 60
    :cond_0
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "IPCWriterToSecPhoneService"

    const-string v2, "messenger is not available"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mSecPhoneServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method


# virtual methods
.method public connectToSecPhoneService()V
    .locals 4

    .prologue
    .line 94
    const-string v1, "IPCWriterToSecPhoneService"

    const-string v2, "connectToSecPhoneService"

    const-string v3, " "

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 96
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    iget-object v1, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mFactoryTestServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 98
    return-void
.end method

.method public disConnectSecPhoneService()V
    .locals 3

    .prologue
    .line 101
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "disConnectSecPhoneService"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mFactoryTestServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public sendRILBootMsg()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 130
    const/4 v0, 0x6

    .line 131
    .local v0, "OEM_FUNCTION_ID_IMEI":B
    const/4 v1, 0x5

    .line 136
    .local v1, "OEM_IMEI_EVENT_START_IMEI":B
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 137
    .local v2, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 138
    .local v4, "dos":Ljava/io/DataOutputStream;
    const/4 v10, 0x2

    new-array v3, v10, [B

    .line 139
    .local v3, "command":[B
    const/4 v10, 0x6

    aput-byte v10, v3, v9

    .line 140
    const/4 v10, 0x5

    aput-byte v10, v3, v8

    .line 141
    const/4 v7, 0x2

    .line 142
    .local v7, "sizeofShort":S
    array-length v10, v3

    add-int/2addr v10, v7

    int-to-short v6, v10

    .line 145
    .local v6, "length":S
    const/4 v10, 0x0

    :try_start_0
    array-length v11, v3

    invoke-virtual {v4, v3, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    .line 146
    invoke-virtual {v4, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 147
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :try_start_1
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 153
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 159
    :goto_0
    iget-object v9, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mHandler:Landroid/os/Handler;

    const/16 v10, 0x3e8

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResponse:Landroid/os/Message;

    .line 160
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->write([B)V

    .line 161
    :goto_1
    return v8

    .line 154
    :catch_0
    move-exception v5

    .line 155
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 148
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 152
    .restart local v5    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 153
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    move v8, v9

    .line 156
    goto :goto_1

    .line 154
    :catch_2
    move-exception v5

    .line 155
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 151
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 152
    :try_start_3
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V

    .line 153
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 156
    :goto_3
    throw v8

    .line 154
    :catch_3
    move-exception v5

    .line 155
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public write([B)V
    .locals 7
    .param p1, "data"    # [B

    .prologue
    .line 73
    const-string v3, "IPCWriterToSecPhoneService"

    const-string v4, "write"

    const-string v5, "Send Response Message to SecPhone"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "IPCWriterToSecPhoneService"

    const-string v4, "write"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Response "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v1, "req":Landroid/os/Bundle;
    const-string v3, "request"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 77
    iget-object v2, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResponse:Landroid/os/Message;

    .line 78
    .local v2, "response":Landroid/os/Message;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResponse:Landroid/os/Message;

    .line 79
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 80
    iget-object v3, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mResoponseMessenger:Landroid/os/Messenger;

    iput-object v3, v2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 83
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mSecPhoneServiceMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 84
    iget-object v3, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;->mSecPhoneServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    const-string v3, "IPCWriterToSecPhoneService"

    const-string v4, "write"

    const-string v5, "send failed!!!"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
