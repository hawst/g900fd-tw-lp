.class public Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
.super Ljava/lang/Object;
.source "ParallelCommandsParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/ParallelCommandsParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Category"
.end annotation


# static fields
.field public static final PRIORITY_DEFAULT:I = 0x1

.field public static final PRIORITY_HIGH:I = 0x3

.field public static final PRIORITY_HIGHEST:I = 0x4

.field public static final PRIORITY_LOW:I = 0x0

.field public static final PRIORITY_MID:I = 0x2


# instance fields
.field private mCommandsGap:I

.field private mDependencyList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsRunning:Z

.field private mName:Ljava/lang/String;

.field private mPriority:I

.field private mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;I[Ljava/lang/String;)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "priority"    # I
    .param p3, "dependency"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 94
    iput-object v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mDependencyList:Ljava/util/LinkedList;

    .line 95
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mCommandsGap:I

    .line 96
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mIsRunning:Z

    .line 28
    iput-object p1, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mName:Ljava/lang/String;

    .line 29
    iput p2, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mPriority:I

    .line 30
    new-instance v4, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 31
    if-eqz p3, :cond_0

    .line 32
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mDependencyList:Ljava/util/LinkedList;

    .line 33
    move-object v0, p3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 34
    .local v1, "cmd":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mDependencyList:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 33
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 37
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "cmd":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/String;I[Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "priority"    # I
    .param p3, "dependency"    # [Ljava/lang/String;
    .param p4, "gap"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    .line 41
    iput p4, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mCommandsGap:I

    .line 42
    return-void
.end method


# virtual methods
.method public addToQueue(Ljava/lang/String;)Z
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCommandGap()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mCommandsGap:I

    return v0
.end method

.method public getDependencyList()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mDependencyList:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mDependencyList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mDependencyList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mPriority:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mIsRunning:Z

    return v0
.end method

.method public pollCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public setRunningStatus(Z)V
    .locals 0
    .param p1, "r"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mIsRunning:Z

    .line 85
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->mName:Ljava/lang/String;

    return-object v0
.end method
