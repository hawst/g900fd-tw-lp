.class Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;
.super Landroid/content/BroadcastReceiver;
.source "AtWifiidrw.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtWifiidrw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    .line 73
    const-string v4, "S_DATA"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "resData":Ljava/lang/String;
    const-string v4, "OK"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 76
    const-string v4, "boot"

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->getCmdType()I

    move-result v4

    if-nez v4, :cond_2

    .line 79
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const-string v5, "28"

    const-string v6, "02"

    invoke-virtual {v4, v7, v5, v6, v2}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :goto_1
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v5}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$100(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)Landroid/content/BroadcastReceiver;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->stopReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 81
    :cond_2
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$000()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    goto :goto_1

    .line 85
    :cond_3
    const-string v4, "NG"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 86
    const-string v4, "boot"

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 88
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->getCmdType()I

    move-result v4

    if-nez v4, :cond_4

    .line 89
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const/4 v5, 0x4

    const-string v6, "28"

    const-string v7, "02"

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :goto_2
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v5}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$100(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)Landroid/content/BroadcastReceiver;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->stopReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 91
    :cond_4
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$000()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    goto :goto_2

    .line 96
    :cond_5
    const-string v1, ""

    .line 97
    .local v1, "mData":Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "splitdata":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    array-length v4, v3

    if-ge v0, v4, :cond_6

    .line 100
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 103
    :cond_6
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->getCmdType()I

    move-result v4

    if-nez v4, :cond_7

    .line 104
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const-string v5, "28"

    const-string v6, "01"

    invoke-virtual {v4, v7, v5, v6, v1}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :goto_4
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v5}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$100(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)Landroid/content/BroadcastReceiver;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->stopReceiver(Landroid/content/BroadcastReceiver;)V

    goto/16 :goto_0

    .line 106
    :cond_7
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->access$000()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    goto :goto_4
.end method
