.class Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;
.super Ljava/lang/Thread;
.source "AtFaildump.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtFaildump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Reader"
.end annotation


# instance fields
.field is:Ljava/io/InputStream;

.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtFaildump;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtFaildump;Ljava/io/InputStream;)V
    .locals 0
    .param p2, "is"    # Ljava/io/InputStream;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;->this$0:Lcom/sec/factory/aporiented/athandler/AtFaildump;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 196
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;->is:Ljava/io/InputStream;

    .line 197
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 201
    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;->is:Ljava/io/InputStream;

    invoke-direct {v2, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 202
    .local v2, "inStreamReader":Ljava/io/InputStreamReader;
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 203
    .local v0, "br":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 205
    .local v3, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 206
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFaildump$Reader;->this$0:Lcom/sec/factory/aporiented/athandler/AtFaildump;

    iget-object v4, v4, Lcom/sec/factory/aporiented/athandler/AtFaildump;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Reader"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "out = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "inStreamReader":Ljava/io/InputStreamReader;
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 209
    .local v1, "ex":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 211
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_0
    return-void
.end method
