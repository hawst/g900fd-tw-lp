.class Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;
.super Ljava/lang/Object;
.source "AtFpsensor.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

.field final synthetic val$wm:Landroid/view/WindowManager;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;Landroid/view/WindowManager;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->val$wm:Landroid/view/WindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 625
    const/4 v1, 0x3

    if-ne p2, v1, :cond_1

    .line 627
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I
    invoke-static {v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->access$100(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)I

    move-result v1

    const/16 v2, 0x3f1

    if-ne v1, v2, :cond_2

    .line 629
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;

    invoke-direct {v2, p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 644
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->val$wm:Landroid/view/WindowManager;

    invoke-interface {v1, p1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 646
    :cond_1
    const/4 v1, 0x0

    return v1

    .line 639
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->access$200(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 640
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->access$200(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/validity/fingerprint/Fingerprint;->notify(ILjava/lang/Object;)I

    move-result v0

    .line 641
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "createPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sensor status : VCS_NOTIFY_SNSR_TEST_CONTINUE ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
