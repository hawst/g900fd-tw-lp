.class Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;
.super Landroid/content/BroadcastReceiver;
.source "AtEwriteck.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/athandler/AtEwriteck;->handleCommand([Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtEwriteck;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x2

    .line 45
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    const-string v1, "penInsert"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->spenInserted:Z

    .line 46
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onReceive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "spenInserted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-boolean v3, v3, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->spenInserted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->getCmdType()I

    move-result v0

    if-nez v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-boolean v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->spenInserted:Z

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onReceive"

    const-string v2, "S pen detected"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const-string v1, "70"

    const-string v2, "01"

    const-string v3, "OK"

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 59
    :goto_1
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onReceive"

    const-string v2, "S pen not detected"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtEwriteck;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    const-string v1, "70"

    const-string v2, "01"

    const-string v3, "NG"

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/factory/cporiented/ResponseWriterCPO;->write(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->rcvIntent:Z

    goto :goto_1
.end method
