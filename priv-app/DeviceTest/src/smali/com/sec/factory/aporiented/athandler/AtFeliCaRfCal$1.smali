.class Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;
.super Landroid/content/BroadcastReceiver;
.source "AtFeliCaRfCal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "action":Ljava/lang/String;
    const-string v1, "NG"

    .line 78
    .local v1, "resData":Ljava/lang/String;
    const-string v2, "com.sec.android.app.felicatest.FELICA_TEST_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.sec.android.app.felicatest.FELICA_TEST_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "1"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 82
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    const-string v2, "com.sec.android.app.felicatest.FELICA_TEST_SETNV_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 84
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.sec.android.app.felicatest.FELICA_TEST_SETNV_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "0"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 87
    const/4 v2, 0x1

    # setter for: Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->isLaunchFeliCaTest:Z
    invoke-static {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->access$002(Z)Z

    .line 88
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto :goto_0

    .line 89
    :cond_2
    const-string v2, "com.sec.android.app.felicatest.FELICA_TEST_GETNV_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 90
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.sec.android.app.felicatest.FELICA_TEST_GETNV_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "0"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 93
    const/4 v2, 0x0

    # setter for: Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->isLaunchFeliCaTest:Z
    invoke-static {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->access$002(Z)Z

    .line 94
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto :goto_0

    .line 95
    :cond_3
    const-string v2, "com.sec.android.app.felicatest.FELICA_TEST_PASS_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.sec.android.app.felicatest.FELICA_TEST_PASS_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "2"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 99
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 100
    :cond_4
    const-string v2, "com.sec.android.app.felicatest.FELICA_TEST_FAIL_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 101
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.sec.android.app.felicatest.FELICA_TEST_FAIL_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "2"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 104
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 105
    :cond_5
    const-string v2, "com.sec.android.app.felicatest.FELICA_GETIDM_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 106
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.sec.android.app.felicatest.FELICA_GETIDM_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "3"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 109
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 110
    :cond_6
    const-string v2, "com.samsung.felicalock.FELICA_LOCK_STATUS_LOCK_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 111
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.samsung.felicalock.FELICA_LOCK_STATUS_LOCK_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 113
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "4"

    const-string v5, "LOCK"

    invoke-virtual {v3, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 114
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 115
    :cond_7
    const-string v2, "com.samsung.felicalock.FELICA_LOCK_STATUS_UNLOCK_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 116
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.samsung.felicalock.FELICA_LOCK_STATUS_UNLOCK_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "4"

    const-string v5, "UNLOCK"

    invoke-virtual {v3, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 119
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 120
    :cond_8
    const-string v2, "com.samsung.felicalock.FELICA_LOCK_STATUS_ERROR_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 121
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.samsung.felicalock.FELICA_LOCK_STATUS_ERROR_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "4"

    const-string v5, "ERROR"

    invoke-virtual {v3, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 124
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 125
    :cond_9
    const-string v2, "com.samsung.felicalock.FELICA_LOCK_RESET_PASS_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 126
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.samsung.felicalock.FELICA_LOCK_RESET_PASS_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "4"

    const-string v5, "OK"

    invoke-virtual {v3, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 129
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0

    .line 130
    :cond_a
    const-string v2, "com.samsung.felicalock.FELICA_LOCK_RESET_FAIL_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "com.samsung.felicalock.FELICA_LOCK_RESET_FAIL_RESPONSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const-string v2, "S_DATA"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 133
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    const-string v4, "4"

    const-string v5, "NG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 134
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;

    invoke-virtual {v2}, Lcom/sec/factory/aporiented/athandler/AtFeliCaRfCal;->stopReceiver()V

    goto/16 :goto_0
.end method
