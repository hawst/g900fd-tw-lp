.class public Lcom/sec/factory/aporiented/athandler/AtFuelgaic;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFuelgaic.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 10
    const-string v0, "FUELGAIC"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->CMD_NAME:Ljava/lang/String;

    .line 11
    const-string v0, "AtFuelgaic"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->CLASS_NAME:Ljava/lang/String;

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->NUM_ARGS:I

    .line 13
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 17
    monitor-enter p0

    const/4 v0, 0x0

    .line 19
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 20
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 36
    :goto_1
    monitor-exit p0

    return-object v1

    .line 19
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->NUM_ARGS:I

    goto :goto_0

    .line 23
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 24
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModulePower;->resetFuelGaugeIC()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25
    const/4 v1, 0x0

    aget-object v1, p1, v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 36
    goto :goto_1

    .line 27
    :cond_2
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 29
    :cond_3
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 30
    const/4 v1, 0x0

    aget-object v1, p1, v1

    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    invoke-virtual {v2}, Lcom/sec/factory/modules/ModulePower;->readFuelGaugeSOC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 33
    :cond_4
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFuelgaic;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 17
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
