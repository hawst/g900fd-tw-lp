.class public Lcom/sec/factory/aporiented/athandler/AtWifitest;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtWifitest.java"


# static fields
.field private static final ACTION_WIFI_STRESS_TEST:Ljava/lang/String; = "com.sec.factory.aporiented.athandler.AtWifitest.WIFI_STRESS_TEST"

.field private static final ACTION_WIFI_STRESS_TEST_REMOTE:Ljava/lang/String; = "remote"

.field private static final ACTION_WIFI_STRESS_TEST_RESULT:Ljava/lang/String; = "result"

.field private static isReceivedATCommands:Z


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mWiFiStressTestReceiver:Landroid/content/BroadcastReceiver;

.field private mWlanTest:I

.field private savedArgu:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->isReceivedATCommands:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 107
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;-><init>(Lcom/sec/factory/aporiented/athandler/AtWifitest;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWiFiStressTestReceiver:Landroid/content/BroadcastReceiver;

    .line 24
    const-string v0, "WIFITEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CMD_NAME:Ljava/lang/String;

    .line 25
    const-string v0, "AtWifitest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    .line 26
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->NUM_ARGS:I

    .line 27
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 28
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->startReceiver()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 107
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;-><init>(Lcom/sec/factory/aporiented/athandler/AtWifitest;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWiFiStressTestReceiver:Landroid/content/BroadcastReceiver;

    .line 33
    const-string v0, "WIFITEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CMD_NAME:Ljava/lang/String;

    .line 34
    const-string v0, "AtWifitest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    .line 35
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->NUM_ARGS:I

    .line 36
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 37
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->startReceiver()V

    .line 38
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 16
    sget-boolean v0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->isReceivedATCommands:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/factory/aporiented/athandler/AtWifitest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtWifitest;
    .param p1, "x1"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/factory/aporiented/athandler/AtWifitest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtWifitest;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->savedArgu:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/factory/aporiented/athandler/AtWifitest;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtWifitest;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWiFiStressTestReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private startWiFiStressTestReceiver()V
    .locals 3

    .prologue
    .line 101
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startWiFiStressTestReceiver"

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 103
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.factory.aporiented.athandler.AtWifitest.WIFI_STRESS_TEST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWiFiStressTestReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 105
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    const-string v1, ""

    .line 149
    .local v1, "command":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, p1

    if-ge v3, v5, :cond_0

    .line 150
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 153
    :cond_0
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "command : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v4, 0x0

    .line 156
    .local v4, "resData":Ljava/lang/String;
    array-length v6, p1

    sget-boolean v5, Lcom/sec/factory/aporiented/athandler/AtWifitest;->STAGE_PARALLEL:Z

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->NUM_ARGS:I

    add-int/lit8 v5, v5, 0x1

    :goto_1
    if-eq v6, v5, :cond_3

    array-length v6, p1

    sget-boolean v5, Lcom/sec/factory/aporiented/athandler/AtWifitest;->STAGE_PARALLEL:Z

    if-eqz v5, :cond_2

    const/16 v5, 0xd

    :goto_2
    if-eq v6, v5, :cond_3

    .line 158
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 334
    .end local v3    # "i":I
    :goto_3
    monitor-exit p0

    return-object v5

    .line 156
    .restart local v3    # "i":I
    :cond_1
    :try_start_1
    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->NUM_ARGS:I

    goto :goto_1

    :cond_2
    const/16 v5, 0xc

    goto :goto_2

    .line 162
    :cond_3
    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "3"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "9"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "9"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 163
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->runWiFiStressTest()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 164
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->startWiFiStressTestReceiver()V

    :goto_4
    move-object v5, v4

    .line 169
    goto :goto_3

    .line 166
    :cond_4
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 172
    :cond_5
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mWlanTest: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/factory/aporiented/athandler/AtWifitest;->isReceivedATCommands:Z

    .line 175
    iget v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I

    if-nez v5, :cond_9

    .line 176
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "2"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 178
    :cond_6
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {p0, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .end local v3    # "i":I
    :cond_7
    :goto_5
    move-object v5, v4

    .line 334
    goto/16 :goto_3

    .line 182
    .restart local v3    # "i":I
    :cond_8
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->savedArgu:[Ljava/lang/String;

    .line 183
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "i":I
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 184
    .local v3, "i":Landroid/content/Intent;
    const-string v5, "com.sec.android.app.wlantest"

    const-string v6, "com.sec.android.app.wlantest.WlanTest"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    const/high16 v5, 0x10000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 187
    const-string v5, "MODE"

    const-string v6, "auto"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->context:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->getCmdType()I

    move-result v5

    if-nez v5, :cond_7

    .line 191
    const-string v4, "WAIT"

    goto :goto_5

    .line 197
    .local v3, "i":I
    :cond_9
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->getCmdType()I

    move-result v5

    if-nez v5, :cond_a

    .line 198
    const-string v4, "WAIT"

    .line 201
    :cond_a
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 203
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : closeDut"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const/4 v5, 0x1

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    .line 205
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 147
    .end local v1    # "command":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "resData":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 207
    .restart local v1    # "command":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "resData":Ljava/lang/String;
    :cond_b
    const/4 v5, 0x3

    :try_start_2
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 209
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : openDut"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const/4 v5, 0x0

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 212
    :cond_c
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "2"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 214
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : unloadNormalFW"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const/16 v5, 0x13

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 217
    :cond_d
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "1"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "3"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 219
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : loadNormalFW"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/16 v5, 0x12

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 222
    :cond_e
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 224
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : channelSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v5, 0x6

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 227
    :cond_f
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 229
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : dataRateSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const/4 v5, 0x2

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 232
    :cond_10
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "2"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 234
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : preambleSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const/4 v5, 0x5

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 237
    :cond_11
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "3"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 239
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : txGainSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v5, 0x7

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 242
    :cond_12
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "4"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 244
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : payloadLengthSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const/16 v5, 0x9

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 247
    :cond_13
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "5"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 249
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : burstIntervalSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/16 v5, 0x8

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 252
    :cond_14
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "6"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 254
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : BandSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const/16 v5, 0x10

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 257
    :cond_15
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "7"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 259
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : BandwidthSetting"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const/16 v5, 0x11

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 262
    :cond_16
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "2"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 264
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : stopTx"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/16 v5, 0xb

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 267
    :cond_17
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "2"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 269
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : startTx"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/16 v5, 0xa

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 272
    :cond_18
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "3"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 274
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : stopRx"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/16 v5, 0xd

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 277
    :cond_19
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "3"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 279
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : startRx"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const/16 v5, 0xc

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 282
    :cond_1a
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "4"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 284
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : rxFrameReceiveError"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/16 v5, 0xe

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 287
    :cond_1b
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "4"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 289
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : rxFrameReceiveGood"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const/16 v5, 0xf

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 292
    :cond_1c
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "4"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "2"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 294
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : GetRSSI"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/16 v5, 0x17

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 297
    :cond_1d
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "6"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 299
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : joinAP"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const/16 v5, 0x14

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 302
    :cond_1e
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "6"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "1"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 304
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : joinAP with 2GHz"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const/16 v5, 0x15

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 307
    :cond_1f
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "6"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "2"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_20

    .line 309
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : joinAP with 5GHz"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const/16 v5, 0x16

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 312
    :cond_20
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "7"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 314
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : Ant Select"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const/4 v5, 0x4

    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 317
    :cond_21
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "0"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "9"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "9"

    aput-object v7, v5, v6

    invoke-virtual {p0, p1, v5}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 319
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    const-string v7, "command : Set All settings"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const/4 v5, 0x3

    aget-object v2, p1, v5

    .line 321
    .local v2, "dataArgu":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/factory/aporiented/athandler/AtWifitest;->STAGE_PARALLEL:Z

    if-eqz v5, :cond_22

    array-length v0, p1

    .line 322
    .local v0, "cmdLength":I
    :goto_6
    const/4 v3, 0x4

    :goto_7
    if-ge v3, v0, :cond_23

    .line 323
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 324
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 322
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 321
    .end local v0    # "cmdLength":I
    :cond_22
    array-length v5, p1

    add-int/lit8 v0, v5, -0x1

    goto :goto_6

    .line 326
    .restart local v0    # "cmdLength":I
    :cond_23
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "handleCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dataArgu : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/16 v5, 0x18

    invoke-virtual {p0, v5, v2}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->sendIntent(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 330
    .end local v0    # "cmdLength":I
    .end local v2    # "dataArgu":Ljava/lang/String;
    :cond_24
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    goto/16 :goto_5
.end method

.method public runWiFiStressTest()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 128
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "runWiFiStressTest"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "FACTORY_TEST_UNDER_USER_BINARY"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Start WiFiStressTest"

    invoke-static {v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 134
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.factorykeystring"

    const-string v3, "com.sec.android.app.status.WiFiStressTest"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v2, "remote"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 136
    const/high16 v2, 0x34000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 138
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->context:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 142
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected sendIntent(ILjava/lang/String;)V
    .locals 3
    .param p1, "cmdID"    # I
    .param p2, "argu"    # Ljava/lang/String;

    .prologue
    .line 338
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWlanTest:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 339
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.wlantest.WIFI_TEST_INDICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 340
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "CMDID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 341
    const-string v1, "S_DATA"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 344
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public startReceiver()V
    .locals 7

    .prologue
    .line 50
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-nez v4, :cond_0

    .line 51
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 52
    .local v3, "mFilter":Landroid/content/IntentFilter;
    const-string v4, "com.sec.android.app.wlantest.WIFI_TEST_RESPONSE"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 53
    const-string v4, "android.intent.action.WIFI_DRIVER_INDICATION"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 54
    new-instance v4, Lcom/sec/factory/aporiented/athandler/AtWifitest$1;

    invoke-direct {v4, p0}, Lcom/sec/factory/aporiented/athandler/AtWifitest$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtWifitest;)V

    iput-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 85
    new-instance v1, Landroid/os/HandlerThread;

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    invoke-direct {v1, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 86
    .local v1, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 87
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    .line 88
    .local v2, "looper":Landroid/os/Looper;
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 90
    .local v0, "handler":Landroid/os/Handler;
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 92
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Success startReceiver"

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .end local v0    # "handler":Landroid/os/Handler;
    .end local v1    # "handlerThread":Landroid/os/HandlerThread;
    .end local v2    # "looper":Landroid/os/Looper;
    .end local v3    # "mFilter":Landroid/content/IntentFilter;
    :cond_0
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Failed startReceiver"

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method protected stopReceiver()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "stopReceiver"

    invoke-static {v0, v1}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 46
    :cond_0
    return-void
.end method
