.class public Lcom/sec/factory/aporiented/DIServiceConnectionManager;
.super Ljava/lang/Object;
.source "DIServiceConnectionManager.java"


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "DIServiceConnectionManager"

.field private static final CMD_INDEX_BASE:I = 0x3e8

.field public static final KEY_STRING_COMMAND:Ljava/lang/String; = "command"

.field public static final KEY_STRING_NV_INDEX:Ljava/lang/String; = "nv_index"

.field public static final KEY_STRING_NV_VALUE:Ljava/lang/String; = "nv_value"

.field public static final KEY_STRING_TYPE:Ljava/lang/String; = "type"

.field public static final WHAT_INIT_CMD:I = 0x3e9

.field public static final WHAT_REQUEST_CMD:I = 0x3ea

.field public static final WHAT_REQUEST_GET_NV:I = 0x3ec

.field public static final WHAT_REQUEST_SET_NV:I = 0x3eb

.field public static final WHAT_RESPONSE_CMD:I = 0x3ed

.field public static final WHAT_RESPONSE_GET_NV:I = 0x3ef

.field public static final WHAT_RESPONSE_SET_NV:I = 0x3ee


# instance fields
.field private mContext:Landroid/content/Context;

.field protected mDIAtCommandConnection:Landroid/content/ServiceConnection;

.field private mIncomingMessenger:Landroid/os/Messenger;

.field private mIncommingHandler:Landroid/os/Handler;

.field private mOutgoingMessenger:Landroid/os/Messenger;

.field private mWriter:Lcom/sec/factory/aporiented/ResponseWriter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 40
    iput-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;

    .line 41
    iput-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 95
    new-instance v0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/DIServiceConnectionManager$2;-><init>(Lcom/sec/factory/aporiented/DIServiceConnectionManager;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mDIAtCommandConnection:Landroid/content/ServiceConnection;

    .line 46
    iput-object p1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 49
    new-instance v0, Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;

    iget-object v1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/factory/aporiented/DIServiceConnectionManager$1;-><init>(Lcom/sec/factory/aporiented/DIServiceConnectionManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mIncommingHandler:Landroid/os/Handler;

    .line 92
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mIncommingHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mIncomingMessenger:Landroid/os/Messenger;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/DIServiceConnectionManager;)Lcom/sec/factory/aporiented/ResponseWriter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/factory/aporiented/DIServiceConnectionManager;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/factory/aporiented/DIServiceConnectionManager;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;

    return-object p1
.end method


# virtual methods
.method public connectToDIAtCommandService()V
    .locals 4

    .prologue
    .line 110
    const-string v1, "DIServiceConnectionManager"

    const-string v2, "connectToDIAtCommandService"

    invoke-static {v1, v2}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 112
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.dizoom"

    const-string v2, "com.samsung.dizoom.atservice.DIAtCommandService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 115
    iget-object v1, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mDIAtCommandConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 116
    return-void
.end method

.method public send(IBB)Z
    .locals 7
    .param p1, "what"    # I
    .param p2, "nvIndex"    # B
    .param p3, "nvValue"    # B

    .prologue
    .line 149
    const-string v3, "DIServiceConnectionManager"

    const-string v4, "send"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "what:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", nvIndex:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", nvValue:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v3, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 153
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 154
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "nv_index"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 155
    const-string v3, "nv_value"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 157
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 158
    .local v1, "msg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->what:I

    .line 159
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 160
    iget-object v3, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mIncomingMessenger:Landroid/os/Messenger;

    iput-object v3, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 163
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_0
    const/4 v3, 0x1

    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "req":Landroid/os/Bundle;
    :goto_1
    return v3

    .line 164
    .restart local v1    # "msg":Landroid/os/Message;
    .restart local v2    # "req":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Landroid/os/RemoteException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 168
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "req":Landroid/os/Bundle;
    :cond_0
    const-string v3, "DIServiceConnectionManager"

    const-string v4, "send"

    const-string v5, "No connection with DIAtCommandService"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public send(ILjava/lang/String;)Z
    .locals 7
    .param p1, "what"    # I
    .param p2, "cmd"    # Ljava/lang/String;

    .prologue
    .line 123
    const-string v3, "DIServiceConnectionManager"

    const-string v4, "send"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "what:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cmd:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v3, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 127
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 128
    .local v2, "req":Landroid/os/Bundle;
    const-string v3, "command"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v3, "type"

    const-string v4, "uart"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 132
    .local v1, "msg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->what:I

    .line 133
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 134
    iget-object v3, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mIncomingMessenger:Landroid/os/Messenger;

    iput-object v3, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 137
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->mOutgoingMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    const/4 v3, 0x1

    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "req":Landroid/os/Bundle;
    :goto_1
    return v3

    .line 138
    .restart local v1    # "msg":Landroid/os/Message;
    .restart local v2    # "req":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Landroid/os/RemoteException;
    invoke-static {v0}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 142
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "req":Landroid/os/Bundle;
    :cond_0
    const-string v3, "DIServiceConnectionManager"

    const-string v4, "send"

    const-string v5, "No connection with DIAtCommandService"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public send(Ljava/lang/String;)Z
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 119
    const/16 v0, 0x3ea

    invoke-virtual {p0, v0, p1}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->send(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method
