.class public Lcom/sec/factory/aporiented/athandler/AtKey;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtKey.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 13
    const-string v0, "KEY"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->CMD_NAME:Ljava/lang/String;

    .line 14
    const-string v0, "AtKey"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->CLASS_NAME:Ljava/lang/String;

    .line 16
    invoke-static {}, Lcom/sec/factory/support/FtUtil;->isFactoryAppAPO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->NUM_ARGS:I

    .line 21
    :goto_0
    return-void

    .line 19
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->NUM_ARGS:I

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 26
    monitor-enter p0

    const/4 v1, 0x0

    .line 28
    .local v1, "resData":Ljava/lang/String;
    :try_start_0
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtKey;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 29
    const-string v3, "wrong"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :goto_1
    monitor-exit p0

    return-object v3

    .line 28
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->NUM_ARGS:I

    goto :goto_0

    .line 32
    :cond_1
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtKey;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/os/FactoryTest;->isAutomaticTestMode(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v5, :cond_2

    .line 33
    const/4 v3, 0x0

    goto :goto_1

    .line 36
    :cond_2
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtKey;->getCmdType()I

    move-result v3

    if-nez v3, :cond_5

    .line 37
    const/4 v3, 0x0

    aget-object v3, p1, v3

    const-string v4, "00"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 38
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKey;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v4, 0x1

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleDFT;->DftKey(Ljava/lang/String;)V

    .line 44
    :cond_3
    :goto_2
    const/4 v3, 0x0

    aget-object v3, p1, v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    move-result v2

    .line 45
    .local v2, "stateValue":S
    const/4 v3, 0x1

    aget-object v3, p1, v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Short;->parseShort(Ljava/lang/String;I)S

    move-result v0

    .line 46
    .local v0, "keycodeValue":S
    mul-int/lit16 v3, v2, 0x100

    add-int/2addr v3, v0

    int-to-short v3, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 47
    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKey;->setResultType(I)V

    .end local v0    # "keycodeValue":S
    .end local v2    # "stateValue":S
    :goto_3
    move-object v3, v1

    .line 53
    goto :goto_1

    .line 39
    :cond_4
    const/4 v3, 0x0

    aget-object v3, p1, v3

    const-string v4, "01"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 41
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKey;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v4, 0x1

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleDFT;->DftKeyHold(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 26
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 49
    :cond_5
    :try_start_2
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKey;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleDFT;->DftKey(Ljava/lang/String;)V

    .line 50
    const-string v1, "OK\r\n"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method
