.class public Lcom/sec/factory/aporiented/athandler/AtWifiidrw;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtWifiidrw.java"


# static fields
.field public static final ACTION_WIFI_ID_RESPONSE:Ljava/lang/String; = "com.sec.android.app.wlantest.WIFI_ID_RESPONSE"

.field private static subString:Ljava/lang/String;


# instance fields
.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 70
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 22
    const-string v0, "WIFIIDRW"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->CMD_NAME:Ljava/lang/String;

    .line 23
    const-string v0, "AtWifiidrw"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->CLASS_NAME:Ljava/lang/String;

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->NUM_ARGS:I

    .line 25
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 26
    const-string v0, "boot"

    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 70
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 47
    const-string v0, "WIFIIDRW"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->CMD_NAME:Ljava/lang/String;

    .line 48
    const-string v0, "AtWifiidrw"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->CLASS_NAME:Ljava/lang/String;

    .line 49
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->NUM_ARGS:I

    .line 50
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 59
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/aporiented/athandler/AtWifiidrw;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtWifiidrw;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method


# virtual methods
.method public addColon(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "aAddr"    # Ljava/lang/String;

    .prologue
    .line 168
    const-string v1, ""

    .line 170
    .local v1, "temp":Ljava/lang/String;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    .line 171
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_1

    .line 172
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v0, 0x2

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    .line 175
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 171
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "i":I
    :cond_1
    return-object v1
.end method

.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 116
    monitor-enter p0

    const/4 v0, 0x0

    .line 118
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 119
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 159
    :goto_1
    monitor-exit p0

    return-object v1

    .line 118
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->NUM_ARGS:I

    goto :goto_0

    .line 122
    :cond_1
    const/4 v1, 0x0

    aget-object v1, p1, v1

    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->subString:Ljava/lang/String;

    .line 124
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->startReceiver()V

    .line 126
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->readWifiId()V

    .line 130
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->getCmdType()I

    move-result v1

    if-nez v1, :cond_2

    .line 131
    const-string v0, "WAIT"

    :cond_2
    :goto_2
    move-object v1, v0

    .line 159
    goto :goto_1

    .line 134
    :cond_3
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "2"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135
    const/4 v1, 0x1

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_5

    .line 136
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->getCmdType()I

    move-result v1

    if-nez v1, :cond_4

    .line 137
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 139
    :cond_4
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 142
    :cond_5
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->startReceiver()V

    .line 143
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->writeWifiId(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->getCmdType()I

    move-result v1

    if-nez v1, :cond_2

    .line 151
    const-string v0, "WAIT"

    goto :goto_2

    .line 156
    :cond_6
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 116
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "aFile"    # Ljava/lang/String;

    .prologue
    .line 163
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 65
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 66
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.wlantest.WIFI_ID_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifiidrw;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    return-void
.end method
