.class public Lcom/sec/factory/aporiented/athandler/AtSpkstest;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtSpkstest.java"


# static fields
.field protected static final AUDIO_PARAMETER_CALIBRATION:Ljava/lang/String; = "ret="

.field protected static final AUDIO_PARAMETER_IMPENDANCE:Ljava/lang/String; = "impedance="

.field private static volume_index:I


# instance fields
.field private final SLEEP_TIME:I

.field private mAudioManager:Landroid/media/AudioManager;

.field mContext:Landroid/content/Context;

.field moduleCommon:Lcom/sec/factory/modules/ModuleCommon;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 25
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mContext:Landroid/content/Context;

    .line 27
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->moduleCommon:Lcom/sec/factory/modules/ModuleCommon;

    .line 29
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->SLEEP_TIME:I

    .line 40
    const-string v0, "SPKSTEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CMD_NAME:Ljava/lang/String;

    .line 41
    const-string v0, "AtSpkstest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    .line 42
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->NUM_ARGS:I

    .line 43
    return-void
.end method

.method private getParameter(Ljava/lang/String;Ljava/lang/String;)D
    .locals 16
    .param p1, "keys"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 46
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getParameter"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "keys: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getParameter"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "target: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 50
    :cond_0
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 75
    :cond_1
    :goto_0
    return-wide v8

    .line 53
    :cond_2
    const-string v7, "-1"

    .line 54
    .local v7, "param":Ljava/lang/String;
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    .line 56
    .local v8, "rms":D
    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 57
    const-string v2, ";"

    .line 58
    .local v2, "DELIMITER":Ljava/lang/String;
    const-string v12, ";"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 59
    .local v11, "values":[Ljava/lang/String;
    move-object v3, v11

    .local v3, "arr$":[Ljava/lang/String;
    array-length v5, v3

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v10, v3, v4

    .line 60
    .local v10, "value":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 59
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 63
    :cond_3
    const-string v12, ""

    move-object/from16 v0, p2

    invoke-virtual {v10, v0, v12}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 67
    .end local v2    # "DELIMITER":Ljava/lang/String;
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v10    # "value":Ljava/lang/String;
    .end local v11    # "values":[Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getParameter"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "target param: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    if-eqz v7, :cond_1

    .line 70
    :try_start_0
    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    float-to-double v8, v12

    goto :goto_0

    .line 72
    :catch_0
    move-exception v6

    .line 73
    .local v6, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v6}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 17
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 80
    monitor-enter p0

    const/4 v12, 0x0

    .line 82
    .local v12, "resData":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->context:Landroid/content/Context;

    const-string v14, "audio"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mAudioManager:Landroid/media/AudioManager;

    .line 84
    move-object/from16 v0, p1

    array-length v14, v0

    sget-boolean v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->STAGE_PARALLEL:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->NUM_ARGS:I

    add-int/lit8 v13, v13, 0x1

    :goto_0
    if-eq v14, v13, :cond_1

    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 225
    :goto_1
    monitor-exit p0

    return-object v13

    .line 84
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->NUM_ARGS:I

    goto :goto_0

    .line 88
    :cond_1
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->isPlayingSound()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 89
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 90
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const/16 v14, 0xc8

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleCommon;->sleepThread(I)V

    .line 93
    :cond_2
    const-string v13, "factory"

    const-string v14, "BINARY_TYPE"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 95
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 96
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 97
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 98
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const v14, 0x7f040002

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 99
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playSound"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "volume: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 172
    :cond_3
    :goto_2
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_12

    .line 175
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "setAudioPath"

    const-string v15, "setAudioPath"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 177
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 178
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/high16 v14, 0x7f040000

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 179
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playMedia"

    const-string v15, "playMedia"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-wide/16 v14, 0x64

    invoke-static {v14, v15}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 183
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "TFA"

    const-string v15, "TFA calibration start"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mAudioManager:Landroid/media/AudioManager;

    const-string v14, "TFA_DO_CALIBRATION"

    invoke-virtual {v13, v14}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 185
    .local v10, "mCalibration":Ljava/lang/String;
    const-string v13, "ret="

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->getParameter(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v2

    .line 186
    .local v2, "calibration":D
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "TFA"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "calibration : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-wide/16 v14, 0x0

    cmpl-double v13, v2, v14

    if-nez v13, :cond_11

    .line 189
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 193
    :goto_3
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    .line 221
    .end local v2    # "calibration":D
    .end local v10    # "mCalibration":Ljava/lang/String;
    :cond_4
    :goto_4
    if-nez v12, :cond_5

    .line 222
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseNA()Ljava/lang/String;

    move-result-object v12

    :cond_5
    move-object v13, v12

    .line 225
    goto/16 :goto_1

    .line 101
    :cond_6
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "2"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 102
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 103
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 104
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const v14, 0x7f040003

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 105
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playSound"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "volume: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 107
    :cond_7
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "3"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 108
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 109
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 110
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const v14, 0x7f040001

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 112
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playSound"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "volume: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 114
    :cond_8
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "4"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 116
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 117
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 118
    :cond_9
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "5"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 119
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 120
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 121
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const v14, 0x7f040001

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 123
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playSound"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "volume: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 125
    :cond_a
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "5"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 126
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 127
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 128
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const v14, 0x7f040003

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 129
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playSound"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "volume: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 131
    :cond_b
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "5"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "2"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 132
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 133
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 134
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const v14, 0x7f040002

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 135
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playSound"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "volume: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 137
    :cond_c
    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v13

    if-eqz v13, :cond_e

    .line 138
    const/4 v9, -0x1

    .line 140
    .local v9, "index":I
    const/4 v13, 0x2

    :try_start_2
    aget-object v13, p1, v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    .line 144
    :goto_5
    if-ltz v9, :cond_d

    const/16 v13, 0x9

    if-gt v9, v13, :cond_d

    .line 145
    mul-int/lit8 v13, v9, 0x5

    :try_start_3
    sput v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    .line 146
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 147
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 141
    :catch_0
    move-exception v4

    .line 142
    .local v4, "e":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "NumberFormatException"

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    .line 80
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    .end local v9    # "index":I
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 149
    .restart local v9    # "index":I
    :cond_d
    const/4 v13, 0x0

    :try_start_4
    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 151
    .end local v9    # "index":I
    :cond_e
    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v13

    if-eqz v13, :cond_10

    .line 152
    const/4 v9, -0x1

    .line 154
    .restart local v9    # "index":I
    const/4 v13, 0x2

    :try_start_5
    aget-object v13, p1, v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v9

    .line 158
    :goto_6
    if-ltz v9, :cond_f

    const/16 v13, 0x9

    if-gt v9, v13, :cond_f

    .line 159
    mul-int/lit8 v13, v9, 0x5

    add-int/lit8 v13, v13, 0x32

    :try_start_6
    sput v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    .line 160
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    sget v14, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolume(I)V

    .line 161
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 155
    :catch_1
    move-exception v4

    .line 156
    .restart local v4    # "e":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "NumberFormatException"

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 163
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :cond_f
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 165
    .end local v9    # "index":I
    :cond_10
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 166
    const/16 v13, 0x64

    sput v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->volume_index:I

    .line 167
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 168
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 191
    .restart local v2    # "calibration":D
    .restart local v10    # "mCalibration":Ljava/lang/String;
    :cond_11
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_3

    .line 194
    .end local v2    # "calibration":D
    .end local v10    # "mCalibration":Ljava/lang/String;
    :cond_12
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 197
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "setAudioPath"

    const-string v15, "setAudioPath"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleAudio;->setAudioPath(I)V

    .line 199
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->setStreamMusicVolumeMax()V

    .line 200
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/high16 v14, 0x7f040000

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Lcom/sec/factory/modules/ModuleAudio;->playMedia(IZ)V

    .line 201
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "playMedia"

    const-string v15, "playMedia"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-wide/16 v14, 0x64

    invoke-static {v14, v15}, Lcom/sec/factory/support/FtUtil$Sleep;->sleep(J)V

    .line 205
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "TFA"

    const-string v15, "TFA_GET_IMPEDANCE start"

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mAudioManager:Landroid/media/AudioManager;

    const-string v14, "TFA_GET_IMPEDANCE"

    invoke-virtual {v13, v14}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 207
    .local v11, "mImpedance":Ljava/lang/String;
    const-string v13, "impedance="

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->getParameter(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v6

    .line 208
    .local v6, "impendance":D
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "TFA"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "impedance : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v13, "#.###"

    invoke-direct {v5, v13}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 211
    .local v5, "format":Ljava/text/DecimalFormat;
    invoke-virtual {v5, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 213
    .local v8, "in_impendance":Ljava/lang/String;
    const-string v13, "0"

    invoke-virtual {v13, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_13

    .line 214
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 218
    :goto_7
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    invoke-virtual {v13}, Lcom/sec/factory/modules/ModuleAudio;->stopMedia()V

    goto/16 :goto_4

    .line 216
    :cond_13
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v8}, Lcom/sec/factory/aporiented/athandler/AtSpkstest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v12

    goto :goto_7
.end method
