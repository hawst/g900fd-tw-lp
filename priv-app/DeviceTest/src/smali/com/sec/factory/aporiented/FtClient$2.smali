.class Lcom/sec/factory/aporiented/FtClient$2;
.super Ljava/lang/Object;
.source "FtClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/FtClient;->sendBootCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FtClient;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v3, 0x1

    .line 272
    sget-boolean v0, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->misSecphoneReady:Z

    if-eqz v0, :cond_4

    .line 273
    const-string v0, "FtClient"

    const-string v1, "sendBootCompleted"

    const-string v2, "misSecphoneReady"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v0, "SUPPORT_DUAL_STANBY"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUPPORT_2ND_CP"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # operator-- for: Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$610(Lcom/sec/factory/aporiented/FtClient;)I

    .line 278
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->checkPhoneVer()Z
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$700(Lcom/sec/factory/aporiented/FtClient;)Z

    move-result v0

    if-ne v3, v0, :cond_1

    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->checkPhone2Ver()Z
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$800(Lcom/sec/factory/aporiented/FtClient;)Z

    move-result v0

    if-ne v3, v0, :cond_1

    .line 279
    const-string v0, "FtClient"

    const-string v1, "sendBootCompleted"

    const-string v2, "CP1/CP2 version name available"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;

    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {v0, v1}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;-><init>(Landroid/content/Context;)V

    .line 319
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$600(Lcom/sec/factory/aporiented/FtClient;)I

    move-result v0

    if-lez v0, :cond_2

    .line 289
    const-string v0, "FtClient"

    const-string v1, "sendBootCompleted"

    const-string v2, "Waiting for CP version name"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # operator-- for: Lcom/sec/factory/aporiented/FtClient;->mCPverWaitCount:I
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$610(Lcom/sec/factory/aporiented/FtClient;)I

    .line 293
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$000(Lcom/sec/factory/aporiented/FtClient;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 298
    :cond_2
    const-string v0, "FtClient"

    const-string v1, "sendBootCompleted"

    const-string v2, "40s Time Out for CP version name send ANYWAY "

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;

    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {v0, v1}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 308
    :cond_3
    new-instance v0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;

    iget-object v1, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {v0, v1}, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 309
    :cond_4
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mCount:I
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$900(Lcom/sec/factory/aporiented/FtClient;)I

    move-result v0

    if-lez v0, :cond_5

    .line 310
    const-string v0, "FtClient"

    const-string v1, "sendBootCompleted"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "misSecphonReady= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/factory/entry/ProtectedFactoryTestBroadcastReceiver;->misSecphoneReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # operator-- for: Lcom/sec/factory/aporiented/FtClient;->mCount:I
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$910(Lcom/sec/factory/aporiented/FtClient;)I

    .line 314
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$2;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$000(Lcom/sec/factory/aporiented/FtClient;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 316
    :cond_5
    const-string v0, "FtClient"

    const-string v1, "sendBootCompletedrp"

    const-string v2, "80s Time Out "

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
