.class Lcom/sec/factory/aporiented/athandler/AtWifitest$2;
.super Landroid/content/BroadcastReceiver;
.source "AtWifitest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtWifitest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtWifitest;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 110
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "mAction":Ljava/lang/String;
    const-string v2, "com.sec.factory.aporiented.athandler.AtWifitest.WIFI_STRESS_TEST"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    const-string v2, "result"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "response":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtWifitest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "BroadcastReceiver.onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_SEMI_FUNCTION_TEST_RESULT response: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtWifitest;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    if-eqz v2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    iget-object v2, v2, Lcom/sec/factory/aporiented/athandler/AtWifitest;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    const-string v4, "3"

    invoke-virtual {v3, v4, v1}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 123
    .end local v1    # "response":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtWifitest$2;->this$0:Lcom/sec/factory/aporiented/athandler/AtWifitest;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtWifitest;->mWiFiStressTestReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v3}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->access$300(Lcom/sec/factory/aporiented/athandler/AtWifitest;)Landroid/content/BroadcastReceiver;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/factory/aporiented/athandler/AtWifitest;->stopReceiver(Landroid/content/BroadcastReceiver;)V

    .line 124
    return-void
.end method
