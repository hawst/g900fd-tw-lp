.class Lcom/sec/factory/aporiented/FtClient$1;
.super Landroid/content/BroadcastReceiver;
.source "FtClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/FtClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FtClient;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/factory/aporiented/FtClient$1;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "action":Ljava/lang/String;
    const-string v3, "FtClient"

    const-string v4, "mBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action= = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v3, "com.sec.factory.SEND_TO_RIL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 167
    const-string v3, "message"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "message":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 170
    const-string v3, "FtClient"

    const-string v4, "mBroadcastReceiver ACTION_SEND_TO_RIL"

    const-string v5, "Empty Message!"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    .end local v1    # "message":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 175
    .restart local v1    # "message":Ljava/lang/String;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient$1;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$000(Lcom/sec/factory/aporiented/FtClient;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/sec/factory/aporiented/FtClient$1$1;

    invoke-direct {v4, p0, v1}, Lcom/sec/factory/aporiented/FtClient$1$1;-><init>(Lcom/sec/factory/aporiented/FtClient$1;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "message":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 188
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    :try_start_2
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 189
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "path":Ljava/lang/String;
    const-string v3, "FtClient"

    const-string v4, "mBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_MEDIA_SCANNER_FINISHED = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 192
    const-string v3, "FtClient"

    const-string v4, "mBroadcastReceiver"

    const-string v5, "ACTION_MEDIA_SCANNER_FINISHED = Ignored"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.factory.entry.ACTION_GET_MEDIASCANNER_FINISHED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 197
    # getter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z
    invoke-static {}, Lcom/sec/factory/aporiented/FtClient;->access$100()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v3

    if-nez v3, :cond_4

    .line 198
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient$1;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->sendBootCompleted()V
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$200(Lcom/sec/factory/aporiented/FtClient;)V

    .line 201
    :cond_4
    const/4 v3, 0x1

    # setter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$102(Z)Z

    .line 203
    # getter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedCscModemSetting:Z
    invoke-static {}, Lcom/sec/factory/aporiented/FtClient;->access$300()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->isFirstBoot()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 204
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient$1;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->sendResetCompleted()V
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$400(Lcom/sec/factory/aporiented/FtClient;)V

    .line 205
    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->setFirstBoot()Z

    .line 210
    :goto_1
    const-string v3, "SUPPORT_BOOST_MEDIASCAN"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    const-string v3, "FtClient"

    const-string v4, "onReceived-ACTION_MEDIA_SCANNER_FINISHED"

    const-string v5, "SUPPORT_BOOST_MEDIASCAN"

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-static {p1}, Lcom/sec/factory/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModulePower;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModulePower;->doMediaScanWakeLock(Z)V

    goto/16 :goto_0

    .line 207
    :cond_5
    const-string v3, "FtClient"

    const-string v4, "mBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mIsReceivedCscModemSetting = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedCscModemSetting:Z
    invoke-static {}, Lcom/sec/factory/aporiented/FtClient;->access$300()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 214
    .end local v2    # "path":Ljava/lang/String;
    :cond_6
    const-string v3, "android.intent.action.CSC_MODEM_SETTING"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "android.intent.action.CSC_MODEM_SETTING_FACTORY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_7
    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->isConnectionModeNone()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 215
    const/4 v3, 0x1

    # setter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedCscModemSetting:Z
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$302(Z)Z

    .line 216
    # getter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z
    invoke-static {}, Lcom/sec/factory/aporiented/FtClient;->access$100()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->isFirstBoot()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 217
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient$1;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->sendResetCompleted()V
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$400(Lcom/sec/factory/aporiented/FtClient;)V

    .line 218
    invoke-static {p1}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->setFirstBoot()Z

    goto/16 :goto_0

    .line 220
    :cond_8
    const-string v3, "FtClient"

    const-string v4, "mBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mIsReceivedMediaScanningFinishedExternalMem = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mIsReceivedMediaScanningFinishedExternalMem:Z
    invoke-static {}, Lcom/sec/factory/aporiented/FtClient;->access$100()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 222
    :cond_9
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "JIG_CONNECTION_CHECK"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "JIG"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 224
    iget-object v3, p0, Lcom/sec/factory/aporiented/FtClient$1;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->writeSOC()V
    invoke-static {v3}, Lcom/sec/factory/aporiented/FtClient;->access$500(Lcom/sec/factory/aporiented/FtClient;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
