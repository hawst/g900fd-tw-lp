.class Lcom/sec/factory/aporiented/FailDumpService$2;
.super Landroid/os/Handler;
.source "FailDumpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/FailDumpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FailDumpService;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FailDumpService;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x3e8

    const/16 v6, 0x3ee

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 78
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 129
    :goto_0
    :pswitch_0
    const-string v1, "SUPPORT_MARVELL_RIL"

    invoke-static {v1}, Lcom/sec/factory/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$300(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$400(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p0, v6, v8, v9}, Lcom/sec/factory/aporiented/FailDumpService$2;->sendEmptyMessageDelayed(IJ)Z

    .line 132
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/factory/aporiented/FailDumpService;->access$302(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    .line 133
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.sec.FAILDUMP.DONE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/factory/aporiented/FailDumpService;->sendBroadcast(Landroid/content/Intent;)V

    .line 136
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/FailDumpService;->stopSelf()V

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$300(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$400(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mConnectionSuccess:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$500(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 142
    invoke-virtual {p0, v6, v8, v9}, Lcom/sec/factory/aporiented/FailDumpService$2;->sendEmptyMessageDelayed(IJ)Z

    .line 143
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/factory/aporiented/FailDumpService;->access$302(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    .line 144
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    if-eqz v1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.sec.FAILDUMP.DONE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/factory/aporiented/FailDumpService;->sendBroadcast(Landroid/content/Intent;)V

    .line 147
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/FailDumpService;->stopSelf()V

    .line 160
    :cond_1
    :goto_1
    return-void

    .line 80
    :pswitch_1
    const-string v1, "FailDumpService"

    const-string v2, "faildumphandler : ACQUIRE_WAKE_LOCK "

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->doWakeLock(Z)V
    invoke-static {v1, v5}, Lcom/sec/factory/aporiented/FailDumpService;->access$100(Lcom/sec/factory/aporiented/FailDumpService;Z)V

    goto :goto_1

    .line 84
    :pswitch_2
    const-string v1, "FailDumpService"

    const-string v2, "faildumphandler : RELEASE_WAKE_LOCK "

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # invokes: Lcom/sec/factory/aporiented/FailDumpService;->doWakeLock(Z)V
    invoke-static {v1, v4}, Lcom/sec/factory/aporiented/FailDumpService;->access$100(Lcom/sec/factory/aporiented/FailDumpService;Z)V

    goto :goto_1

    .line 88
    :pswitch_3
    const-string v1, "FailDumpService"

    const-string v2, "MODEMLOG_DONE Success, "

    const-string v3, "isEOS2=true"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v2, "Get modem log done - success"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 91
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v5}, Lcom/sec/factory/aporiented/FailDumpService;->access$202(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    goto/16 :goto_0

    .line 94
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 96
    .local v0, "error":I
    if-nez v0, :cond_2

    .line 97
    const-string v1, "FailDumpService"

    const-string v2, "MODEMLOG_DONE Success "

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v2, "Get modem log done - success"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 106
    :goto_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v5}, Lcom/sec/factory/aporiented/FailDumpService;->access$202(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    goto/16 :goto_0

    .line 101
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v2, "Get modem log done - fail"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 103
    const-string v1, "FailDumpService"

    const-string v2, "MODEMLOG_DONE Fail "

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 109
    .end local v0    # "error":I
    :pswitch_5
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v2, "Get AP log done - success"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 111
    const-string v1, "FailDumpService"

    const-string v2, "AP LOG Success"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v5}, Lcom/sec/factory/aporiented/FailDumpService;->access$302(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    goto/16 :goto_0

    .line 115
    :pswitch_6
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v2, "Get AP log done - fail"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 117
    const-string v1, "FailDumpService"

    const-string v2, "AP LOG Fail"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :pswitch_7
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    const-string v2, "Get Copy log done - success"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 122
    const-string v1, "FailDumpService"

    const-string v2, "QUERY_COPY_LOG_DONE"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1, v5}, Lcom/sec/factory/aporiented/FailDumpService;->access$402(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    goto/16 :goto_0

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$300(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$200(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # getter for: Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1}, Lcom/sec/factory/aporiented/FailDumpService;->access$400(Lcom/sec/factory/aporiented/FailDumpService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    invoke-virtual {p0, v6, v8, v9}, Lcom/sec/factory/aporiented/FailDumpService$2;->sendEmptyMessageDelayed(IJ)Z

    .line 151
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mAPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/factory/aporiented/FailDumpService;->access$302(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    .line 152
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mCPLogsDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/factory/aporiented/FailDumpService;->access$202(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    .line 153
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    # setter for: Lcom/sec/factory/aporiented/FailDumpService;->mCopyLogDoneFlag:Z
    invoke-static {v1, v4}, Lcom/sec/factory/aporiented/FailDumpService;->access$402(Lcom/sec/factory/aporiented/FailDumpService;Z)Z

    .line 154
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    if-eqz v1, :cond_1

    .line 155
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.sec.FAILDUMP.DONE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/factory/aporiented/FailDumpService;->sendBroadcast(Landroid/content/Intent;)V

    .line 157
    iget-object v1, p0, Lcom/sec/factory/aporiented/FailDumpService$2;->this$0:Lcom/sec/factory/aporiented/FailDumpService;

    invoke-virtual {v1}, Lcom/sec/factory/aporiented/FailDumpService;->stopSelf()V

    goto/16 :goto_1

    .line 78
    :pswitch_data_0
    .packed-switch 0x3ed
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
