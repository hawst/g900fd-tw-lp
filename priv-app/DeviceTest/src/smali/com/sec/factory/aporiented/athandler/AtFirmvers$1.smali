.class Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;
.super Landroid/os/Handler;
.source "AtFirmvers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/athandler/AtFirmvers;->handleCommand([Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtFirmvers;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "tspFirmwareUpdateHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "msg.what = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_OK:I

    if-ne v0, v1, :cond_2

    .line 78
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "tspFirmwareUpdateHandler"

    const-string v2, "TSP_WHAT_STATUS_OK"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget v0, p1, Landroid/os/Message;->arg1:I

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_UPDATE:I

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const-string v0, "PASS"

    # setter for: Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 97
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "tspFirmwareUpdateHandler"

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    return-void

    .line 85
    :cond_1
    const-string v0, "FAIL"

    # setter for: Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->access$002(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 88
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NG:I

    if-ne v0, v1, :cond_3

    .line 89
    const-string v0, "FAIL"

    # setter for: Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "tspFirmwareUpdateHandler"

    const-string v2, "TSP_WHAT_STATUS_NG"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_WHAT_STATUS_NA:I

    if-ne v0, v1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFirmvers;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "tspFirmwareUpdateHandler"

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    invoke-static {}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "FAIL"

    # setter for: Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->access$002(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
