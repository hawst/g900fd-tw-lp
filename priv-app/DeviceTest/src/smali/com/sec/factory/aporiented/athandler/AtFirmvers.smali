.class public Lcom/sec/factory/aporiented/athandler/AtFirmvers;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFirmvers.java"

# interfaces
.implements Lcom/validity/fingerprint/FingerprintCore$EventListener;


# static fields
.field private static final DONE_FAIL_TSP_UPGRADE:Ljava/lang/String; = "FAIL"

.field private static final DONE_PASS_TSP_UPGRADE:Ljava/lang/String; = "PASS"

.field private static final DOWNLOADING_TSP_UPGRADE:Ljava/lang/String; = "DOWNLOADING"

.field private static final NO_STATUS_TSP_UPGRADE:Ljava/lang/String; = "NG"

.field private static final TSP:I = 0x0

.field private static final TSP_FACTORY:I = 0x7

.field private static final TSP_UMS:I = 0x4

.field private static mTSPUpdateStatus:Ljava/lang/String;


# instance fields
.field protected mDetected:Z

.field private mFingerprint:Lcom/validity/fingerprint/Fingerprint;

.field mTspFirmwareUpdateHandler:Landroid/os/Handler;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field public tspUpdateResult:Ljava/lang/String;

.field public tspUpdateStatus:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "NG"

    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->tspUpdateStatus:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->tspUpdateResult:Ljava/lang/String;

    .line 44
    const-string v0, "FIRMVERS"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CMD_NAME:Ljava/lang/String;

    .line 45
    const-string v0, "AtFirmvers"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    .line 46
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->NUM_ARGS:I

    .line 47
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 23
    sput-object p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 17
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 52
    monitor-enter p0

    const/4 v6, 0x0

    .line 54
    .local v6, "resData":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p1

    array-length v14, v0

    sget-boolean v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->STAGE_PARALLEL:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->NUM_ARGS:I

    add-int/lit8 v13, v13, 0x1

    :goto_0
    if-eq v14, v13, :cond_1

    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 250
    :goto_1
    monitor-exit p0

    return-object v13

    .line 54
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->NUM_ARGS:I

    goto :goto_0

    .line 58
    :cond_1
    new-instance v13, Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-direct {v13, v14, v0}, Lcom/validity/fingerprint/Fingerprint;-><init>(Landroid/content/Context;Lcom/validity/fingerprint/FingerprintCore$EventListener;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    .line 61
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 62
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNA()Ljava/lang/String;

    move-result-object v6

    :goto_2
    move-object v13, v6

    .line 250
    goto :goto_1

    .line 65
    :cond_2
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNA()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 69
    :cond_3
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 70
    const-string v13, "DOWNLOADING"

    sput-object v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;

    .line 71
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTspFirmwareUpdateHandler:Landroid/os/Handler;

    .line 72
    new-instance v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->context:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFirmvers;Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTspFirmwareUpdateHandler:Landroid/os/Handler;

    .line 101
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    sget v14, Lcom/sec/factory/modules/ModuleTouchScreen;->TSP_ID__FW_UPDATE:I

    const-string v15, "0"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTspFirmwareUpdateHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPResult(ILjava/lang/String;Landroid/os/Handler;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 103
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 105
    :cond_4
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 106
    const-string v13, "NG"

    sput-object v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 52
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 110
    :cond_5
    const/4 v13, 0x3

    :try_start_2
    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 111
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mTSPUpdateStatus:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 114
    :cond_6
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 117
    const-string v13, "factory"

    const-string v14, "BINARY_TYPE"

    invoke-static {v14}, Lcom/sec/factory/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 118
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->updateFirmware_melfas(I)V

    .line 120
    const-string v10, "RUNNING"

    .line 121
    .local v10, "status":Ljava/lang/String;
    const/16 v2, 0x14

    .local v2, "EXPIRED_COUNTER":I
    move v3, v2

    .line 123
    .end local v2    # "EXPIRED_COUNTER":I
    .local v3, "EXPIRED_COUNTER":I
    :goto_3
    const-string v13, "RUNNING"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1d

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->readStatus()Ljava/lang/String;

    move-result-object v10

    .line 125
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "EXPIRED_COUNTER":I
    .restart local v2    # "EXPIRED_COUNTER":I
    if-gez v3, :cond_7

    .line 126
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "Failed TSP update (Timeout 120s)"

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 128
    goto/16 :goto_2

    .line 131
    :cond_7
    const-string v13, "OK"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 132
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->readResult()Ljava/lang/String;

    move-result-object v7

    .line 133
    .local v7, "result":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "TSP update (result == "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    if-eqz v7, :cond_8

    const-string v13, "OK"

    invoke-virtual {v7, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 135
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 137
    :cond_8
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 139
    goto/16 :goto_2

    .line 140
    .end local v7    # "result":Ljava/lang/String;
    :cond_9
    const-string v13, "FAIL"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 141
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "Failed TSP update (Status == FAIL)"

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 143
    goto/16 :goto_2

    .line 147
    :cond_a
    const-wide/16 v14, 0x1388

    :try_start_3
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v3, v2

    .line 150
    .end local v2    # "EXPIRED_COUNTER":I
    .restart local v3    # "EXPIRED_COUNTER":I
    goto/16 :goto_3

    .line 148
    .end local v3    # "EXPIRED_COUNTER":I
    .restart local v2    # "EXPIRED_COUNTER":I
    :catch_0
    move-exception v5

    .line 149
    .local v5, "ie":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v3, v2

    .line 150
    .end local v2    # "EXPIRED_COUNTER":I
    .restart local v3    # "EXPIRED_COUNTER":I
    goto/16 :goto_3

    .line 153
    .end local v3    # "EXPIRED_COUNTER":I
    .end local v5    # "ie":Ljava/lang/InterruptedException;
    .end local v10    # "status":Ljava/lang/String;
    :cond_b
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleDevice;->firmwareDownload(I)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 154
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 156
    :cond_c
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 161
    :cond_d
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 162
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleDevice;->readModuleUpdateStatus(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 165
    :cond_e
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNA()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 169
    :cond_f
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "0"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNA()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 173
    :cond_10
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 174
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-virtual {v14}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPFirmwareVersionIC()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 177
    :cond_11
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_12

    .line 178
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleTouchScreen:Lcom/sec/factory/modules/ModuleTouchScreen;

    invoke-virtual {v14}, Lcom/sec/factory/modules/ModuleTouchScreen;->getTSPFirmwareVersionBinary()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 181
    :cond_12
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_13

    .line 182
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 185
    :cond_13
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "2"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_14

    .line 186
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleDevice;->readModuleBinVersion(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 190
    :cond_14
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "3"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_15

    .line 191
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 195
    :cond_15
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "4"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v13

    if-eqz v13, :cond_16

    .line 197
    :try_start_5
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v14, 0x5

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 198
    .local v9, "sensorHubFirmModule":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, p1, v13

    const/4 v14, 0x0

    aget-object v14, v9, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v6

    goto/16 :goto_2

    .line 199
    .end local v9    # "sensorHubFirmModule":[Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 200
    .local v4, "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    :try_start_6
    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 201
    goto/16 :goto_2

    .line 204
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_16
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "4"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v13

    if-eqz v13, :cond_17

    .line 206
    :try_start_7
    sget-object v13, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v14, 0x5

    invoke-virtual {v13, v14}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 207
    .local v8, "sensorHubFirmBin":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, p1, v13

    const/4 v14, 0x1

    aget-object v14, v8, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v6

    goto/16 :goto_2

    .line 208
    .end local v8    # "sensorHubFirmBin":[Ljava/lang/String;
    :catch_2
    move-exception v4

    .line 209
    .restart local v4    # "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    :try_start_8
    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 210
    goto/16 :goto_2

    .line 213
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_17
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "5"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "0"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_18

    .line 214
    const/4 v13, 0x0

    aget-object v13, p1, v13

    sget-object v14, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const/4 v15, 0x7

    invoke-virtual {v14, v15}, Lcom/sec/factory/modules/ModuleDevice;->readModuleFirmwareVersion(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 217
    :cond_18
    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "1"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "6"

    aput-object v15, v13, v14

    const/4 v14, 0x2

    const-string v15, "1"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1c

    .line 218
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    if-nez v13, :cond_19

    .line 219
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "mFingerprint is null"

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v13, v6

    .line 221
    goto/16 :goto_1

    .line 224
    :cond_19
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v13}, Lcom/validity/fingerprint/Fingerprint;->getVersion()Ljava/lang/String;

    move-result-object v12

    .line 225
    .local v12, "version":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mFingerprint.getVersion() is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    if-nez v12, :cond_1a

    .line 228
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "mFingerprint.getVersion() is null"

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v13, v6

    .line 230
    goto/16 :goto_1

    .line 233
    :cond_1a
    invoke-virtual {v12}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v12

    .line 235
    move-object v11, v12

    .line 236
    .local v11, "tempVersion":Ljava/lang/String;
    const-string v13, "[^0-9]"

    const-string v14, ""

    invoke-virtual {v11, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 237
    const-string v13, "[0]"

    const-string v14, ""

    invoke-virtual {v11, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 238
    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_1b

    .line 239
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mFingerprint.getVersion() is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v13, v6

    .line 241
    goto/16 :goto_1

    .line 244
    :cond_1b
    const/4 v13, 0x0

    aget-object v13, p1, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 245
    goto/16 :goto_2

    .line 247
    .end local v11    # "tempVersion":Ljava/lang/String;
    .end local v12    # "version":Ljava/lang/String;
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->responseNA()Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v6

    goto/16 :goto_2

    .restart local v3    # "EXPIRED_COUNTER":I
    .restart local v10    # "status":Ljava/lang/String;
    :cond_1d
    move v2, v3

    .end local v3    # "EXPIRED_COUNTER":I
    .restart local v2    # "EXPIRED_COUNTER":I
    goto/16 :goto_2
.end method

.method public onEvent(Lcom/validity/fingerprint/FingerprintEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/validity/fingerprint/FingerprintEvent;

    .prologue
    .line 338
    return-void
.end method

.method public readResult()Ljava/lang/String;
    .locals 8

    .prologue
    .line 296
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readResult"

    const-string v6, "***readResult ()"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/4 v3, 0x0

    .line 298
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 301
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_result"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 303
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 309
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 316
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    return-object v3

    .line 310
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 312
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 314
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 304
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 306
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 310
    :catch_2
    move-exception v2

    .line 312
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 308
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 309
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 313
    :goto_3
    throw v4

    .line 310
    :catch_3
    move-exception v2

    .line 312
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 308
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 304
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public readStatus()Ljava/lang/String;
    .locals 8

    .prologue
    .line 274
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readStatus"

    const-string v6, "***readStatus ()"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const/4 v3, 0x0

    .line 276
    .local v3, "status":Ljava/lang/String;
    const/4 v0, 0x0

    .line 279
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_status"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 281
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readStatus"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "status = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 286
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 292
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    return-object v3

    .line 287
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 288
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 290
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 282
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 283
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 286
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 287
    :catch_2
    move-exception v2

    .line 288
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 285
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 286
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 289
    :goto_3
    throw v4

    .line 287
    :catch_3
    move-exception v2

    .line 288
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 285
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 282
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method updateFirmware_melfas(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "updateFirmware_melfas"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "***updateFirmware ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    if-nez p1, :cond_1

    .line 322
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "updateFirmware_melfas"

    const-string v2, "***updateFirmware (TSP)"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v0, "fw_update,0"

    invoke-virtual {p0, v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->writeCmd(Ljava/lang/String;)V

    .line 332
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "updateFirmware_melfas"

    const-string v2, "Thread Notified Message"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void

    .line 324
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "updateFirmware_melfas"

    const-string v2, "***updateFirmware (TSP_UMS)"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v0, "fw_update,1"

    invoke-virtual {p0, v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->writeCmd(Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_2
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "updateFirmware_melfas"

    const-string v2, "***updateFirmware (TSP_FACTORY)"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v0, "fw_update,2"

    invoke-virtual {p0, v0}, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->writeCmd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeCmd(Ljava/lang/String;)V
    .locals 7
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 254
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "writeCmd = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x0

    .line 258
    .local v0, "bw":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const-string v4, "/sys/class/sec/tsp/cmd"

    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .local v1, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 260
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+++++++++++++++++++++++ write("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 266
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 271
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :goto_0
    return-void

    .line 267
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v2

    .line 268
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 270
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 261
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 262
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFirmvers;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+++++++++++++++++++++++"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 266
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 267
    :catch_2
    move-exception v2

    .line 268
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 265
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 266
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 269
    :goto_3
    throw v3

    .line 267
    :catch_3
    move-exception v2

    .line 268
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 265
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 261
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_1
.end method
