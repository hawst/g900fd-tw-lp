.class public Lcom/sec/factory/aporiented/athandler/AtBakupchk;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtBakupchk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;
    }
.end annotation


# static fields
.field static final I_NVBACKUP:I = 0x0

.field static final I_NVERASE:I = 0x2

.field static final I_NVRESTORE:I = 0x1

.field static final NVBACKUP:Ljava/lang/String; = "nvbackup"

.field static final NVERASE:Ljava/lang/String; = "nverase"

.field static final NVRECOVERY:Ljava/lang/String; = "nvrecovery"

.field static final NVRESTORE:Ljava/lang/String; = "nvrestore"

.field static final REBOOT:Ljava/lang/String; = "reboot"


# instance fields
.field private mNResult:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 19
    const-string v0, "BAKUPCHK"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CMD_NAME:Ljava/lang/String;

    .line 20
    const-string v0, "AtBakupchk"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    .line 21
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->NUM_ARGS:I

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/athandler/AtBakupchk;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtBakupchk;

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->mNResult:I

    return v0
.end method

.method private doSystemCall(I)V
    .locals 2
    .param p1, "cmd"    # I

    .prologue
    .line 91
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtBakupchk;Landroid/os/Looper;)V

    .line 122
    .local v0, "nvBackupHandler":Landroid/os/Handler;
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 123
    return-void
.end method

.method private setProperties(Ljava/lang/String;)Z
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setProperties"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmd = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "CPNV_BACKUP_CMD"

    invoke-static {v0, p1}, Lcom/sec/factory/support/Support$Properties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method DoShellCmd(Ljava/lang/String;)I
    .locals 12
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, -0x1

    .line 126
    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "DoShellCmd"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "cmd = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v4, 0x0

    .line 128
    .local v4, "p":Ljava/lang/Process;
    const/4 v8, 0x3

    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "/system/bin/sh"

    aput-object v9, v5, v8

    const-string v8, "-c"

    aput-object v8, v5, v6

    const/4 v8, 0x2

    aput-object p1, v5, v8

    .line 132
    .local v5, "shell_command":[Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "DoShellCmd"

    const-string v10, "exec command"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    .line 134
    invoke-virtual {v4}, Ljava/lang/Process;->waitFor()I

    .line 135
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;

    invoke-virtual {v4}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v1, p0, v8}, Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;-><init>(Lcom/sec/factory/aporiented/athandler/AtBakupchk;Ljava/io/InputStream;)V

    .line 136
    .local v1, "err":Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;
    new-instance v3, Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v3, p0, v8}, Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;-><init>(Lcom/sec/factory/aporiented/athandler/AtBakupchk;Ljava/io/InputStream;)V

    .line 137
    .local v3, "output":Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;
    invoke-virtual {v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;->start()V

    .line 138
    invoke-virtual {v3}, Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;->start()V

    .line 139
    iget-object v8, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "DoShellCmd"

    const-string v10, "exec done"

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 151
    iget-object v7, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    const-string v9, "DoShellCmd done"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .end local v1    # "err":Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;
    .end local v3    # "output":Lcom/sec/factory/aporiented/athandler/AtBakupchk$Reader;
    :goto_0
    return v6

    .line 140
    :catch_0
    move-exception v2

    .line 141
    .local v2, "exception":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DoShellCmd - IOException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 142
    goto :goto_0

    .line 143
    .end local v2    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 144
    .local v2, "exception":Ljava/lang/SecurityException;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SecurityException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 145
    goto :goto_0

    .line 146
    .end local v2    # "exception":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "DoShellCmd"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "InterruptedException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 148
    goto :goto_0
.end method

.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 38
    monitor-enter p0

    const/4 v0, 0x0

    .line 40
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 81
    :goto_1
    monitor-exit p0

    return-object v1

    .line 40
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->NUM_ARGS:I

    goto :goto_0

    .line 44
    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 45
    const-string v1, "nvrecovery"

    invoke-direct {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->setProperties(Ljava/lang/String;)Z

    .line 46
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_2
    move-object v1, v0

    .line 81
    goto :goto_1

    .line 55
    :cond_3
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 57
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->doSystemCall(I)V

    .line 58
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 60
    :cond_4
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "2"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 62
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->doSystemCall(I)V

    .line 63
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 65
    :cond_5
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 69
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "1"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 73
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "2"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBakupchk;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 38
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
